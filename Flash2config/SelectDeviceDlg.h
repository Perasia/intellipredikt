#pragma once
#include "afxwin.h"


// SelectDeviceDlg dialog

class SelectDeviceDlg : public CDialog
{
	DECLARE_DYNAMIC(SelectDeviceDlg)

public:
	SelectDeviceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~SelectDeviceDlg();
    void InitDpaComboBoxText();
    DWORD m_dwDeviceId;
    char m_SelectedDeviceName[128];
    HKEY m_hkey; // handle for key

// Dialog Data
	enum { IDD = IDD_DIALOG_SELECT_DEVICE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CComboBox m_cmbSelectDevice;
    afx_msg void OnBnClickedOk();
    virtual BOOL OnInitDialog();
    afx_msg void OnCbnSelchangeComboSelectDevice();
};
