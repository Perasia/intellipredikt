// CaymanConfig.h : main header file for the CAYMANCONFIG application
//

#if !defined(AFX_CAYMANCONFIG_H__67F99593_D1C0_47D3_AE58_4D2089BC76E2__INCLUDED_)
#define AFX_CAYMANCONFIG_H__67F99593_D1C0_47D3_AE58_4D2089BC76E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
    #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigApp:
// See CaymanConfig.cpp for the implementation of this class
//
#define CONFIGDLG_MAXVENDORENTRY    100
#define CONFIGDLG_MAXDEVICEENTRY    100

class CCaymanConfigApp : public CWinApp
{
public:
    CCaymanConfigApp();
    int Is64BitWindows(void);
    BOOL flag,shFlag;
    unsigned long nDeviceId;
    BOOL WriteRegistry(char *strParm);
    void UninstallRegistry();
#ifdef J2534_0305
    void CreateDefaults(HKEY *);
#endif

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCaymanConfigApp)
    public:
    virtual BOOL InitInstance();
    //}}AFX_VIRTUAL

// Implementation

    //{{AFX_MSG(CCaymanConfigApp)
        // NOTE - the ClassWizard will add and remove member functions here.
        //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAYMANCONFIG_H__67F99593_D1C0_47D3_AE58_4D2089BC76E2__INCLUDED_)
