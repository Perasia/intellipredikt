// Connect.cpp : implementation file
//

#include "stdafx.h"
#include "CaymanConfig.h"
#include "Connect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConnect dialog


CConnect::CConnect(CWnd* pParent /*=NULL*/)
    : CDialog(CConnect::IDD, pParent)
{
    //{{AFX_DATA_INIT(CConnect)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
}


void CConnect::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CConnect)
    DDX_Control(pDX, IDC_EDIT, m_ctrlEdtTryCom);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConnect, CDialog)
    //{{AFX_MSG_MAP(CConnect)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnect message handlers

BOOL CConnect::OnInitDialog() 
{
    CDialog::OnInitDialog();
    
    m_ctrlEdtTryCom.SetWindowText("Trying COM1");
    
    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}
