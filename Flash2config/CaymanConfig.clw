; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CLogConfig
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "caymanconfig.h"
LastPage=0

ClassCount=6
Class1=CCaymanConfigApp
Class2=CAboutDlg
Class3=CCaymanConfigDlg
Class4=CConnect
Class5=CJ2534AutoConfigWindow

ResourceCount=5
Resource1=IDD_DIALOG_CONNECT
Resource2=IDD_CAYMANCONFIG_DIALOG
Resource3=IDD_DIALOG1
Resource4=IDD_ABOUTBOX
Class6=CLogConfig
Resource5=IDD_LOG_CONFIG

[CLS:CCaymanConfigApp]
Type=0
BaseClass=CWinApp
HeaderFile=CaymanConfig.h
ImplementationFile=CaymanConfig.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=CaymanConfigDlg.cpp
ImplementationFile=CaymanConfigDlg.cpp
LastObject=CAboutDlg

[CLS:CCaymanConfigDlg]
Type=0
BaseClass=CDialog
HeaderFile=CaymanConfigDlg.h
ImplementationFile=CaymanConfigDlg.cpp
LastObject=IDC_ST_VENDOR
Filter=D
VirtualFilter=dWC

[CLS:CConnect]
Type=0
BaseClass=CDialog
HeaderFile=Connect.h
ImplementationFile=Connect.cpp

[CLS:CJ2534AutoConfigWindow]
Type=0
BaseClass=CDialog
HeaderFile=J2534AutoConfigWindow.h
ImplementationFile=J2534AutoConfigWindow.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CAYMANCONFIG_DIALOG]
Type=1
Class=CCaymanConfigDlg
ControlCount=23
Control1=IDC_ST_VENDOR,static,1342308352
Control2=IDC_ST_NAME,static,1342308352
Control3=IDC_ST_PROT_SUPP,static,1342308352
Control4=IDC_ST_CONF_APP,static,1342308352
Control5=IDC_ST_FUNC_LIB,static,1342308352
Control6=IDC_ST_API_VER,static,1342308352
Control7=IDC_EDT_VENDOR,edit,1350633600
Control8=IDC_EDT_NAME,edit,1350633600
Control9=IDC_EDT_PROT_SUPP,edit,1350633600
Control10=IDC_EDT_CONG_APP,edit,1350633600
Control11=IDC_EDT_FUNC_LIB,edit,1350633600
Control12=IDC_EDT_API_VER,edit,1350633600
Control13=IDC_BTN_OK,button,1342242816
Control14=IDC_BTN_CANCEL,button,1073807360
Control15=IDC_ST_PROD_VER2,static,1342308352
Control16=IDC_EDT_PROD_VER,edit,1350633600
Control17=IDC_CHECKLOGGING,button,1342242819
Control18=IDC_BTN_CANCEL2,button,1342242816
Control19=IDC_BTN_OK2,button,1342242816
Control20=IDC_BTN_LOG_CONFIGURATION,button,1073807360
Control21=IDC_STATIC_LOG,button,1342177287
Control22=IDC_COMBO_DPA,combobox,1075904515
Control23=IDC_ST_DPA_TYPE,static,1342308352

[DLG:IDD_DIALOG_CONNECT]
Type=1
Class=CConnect
ControlCount=2
Control1=IDC_STATIC,static,1342308352
Control2=IDC_EDIT,edit,1350633600

[DLG:IDD_DIALOG1]
Type=1
Class=CJ2534AutoConfigWindow
ControlCount=2
Control1=IDC_STATIC,static,1342308352
Control2=IDC_EDIT1,edit,1350633472

[DLG:IDD_LOG_CONFIG]
Type=1
Class=CLogConfig
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_CHECK1,button,1342242819
Control4=IDC_CHECK2,button,1342242819
Control5=IDC_CHECK3,button,1342242819
Control6=IDC_STATIC_LOGGING_TYPE,button,1342177287
Control7=IDC_STATIC_LOGGING_METHOD,button,1342177287
Control8=IDC_RADIO1,button,1342308361
Control9=IDC_RADIO2,button,1342308361

[CLS:CLogConfig]
Type=0
HeaderFile=LogConfig.h
ImplementationFile=LogConfig.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_CHECK1

