// CaymanConfigDlg.h : header file
//

#if !defined(AFX_CAYMANCONFIGDLG_H__105E4236_7209_47EB_B287_389190F86CF0__INCLUDED_)
#define AFX_CAYMANCONFIGDLG_H__105E4236_7209_47EB_B287_389190F86CF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LogConfig.h"
#include "SelectDeviceDlg.h"

typedef enum USBDPAType
{
    eUSBAny,        //connect to any Euro5 USB device attached to the PC
    eUSBSoftBridge, //connect to just a SoftBridge USB device
    eUSBPx3,        //connect to just a Px-3 USB device
    eUSBWurth,      //connect to just a Wurth USB device
    eUSBDelphi,     //connect to just a Delphi USB device
    eUSBSVCI        //connect to just a SVCI USB device
} USBDPAType;


// Constants
#define DBRIDGE_DEVICEID 2
#define DPA5_DEVICEID 1
#ifdef J2534_0305
#define J2534REGISTRY_KEY_PATH              "Software\\PassThruSupport"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport"
#else
#ifdef J2534_0404
#define J2534REGISTRY_KEY_PATH          "Software\\PassThruSupport.04.04"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport.04.04"

#else 
#ifdef J2534_0500
#define J2534REGISTRY_KEY_PATH          "Software\\PassThruSupport.05.00"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport.05.00"
#endif
#endif
#endif

#define J2534REGISTRY_DEVICE_ID         "DeviceId"

//Constants depending on which Vendor and Device we're building
#if defined (J2534_DEVICE_CAYMAN)
#define J2534REGISTRY_DEARBORN_GROUP    "Cardone Industries, Inc. - FLASH2 All-Makes Reprogrammer"
#define DEVICE_INI                      "DGFlash2Console.INI"
#define CONFIGAPP                       "Flash2ToolConfig.exe"
#define FUNCLIB                         "\\dgFlsh32.dll"
#define DEVICEDESC                      "Flash2"
#define REGISTRY_NAME                   "FLASH2 All-Makes Reprogrammer"
#define VENDOR                          "Cardone Industries, Inc."
#define PROTOCOLSSUPPORTED              "J1850VPW:1, J1850PWM:1, CAN:1, ISO9141:1, ISO14230:1, ISO15765:1, SCI_A_ENGINE:1, SCI_A_TRANS:1, SCI_B_ENGINE:1, SCI_B_TRANS:1, SW_ISO15765_PS:1, SW_CAN_PS:1, SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:2,ISO15765_PS:2,CAN_CH1:1,CAN_CH2:1,ISO15765_CH1:1,ISO15765_CH2:1, GM_UART_PS:1"

#else
#if defined (J2534_DEVICE_VSI)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - VSI-2534"
#define DEVICE_INI                      "DGVSI.INI"
#define CONFIGAPP                       "VSI2534Utility.exe"
#define FUNCLIB                         "\\dgVSI32.dll"
#define DEVICEDESC                      "VSI-2534"
#define REGISTRY_NAME                   "VSI-2534"
#define VENDOR                          "Dearborn Group, Inc."
#define PROTOCOLSSUPPORTED              "J1850VPW:1, J1850PWM:1, CAN:1,ISO14230:1,ISO15765:1,ISO9141:1,SCI_A_ENGINE:1,SCI_A_TRANS:1,SCI_B_ENGINE:1,SCI_B_TRANS:1,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:2,ISO15765_PS:2,CAN_CH1:1,CAN_CH2:1,ISO15765_CH1:1,ISO15765_CH2:1,GM_UART_PS:1"

#else
#if defined (J2534_DEVICE_KRHTWIRE)
#define J2534REGISTRY_DEARBORN_GROUP    "KeylessRide - KeylessRide HotWire"
#define DEVICE_INI                      "KRHtWire.INI"
#define CONFIGAPP                       "KRHtWireUtility.exe"
#define FUNCLIB                         "\\KRHtWire.dll"
#define DEVICEDESC                      "KeylessRide HotWire"
#define REGISTRY_NAME                   "KeylessRide HotWire"
#define VENDOR                          "KeylessRide"
#define PROTOCOLSSUPPORTED              "J1850VPW:1, J1850PWM:1, CAN:1, ISO9141:1, ISO14230:1, ISO15765:1, SCI_A_ENGINE:1, SCI_A_TRANS:1, SCI_B_ENGINE:1, SCI_B_TRANS:1, SW_ISO15765_PS:1, SW_CAN_PS:1, SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:2,ISO15765_PS:2,CAN_CH1:1,CAN_CH2:1,ISO15765_CH1:1,ISO15765_CH2:1, GM_UART_PS:1, CCD:1"

#else
#if defined (J2534_DEVICE_DPA5)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - DPA 5"
#define DEVICE_INI                      "DGDPA5SA.INI"
#define CONFIGAPP                       "DPA5J2534Utility.exe"
#define FUNCLIB                         "\\DGDPA532.dll"
#define DEVICEDESC                      "DPA 5"
#define REGISTRY_NAME                   "DPA 5"
#define VENDOR                          "Dearborn Group, Inc."
#define PROTOCOLSSUPPORTED              "CAN:1,ISO14230:1,ISO15765:1,ISO9141:1,J1939_PS:3,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:3,ISO15765_PS:3,CAN_CH1:1,CAN_CH2:1,CAN_CH3:1,CAN_CH4:1,ISO15765_CH1:1,ISO15765_CH2:1,ISO15765_CH3:1,ISO15765_CH4:1,J1939_CH1:1,J1939_CH2:1,J1939_CH3:1,J1939_CH4:1,J1850VPW:1,J1708_PS:1,J1708_CH1:1"

#else
#if defined (J2534_DEVICE_SOFTBRIDGE)
#define J2534REGISTRY_DEARBORN_GROUP    "Autocom Diagnostic Partner AB - SoftBridge"
#define DEVICE_INI                      "Euro5M32.ini"
#define CONFIGAPP                       "SoftBridgeEuro5Utility.exe"
#define FUNCLIB                         "\\SoftBridge.dll"
#define DEVICEDESC                      "SoftBridge"
#define REGISTRY_NAME                   "SoftBridge"
#define VENDOR                          "Autocom Diagnostic Partner AB"
#define DEVICE_TYPE                     eUSBSoftBridge

#else
#if defined (J2534_DEVICE_WURTH)
#define J2534REGISTRY_DEARBORN_GROUP    "W�rth Online World! - WoWFlashBox"
#define DEVICE_INI                      "Euro5M32.ini"
#define CONFIGAPP                       "WoWFlashBoxEuro5Utility.exe"
#define FUNCLIB                         "\\WoWFlashBox.dll"
#define DEVICEDESC                      "WoWFlashBox"
#define REGISTRY_NAME                   "WoW! Flash Box"
#define VENDOR                          "W�rth Online World!"
#define DEVICE_TYPE                     eUSBWurth

#else
#if defined (J2534_DEVICE_DELPHI)
#define J2534REGISTRY_DEARBORN_GROUP    "Delphi - Delphi Euro 5 Diagnostics"
#define DEVICE_INI                      "Euro5M32.ini"
#define CONFIGAPP                       "DelphiEuro5Utility.exe"
#define FUNCLIB                         "\\DelphiEuro5.dll"
#define DEVICEDESC                      "Delphi Euro 5 Diagnostics"
#define REGISTRY_NAME                   "Delphi Euro 5 Diagnostics"
#define VENDOR                          "Delphi"
#define DEVICE_TYPE                     eUSBDelphi

#else
#if defined (J2534_DEVICE_DBRIDGE) && !defined (J2534_DEVICE_SVCI)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - d-briDGe"
#define DEVICE_INI                      "Euro5M32.ini"
#define CONFIGAPP                       "dbriDGeUtility.exe"
#define FUNCLIB                         "\\dbriDGe.dll"
#define DEVICEDESC                      "d-briDGe"
#define REGISTRY_NAME                   "d-briDGe"
#define VENDOR                          "Dearborn Group, Inc."
#define PROTOCOLSSUPPORTED              "CAN:1,ISO14230:1,ISO15765:1,ISO9141:1,J1939_PS:3,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:3,ISO15765_PS:3,CAN_CH1:1,CAN_CH2:1,CAN_CH3:1,CAN_CH4:1,ISO15765_CH1:1,ISO15765_CH2:1,ISO15765_CH3:1,ISO15765_CH4:1,J1939_CH1:1,J1939_CH2:1,J1939_CH3:1,J1939_CH4:1,J1850VPW:1"
#define DEVICE_TYPE                     eUSBPx3

#else
#if defined (J2534_DEVICE_SVCI)
#define J2534REGISTRY_DEARBORN_GROUP    "Zhiche Automotive Technologies - SVCI"
#define DEVICE_INI                      "Euro5M32.ini"
#define CONFIGAPP                       "SVCIUtility.exe"
#define FUNCLIB                         "\\SVCI.dll"
#define DEVICEDESC                      "SVCI"
#define REGISTRY_NAME                   "SVCI"
#define VENDOR                          "Zhiche Automotive Technologies"
#define PROTOCOLSSUPPORTED              "CAN:1,ISO14230:1,ISO15765:1,ISO9141:1,J1939_PS:2,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:2,ISO15765_PS:2,CAN_CH1:1,CAN_CH2:1,ISO15765_CH1:1,ISO15765_CH2:1,J1939_CH1:1,J1939_CH2:1,J1850VPW:1"
#define DEVICE_TYPE                     eUSBSVCI

#else
#if defined (J2534_DEVICE_DBRIDGE0305)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - d-briDGe"
#define DEVICE_INI                      "Euro5M32.ini"
 #ifdef UD_TRUCK
 #define CONFIGAPP                       "UDTruckUtility.exe"
 #else
 #define CONFIGAPP                       "dbriDGe0305Utility.exe"
 #endif
 #ifdef UD_TRUCK
#define FUNCLIB                         "\\dgUDT305.dll"
 #else
#define FUNCLIB                         "\\dbriDGe0305.dll"
 #endif
#define DEVICEDESC                      "d-briDGe"
 #ifdef UD_TRUCK
#define REGISTRY_NAME                   "Python1B"
 #else
#define REGISTRY_NAME                   "d-briDGe"
 #endif
#define VENDOR                          "Dearborn Group Technology, Inc."
#define PROTOCOLSSUPPORTED              "J1850VPW, J1850PWM, CAN, ISO9141, ISO14230, ISO15765"
#define DEVICE_TYPE                     eUSBPx3

#else
#if defined (J2534_DEVICE_NETBRIDGE)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - Netbridge"
#define DEVICE_INI                      "NBRIDGES.INI"
#define CONFIGAPP                       "NetbridgeJ2534Utility.exe"
#define FUNCLIB                         "\\nbridgej.dll"
#define DEVICEDESC                      "Netbridge"
#define REGISTRY_NAME                   "Netbridge"
#define VENDOR                          "Dearborn Group, Inc."
#define PROTOCOLSSUPPORTED              "CAN:1,ISO14230:1,ISO15765:1,J1939_PS:3,J1939_CH1:1,J1939_CH2:1,J1939_CH3:1,J1939_CH4:1,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:3,ISO15765_PS:3,CAN_CH1:1,CAN_CH2:1,CAN_CH3:1,CAN_CH4:1,ISO15765_CH1:1,ISO15765_CH2:1,ISO15765_CH3:1,ISO15765_CH4:1,FT_CAN_CH1:1,FT_ISO15765_CH1:1"

#else
#if defined (J2534_DEVICE_DPA50305)
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc. - DPA 5"
#define DEVICE_INI                      "DGDPA5SA.INI"
 #ifdef UD_TRUCK
#define CONFIGAPP                       "DPA5UDTruckUtility.exe"
 #else
#define CONFIGAPP                       "DPA50305Utility.exe"
 #endif
 #ifdef UD_TRUCK
#define FUNCLIB                         "\\UDTDPA32.dll"
 #else
#define FUNCLIB                         "\\DGDPA35.dll"
 #endif
#define DEVICEDESC                      "DPA 5"
 #ifdef UD_TRUCK
#define REGISTRY_NAME                   "Python1B"
 #else
#define REGISTRY_NAME                   "DPA 5"
 #endif
#define VENDOR                          "Dearborn Group Technology, Inc."
#define PROTOCOLSSUPPORTED              "J1850VPW, J1850PWM, CAN, ISO9141, ISO14230, ISO15765"

#else
#error  ERROR: J2534Registry.h Define registry entries for vendor and tool
#endif //dpa50305
#endif //netbridge
#endif //d-bridge0305
#endif //SVCI d-bridge
#endif //d-bridge
#endif //delphi
#endif //wurth
#endif //softbridge
#endif //dpa5
#endif //krhw
#endif //vsi
#endif //cardone

#if defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI)
#define PROTOCOLSSUPPORTED              "CAN:1,ISO14230:1,ISO15765:1,ISO9141:1,J1939_PS:2,SW_ISO15765_PS:1,SW_CAN_PS:1,SW_CAN_PS:1,SW_CAN_ISO15765_CH1:1,SW_CAN_CAN_CH1:1,CAN_PS:2,ISO15765_PS:2,CAN_CH1:1,CAN_CH2:1,ISO15765_CH1:1,ISO15765_CH2:1,J1939_CH1:1,J1939_CH2:1"
#endif

#define J2534REGISTRY_VENDOR            "Vendor"
#define J2534REGISTRY_NAME              "Name"
#define J2534REGISTRY_PROTOCOLSSUPPORT  "ProtocolsSupported"
#define J2534REGISTRY_CONFIGAPP         "ConfigApplication"
#define J2534REGISTRY_FUNCLIB           "FunctionLibrary"
#define J2534REGISTRY_APIVERSION        "APIVersion"
#define J2534REGISTRY_PRODUCTVERSION    "ProductVersion"
#define J2534REGISTRY_LANGUAGE          "Language"
#define J2534REGISTRY_LOGGING           "Logging"
#define J2534REGISTRY_LOGGINGDIRECTORY  "LoggingDirectory"
#define J2534REGISTRY_LOGGING_MAXSIZE   "LoggingSizeMaxKB"
#define J2534REGISTRY_CAN               "CAN"
#define J2534REGISTRY_ISO15765          "ISO15765"
#define J2534REGISTRY_J1850PWM          "J1850PWM"
#define J2534REGISTRY_J1850VPW          "J1850VPW"
#define J2534REGISTRY_ISO9141           "ISO9141"
#define J2534REGISTRY_ISO14230          "ISO14230"
#define J2534REGISTRY_SCI_A_ENGINE      "SCI_A_ENGINE"
#define J2534REGISTRY_SCI_A_TRANS       "SCI_A_TRANS"
#define J2534REGISTRY_SCI_B_ENGINE      "SCI_B_ENGINE"
#define J2534REGISTRY_SCI_B_TRANS       "SCI_B_TRANS"
#define J2534REGISTRY_SW_ISO15765_PS    "SW_ISO15765_PS"
#define J2534REGISTRY_SW_CAN_PS         "SW_CAN_PS"       
#define J2534REGISTRY_SW_CAN_ISO15765_CH1    "SW_CAN_ISO15765_CH1"
#define J2534REGISTRY_SW_CAN_CAN_CH1         "SW_CAN_CAN_CH1"
#define J2534REGISTRY_GM_UART_PS        "GM_UART_PS"
#define J2534REGISTRY_CCD               "CCD"
#define J2534REGISTRY_J1708             "J1708_PS"
#define J2534REGISTRY_J1708_CH1         "J1708_CH1"
#define J2534REGISTRY_J1939             "J1939_PS"
#define J2534REGISTRY_J1939_CH1         "J1939_CH1"
#define J2534REGISTRY_J1939_CH2         "J1939_CH2"
#define J2534REGISTRY_J1939_CH3         "J1939_CH3"
#define J2534REGISTRY_J1939_CH4         "J1939_CH4"
#define J2534REGISTRY_CAN_PS            "CAN_PS"
#define J2534REGISTRY_ISO15765_PS       "ISO15765_PS"
#define J2534REGISTRY_CAN_CH1           "CAN_CH1"
#define J2534REGISTRY_CAN_CH2           "CAN_CH2"
#define J2534REGISTRY_CAN_CH3           "CAN_CH3"
#define J2534REGISTRY_CAN_CH4           "CAN_CH4"
#define J2534REGISTRY_ISO15765_CH1      "ISO15765_CH1"
#define J2534REGISTRY_ISO15765_CH2      "ISO15765_CH2"
#define J2534REGISTRY_ISO15765_CH3      "ISO15765_CH3"
#define J2534REGISTRY_ISO15765_CH4      "ISO15765_CH4"
#define J2534REGISTRY_FT_CAN_CH1        "FT_CAN_CH1"      
#define J2534REGISTRY_FT_ISO15765_CH1   "FT_ISO15765_CH1"


// DPA specific
#define J2534REGISTRY_DEVICE_DPA_COMMPORT   "CommPort"
#define J2534REGISTRY_DEVICE_DPA_PYTHON_TYPE   "Python Type"

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigDlg dialog

class CCaymanConfigDlg : public CDialog
{
// Construction
public:
    CCaymanConfigDlg(CWnd* pParent = NULL); // standard constructor
    int Is64BitWindows(void);
    CString m_strDeviceName;
    HKEY FindJ2534Entry();
    HKEY tempkey, tempkey2, m_hkey; // handle for key
    void ReadValues();  // function for reading registry key values
    unsigned long g_ulLogging;
    CLogConfig m_clLogConfig;
    int iLanguage;
    void FillDialog();
    DWORD m_dwDeviceId;
    char m_SelectedDeviceName[128];
    SelectDeviceDlg m_clsSelectDeviceDlg;

// Dialog Data
    //{{AFX_DATA(CCaymanConfigDlg)
    enum { IDD = IDD_CAYMANCONFIG_DIALOG };
    CButton m_ctrlBtnLogConfig;
    CButton m_ctrlChkLogging;
    CComboBox   m_ctrlComboDPA;
    CEdit   m_ctrlEdtVendor;
    CEdit   m_ctrlEdtProtSupp;
    CEdit   m_ctrlProdVersion;
    CEdit   m_ctrlEdtName;
    CEdit   m_ctrlEdtFuncLib;
    CEdit   m_ctrlEdtConfigApp;
    CEdit   m_ctrlEdtApi;
    CButton m_ctrlBtnOk;
    CButton m_ctrlBtnOk2;
    CButton m_ctrlBtnCancel;
    CButton m_ctrlBtnCancel2;
    CString m_cszDataType;
    CString m_cszName;
    CString m_cszProtSup;
    CString m_cszFuncLib;
    CString m_cszConfigApp;
    CString m_cszProdVer;
    CString m_cszVendor;
    CString m_cszAPIVersion;
    //}}AFX_DATA

    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCaymanConfigDlg)
    public:
    virtual BOOL DestroyWindow();
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    HICON m_hIcon;

    // Generated message map functions
    //{{AFX_MSG(CCaymanConfigDlg)
    virtual BOOL OnInitDialog();
    void InitDpaComboBoxText();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnBtnOk();
    afx_msg void OnBtnOk2();
    afx_msg void OnBtnCancel();
    afx_msg void OnBtnCancel2();
    afx_msg void OnBtnLogConfiguration();
    afx_msg void OnChecklogging();
    afx_msg void OnSelchangeComboDpa();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedBtnSelectDevice();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CAYMANCONFIGDLG_H__105E4236_7209_47EB_B287_389190F86CF0__INCLUDED_)
