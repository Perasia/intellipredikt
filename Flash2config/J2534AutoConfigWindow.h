#if !defined(AFX_J2534AUTOCONFIGWINDOW_H__7E92829E_7CB2_4EAC_AC0C_79161E2BA545__INCLUDED_)
#define AFX_J2534AUTOCONFIGWINDOW_H__7E92829E_7CB2_4EAC_AC0C_79161E2BA545__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// J2534AutoConfigWindow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJ2534AutoConfigWindow dialog

class CJ2534AutoConfigWindow : public CDialog
{
// Construction
public:
    CJ2534AutoConfigWindow(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
    //{{AFX_DATA(CJ2534AutoConfigWindow)
    enum { IDD = IDD_DIALOG1 };
    CEdit   m_hText;
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CJ2534AutoConfigWindow)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CJ2534AutoConfigWindow)
        // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_J2534AUTOCONFIGWINDOW_H__7E92829E_7CB2_4EAC_AC0C_79161E2BA545__INCLUDED_)
