# Microsoft Developer Studio Project File - Name="CaymanConfig" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CaymanConfig - Win32 J2534_dbriDGe_Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CaymanConfig.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CaymanConfig.mak" CFG="CaymanConfig - Win32 J2534_dbriDGe_Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CaymanConfig - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_VSI_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_VSI_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_KRHTWIRE_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_KRHTWIRE_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_PX3_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_PX3_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_DPA_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_DPA_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_DPA5_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_DPA5_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_SoftBridge_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_SoftBridge_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_Wurth_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_Wurth_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_Delphi_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_Delphi_Release" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_dbriDGe_Debug" (based on "Win32 (x86) Application")
!MESSAGE "CaymanConfig - Win32 J2534_dbriDGe_Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DEI2005003/CaymanConfig", KNGAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CaymanConfig - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CAYMAN" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Flash2ToolConfig.exe"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Desc=Embed Manifest file
PostBuild_Cmds=mt.exe -manifest Release\Flash2ToolConfig.exe.manifest -outputresource:Release\Flash2ToolConfig.exe;#1
# End Special Build Tool

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CAYMAN" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Flash2ToolConfig.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_VSI_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_VSI_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_VSI_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_VSI_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_VSI_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Flash2ToolConfig.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/VSI2534Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_VSI_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_VSI_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_VSI_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_VSI_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_VSI_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Flash2ToolConfig.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/VSI2534Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_KRHTWIRE_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_KRHTWIRE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/VSI2534Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/KRHtWireUtility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_KRHTWIRE_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Debug0"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Debug0"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_KRHTWIRE_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CAYMAN" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_KRHTWIRE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Flash2ToolConfig.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/KRHtWireUtility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_PX3_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_PX3_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_PX3_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_PX3_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_PX3_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/VSI2534Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Px-3J2534Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_PX3_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_PX3_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_PX3_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_PX3_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_PX3_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/VSI2534Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Px-3J2534Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_DPA_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_DPA_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_DPA_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_DPA_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_DPA_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA4" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/VSI2534Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/DPA2534Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_DPA_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_DPA_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_DPA_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_DPA_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_DPA_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA4" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/VSI2534Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/DPA5J2534Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_DPA5_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_DPA5_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_DPA5_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_DPA5_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_DPA5_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Px-3J2534Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/DPA5J2534Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_DPA5_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_DPA5_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_DPA5_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_DPA5_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_DPA5_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Px-3J2534Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/DPA5J2534Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_SoftBridge_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_SoftBridge_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_SoftBridge_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_SoftBridge_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_SoftBridge_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Px-3J2534Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/SoftBridgeEuro5Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_SoftBridge_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_SoftBridge_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_SoftBridge_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_SoftBridge_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_SoftBridge_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Px-3J2534Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/SoftBridgeEuro5Utility.exe"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Desc=Embed Manifest file
PostBuild_Cmds=mt.exe -manifest Release\SoftBridgeEuro5Utility.exe.manifest -outputresource:Release\SoftBridgeEuro5Utility.exe;#1
# End Special Build Tool

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_Wurth_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_Wurth_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_Wurth_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_Wurth_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_Wurth_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_WURTH" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/SoftBridgeEuro5Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/WoWFlashBoxEuro5Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_Wurth_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_Wurth_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_Wurth_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_Wurth_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_Wurth_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_WURTH" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/SoftBridgeEuro5Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/WoWFlashBoxEuro5Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_Delphi_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_Delphi_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_Delphi_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_Delphi_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_Delphi_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DELPHI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/SoftBridgeEuro5Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/DelphiEuro5Utility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_Delphi_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_Delphi_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_Delphi_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_Delphi_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_Delphi_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DELPHI" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/SoftBridgeEuro5Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/DelphiEuro5Utility.exe"

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_dbriDGe_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_dbriDGe_Debug"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_dbriDGe_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "CaymanConfig___Win32_J2534_dbriDGe_Debug"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_dbriDGe_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/SoftBridgeEuro5Utility.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/dbriDGeUtility.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CaymanConfig - Win32 J2534_dbriDGe_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "CaymanConfig___Win32_J2534_dbriDGe_Release"
# PROP BASE Intermediate_Dir "CaymanConfig___Win32_J2534_dbriDGe_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "CaymanConfig___Win32_J2534_dbriDGe_Release"
# PROP Intermediate_Dir "CaymanConfig___Win32_J2534_dbriDGe_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/SoftBridgeEuro5Utility.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/dbriDGeUtility.exe"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Desc=Embed Manifest file
PostBuild_Cmds=mt.exe -manifest Release\SoftBridgeEuro5Utility.exe.manifest -outputresource:Release\SoftBridgeEuro5Utility.exe;#1
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "CaymanConfig - Win32 Release"
# Name "CaymanConfig - Win32 Debug"
# Name "CaymanConfig - Win32 J2534_VSI_Release"
# Name "CaymanConfig - Win32 J2534_VSI_Debug"
# Name "CaymanConfig - Win32 J2534_KRHTWIRE_Release"
# Name "CaymanConfig - Win32 J2534_KRHTWIRE_Debug"
# Name "CaymanConfig - Win32 J2534_PX3_Debug"
# Name "CaymanConfig - Win32 J2534_PX3_Release"
# Name "CaymanConfig - Win32 J2534_DPA_Release"
# Name "CaymanConfig - Win32 J2534_DPA_Debug"
# Name "CaymanConfig - Win32 J2534_DPA5_Debug"
# Name "CaymanConfig - Win32 J2534_DPA5_Release"
# Name "CaymanConfig - Win32 J2534_SoftBridge_Debug"
# Name "CaymanConfig - Win32 J2534_SoftBridge_Release"
# Name "CaymanConfig - Win32 J2534_Wurth_Debug"
# Name "CaymanConfig - Win32 J2534_Wurth_Release"
# Name "CaymanConfig - Win32 J2534_Delphi_Debug"
# Name "CaymanConfig - Win32 J2534_Delphi_Release"
# Name "CaymanConfig - Win32 J2534_dbriDGe_Debug"
# Name "CaymanConfig - Win32 J2534_dbriDGe_Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CaymanConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\CaymanConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Connect.cpp
# End Source File
# Begin Source File

SOURCE=.\J2534AutoConfigWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\LogConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CaymanConfig.h
# End Source File
# Begin Source File

SOURCE=.\CaymanConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\Connect.h
# End Source File
# Begin Source File

SOURCE=.\J2534AutoConfigWindow.h
# End Source File
# Begin Source File

SOURCE=.\LogConfig.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\CaymanConfig.ico
# End Source File
# Begin Source File

SOURCE=.\CaymanConfig.rc
# End Source File
# Begin Source File

SOURCE=.\res\CaymanConfig.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
