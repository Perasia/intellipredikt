#if !defined(AFX_CONNECT_H__ECF185F6_2CFE_4D1A_8C62_B17897CC6E55__INCLUDED_)
#define AFX_CONNECT_H__ECF185F6_2CFE_4D1A_8C62_B17897CC6E55__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Connect.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CConnect dialog

class CConnect : public CDialog
{
// Construction
public:
    CConnect(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
    //{{AFX_DATA(CConnect)
    enum { IDD = IDD_DIALOG_CONNECT };
    CEdit   m_ctrlEdtTryCom;
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CConnect)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CConnect)
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNECT_H__ECF185F6_2CFE_4D1A_8C62_B17897CC6E55__INCLUDED_)
