// CaymanConfig.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CaymanConfig.h"
#include "CaymanConfigDlg.h"
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigApp

BEGIN_MESSAGE_MAP(CCaymanConfigApp, CWinApp)
    //{{AFX_MSG_MAP(CCaymanConfigApp)
        // NOTE - the ClassWizard will add and remove mapping macros here.
        //    DO NOT EDIT what you see in these blocks of generated code!
    //}}AFX_MSG
    ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigApp construction

CCaymanConfigApp::CCaymanConfigApp()
{
      flag=1;
      shFlag=1;
    // TODO: add construction code here,
    // Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCaymanConfigApp object

CCaymanConfigApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigApp initialization

int  CCaymanConfigApp::Is64BitWindows(void)
{
#if defined(_WIN64)
    return TRUE;             // 64-bit programs run only on Win64
#elif defined(_WIN32)

    //
    // 32-bit programs run on both 32-bit and 64-bit Windows so must sniff

    BOOL f64 = FALSE;

    return IsWow64Process(GetCurrentProcess(), &f64) && f64;

#else
    return FALSE; // Win64 does not support Win16
#endif
}

BOOL CCaymanConfigApp::InitInstance()
{
    AfxEnableControlContainer();

    // Standard initialization
    // If you are not using these features and wish to reduce the size
    //  of your final executable, you should remove from the following
    //  the specific initialization routines you do not need.
    char *strParm = "";
    if (__argc > 2)
    {
        strParm = __argv[2];
    }
    CCommandLineInfo cmdLine;
    ParseCommandLine(cmdLine);
    if (!cmdLine.m_strFileName.CompareNoCase(_T("delete")))
    {
#if defined (J2534_0404) || defined (J2534_0500)
        HKEY hMainkey;
        long lSuccess = RegCreateKey(HKEY_LOCAL_MACHINE, J2534REGISTRY_KEY_PATH,&hMainkey); // "Software\PassThruSupport.04.04 || Software\PassThruSupport.05.00"
        if(lSuccess == ERROR_SUCCESS)
        {
            RegDeleteKey(hMainkey, J2534REGISTRY_DEARBORN_GROUP);
        }
#endif
#ifdef J2534_0305
        UninstallRegistry();
#endif
         return FALSE;
    }

    if (!cmdLine.m_strFileName.CompareNoCase(_T("create")))
    {
        WriteRegistry(strParm);
        return FALSE;
    }
#ifdef J2534_0305
    if (!cmdLine.m_strFileName.CompareNoCase(_T("UsedbriDGe")))
    {
        char szMessage[MAX_PATH];
        char tempbuffer[MAX_PATH];

        // Prompt the user and confirm they want to use d-briDGe instead of the original Python tool.
        sprintf_s( szMessage, "This will enable the use of d-briDGe instead of the original Python tool");
        if( IDCANCEL == MessageBox(NULL, szMessage, "d-briDGe Setup", MB_OKCANCEL ) ) 
            return FALSE;

        // If dbriDGe0305.dll exist, proceed to copy it 
        if (Is64BitWindows())
        {
            GetSystemWow64Directory(tempbuffer,MAX_PATH);
        }
        else
        {
            GetSystemDirectory(tempbuffer,MAX_PATH);
        }
        CString PythonDLL = tempbuffer;
        PythonDLL += "\\dbriDGe0305.dll";
        if( (_access( PythonDLL, 0 )) == -1 )
        {
            sprintf_s( szMessage, "d-briDGe DLL missing\nInstall d-briDGe again");
            MessageBox(NULL, szMessage, "d-briDGe Setup", MB_OK );
            return FALSE;
        }
        WriteRegistry("");
        // Replace DGPytb32.dll with dbriDGe0305.dll
        CString PythonDLLDestination = tempbuffer;
        PythonDLLDestination += "\\DGPytb32.dll";
        CopyFile(PythonDLL, PythonDLLDestination, FALSE);
        return FALSE;   // We're done here - bail out
    }    
    if (!cmdLine.m_strFileName.CompareNoCase(_T("UseDPA5")))
    {
        char szMessage[MAX_PATH];
        char tempbuffer[MAX_PATH];

        // Prompt the user and confirm they want to use DPA 5 instead of the original Python tool.
        sprintf_s( szMessage, "This will enable the use of DPA 5 instead of the original Python tool");
        if( IDCANCEL == MessageBox(NULL, szMessage, "DPA 5 Setup", MB_OKCANCEL ) ) 
            return FALSE;

        // If UDTDPA32.dll exist, proceed to copy it 
        if (Is64BitWindows())
        {
            GetSystemWow64Directory(tempbuffer,MAX_PATH);
        }
        else
        {
            GetSystemDirectory(tempbuffer,MAX_PATH);
        }
        CString PythonDLL = tempbuffer;
        PythonDLL += "\\UDTDPA32.dll";
        if( (_access( PythonDLL, 0 )) == -1 )
        {
            sprintf_s( szMessage, "DPA 5 DLL missing\nInstall DPA 5 again");
            MessageBox(NULL, szMessage, "DPA 5 Setup", MB_OK );
            return FALSE;
        }
        WriteRegistry("");
        // Replace DGPytb32.dll with UDTDPA32.dll
        CString PythonDLLDestination = tempbuffer;
        PythonDLLDestination += "\\DGPytb32.dll";
        CopyFile(PythonDLL, PythonDLLDestination, FALSE);
        return FALSE;   // We're done here - bail out
    }
    if (!cmdLine.m_strFileName.CompareNoCase(_T("UsePython")))
    {
        char szMessage[MAX_PATH];
        char tempbuffer[MAX_PATH];
        CString strLogFilepath;

        // Prompt the user and confirm they want to use the original Python tool.
        sprintf_s( szMessage, "This will enable the use the original Python tool instead of d-briDGe");
        if( IDCANCEL == MessageBox(NULL, szMessage, "d-briDGe Setup", MB_OKCANCEL ) )
            return FALSE;

        // If C:\Python1B2534\Python1BConfig.exe and DGPytb32.dll exist, proceed to execute it
        CString PythonConfigApp = "C:\\Python1B2534\\Python1BConfig.exe";
        CString PythonDLL = "DGPytb32.dll";
        if( (_access( PythonConfigApp, 0 )) == -1 )
        {
            sprintf_s( szMessage, "Python tool not previously installed\nInstall it if you wish to use it");
            MessageBox(NULL, szMessage, "Python Setup", MB_OK );
            return FALSE;
        }
        if( (_access( PythonDLL, 0 )) == -1 )
        {
            sprintf_s( szMessage, "Python DLL missing\nInstall d-briDGe again");
            MessageBox(NULL, szMessage, "Python Setup", MB_OK );
            return FALSE;
        }
        // Prompt the user with instruction and confirm they want to continue.
        sprintf_s( szMessage, "On the next screen:\n  + Select 'USB' for the Python interface\n  + Select 'AutoDetect'\nAfter you see 'Successfully detected Python.', select 'OK'");
        if( IDCANCEL == MessageBox(NULL, szMessage, "Python Setup", MB_OKCANCEL ) )
            return FALSE;

        OSVERSIONINFO osvi;
        CString ShellExecuteVerb;
        osvi.dwOSVersionInfoSize = sizeof(osvi);
        GetVersionEx(&osvi);
        if( osvi.dwMajorVersion < 6 )
            ShellExecuteVerb = "open";
        else
            ShellExecuteVerb = "runas";

        HINSTANCE  nOpen = ShellExecute(NULL, ShellExecuteVerb, PythonConfigApp, NULL, NULL, SW_SHOWNORMAL);
        if ((int) nOpen <= SE_ERR_DLLNOTFOUND)
        {
            sprintf_s( szMessage, "Python tool executuioned\nInstall Python again");
            MessageBox(NULL, szMessage, "d-briDGe Setup", MB_OK );
            return FALSE;
        }
        // Restore DGPytb32.dll with the original version
        if (Is64BitWindows())
        {
            GetSystemWow64Directory(tempbuffer,MAX_PATH);
        }
        else
        {
            GetSystemDirectory(tempbuffer,MAX_PATH);
        }
        CString PythonDLLDestination = tempbuffer;
        PythonDLLDestination += "\\";
        PythonDLLDestination += PythonDLL;
        CopyFile(PythonDLL, PythonDLLDestination, FALSE);
        return FALSE;   // We're done here - bail out
    }
    if (!cmdLine.m_strFileName.CompareNoCase(_T("UseDPA5Python")))
    {
        char szMessage[MAX_PATH];
        char tempbuffer[MAX_PATH];
        CString strLogFilepath;

        // Prompt the user and confirm they want to use the original Python tool.
        sprintf_s( szMessage, "This will enable the use the original Python tool instead of DPA 5");
        if( IDCANCEL == MessageBox(NULL, szMessage, "DPA 5 Setup", MB_OKCANCEL ) )
            return FALSE;

        // If C:\Python1B2534\Python1BConfig.exe and DGPytb32.dll exist, proceed to execute it
        CString PythonConfigApp = "C:\\Python1B2534\\Python1BConfig.exe";
        CString PythonDLL = "DGPytb32.dll";
        if( (_access( PythonConfigApp, 0 )) == -1 )
        {
            sprintf_s( szMessage, "Python tool not previously installed\nInstall it if you wish to use it");
            MessageBox(NULL, szMessage, "Python Setup", MB_OK );
            return FALSE;
        }
        if( (_access( PythonDLL, 0 )) == -1 )
        {
            sprintf_s( szMessage, "Python DLL missing\nInstall DPA 5 again");
            MessageBox(NULL, szMessage, "Python Setup", MB_OK );
            return FALSE;
        }
        // Prompt the user with instruction and confirm they want to continue.
        sprintf_s( szMessage, "On the next screen:\n  + Select 'USB' for the Python interface\n  + Select 'AutoDetect'\nAfter you see 'Successfully detected Python.', select 'OK'");
        if( IDCANCEL == MessageBox(NULL, szMessage, "Python Setup", MB_OKCANCEL ) )
            return FALSE;

        OSVERSIONINFO osvi;
        CString ShellExecuteVerb;
        osvi.dwOSVersionInfoSize = sizeof(osvi);
        GetVersionEx(&osvi);
        if( osvi.dwMajorVersion < 6 )
            ShellExecuteVerb = "open";
        else
            ShellExecuteVerb = "runas";

        HINSTANCE  nOpen = ShellExecute(NULL, ShellExecuteVerb, PythonConfigApp, NULL, NULL, SW_SHOWNORMAL);
        if ((int) nOpen <= SE_ERR_DLLNOTFOUND)
        {
            sprintf_s( szMessage, "Python tool executuioned\nInstall Python again");
            MessageBox(NULL, szMessage, "DPA 5 Setup", MB_OK );
            return FALSE;
        }
        // Restore DGPytb32.dll with the original version
        if (Is64BitWindows())
        {
            GetSystemWow64Directory(tempbuffer,MAX_PATH);
        }
        else
        {
            GetSystemDirectory(tempbuffer,MAX_PATH);
        }
        CString PythonDLLDestination = tempbuffer;
        PythonDLLDestination += "\\";
        PythonDLLDestination += PythonDLL;
        CopyFile(PythonDLL, PythonDLLDestination, FALSE);
        return FALSE;   // We're done here - bail out
    }
#endif
    CCaymanConfigDlg dlg;
    m_pMainWnd = &dlg;
    int nResponse = dlg.DoModal();
    if (nResponse == IDOK)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with Cancel
    }

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}

CString GetIniString();

#if defined (J2534_0404) || defined (J2534_0500)
BOOL CCaymanConfigApp::WriteRegistry(char *strDeviceID)
{
    long            lSuccess;
    DWORD           dwDword;
    HKEY            hMainkey, hTempkey;
    char            strVersion[80];
    char            charMaxLogSize[MAX_PATH] = {'\0'};
    char            charPresentPath[MAX_PATH];
    char            charLogFilepath[MAX_PATH];
    char            charDataLogFilename[MAX_PATH];
    char tempbuffer[MAX_PATH];
    HINSTANCE  hDLL;
    typedef long (CALLBACK* GTVERSTR)(char *, int);
    GTVERSTR    GetVersionStr;

#ifdef J2534_DEVICE_KRHTWIRE
    CString m_strInternetAccessINI = GetIniString();
    GetPrivateProfileString("DefaultDirectory","PresentPath","",tempbuffer,MAX_PATH,m_strInternetAccessINI);
    strcat_s(tempbuffer, FUNCLIB);
#else
    if (Is64BitWindows())
    {
        GetSystemWow64Directory(tempbuffer,MAX_PATH);
    }
    else
    {
        GetSystemDirectory(tempbuffer,MAX_PATH);
    }
    strcpy_s(tempbuffer, FUNCLIB+1);
#endif
//    strcat_s(tempbuffer, FUNCLIB);

    if ((hDLL = LoadLibrary(tempbuffer)) == NULL)
        {
            ::MessageBox(NULL,"Driver not installed properly","Error",MB_OK);
            return FALSE;
        }
    if ((GetVersionStr = (GTVERSTR)GetProcAddress(hDLL, "GetVersionStr")) == NULL)
        {
            ::MessageBox(NULL,"Driver not installed properly","Error",MB_OK);
            FreeLibrary(hDLL);
            return FALSE;
        }
    int error = GetVersionStr(strVersion, sizeof(strVersion) - 1);
    FreeLibrary(hDLL);
    if (error != ERROR_SUCCESS)
        {
            ::MessageBox(NULL,"strVersion too small","Error",MB_OK);
            return FALSE;
        }
    lSuccess = RegCreateKey(HKEY_LOCAL_MACHINE,J2534REGISTRY_KEY_PATH,&hMainkey);   // "Software\PassThruSupport.04.04"

    if(lSuccess == ERROR_SUCCESS)
    {
        lSuccess = RegCreateKey(hMainkey,J2534REGISTRY_DEARBORN_GROUP,&hTempkey);
        if(lSuccess == ERROR_SUCCESS)
        {   
            int n=strlen("04.04")+1;
            RegSetValueEx(hTempkey,J2534REGISTRY_APIVERSION,0,REG_SZ,(BYTE*)"04.04",n);
            
            CString m_strInternetAccessINI = GetIniString();
            GetPrivateProfileString("DefaultDirectory","PresentPath","",charPresentPath,MAX_PATH,m_strInternetAccessINI);
            GetPrivateProfileString("Logging","PresentPath","",charLogFilepath,MAX_PATH,m_strInternetAccessINI);
            if (strlen(charLogFilepath) == 0)
            {
                strcpy_s(charLogFilepath, charPresentPath);
            }
            GetPrivateProfileString("Logging","MaxKBSize","",charMaxLogSize,MAX_PATH,m_strInternetAccessINI);
            if (strlen(charMaxLogSize)) // MaxKBSize implies logging is on all the time
            {
                dwDword = atol(charMaxLogSize);
                RegSetValueEx(hTempkey,J2534REGISTRY_LOGGING_MAXSIZE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            }
            n=strlen(charLogFilepath)+1;
            RegSetValueEx(hTempkey,J2534REGISTRY_LOGGINGDIRECTORY,0,REG_SZ,(BYTE*)charLogFilepath,n);
            GetPrivateProfileString("DataRecorder","PresentPath","",charDataLogFilename,MAX_PATH,m_strInternetAccessINI);
            if (strlen(charDataLogFilename) == 0)
            {
                strcpy_s(charDataLogFilename, charPresentPath);
            }
#ifdef J2534_DEVICE_KRHTWIRE
            if (Is64BitWindows())
            {
                GetSystemWow64Directory(charPresentPath, MAX_PATH);
            }
            else
            {
                GetSystemDirectory(charPresentPath, MAX_PATH);
            }
#endif

            CString strPresentPath = charPresentPath + CString("\\") + CONFIGAPP;
            n=strlen(strPresentPath)+1;
            strcpy_s(tempbuffer, strPresentPath);
            RegSetValueEx(hTempkey,J2534REGISTRY_CONFIGAPP,0,REG_SZ,(BYTE*)tempbuffer,n);
#ifdef J2534_DEVICE_KRHTWIRE
//            RegSetValueEx(hTempkey,J2534REGISTRY_CONFIGAPP,0,REG_SZ,(BYTE*)"",1);
            GetPrivateProfileString("DefaultDirectory","PresentPath","",tempbuffer,MAX_PATH,m_strInternetAccessINI);
#else
            if (Is64BitWindows())
            {
                GetSystemWow64Directory(tempbuffer,MAX_PATH);
            }
            else
            {
                GetSystemDirectory(tempbuffer,MAX_PATH);
            }
#endif
            strcat_s(tempbuffer, FUNCLIB);
            
            // Device ID passed in as 1st parm
            dwDword = atol(strDeviceID);
#if defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
            // This 'fudges' DeviceId so that no INI file is needed. If these devices have other connection types besides USB, we need to
            // changes this to allow user to select it from drop down list. Till then, get value as defined at compile time
            dwDword = DEVICE_TYPE;
#endif
            RegSetValueEx(hTempkey,J2534REGISTRY_DEVICE_ID,0,REG_DWORD,(unsigned char*)&dwDword,4);

            n=strlen(tempbuffer)+1;
            //RegSetValueEx(hTempkey,J2534REGISTRY_FUNCLIB,0,REG_SZ,(BYTE*)"C:\\WINDOWS\\system32\\Cayman32.dll",n);
            RegSetValueEx(hTempkey,J2534REGISTRY_FUNCLIB,0,REG_SZ,(BYTE*) tempbuffer,n);

#ifdef J2534_DEVICE_CAYMAN
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850PWM,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_GM_UART_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x2;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif          
#ifdef J2534_DEVICE_VSI
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            //RegSetValueEx(hTempkey,J2534REGISTRY_J1939,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850PWM,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_GM_UART_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x2;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif          
#ifdef J2534_DEVICE_KRHTWIRE
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850PWM,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_A_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_ENGINE,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SCI_B_TRANS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);  
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_GM_UART_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CCD,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x2;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif
#ifdef J2534_DEVICE_DPA4
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1708,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif  
#ifdef J2534_DEVICE_DPA5
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1708,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1708_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4); 
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x4;
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif 
#ifdef J2534_DEVICE_NETBRIDGE
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_FT_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_FT_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x3;
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif  

#if defined(J2534_DEVICE_SOFTBRIDGE) || defined(J2534_DEVICE_WURTH) || defined(J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE)
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO14230,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH2,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO9141,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4); 
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_CAN_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_SW_CAN_ISO15765_CH1,0,REG_DWORD,(unsigned char*)&dwDword,4);
            dwDword=0x2;
#if defined(J2534_DEVICE_DBRIDGE)
#if !defined (J2534_DEVICE_SVCI)
            dwDword=0x3;
#endif
#endif
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_PS,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif

#if defined(J2534_DEVICE_DBRIDGE)
            dwDword=0x1;
            RegSetValueEx(hTempkey,J2534REGISTRY_J1850VPW,0,REG_DWORD,(unsigned char*)&dwDword,4);
#if !defined (J2534_DEVICE_SVCI)
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_CAN_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_J1939_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH3,0,REG_DWORD,(unsigned char*)&dwDword,4);
            RegSetValueEx(hTempkey,J2534REGISTRY_ISO15765_CH4,0,REG_DWORD,(unsigned char*)&dwDword,4);
#endif
#endif

//#if defined (J2534_DEVICE_KRHTWIRE) && defined (NDEBUG)
//            dwDword=0x00;                   // Logging turned off
//#else
            if (strlen(charMaxLogSize) == 0) // MaxKBSize implies logging is on all the time
            {
                dwDword=0x00;               // Logging turned off
            }
            else
            {
                dwDword=0x80000007;         // All logging turned on and log file appended to
            }
//#endif
            RegSetValueEx(hTempkey,J2534REGISTRY_LOGGING,0,REG_DWORD,(unsigned char*)&dwDword,4);
            n= strlen(REGISTRY_NAME)+1;
            RegSetValueEx(hTempkey,J2534REGISTRY_NAME,0,REG_SZ,(BYTE*) REGISTRY_NAME,n);            

            // Product Version retrieved from DLL
            n= strlen(strVersion)+1;
            RegSetValueEx(hTempkey,J2534REGISTRY_PRODUCTVERSION,0,REG_SZ, (BYTE*) strVersion,n);
            
            n= strlen(PROTOCOLSSUPPORTED)+1;
//            RegSetValueEx(hTempkey,J2534REGISTRY_PROTOCOLSSUPPORT,0,REG_SZ,(BYTE*)PROTOCOLSSUPPORTED,n);
            
            n= strlen(VENDOR)+1;
            RegSetValueEx(hTempkey,J2534REGISTRY_VENDOR,0,REG_SZ,(BYTE*) VENDOR,n);
            RegCloseKey(hTempkey);
        }
        RegCloseKey(hMainkey);
        //RegCloseKey(hTempkey);
        
    }
    return TRUE;
}
#endif

#ifdef J2534_0305
BOOL CCaymanConfigApp::WriteRegistry(char *strParm)
{
    long lSuccess;
    char tempstr[1024], tempstr2[1024], strDevice[100];
    DWORD dwSzdata;
    unsigned char ucValueread[100]; // read in String value
    DWORD dwDword = REG_DWORD, dwString = REG_SZ;
    char chValueset[100];           // used to set String values
    HKEY mainkey;
    int i=0, highdevice=1, highvendor=1, x;
    DWORD datasize, dworddata;
    HKEY            tempkey, tempkey2;
    
    if (strlen(strParm))                        // Not fully implemented - CreateDefaults still uses the #defined REGISTRY_NAME                    
        strcpy_s(strDevice, strParm);           // Device Name passed in as parm2

    else
        strcpy_s(strDevice, REGISTRY_NAME);     // Use Device Name as defined at top

    UninstallRegistry();                        // Cleanup from previous entry
    strcpy_s(tempstr, J2534REGISTRY_KEY_PATH);  // "Software\PassThruSupport"
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0, KEY_ALL_ACCESS, &mainkey);

    if(lSuccess==ERROR_SUCCESS)
    {
        i=0;
        dwSzdata=1024;
        lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
        if(lSuccess!=ERROR_SUCCESS)
        {
            sprintf_s(tempstr, "Vendor%d", highvendor);
            RegCreateKey(mainkey, tempstr, &tempkey2);
            strcpy_s(tempstr, VENDOR);
            datasize=strlen(tempstr)+1;
            RegSetValueEx(tempkey2, J2534REGISTRY_NAME, 0, REG_SZ, (unsigned char*)tempstr, datasize);

            dworddata=10;
            datasize=4;
            RegSetValueEx(tempkey2, "Id", 0, REG_DWORD, (unsigned char*)&dworddata, datasize);
            sprintf_s(tempstr, "Devices\\Device1", highvendor);
            RegCreateKey(tempkey2, tempstr, &tempkey);
            CreateDefaults(&tempkey);   // Defaults to those of d-briDGe
            RegCloseKey(tempkey2);
            RegCloseKey(tempkey);
            RegCloseKey(mainkey);
            return TRUE;
        }
        x=atol(tempstr+strlen("Vendor"));
        if(x==highvendor)
            highvendor++;
        lSuccess = RegOpenKeyEx(mainkey, tempstr, 0, KEY_ALL_ACCESS, &tempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwSzdata = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwSzdata);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset, VENDOR))
                {
                    break;
                }
            }

            i++;
            lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                sprintf_s(tempstr, "Vendor%d", highvendor);
                RegCreateKey(mainkey, tempstr, &tempkey2);
                strcpy_s(tempstr, VENDOR);
                datasize=strlen(tempstr)+1;
                RegSetValueEx(tempkey2, J2534REGISTRY_NAME, 0, REG_SZ, (unsigned char*)tempstr, datasize);

                dworddata=10;
                datasize=4;
                RegSetValueEx(tempkey2, "Id", 0, REG_DWORD, (unsigned char*)&dworddata, datasize);
                sprintf_s(tempstr, "Devices\\Device1", highvendor);
                RegCreateKey(tempkey2, tempstr, &tempkey);
                CreateDefaults(&tempkey);
                RegCloseKey(tempkey);
                RegCloseKey(tempkey2);
                break;
            }
            x=atol(tempstr+strlen("Vendor"));
            if(x==highvendor)
                highvendor++;

            lSuccess = RegOpenKeyEx(mainkey, tempstr, 0, KEY_ALL_ACCESS, &tempkey);
        }

        if(lSuccess==ERROR_SUCCESS)
        {
            i=0;
            dwSzdata=1024;
            lSuccess = RegOpenKeyEx(tempkey, "Devices", 0, KEY_ALL_ACCESS, &tempkey2);

            lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                sprintf_s(tempstr, "Device%d", highdevice);
                RegCreateKey(tempkey2, tempstr, &tempkey);
                CreateDefaults(&tempkey);
                RegCloseKey(tempkey2);
                RegCloseKey(tempkey);
                RegCloseKey(mainkey);
                return TRUE;
            }
            x=atol(tempstr2+strlen("Device"));
            if(x==highdevice)
                highdevice++;
            lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                0, KEY_ALL_ACCESS, &tempkey);

            while(lSuccess==ERROR_SUCCESS)
            {
                dwSzdata = sizeof(ucValueread);
                lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwSzdata);

                if(lSuccess==ERROR_SUCCESS)
                {
                    sprintf_s(chValueset, "%s", ucValueread);
                    if(!strcmp(chValueset, strDevice))
                    {
                        CreateDefaults(&tempkey);
                        RegCloseKey(tempkey2);
                        RegCloseKey(tempkey);
                        RegCloseKey(mainkey);
                        break;
                    }
                }

                i++;
                dwSzdata=1024;
                lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
                if(lSuccess!=ERROR_SUCCESS)
                {
                    sprintf_s(tempstr, "Device%d", highdevice);
                    RegCreateKey(tempkey2, tempstr, &tempkey);
                    CreateDefaults(&tempkey);
                    RegCloseKey(tempkey2);
                    RegCloseKey(tempkey);
                    RegCloseKey(mainkey);
                    break;
                }

                x=atol(tempstr2+strlen("Device"));
                if(x==highdevice)
                    highdevice++;
                lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 0, KEY_ALL_ACCESS, &tempkey);
            }
        }
        else
            RegCloseKey(mainkey);
    }
    else
    {
        strcpy_s(tempstr, "Software\\PassThruSupport\\Vendor1");
        lSuccess = RegCreateKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0,0,0, KEY_ALL_ACCESS, 0, &tempkey2, 0);

        if (lSuccess != ERROR_SUCCESS)
        {
//          MessageBox("Cannot configure because of insufficent access privilege or unknown error.", "ERROR:", MB_OK);
            return FALSE;
        }

        strcpy_s(tempstr, VENDOR);
        datasize=strlen(tempstr)+1;
        RegSetValueEx(tempkey2, J2534REGISTRY_NAME, 0, REG_SZ, (unsigned char*)tempstr, datasize);

        dworddata=10;
        datasize=4;
        RegSetValueEx(tempkey2, "Id", 0, REG_DWORD, (unsigned char*)&dworddata, datasize);
        sprintf_s(tempstr, "Devices\\Device1", highvendor);
        RegCreateKey(tempkey2, tempstr, &tempkey);
        CreateDefaults(&tempkey);
        RegCloseKey(tempkey2);
        RegCloseKey(tempkey);
    }

    return TRUE;// &tempkey;
}

void CCaymanConfigApp::CreateDefaults(HKEY *thiskey)
{
    DWORD dwDword;
    CString cszString;
    char charMaxLogSize[MAX_PATH] = {'\0'};
    char charPresentPath[MAX_PATH];
    char charLogFilepath[MAX_PATH];
    char tempbuffer[MAX_PATH]; 
    char            strVersion[80];
    HINSTANCE  hDLL;
    typedef long (CALLBACK* GTVERSTR)(char *, int);
    GTVERSTR    GetVersionStr;

    CString m_strInternetAccessINI = GetIniString();
    GetPrivateProfileString("DefaultDirectory","PresentPath","",charPresentPath,MAX_PATH,m_strInternetAccessINI);
    GetPrivateProfileString("Logging","PresentPath","",charLogFilepath,MAX_PATH,m_strInternetAccessINI);
    if (strlen(charLogFilepath) == 0)
    {
        strcpy_s(charLogFilepath, charPresentPath);
    }
    else
    {
        GetPrivateProfileString("Logging","MaxKBSize","",charMaxLogSize,MAX_PATH,m_strInternetAccessINI);
        if (strlen(charMaxLogSize)) // MaxKBSize implies logging is on all the time
        {
            dwDword = atol(charMaxLogSize);
            RegSetValueEx(*thiskey,J2534REGISTRY_LOGGING_MAXSIZE,0,REG_DWORD,(unsigned char*)&dwDword,4);
        }
    }
    int n = strlen(charLogFilepath)+1;
    RegSetValueEx(*thiskey,J2534REGISTRY_LOGGINGDIRECTORY,0,REG_SZ,(BYTE*)charLogFilepath,n);

    CString strPresentPath = charPresentPath + CString("\\") + CONFIGAPP;
    n=strlen(strPresentPath)+1;
    strcpy_s(tempbuffer, strPresentPath);
    RegSetValueEx(*thiskey,J2534REGISTRY_CONFIGAPP,0,REG_SZ,(BYTE*)tempbuffer,n);

    dwDword=DBRIDGE_DEVICEID;
#ifdef J2534_DEVICE_DPA50305
    dwDword=DPA5_DEVICEID;
#endif
    n=4;
    RegSetValueEx(*thiskey, J2534REGISTRY_DEVICE_ID, 0, REG_DWORD, (unsigned char*)&dwDword, n);

    //strcpy_s(tempstr, strLogFilepath);
    //n=strlen(tempstr)+1;
    //RegSetValueEx(*thiskey, J2534REGISTRY_LOGGINGDIRECTORY, 0, REG_SZ, (unsigned char*)tempstr, n);
    strcpy_s(tempbuffer, REGISTRY_NAME);
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_NAME, 0, REG_SZ, (unsigned char*)tempbuffer, n);
    strcpy_s(tempbuffer, "CAN, ISO9141, ISO14230, ISO15765");
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_PROTOCOLSSUPPORT, 0, REG_SZ, (unsigned char*)tempbuffer, n);
    strcpy_s(tempbuffer, "03.05");
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_APIVERSION, 0, REG_SZ, (unsigned char*)tempbuffer, n);
    strcpy_s(strVersion, "1.00");
    n=strlen(strVersion)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_PRODUCTVERSION, 0, REG_SZ, (unsigned char*)strVersion, n);
    n= strlen(VENDOR)+1;
    RegSetValueEx(*thiskey,J2534REGISTRY_VENDOR,0,REG_SZ,(BYTE*) VENDOR,n);
#ifdef UD_TRUCK
    strcpy_s(tempbuffer, "COM1");
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_DEVICE_DPA_COMMPORT, 0, REG_SZ, (unsigned char*)tempbuffer, n);
    strcpy_s(tempbuffer, "RS232");
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_DEVICE_DPA_PYTHON_TYPE, 0, REG_SZ, (unsigned char*)tempbuffer, n);
#endif
    dwDword=0;
    n=4;
#if !defined (J2534_DEVICE_KRHTWIRE) || defined (_DEBUG)
    RegSetValueEx(*thiskey, J2534REGISTRY_LOGGING, 0, REG_DWORD, (unsigned char*)&dwDword, n);
#endif
    if (Is64BitWindows())
    {
        GetSystemWow64Directory(tempbuffer,MAX_PATH);
    }
    else
    {
        GetSystemDirectory(tempbuffer,MAX_PATH);
    }
    strcat_s(tempbuffer, FUNCLIB);
    n=strlen(tempbuffer)+1;
    RegSetValueEx(*thiskey,J2534REGISTRY_FUNCLIB,0,REG_SZ,(BYTE*) tempbuffer,n);

    if ((hDLL = LoadLibrary(tempbuffer)) == NULL)
        {
            ::MessageBox(NULL,"Driver not installed properly","Error",MB_OK);
            return ;
        }
    if ((GetVersionStr = (GTVERSTR)GetProcAddress(hDLL, "GetVersionStr")) == NULL)
        {
            ::MessageBox(NULL,"Driver not installed properly","Error",MB_OK);
            FreeLibrary(hDLL);
            return ;
        }
    int error = GetVersionStr(strVersion, sizeof(strVersion) - 1);
    FreeLibrary(hDLL);
    if (error != ERROR_SUCCESS)
        {
            ::MessageBox(NULL,"strVersion too small","Error",MB_OK);
            return ;
        }
    n=strlen(strVersion)+1;
    RegSetValueEx(*thiskey, J2534REGISTRY_PRODUCTVERSION, 0, REG_SZ, (unsigned char*)strVersion, n);
}
#endif

void CCaymanConfigApp::UninstallRegistry()
{
    long lSuccess;
    char tempstr[1024], tempstr2[1024];
    DWORD dwSzdata;
    unsigned char ucValueread[100]; // read in String value
    DWORD dwDword = REG_DWORD, dwString = REG_SZ;
    char chValueset[100];           // used to set String values
    HKEY mainkey;
    int i=0, j=0, highdevice=1, highvendor=1, x;
//  DWORD datasize, dworddata;
    HKEY            tempkey, tempkey2, tempkey1;

    strcpy_s(tempstr, J2534REGISTRY_KEY_PATH);  // "Software\PassThruSupport"
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0, KEY_ALL_ACCESS, &mainkey);

    if(lSuccess==ERROR_SUCCESS)
    {
        i=0;
        dwSzdata=1024;
        lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
        x=atol(tempstr+strlen("Vendor"));
        if(x==highvendor)
            highvendor++;
        lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                0, KEY_ALL_ACCESS, &tempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwSzdata = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, 
                ucValueread, &dwSzdata);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset,VENDOR))
                    break;
            }

            i++;
            lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                sprintf_s(tempstr, "Vendor%d", highvendor);
                sprintf_s(tempstr, "Devices\\Device1", highvendor);
                break;
            }
            x=atol(tempstr+strlen("Vendor"));
            if(x==highvendor)
                highvendor++;
            RegCloseKey(tempkey);
            lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                    0, KEY_ALL_ACCESS, &tempkey);
        }

        if(lSuccess==ERROR_SUCCESS)
        {
            i=0;
            dwSzdata=1024;
            lSuccess = RegOpenKeyEx(tempkey, "Devices", 
                    0, KEY_ALL_ACCESS, &tempkey2);

//          lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
//          if(lSuccess!=ERROR_SUCCESS)
//          {
//              sprintf_s(tempstr2, "Device%d", highdevice);
//              RegCreateKey(tempkey2, tempstr, &tempkey);
//          }
//          if(tempstr2[strlen(tempstr2)-1]-'0'==highdevice)
//              highdevice++;
//          lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
//              0, KEY_ALL_ACCESS, &tempkey);

//          while(lSuccess==ERROR_SUCCESS)
            while (j < CONFIGDLG_MAXDEVICEENTRY)
            {
                j++;
//              i++;
//              dwSzdata=1024;
//              lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
//              if(lSuccess==ERROR_SUCCESS)
//              {
                    sprintf_s(tempstr2, "Device%d", highdevice);
//              }
//              else
//                  break;
                x=atol(tempstr2+strlen("Device"));
                if(x==highdevice)
                    highdevice++;
                lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                    0, KEY_ALL_ACCESS, &tempkey1);
                if (lSuccess==ERROR_SUCCESS)
                {
                    i++;
                }

                dwSzdata = sizeof(ucValueread);
                lSuccess = RegQueryValueEx(tempkey1, J2534REGISTRY_NAME, NULL, &dwString, 
                    ucValueread, &dwSzdata);

                if(lSuccess==ERROR_SUCCESS)
                {
                    sprintf_s(chValueset, "%s", ucValueread);
                    if(!strcmp(chValueset, REGISTRY_NAME))
                    {
                        i--;
                        RegDeleteKey(tempkey2, tempstr2);
//                      break;
                    }
                    RegCloseKey(tempkey1);
                }
            }

            // If no other devices
            if (i == 0)
            {
                RegDeleteKey(tempkey, "Devices");
                RegDeleteKey(mainkey, tempstr);

                // See if PassThruSupport needs to be deleted
                j = 0;
                highvendor=1;
                while (j < CONFIGDLG_MAXVENDORENTRY)
                {
                    j++;
    //              i++;
    //              dwSzdata=1024;
    //              lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
    //              if(lSuccess==ERROR_SUCCESS)
    //              {
                        sprintf_s(tempstr, "Vendor%d", highvendor);
    //              }
    //              else
    //                  break;

                    x=atol(tempstr+strlen("Vendor"));
                    if(x==highvendor)
                        highvendor++;
                    lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                        0, KEY_ALL_ACCESS, &tempkey);
                    if (lSuccess==ERROR_SUCCESS)
                    {
                        i++;
                    }
                }

                // if no other vendors
                if (i == 0)
                {
                    strcpy_s(tempstr, J2534REGISTRY_KEY_PATH);  // "Software\PassThruSupport"
                    RegDeleteKey(HKEY_LOCAL_MACHINE, tempstr);
                }
            }
            RegCloseKey(tempkey2);
            RegCloseKey(tempkey);
            RegCloseKey(mainkey);
        }
        else
        {
            RegCloseKey(mainkey);
        }
    }
/*
    GetCurrentDirectory(1024, tempstr);
    strcat_s(tempstr, "\\*");
    DeleteFile(tempstr);
    GetCurrentDirectory(1024, tempstr);
    RemoveDirectory(tempstr);
*/
}