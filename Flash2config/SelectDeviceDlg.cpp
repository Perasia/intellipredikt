// SelectDeviceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CaymanConfig.h"
#include "SelectDeviceDlg.h"
#include "CaymanConfigDlg.h"

CString SelectDeviceGetIniString()
{
    char buffer[5000];
    GetWindowsDirectory(buffer, sizeof(buffer));
    CString strIni = (LPCTSTR)buffer + CString("\\");
    strIni += DEVICE_INI;
    return strIni;
}

void SelectDeviceGetDeviceName(CString strIniFile, int DeviceID, char *DeviceName, int MaxDeviceNameSize)
{
    char deviceSection[80];
    sprintf_s(deviceSection, "DeviceInformation%d", DeviceID);  
    GetPrivateProfileString(deviceSection, // points to section name
                                "DeviceName",   // points to key name
                                "", // points to default string
                                DeviceName, // points to destination buffer
                                MaxDeviceNameSize,  // size of destination buffer
                                strIniFile);

}

// SelectDeviceDlg dialog

IMPLEMENT_DYNAMIC(SelectDeviceDlg, CDialog)

SelectDeviceDlg::SelectDeviceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SelectDeviceDlg::IDD, pParent)
{
}

SelectDeviceDlg::~SelectDeviceDlg()
{
}

void SelectDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_SELECT_DEVICE, m_cmbSelectDevice);
}


BEGIN_MESSAGE_MAP(SelectDeviceDlg, CDialog)
    ON_BN_CLICKED(IDOK, &SelectDeviceDlg::OnBnClickedOk)
    ON_CBN_SELCHANGE(IDC_COMBO_SELECT_DEVICE, &SelectDeviceDlg::OnCbnSelchangeComboSelectDevice)
END_MESSAGE_MAP()


// SelectDeviceDlg message handlers

void SelectDeviceDlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    /*CString str,str1,str2,strdevicename;
    str = m_strDeviceName;
    str1 = "Config";
    strdevicename = str +str1;
    MessageBox("Please select Cayman Interface",strdevicename,MB_ICONWARNING);*/
#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) //|| defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    int i;
    i = m_cmbSelectDevice.GetCurSel();
    m_cmbSelectDevice.GetLBText(i, m_SelectedDeviceName);
#endif
//    pWnd = (CButton *)GetDlgItem(IDC_CHECKLOGGING);
//    i = pWnd->GetCheck();
    DWORD dwDword = REG_DWORD, dwString = REG_SZ; 

    char ucRead[1024];

    DWORD regdata = sizeof(ucRead);

    // Write to VSI Logging Registry
    // Obtain current values shown on dialog
    UpdateData(TRUE);

//  if (i == 1)
//      g_ulLogging = 7;
//  else
/*    if (i == 0)
        g_ulLogging = 0;

    sprintf_s(ucRead, "%lu", g_ulLogging);
    dwMask = 0x000000FF;
    for (i = 0; i < sizeof(g_ulLogging); i++)
    {
        ucRead[i] = (unsigned char) ((g_ulLogging & dwMask) >> (8 * i));
        dwMask = dwMask << 8;
    }                   */
//#if defined (J2534_DEVICE_KRHTWIRE) && defined (NDEBUG)
//    ucRead[0] = 0x00;               // Logging turned off
//#endif
/*    LONG lSuccess = RegSetValueEx(m_hkey, J2534REGISTRY_LOGGING, NULL, dwDword, (unsigned char *) ucRead, sizeof(g_ulLogging));
#ifdef J2534_DEVICE_DPA4
    unsigned long ulDeviceId;
    if (m_ctrlComboDPA.GetCurSel() == 0)
    {
        ulDeviceId = 150;
    }
//  else
//  {
//      ulDeviceId = 601;
//  }

    sprintf_s((char *)ucRead, "%lu", ulDeviceId);
    dwMask = 0x000000FF;
    for (i = 0; i < sizeof(ulDeviceId); i++)
    {
        ucRead[i] = (unsigned char) ((ulDeviceId & dwMask) >> (8 * i));
        dwMask = dwMask << 8;
    }
    lSuccess = RegSetValueEx(m_hkey, J2534REGISTRY_DEVICE_ID, NULL, dwDword, 
        ucRead, sizeof(ulDeviceId));
#endif     
#if defined (J2534_DEVICE_SOFTBRIDGE) || defined (J2534_DEVICE_WURTH) || defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    // This 'fudges' DeviceId so that no INI file is needed. If these devices have other connection types besides USB, we need to
    // changes this to allow user to select it from drop down list. Till then, get value as defined at compile time
    m_dwDeviceId = DEVICE_TYPE;
#endif    */
#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) //|| defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    // Write Device-Connection Type to Registry
    RegSetValueEx(m_hkey, J2534REGISTRY_DEVICE_ID, 0, REG_DWORD, (unsigned char*)&m_dwDeviceId, sizeof(m_dwDeviceId));
#endif
    // Show any changes on the dialog
    UpdateData(FALSE);

    OnOK();
}

BOOL SelectDeviceDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    // TODO:  Add extra initialization here
    m_cmbSelectDevice.ResetContent();
    // scan INI for all DeviceName entries and add to Combo
    InitDpaComboBoxText();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) || defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
int SelectDeviceGetDeviceId(const char *SelectedDeviceName)
{
    int MaxDevId = 300;
    int DevID;
    CString strIniFile = SelectDeviceGetIniString();
    for (DevID = 1; DevID < MaxDevId; DevID++)
    {
        char DeviceName[80];
        SelectDeviceGetDeviceName(strIniFile, DevID, DeviceName, sizeof(DeviceName) - 1);
        if (strlen(DeviceName) == 0)
        {
            continue; // We are done here - get out
        }
        if (strcmp(DeviceName, SelectedDeviceName) == 0)
            break;

    }
    return DevID < MaxDevId ? DevID : 1;
}
#endif

// scan INI for all DeviceName entries and add to Combo
// Read registry for a type previously selected
void SelectDeviceDlg::InitDpaComboBoxText()
{
    char DeviceName[80];
    CString strIniFile = SelectDeviceGetIniString();
    int SelectedDevice = 0;

    // m_dwDeviceId is set to what is found in registry, defaults to 0
    int i, DeviceID = m_dwDeviceId ?  m_dwDeviceId : 1;
    SelectDeviceGetDeviceName(strIniFile, DeviceID, m_SelectedDeviceName, sizeof(m_SelectedDeviceName) - 1);

    for (i = 0, DeviceID = 1; DeviceID < 300; DeviceID++)
    {
        SelectDeviceGetDeviceName(strIniFile, DeviceID, DeviceName, sizeof(DeviceName) - 1);
        if (strlen(DeviceName) == 0)
        {
            continue; // No device at this DeviceID, keep looking
        }
        if (strcmp(DeviceName, m_SelectedDeviceName) == 0)
            SelectedDevice = i; // Remember this

            
        m_cmbSelectDevice.AddString(DeviceName); // Add each Name found in a DeviceInformationX section
        i++;
    }
    // m_dwDeviceId is set to what is found in registry, defaults to 0
    m_cmbSelectDevice.SetCurSel(SelectedDevice);
}

void SelectDeviceDlg::OnCbnSelchangeComboSelectDevice()
{
    // TODO: Add your control notification handler code here
#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) || defined (J2534_DEVICE_SOFTBRIDGE) || defined (J2534_DEVICE_WURTH) || defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    m_dwDeviceId = m_cmbSelectDevice.GetCurSel() + 1;
    int Sel = m_cmbSelectDevice.GetCurSel();
    m_cmbSelectDevice.GetLBText(Sel, m_SelectedDeviceName);
    m_dwDeviceId = SelectDeviceGetDeviceId(m_SelectedDeviceName);
#endif
}
