//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CaymanConfig.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CAYMANCONFIG_DIALOG         102
#define IDS_STRING102                   102
#define IDS_STRING103                   103
#define IDS_STRING104                   104
#define IDS_STRING105                   105
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_CONNECT              130
#define IDD_DIALOG1                     131
#define IDR_ICON1                       132
#define IDD_LOG_CONFIG                  134
#define IDD_DIALOG_SELECT_DEVICE        135
#define IDC_ST_VENDOR                   1000
#define IDC_ST_NAME                     1001
#define IDC_ST_PROT_SUPP                1002
#define IDC_ST_CONF_APP                 1003
#define IDC_ST_FUNC_LIB                 1004
#define IDC_ST_API_VER                  1005
#define IDC_ST_COM_PORT                 1006
#define IDC_ST_DPA_TYPE                 1006
#define IDC_ST_BAUD_RATE                1007
#define IDC_ST_PYTHON_INTER             1008
#define IDC_EDT_VENDOR                  1009
#define IDC_EDT_NAME                    1010
#define IDC_EDT_PROT_SUPP               1011
#define IDC_EDT_CONG_APP                1012
#define IDC_EDT_FUNC_LIB                1013
#define IDC_EDT_API_VER                 1014
#define IDC_COM_PORT                    1015
#define IDC_COMBO_PYT_INTR              1016
#define IDC_COMBO_BAUD_RATE             1017
#define IDC_ST_PROD_VER2                1018
#define IDC_BTN_AUTO_DET                1019
#define IDC_BTN_OK                      1020
#define IDC_BTN_CANCEL                  1021
#define IDC_EDIT                        1021
#define IDC_EDT_PROD_VER                1022
#define IDC_COMBO_J2534DEVICES          1023
#define IDC_BTN_CANCEL2                 1023
#define IDC_EDIT1                       1024
#define IDC_BTN_OK2                     1024
#define IDC_CHECKLOGGING                1025
#define IDC_BTN_LOG_CONFIGURATION       1026
#define IDC_BTN_SELECT_DEVICE           1027
#define IDC_STATIC_LOG                  1040
#define IDC_CHECK1                      1041
#define IDC_CHECK2                      1042
#define IDC_CHECK3                      1043
#define IDC_RADIO1                      1044
#define IDC_RADIO2                      1045
#define IDC_COMBO_DPA                   1046
#define IDC_STATIC_LOGGING_TYPE         1047
#define IDC_STATIC_LOGGING_METHOD       1048
#define IDC_COMBO_SELECT_DEVICE         1049
#define IDC_EDIT2                       1050
#define IDC_EDIT_SELECTED_DEVICE        1050

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
