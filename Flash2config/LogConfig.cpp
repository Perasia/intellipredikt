// LogConfig.cpp : implementation file
//

#include "stdafx.h"
#include "caymanconfig.h"
#include "LogConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogConfig dialog


CLogConfig::CLogConfig(CWnd* pParent /*=NULL*/)
    : CDialog(CLogConfig::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLogConfig)
    //}}AFX_DATA_INIT
}


void CLogConfig::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLogConfig)
    DDX_Control(pDX, IDC_STATIC_LOGGING_METHOD, m_clLoggingMethod);
    DDX_Control(pDX, IDC_STATIC_LOGGING_TYPE, m_clLoggingType);
    DDX_Control(pDX, IDCANCEL, m_clCancel);
    DDX_Control(pDX, IDOK, m_clOK);
    DDX_Control(pDX, IDC_CHECK3, m_clData);
    DDX_Control(pDX, IDC_CHECK2, m_clTrace);
    DDX_Control(pDX, IDC_CHECK1, m_clError);
    DDX_Control(pDX, IDC_RADIO2, m_clOverwrite);
    DDX_Control(pDX, IDC_RADIO1, m_clAppend);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogConfig, CDialog)
    //{{AFX_MSG_MAP(CLogConfig)
    ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
    ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogConfig message handlers

void CLogConfig::OnOK() 
{
    // TODO: Add extra validation here
    m_ulLogging = 0;
    m_ulLogging |= (1 * m_clError.GetCheck());
    m_ulLogging |= (2 * m_clTrace.GetCheck());
    m_ulLogging |= (4 * m_clData.GetCheck());
    m_ulLogging |= (0x80000000 * m_clAppend.GetCheck());
    
    CDialog::OnOK();
}

BOOL CLogConfig::OnInitDialog() 
{
    CDialog::OnInitDialog();
    
    // TODO: Add extra initialization here
    if (m_ulLogging & 0x80000000)
    {
        m_clAppend.SetCheck(1);
        m_clOverwrite.SetCheck(0);  
    }
    else
    {
        m_clAppend.SetCheck(0);
        m_clOverwrite.SetCheck(1);  
    }
    if (m_ulLogging & 4)
    {
        m_clData.SetCheck(1);
    }
    else
    {
        m_clData.SetCheck(0);
    }
    if (m_ulLogging & 2)
    {
        m_clTrace.SetCheck(1);
    }
    else
    {
        m_clTrace.SetCheck(0);
    }
    if (m_ulLogging & 1)
    {
        m_clError.SetCheck(1);
    }
    else
    {
        m_clError.SetCheck(0);
    }

    if (iLanguage==1)
    {
        m_clOK.SetWindowText("OK");
        m_clCancel.SetWindowText("Cancel");
        m_clError.SetWindowText("Error");
        m_clTrace.SetWindowText("Trace");
        m_clData.SetWindowText("Data");
        m_clAppend.SetWindowText("Append Logs");
        m_clOverwrite.SetWindowText("Overwrite Logs");
        SetWindowText("Logging Configuration");
        m_clLoggingMethod.SetWindowText("Logging Method");
        m_clLoggingType.SetWindowText("Logging Type");
    }
    if (iLanguage==2)
    {
        m_clOK.SetWindowText("OK");
        m_clCancel.SetWindowText("Annuler");
        m_clError.SetWindowText("Erreur");
        m_clTrace.SetWindowText("Trace");
        m_clData.SetWindowText("Donn�es");
        m_clAppend.SetWindowText("Ajouter les journaux");
        m_clOverwrite.SetWindowText("Remplacer les journaux");
        SetWindowText("Configuration de la journalisation");
        m_clLoggingMethod.SetWindowText("Mode de journalisation");
        m_clLoggingType.SetWindowText("Type de journalisation");
    }
    if (iLanguage==3)
    {
        m_clOK.SetWindowText("OK");
        m_clCancel.SetWindowText("Abbrechen");
        m_clError.SetWindowText("Fehler");
        m_clTrace.SetWindowText("Fehlerprotokollierung");
        m_clData.SetWindowText("Daten");
        m_clAppend.SetWindowText("Protokolle anh�ngen");
        m_clOverwrite.SetWindowText("Protokolle �berschreiben");
        SetWindowText("Protokollierungskonfiguration");
        m_clLoggingMethod.SetWindowText("Protokollierungsmethode");
        m_clLoggingType.SetWindowText("Protokollierungstyp");
    }
    if (iLanguage==4)
    {
        m_clOK.SetWindowText("Aceptar");
        m_clCancel.SetWindowText("Cancelar");
        m_clError.SetWindowText("Error");
        m_clTrace.SetWindowText("Rastreo");
        m_clData.SetWindowText("Datos");
        m_clAppend.SetWindowText("Adjuntar registros");
        m_clOverwrite.SetWindowText("Sobrescribir registros");
        SetWindowText("Configuraci�n de registro");
        m_clLoggingMethod.SetWindowText("M�todo de registro");
        m_clLoggingType.SetWindowText("Tipo de registro");
    }
    if (iLanguage==5)
    {
        m_clOK.SetWindowText("OK");
        m_clCancel.SetWindowText("Annulla");
        m_clError.SetWindowText("Errore");
        m_clTrace.SetWindowText("Traccia");
        m_clData.SetWindowText("Dati");
        m_clAppend.SetWindowText("Aggiungi Registri");
        m_clOverwrite.SetWindowText("Sovrascrivi Registri");
        SetWindowText("Configurazione della Registrazione");
        m_clLoggingMethod.SetWindowText("Metodo di Registrazione");
        m_clLoggingType.SetWindowText("Tipologia di Registrazione");
    }
    UpdateData(FALSE);
    
    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

void CLogConfig::OnRadio1() 
{
    // TODO: Add your control notification handler code here
//  m_bAppendLog = true;
//  m_bOverwriteLog = false;
    m_clAppend.SetCheck(1);
    m_clOverwrite.SetCheck(0);
}

void CLogConfig::OnRadio2() 
{
    // TODO: Add your control notification handler code here
//  m_bAppendLog = false;
//  m_bOverwriteLog = true;
    m_clAppend.SetCheck(0);
    m_clOverwrite.SetCheck(1);  
}
