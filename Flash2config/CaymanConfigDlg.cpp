// CaymanConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CaymanConfig.h"
#include "CaymanConfigDlg.h"
#include "Connect.h"
#include "SelectDeviceDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

// Dialog Data
    //{{AFX_DATA(CAboutDlg)
    enum { IDD = IDD_ABOUTBOX };
    //}}AFX_DATA

    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAboutDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    //{{AFX_MSG(CAboutDlg)
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
    //{{AFX_DATA_INIT(CAboutDlg)
    //}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CAboutDlg)
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
    //{{AFX_MSG_MAP(CAboutDlg)
        // No message handlers
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigDlg dialog

CCaymanConfigDlg::CCaymanConfigDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CCaymanConfigDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CCaymanConfigDlg)
    m_cszDataType = _T("");
    m_cszName = _T("");
    m_cszProtSup = _T("");
    m_cszFuncLib = _T("");
    m_cszConfigApp = _T("");
    m_cszProdVer = _T("");
    m_cszVendor = _T("");
    m_cszAPIVersion = _T("");
    //}}AFX_DATA_INIT
    // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_dwDeviceId = 0;
    
    
}

void CCaymanConfigDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CCaymanConfigDlg)
    DDX_Control(pDX, IDC_BTN_LOG_CONFIGURATION, m_ctrlBtnLogConfig);
    DDX_Control(pDX, IDC_CHECKLOGGING, m_ctrlChkLogging);
    DDX_Control(pDX, IDC_COMBO_DPA, m_ctrlComboDPA);
    DDX_Control(pDX, IDC_EDT_VENDOR, m_ctrlEdtVendor);
    DDX_Control(pDX, IDC_EDT_PROT_SUPP, m_ctrlEdtProtSupp);
    DDX_Control(pDX, IDC_EDT_PROD_VER, m_ctrlProdVersion);
    DDX_Control(pDX, IDC_EDT_NAME, m_ctrlEdtName);
    DDX_Control(pDX, IDC_EDT_FUNC_LIB, m_ctrlEdtFuncLib);
    DDX_Control(pDX, IDC_EDT_CONG_APP, m_ctrlEdtConfigApp);
    DDX_Control(pDX, IDC_EDT_API_VER, m_ctrlEdtApi);
    DDX_Control(pDX, IDC_BTN_OK, m_ctrlBtnOk);
    DDX_Control(pDX, IDC_BTN_OK2, m_ctrlBtnOk2);
    DDX_Control(pDX, IDC_BTN_CANCEL, m_ctrlBtnCancel);
    DDX_Control(pDX, IDC_BTN_CANCEL2, m_ctrlBtnCancel2);
    DDX_Text(pDX, IDC_ST_DPA_TYPE, m_cszDataType);
    DDX_Text(pDX, IDC_ST_NAME, m_cszName);
    DDX_Text(pDX, IDC_ST_PROT_SUPP, m_cszProtSup);
    DDX_Text(pDX, IDC_ST_FUNC_LIB, m_cszFuncLib);
    DDX_Text(pDX, IDC_ST_CONF_APP, m_cszConfigApp);
    DDX_Text(pDX, IDC_ST_PROD_VER2, m_cszProdVer);
    DDX_Text(pDX, IDC_ST_VENDOR, m_cszVendor);
    DDX_Text(pDX, IDC_ST_API_VER, m_cszAPIVersion);
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCaymanConfigDlg, CDialog)
    //{{AFX_MSG_MAP(CCaymanConfigDlg)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BTN_OK, OnBtnOk)
    ON_BN_CLICKED(IDC_BTN_OK2, OnBtnOk2)
    ON_BN_CLICKED(IDC_BTN_CANCEL, OnBtnCancel)
    ON_BN_CLICKED(IDC_BTN_CANCEL2, OnBtnCancel2)
    ON_BN_CLICKED(IDC_BTN_LOG_CONFIGURATION, OnBtnLogConfiguration)
    ON_BN_CLICKED(IDC_CHECKLOGGING, OnChecklogging)
    ON_CBN_SELCHANGE(IDC_COMBO_DPA, OnSelchangeComboDpa)
    //}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_BTN_SELECT_DEVICE, &CCaymanConfigDlg::OnBnClickedBtnSelectDevice)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCaymanConfigDlg message handlers

BOOL CCaymanConfigDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    CCaymanConfigApp *pApp=(CCaymanConfigApp*)::AfxGetApp();

    // Add "About..." menu item to system menu.

    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
    
    CWnd *pWndOk, *pWndCancel;
    m_hkey = FindJ2534Entry(); // Read registry, DeviceID value used, in some cases, to init screen
    if (m_hkey) {
        ReadValues();
    }
    else {
        // No J2534 device found in registry - let user know NONE
        CFont fnt;
        fnt.CreateFont(-10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Arial" );
        m_ctrlEdtConfigApp.SetFont(&fnt);
        m_ctrlEdtConfigApp.SetFont(&fnt);
        m_ctrlEdtProtSupp.SetFont(&fnt);
        m_ctrlEdtApi.SetFont(&fnt);
        m_ctrlEdtFuncLib.SetFont(&fnt);
        m_ctrlProdVersion.SetFont(&fnt);

        m_ctrlEdtConfigApp.SetWindowText("NONE");
        m_ctrlEdtProtSupp.SetWindowText("NONE");
        m_ctrlEdtApi.SetWindowText("NONE");
        m_ctrlEdtFuncLib.SetWindowText("NONE");
        m_ctrlProdVersion.SetWindowText("INVALID INSTALL");
        pWndCancel=GetDlgItem(IDC_BTN_CANCEL);
        pWndCancel->ShowWindow(SW_HIDE);
    }

    CButton *pWndLog;
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL);
    pWndCancel->ShowWindow(SW_SHOWNORMAL);
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL2);
    pWndCancel->ShowWindow(SW_SHOWNORMAL);
    pWndOk=GetDlgItem(IDC_BTN_OK);
    pWndOk->ShowWindow(SW_HIDE);
    pWndOk=GetDlgItem(IDC_BTN_OK2);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    pWndOk=GetDlgItem(IDC_CHECKLOGGING);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    pWndOk=GetDlgItem(IDC_STATIC_LOG);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    pWndOk=GetDlgItem(IDC_BTN_LOG_CONFIGURATION);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
#ifdef J2534_DEVICE_VSI
    this->SetWindowText("VSI-2534 Utility");

#endif
#ifdef J2534_DEVICE_KRHTWIRE
// #if defined (NDEBUG)
    this->SetWindowText("KeylessRide HotWire Utility");
 //   pWndOk=GetDlgItem(IDC_CHECKLOGGING);
 //   pWndOk->ShowWindow(SW_HIDE);
 //   pWndOk=GetDlgItem(IDC_STATIC_LOG);
 //   pWndOk->ShowWindow(SW_HIDE);
 //   pWndOk=GetDlgItem(IDC_BTN_LOG_CONFIGURATION);
 //   pWndOk->ShowWindow(SW_HIDE);
 //#else
 //   this->SetWindowText("KeylessRide HotWire Utility - DEBUG");
#endif
#ifdef J2534_DEVICE_CAYMAN
    pWndOk=GetDlgItem(IDC_BTN_OK);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    pWndOk=GetDlgItem(IDC_BTN_OK2);
    pWndOk->ShowWindow(SW_HIDE);
    pWndOk=GetDlgItem(IDC_CHECKLOGGING);
    pWndOk->ShowWindow(SW_HIDE);
    pWndOk=GetDlgItem(IDC_STATIC_LOG);
    pWndOk->ShowWindow(SW_HIDE);
    pWndOk=GetDlgItem(IDC_BTN_LOG_CONFIGURATION);
    pWndOk->ShowWindow(SW_HIDE);
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL);
    pWndCancel->ShowWindow(SW_HIDE);
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL2);
    pWndCancel->ShowWindow(SW_HIDE);
#endif
#ifdef J2534_DEVICE_DPA4
    this->SetWindowText("DPA J2534 Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
//  m_ctrlComboDPA.AddString("DPA 4+");
//  m_ctrlComboDPA.AddString("DPA 5 RS232");
    m_ctrlComboDPA.AddString("DPA 5 USB");
    m_ctrlComboDPA.SetCurSel(0);

    m_cszDataType = "DPA Type";
#endif
#ifdef J2534_DEVICE_DPA5

    this->SetWindowText("DPA 5 J2534 Config Utility");

    pWndOk=GetDlgItem(IDC_BTN_SELECT_DEVICE);
    pWndOk->ShowWindow(SW_SHOWNORMAL);

    pWndOk=GetDlgItem(IDC_EDIT_SELECTED_DEVICE);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_cszDataType = "DPA 5 Type";
    // scan INI for all DeviceName entries and add to Combo
    InitDpaComboBoxText();
#endif
#ifdef J2534_DEVICE_DPA50305

    this->SetWindowText("DPA 5 J2534 0305 Config Utility");

    pWndOk=GetDlgItem(IDC_BTN_SELECT_DEVICE);
    pWndOk->ShowWindow(SW_SHOWNORMAL);

    pWndOk=GetDlgItem(IDC_EDIT_SELECTED_DEVICE);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_cszDataType = "DPA 5 Type";
    // scan INI for all DeviceName entries and add to Combo
    InitDpaComboBoxText();
#endif
#ifdef J2534_DEVICE_SOFTBRIDGE
    this->SetWindowText("SoftBridge Euro5 Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("SoftBridge USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "Euro5 Type";

#endif
#ifdef J2534_DEVICE_WURTH
    this->SetWindowText("WoW! Flash Box Euro5 Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("WoW! Flash Box USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "Euro5 Type";

#endif
#ifdef J2534_DEVICE_DELPHI
    this->SetWindowText("Delphi Euro5 Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("Delphi Euro5 USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "Euro5 Type";

#endif
#ifdef J2534_DEVICE_DBRIDGE
    this->SetWindowText("d-briDGe Config Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("d-briDGe USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "d-briDGe Type";
#ifdef J2534_DEVICE_SVCI
    this->SetWindowText("SVCI Config Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("SVCI USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "SVCI Type";
#endif

#endif
#ifdef J2534_DEVICE_DBRIDGE0305
    this->SetWindowText("d-briDGe0305 Config Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("d-briDGe USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "d-briDGe0305 Type";

#endif
#ifdef J2534_DEVICE_NETBRIDGE
    this->SetWindowText("Netbridge Config Utility");

    pWndOk=GetDlgItem(IDC_COMBO_DPA);
    pWndOk->ShowWindow(SW_SHOWNORMAL);
    m_ctrlComboDPA.ResetContent();
    m_ctrlComboDPA.AddString("Netbridge USB");
    m_ctrlComboDPA.SetCurSel(0);
    m_cszDataType = "Netbridge Type";
#endif

    pApp->shFlag=0;
    
    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);         // Set big icon
    SetIcon(m_hIcon, FALSE);        // Set small icon
        
    // TODO: Add extra initialization here
    // Read from VSI Logging Registry
    int i;
    //g_ulLogging = 0;

    i = g_ulLogging ? 1 : 0;
    pWndLog = (CButton *)GetDlgItem(IDC_CHECKLOGGING);
    pWndLog->SetCheck(i);
    pWndLog = (CButton *)GetDlgItem(IDC_BTN_LOG_CONFIGURATION);
    pWndLog->EnableWindow(i);

    FillDialog();
    return TRUE;  // return TRUE  unless you set the focus to a control
}

CString GetIniString()
{
    char buffer[5000];
    GetWindowsDirectory(buffer, sizeof(buffer));
    CString strIni = (LPCTSTR)buffer + CString("\\");
    strIni += DEVICE_INI;
    return strIni;
}

void GetDeviceName(CString strIniFile, int DeviceID, char *DeviceName, int MaxDeviceNameSize)
{
    char deviceSection[80];
    sprintf_s(deviceSection, "DeviceInformation%d", DeviceID);  
    GetPrivateProfileString(deviceSection, // points to section name
                                "DeviceName",   // points to key name
                                "", // points to default string
                                DeviceName, // points to destination buffer
                                MaxDeviceNameSize,  // size of destination buffer
                                strIniFile);

}


// scan INI for all DeviceName entries and add to Combo
// Read registry for a type previously selected
void CCaymanConfigDlg::InitDpaComboBoxText()
{
    char DeviceName[80];
    CString strIniFile = GetIniString();
    int SelectedDevice = 0;

    // m_dwDeviceId is set to what is found in registry, defaults to 0
    int i, DeviceID = m_dwDeviceId ?  m_dwDeviceId : 1;
    GetDeviceName(strIniFile, DeviceID, m_SelectedDeviceName, sizeof(m_SelectedDeviceName) - 1);

    for (i = 0, DeviceID = 1; DeviceID < 300; DeviceID++)
    {
        GetDeviceName(strIniFile, DeviceID, DeviceName, sizeof(DeviceName) - 1);
        if (strlen(DeviceName) == 0)
        {
            continue; // No device at this DeviceID, keep looking
        }
        if (strcmp(DeviceName, m_SelectedDeviceName) == 0)
        {
            SelectedDevice = i; // Remember this
            GetDlgItem(IDC_EDIT_SELECTED_DEVICE)->SetWindowText(DeviceName);
        }
            
        m_ctrlComboDPA.AddString(DeviceName); // Add each Name found in a DeviceInformationX section
        i++;
    }
    // m_dwDeviceId is set to what is found in registry, defaults to 0
    m_ctrlComboDPA.SetCurSel(SelectedDevice);
}

void CCaymanConfigDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialog::OnSysCommand(nID, lParam);
    }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCaymanConfigDlg::OnPaint() 
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCaymanConfigDlg::OnQueryDragIcon()
{
    return (HCURSOR) m_hIcon;
}

void CCaymanConfigDlg::OnBtnOk() 
{
    /*CString str,str1,str2,strdevicename;
    str = m_strDeviceName;
    str1 = "Config";
    strdevicename = str +str1;
    MessageBox("Please select Cayman Interface",strdevicename,MB_ICONWARNING);*/
    CDialog::OnOK();
    
    
}

void CCaymanConfigDlg::OnBtnOk2() 
{
    /*CString str,str1,str2,strdevicename;
    str = m_strDeviceName;
    str1 = "Config";
    strdevicename = str +str1;
    MessageBox("Please select Cayman Interface",strdevicename,MB_ICONWARNING);*/
    CButton *pWnd;
    int i;
#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) || defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    i = m_ctrlComboDPA.GetCurSel();
    m_ctrlComboDPA.GetLBText(i, m_SelectedDeviceName);
#endif
    pWnd = (CButton *)GetDlgItem(IDC_CHECKLOGGING);
    i = pWnd->GetCheck();
    DWORD dwDword = REG_DWORD, dwString = REG_SZ; 

    char ucRead[1024];

    DWORD regdata = sizeof(ucRead);
    // DWORD cdata = sizeof(ucChange);
    DWORD dwMask;   

    // Write to VSI Logging Registry
    // Obtain current values shown on dialog
    UpdateData(TRUE);

//  if (i == 1)
//      g_ulLogging = 7;
//  else
    if (i == 0)
        g_ulLogging = 0;

    sprintf_s(ucRead, "%lu", g_ulLogging);
    dwMask = 0x000000FF;
    for (i = 0; i < sizeof(g_ulLogging); i++)
    {
        ucRead[i] = (unsigned char) ((g_ulLogging & dwMask) >> (8 * i));
        dwMask = dwMask << 8;
    }
//#if defined (J2534_DEVICE_KRHTWIRE) && defined (NDEBUG)
//    ucRead[0] = 0x00;               // Logging turned off
//#endif
    LONG lSuccess = RegSetValueEx(m_hkey, J2534REGISTRY_LOGGING, NULL, dwDword, (unsigned char *) ucRead, sizeof(g_ulLogging));
#ifdef J2534_DEVICE_DPA4
    unsigned long ulDeviceId;
    if (m_ctrlComboDPA.GetCurSel() == 0)
    {
        ulDeviceId = 150;
    }
//  else
//  {
//      ulDeviceId = 601;
//  }

    sprintf_s((char *)ucRead, "%lu", ulDeviceId);
    dwMask = 0x000000FF;
    for (i = 0; i < sizeof(ulDeviceId); i++)
    {
        ucRead[i] = (unsigned char) ((ulDeviceId & dwMask) >> (8 * i));
        dwMask = dwMask << 8;
    }
    lSuccess = RegSetValueEx(m_hkey, J2534REGISTRY_DEVICE_ID, NULL, dwDword, 
        ucRead, sizeof(ulDeviceId));
#endif
#if defined (J2534_DEVICE_SOFTBRIDGE) || defined (J2534_DEVICE_WURTH) || defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    // This 'fudges' DeviceId so that no INI file is needed. If these devices have other connection types besides USB, we need to
    // changes this to allow user to select it from drop down list. Till then, get value as defined at compile time
    m_dwDeviceId = DEVICE_TYPE;
#endif
#if /*defined (J2534_DEVICE_DPA5) || */defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    // Write Device-Connection Type to Registry
    RegSetValueEx(m_hkey, J2534REGISTRY_DEVICE_ID, 0, REG_DWORD, (unsigned char*)&m_dwDeviceId, sizeof(m_dwDeviceId));
#endif
    // Show any changes on the dialog
    UpdateData(FALSE);

    CDialog::OnOK();
    
    
}

void CCaymanConfigDlg::OnBtnCancel2() 
{
    //MessageBox("The current settings will not be useful if cancelled","Warning",MB_OKCANCEL );
    CDialog::OnCancel();

    
}
#ifdef J2534_0500
// For new J2534 new Spec implementation 

#endif 

#if defined (J2534_0404) || defined (J2534_0500)

typedef long (CALLBACK* PTOPEN)(void *, unsigned long *);
typedef long (CALLBACK* PTCLOSE)(unsigned long);
typedef long (CALLBACK* PTCONNECT)(unsigned long, unsigned long, unsigned long, unsigned long, unsigned long *);
typedef long (CALLBACK* PTDISCONNECT)(unsigned long);
typedef long (CALLBACK* PTREADVERSION)(unsigned long, char *, char *, char *);
typedef long (CALLBACK* PTREADREADSERIALNUM)(unsigned long, char *);

PTOPEN  PassThruOpen;
PTCLOSE PassThruClose;
PTCONNECT PassThruConnect;
PTDISCONNECT PassThruDisconnect;
PTREADVERSION PassThruReadVersion;
PTREADREADSERIALNUM PassThruReadSerialNum;

//STATUS_NOERROR
//ERR_DEVICE_NOT_CONNECTED
char* ErrorList[] = {"Device Is Successfully Detected",
                    "ERR_NOT_SUPPORTED",
                    "ERR_INVALID_CHANNEL_ID",
                    "ERR_INVALID_PROTOCOL_ID",
                    "ERR_NULL_PARAMETER",
                    "ERR_INVALID_IOCTL",
                    "ERR_INVALID_FLAGS",
                    "ERR_FAILED",
                    "ERR_DEVICE_NOT_CONNECTED",
                    "ERR_TIMEOUT",
                    "ERR_INVALID_MSG",
                    "ERR_INVALID_TIME_INTERVAL",
                    "ERR_EXCEEDED_LIMIT",
                    "ERR_INVALID_MSG_ID",
                    "ERR_DEVICE_IN_USE",
                    "ERR_INVALID_IOCTL_ID",
                    "ERR_BUFFER_EMPTY",
                    "ERR_BUFFER_FULL",
                    "ERR_BUFFER_OVERFLOW",
                    "ERR_PIN_INVALID",
                    "ERR_CHANNEL_IN_USE",
                    "ERR_MSG_PROTOCOL_ID",
                    "ERR_INVALID_FILTER_ID",
                    "ERROR_NO_FLOW_CONTROL",
                    "ERR_NOT_UNIQUE",
                    "ERR_INVALID_BAUDRATE",
                    "J2534_ERR_INVALID_DEVICE_ID"};
#endif

#ifdef J2534_0305
typedef long (CALLBACK* PTCONNECT)(unsigned long, unsigned long, unsigned long *);
typedef long (CALLBACK* PTDISCONNECT)(unsigned long);
typedef long (CALLBACK* PTREADVERSION)(char *, char *, char *);
typedef long (CALLBACK* PTREADREADSERIALNUM)(char *);

PTCONNECT PassThruConnect;
PTDISCONNECT PassThruDisconnect;
PTREADVERSION PassThruReadVersion;
PTREADREADSERIALNUM PassThruReadSerialNum;
//STATUS_NOERROR
//ERR_DEVICE_NOT_CONNECTED
char* ErrorList[] = {"Device Is Successfully Detected",
                    "ERR_NOT_SUPPORTED",
                    "ERR_INVALID_CHANNEL_ID",
                    "ERR_INVALID_PROTOCOL_ID",
                    "ERR_NULL_PARAMETER",
                    "ERR_INVALID_IOCTL",
                    "ERR_INVALID_FLAGS",
                    "ERR_FAILED",
                    "ERR_DEVICE_NOT_CONNECTED",
                    "ERR_TIMEOUT",
                    "ERR_INVALID_MSG",
                    "ERR_INVALID_TIME_INTERVAL",
                    "ERR_EXCEEDED_LIMIT",
                    "ERR_INVALID_MSG_ID",
                    "ERR_DEVICE_IN_USE",
                    "ERR_INVALID_IOCTL_ID",
                    "ERR_BUFFER_EMPTY",
                    "ERR_BUFFER_FULL",
                    "ERR_BUFFER_OVERFLOW",
                    "ERR_PIN_INVALID",
                    "ERR_CHANNEL_IN_USE",
                    "ERR_MSG_PROTOCOL_ID",
                    "ERR_INVALID_FILTER_ID",
                    "ERROR_NO_FLOW_CONTROL"};
#endif

int  CCaymanConfigDlg::Is64BitWindows(void)
{
#if defined(_WIN64)
    return TRUE;             // 64-bit programs run only on Win64
#elif defined(_WIN32)

    //
    // 32-bit programs run on both 32-bit and 64-bit Windows so must sniff

    BOOL f64 = FALSE;

    return IsWow64Process(GetCurrentProcess(), &f64) && f64;

#else
    return FALSE; // Win64 does not support Win16
#endif
}

void CCaymanConfigDlg::OnBtnCancel() 
{
    //MessageBox("The current settings will not be useful if cancelled","Warning",MB_OKCANCEL );
//  CDialog::OnCancel();
    CCaymanConfigApp *pApp=(CCaymanConfigApp*)::AfxGetApp();

    HINSTANCE  hDLL;
    CWnd *pWnd,*pWndOk,*pWndAutoDetct,*pWndCancel;
    pWnd=GetDlgItem(IDC_COMBO_J2534DEVICES);
    CString string;
    pWnd->GetWindowText(string);
    int error; 

    pWndOk=GetDlgItem(IDC_BTN_OK2);
    pWndAutoDetct=GetDlgItem(IDC_BTN_CANCEL2);
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL);

        pWndOk->EnableWindow(FALSE);
        pWndAutoDetct->EnableWindow(FALSE);
        pWndCancel->EnableWindow(FALSE);
        
    char chFWVer[5000];
    char chDLLVer[5000];
    char chAPIVer[5000];
    //char chSerialNumber[5000];
    //char tempbuffer[MAX_PATH];

    char system1[MAX_PATH];

    // CString strLogFilepath;

#ifdef J2534_DEVICE_KRHTWIRE
    CString m_strInternetAccessINI = GetIniString();
    GetPrivateProfileString("DefaultDirectory","PresentPath","",system1,256,m_strInternetAccessINI);
#else
    if (Is64BitWindows())
    {
        GetSystemWow64Directory(system1,MAX_PATH);
    }
    else
    {
        GetSystemDirectory(system1,MAX_PATH);
    }
#endif
    
    // GetPrivateProfileString("DefaultDirectory","PresentPath","",tempbuffer,256,m_strInternetAccessINI);
    // strLogFilepath = (LPCSTR)tempbuffer;
    strcat_s(system1, sizeof(system1), FUNCLIB);
    char DeviceDescr[30] = DEVICEDESC;

    if ((hDLL = LoadLibrary(system1)) == NULL)
        {
            ::MessageBox(NULL,"1)	Make sure that drivers are installed properly\n2)	Make sure that device is connected","Error",MB_OK);
            pWndOk->EnableWindow(TRUE);
            pWndAutoDetct->EnableWindow(TRUE);
            pWndCancel->EnableWindow(TRUE); 
            FreeLibrary(hDLL);
            return;
//          return 1;

        }

    char tempbuf[1024];

#if defined (J2534_0404) || defined (J2534_0500)   
    if ((PassThruOpen = (PTOPEN)GetProcAddress(hDLL, "PassThruOpen")) == NULL)
    {
        ::MessageBox(NULL,"1)	Make sure that drivers are installed properly\n2)	Make sure that device is connected","Error",MB_OK);
        pWndOk->EnableWindow(TRUE);
        pWndAutoDetct->EnableWindow(TRUE);
        pWndCancel->EnableWindow(TRUE); 
        FreeLibrary(hDLL);
        return;
//      return 1 ;
    }
                        
    error=PassThruOpen(NULL, &pApp->nDeviceId);
    sprintf_s(tempbuf, "%s", ErrorList[error]);
//  MessageBox(tempbuf,"Flash2Tool",MB_OK);

    if ((PassThruReadVersion = (PTREADVERSION)GetProcAddress(hDLL, "PassThruReadVersion")) == NULL)
    {
    
    }
    error=PassThruReadVersion(pApp->nDeviceId, chFWVer, chDLLVer, chAPIVer);
    if ((PassThruReadSerialNum = (PTREADREADSERIALNUM)GetProcAddress(hDLL, "PassThruReadSerialNum")) == NULL)
    {
    
    }
    //error=PassThruReadSerialNum(pApp->nDeviceId, chSerialNumber);
#endif
#ifdef J2534_0305       
    if ((PassThruReadVersion = (PTREADVERSION)GetProcAddress(hDLL, "PassThruReadVersion")) == NULL)
    {
    
    }
    error=PassThruReadVersion(chFWVer, chDLLVer, chAPIVer);
    if ((PassThruReadSerialNum = (PTREADREADSERIALNUM)GetProcAddress(hDLL, "PassThruReadSerialNum")) == NULL)
    {
    
    }
    if ((PassThruReadSerialNum = (PTREADREADSERIALNUM)GetProcAddress(hDLL, "PassThruReadSerialNum")) == NULL)
    {
    
    }
    //error=PassThruReadSerialNum(chSerialNumber);
#endif
    if (error == 0)
    {
        if (iLanguage==1)
        {
//                sprintf_s(tempbuf, "%s detected successfully! Firmware version %s\nS/N %s.", DeviceDescr, chFWVer, chSerialNumber);
                sprintf_s(tempbuf, "%s detected successfully! Firmware version %s", DeviceDescr, chFWVer);
        }
        if (iLanguage==2)
        {
//                sprintf_s(tempbuf, "Px2 d�tect�! Micrologiciel version %s\nS/N %s.", DeviceDescr, chFWVer, chSerialNumber);
                sprintf_s(tempbuf, "%s d�tect�! Micrologiciel version %s", DeviceDescr, chFWVer);
        }
        if (iLanguage==3)
        {
//                sprintf_s(tempbuf, "Px2 erfolgreich erfasst! Firmware version %s\nS/N %s.", DeviceDescr, chFWVer, chSerialNumber);
                sprintf_s(tempbuf, "%s erfolgreich erfasst! Firmware version %s", DeviceDescr, chFWVer);
        }
        if (iLanguage==4)
        {
//                sprintf_s(tempbuf, "Px2 detectado satisfactoriamente! Firmware versi�n %s\nS/N %s.", DeviceDescr, chFWVer, chSerialNumber);
                sprintf_s(tempbuf, "%s detectado satisfactoriamente! Firmware versi�n %s", DeviceDescr, chFWVer);
        }
        if (iLanguage==5)
        {
//                sprintf_s(tempbuf, "Rilevazione di Px2 riuscita! Firmware versione %s\nS/N %s.", DeviceDescr, chFWVer, chSerialNumber);
                sprintf_s(tempbuf, "Rilevazione di %s riuscita! Firmware versione %s", DeviceDescr, chFWVer);
        }
        MessageBox(tempbuf, DeviceDescr, MB_OK);            
    }
    else
    {
//      MessageBox("Error reading VSI-2534 firmware version.","VSI-2534",MB_OK | MB_ICONERROR);         
        char ErrTitle[20] = "";
        if (iLanguage==1)
        {
            sprintf_s(tempbuf, "Could not find %s. Please power cycle the %s and try again.\n\nPlease be sure:\n1) %s is connected to your PC and is powered.\n2) The drivers are installed properly.\n3) No other programs that use ports are open.\n4) Libraries are installed properly.\n", DeviceDescr, DeviceDescr, DeviceDescr);
            strcpy_s(ErrTitle, "Error");
        }
        if (iLanguage==2)
        {
            sprintf_s(tempbuf, "%s introuvable. Red�marrez le %s et essayez � nouveau.\n\nV�rifiez que:\n1) Le %s est connect� � votre PC et sous tension.\n2) Les pilotes sont correctement install�s.\n3) Aucun autre programme utilisant le port n'est ouvert.\n4) Les biblioth�ques sont correctement install�es.\n", DeviceDescr, DeviceDescr, DeviceDescr);
            strcpy_s(ErrTitle, "Erreur");
        }
        if (iLanguage==3)
        {
            sprintf_s(tempbuf, "%s nicht gefunden. Bitte %s aus- und einschalten und nochmals versuchen.\n\nVergewissern Sie sich:\n1) %s ist an Ihren PC angeschlossen und angeschaltet.\n2) Die Treiber sind korrekt installiert.\n3) Keine anderen Programme, die einen Port verwenden, sind ge�ffnet.\n4) Die Archive sind korrekt angelegt.\n", DeviceDescr, DeviceDescr, DeviceDescr);
            strcpy_s(ErrTitle, "Fehler");
        }
        if (iLanguage==4)
        {
            sprintf_s(tempbuf, "No se pudo encontrar %s. Reinicie %s e intente de nuevo.\n\nAseg�rese de que:\n1) %s est� conectado a su PC y est� encendido.\n2) Los controladores est�n instalados correctamente.\n3) Ning�n otro programa que use puerto est� abierto.\n4) Las bibliotecas est�n instaladas correctamente.\n", DeviceDescr, DeviceDescr, DeviceDescr);
            strcpy_s(ErrTitle, "Error");
        }
        if (iLanguage==5)
        {
            sprintf_s(tempbuf, "%s non trovato. Spegnere e riaccendere il %s e riprovare.\n\nAssicurarsi che:\n1) Il %s sia collegato al PC e sia acceso.\n2) I driver siano installati correttamente.\n3) Nessun altro programma che utilizza la porta sia aperto.\n4) Le librerie siano installate correttamente.\n", DeviceDescr, DeviceDescr, DeviceDescr);
            strcpy_s(ErrTitle, "Errore");
        }
        MessageBox(tempbuf, ErrTitle, MB_OK | MB_ICONERROR);        
    }
#if defined (J2534_0404) || defined (J2534_0500)   
    if ((PassThruClose = (PTCLOSE)GetProcAddress(hDLL, "PassThruClose")) == NULL)
    {
        
    }
    PassThruClose(pApp->nDeviceId);
#endif
    pWndOk->EnableWindow(TRUE);
    pWndAutoDetct->EnableWindow(TRUE);
    pWndCancel->EnableWindow(TRUE);
    FreeLibrary(hDLL);
//  return 1;
    
}

/*
int CCaymanConfigDlg::OnBtnAutoDet() 
{
    CCaymanConfigApp *pApp=(CCaymanConfigApp*)::AfxGetApp();

    HINSTANCE  hDLL,hDLL1;
    CWnd *pWnd,*pWndOk,*pWndAutoDetct,*pWndCancel;
    pWnd=GetDlgItem(IDC_COMBO_J2534DEVICES);
    CString string;
    pWnd->GetWindowText(string);
    char tempbuf[1024];
    int error; 

    pWndOk=GetDlgItem(IDC_BTN_OK);
    pWndAutoDetct=GetDlgItem(IDC_BTN_AUTO_DET);
    pWndCancel=GetDlgItem(IDC_BTN_CANCEL);
    
    if(string=="VSI-2534")
    {
        pWndOk->EnableWindow(FALSE);
        pWndAutoDetct->EnableWindow(FALSE);
        pWndCancel->EnableWindow(FALSE);
        
        if ((hDLL = LoadLibrary("dgVSI32.dll")) == NULL)
        {
            ::MessageBox(NULL,"1)	Make sure that drivers are installed properly2)Make sure that device is connected","Error",MB_OK);
            pWndOk->EnableWindow(TRUE);
            pWndAutoDetct->EnableWindow(TRUE);
            pWndCancel->EnableWindow(TRUE); 
            FreeLibrary(hDLL);
            return 1;

        }
        
        if ((PassThruOpen = (PTOPEN)GetProcAddress(hDLL, "PassThruOpen")) == NULL)
        {
            ::MessageBox(NULL,"1)	Make sure that drivers are installed properly\n2)Make sure that device is connected","Error",MB_OK);
            pWndOk->EnableWindow(TRUE);
            pWndAutoDetct->EnableWindow(TRUE);
            pWndCancel->EnableWindow(TRUE); 
            FreeLibrary(hDLL);
            return 1 ;
        }
                            
        error=PassThruOpen(NULL, &pApp->nDeviceId);
        sprintf_s(tempbuf, "%s", ErrorList[error]);
        MessageBox(tempbuf,"Flash2Tool",MB_OK);

        if ((PassThruClose = (PTCLOSE)GetProcAddress(hDLL, "PassThruClose")) == NULL)
        {
        
        }
        PassThruClose(pApp->nDeviceId);
        
        pWndOk->EnableWindow(TRUE);
        pWndAutoDetct->EnableWindow(TRUE);
        pWndCancel->EnableWindow(TRUE);
        FreeLibrary(hDLL);

    }

    if(string=="Python1B")
    {
        pWndOk->EnableWindow(FALSE);
        pWndAutoDetct->EnableWindow(FALSE);
        pWndCancel->EnableWindow(FALSE);

        if ((hDLL1 = LoadLibrary("DGP40432.dll")) == NULL)
        {
            ::MessageBox(NULL,"1)	Make sure that drivers are installed properly\n2)Make sure that device is connected","Error",MB_OK);      
              FreeLibrary(hDLL1);
              pWndOk->EnableWindow(TRUE);
              pWndAutoDetct->EnableWindow(TRUE);
              pWndCancel->EnableWindow(TRUE);
              return 1;
        
        }
        if ((PassThruOpen = (PTOPEN)GetProcAddress(hDLL, "PassThruOpen")) == NULL)
        {
            ::MessageBox(NULL,"1)Make sure that drivers are installed properly\n2)Make sure that device is connected","Error",MB_OK);
            pWndOk->EnableWindow(TRUE);
            pWndAutoDetct->EnableWindow(TRUE);
            pWndCancel->EnableWindow(TRUE); 
            FreeLibrary(hDLL1);
            return 1;
        
        }
        
        error=PassThruOpen(NULL, &pApp->nDeviceId);
        sprintf_s(tempbuf, "%s", ErrorList[error]);
        //sprintf_s(tempbuf, "%d", error);
        MessageBox(tempbuf,"Python1B",MB_OK);

        if ((PassThruClose = (PTCLOSE)GetProcAddress(hDLL, "PassThruClose")) == NULL)
        {
        
        }
        PassThruClose(pApp->nDeviceId);

        pWndOk->EnableWindow(TRUE);
        pWndAutoDetct->EnableWindow(TRUE);
        pWndCancel->EnableWindow(TRUE);
        FreeLibrary(hDLL1);
    }

    return 1;
}





void CCaymanConfigDlg::OnSelendokComboJ2534devices() 
{
    CCaymanConfigApp *pApp=(CCaymanConfigApp*)::AfxGetApp();
    pApp->flag=0;
    FillJ2534List();    
}
*/

HKEY CCaymanConfigDlg::FindJ2534Entry()
{
    long lSuccess;
    char tempstr[1024];
    DWORD dwSzdata;  
    unsigned char ucValueread[1024]; // read in String value
    DWORD dwDword = REG_DWORD, dwString = REG_SZ; 
    char chValueset[1024];          // used to set String values
    HKEY mainkey;
    int i=0, highdevice=1, highvendor=1;
//  DWORD datasize, dworddata;

#if defined (J2534_0404) || defined (J2534_0500)
	/*Block is required to check Windows OS > 10 and < 10*/
    strcpy_s(tempstr, J2534REGISTRY_KEY_PATH);  // "Software\PassThruSupport.04.04"
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0, KEY_ALL_ACCESS, &mainkey);
	if (lSuccess != ERROR_SUCCESS)
	{
		strcpy_s(tempstr, J2534REGISTRY_KEY_PATH_6432);  // "Software\PassThruSupport.04.04"
		lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0, KEY_ALL_ACCESS, &mainkey);
	}
    if(lSuccess==ERROR_SUCCESS)
    {
        i=0;
        dwSzdata=1024;
        lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
        
//      if(tempstr[strlen(tempstr)-1]-'0'==highvendor)
//          highvendor++;
        lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                0, KEY_ALL_ACCESS, &tempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwSzdata = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, 
                ucValueread, &dwSzdata);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset,REGISTRY_NAME))
                {
                    dwSzdata = sizeof(ucValueread);
                    lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_VENDOR, NULL, &dwString, ucValueread, &dwSzdata);
                
                    if(lSuccess==ERROR_SUCCESS)
                    {
                        sprintf_s(chValueset, "%s", ucValueread);
                        if(!strcmp(chValueset,VENDOR))
                        {
                            break;
                        }
                    }
                }
            }

            i++;
            dwSzdata=1024;
            lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
            
//          if(tempstr[strlen(tempstr)-1]-'0'==highvendor)
//              highvendor++;
            RegCloseKey(tempkey);
            if (lSuccess != ERROR_SUCCESS) {
                return NULL;
            }

            lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                    0, KEY_ALL_ACCESS, &tempkey);
        }
/*
        if(lSuccess==ERROR_SUCCESS)
        {
            char tempstr2[1024];
            i=0;
            dwSzdata=1024;
            lSuccess = RegOpenKeyEx(tempkey, "Devices", 
                    0, KEY_ALL_ACCESS, &tempkey2);

            lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                sprintf_s(tempstr, "Device%d", highdevice);
                RegCreateKey(tempkey2, tempstr, &tempkey);
                CreateDefaults(&tempkey);
                return &tempkey;
            }
            if(tempstr2[strlen(tempstr2)-1]-'0'==highdevice)
                highdevice++;
            lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                0, KEY_ALL_ACCESS, &tempkey);

            while(lSuccess==ERROR_SUCCESS)
            {
                dwSzdata = sizeof(ucValueread);
                lSuccess = RegQueryValueEx(tempkey, "Name", NULL, &dwString, 
                    ucValueread, &dwSzdata);

                if(lSuccess==ERROR_SUCCESS)
                {
                    sprintf_s(chValueset, "%s", ucValueread);
                    if(!strcmp(chValueset,"Gryphon"))
                    {
                        RegCloseKey(tempkey2);
                        break;
                    }
                }

                i++;
                dwSzdata=1024;
                lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
                if(lSuccess!=ERROR_SUCCESS)
                {
                    sprintf_s(tempstr, "Device%d", highdevice);
                    RegCreateKey(tempkey2, tempstr, &tempkey);
                    CreateDefaults(&tempkey);
                    return &tempkey;
                    break;
                }
                if(tempstr2[strlen(tempstr2)-1]-'0'==highdevice)
                    highdevice++;
                lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                    0, KEY_ALL_ACCESS, &tempkey);
            }
        }
*/  }
    RegCloseKey(mainkey);
#endif
#ifdef J2534_0305
    strcpy_s(tempstr, J2534REGISTRY_KEY_PATH);  // "Software\PassThruSupport"
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tempstr, 0, KEY_ALL_ACCESS, &mainkey);

    if(lSuccess==ERROR_SUCCESS)
    {
        i=0;
        dwSzdata=1024;
        char tempstr2[1024];

        lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
        if(lSuccess!=ERROR_SUCCESS)
        {
            RegCloseKey(mainkey);
            return NULL;
        }
        lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                0, KEY_ALL_ACCESS, &tempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwSzdata = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, 
                ucValueread, &dwSzdata);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset,VENDOR))
                    break;
            }

            RegCloseKey(tempkey);
            i++;
            lSuccess = RegEnumKey(mainkey, i, tempstr, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                break;
            }
            lSuccess = RegOpenKeyEx(mainkey, tempstr, 
                    0, KEY_ALL_ACCESS, &tempkey);
        }

        if(lSuccess==ERROR_SUCCESS)
        {
            i=0;
            dwSzdata=1024;
            lSuccess = RegOpenKeyEx(tempkey, "Devices", 
                    0, KEY_ALL_ACCESS, &tempkey2);

            lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                RegCloseKey(tempkey2);
                RegCloseKey(tempkey);
                RegCloseKey(mainkey);
                return NULL;
            }
            RegCloseKey(tempkey);
            lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                0, KEY_ALL_ACCESS, &tempkey);

            while(lSuccess==ERROR_SUCCESS)
            {
                dwSzdata = sizeof(ucValueread);
                lSuccess = RegQueryValueEx(tempkey, J2534REGISTRY_NAME, NULL, &dwString, 
                    ucValueread, &dwSzdata);

                if(lSuccess==ERROR_SUCCESS)
                {
                    sprintf_s(chValueset, "%s", ucValueread);
                    if(!strcmp(chValueset, REGISTRY_NAME))
                    {
/*                      dwSzdata = sizeof(ucValueread);
                        lSuccess = RegQueryValueEx(tempkey, "Logging", 
                            NULL, &dwDword, 
                            (unsigned char *)&ulValueread, &dwSzdata);

                        if(lSuccess==ERROR_SUCCESS)
                        {
                            g_ulLogging = ulValueread;
                        }

//                      RegCloseKey(tempkey2);
*/                      break;
                    }
                }

                RegCloseKey(tempkey);
                i++;
                dwSzdata=1024;
                lSuccess = RegEnumKey(tempkey2, i, tempstr2, dwSzdata);
                if(lSuccess!=ERROR_SUCCESS)
                {
                    break;
                }
                lSuccess = RegOpenKeyEx(tempkey2, tempstr2, 
                    0, KEY_ALL_ACCESS, &tempkey);
            }
        }
    }

    RegCloseKey(mainkey);
//  RegCloseKey(tempkey);
    RegCloseKey(tempkey2);
#endif
    return tempkey;
}

//-----------------------------------------------------------------------------
//  Function Name   : ReadValues()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function reads the values for each characteristics 
//                    currently in the registry system.
//-----------------------------------------------------------------------------
void CCaymanConfigDlg::ReadValues()
{
    DWORD dwValueread[1024];            // read in DWORD value
    long lSuccess;


    CString szParamsPort;
    
    DWORD dwRegdata;        // size of data
    
    DWORD dwDword = REG_DWORD, dwString = REG_SZ; // type of data

    unsigned char ucValueread[1024]; // read in String value
    DWORD dwSzdata;

    // Flush the cache 
    // Note: Required for Windows 95

    // DeviceId
    dwRegdata = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(m_hkey, "DeviceId", NULL, &dwDword, (unsigned char *) dwValueread, &dwRegdata);
    m_dwDeviceId = dwValueread[0];

    // Vendor
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_VENDOR, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtVendor.SetWindowText((char *) ucValueread);

    // Name
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtName.SetWindowText((char *) ucValueread);

    // ProtocolsSupported
#ifdef J2534_0305
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_PROTOCOLSSUPPORT, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtProtSupp.SetWindowText((char *) ucValueread);
#else
    m_ctrlEdtProtSupp.SetWindowText(PROTOCOLSSUPPORTED);
#endif

    // ConfigApplication
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_CONFIGAPP, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtConfigApp.SetWindowText((char *) ucValueread);

    // FunctionLibrary
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_FUNCLIB, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtFuncLib.SetWindowText((char *) ucValueread);   


    // APIVersion
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_APIVERSION, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlEdtApi.SetWindowText((char *) ucValueread);
/*
    // TimeStampResolution
    dwRegdata = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(m_hkey, "TimeStampResolution (microseconds)", 
        NULL, &dwDword, (unsigned char *) dwValueread, &dwRegdata);
    m_dwTimeStampRes = dwValueread[0];
*/
    // ProductVersion
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_PRODUCTVERSION, NULL, &dwString, ucValueread, &dwSzdata);
    m_ctrlProdVersion.SetWindowText((char *) ucValueread);
/*
    // IPAddress
    char chValueset[1024];          // used to set String values
    dwSzdata = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(m_hkey, "IPAddress", NULL, &dwString, ucValueread, &dwSzdata);
    sprintf_s(chValueset, "%s", ucValueread);
    m_szIPAddress = chValueset;

    // PortNum
    dwRegdata = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(m_hkey, "PortNum", NULL, &dwDword, (unsigned char *) dwValueread, &dwRegdata);
    m_dwPortNum = dwValueread[0];
*/
    // Logging
//#if !defined (J2534_DEVICE_KRHTWIRE) || defined (_DEBUG)
    dwRegdata = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_LOGGING, NULL, &dwDword, (unsigned char *) dwValueread, &dwRegdata);
    g_ulLogging = dwValueread[0];
//#endif
    // Language
    dwRegdata = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(m_hkey, J2534REGISTRY_LANGUAGE, NULL, &dwDword, (unsigned char *) dwValueread, &dwRegdata);
    iLanguage = !lSuccess ? dwValueread[0] : 1; // If Language is in registery, use what it, else default to English
    // Show any changes on the dialog
    UpdateData(FALSE);
}
void CCaymanConfigDlg::OnBtnLogConfiguration() 
{
    // TODO: Add your control notification handler code here
    m_clLogConfig.m_ulLogging = g_ulLogging;
    m_clLogConfig.iLanguage = iLanguage;
    if (m_clLogConfig.DoModal() == IDOK)
    {
        g_ulLogging = m_clLogConfig.m_ulLogging;
    }
}

void CCaymanConfigDlg::OnChecklogging() 
{
    // TODO: Add your control notification handler code here
    CButton *pWndLog;
    int i;
    pWndLog = (CButton *)GetDlgItem(IDC_CHECKLOGGING);
    i = pWndLog->GetCheck();

    pWndLog = (CButton *)GetDlgItem(IDC_BTN_LOG_CONFIGURATION);
    pWndLog->EnableWindow(i);
    g_ulLogging =  i ? 7 : 0;
}

BOOL CCaymanConfigDlg::DestroyWindow() 
{
    // TODO: Add your specialized code here and/or call the base class
#ifndef J2534_DEVICE_CAYMAN
    if (m_hkey != NULL)
        RegCloseKey(m_hkey);
#endif
    return CDialog::DestroyWindow();
}

#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) || defined (J2534_DEVICE_SOFTBRIDGE)|| defined (J2534_DEVICE_WURTH)|| defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
int GetDeviceId(const char *SelectedDeviceName)
{
    int MaxDevId = 300;
    int DevID;
    CString strIniFile = GetIniString();
    for (DevID = 1; DevID < MaxDevId; DevID++)
    {
        char DeviceName[80];
        GetDeviceName(strIniFile, DevID, DeviceName, sizeof(DeviceName) - 1);
        if (strlen(DeviceName) == 0)
        {
            continue; // We are done here - get out
        }
        if (strcmp(DeviceName, SelectedDeviceName) == 0)
            break;

    }
    return DevID < MaxDevId ? DevID : 1;
}
#endif

void CCaymanConfigDlg::OnSelchangeComboDpa() 
{
#if defined (J2534_DEVICE_DPA5) || defined (J2534_DEVICE_DPA50305) || defined (J2534_DEVICE_SOFTBRIDGE) || defined (J2534_DEVICE_WURTH) || defined (J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
    m_dwDeviceId = m_ctrlComboDPA.GetCurSel() + 1;
    int Sel = m_ctrlComboDPA.GetCurSel();
    m_ctrlComboDPA.GetLBText(Sel, m_SelectedDeviceName);
    m_dwDeviceId = GetDeviceId(m_SelectedDeviceName);
    FillDialog();
#endif
}

void CCaymanConfigDlg::FillDialog() 
{
    CMenu* pSysMenu = GetSystemMenu(FALSE);
    UpdateData(TRUE);
    if (pSysMenu != NULL)
    {
//      pSysMenu->DestroyMenu();
        CString strAboutMenu;
        if (iLanguage == 1)
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (iLanguage == 2)
        strAboutMenu.LoadString(IDS_STRING102);
        if (iLanguage == 3)
        strAboutMenu.LoadString(IDS_STRING103);
        if (iLanguage == 4)
        strAboutMenu.LoadString(IDS_STRING104);
        if (iLanguage == 5)
        strAboutMenu.LoadString(IDS_STRING105);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->DeleteMenu(3, MF_BYPOSITION);
//          pSysMenu->RemoveMenu(0, IDM_ABOUTBOX);
//          pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }
        
    if (iLanguage==1)
    {
        m_cszDataType = J2534REGISTRY_LANGUAGE;
        m_cszDataType = "";
#ifdef J2534_DEVICE_DPA4
        m_cszDataType = "DPA Type";
#endif
        m_cszName = J2534REGISTRY_NAME;
        m_cszVendor = J2534REGISTRY_VENDOR;
        m_cszProtSup = J2534REGISTRY_PROTOCOLSSUPPORT;
        m_cszConfigApp = J2534REGISTRY_CONFIGAPP;
        m_cszFuncLib = J2534REGISTRY_FUNCLIB;
        m_cszProdVer = J2534REGISTRY_PRODUCTVERSION;
        m_cszAPIVersion = J2534REGISTRY_APIVERSION;
        m_ctrlBtnOk2.SetWindowText("OK");
        m_ctrlBtnCancel2.SetWindowText("Cancel");
        m_ctrlChkLogging.SetWindowText("Turn Logging ON");
        m_ctrlBtnLogConfig.SetWindowText("Configuration");
        m_ctrlBtnCancel.SetWindowText("Get Firmware Version");
    }
    if (iLanguage==2)
    {
        m_cszDataType = "Langue";
        m_cszName = "Nom";
        m_cszVendor = "Vendeur";
        m_cszProtSup = "Protocoles support�s";
        m_cszConfigApp = "Application de config.";
        m_cszFuncLib = "Biblio. de fonctions";
        m_cszProdVer = "Version du produit";
        m_cszAPIVersion = "Version du API";
        m_ctrlBtnOk2.SetWindowText("OK");
        m_ctrlBtnCancel2.SetWindowText("Annuler");
        m_ctrlChkLogging.SetWindowText("Activer la journalisation");
        m_ctrlBtnLogConfig.SetWindowText("Configuration");
        m_ctrlBtnCancel.SetWindowText("Obtenir la version du micrologiciel");
    }
    if (iLanguage==3)
    {
        m_cszDataType = "Sprache";
        m_cszName = J2534REGISTRY_NAME;
        m_cszVendor = "Verk�ufer";
        m_cszProtSup = "Unterst�tzte Protokolle";
        m_cszConfigApp = "Anwendung Config";
        m_cszFuncLib = "Funktionsbibliothek";
        m_cszProdVer = "Produktversion";
        m_cszAPIVersion = J2534REGISTRY_APIVERSION;
        m_ctrlBtnOk2.SetWindowText("OK");
        m_ctrlBtnCancel2.SetWindowText("Abbrechen");
        m_ctrlChkLogging.SetWindowText("Protokollierung einschalten");
        m_ctrlBtnLogConfig.SetWindowText("Konfiguration");
        m_ctrlBtnCancel.SetWindowText("Firmware Version erfragen");
    }
    if (iLanguage==4)
    {
        m_cszDataType = "Lengua";
        m_cszName = "Nombre";
        m_cszVendor = "Proveedor";
        m_cszProtSup = "Protocolos admitidos";
        m_cszConfigApp = "Aplicaci�n config.";
        m_cszFuncLib = "Biblioteca de funciones";
        m_cszProdVer = "Versi�n de producto";
        m_cszAPIVersion = "Versi�n de API";
        m_ctrlBtnOk2.SetWindowText("Aceptar");
        m_ctrlBtnCancel2.SetWindowText("Cancelar");
        m_ctrlChkLogging.SetWindowText("Activar el registro");
        m_ctrlBtnLogConfig.SetWindowText("Configuraci�n");
        m_ctrlBtnCancel.SetWindowText("Obtener la versi�n de firmware");
    }
    if (iLanguage==5)
    {
        m_cszDataType = "Lingua";
        m_cszName = "Nome";
        m_cszVendor = "Fornitore";
        m_cszProtSup = "Protocolli supportati";
        m_cszConfigApp = "Applicazione di config.";
        m_cszFuncLib = "Libreria di funzioni";
        m_cszProdVer = "Versione prodotto";
        m_cszAPIVersion = "Versione API";
        m_ctrlBtnOk2.SetWindowText("OK");
        m_ctrlBtnCancel2.SetWindowText("Annulla");
        m_ctrlChkLogging.SetWindowText("Attivazione della Registrazione");
        m_ctrlBtnLogConfig.SetWindowText("Configurazione");
        m_ctrlBtnCancel.SetWindowText("Ottenimento Versione Firmware");
    }
    UpdateData(FALSE);
}


void CCaymanConfigDlg::OnBnClickedBtnSelectDevice()
{
    // TODO: Add your control notification handler code here
    m_clsSelectDeviceDlg.m_dwDeviceId = m_dwDeviceId;
    m_clsSelectDeviceDlg.m_hkey = m_hkey;

    if (m_clsSelectDeviceDlg.DoModal() == IDOK)
    {
        m_dwDeviceId = m_clsSelectDeviceDlg.m_dwDeviceId;
        m_ctrlComboDPA.ResetContent();
        InitDpaComboBoxText();
    }
}
