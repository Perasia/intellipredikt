// J2534AutoConfigWindow.cpp : implementation file
//

#include "stdafx.h"
#include "CaymanConfig.h"
#include "J2534AutoConfigWindow.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJ2534AutoConfigWindow dialog


CJ2534AutoConfigWindow::CJ2534AutoConfigWindow(CWnd* pParent /*=NULL*/)
    : CDialog(CJ2534AutoConfigWindow::IDD, pParent)
{
    //{{AFX_DATA_INIT(CJ2534AutoConfigWindow)
        // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
}


void CJ2534AutoConfigWindow::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CJ2534AutoConfigWindow)
    DDX_Control(pDX, IDC_EDIT1, m_hText);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJ2534AutoConfigWindow, CDialog)
    //{{AFX_MSG_MAP(CJ2534AutoConfigWindow)
        // NOTE: the ClassWizard will add message map macros here
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJ2534AutoConfigWindow message handlers
