#if !defined(AFX_LOGCONFIG_H__07F1D41A_9EE3_4FED_A477_889AE4DAF399__INCLUDED_)
#define AFX_LOGCONFIG_H__07F1D41A_9EE3_4FED_A477_889AE4DAF399__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogConfig dialog

class CLogConfig : public CDialog
{
// Construction
public:
    CLogConfig(CWnd* pParent = NULL);   // standard constructor
    unsigned long m_ulLogging;
    int iLanguage;

// Dialog Data
    //{{AFX_DATA(CLogConfig)
    enum { IDD = IDD_LOG_CONFIG };
    CButton m_clLoggingMethod;
    CButton m_clLoggingType;
    CButton m_clCancel;
    CButton m_clOK;
    CButton m_clData;
    CButton m_clTrace;
    CButton m_clError;
    CButton m_clOverwrite;
    CButton m_clAppend;
    //}}AFX_DATA


// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CLogConfig)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CLogConfig)
    virtual void OnOK();
    virtual BOOL OnInitDialog();
    afx_msg void OnRadio1();
    afx_msg void OnRadio2();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGCONFIG_H__07F1D41A_9EE3_4FED_A477_889AE4DAF399__INCLUDED_)
