/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: SWISO15765.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support SWSWISO15765 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "SWISO15765.h"

void fnWriteLogMsg(char * chFileName, char * chFunctionName, unsigned long MsgType, char * chMsg, ...);

//-----------------------------------------------------------------------------
//  Function Name   : CSWISO15765
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CSWISO15765 class
//-----------------------------------------------------------------------------
CSWISO15765::CSWISO15765(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog) : CProtocolBase(pclsDevice, pclsDebugLog, pclsDataLog)
{
    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "CSWISO15765()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;
    m_bLoopback = false;       
	m_bErrorReporting = false;
    m_ucNormalToHighSpeed = 0;
    m_bBackToNormalSpeed = false;
    m_ulHSDataRate = 83333;
    m_ulSWCAN_SpeedChange_Enable = 0;
    m_ulSWCAN_Res_Switch = 0;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "CSWISO15765()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CSWISO15765
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CSWISO15765 class
//-----------------------------------------------------------------------------
CSWISO15765::~CSWISO15765()
{
    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "~CSWISO15765()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "~CSWISO15765()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vConnect(J2534_PROTOCOL enProtocolID,
                                unsigned long   ulFlags,
                                unsigned long   ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                                LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    m_ulCANHeaderSupport = ((ulFlags >> 11) & 0x01);
    m_ulCANIDtype = ((ulFlags >> 8) & 0x01);
    m_ulCANExtAddr = ((ulFlags >> 7) & 0x01);
    
    if (m_ulCANIDtype == 0) // Standard CAN
    {
        m_ulCANHeaderBits = SWISO15765_HEADER_SIZE;
//      ulFlags = 0;
    }
    else  // Extended CAN
    {       
        m_ulCANHeaderBits = SWISO15765_EXTENDED_HEADER_SIZE;
//      ulFlags = 1;
    }

    if(enProtocolID == SW_ISO15765_PS || enProtocolID == SW_CAN_ISO15765_CH1)
    {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        if ((ulBaudRate != SWISOCAN_DATA_RATE_DEFAULT) && ((ulBaudRate != SWISOCAN_DATA_RATE_MEDIUM)))
        {
            return(J2534_ERR_INVALID_BAUDRATE);
        }
#endif
#ifdef J2534_0305
        ulBaudRate = SWISOCAN_DATA_RATE_MEDIUM;
#endif
        m_ulDataRate = ulBaudRate;
        m_enSWISO15765Protocol = enProtocolID;
        
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate,
                                                OnSWISO15765RxMessage, this))
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    m_ulBlockSize = 0;
    m_ulBlockSizeTx = 0xFFFF;
    m_ulSTmin = 0;
    m_ulSTminTx = 0xFFFF;
    m_ulWftMax = 0;
    m_ulPPSS = 0;
    m_bJ1962Pins = false;
	m_enCANMixedFormat = J2534_CAN_MIXED_FORMAT_OFF;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vReadMsgs(PASSTHRU_MSG  *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

/*
    if (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
        return(J2534_ERR_NO_FLOW_CONTROL);
    }
*/  
    // Read using the generic Read.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        if (enJ2534Error == J2534_ERR_BUFFER_EMPTY)
        {
            if ((m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF) && 
                (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet()))))
            {
                // Write to Log File.
                WriteLogMsg("SWISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
                return(J2534_ERR_NO_FLOW_CONTROL);
            }
        }
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vWriteMsgs(PASSTHRU_MSG *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
		if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
		{
			// Check Msg. Protocol ID.
			if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enSWISO15765Protocol)
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}

			if (((pstPassThruMsg + ulIdx1)->ulDataSize > SWISO15765_MSG_SIZE_MAX_SF) && 
				((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
				return(J2534_ERR_NO_FLOW_CONTROL);
			}

			// Check if msg. format is valid.
			if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
	#ifdef J2534_0305
			if (m_ulCANExtAddr)
			{
				(pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x80;
			}
	#endif
			if((pstPassThruMsg + ulIdx1)->ulDataSize > (SWISO15765_MSG_SIZE_MIN + 7)) //Check Only for long messages
			{
				if(!m_pclsFilterMsg->DoesFlowControlFilterMatch(pstPassThruMsg + ulIdx1))
				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
					return(J2534_ERR_NO_FLOW_CONTROL);
				}
			}
		}
		else
		{
			if ((m_enSWISO15765Protocol == SW_ISO15765_PS && (pstPassThruMsg + ulIdx1)->ulProtocolID == SW_CAN_PS) ||
				(m_enSWISO15765Protocol == SW_CAN_ISO15765_CH1 && (pstPassThruMsg + ulIdx1)->ulProtocolID == SW_CAN_CAN_CH1))
			{
			}
			else
			{
				// Check Msg. Protocol ID.
				if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enSWISO15765Protocol)
				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
				}
			}

			if (((pstPassThruMsg + ulIdx1)->ulProtocolID == SW_ISO15765_PS) ||
				((pstPassThruMsg + ulIdx1)->ulProtocolID == SW_CAN_ISO15765_CH1))
			{
				if (((pstPassThruMsg + ulIdx1)->ulDataSize > SWISO15765_MSG_SIZE_MAX_SF) && 
					((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
					return(J2534_ERR_NO_FLOW_CONTROL);
				}

				// Check if msg. format is valid.
				if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
					return(J2534_ERR_INVALID_MSG);
				}
		#ifdef J2534_0305
				if (m_ulCANExtAddr)
				{
					(pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x80;
				}
		#endif
				if((pstPassThruMsg + ulIdx1)->ulDataSize > (SWISO15765_MSG_SIZE_MIN + 7)) //Check Only for long messages
				{
					if(!m_pclsFilterMsg->DoesFlowControlFilterMatch(pstPassThruMsg + ulIdx1))
					{
						// Write to Log File.
						WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
						return(J2534_ERR_NO_FLOW_CONTROL);
					}
				}
			}
			else
			{
				// Check if msg. format is valid.
				if (!IsCANMsgValid((pstPassThruMsg + ulIdx1)))
				{
					// Write to Log File.
					WriteLogMsg("SWCAN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
					return(J2534_ERR_INVALID_MSG);
				}
			}
		}
    }
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vStartPeriodicMsg(PASSTHRU_MSG  *pstPassThruMsg,
                                        unsigned long   *pulMsgID,
                                        unsigned long   ulTimeInterval
	)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

	if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
	{
		// Check Msg. Protocol ID.
		if (pstPassThruMsg->ulProtocolID != m_enSWISO15765Protocol)
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}
    
		if ((pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX_SF))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
		// Check if msg. format is valid.
		if (!IsMsgValid(pstPassThruMsg))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

	#ifdef J2534_0305
		if (m_ulCANExtAddr)
		{
			pstPassThruMsg->ulTxFlags |= 0x80;
		}
	#endif
	}
	else
	{
		if ((m_enSWISO15765Protocol == SW_ISO15765_PS && pstPassThruMsg->ulProtocolID == SW_CAN_PS) ||
		    (m_enSWISO15765Protocol == SW_CAN_ISO15765_CH1 && pstPassThruMsg->ulProtocolID == SW_CAN_CAN_CH1))
		{
		}
		else
		{
			// Check Msg. Protocol ID.
			if (pstPassThruMsg->ulProtocolID != m_enSWISO15765Protocol)
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}
		}

		if ((pstPassThruMsg->ulProtocolID == SW_ISO15765_PS) ||
			(pstPassThruMsg->ulProtocolID == SW_CAN_ISO15765_CH1))
		{
			if ((pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX_SF))
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
			// Check if msg. format is valid.
			if (!IsMsgValid(pstPassThruMsg))
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}

		#ifdef J2534_0305
			if (m_ulCANExtAddr)
			{
				pstPassThruMsg->ulTxFlags |= 0x80;
			}
		#endif
		}
		else
		{
			// Check if msg. format is valid.
			if (!IsCANMsgValid(pstPassThruMsg))
			{
				// Write to Log File.
				WriteLogMsg("SWCAN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
		}
	}
    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
	 ,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CSWISO15765::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::vStartMsgFilter(J2534_FILTER   enumFilterType,
                                       PASSTHRU_MSG     *pstMask,
                                       PASSTHRU_MSG     *pstPattern,
                                       PASSTHRU_MSG     *pstFlowControl,
                                       unsigned long    *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
	if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
	{
		// Check Filter Type.
		if (enumFilterType != J2534_FILTER_FLOW_CONTROL)
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
			SetLastErrorText("SWISO15765 : Invalid FilterType value");
            return(J2534_ERR_FAILED);
		}
		// Check Msg. Protocol ID.
		if ((pstMask->ulProtocolID != m_enSWISO15765Protocol) ||
			(pstPattern->ulProtocolID != m_enSWISO15765Protocol) || 
			(pstFlowControl->ulProtocolID != m_enSWISO15765Protocol))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}
	}
	else
	{
		if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
		{			
			// Check Msg. Protocol ID.
			if ((pstMask->ulProtocolID != m_enSWISO15765Protocol) ||
				(pstPattern->ulProtocolID != m_enSWISO15765Protocol) || 
				(pstFlowControl->ulProtocolID != m_enSWISO15765Protocol))
			{
				// Write to Log File.
				WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}
		}
		else
		{
			if ((m_enSWISO15765Protocol == SW_ISO15765_PS && pstMask->ulProtocolID == SW_CAN_PS) ||
				(m_enSWISO15765Protocol == SW_CAN_ISO15765_CH1 && pstMask->ulProtocolID == SW_CAN_CAN_CH1))
			{
			}
			else
			{
				// Check Msg. Protocol ID.
//				if ((pstMask->ulProtocolID != m_enSWISO15765Protocol))
//				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
//				}
			}
			if ((m_enSWISO15765Protocol == SW_ISO15765_PS && pstPattern->ulProtocolID == SW_CAN_PS) ||
				(m_enSWISO15765Protocol == SW_CAN_ISO15765_CH1 && pstPattern->ulProtocolID == SW_CAN_CAN_CH1))
			{
			}
			else
			{
				// Check Msg. Protocol ID.
//				if ((pstPattern->ulProtocolID != m_enSWISO15765Protocol))
//				{
					// Write to Log File.
					WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
//				}
			}
		}
	}
#endif
#ifdef J2534_0305
    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enSWISO15765Protocol) ||
        (pstPattern->ulProtocolID != m_enSWISO15765Protocol))
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
#endif
	if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
	{
		// Check Filter Type.

		// NOTE : In SWISO15765, if pstFlowControl is not NULL then check for  
		//        J2534_ERR_MSG_PROTOCOL_ID.

		// NOTE : In SWISO15765, pass the pstFlowControl through 
		//        IsMsgValid().
		// Check if msg. format is valid.
		if (!IsMsgValid(pstPattern, true))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

		if (!IsMsgValid(pstMask, true))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

		if (!IsMsgValid(pstFlowControl, true))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}
	else
	{
		if (!IsCANMsgValid(pstPattern, true))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "470 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

		if (!IsCANMsgValid(pstMask, true))
		{
			// Write to Log File.
			WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "477 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}

    // NOTE : In SWISO15765, check to see that the mask of 4 or 5 bytes are
    //        all 0xFF. The size of pstMask, pstPattern and pstFlowControl
    //        should be 4 or 5 bytes depanding on extended addressing or not
    //        respectively.

    // NOTE : In SWISO15765, if the filter type is PASS or BLOCK return error.
    //        In other words only allow FLOW_CONTROL filter type.

#ifdef J2534_0305
    if (m_ulCANExtAddr)
    {
        pstMask->ulTxFlags |= 0x80;
        pstPattern->ulTxFlags |= 0x80;
        pstFlowControl->ulTxFlags |= 0x80;
    }
#endif
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::vIoctl(J2534IOCTLID enumIoctlID,
                              void *pInput,
                              void *pOutput)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;
    PASSTHRU_MSG stPassThruMsg;
    unsigned long ulGetTimestamp;
    
    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
    // IoctlID values
    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration
        
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;
        
    case SET_CONFIG:            // Set configuration
        
        enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);

        break;
        
    case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue
        
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
            m_pclsTxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
        
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
            m_pclsRxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
        if (m_pclsPeriodicMsg != NULL)
        {
            delete m_pclsPeriodicMsg;
            m_pclsPeriodicMsg = NULL;
        }
        
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
        if (m_pclsFilterMsg != NULL)
        {
            delete m_pclsFilterMsg;
            m_pclsFilterMsg = NULL;
        }
        
        break;

    case SW_CAN_HS:
        {
            if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        NULL,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }   
            if (!m_bIsRunningHighSpeed)
            {
                memset(&stPassThruMsg, 0, sizeof(PASSTHRU_MSG));
                stPassThruMsg.ulProtocolID = m_enSWISO15765Protocol;
                stPassThruMsg.ulRxStatus = SWISO15765_CAN_HS_RX;   
                if (CProtocolBase::vIoctl(GET_TIMESTAMP, NULL, &ulGetTimestamp) == J2534_STATUS_NOERROR)
                {
                    stPassThruMsg.ulTimeStamp = ulGetTimestamp * 1000;
                }
                OnSWISO15765RxMessage(&stPassThruMsg, this);
            }
            m_bIsRunningHighSpeed = true;   
        }
        break;
    
    case SW_CAN_NS:
        {
            if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        NULL,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }   
            if (m_bIsRunningHighSpeed)
            {
                memset(&stPassThruMsg, 0, sizeof(PASSTHRU_MSG));
                stPassThruMsg.ulProtocolID = m_enSWISO15765Protocol;
                stPassThruMsg.ulRxStatus = SWISO15765_CAN_NS_RX;     
                if (CProtocolBase::vIoctl(GET_TIMESTAMP, NULL, &ulGetTimestamp) == J2534_STATUS_NOERROR)
                {
                    stPassThruMsg.ulTimeStamp = ulGetTimestamp * 1000;
                }
                OnSWISO15765RxMessage(&stPassThruMsg, this);
            }
            m_bIsRunningHighSpeed = false;   
        }
        break;

#ifdef J2534_DEVICE_NETBRIDGE
	case CAN_SET_BTR:
        {
            if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;   
    case CAN_SET_ERROR_REPORTING:
        if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
            enumJ2534Error = J2534_ERR_PIN_INVALID;
        else
		enumJ2534Error=SetErrorReporting((unsigned char *)pInput);
		break;
#endif

    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }
    
    
    // Write to Log File.
    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    WriteLogMsg("SWISO15765.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);

    // Write to Log File.
    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
} 

UINT CSWISO15765ChangeSpeedThread(LPVOID pVoid)
{
    CSWISO15765                 *pclsSWISO15765;
    pclsSWISO15765 = (CSWISO15765 *) pVoid;
    if (pclsSWISO15765->m_ucNormalToHighSpeed == 2)
    {
        pclsSWISO15765->vIoctl(SW_CAN_HS, NULL, NULL);
        pclsSWISO15765->m_ucNormalToHighSpeed = 0;
        pclsSWISO15765->m_bBackToNormalSpeed = false;
    }
    else if (pclsSWISO15765->m_bBackToNormalSpeed == true)
    {
        pclsSWISO15765->vIoctl(SW_CAN_NS, NULL, NULL);
        pclsSWISO15765->m_ucNormalToHighSpeed = 0;
        pclsSWISO15765->m_bBackToNormalSpeed = false;
    }
    return 1;
}

//-----------------------------------------------------------------------------
//  Function Name   : OnSWISO15765Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    SWISO15765 messages.
//-----------------------------------------------------------------------------
void OnSWISO15765RxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CSWISO15765                 *pclsSWISO15765;
    FILTERMSG_CONFORM_REQ   stConformReq;
    unsigned long ulSavedDataSize = pstPassThruMsg->ulDataSize;
    unsigned long   i, j; 
    char            szChannelPins[MAX_PATH];
    char            szProtocol[MAX_PATH];
    unsigned long   ulRxTx;
    unsigned long   ulTimestamp;
    char            szID[MAX_PATH];
    char            szData[MAX_PATH];

    pclsSWISO15765 = (CSWISO15765 *) pVoid;

    // Check for NULL pointer.
    if (pclsSWISO15765 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    if ((pclsSWISO15765->m_enSWISO15765Protocol == SW_ISO15765_PS) && !pclsSWISO15765->m_bJ1962Pins)
        return;                      

    memset(szChannelPins,0,MAX_PATH);
    memset(szProtocol,0,MAX_PATH);
    ulRxTx = 0;
    ulTimestamp = 0;
    memset(szID,0,MAX_PATH);
    memset(szData,0,MAX_PATH);

    // Data Logging Protocol and its Channel/Pins
    switch(pstPassThruMsg->ulProtocolID)
    {
    case SW_CAN_PS:
        if (pclsSWISO15765->m_bJ1962Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1962 Pins 0x%04X", pclsSWISO15765->m_ulPPSS);
        }
        sprintf_s(szProtocol,"SWCAN");
        break;
    case SW_ISO15765_PS:
        if (pclsSWISO15765->m_bJ1962Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1962 Pins 0x%04X", pclsSWISO15765->m_ulPPSS);
        }
        sprintf_s(szProtocol,"SWISO15765");
        break; 
    case SW_CAN_CAN_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"SWCAN");
        break;
    case SW_CAN_ISO15765_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"SWISO15765");
        break;
    default:
        break;
    }

    // Data Logging Rx/Tx
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        ulRxTx = DATALOG_TX;
    }
    else
    {
        ulRxTx = DATALOG_RX;
    }        

    if (!(pstPassThruMsg->ulProtocolID == SW_ISO15765_PS || pstPassThruMsg->ulProtocolID == SW_CAN_ISO15765_CH1))
	{
        if (pclsSWISO15765->m_pclsFilterMsg != NULL)
	    {
            // See if any received CAN segmented message is really a transmitted message
            if (pclsSWISO15765->m_pclsFilterMsg->DoesFlowControlFilterMatchRxFCID(pstPassThruMsg))
            {
                if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE) == 0)
                {
                    ulRxTx = DATALOG_TX;
                }
            }
        }
    }

    // Data Logging timestamp
    ulTimestamp = pstPassThruMsg->ulTimeStamp;

    // Data Logging ID
    if(pstPassThruMsg->ulDataSize > 0)
    {
        j = 0;

        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == J2534_RX_FLAGBIT_CAN29BIT)
        {
            for(i = 0; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }
        else
        {
            for(i = 2; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }

        j = 0;

        for(i = 4; i < pstPassThruMsg->ulDataSize && i < 15; i++)
        {
            j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
        }

        if(i >= 15)
            j += sprintf_s(szData + j, sizeof(szData)-j, "...");
    }

    if (((pstPassThruMsg->ulRxStatus & 2) == 0) &&
        ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_ISO15765_TIMEOUT) == 0) &&
        ((pstPassThruMsg->ulRxStatus & SWISO15765_CAN_NS_RX) == 0) &&
        ((pstPassThruMsg->ulRxStatus & SWISO15765_CAN_HS_RX) == 0))
    {
        // Write to Data Log File.
        CProtocolBase::WriteDataLogMsg(szChannelPins, szProtocol, ulRxTx, ulTimestamp, szID, szData);
    }  

    // Check if this is a SWISO15765_CAN_NS_RX Indication.
    if (pstPassThruMsg->ulRxStatus & SWISO15765_CAN_NS_RX)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CSWISO15765.cpp", "OnSWISO15765Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "SWISO15765_CAN_NS_RX");
        }

        // Enqueue to Circ Buffer.
        pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if this is a SWISO15765_CAN_HS_RX Indication.
    if (pstPassThruMsg->ulRxStatus & SWISO15765_CAN_HS_RX)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CSWISO15765.cpp", "OnSWISO15765Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "SWISO15765_CAN_HS_RX");
        }

        // Enqueue to Circ Buffer.
        pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if the message is valid as per the Connect Flag is set.
    if ((pclsSWISO15765->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (pclsSWISO15765->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return;
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return;
            }
        }
    }

	if (pstPassThruMsg->ulProtocolID == SW_ISO15765_PS || pstPassThruMsg->ulProtocolID == SW_CAN_ISO15765_CH1)
	{
#ifdef J2534_DEVICE_NETBRIDGE
        // ISO15765 timeout
        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_ISO15765_TIMEOUT) && (pclsSWISO15765->m_bErrorReporting))
        {
 			// Write to Log File.
			CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "SWISO15765 Timeout");
			// Write to Log File.
			CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulTxFlags 0x%X", pstPassThruMsg->ulTxFlags);
			// Write to Log File.
			CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulDataSize 0x%X", pstPassThruMsg->ulDataSize);
			// Enqueue to Circ Buffer.
			pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
            return;
        }
#endif
		if (pclsSWISO15765->m_ulSWCAN_SpeedChange_Enable == 1)
		{
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
			{
				if ((pstPassThruMsg->ucData[2] == 0x01) &&
					(pstPassThruMsg->ucData[3] == 0x01) &&
					(pstPassThruMsg->ucData[4] == 0xFE) &&
                    ((pstPassThruMsg->ulRxStatus & 0x80) == 0x80))
				{
					if (//(pstPassThruMsg->ulDataSize == 8) && 
						//(pstPassThruMsg->ucData[5] == 0x02) &&
						(pstPassThruMsg->ucData[5] == 0xA5))
					{
						if (pstPassThruMsg->ucData[6] == 0x02 && pstPassThruMsg->ulDataSize == 7)
						{
							pclsSWISO15765->m_ucNormalToHighSpeed = 1;
						}
						else if (pstPassThruMsg->ucData[6] == 0x03 && pstPassThruMsg->ulDataSize == 7)
						{
                            if (pclsSWISO15765->m_ucNormalToHighSpeed == 1)
                            {
                                pclsSWISO15765->m_ucNormalToHighSpeed = 2;
                                AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
						    }
                        }
					}
                    else if (pstPassThruMsg->ucData[5] == 0x20 && pstPassThruMsg->ulDataSize == 6)
                    {
                        pclsSWISO15765->m_bBackToNormalSpeed = true;
                        AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
                    }
				}
				else if ((pstPassThruMsg->ucData[2] == 0x02) &&
					(pstPassThruMsg->ucData[3] >= 0x41) &&
					(pstPassThruMsg->ucData[3] <= 0x5F) &&
                    ((pstPassThruMsg->ulRxStatus & 0x80) == 0))
				{
					if (//(pstPassThruMsg->ulDataSize == 7) && 
						//(pstPassThruMsg->ucData[4] == 0x02) &&
						(pstPassThruMsg->ucData[4] == 0xA5))
					{
						if (pstPassThruMsg->ucData[5] == 0x02 && pstPassThruMsg->ulDataSize == 6)
						{
							pclsSWISO15765->m_ucNormalToHighSpeed = 1;
						}
						else if (pstPassThruMsg->ucData[5] == 0x03 && pstPassThruMsg->ulDataSize == 6)
						{
                            if (pclsSWISO15765->m_ucNormalToHighSpeed == 1)
                            {
                                pclsSWISO15765->m_ucNormalToHighSpeed = 2;
                                AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
						    }
                        }
					} 
                    else if (pstPassThruMsg->ucData[4] == 0x20 && pstPassThruMsg->ulDataSize == 5)
                    {
                        pclsSWISO15765->m_bBackToNormalSpeed = true;
                        AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
                    }
				}
			}
		}
        
		// Check if this is a Loopback message.
		if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
		{
			// Write to Log File.
			CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765Rx()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");

			//if (pclsSWISO15765->m_bLoopback == false)
			//	return;

			pstPassThruMsg->ulDataSize = ulSavedDataSize;  // Restore ulDataSize
			pstPassThruMsg->ulExtraDataIndex = pstPassThruMsg->ulDataSize;

            if (pclsSWISO15765->m_bLoopback)
            {
                pstPassThruMsg->ulRxStatus &= (0xFFFFFFFF - 0x08);
			    // Enqueue to Circ Buffer. log data
			    pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
                pstPassThruMsg->ulRxStatus |= 0x08;
            }
			//return;
		}
        // Check if this is a Loopback message.
		if (pstPassThruMsg->ulRxStatus & (0x08 | J2534_RX_FLAGBIT_MSGTYPE))
		{
			// Write to Log File.
			CProtocolBase::WriteLogMsg("CSWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx Done CALLBACK");

	//      if (pclsSWISO15765->m_bLoopback == false)
	//          return;

			if (pstPassThruMsg->ulTxFlags & 0x80)
				pstPassThruMsg->ulDataSize = 5;
			else
				pstPassThruMsg->ulDataSize = 4;

			pstPassThruMsg->ulExtraDataIndex = 0;//pstPassThruMsg->ulDataSize;

			// Write to Log File.
			CProtocolBase::WriteLogMsg("SWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulTxFlags 0x%X", pstPassThruMsg->ulTxFlags);
			// Write to Log File.
			CProtocolBase::WriteLogMsg("SWISO15765.cpp", "OnSWISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulDataSize 0x%X", pstPassThruMsg->ulDataSize);

			// Enqueue to Circ Buffer. log data
			pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
			return;
		}
    
		fnWriteLogMsg("SWISO15765.cpp", "OnSWISO15765RxMessage() 889", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
		// Apply Filters and see if msg. is required.
		if (pclsSWISO15765->m_pclsFilterMsg != NULL)
		{
			stConformReq.bReqPass = false;
			stConformReq.bReqBlock = false;
			stConformReq.bReqFlowControl = true;
			if (pclsSWISO15765->IsMsgValidRx(pstPassThruMsg) &&
                pclsSWISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			{

				pstPassThruMsg->ulDataSize = ulSavedDataSize; 
				pstPassThruMsg->ulExtraDataIndex = ulSavedDataSize;

				// Enqueue to Circ Buffer. log data
				pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
			}
		}
	#ifdef J2534_0305
		else
        {
            if (pclsSWISO15765->IsMsgValidRx(pstPassThruMsg))
            {
			    // Enqueue to Circ Buffer.
			    pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
            }
        }
	#endif
	}
	else
	{
        if (pclsSWISO15765->m_enCANMixedFormat != J2534_CAN_MIXED_FORMAT_OFF)
		{
		    if (pclsSWISO15765->m_ulSWCAN_SpeedChange_Enable == 1)
		    {
                if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
			    {
				    if ((pstPassThruMsg->ucData[2] == 0x01) &&
					    (pstPassThruMsg->ucData[3] == 0x01) &&
					    (pstPassThruMsg->ucData[4] == 0xFE))
				    {
					    if (//(pstPassThruMsg->ulDataSize == 8) && 
						    (pstPassThruMsg->ucData[5] == 0x02) &&
						    (pstPassThruMsg->ucData[6] == 0xA5))
					    {
						    if (pstPassThruMsg->ucData[7] == 0x02 && pstPassThruMsg->ulDataSize >= 8)
						    {
							    pclsSWISO15765->m_ucNormalToHighSpeed = 1;
						    }
						    else if (pstPassThruMsg->ucData[7] == 0x03 && pstPassThruMsg->ulDataSize >= 8)
						    {
                                if (pclsSWISO15765->m_ucNormalToHighSpeed == 1)
                                {
                                    pclsSWISO15765->m_ucNormalToHighSpeed = 2;
                                    AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
						        }
                            }
					    }          
                        else if ((pstPassThruMsg->ucData[5] == 0x01) &&
                            (pstPassThruMsg->ucData[6] == 0x20) && pstPassThruMsg->ulDataSize >= 7)
                        {
                            pclsSWISO15765->m_bBackToNormalSpeed = true;
                            AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
                        }
				    }
				    else if ((pstPassThruMsg->ucData[2] == 0x02) &&
					    (pstPassThruMsg->ucData[3] >= 0x41) &&
					    (pstPassThruMsg->ucData[3] <= 0x5F))
				    {
					    if (//(pstPassThruMsg->ulDataSize == 7) && 
						    (pstPassThruMsg->ucData[4] == 0x02) &&
						    (pstPassThruMsg->ucData[5] == 0xA5))
					    {
						    if (pstPassThruMsg->ucData[6] == 0x02 && pstPassThruMsg->ulDataSize >= 7)
						    {
							    pclsSWISO15765->m_ucNormalToHighSpeed = 1;
						    }
						    else if (pstPassThruMsg->ucData[6] == 0x03 && pstPassThruMsg->ulDataSize >= 7)
						    {
                                if (pclsSWISO15765->m_ucNormalToHighSpeed == 1)
                                {
                                    pclsSWISO15765->m_ucNormalToHighSpeed = 2;
                                    AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
						        }
                            }
					    }
                        else if ((pstPassThruMsg->ucData[4] == 0x01) &&
                            (pstPassThruMsg->ucData[5] == 0x20) && pstPassThruMsg->ulDataSize >= 6)
                        {
                            pclsSWISO15765->m_bBackToNormalSpeed = true;
                            AfxBeginThread((AFX_THREADPROC)CSWISO15765ChangeSpeedThread, pclsSWISO15765, THREAD_PRIORITY_HIGHEST);
                        }
				    }
			    }
		    }
            if (pclsSWISO15765->m_pclsFilterMsg != NULL)
		    {
                // See if any received CAN segmented message is really a transmitted message
                if (pclsSWISO15765->m_pclsFilterMsg->DoesFlowControlFilterMatchRxFCID(pstPassThruMsg))
                {
                    // Write to Log File.
			        CProtocolBase::WriteLogMsg("CSWCAN.cpp", "OnSWCANRx()", DEBUGLOG_TYPE_COMMENT, "DoesFlowControlFilterMatchRxFCID Yes");
                    if (pclsSWISO15765->m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_ON &&
                        (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE) == 0)
                    {
			            CProtocolBase::WriteLogMsg("CSWCAN.cpp", "OnSWCANRx()", DEBUGLOG_TYPE_COMMENT, "Thrown out");
                        return;
                    }
                    else
                    {
                        pstPassThruMsg->ulRxStatus |= J2534_RX_FLAGBIT_MSGTYPE;
                        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
                        {
                            pstPassThruMsg->ulTxFlags |= J2534_TX_FLAGBIT_CAN29BIT;
                        }
                        if ((pstPassThruMsg->ulRxStatus & (1 << 16)) != 0)
                        {
                            pstPassThruMsg->ulTxFlags |= (1 << 10);
                        }
			            CProtocolBase::WriteLogMsg("CSWCAN.cpp", "OnSWCANRx()", DEBUGLOG_TYPE_COMMENT, "Changed to Tx CALLBACK");
                    }
                }
            }

		    // Check if this is a Loopback message.
		    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
		    {
			    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
			    {
				    CProtocolBase::m_pclsLog->Write("CSWCAN.cpp", "OnSWCANRx()", 
								     DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
			    }

			    if (pclsSWISO15765->m_bLoopback == false)
				    return;
			    // Enqueue to Circ Buffer.
			    pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
						    pstPassThruMsg->ulDataSize));
			    return;
		    }
		    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
		    {
			    CProtocolBase::m_pclsLog->Write("CSWCAN.cpp", "OnSWCANRx()", 
							     DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
		    }
		    // Apply Filters and see if msg. is required.
		    if (pclsSWISO15765->m_pclsFilterMsg != NULL)
		    {
                stConformReq.bReqPass = false;
			    stConformReq.bReqBlock = false;
			    stConformReq.bReqFlowControl = true;
			    if (pclsSWISO15765->IsCANMsgValidRx(pstPassThruMsg) &&
                    pclsSWISO15765->m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_ON &&
                    pclsSWISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			    {
				    // If CAN Mixed Format On and rx message conforms to Flow Control Filter, then ignore
                    // Write to Log File.
				    CProtocolBase::WriteLogMsg("CSWCAN.cpp", "OnSWCANRx()", DEBUGLOG_TYPE_COMMENT, "Rx Message Ignored");
				    return;
			    }
			    stConformReq.bReqPass = true;
			    stConformReq.bReqBlock = true;      
			    stConformReq.bReqFlowControl = false;
			    if (pclsSWISO15765->IsCANMsgValidRx(pstPassThruMsg) &&
                    pclsSWISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			    {
				    // Enqueue to Circ Buffer.
				    pclsSWISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
						    pstPassThruMsg->ulDataSize));
			    }
		    }
        }
	}

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CSWISO15765::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    if ((pstPassThruMsg->ulTxFlags & 0x80) == 0)
    {
        if ((pstPassThruMsg->ulDataSize < SWISO15765_MSG_SIZE_MIN) || 
            (pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX))
        {
            return(false);
        }
    }
    else if ((pstPassThruMsg->ulTxFlags & 0x80) != 0)
    {
        if ((pstPassThruMsg->ulDataSize < SWISO15765_MSG_SIZE_MIN+1) || 
            (pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX+1))
        {
            return(false);
        }
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsCANMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CSWISO15765::IsCANMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    if ((pstPassThruMsg->ulDataSize < SWISO15765_CAN_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > SWISO15765_CAN_MSG_SIZE_MAX))
    {
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}                   

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CSWISO15765::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulRxStatus & 0x80) == 0)
    {
        if ((pstPassThruMsg->ulDataSize < SWISO15765_MSG_SIZE_MIN) || 
            (pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX))
        {
            return(false);
        }
    }
    else if ((pstPassThruMsg->ulRxStatus & 0x80) != 0)
    {
        if ((pstPassThruMsg->ulDataSize < SWISO15765_MSG_SIZE_MIN+1) || 
            (pstPassThruMsg->ulDataSize > SWISO15765_MSG_SIZE_MAX+1))
        {
            return(false);
        }
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsCANMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CSWISO15765::IsCANMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < SWISO15765_CAN_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > SWISO15765_CAN_MSG_SIZE_MAX))
    {
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("SWISO15765.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("SWISO15765 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                pSconfig->ulValue = (unsigned long) m_fSamplePoint;

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                pSconfig->ulValue = (unsigned long) m_fJumpWidth;

                break;

            case ISO15765_BS:       // Block Size

                pSconfig->ulValue = m_ulBlockSize;

                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case BS_TX:             // Block Size Tx

                pSconfig->ulValue = m_ulBlockSizeTx;

                break;
#endif
            case ISO15765_STMIN:        // STmin

                pSconfig->ulValue = m_ulSTmin;

                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case STMIN_TX:              // STminTx

                pSconfig->ulValue = m_ulSTminTx;

                break;
#ifdef J2534_0500
            case ISO15765_WAIT_LIMIT:
#endif
#ifdef J2534_0404
			case ISO15765_WFT_MAX:
#endif
                pSconfig->ulValue = m_ulWftMax;

                break;

            case CAN_MIXED_FORMAT:

                pSconfig->ulValue = m_enCANMixedFormat;

                break;
#endif
            case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS;

                if (m_enSWISO15765Protocol != SW_ISO15765_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case SWCAN_HS_DATA_RATE:    // Sync Jump Width

                pSconfig->ulValue = m_ulHSDataRate;

                break;

            case SWCAN_SPEEDCHANGE_ENABLE:  // Sync Jump Width

                pSconfig->ulValue = m_ulSWCAN_SpeedChange_Enable;

                break;

            case SWCAN_RES_SWITCH:  // Sync Jump Width

                pSconfig->ulValue = m_ulSWCAN_Res_Switch;

                break;
            
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;
    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("SWISO15765.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("SWISO15765 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Parameter is 0x%02X", pSconfig->Parameter);
        WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = DataRate(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback
    
                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                //enumJ2534Error = SamplePoint(pSconfig->ulValue);

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;
            
            case ISO15765_BS:       // Block Size

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = BlockSize(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            case BS_TX:             // Block Size Tx


                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = BlockSizeTx(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#endif
            case ISO15765_STMIN:        // STmin
                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = STmin(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            case STMIN_TX:              // STminTx

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = STminTx(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifdef J2534_0500
			case ISO15765_WAIT_LIMIT:
#endif
#ifdef J2534_0404
			case ISO15765_WFT_MAX:
#endif
                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = WftMax(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#endif            
            case J1962_PINS:    // Sync Jump Width

                if (m_enSWISO15765Protocol != SW_ISO15765_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else if (m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                else
                enumJ2534Error = J1962Pins(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                if(enumJ2534Error == J2534_STATUS_NOERROR)
                    m_ulPPSS = pSconfig->ulValue;
                else
                    m_bJ1962Pins = false;
                break;

            case SWCAN_HS_DATA_RATE:    // Sync Jump Width

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = HSDataRate(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case SWCAN_SPEEDCHANGE_ENABLE:  // Sync Jump Width

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = SpeedChange(pSconfig->ulValue);

                break;

            case SWCAN_RES_SWITCH:  // Sync Jump Width

                if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = ResSwitch(pSconfig->ulValue);

                break;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality				
			case CAN_MIXED_FORMAT:       // CAN Mixed Format
 #ifndef J2534_DEVICE_VSI
                // CAN_MIXED_FORMAT has only been added to VSI2534 (for now) par130904
                // remove when supported add for all DG tools
 //               return(J2534_ERR_NOT_SUPPORTED);
 #endif

				if ((m_enSWISO15765Protocol == SW_ISO15765_PS) && !m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = CANMixedMode(pSconfig->ulValue);
                break;
#endif            
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate for a SWISO15765 channel.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Set DataRate to # of kbps
    if ((ulValue == SWISOCAN_DATA_RATE_MEDIUM) || 
        (ulValue == SWISOCAN_DATA_RATE_DEFAULT))
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

J2534ERROR  CSWISO15765::BlockSize(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulBlockSize = ulValue;

    return(iReturn);
}

J2534ERROR  CSWISO15765::BlockSizeTx(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulBlockSizeTx = ulValue;

    return(iReturn);
}

J2534ERROR  CSWISO15765::STmin(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulSTmin = ulValue;

    return(iReturn);
}

J2534ERROR  CSWISO15765::STminTx(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulSTminTx = ulValue;

    return(iReturn);
}

J2534ERROR  CSWISO15765::WftMax(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
#ifndef J2534_DEVICE_DPA5_EURO5
    if (ulValue != 0)
        return(J2534_ERR_NOT_SUPPORTED);
#endif
    m_ulWftMax = ulValue;
    return(iReturn);
}

J2534ERROR  CSWISO15765::HSDataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // Set DataRate to # of kbps
    if ((ulValue ==SWISOCAN_DATA_RATE_MEDIUM) || 
        (ulValue == SWISOCAN_DATA_RATE_DEFAULT))
    {
        m_ulHSDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

J2534ERROR  CSWISO15765::SpeedChange(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
//  unsigned char uchData[1];
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    
/*  if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    uchData[0] = ulValue;
*/  
    /*Send the device specific ioctl to set the loopback message*/
    //enumJ2534Error = vIOCTL(ulProtocol,LOOPBACK,uchData,NULL));
    m_ulSWCAN_SpeedChange_Enable = ulValue; 
    return(enumJ2534Error);
}

J2534ERROR  CSWISO15765::ResSwitch(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
//  unsigned char uchData[1];
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    
//  This is a HACK to just return success on any valid values cuz it allows some applications
//  to just go further although there is NO provision to support the non-default valid values 
//  on the products
//    if (ulValue != 0)
//        return(J2534_ERR_NOT_SUPPORTED);
/*  if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    uchData[0] = ulValue;
*/  
    /*Send the device specific ioctl to set the loopback message*/
    //enumJ2534Error = vIOCTL(ulProtocol,LOOPBACK,uchData,NULL));
    
    return(enumJ2534Error);
}

J2534ERROR  CSWISO15765::J1962Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x1000) || ((ulValue & 0x00FF) > 0x0010))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 4) || (ulValue == 4) ||
        ((ulValue >> 8) == 5) || (ulValue == 5) ||
        ((ulValue >> 8) == 16) || (ulValue == 16))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0100))
    {
        m_bJ1962Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

J2534ERROR  CSWISO15765::CANMixedMode(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

#ifndef J2534_DEVICE_VSI
    if (ulValue != J2534_CAN_MIXED_FORMAT_OFF && ulValue != J2534_CAN_MIXED_FORMAT_ON &&
        ulValue != J2534_CAN_MIXED_FORMAT_ALL_FRAMES)
#else
    if (ulValue != J2534_CAN_MIXED_FORMAT_OFF && ulValue != J2534_CAN_MIXED_FORMAT_ON)
#endif
        return(J2534_ERR_NOT_SUPPORTED);

	if (m_pclsPeriodicMsg != NULL)
    {
        m_pclsPeriodicMsg->StopCANPeriodic();
    }
        
    if (m_pclsFilterMsg != NULL)
    {
        m_pclsFilterMsg->StopPassAndBlockFilter();
    }

	m_pclsTxCircBuffer->ClearBuffer();
	m_pclsRxCircBuffer->ClearBuffer();

	m_enCANMixedFormat = (J2534_CAN_MIXED_FORMAT)ulValue;

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetErrorReporting
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set error level reporting.
//-----------------------------------------------------------------------------
J2534ERROR  CSWISO15765::SetErrorReporting(unsigned char *pucValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
	// Make sure pucValue is not NULL
    if (pucValue == NULL)
        return(J2534_ERR_NULLPARAMETER);

    //Valid values for <parameter> are 0-1
    if(*pucValue < 0 || *pucValue > 1)
    {
        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
        return(enumJ2534Error);
    }

    m_bErrorReporting = (*pucValue == 1) ? true : false;

    return(enumJ2534Error);
}
#ifdef J2534_0500
J2534ERROR  CSWISO15765::vLogicalConnect(J2534_PROTOCOL  enProtocolID)
{
	return J2534_STATUS_NOERROR;
}
#endif