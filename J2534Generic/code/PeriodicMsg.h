/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: PeriodicMsg.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains declaration of the CPeriodicMsg 
*              class.
* Note:
*
*******************************************************************************/


#ifndef _PERIODICMSG_H_
#define _PERIODICMSG_H_

#include "DebugLog.h"

#include "..\..\DeviceCayman\code\DeviceCayman.h"

#define PERIODICMSG_ERROR_TEXT_SIZE		120

#define PERIODICMSG_LIST_SIZE			10	// Maximum # of Periodics in the list.
#define PERIODICMSG_MSGLEN_MAX			12		

#define PERIODICMSG_SYNC_TIMEOUT		5000

typedef struct
{
	PASSTHRU_MSG	*pstMsg;
	unsigned long	ulTimeInterval;
	unsigned long	ulDevPeriodicID;
}
PERIODICMSGLIST;

// CPeriodicMsg Class
class CPeriodicMsg 
{
public:
	// Constructor
	CPeriodicMsg(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID,
			   CDebugLog * pclsDebugLog = NULL);
	~CPeriodicMsg();

	J2534ERROR			StartPeriodic(
							PASSTHRU_MSG	*pstMsg, 
							unsigned long	*pulPeriodicID,
							unsigned long	ulTimeInterval);
	J2534ERROR			StopPeriodic(
							unsigned long	ulPeriodicID);
	J2534ERROR			StopPeriodic();
	J2534ERROR			StopCANPeriodic();

	PERIODICMSGLIST		m_stPeriodicList[PERIODICMSG_LIST_SIZE+1];

private:

	HANDLE				m_hSyncAccess;
	CDebugLog			*m_pclsLog;
	CDeviceBase			*m_pclsDevice;
	unsigned long		m_ulDevChannelID;
	char				m_szLastErrorText[PERIODICMSG_ERROR_TEXT_SIZE];

	J2534ERROR			Add(PASSTHRU_MSG *pstMsg, 
							unsigned long ulTimeInterval,
							unsigned long ulDevPeriodicID,
							unsigned long *pulPeriodicID);
	J2534ERROR			Delete(unsigned long ulPeriodicID);
	bool				IsPeriodicIDValid(unsigned long ulPeriodicID);
	bool				IsListFull();
	unsigned char		GetNewPeriodicID();
};

#endif