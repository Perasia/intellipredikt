//*****************************************************************************
//					Dearborn Group Technology
//*****************************************************************************
// Project Name			: J2534 API
// File Name			: RepeatMsg.h
// Description			: This file contains declaration of the CRepeatMsg 
//						  class.
// Date					: October 22, 2014
// Version				: 1.0
// Author				: Val Kpycheva
// Revision				: 
//*****************************************************************************

#ifndef _REPEATMSG_H_
#define _REPEATMSG_H_

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

#include "PeriodicMsg.h"
#include "DebugLog.h"

#define REPEATMSG_ERROR_TEXT_SIZE		120
#define REPEATMSG_LIST_SIZE			    10	// Maximum # of Repeats in the list
#define REPEATMSG_MSGLEN_MAX			12		
#define TRANSMISSION_IN_PROGRESS        1   // Repeat message transmission in progress, filter criteria not yet met	
#define TRANSMISSION_CEASED 	        0	// Filter criteria met and repeat transmission ceased.	
#define CONDITION_CONTINUE_UNTIL        0   // Continue until a message is received that matches the pattern	
#define CONDITION_CONTINUE_WHILE        1	// Continue while every received message matches the pattern
#define INDEX_PERIODIC                  0
#define INDEX_MASK                      1
#define INDEX_PATTERN                   2
#define REPEATMSG_SYNC_TIMEOUT		    5000
#define THREAD_END_TIMEOUT		        5000

typedef struct
{
    unsigned long   ulCondition;
    unsigned long   ulStatus;
	PASSTHRU_MSG	*pstMask;
	PASSTHRU_MSG	*pstPtrn;
	unsigned long	ulRepeatID;
    bool            bStopPeriodic;
	unsigned long	ulPeriodicID;
}
REPEATMSGLIST;

// CPeriodicMsg Class
class CRepeatMsg 
{
public:
	// Constructor
	CRepeatMsg(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID, CPeriodicMsg *pclsPeriodicMsg,
			   CDebugLog * pclsDebugLog = NULL);
	~CRepeatMsg();

	J2534ERROR			StartRepeat(
                            unsigned long   ulTimeInterval,
                            unsigned long   ulCondition,
							PASSTHRU_MSG	*pstMsg, 
							PASSTHRU_MSG	*pstMask, 
							PASSTHRU_MSG	*pstPttrn, 
							unsigned long	*pulRepeatID);
	J2534ERROR			StopRepeat(
							unsigned long	*pulRepeatID);
	J2534ERROR			StopRepeat();
	J2534ERROR			QueryRepeat(
							unsigned long	ulRepeatID,
							unsigned long	*pulStatus);
	void			    CompareMsg(
                            PASSTHRU_MSG	*pstMsg);

    void                WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);
	
    REPEATMSGLIST		m_stRepeatList[REPEATMSG_LIST_SIZE+1];

	HANDLE				m_hSyncAccess;
    HANDLE              m_hThreadExit;
    HANDLE              m_hThreadExited;
    CWinThread          *m_pclsStopPeriodicThread;
    CPeriodicMsg        *m_pclsPeriodicMsg;
	CDebugLog			*m_pclsLog;
    int                 m_iProgressingRepeatMsgs; // Number of transmissions in progress
private:
	CDeviceBase			*m_pclsDevice;
	unsigned long		m_ulDevChannelID;
	char				m_szLastErrorText[PERIODICMSG_ERROR_TEXT_SIZE];

	J2534ERROR			Add( 
                            unsigned long   ulCondition,
							PASSTHRU_MSG	*pstMask, 
							PASSTHRU_MSG	*pstPttrn, 
							unsigned long	*pulRepeatID,
							unsigned long	ulPeriodicID);

	J2534ERROR			Delete(unsigned long ulPeriodicID);
	bool				IsRepeatIDValid(unsigned long ulPeriodicID);
	bool				IsListFull();
	bool				IsCAN(unsigned long ulProtocolID);
	unsigned char		GetNewRepeatID();
};

#endif