/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: ISO9141.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              ISO9141 Class.
* Note:
*
*******************************************************************************/

#ifndef _ISO9141_H_
#define _ISO9141_H_

// Constants

#define ISO9141_ERROR_TEXT_SIZE		120
#define ISO9141_DATA_RATE_DEFAULT	10400
#define ISO9141_HEADER_SIZE			0		// Number of Header bits.

#define ISO9141_P1MIN_DEFAULT		2*0
#define ISO9141_P1MAX_DEFAULT		2*20
#define ISO9141_P2MIN_DEFAULT		2*25
#define ISO9141_P2MAX_DEFAULT		2*50
#define ISO9141_P3MIN_DEFAULT		2*55
#define ISO9141_P3MAX_DEFAULT		2*5000
#define ISO9141_P4MIN_DEFAULT		2*5
#define ISO9141_P4MAX_DEFAULT		2*20

#define ISO9141_P1_DEFAULT			2*0
#define ISO9141_P2_DEFAULT			2*25
#define ISO9141_P3_DEFAULT			2*55
#define ISO9141_P4_DEFAULT			2*5

#define ISO9141_W1MIN_DEFAULT		60
#define ISO9141_W1MAX_DEFAULT		300
#define ISO9141_W2MIN_DEFAULT		5
#define ISO9141_W2MAX_DEFAULT		20
#define ISO9141_W3MIN_DEFAULT		0
#define ISO9141_W3MAX_DEFAULT		20
#define ISO9141_W4MIN_DEFAULT		25
#define ISO9141_W4MAX_DEFAULT		50
#define ISO9141_W5MIN_DEFAULT		300
#define ISO9141_W5MAX_DEFAULT		600

#define ISO9141_W1_DEFAULT			300
#define ISO9141_W2_DEFAULT			20
#define ISO9141_W3_DEFAULT			20
#define ISO9141_W4_DEFAULT			50
#define ISO9141_W5_DEFAULT			300

#define ISO9141_KEYBYTEID			0x55
#define ISO9141_TIMING_DATA_MAX		30

//#define ISO9141_CARB_INIT_ADDR		0x33
#define ISO9141_MSG_SIZE_MIN		1
#define ISO9141_MSG_SIZE_MAX		4128
#define ISO9141_TIDLE_DEFAULT	300
#define ISO9141_TINIL_DEFAULT	25
#define ISO9141_TWUP_DEFAULT	50
#define ISO9141_NO_PARITY		0
#define ISO9141_ODD_PARITY		1
#define ISO9141_EVEN_PARITY		2
#define ISO9141_8_DATABITS		0
#define ISO9141_7_DATABITS		1
#define ISO9141_5_BAUD_REG		0
#define ISO9141_5_BAUD_ISO9141	3

typedef char  unsigned  dgUint8;

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnISO9141RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CISO9141 Class
class CISO9141 : public CProtocolBase
{
public:
	CISO9141(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CISO9141();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
#ifdef J2534_0500
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulNumMsgs,
								unsigned long ulTimeout,
								unsigned long ulChannelId);

#endif
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);


public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				CarbInit(SBYTE_ARRAY *pInput, SBYTE_ARRAY *pOutput);
	J2534ERROR				Loopback(unsigned long ulValue);

	J2534ERROR				P1_Min(unsigned long ulValue),
							P1_Max(unsigned long ulValue),
							P2_Min(unsigned long ulValue),
							P2_Max(unsigned long ulValue),
							P3_Min(unsigned long ulValue),
							P3_Max(unsigned long ulValue),
							P4_Min(unsigned long ulValue),
							P4_Max(unsigned long ulValue);

	J2534ERROR				w1(unsigned long ulValue),
							w2(unsigned long ulValue),
							w3(unsigned long ulValue),
							w4(unsigned long ulValue),
							w5(unsigned long ulValue);

	unsigned short			m_usP1MIN,
							m_usP1MAX, 
							m_usP2MIN,
							m_usP2MAX,
							m_usP3MIN,
							m_usP3MAX, 
							m_usP4MIN,
							m_usP4MAX;

	unsigned short			m_usP1,
							m_usP2,
							m_usP3,
							m_usP4;

	unsigned short			m_usW1MIN,
							m_usW1MAX, 
							m_usW2MIN,
							m_usW2MAX,
							m_usW3MIN,
							m_usW3MAX, 
							m_usW4MIN,
							m_usW4MAX,
							m_usW5MIN,
							m_usW5MAX;

	unsigned short			m_usW1,
							m_usW2,
							m_usW3,
							m_usW4,
							m_usW5;

	unsigned long			m_ulChecksumFlag;

	void					Ptimedata(),
							Wtimedata();
	J2534ERROR				FastTimingSet(unsigned long ulValue, unsigned char ucType);
	unsigned short			m_usTidle,
							m_usTinil,
							m_usTwup;

	unsigned short			m_usParity,
							m_usDataBits,
							m_usFiveBaudMod;

	J2534ERROR				FastInit(PASSTHRU_MSG *pInput, PASSTHRU_MSG *pOutput);
	J2534ERROR				Parity(unsigned long ulValue);
	J2534ERROR				DataBits(unsigned long ulValue);
	J2534ERROR				FiveBaudMod(unsigned long ulValue);
	J2534ERROR		        J1962Pins(unsigned long ulValue);

public:
	unsigned char		m_ucIOdata;
	dgUint8					ISO9141Ptime[ISO9141_TIMING_DATA_MAX], 
							ISO9141Wtime[ISO9141_TIMING_DATA_MAX];
    J2534_PROTOCOL          m_enProtocol;
    bool				    m_bJ1962Pins;
};

#endif
