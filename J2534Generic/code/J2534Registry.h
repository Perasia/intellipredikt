/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534Registry.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the logging headers
* Note:
*
*******************************************************************************/

#ifndef _J2534REGISTRY_H_
#define _J2534REGISTRY_H_

#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

//Constants depending on which dll we're building
#ifdef J2534_DEVICE_CARDONE             //Cayman
#define J2534REGISTRY_VENDOR_NAME       "Cardone Industries, Inc."
#define J2534REGISTRY_TOOL_NAME         "FLASH2 All-Makes Reprogrammer"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-Flash2.csv"
#else
#ifdef J2534_DEVICE_VSI                 //VSI
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group, Inc."
#define J2534REGISTRY_TOOL_NAME         "VSI-2534"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-VSI-2534.csv"
#else
#ifdef J2534_DEVICE_KRHTWIRE            //KeylessRide
#define J2534REGISTRY_VENDOR_NAME       "KeylessRide"
#define J2534REGISTRY_TOOL_NAME         "KeylessRide HotWire"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-KeylessRide HotWire.csv"
#else
#ifdef J2534_DEVICE_DPA5                //DPA 5
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group, Inc."
#define J2534REGISTRY_TOOL_NAME         "DPA 5"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-DPA 5.csv"
#else
#ifdef J2534_DEVICE_NETBRIDGE            //netbridge aka DPA5QC
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group, Inc."
#define J2534REGISTRY_TOOL_NAME         "Netbridge"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-Netbridge.csv"
#else
#ifdef J2534_DEVICE_SOFTBRIDGE           //SoftBridge
#define J2534REGISTRY_VENDOR_NAME       "Autocom Diagnostic Partner AB"
#define J2534REGISTRY_TOOL_NAME         "SoftBridge"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-SoftBridge.csv"
#else
#ifdef J2534_DEVICE_WURTH               //Wurth
#define J2534REGISTRY_VENDOR_NAME       "W�rth Online World!"
#define J2534REGISTRY_TOOL_NAME         "WoW! Flash Box"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-WoWFlashBox.csv"
#else
#ifdef J2534_DEVICE_DELPHI              //Delphi
#define J2534REGISTRY_VENDOR_NAME       "Delphi"
#define J2534REGISTRY_TOOL_NAME         "Delphi Euro 5 Diagnostics"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-Delphi Euro 5 Diagnostics.csv"
#else
#ifdef J2534_DEVICE_DBRIDGE             //dbridge
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group, Inc."
#define J2534REGISTRY_TOOL_NAME         "d-briDGe"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-d-briDGe.csv"
#ifdef J2534_DEVICE_SVCI
#undef J2534REGISTRY_TOOL_NAME
#define J2534REGISTRY_TOOL_NAME         "SVCI"
#define J2534REGISTRY_VENDOR_NAME       "Zhiche Automotive Technologies"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME "DataRecorder-SVCI.csv"
#endif
#else
#ifdef J2534_DEVICE_DBRIDGE0305         //dbridge0305
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group Technology, Inc."
#define J2534REGISTRY_TOOL_NAME         "d-briDGe"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME ""
 #ifdef UD_TRUCK
 #undef J2534REGISTRY_TOOL_NAME
 #define J2534REGISTRY_TOOL_NAME         "Python1B"
 #define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME ""
 #endif

#else
#ifdef J2534_DEVICE_DPA50305            //dpa50305
#define J2534REGISTRY_VENDOR_NAME       "Dearborn Group Technology, Inc."
#define J2534REGISTRY_TOOL_NAME         "DPA 5"
#define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME ""
 #ifdef UD_TRUCK
 #undef J2534REGISTRY_TOOL_NAME
 #define J2534REGISTRY_TOOL_NAME         "Python1B"
 #define J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME ""
 #endif

#else
#error  ERROR: J2534Registry.h Define registry entries for vendor and tool
#endif //dpa50305
#endif //dbridge0305
#endif //dbridge
#endif //delphi
#endif //wurth
#endif //softbridge
#endif //netbridge
#endif //dpa5
#endif //krhw
#endif //vsi
#endif //cardone

// Constants used by all vendor dlls
#ifdef J2534_0305
#define J2534REGISTRY_KEY_PATH              "Software\\PassThruSupport"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport"
#else
#ifdef J2534_0404
#define J2534REGISTRY_KEY_PATH          "Software\\PassThruSupport.04.04"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport.04.04"

#else 
#ifdef J2534_0500
#define J2534REGISTRY_KEY_PATH          "Software\\PassThruSupport.05.00"
#define J2534REGISTRY_KEY_PATH_6432         "Software\\WOW6432Node\\PassThruSupport.05.00"
#endif
#endif
#endif

#define J2534REGISTRY_DEVICE_ID         "DeviceId"
#define J2534REGISTRY_NAME              "Name"
#define J2534REGISTRY_DEVICES           "Devices"
#define J2534REGISTRY_DOUBLE_BACKSLASH  "\\"
#define J2534REGISTRY_HYPHEN_SPACE      " - "
#define J2534REGISTRY_DPA               "DPA"
#define J2534REGISTRY_VENDOR            "Vendor"
#define J2534REGISTRY_NAME              "Name"
#define J2534REGISTRY_PROTOCOLSSUPPORT  "ProtocolsSupported"
#define J2534REGISTRY_CONFIGAPP         "ConfigApplication"
#define J2534REGISTRY_FUNCLIB           "FunctionLibrary"
#define J2534REGISTRY_APIVERSION        "APIVersion"
#define J2534REGISTRY_PRODUCTVERSION    "ProductVersion"
#define J2534REGISTRY_LOGGING           "Logging"
#define J2534REGISTRY_LOGGINGDIRECTORY  "LoggingDirectory"
#define J2534REGISTRY_LOGGING_MAXSIZE   "LoggingSizeMaxKB"
#define J2534REGISTRY_CAN               "CAN"
#define J2534REGISTRY_ISO15765          "ISO15765"
#define J2534REGISTRY_J1850PWM          "J1850PWM"
#define J2534REGISTRY_J1850VPW          "J1850VPW"
#define J2534REGISTRY_ISO9141           "ISO9141"
#define J2534REGISTRY_ISO14230          "ISO14230"
#define J2534REGISTRY_SCI_A_ENGINE      "SCI_A_ENGINE"
#define J2534REGISTRY_SCI_A_TRANS       "SCI_A_TRANS"
#define J2534REGISTRY_SCI_B_ENGINE      "SCI_B_ENGINE"
#define J2534REGISTRY_SCI_B_TRANS       "SCI_B_TRANS"
#define J2534REGISTRY_SW_ISO15765_PS    "SW_ISO15765_PS"
#define J2534REGISTRY_SW_CAN_PS         "SW_CAN_PS"
#define J2534REGISTRY_GM_UART_PS        "GM_UART_PS"
#define J2534REGISTRY_CANDEFAULTBAUDRATE "CANDefaultBaudRate"


// DPA specific
#define J2534REGISTRY_DEVICE_DPA_PX3_COMMPORT   "CommPort"

#define J2534REGISTRY_MAX_STRING_LENGTH     1024
#define J2534REGISTRY_MINI_STRING_LENGTH    256

typedef enum
{
    J2534REGISTRY_DEVICE_DPA_PX3 = 0x01,
    J2534REGISTRY_DEVICE_GENERIC,
    J2534REGISTRY_DEVICE_BLUE_VCI,
    J2534REGISTRY_INVALID_DEVICE
}
J2534REGISTRY_DEVICE_TYPE;

typedef enum
{
    J2534REGISTRY_ERR_SUCCESS = 0x00,
    J2534REGISTRY_ERR_DEVICE_NOT_FOUND,
    J2534REGISTRY_ERR_NULL_PARAMETER,
    J2534REGISTRY_ERR_INSUFFICIENT_WRITING_PRIVILEGE,
    J2534REGISTRY_ERR_NO_DATA_KEY_VALUE
}
J2534REGISTRY_ERROR_TYPE;

typedef struct 
{
    char chCommPort[J2534REGISTRY_MAX_STRING_LENGTH];
}
J2534REGISTRY_CONFIG_DPA;

typedef union
{
    J2534REGISTRY_CONFIG_DPA stJ2534RegConfigDPA;
}
J2534REGISTRY_DEVICE;

typedef struct {
    DWORD   dwDeviceId;
    char    chVendor[J2534REGISTRY_MAX_STRING_LENGTH];
    char    chName[J2534REGISTRY_MAX_STRING_LENGTH];
    char    chProtocolSupported[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD   dwCAN;
    DWORD   dwISO15765;
    DWORD   dwJ1850PWM;
    DWORD   dwJ1850VPW;
    DWORD   dwISO9141;
    DWORD   dwISO14230;
    DWORD   dwSCIAEng;
    DWORD   dwSCIATrans;
    DWORD   dwSCIBEng;
    DWORD   dwSCIBTrans;
    DWORD   dwSWISO15765PS;
    DWORD   dwSWCANPS;
    DWORD   dwGMUARTPS;
    char    chConfigApplication[J2534REGISTRY_MAX_STRING_LENGTH];
    char    chFunctionLibrary[J2534REGISTRY_MAX_STRING_LENGTH];
    char    chAPIVersion[J2534REGISTRY_MAX_STRING_LENGTH];  
    char    chProductVersion[J2534REGISTRY_MAX_STRING_LENGTH];  
    DWORD   dwLogging;
    char    chLoggingDirectory[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD   dwLoggingSizeMaxKB;
    char    chDataLoggingDirectory[J2534REGISTRY_MAX_STRING_LENGTH];
    char    chDataLoggingFileName[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD   dwDataLoggingSizeMaxKB;
    DWORD   dwDataLoggingAppend;
    DWORD   dwDefaultCANDataRate;   // Used for Python Applications
                        
    J2534REGISTRY_DEVICE_TYPE   enJ2534RegistryDeviceType; 
    J2534REGISTRY_DEVICE        unJ2534RegistryDevice;
    
//  DWORD   dwTimeStampResolution;
//  char    chIPAddress[J2534REGISTRY_MAX_STRING_LENGTH];
//  DWORD   dwPortNum;
} J2534REGISTRY_CONFIGURATION;

class CJ2534Registry
{
    public:
        CJ2534Registry();
        ~CJ2534Registry();
        J2534REGISTRY_ERROR_TYPE GetRegistry(
            J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);
        J2534REGISTRY_ERROR_TYPE WriteToRegistry(
            J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);
        void GetDataRecorderSettings(
            J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);
#ifdef J2534_0500
		J2534REGISTRY_ERROR_TYPE GetDeviceCount(
			SDEVICE *j2534_device_list,
			unsigned long *ulDeviceCount);
#endif
    private:
        HKEY* FindJ2534Entry(
            char chJ2534DeviceName[J2534REGISTRY_MAX_STRING_LENGTH]);
        void CreateWriteKeyValues(HKEY *thiskey,
            J2534REGISTRY_CONFIGURATION stJ2534RegistryConfig);
};

#endif 
