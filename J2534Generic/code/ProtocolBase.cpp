/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: ProtocolBase.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of the base class
*              that will be used to derive protocol specific class
*              for implementing J2534 protocol specific requirements.
* Note:
*
*******************************************************************************/

// Includes
#include "StdAfx.h"
#include "J2534Generic.h"
#include "ProtocolBase.h"

// Static variables
CDebugLog        *CProtocolBase::m_pclsLog = NULL;
CDataLog         *CProtocolBase::m_pclsDataLog = NULL;
CMap<unsigned long, unsigned long, PROTOCOLBASE_PUMP_DATA *, PROTOCOLBASE_PUMP_DATA *> CProtocolBase::m_stPumpDatabase;
// This is an Auto-Reset Event.
HANDLE           CProtocolBase::m_hTxBuffer;
HANDLE           CProtocolBase::m_hWritePumpSync;// = CreateEvent(NULL, FALSE, TRUE, NULL);
HANDLE           CProtocolBase::m_hWritePumpExit;// = CreateEvent(NULL, TRUE, TRUE, NULL);
HANDLE           CProtocolBase::m_hWritePumpExited;// = CreateEvent(NULL, TRUE, TRUE, NULL);
CWinThread       *CProtocolBase::m_pclsWritePumpThread = NULL;
unsigned long    CProtocolBase::m_ulInstance = 0;
J2534ERROR       CProtocolBase::m_J2534WriteError = J2534_STATUS_NOERROR;
CRITICAL_SECTION CProtocolBase::CProtocolBaseSection;
// This global variable is a trial. It is done because otherwise, 
//if the pos is received and the protocol gets deleted, the pos is invalid. 
//This is global to avoid that mismatch

POSITION         gpos;
bool             bISO15765 = false;

DWORD MilliSecondsNow();

// Extern Globals
extern CJ2534GenericApp theApp;

//-----------------------------------------------------------------------------
//  Function Name   : CProtocolBase
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CProtocolBase class
//-----------------------------------------------------------------------------
CProtocolBase::CProtocolBase(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog)
{
	InitializeCriticalSection(&CProtocolBaseSection);   // Used in WriteLogMsg()

														// Save pointers.
	m_pclsDevice = pclsDevice;
	m_pclsLog = pclsDebugLog;
	m_pclsDataLog = pclsDataLog;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "CProtocolBase()", DEBUGLOG_TYPE_COMMENT, "Start");

#ifdef J2534_0500
	m_pclsLogicalChannel = NULL;
	m_ulPhyChnRxCount = 0;
#endif
	// Initialize.
	m_bConnected = false;
	m_pclsFilterMsg = NULL;
	m_pclsPeriodicMsg = NULL;
	m_pclsRepeatMsg = NULL;
	m_ulConnectFlag = 0;
	m_ulCANDataRateDefault = theApp.m_J2534Registry.dwDefaultCANDataRate; // Allow for over ride from registry

	if ((m_hBuffEmptyEvent = CreateEvent(NULL, TRUE, TRUE, NULL)) == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "CProtocolBase()", DEBUGLOG_TYPE_ERROR, "Could not create Buff Empty Event");
	}

	if ((m_hPendingEvent = CreateEvent(NULL, TRUE, TRUE, NULL)) == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "CProtocolBase()", DEBUGLOG_TYPE_ERROR, "Could not create Pending Event");
	}

	// Keep track of instances derived from this base.
	m_ulInstance++;

	if (m_ulInstance == 1)
	{
		m_hTxBuffer = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_hWritePumpSync = CreateEvent(NULL, FALSE, TRUE, NULL);
		m_hWritePumpExit = CreateEvent(NULL, TRUE, TRUE, NULL);
		m_hWritePumpExited = CreateEvent(NULL, TRUE, TRUE, NULL);
		//      m_ulCANDataRateDefault = CAN_DATA_RATE_DEFAULT; // Can be overriden in registry entry
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "CProtocolBase()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CProtocolBase
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CProtocolBase class
//-----------------------------------------------------------------------------
CProtocolBase::~CProtocolBase()
{
	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "~CProtocolBase()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Decrement (derived) object instance count
	m_ulInstance--;

	// Disconnect protocol incase not disconnected yet.
	if (m_bConnected)
	{
		if (vDisconnect(true) != J2534_STATUS_NOERROR)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "~CProtocolBase()", DEBUGLOG_TYPE_ERROR, "vDisconnect() failed");
		}
	}

	// Check if this is the last instance.
	//  DeleteFromPumpDatabase(m_ulPumpRefID);
	if (m_ulInstance < 1)
	{
		// Delete all the entries from the database incase it is not.
		DeleteFromPumpDatabase(0, true);
		StopWritePump();
		CloseHandle(m_hWritePumpSync);
		CloseHandle(m_hWritePumpExit);
		CloseHandle(m_hWritePumpExited);
		CloseHandle(m_hTxBuffer);
	}

	if (m_hBuffEmptyEvent != NULL)
		CloseHandle(m_hBuffEmptyEvent);
	if (m_hPendingEvent != NULL)
		CloseHandle(m_hPendingEvent);

	// Delete Filter Class.
	if (m_pclsFilterMsg != NULL)
	{
		delete m_pclsFilterMsg;
		m_pclsFilterMsg = NULL;
	}
#ifdef J2534_0500
	if (m_pclsLogicalChannel != NULL)
	{
		delete m_pclsLogicalChannel;
		m_pclsLogicalChannel = NULL;
	}
#endif
	// Delete Periodic Class.
	if (m_pclsPeriodicMsg != NULL)
	{
		delete m_pclsPeriodicMsg;
		m_pclsPeriodicMsg = NULL;
	}

	// Delete Repeat Msg Class.
	if (m_pclsRepeatMsg != NULL)
	{
		delete m_pclsRepeatMsg;
		m_pclsRepeatMsg = NULL;
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "~CProtocolBase()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//Description       : This function is a generic Connect to connect to a
//                    protocol. This function should not contain Protocol 
//                    specific implementation.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vConnect(J2534_PROTOCOL               enProtocolID,
	unsigned long                ulFlags,
	unsigned long                ulBaudRate,
	DEVICEBASE_CALLBACK_RX_FUNC  pfnCallback,
	LPVOID                       pVoid)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	//    char                    szBuffer[PROTOCOLBASE_ERROR_TEXT_SIZE];
	bool                    bBuffError;
	J2534ERROR              enJ2534Error;
	unsigned long           ulDevChannelRef;
	PROTOCOLBASE_PUMP_DATA  stAddData;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
	// Check for invalid Device ID.
	if (m_pclsDevice == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
		return (J2534_ERR_INVALID_DEVICE_ID);
	}
#endif
	// Check if it is already connected.
	if (m_bConnected)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_CHANNEL_IN_USE);
		return (J2534_ERR_CHANNEL_IN_USE);
	}

	// Check the Events that were created in the constructor.
	if ((m_hBuffEmptyEvent == NULL) || (m_hPendingEvent == NULL))
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "Event Handles not created");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Event Handles not created");
		return(J2534_ERR_FAILED);
	}

	// Delete filters. (This will indirectly set the filter to Block All
	// because if the Object does not exist, default is Block All). 
	if (m_pclsFilterMsg != NULL)
	{
		delete m_pclsFilterMsg;
		m_pclsFilterMsg = NULL;
	}
#ifdef J2534_0500
	if (m_pclsLogicalChannel != NULL)
	{
		delete m_pclsLogicalChannel;
		m_pclsLogicalChannel = NULL;
	}
#endif
	// If Periodic object there, Delete it.
	if (m_pclsPeriodicMsg != NULL)
	{
		delete m_pclsPeriodicMsg;
		m_pclsPeriodicMsg = NULL;
	}

	// If Repeat Msg object there, Delete it.
	if (m_pclsRepeatMsg != NULL)
	{
		delete m_pclsRepeatMsg;
		m_pclsRepeatMsg = NULL;
	}

	// Create a Rx Circular Buffer.
	m_pclsRxCircBuffer = new CCircBuffer(PASSTHRU_MSG_DATA_SIZE, &bBuffError);
	if ((m_pclsRxCircBuffer == NULL) || ((m_pclsRxCircBuffer != NULL) && bBuffError))
	{
		// Delete the Rx Buffer.
		if ((m_pclsRxCircBuffer != NULL) && bBuffError)
		{
			delete(m_pclsRxCircBuffer);
			m_pclsRxCircBuffer = NULL;
		}
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "Buffer cannot be created");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Buffer cannot be created");
		return(J2534_ERR_FAILED);
	}

	// Create a Tx Circular Buffer.
	m_pclsTxCircBuffer = new CCircBuffer(PASSTHRU_MSG_DATA_SIZE, &bBuffError);
	if ((m_pclsTxCircBuffer == NULL) || ((m_pclsTxCircBuffer != NULL) && bBuffError))
	{
		// Delete the Tx Buffer.
		if ((m_pclsTxCircBuffer != NULL) && bBuffError)
		{
			delete(m_pclsTxCircBuffer);
			m_pclsTxCircBuffer = NULL;
		}
		// Delete the Rx Buffer as well.
		if (m_pclsRxCircBuffer != NULL)
		{
			delete(m_pclsRxCircBuffer);
			m_pclsRxCircBuffer = NULL;
		}
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "Buffer cannot be created");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Buffer cannot be created");
		return(J2534_ERR_FAILED);
	}
	// Connect to Device.
	enJ2534Error = m_pclsDevice->vConnectProtocol(enProtocolID,
		ulFlags,
		ulBaudRate,
		pfnCallback,
		pVoid,
		&ulDevChannelRef);
	if (enJ2534Error != J2534_STATUS_NOERROR)
	{
		// Delete the Rx Buffer as well.
		if (m_pclsRxCircBuffer != NULL)
		{
			delete(m_pclsRxCircBuffer);
			m_pclsRxCircBuffer = NULL;
		}
		// Delete the Tx Buffer as well.
		if (m_pclsTxCircBuffer != NULL)
		{
			delete(m_pclsTxCircBuffer);
			m_pclsTxCircBuffer = NULL;
		}
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Set the data to be added to the Pump Database.
	stAddData.hBuffEmptyEvent = m_hBuffEmptyEvent;
	stAddData.hPendingEvent = m_hPendingEvent;
	stAddData.pclsCircBuffer = m_pclsTxCircBuffer;
	stAddData.pclsDevice = m_pclsDevice;
	stAddData.ulDevChannelRef = ulDevChannelRef;

	if (enProtocolID == ISO15765 || enProtocolID == ISO15765_PS ||
		enProtocolID == ISO15765_CH1 || enProtocolID == ISO15765_CH2 ||
		enProtocolID == ISO15765_CH3 || enProtocolID == ISO15765_CH4)
		bISO15765 = true;
	else
		bISO15765 = false;

	// Add Tx Buffer and Device to the database for pumping message out.
	if (!AddToPumpDatabase(&stAddData, &m_ulPumpRefID))
	{
		// Disconnect Protocol from Device.
		m_pclsDevice->vDisconnectProtocol(ulDevChannelRef);

		// Delete the Rx Buffer as well.
		if (m_pclsRxCircBuffer != NULL)
		{
			delete(m_pclsRxCircBuffer);
			m_pclsRxCircBuffer = NULL;
		}
		// Delete the Tx Buffer as well.
		if (m_pclsTxCircBuffer != NULL)
		{
			delete(m_pclsTxCircBuffer);
			m_pclsTxCircBuffer = NULL;
		}
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "AddBufferEntry() failed");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Failed to Add Buffer");
		return(J2534_ERR_FAILED);
	}

	// Start the WritePump thread.
	if (!StartWritePump())
	{
		// Delete the Buffer that was added earlier in the Database.
		DeleteFromPumpDatabase(m_ulPumpRefID);

		// Disconnect Protocol from Device.
		m_pclsDevice->vDisconnectProtocol(ulDevChannelRef);

		// Delete the Rx Buffer as well.
		if (m_pclsRxCircBuffer != NULL)
		{
			delete(m_pclsRxCircBuffer);
			m_pclsRxCircBuffer = NULL;
		}
		// Delete the Tx Buffer as well.
		if (m_pclsTxCircBuffer != NULL)
		{
			delete(m_pclsTxCircBuffer);
			m_pclsTxCircBuffer = NULL;
		}
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "StartWritePump() failed");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Failed to start Write Pump");
		return(J2534_ERR_FAILED);
	}

	// Save Connect Flag value.
	m_ulConnectFlag = ulFlags;

	// Save the ChannelID for further reference.
	m_ulDevChannelRef = ulDevChannelRef;

	// Set the Connected Flag.
	m_bConnected = true;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
#ifdef J2534_0305
	m_pclsDevice->vStartAllPassFilter(ulDevChannelRef, ulFlags);
#endif

#ifdef J2534_0500
	// If Filter Class is not created, create it now.
	if (m_pclsPeriodicMsg == NULL)
	{
		if ((m_pclsPeriodicMsg = new CPeriodicMsg(m_pclsDevice, m_ulDevChannelRef, m_pclsLog)) == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "Unable to create Periodic");
			SetLastErrorText("ProtocolBase : Unable to Create Periodic");
			return(J2534_ERR_FAILED);

		}
	}
	if (m_pclsLogicalChannel == NULL)
	{
		if ((m_pclsLogicalChannel = new CLogicalChannelList(m_pclsDevice, m_ulDevChannelRef, m_pclsPeriodicMsg,m_pclsLog)) == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelConnect()", DEBUGLOG_TYPE_ERROR, "Unable to create Logical Channel");
			SetLastErrorText("ProtocolBase : Unable to Create Logical Channel");
			return(J2534_ERR_FAILED);
		}
	}
#endif
	return(J2534_STATUS_NOERROR);
}
//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is a generic Disconnect to disconnect the 
//                    protocol that was connected earlier. It restores the 
//                    state it was in prior to calling vConnect(). This function
//                    should not contain Protocol specific implementation.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vDisconnect(bool bAuto)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	//    char            szBuffer[PROTOCOLBASE_ERROR_TEXT_SIZE];
	J2534ERROR      enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check if device is not connected.
	if (!m_bConnected)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "Protocol never connected");
		SetLastErrorText("ProtocolBase : Protocol never connected");
		return (J2534_ERR_FAILED);
	}

	m_pclsTxCircBuffer->ClearBuffer();

	// Disconnect from Device.
#ifdef J2534_0305
	m_pclsDevice->vStopAllPassFilter(m_ulDevChannelRef);
#endif
	enJ2534Error = m_pclsDevice->vDisconnectProtocol(m_ulDevChannelRef);

	if (!bAuto)
		if (enJ2534Error != J2534_STATUS_NOERROR)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			return(enJ2534Error);
		}

	// Delete the item that was added in the Database at Connect time.
	if (!DeleteFromPumpDatabase(m_ulPumpRefID))
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_FAILED);
		SetLastErrorText("ProtocolBase : Failed to Delete item from Database");
		return(J2534_ERR_FAILED);
	}

	// Delete Filter if not deleted already.
	if (m_pclsFilterMsg != NULL)
	{
		// Stop all Filters.
		m_pclsFilterMsg->StopFilter();
		delete m_pclsFilterMsg;
		m_pclsFilterMsg = NULL;
	}
#ifdef J2534_0500
	if (m_pclsLogicalChannel!= NULL)
	{
		//m_pclsLogicalChannel->StopFilter();
		delete m_pclsLogicalChannel;
		m_pclsLogicalChannel = NULL;
	}
#endif
	// Delete Periodic if not deleted already.
	if (m_pclsPeriodicMsg != NULL)
	{
		// Stop all Periodics.
		m_pclsPeriodicMsg->StopPeriodic();
		delete m_pclsPeriodicMsg;
		m_pclsPeriodicMsg = NULL;
	}

	// Delete Repeat if not deleted already.
	if (m_pclsRepeatMsg != NULL)
	{
		// Stop all Periodics.
		m_pclsRepeatMsg->StopRepeat();
		delete m_pclsRepeatMsg;
		m_pclsRepeatMsg = NULL;
	}

	// Stop the Write Pump if Pump Database Empty.
	if (IsEmptyPumpDatabase())
	{
		StopWritePump();
	}

	// Delete the Rx Buffer as well.
	if (m_pclsRxCircBuffer != NULL)
	{
		delete(m_pclsRxCircBuffer);
		m_pclsRxCircBuffer = NULL;
	}
	// Delete the Tx Buffer as well.
	if (m_pclsTxCircBuffer != NULL)
	{
		delete(m_pclsTxCircBuffer);
		m_pclsTxCircBuffer = NULL;
	}

	// Reset Flag value.
	m_ulConnectFlag = 0;

	// Reset Channel ID.
	m_ulDevChannelRef = 0;

	// Set the Connected flag.
	m_bConnected = false;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the requested number of messages from
//                    the circular buffer sends it over to the caller. This is
//                    a genric Read and so the protocol specific implementation
//                    should be carried out in the class derived from this.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vReadMsgs(PASSTHRU_MSG    *pstPassThruMsg,
	unsigned long   *pulNumMsgs,
	unsigned long   ulTimeout)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	bool            bBufferOverflow = true;
	bool            bOverflowed = false;
	bool            bBufferEmpty = true;
	DWORD           dwStartTick;
	unsigned long   ulReadMsgs;
	unsigned long   ulNumMsgs;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	dwStartTick = MilliSecondsNow();


	ulReadMsgs = *pulNumMsgs;
	*pulNumMsgs = 0;
	do
	{
		// Check to see if the buffer is overflowed.
		m_pclsRxCircBuffer->GetBufferOverflowStatus(&bBufferOverflow);
		if (bBufferOverflow)
		{
			bOverflowed = true;
		}

		if (pstPassThruMsg != NULL)
		{
			ulNumMsgs = ulReadMsgs - *pulNumMsgs;
			if (!m_pclsRxCircBuffer->Read((unsigned char *)(pstPassThruMsg+*pulNumMsgs), &ulNumMsgs))
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "Circular Buffer Error");
				SetLastErrorText("ProtocolBase : Circular Buffer Error");
				return(J2534_ERR_FAILED);
			}
			*pulNumMsgs = *pulNumMsgs + ulNumMsgs;
		}
		else
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "Buffer supplied is not big enough");
			SetLastErrorText("ProtocolBase : Buffer supplied is not big enough");
			return(J2534_ERR_FAILED);
		}

		// If all messages are read
		//      if ((*pulNumMsgs < ulReadMsgs) || (pstPassThruMsg->ulProtocolID == ISO15765))
		if ((*pulNumMsgs < ulReadMsgs) && ((MilliSecondsNow() - dwStartTick) < ulTimeout))
			Sleep(1);
	} while ((*pulNumMsgs < ulReadMsgs) && ((MilliSecondsNow() - dwStartTick) < ulTimeout));
	if (bOverflowed)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_OVERFLOW);
		return(J2534_ERR_BUFFER_OVERFLOW);
	}

	if (*pulNumMsgs < ulReadMsgs)
	{
		if (*pulNumMsgs == 0)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_EMPTY);
			return(J2534_ERR_BUFFER_EMPTY);
		}
		if ((MilliSecondsNow() - dwStartTick) >= ulTimeout)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
			return(J2534_STATUS_NOERROR);
		}
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a generic function to write msgs. to a circular 
//                    buffer that waits until it is transmitted out on the bus 
//                    or exits immediately after writing to buffer if it is 
//                    non-blocking. The message is Blocking if the given timeout
//                    value is greater than 0. This is a generic Wrie and so 
//                    the protocol specific implementation should be carried 
//                    out in the class derived from this.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
	unsigned long  *pulNumMsgs,
	unsigned long  ulTimeout
#ifdef J2534_0500
	,unsigned long  ulChannelId
#endif
)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	unsigned long   ulNumMsgs;
	J2534ERROR      enJ2534Error;
	bool            bBufferFull = true;
	unsigned long   i;
	//PASSTHRU_MSG*       tmpMsg;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Write one msg. at a time to circular buffer and wait for each message
	// to be sent to Device if the Blocking is requested.
	ulNumMsgs = *pulNumMsgs;
	for (i = 0; i < ulNumMsgs; i++)
	{
		// Set the msg. pending event before putting in the buffer if Blocking requested.
		if (ulTimeout > 0)
		{
			if ((enJ2534Error = SetMsgPending(m_ulPumpRefID, &ulTimeout)) != J2534_STATUS_NOERROR)
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
				return(enJ2534Error);
			}
		}

		// Check if Buffer is full.
		m_pclsTxCircBuffer->GetBufferFullStatus(&bBufferFull);
		if (bBufferFull)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_FULL);
			return(J2534_ERR_BUFFER_FULL);
		}

		if ((pstPassThruMsg + i) != NULL)
		{
#ifndef J2534_0500
			// Write msgs to the circular buffer.
			if (!m_pclsTxCircBuffer->Write((unsigned char *)(pstPassThruMsg + i),
				sizeof(PASSTHRU_MSG) - PASSTHRU_MSG_DATA_SIZE +
				(pstPassThruMsg + i)->ulDataSize))
#endif
#ifdef J2534_0500
				if (!m_pclsTxCircBuffer->Write((unsigned char *)(pstPassThruMsg + i),
					sizeof(PASSTHRU_MSG) - 4 +
					(pstPassThruMsg + i)->ulDataSize))
#endif
				{
					// Write to Log File.
					WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "Circular buffer error");
					SetLastErrorText("ProtocolBase : Circular buffer error");
					return(J2534_ERR_FAILED);
				}
				
			SetEvent(m_hTxBuffer);
		}
		else
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "Buffer supplied is not big enough");
			SetLastErrorText("ProtocolBase : Buffer supplied is not big enough");
			return(J2534_ERR_FAILED);
		}
		// Confirm if the message is sent out to the Device from the Buffer if 
		// Blocking is requested.
		if (ulTimeout > 0)
		{
			if ((enJ2534Error = IsMsgSent(m_ulPumpRefID, &ulTimeout)) != J2534_STATUS_NOERROR)
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
				return(enJ2534Error);
			}
		}
		*pulNumMsgs = (i + 1);
		// If all messages are written
		//      if ((*pulNumMsgs < ulNumMsgs) || (pstPassThruMsg->ulProtocolID == ISO15765))
		// 20150903 buf fix #3645
		// No, this routine does not need to sleep, unless it is non-blocking write. Blocking writes will block waiting for hBuffEmptyEvent for each message written
		if (ulTimeout == 0) {
			if (*pulNumMsgs < ulNumMsgs)
				Sleep(50);
		}
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This a generic function to Start Periodic Msg.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vStartPeriodicMsg(PASSTHRU_MSG  *pstPassThruMsg,
	unsigned long *pulMsgID,
	unsigned long ulTimeInterval
#ifdef J2534_0500
	,unsigned long    ulChannelId
#endif
	)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************
	J2534ERROR  enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

	// If Filter Class is not created, create it now.
	if (m_pclsPeriodicMsg == NULL)
	{
		if ((m_pclsPeriodicMsg = new CPeriodicMsg(m_pclsDevice, m_ulDevChannelRef, m_pclsLog)) == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "Unable to create Periodic");
			SetLastErrorText("ProtocolBase : Unable to Create Periodic");
			return(J2534_ERR_FAILED);

		}
	}

	// Setup Periodic.
	if ((enJ2534Error = m_pclsPeriodicMsg->StartPeriodic(pstPassThruMsg,pulMsgID,ulTimeInterval))
																			!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a generic function to stop Periodic Msg that was
//                    started earlier.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vStopPeriodicMsg(unsigned long ulMsgID)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	J2534ERROR  enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (m_pclsPeriodicMsg == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
		return (J2534_ERR_INVALID_MSG_ID);
	}

	// Setup Filter.
	if ((enJ2534Error = m_pclsPeriodicMsg->StopPeriodic(ulMsgID))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This a generic function and so the protocol specific 
//                    implementation should be carried out in the class derived 
//                    from this.
//-----------------------------------------------------------------------------
J2534ERROR  CProtocolBase::vStartMsgFilter(
	J2534_FILTER   enFilterType,
	PASSTHRU_MSG  *pstPassThruMask,
	PASSTHRU_MSG  *pstPassThruPattern,
	PASSTHRU_MSG  *pstPassThruFlowControl,
	unsigned long *pulFilterID
#ifdef J2534_DEVICE_SVCI1
	,
	bool           bTP2_0_Implicit,
	bool           bTP2_0_Implicit_Passive
#endif
)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	J2534ERROR enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

	// If Filter Class is not created, create it now.
	if (m_pclsFilterMsg == NULL)
	{
		if ((m_pclsFilterMsg = new CFilterMsg(m_pclsDevice, m_ulDevChannelRef, m_pclsLog)) == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "Unable to create Filter");
			SetLastErrorText("ProtocolBase : Unable to Create Filter");
			return(J2534_ERR_FAILED);
		}
	}

	// Setup Filter.
	if ((enJ2534Error = m_pclsFilterMsg->StartFilter(
		enFilterType,
		pstPassThruMask,
		pstPassThruPattern,
		pstPassThruFlowControl,
		pulFilterID
#ifdef J2534_DEVICE_SVCI1
		,
		bTP2_0_Implicit,
		bTP2_0_Implicit_Passive
#endif
	))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
// Function Name    : vStopMsgFilter
// Input Params     :
// Output Params   :
// Description     : This a generic function and so the protocol specific 
//                    implementation should be carried out in the class derived 
//                    from this.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vStopMsgFilter(unsigned long ulFilterID
#ifdef J2534_DEVICE_SVCI1
	,
	bool           bTP2_0_Implicit,
	bool           bTP2_0_Implicit_Passive
#endif
)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	//    char szBuffer[PROTOCOLBASE_ERROR_TEXT_SIZE];
	J2534ERROR enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (m_pclsFilterMsg == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		return (J2534_ERR_INVALID_FILTER_ID);
	}

	// Setup Filter.
	if ((enJ2534Error = m_pclsFilterMsg->StopFilter(ulFilterID
#ifdef J2534_DEVICE_SVCI1
		,
		bTP2_0_Implicit,
		bTP2_0_Implicit_Passive
#endif
	))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}
#ifdef J2534_0500
J2534ERROR CProtocolBase::vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput,
								unsigned long ulChannelId)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	J2534ERROR enJ2534Error;
	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
	if ((enumIoctlID == START_REPEAT_MESSAGE) || (enumIoctlID == QUERY_REPEAT_MESSAGE) || (enumIoctlID == STOP_REPEAT_MESSAGE))
	{
		return IoctlRepeatMsg(enumIoctlID, pInput, pOutput);
	}
#endif

	//enJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelRef, enumIoctlID, pInput, pOutput);
	enJ2534Error = m_pclsLogicalChannel->vIoctl(ulChannelId, enumIoctlID, pInput, pOutput);
	
	if (enJ2534Error != J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
	return(J2534_STATUS_NOERROR);
}

#endif
//-----------------------------------------------------------------------------
// Function Name    : vIoctl
// Input Params     :
// Output Params    :
// Description      : This is a virtual function which should be implemented by
//                    the class derived from this base class.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::vIoctl(J2534IOCTLID enumIoctlID,
	void *pInput,
	void *pOutput)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	J2534ERROR enJ2534Error;
	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
	if ((enumIoctlID == START_REPEAT_MESSAGE) || (enumIoctlID == QUERY_REPEAT_MESSAGE) || (enumIoctlID == STOP_REPEAT_MESSAGE))
	{
		return IoctlRepeatMsg(enumIoctlID, pInput, pOutput);
	}
#endif
	enJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelRef, enumIoctlID, pInput, pOutput);

	if (enJ2534Error != J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : AddToPumpDatabase
//  Input Params    : void
//  Output Params   : void
//  Description     : This function adds a buffer and device to a database that 
//                    is used by the WritePump Thread.
//-----------------------------------------------------------------------------
bool CProtocolBase::AddToPumpDatabase(PROTOCOLBASE_PUMP_DATA *pstAddData,
	unsigned long          *pulPumpRefID)
{
	PROTOCOLBASE_PUMP_DATA          *pstPumpData;
	bool                            bRet = true;

	if ((pulPumpRefID == NULL) || (pstAddData == NULL))
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "AddToPumpDatabase()", DEBUGLOG_TYPE_ERROR, "NULL Pointer passed");
		return(false);
	}

	// Check if Write Thread Sync. event is in signal state.
	if (WaitForSingleObject(m_hWritePumpSync, PROTOCOLBASE_PUMP_SYNC_TIMEOUT)
		== WAIT_OBJECT_0)
	{
		*pulPumpRefID = MilliSecondsNow();
		// Check if the Reference ID is duplicate.
		if (m_stPumpDatabase.Lookup(*pulPumpRefID, pstPumpData))
		{
			// Sleep upto Tick Count resolution so that we can create a unique
			// Reference ID.
			Sleep(56);
			*pulPumpRefID = MilliSecondsNow();
		}

		// Create new item.
		pstPumpData = new(PROTOCOLBASE_PUMP_DATA);
		if (pstPumpData != NULL)
		{
			pstPumpData->hBuffEmptyEvent = pstAddData->hBuffEmptyEvent;
			pstPumpData->hPendingEvent = pstAddData->hPendingEvent;
			pstPumpData->pclsCircBuffer = pstAddData->pclsCircBuffer;
			pstPumpData->pclsDevice = pstAddData->pclsDevice;
			pstPumpData->ulDevChannelRef = pstAddData->ulDevChannelRef;

			// Set created item into the Pump Database.
			m_stPumpDatabase[*pulPumpRefID] = pstPumpData;
			// Get the position of first item in the CMap Database.
			gpos = m_stPumpDatabase.GetStartPosition();
		}
		else
		{
			bRet = false;
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "AddToPumpDatabase()", DEBUGLOG_TYPE_ERROR, "new(PROTOCOLBASE_PUMP_DATA) failed ");
		}
		// Set it back to signal state.
		SetEvent(m_hWritePumpSync);
	}
	else
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "AddToPumpDatabase()", DEBUGLOG_TYPE_ERROR, "Sync. Event not signalled");
		return(false);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "AddToPumpDatabase()", DEBUGLOG_TYPE_COMMENT, "End");
	return(bRet);
}

//-----------------------------------------------------------------------------
//  Function Name   : DeleteFromPumpDatabase
//  Input Params    : void
//  Output Params   : void
//  Description     : This function removes a buffer and a device from the 
//                    database that is used by the WritePump Thread. If the 
//                    bRemoveAll is true, the database is made empty.
//-----------------------------------------------------------------------------
bool CProtocolBase::DeleteFromPumpDatabase(unsigned long ulPumpRefID,
	bool          bRemoveAll)
{
	PROTOCOLBASE_PUMP_DATA          *pstPumpData;
	bool                            bRet = true;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "DeleteFromPumpDatabase()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check if Write Thread Sync. event is in signal state.
	if (WaitForSingleObject(m_hWritePumpSync,
		PROTOCOLBASE_PUMP_SYNC_TIMEOUT) == WAIT_OBJECT_0)
	{
		if (bRemoveAll)
		{
			if (!m_stPumpDatabase.IsEmpty())
			{
				// Remove all the items from the Pump Database.
				m_stPumpDatabase.RemoveAll();
			}
		}
		else
		{
			if (m_stPumpDatabase.Lookup(ulPumpRefID, pstPumpData))
			{
				if (!m_stPumpDatabase.RemoveKey(ulPumpRefID))
				{
					bRet = false;
					// Write to Log File.
					WriteLogMsg("ProtocolBase.cpp", "DeleteFromPumpDatabase()", DEBUGLOG_TYPE_ERROR, "Could not remove Key");
				}
				else
				{
					delete pstPumpData;
					pstPumpData = NULL;
				}

				// Get the position of first item in the CMap Database.
				gpos = m_stPumpDatabase.GetStartPosition();
			}

		}

		// Set it back to signal state.
		SetEvent(m_hWritePumpSync);
	}
	else
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "DeleteFromPumpDatabase()", DEBUGLOG_TYPE_ERROR, "Sync. Event not signalled");
		return(false);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "DeleteFromPumpDatabase()", DEBUGLOG_TYPE_COMMENT, "End");
	return(bRet);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsEmptyPumpDatabase
//  Input Params    : void
//  Output Params   : void
//  Description     : This function checks to see if the Pump Database is empty.
//-----------------------------------------------------------------------------
bool CProtocolBase::IsEmptyPumpDatabase()
{
	bool    bEmpty = true;

	// Check if Write Thread Sync. event is in signal state.
	if (WaitForSingleObject(m_hWritePumpSync, PROTOCOLBASE_PUMP_SYNC_TIMEOUT)
		== WAIT_OBJECT_0)
	{
		// Check if the Reference ID is duplicate.
		if (m_stPumpDatabase.IsEmpty())
		{
			bEmpty = true;
		}
		else
			bEmpty = false;

		// Set it back to signal state.
		SetEvent(m_hWritePumpSync);
	}
	else
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "IsEmptyPumpDatabase()", DEBUGLOG_TYPE_ERROR, "Sync. Event not signalled");
		return(true);
	}

	return(bEmpty);
}

//-----------------------------------------------------------------------------
//  Function Name   : StartWritePump
//  Input Params    : void
//  Output Params   : void
//  Description     : This function starts a thread that will continuosly check
//                    if any of the registered circular buffer has a message.
//                    If yes, it will dequeue it and send it to the 
//                    corresponding device to send it out on the Bus.
//-----------------------------------------------------------------------------
bool CProtocolBase::StartWritePump()
{
	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "StartWritePump()", DEBUGLOG_TYPE_COMMENT, "Start");

	// If the write thread is not running.
	if (WaitForSingleObject(m_hWritePumpExited, 0) == WAIT_OBJECT_0)
	{
		// Create Thread.
		// TEMP-TEST 050613par m_pclsWritePumpThread = AfxBeginThread((AFX_THREADPROC)WritePumpThread, NULL);
		// Improve responsiveness of our DLL
		m_pclsWritePumpThread = AfxBeginThread((AFX_THREADPROC)WritePumpThread, NULL, THREAD_PRIORITY_HIGHEST);

		if (m_pclsWritePumpThread == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "StartWritePump()", DEBUGLOG_TYPE_ERROR, "WriteThread pointer returned NULL!");
		}

		// Give sometime to run the thread
		Sleep(200);

		// Make sure the thread did not exit.
		if (WaitForSingleObject(m_hWritePumpExited, 0) == WAIT_OBJECT_0)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "StartWritePump()", DEBUGLOG_TYPE_ERROR, "Could not create WriteThread !");
			return(false);
		}
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "StartWritePump()", DEBUGLOG_TYPE_COMMENT, "End");
	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : StopWritePump
//  Input Params    : void
//  Output Params   : void
//  Description     : This function stops the write thread that was created
//                    earlier. 
//-----------------------------------------------------------------------------
bool CProtocolBase::StopWritePump()
{
	DWORD   dwExitCode;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "StopWritePump()", DEBUGLOG_TYPE_COMMENT, "Start");

	SetEvent(m_hWritePumpExit);
	SetEvent(m_hTxBuffer);
	if (WaitForSingleObject(m_hWritePumpExited, PROTOCOLBASE_PUMPTHREAD_END_TIMEOUT) !=
		WAIT_OBJECT_0)
	{
		// The thread did not exit gracefully so do the following.
		//      if (m_pclsWritePumpThread != NULL)
		{
			GetExitCodeThread(m_pclsWritePumpThread->m_hThread, &dwExitCode);
			TerminateThread(m_pclsWritePumpThread->m_hThread, dwExitCode);
			SetEvent(m_hWritePumpSync);
			SetEvent(m_hWritePumpExited);
			m_pclsWritePumpThread = NULL;
		}

		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "StopWritePump()", DEBUGLOG_TYPE_ERROR, "WriteThread failed graceful exit !");

		return(false);
	}

	// Set all the Event handles to signal just incase.
	SetEvent(m_hWritePumpExit);
	SetEvent(m_hWritePumpSync);
	SetEvent(m_hWritePumpExited);

	m_pclsWritePumpThread = NULL;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "StopWritePump()", DEBUGLOG_TYPE_COMMENT, "End");

	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetMsgPending
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the status to Msg. pending. This will
//                    be set to not-pending by the WritePumpThread() when all
//                    the msgs. from buffer are sent out to the Device 
//                    successfully. This function takes the timeout value and
//                    returns the remaining time.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::SetMsgPending(unsigned long   ulPumpRefID,
	unsigned long   *pulTimeout)
{
	PROTOCOLBASE_PUMP_DATA      *pstPumpData;
	DWORD                       dwStatus;
	DWORD                       dwStartTick;
	DWORD                       dwTick;

	// Write to Log File.
	//  WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_COMMENT, "Start");

	dwStartTick = MilliSecondsNow();

	// Find the Data corressponding to a given RefID.
	if (!m_stPumpDatabase.Lookup(ulPumpRefID, pstPumpData))
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_ERROR, "Referenced Data not found");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Referenced Data not found");
		return (J2534_ERR_FAILED);
	}

	//
	// ---------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------
	// 20150903 buf fix #3645
	// No, No! It is not the job of this thread to set or reset hBuffEmptyEvent!
	// That is the job of the WritePumpThread. WritePumpThread sets it, this thread reads it.
	// ---------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------
	//

	// Wait for Buffer to empty.
	if ((dwStatus = WaitForSingleObject(pstPumpData->hBuffEmptyEvent, *pulTimeout)) != WAIT_OBJECT_0)
	{
		if (dwStatus == WAIT_TIMEOUT)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
			return (J2534_ERR_TIMEOUT);
		}
		else
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_ERROR, "Waiting for buffer empty");
			// Set right text to be retreived when GetLastError() called.
			SetLastErrorText("ProtocolBase : Waiting for buffer empty");
			return (J2534_ERR_FAILED);
		}
	}

	// Msg. Pending is signalled then set it to non-signalled.
	if (WaitForSingleObject(pstPumpData->hPendingEvent, 0) == WAIT_OBJECT_0)
	{
		// Set the event for this buffer to pending. This will be set
		if (!ResetEvent(pstPumpData->hPendingEvent))
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_ERROR, "Could not set msg. pending");
			// Set right text to be retreived when GetLastError() called.
			SetLastErrorText("ProtocolBase : Could not set msg. pending");
			return (J2534_ERR_FAILED);
		}
	}

	if ((dwTick = (MilliSecondsNow() - dwStartTick)) <= *pulTimeout)
	{
		*pulTimeout = *pulTimeout - dwTick;
	}
	else
	{
		*pulTimeout = 0;
	}
	// Write to Log File.
	//  WriteLogMsg("ProtocolBase.cpp", "SetMsgPending()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgSent
//  Input Params    : 
//  Output Params   : 
//  Description     : This function checks the Buffer in the Pump Database 
//                    corressponding to the given Reference ID to see if the
//                    any message is pending that is not yet sent out to
//                    the device. This function takes the timeout value and
//                    returns the remaining time.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::IsMsgSent(unsigned long   ulPumpRefID,
	unsigned long   *pulTimeout)
{
	PROTOCOLBASE_PUMP_DATA      *pstPumpData;
	DWORD                       dwStatus;
	DWORD                       dwStartTick;
	DWORD                       dwTick;

	dwStartTick = MilliSecondsNow();

	// Find the Data corressponding to a given RefID.
	if (!m_stPumpDatabase.Lookup(ulPumpRefID, pstPumpData))
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "IsMsgSent()", DEBUGLOG_TYPE_ERROR, "Referenced Data not found");
		// Set right text to be retreived when GetLastError() called.
		SetLastErrorText("ProtocolBase : Referenced Data not found");
		return (J2534_ERR_FAILED);
	}

	// Wait for pending msg. to be sent out to device. This will be signalled
	// in the WritePumpThread().
	if ((dwStatus = WaitForSingleObject(pstPumpData->hPendingEvent, *pulTimeout)) != WAIT_OBJECT_0)
	{
		if (dwStatus == WAIT_TIMEOUT)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "IsMsgSent()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
			return (J2534_ERR_TIMEOUT);
		}
		else
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "IsMsgSent()", DEBUGLOG_TYPE_ERROR, "Wait for msg. pending failed");
			// Set right text to be retreived when GetLastError() called.
			SetLastErrorText("ProtocolBase : Wait for msg. pending failed");
			return (J2534_ERR_FAILED);
		}
	}

	if ((dwTick = (MilliSecondsNow() - dwStartTick)) <= *pulTimeout)
	{
		*pulTimeout = *pulTimeout - dwTick;
	}
	else
	{
		*pulTimeout = 0;
	}
	return(m_J2534WriteError);
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CProtocolBase::WriteLogMsg(char * chFileName, char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
	if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
	{
		EnterCriticalSection(&CProtocolBaseSection); // Make processing parameters thread-safe
		char buf[1024];
		va_list ap;
		va_start(ap, chMsg);
		vsprintf_s(buf, sizeof(buf) - 1, chMsg, ap);
		va_end(ap);
		buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
		m_pclsLog->Write(chFileName, chFunctionName, MsgType, buf);
		LeaveCriticalSection(&CProtocolBaseSection);
	}
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteDataLogMsg()
//  Input Params    : csChannelPins, csProtocol, ulRxTx, ulTimestamp, csID, csData 
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CProtocolBase::WriteDataLogMsg(char *csChannelPins, char *csProtocol, unsigned long ulRxTx, unsigned long ulTimestamp, char* csID, char *csData)
{
	if (m_pclsDataLog != NULL && m_pclsDataLog->m_pfdLogFile != NULL)
	{
		EnterCriticalSection(&CProtocolBaseSection); // Make processing parameters thread-safe
		m_pclsDataLog->Write(csChannelPins, csProtocol, ulRxTx, ulTimestamp, csID, csData);
		LeaveCriticalSection(&CProtocolBaseSection);
	}
}

//-----------------------------------------------------------------------------
//  Function Name: WritePumpThread
//  Input Params    : 
//  Output Params   : 
//  Description     : This thread peeks to see if there is any message in the
//                    circular buffers that were registered in the database.
//                    If yes, it dequeues the message and sends it to out to
//                    the corresponding device to write it out on the Bus.
//-----------------------------------------------------------------------------
UINT WritePumpThread(LPVOID lpVoid)
{
	PROTOCOLBASE_PUMP_DATA          *pstPumpData;
	DWORD                           dwSync;
	//  POSITION                        pos;
	unsigned long                   ulKeyIdx;
	unsigned long                   ulNumMsg;
#if (defined J2534_0404 || defined J2534_0305 )
	PASSTHRU_MSG stPassThruMsg;
#endif

	int                             iCount;
	bool                            bEmpty;

	// Write to Log File.
	CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Set this event to non-signal to indicate that the thread is running.
	ResetEvent(CProtocolBase::m_hWritePumpExited);
	// Set this event to non-signal. When signaled, it will exit the thread.
	ResetEvent(CProtocolBase::m_hWritePumpExit);
	gpos = NULL;
	ulNumMsg = 0;
	iCount = 0;
	// Check if Thread Synchronization Event is signaled and Exit Event is not
	// signaled.

	while (WaitForSingleObject(CProtocolBase::m_hWritePumpExit, 0) != WAIT_OBJECT_0)
	{
		if ((dwSync = WaitForSingleObject(CProtocolBase::m_hTxBuffer,
			INFINITE)) == WAIT_OBJECT_0)
		{
			bEmpty = false;
			gpos = NULL;
			// This while loop tries to empty all the buffers by reading one message from a connected
			// protocol/channel at a time
			while ((bEmpty == false || gpos != NULL) &&
				(WaitForSingleObject(CProtocolBase::m_hWritePumpExit, 0) != WAIT_OBJECT_0))
			{
				if ((dwSync = WaitForSingleObject(CProtocolBase::m_hWritePumpSync,
					PROTOCOLBASE_PUMP_SYNC_TIMEOUT)) == WAIT_OBJECT_0)
				{
					// Check if the position in the CMap Database is not NULL.
					if (gpos != NULL)

					{
						// Get item from Database at the given position.
						CProtocolBase::m_stPumpDatabase.GetNextAssoc(gpos, ulKeyIdx, pstPumpData);
#if (defined J2534_0404 || defined J2534_0305 )
						ulNumMsg = 1;
						// Pull out message from this circular buffer
						if (pstPumpData->pclsCircBuffer->Read(
							(unsigned char *)(&stPassThruMsg), &ulNumMsg))
						{
							if (ulNumMsg > 0)
							{
								bEmpty = false;
								// Send out the pulled msg. to the corressponding Device.
								if ((CProtocolBase::m_J2534WriteError = pstPumpData->pclsDevice->vWriteMsgs(
									pstPumpData->ulDevChannelRef, &stPassThruMsg, &ulNumMsg)) != J2534_STATUS_NOERROR)
								{
									// Write to Log File.
									CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_ERROR, "Device::vWriteMsgs() failed");
								}
								// If there is a Pending event set, signal it after msg. sent to Device.
								if (WaitForSingleObject(pstPumpData->hPendingEvent, 0) != WAIT_OBJECT_0)
								{

									SetEvent(pstPumpData->hPendingEvent);
								}
							}
							else if (WaitForSingleObject(pstPumpData->hBuffEmptyEvent, 0) != WAIT_OBJECT_0)
							{
								// Signal the Buffer Empty event.
								SetEvent(pstPumpData->hBuffEmptyEvent);
							}
						}
#endif
#ifdef J2534_0500
						// Pull out message from this circular buffer
						PASSTHRU_MSG*                    stPassThruMsg;
						stPassThruMsg = new PASSTHRU_MSG;
						memset(stPassThruMsg, 0, sizeof( PASSTHRU_MSG));
						ulNumMsg = 1;
						if (pstPumpData->pclsCircBuffer->Read(
							(unsigned char *)(stPassThruMsg), &ulNumMsg))
						{
							if (ulNumMsg > 0)
							{
								bEmpty = false;
								// Send out the pulled msg. to the corressponding Device.
								if ((CProtocolBase::m_J2534WriteError = pstPumpData->pclsDevice->vWriteMsgs(
									pstPumpData->ulDevChannelRef, stPassThruMsg, &ulNumMsg)) != J2534_STATUS_NOERROR)
								{
									// Write to Log File.
									CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_ERROR, "Device::vWriteMsgs() failed");
								}

								//clear the memory allocated 

								if ((NULL != stPassThruMsg) && (NULL != stPassThruMsg->ucData))
								{
									delete [] stPassThruMsg->ucData;
									delete stPassThruMsg;
								}
								// If there is a Pending event set, signal it after msg. sent to Device.
								if (WaitForSingleObject(pstPumpData->hPendingEvent, 0) != WAIT_OBJECT_0)
								{
									SetEvent(pstPumpData->hPendingEvent);
								}
							}
							else if (WaitForSingleObject(pstPumpData->hBuffEmptyEvent, 0) != WAIT_OBJECT_0)
							{
								// Signal the Buffer Empty event.
								SetEvent(pstPumpData->hBuffEmptyEvent);
							}
						}
#endif
					}

					// Check if the position is NULL now.
					else if (gpos == NULL)

					{
						// Get the position of first item in the CMap Database.
						gpos = CProtocolBase::m_stPumpDatabase.GetStartPosition();
						bEmpty = true;

					}

					// Signal the Sync event.
					SetEvent(CProtocolBase::m_hWritePumpSync);
				}
			}
			iCount++;

			// Relinquish the thread.
			//      if (((iCount % 5) == 0) || ((ulNumMsg == 1) && (stPassThruMsg.ulProtocolID == ISO15765)))
			if ((iCount % 5) == 0)
				//      if (((!bISO15765) && ((iCount % 5) == 0)) || (bISO15765))
				// 20150903 buf fix #3645 - sleep(0) *not* sleep(1)
				Sleep(0);
		}
	}

	// If the Synchronization event timed out, may be it never got released
	// by the other thread.
	if (dwSync == WAIT_TIMEOUT)
	{
		// Write to Log File.
		CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_ERROR, "Sync. Event not signalled");
	}

	CProtocolBase::m_pclsWritePumpThread = NULL;

	// Set this event to Signal incase not.
	SetEvent(CProtocolBase::m_hWritePumpExit);

	// Write to Log File.
	CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_COMMENT, "SetEvent(CProtocolBase::m_hWritePumpSync)");

	// Set this event to Signal incase not.
	SetEvent(CProtocolBase::m_hWritePumpSync);

	// Set this event to Signal to indicate that thread exited.
	SetEvent(CProtocolBase::m_hWritePumpExited);

	// Write to Log File.
	CProtocolBase::WriteLogMsg("ProtocolBase.cpp", "WritePumpThread()", DEBUGLOG_TYPE_COMMENT, "End");

	// End thread       AfxEndThread(0);
	return 1;
}


#ifdef J2534_DEVICE_DBRIDGE
//-----------------------------------------------------------------------------
// Function Name    : IoctlRepeatMsg
// Input Params     : enumIoctlID, pInput
// Output Params    : pOutput
// Description      : This function supports Ioctl Repeat Msg
//-----------------------------------------------------------------------------
J2534ERROR CProtocolBase::IoctlRepeatMsg(J2534IOCTLID enumIoctlID, void *pInput, void *pOutput)
{
	J2534ERROR enJ2534Error;
	switch (enumIoctlID)
	{
	case START_REPEAT_MESSAGE:
	{
		// If Periodic Class is not created, create it now.
		if (m_pclsPeriodicMsg == NULL)
		{
			if ((m_pclsPeriodicMsg = new CPeriodicMsg(m_pclsDevice, m_ulDevChannelRef, m_pclsLog)) == NULL)
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_ERROR, "Unable to create Periodic");
				SetLastErrorText("ProtocolBase : Unable to Create Periodic");
				return(J2534_ERR_FAILED);
			}
		}
		// If Repeat Msg Class is not created, create it now.
		if (m_pclsRepeatMsg == NULL)
		{
			if ((m_pclsRepeatMsg = new CRepeatMsg(m_pclsDevice, m_ulDevChannelRef, m_pclsPeriodicMsg, m_pclsLog)) == NULL)
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_ERROR, "Unable to create Repeat Msg");
				SetLastErrorText("ProtocolBase : Unable to Create Repeat Msg");
				return(J2534_ERR_FAILED);
			}
		}
		// Start Repeat Msg
		REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
		enJ2534Error = m_pclsRepeatMsg->StartRepeat(
			pRepeatMsgSetup->TimeInterval,
			pRepeatMsgSetup->Condition,
			&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]),
			&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]),
			&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]),
			(unsigned long*)pOutput);
	}
	break;

	case QUERY_REPEAT_MESSAGE:
	{
		if ((m_pclsPeriodicMsg == NULL) || (m_pclsRepeatMsg == NULL))
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
			return (J2534_ERR_INVALID_MSG_ID);
		}
		enJ2534Error = m_pclsRepeatMsg->QueryRepeat(
			*(unsigned long*)pInput,
			(unsigned long*)pOutput);
	}
	break;

	case STOP_REPEAT_MESSAGE:
	{
		if ((m_pclsPeriodicMsg == NULL) || (m_pclsRepeatMsg == NULL))
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
			return (J2534_ERR_INVALID_MSG_ID);
		}
		enJ2534Error = m_pclsRepeatMsg->StopRepeat((unsigned long*)pInput);
	}
	break;

	default:
		enJ2534Error = J2534_ERR_FAILED;
		break;
	}
	// Write to Log File.
	if (enJ2534Error != J2534_STATUS_NOERROR)
	{
		WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		SetLastErrorText("ProtocolBase : Repeat message failed ");
	}
	else
	{
		WriteLogMsg("ProtocolBase.cpp", "IoctlRepeatMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
	}
	return enJ2534Error;
}
#endif
#ifdef J2534_0500
J2534ERROR CProtocolBase::vLogicalChannelConnect(void * vMsg, J2534_PROTOCOL enProtocolID, unsigned long  *pulChannelID)
{
	
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	J2534ERROR enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

	// If Filter Class is not created, create it now.
	if (m_pclsLogicalChannel == NULL)
	{
		if ((m_pclsLogicalChannel = new CLogicalChannelList (m_pclsDevice, m_ulDevChannelRef , m_pclsPeriodicMsg,m_pclsLog)) == NULL)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelConnect()", DEBUGLOG_TYPE_ERROR, "Unable to create Logical Channel");
			SetLastErrorText("ProtocolBase : Unable to Create Logical Channel");
			return(J2534_ERR_FAILED);
		}
	}


	// Setup Filter.
	if ((enJ2534Error = m_pclsLogicalChannel->CreateLogicalChannel(vMsg, enProtocolID , pulChannelID))
														!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}
J2534ERROR CProtocolBase::vLogicalChannelDisConnect(unsigned long ulChannelID)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	//    char szBuffer[PROTOCOLBASE_ERROR_TEXT_SIZE];
	J2534ERROR enJ2534Error;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelDisConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (m_pclsLogicalChannel == NULL)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelDisConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		return (J2534_ERR_INVALID_CHANNEL_ID);
	}

	// Disconnect Channel.
	if ((enJ2534Error = m_pclsLogicalChannel->DisconnectLogicalChannel(ulChannelID)) != J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelDisConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}
	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vLogicalChannelDisConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}
J2534ERROR CProtocolBase::vReadMsgs(PASSTHRU_MSG *pstrucJ2534Msg,
									unsigned long *pulNumMsgs,
									unsigned long ulTimeout,
									unsigned long ulChannelID)
{
	//*****************************IMPORTANT NOTE******************************* 
	// This is a generic function and so it should not contain anything protocol
	// specific stuff. If any protocol specific stuff is required, implement it
	// in the class derived from this base class.
	//**************************************************************************

	bool            bBufferOverflow = true;
	bool            bOverflowed = false;
	bool            bBufferEmpty = true;
	DWORD           dwStartTick;
	unsigned long   ulReadMsgs;
	unsigned long   ulNumMsgs;

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	dwStartTick = MilliSecondsNow();

	ulReadMsgs = *pulNumMsgs;
	*pulNumMsgs = 0;
	do
	{
		// Check to see if the buffer is overflowed.
		m_pclsRxCircBuffer->GetBufferOverflowStatus(&bBufferOverflow);

		if (bBufferOverflow)
		{
			bOverflowed = true;
		}

		if (pstrucJ2534Msg != NULL)
		{
			ulNumMsgs = ulReadMsgs - *pulNumMsgs;
			if (!m_pclsRxCircBuffer->Read((unsigned char *)(pstrucJ2534Msg + *pulNumMsgs), &ulNumMsgs))
			{
				// Write to Log File.
				WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "Circular Buffer Error");
				SetLastErrorText("ProtocolBase : Circular Buffer Error");
				return(J2534_ERR_FAILED);
			}
			*pulNumMsgs = *pulNumMsgs + ulNumMsgs;
		}
		else
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "Buffer supplied is not big enough");
			SetLastErrorText("ProtocolBase : Buffer supplied is not big enough");
			return(J2534_ERR_FAILED);
		}
		if ((*pulNumMsgs < ulReadMsgs) && ((MilliSecondsNow() - dwStartTick) < ulTimeout))
			Sleep(1);
	} while ((*pulNumMsgs < ulReadMsgs) && ((MilliSecondsNow() - dwStartTick) < ulTimeout));

	if (bOverflowed)
	{
		// Write to Log File.
		WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_OVERFLOW);
		return(J2534_ERR_BUFFER_OVERFLOW);
	}

	if (*pulNumMsgs < ulReadMsgs)
	{
		if (*pulNumMsgs == 0)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_EMPTY);
			return(J2534_ERR_BUFFER_EMPTY);
		}
		if ((MilliSecondsNow() - dwStartTick) >= ulTimeout)
		{
			// Write to Log File.
			WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
			return(J2534_STATUS_NOERROR);
		}
	}

	// Write to Log File.
	WriteLogMsg("ProtocolBase.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}
#endif

