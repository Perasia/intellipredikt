/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: SWISO15765.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              SWISO15765 Class.
* Note:
*
*******************************************************************************/

#ifndef _SWISO15765_H_
#define _SWISO15765_H_

#define SWISO15765_ERROR_TEXT_SIZE		120

#define SWISO15765_MSG_SIZE_MIN			4
#define SWISOCAN_DATA_RATE_DEFAULT      83333

#define SWISO15765_HEADER_SIZE					11
#define SWISO15765_EXTENDED_HEADER_SIZE			29 

#define SWISO15765_MSG_SIZE_MAX_SF	11
#define SWISO15765_MSG_SIZE_MAX		4099
#define SWISOCAN_DATA_RATE_MEDIUM		33333

#define SWISO15765_CAN_MSG_SIZE_MIN        4
#define SWISO15765_CAN_MSG_SIZE_MAX        12  

#define SWISO15765_CAN_NS_RX         (1 << 18)
#define SWISO15765_CAN_HS_RX         (1 << 17)

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnSWISO15765RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CSWISO15765 Class
class CSWISO15765 : public CProtocolBase
{
public:
	CSWISO15765(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
	~CSWISO15765();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
#ifdef J2534_0500
	virtual J2534ERROR					vLogicalConnect(J2534_PROTOCOL  enProtocolID);
#endif
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
    bool                IsCANMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
 	bool				IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);
    bool                IsCANMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR				BlockSize(unsigned long ulValue);
	J2534ERROR				BlockSizeTx(unsigned long ulValue);
	J2534ERROR				STmin(unsigned long ulValue);
	J2534ERROR				STminTx(unsigned long ulValue);
	J2534ERROR				WftMax(unsigned long ulValue);
	J2534ERROR				HSDataRate(unsigned long ulValue);
	J2534ERROR				SpeedChange(unsigned long ulValue);
	J2534ERROR				ResSwitch(unsigned long ulValue);
	J2534ERROR				J1962Pins(unsigned long ulValue);
    J2534ERROR              CANMixedMode(unsigned long ulValue);
 	J2534ERROR				SetErrorReporting(unsigned char *pucValue);

	float					m_fSamplePoint;
	float					m_fJumpWidth;
	unsigned long			m_ulCANIDtype;
	unsigned long			m_ulCANExtAddr;
	J2534_PROTOCOL			m_enSWISO15765Protocol;
	unsigned long			m_ulBlockSize, 
							m_ulBlockSizeTx, 
							m_ulSTmin, 
							m_ulSTminTx, 
							m_ulWftMax;
	unsigned long			m_ulHSDataRate,
							m_ulSWCAN_SpeedChange_Enable,
							m_ulSWCAN_Res_Switch;
	unsigned char			m_ucNormalToHighSpeed;
	bool			        m_bBackToNormalSpeed;
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;
	J2534_CAN_MIXED_FORMAT	m_enCANMixedFormat;
    bool                    m_bIsRunningHighSpeed;
};

#endif
