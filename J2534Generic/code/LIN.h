/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: LIN.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              LIN Class.
* Note:
*
*******************************************************************************/

#ifndef _LIN_H_
#define _LIN_H_

// Constants

#define LIN_ERROR_TEXT_SIZE		120
#define LIN_DATA_RATE_DEFAULT		9600
#define LIN_HEADER_SIZE			0		// Number of Header bits.
#define LIN_MSG_SIZE_MIN			1
#define LIN_MSG_SIZE_MAX			10

typedef char  unsigned  dgUint8;

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnLINRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CLIN Class
class CLIN : public CProtocolBase
{
public:
	CLIN(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CLIN();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);

	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);


public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR				Version(unsigned long ulValue);
};

#endif
