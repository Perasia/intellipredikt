/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: SCI.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support SCI 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "SCI.h"
//bool bFlag;
//unsigned char ucFlag;
//-----------------------------------------------------------------------------
//  Function Name   : CSCI
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CSCI class
//-----------------------------------------------------------------------------
CSCI::CSCI(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) : CProtocolBase(pclsDevice, pclsDebugLog)
{
    
    //  m_pCFilterMsg = NULL;   
    // Write to Log File.
    WriteLogMsg("SCI.cpp", "CSCI()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    //Initialize the speed
    m_ucIOdata = SCI_10_4K;
    m_bLoopback = false;
    // Write to Log File.
    WriteLogMsg("SCI.cpp", "CSCI()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CSCI
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CSCI class
//-----------------------------------------------------------------------------
CSCI::~CSCI()
{
    // Write to Log File.
    WriteLogMsg("SCI.cpp", "~CSCI()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "~CSCI()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vConnect(
                        J2534_PROTOCOL  enProtocolID,
                        unsigned long   ulFlags,
                        unsigned long   ulBaudRate,
                        DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                        LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    switch(enProtocolID)
    {
        case SCI_A_ENGINE:
        case SCI_A_TRANS:
        case SCI_B_ENGINE:
        case SCI_B_TRANS:
            {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
                if ((ulBaudRate != SCI_DATA_RATE_DEFAULT) && (ulBaudRate != SCI_MIDDATA_RATE) &&
                    (ulBaudRate != SCI_MAXDATA_RATE) && (ulBaudRate != (SCI_DATA_RATE_DEFAULT + 1)))
                {
                    return(J2534_ERR_INVALID_BAUDRATE);
                }
#endif
#ifdef J2534_0305
            ulBaudRate = SCI_DATA_RATE_DEFAULT;
#endif
                m_ulDataRate = ulBaudRate;
                m_enSCIProtocol = enProtocolID;
            }
            break;
        default:
            break;
    }
    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate,
                                                OnSCIRxMessage, this))
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    m_usT1MAX = 20;
    m_usT2MAX = 100;
    m_usT3MAX = 50;
    m_usT4MAX = 20;
    m_usT5MAX = 100;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vReadMsgs(PASSTHRU_MSG     *pstPassThruMsg,
                           unsigned long    *pulNumMsgs,
                           unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                            unsigned long   *pulNumMsgs,
                            unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
/*
// THIS HACK IS FOR AN SCI MODULE ONLY FOR REQUESTING PART NUMBER IN HALF-DUPLEX
if ((((pstPassThruMsg + ulIdx1)->ulTxFlags & 0x400000) == 0) && (pstPassThruMsg + ulIdx1)->ucData[0] == 0x2A
    && (pstPassThruMsg + ulIdx1)->ulDataSize == 2)
{
bFlag = true;
(pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x400000;
}
else
bFlag = false;
*/
//if (pstPassThruMsg->ucData[0] == 0x7F)
//pstPassThruMsg->ulTxFlags = 0;
//(pstPassThruMsg + ulIdx1)->ulTxFlags = 0x000000;
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enSCIProtocol)
        {
            // Write to Log File.
            WriteLogMsg("SCI.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("SCI.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vStartPeriodicMsg(PASSTHRU_MSG     *pstPassThruMsg,
                                   unsigned long    *pulMsgID,
                                   unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enSCIProtocol)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		  ,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::vStartMsgFilter(J2534_FILTER      enumFilterType,
                                  PASSTHRU_MSG      *pstMask,
                                  PASSTHRU_MSG      *pstPattern,
                                  PASSTHRU_MSG      *pstFlowControl,
                                  unsigned long     *pulFilterID)
{
    J2534ERROR  enJ2534Error;
    unsigned long   ulOne;
    ulOne = 1;


    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device SCInot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enSCIProtocol) ||
        (pstPattern->ulProtocolID != m_enSCIProtocol))
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // NOTE : In ISO15765, if pstFlowControl is not NULL then check for  
    //        J2534_ERR_MSG_PROTOCOL_ID.

    // NOTE : In ISO15765, pass the pstFlowControl through 
    //        IsMsgValid().

    // NOTE : In ISO15765, check to see that the mask of 4 or 5 bytes are
    //        all 0xFF. The size of pstMask, pstPattern and pstFlowControl
    //        should be 4 or 5 bytes depanding on extended addressing or not
    //        respectively.

    // NOTE : In ISO15765, if the filter type is PASS or BLOCK return error.
    //        In other words only allow FLOW_CONTROL filter type.

    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

        /*FILTERMSGERROR    enumFilterMsgError;

    // See if the Class object is already created.
    if (m_pCFilterMsg == NULL)
        return(J2534_ERR_INVALID_MSG_ID);

    // Stop the periodic message started earlier.
    enumFilterMsgError = m_pCFilterMsg->Delete(ulMsgID);

    if (enumFilterMsgError == FILTERMSG_MSGID_INVALID_ERROR)
        return(J2534_ERR_INVALID_MSG_ID);

    return(J2534_STATUS_NOERROR);*/

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
// THIS HACK IS FOR AN SCI MODULE ONLY FOR NOT DELETING PASS FILTER FOR SCI_B_TRANS
if (m_enSCIProtocol == SCI_B_TRANS)
{
//  return(J2534_STATUS_NOERROR);
}
    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function which should be implemented by
//                    the class derived from this base class.
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::vIoctl(J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput)
{
    J2534ERROR enumJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration    
        {
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        }
        break;
        
    case SET_CONFIG:            // Set configuration
        {
            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
        }
        
        break;
        
    case CLEAR_TX_BUFFER:
        {               // Clear all messages in its transmit queue
            
            m_pclsTxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_RX_BUFFER:// Clear all messages in its receive queue
        {       
            m_pclsRxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        {           
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }
        }
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        {       
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
        }
        break;
        
    case READ_MEMORY:
        {
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                &pInput,
                NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }   
        }
        break;
    case WRITE_MEMORY:
        {
            break;
        }

    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }

    // Write to Log File.
//  WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
    // Write to Log File.
    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    WriteLogMsg("SCI.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);

    // Write to Log File.
    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnSCIRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    SCI messages.
//-----------------------------------------------------------------------------
void OnSCIRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CSCI                    *pclsSCI;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsSCI = (CSCI *) pVoid;

    // Check for NULL pointer.
    if (pclsSCI == NULL)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CSCI.cpp", "OnSCIRx()", 
                             DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        }
        return;
    }

    // Check if the message is valid as per the Connect Flag is set.
/*  if ((pclsSCI->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_SCIBOTH) == 0)
    {
        if (pclsSCI->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_SCI29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_SCI29BIT) == 0)
            {
                return;
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_SCI29BIT) != 0)
            {
                return;
            }
        }
    }*/
    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
    {
        CProtocolBase::m_pclsLog->Write("CSCI.cpp", "OnSCIRx()", 
                         DEBUGLOG_TYPE_COMMENT, "CALLBACK");
    }

    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CSCI.cpp", "OnSCIRx()", 
                             DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        }
/*
// THIS HACK IS FOR AN SCI MODULE ONLY FOR RECEIVING PART NUMBER IN HALF-DUPLEX
if (bFlag && pstPassThruMsg->ucData[0] == 0x2A && pstPassThruMsg->ulDataSize == 2)
{
    bFlag = true;
    ucFlag = pstPassThruMsg->ucData[1];
}
else
bFlag = false;
*/      if (pclsSCI->m_bLoopback == false)
            return;

        // Enqueue to Circ Buffer.
        pclsSCI->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }
    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
    {
        CProtocolBase::m_pclsLog->Write("CSCI.cpp", "OnSCIRx()", 
                         DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
    }
    // Apply Filters and see if msg. is required.
    if (pclsSCI->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsSCI->IsMsgValid(pstPassThruMsg) &&
            pclsSCI->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
/*
// THIS HACK IS FOR AN SCI MODULE ONLY FOR RECEIVING PART NUMBER IN HALF-DUPLEX
if (bFlag)
{
if (pstPassThruMsg->ulDataSize == 1)
{
pstPassThruMsg->ulDataSize = 3;
pstPassThruMsg->ucData[2] = pstPassThruMsg->ucData[0];
pstPassThruMsg->ucData[0] = 0x2A;
pstPassThruMsg->ucData[1] = ucFlag;
}
}
*/      // Enqueue to Circ Buffer.
            pclsSCI->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CSCI::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < SCI_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > SCI_MSG_SIZE_MAX))
    {
        return(false);
    }

/*  if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_SCIBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_SCI29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_SCI29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_SCI29BIT) != 0)
            {
                return(false);
            }
        }
    }*/
    
    return(true);
}

J2534ERROR CSCI::MessageValid(PASSTHRU_MSG     *pstrucJ2534Msg,
                                   unsigned long  *pulNumMsgs)
{
    unsigned long   ulIdx1;
    
    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {       
        if ((pstrucJ2534Msg + ulIdx1)->ulDataSize == 0 ||
            (pstrucJ2534Msg + ulIdx1)->ulDataSize > SCI_MAX_DATALENGTH)
        {
            // Message with data length being 0 bytes or 
            // greater than 4128 bytes
            *pulNumMsgs = 0;
            return(J2534_ERR_INVALID_MSG);
        }
    }

    return(J2534_STATUS_NOERROR);
}


//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("SCI.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("SCI : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case T1_MAX:            // T1_MAX

                pSconfig->ulValue = m_usT1MAX;

                break;

            case T2_MAX:            // T2_MAX

                pSconfig->ulValue = m_usT2MAX;

                break;

            case T3_MAX:            // T3_MAX

                pSconfig->ulValue = m_usT3MAX;

                break;

            case T4_MAX:            // T4_MAX

                pSconfig->ulValue = m_usT4MAX;

                break;

            case T5_MAX:            // T5_MAX

                pSconfig->ulValue = m_usT5MAX;

                break;
            
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("SCI.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("SCI : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                pSconfig->Parameter, pSconfig->ulValue);
        WriteLogMsg("SCI.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
        case DATA_RATE:         // Data Rate

                enumJ2534Error = DataRate(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))
                {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback

                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case T1_MAX:            // T1_MAX

                enumJ2534Error = T1_Max(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                  {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case T2_MAX:            // T2_MAX

                enumJ2534Error = T2_Max(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))
                {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case T3_MAX:            // T3_MAX

                enumJ2534Error = T3_Max(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))
                {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case T4_MAX:            // T4_MAX

                enumJ2534Error = T4_Max(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))
                {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case T5_MAX:            // T5_MAX

                enumJ2534Error = T5_Max(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))
                {
                    // Write to Log File.
                    WriteLogMsg("SCI.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;
            
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue == SCI_DATA_RATE_DEFAULT) || 
        (ulValue == (SCI_DATA_RATE_DEFAULT + 1)) ||
        (ulValue == SCI_MIDDATA_RATE) ||
        (ulValue == SCI_MAXDATA_RATE))
    {
        // Set DataRate to 10.4 kbps
        m_ucIOdata = SCI_10_4K;
    }
    else
    {
        return(J2534_ERR_NOT_SUPPORTED);
    }

        
    m_ulDataRate = ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR CSCI::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T1_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T1_Max
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::T1_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT1MAX = (unsigned short) ulValue;
//  Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T2_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T2_Max
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::T2_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT2MAX = (unsigned short) ulValue;
//  Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T3_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T3_Max
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::T3_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT1MAX = (unsigned short) ulValue;
//  Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T4_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T4_Max
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::T4_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT4MAX = (unsigned short) ulValue;
//  Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T5_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T5_Max
//-----------------------------------------------------------------------------
J2534ERROR  CSCI::T5_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT5MAX = (unsigned short) ulValue;
//  Ptimedata();

    return(enumJ2534Error);
}

/*J2534ERROR CSCI::vSetProgrammingVoltage(unsigned long ulPin,
                                                unsigned long ulVoltage)
{
    char    szBuffer[SCI_ERROR_TEXT_SIZE];
    J2534ERROR enJ2534Error;
    unsigned char ucPin;
    unsigned char ucVoltage;

    ucPin = (unsigned char)ulPin;

    //Voltage shall be in decivolts
    if(ulVoltage == 0xFFFFFFFE) //SHORT TO GROUND
    {
        ucVoltage = 0xFE;
    }
    else if(ulVoltage == 0xFFFFFFFF) // VOLTAGE OFF
    {
        ucVoltage = 0xFF;
    }
    else // Decivolts
    {
        ucVoltage = (unsigned char)(ulVoltage / 100); //Convert from millivolts to decivolts    
    }
    
    if ((enJ2534Error = CProtocolBase::vSetProgrammingVoltage( ulPin,ulVoltage))
        != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("SCI.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
    //  return(enJ2534Error);
    }
    
    return enJ2534Error;
}*/
