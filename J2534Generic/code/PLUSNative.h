/*******************************************************************************
C/C++ global constants and declarations for the PLUSNative API.
Last Modified March 25, 2014

Copyright (c) 2014 Concept Software, Inc.
All rights reserved.

When included in your software, Concept Software, Inc. hereby grants you a
non-exclusive, transferable, royalty-free license to use this Run Time
Distributable Component ("Run-time Tool"), which may be included as part of
your software developed with this Software. This non-exclusive,
transferable, royalty-free license is limited to just your software and not
to any secondary software created by your software.

You shall not remove, obscure, or alter any notices of copyright, trademark
or other proprietary notice or legends appearing in or on any Software, and
associated manuals and documentation, of Concept Software, Inc., and shall
reproduce all such notices on all copies of the Software, and associated
manuals and documentation, as it appears.  You acknowledge and agree any
Software licensed to you by Concept Software, Inc., associated manuals,
associated documentation and associated database structure contain
copyrighted material which is owned exclusively by Concept Software, Inc.
*******************************************************************************/
#ifndef PLUSNATIVE_H
#define PLUSNATIVE_H

#include <stddef.h>
#include <stdio.h>
#include <time.h>
#ifdef WIN32
#include <Windows.h>
#endif

/* The SK_REGION macro is only used for organizing this header file into
 * "regions" in a compiler/platform agnostic way which is similar to use Microsoft's
 * "#pragma region <region name>" and "#pragma endregion" preprocessor directives. */
#ifndef SK_REGION
#define SK_REGION(x) (1)
#endif

#if SK_REGION("Data Types")
    #ifdef WIN32
        #define WINAPI __stdcall
    #else
        #define WINAPI
    #endif

    #ifndef SK_BOOL
        #define SK_BOOL int
    #endif

    #ifndef SK_TRUE
        #define SK_TRUE 1
    #endif

    #ifndef SK_FALSE
        #define SK_FALSE 0
    #endif

    /// <summary>Handle to a SoftwareKey API Context.</summary>
    /// <remarks>
    /// <note type="caution">
    /// <para>
    /// The API Context may only represent a single License File.  When working with
    /// multiple License Files in a single application, one API Context will need to
    /// be created for each License File.
    /// </para>
    /// </note>
    /// <note type="caution">
    /// <para>When you have finished working with an API Context, it needs to be disposed to clear memory properly.  The <see cref="SK_ApiContextDispose"/> function is recommended for this purpose.</para>
    /// </note>
    /// </remarks>
    typedef void *SK_ApiContext;

    /// <summary>Handle to an XML document in memory.</summary>
    /// <remarks>
    /// <note type="caution">
    /// <para>When you have finished working with an XML document, it needs to be disposed to clear memory properly.  The <see cref="SK_XmlDocumentDispose"/> function is recommended for this purpose.</para>
    /// </note>
    /// </remarks>
    typedef void *SK_XmlDoc;

#endif

#if SK_REGION("Preprocessor Macros")
    #if SK_REGION("Function Naming and Prototyping")
        #ifndef SK_EXTERN_TYPED_PROTOTYPE
        #define SK_EXTERN_TYPED_PROTOTYPE(type, name) type WINAPI name
        #endif
        #ifndef SK_EXTERN_PROTOTYPE
        #define SK_EXTERN_PROTOTYPE(name) SK_EXTERN_TYPED_PROTOTYPE(SK_ResultCode,name)
        #endif

    #endif

    #if SK_REGION("Helper Macros")
        #define SK_STRING_TERMINUS ('\0')
        #define SK_STRING_NULLOREMPTY(s) ((s) == NULL || (s)[0] == SK_STRING_TERMINUS)
        #define SK_WSTRING_NULLOREMPTY(s) ((s) == NULL || (s)[0] == L'\0')
        #define SK_FLAG_IS_ENABLED(value, flagConstant) ((value) & (flagConstant))
        #define SK_FLAG_IS_DISABLED(value, flagConstant) (!((value) & (flagConstant)))
        
    #endif

#endif

#if SK_REGION("Flags and Constants")
    #if SK_REGION("Global Flags")
        /// <summary>Use when not specifying any flags for a given function call.</summary>
        /// <remarks>
        /// <note type="caution">
        /// <para>
        ///    Omitting flags in a function call does not prevent global flags from being used.
        ///    Use the <see cref="SK_FLAGS_EXPLICIT_ONLY"/> flag to omit or override global flags for a specific function call.
        /// </para>
        /// </note>
        /// </remarks>
        #define SK_FLAGS_NONE                               (0x00000000)

        // Reserved for internal use.
        #define SK_FLAGS_RESERVED_01                        (0x10000000)
        #define SK_FLAGS_RESERVED_02                        (0x20000000)
        #define SK_FLAGS_RESERVED_03                        (0x40000000)

        /// <summary>When specified in a function call, any global flags in the <see cref="SK_ApiContext">API Context</see> will be ignored.</summary>
        #define SK_FLAGS_EXPLICIT_ONLY                      (0x80000000)

        /// <summary>When specified, the function call will use encryption if applicable.</summary>
        #define SK_FLAGS_USE_ENCRYPTION                     (0x00010000)

        /// <summary>When specified, the function call will use digital signatures.</summary>
        #define SK_FLAGS_USE_SIGNATURE                      (0x00020000)

        /// <summary>When specified, the function call will use Secure Sockets Layering (SSL) if applicable.</summary>
        #define SK_FLAGS_USE_SSL                            (0x00040000)

        /// <summary>When specified, the function call will require Secure Sockets Layering (SSL) if applicable.</summary>
        #define SK_FLAGS_REQUIRE_SSL                        (0x000c0000)

        /// <summary>When specified, the function call will always use 32-bit paths/locations if applicable.  This is sometimes needed to allow 32-bit and 64-bit applications share a license on the same system.</summary>
        #define SK_FLAGS_32BIT_PATHS                        (0x00100000)

        /// <summary>When specified, the function call will use the application's current locale rather than forcing the use of neutral culture settings are omitted.</summary>
        /// <remarks>
        /// <para>This flag is presently only supported in the following functions:</para>
        /// <list type="bullet">
        ///     <item><see cref="SK_XmlNodeGetValueDouble"/></item>
        ///     <item><see cref="SK_XmlNodeGetValueInt"/></item>
        ///     <item><see cref="SK_XmlNodeSetValueDouble"/></item>
        ///     <item><see cref="SK_XmlNodeSetValueInt"/></item>
        /// </list>
        /// <para>
        /// The functions listed above rely on certain standard C string manipulation and formatting routines.  On POSIX-compatible
        /// platforms other than Windows and OS X, the application's locale must be set to the "POSIX" or "C" locale
        /// to function properly; however, this can have unexpected side-effects if your application uses vfork.  To
        /// avoid conflicts while using vfork, you can specify this flag on any relevant functions, and manually set
        /// the locale for your application as appropriate.  Note that this flag is not necessary to use on Windows
        /// or OS X.
        /// </para>
        /// </remarks>
        #define SK_FLAGS_STRING_CULTURE_USE_CURRENT         (0x00200000)

        /// <summary>If specified when calling a function that tries to read or write a license or alias file or Registry key, the <see cref="SK_PermissionsGrantControlToWorld"/> is not called to automatically try to set permissions.</summary>
        /// <remarks>
        /// <para>Functions which support this flag include:</para>
        /// <list type="bullet">
        ///     <item><see cref="SK_PLUS_LicenseFileLoad"/></item>
        ///     <item><see cref="SK_PLUS_LicenseFileSave"/></item>
        ///     <item><see cref="SK_PLUS_LicenseAliasAdd"/></item>
        /// </list>
        /// </remarks>
        #define SK_FLAGS_PERMISSIONS_NEVER_AUTO_SET         (0x00400000)

        /// <summary>When specified, the function call will always use 64-bit paths/locations if applicable.  This is sometimes needed to allow 32-bit applications to initialize license files and aliases in 64 bit locations.</summary>
        #define SK_FLAGS_64BIT_PATHS                        (0x00800000)

        /// <summary>When specified, the function call will not use WMI queries in Windows.  While WMI works in most cases, there are occasionally cases where its use could conflict with protected applications.</summary>
        #define SK_FLAGS_DISABLE_WMI                        (0x01000000)

        /// <summary>This is presently only applicable on Mac OS X (it is ignored on other platforms).  If you link the dependency-free version of the static PLUSNative library for Mac OS X with your own builds of libcurl and OpenSSL, and you configure libcurl to use OpenSSL [--with-openssl] (not DarwinSSL [--with-darwinssl]) for encrypted transmissions, then you may need specify this global flag when calling <see cref="SK_ApiContextInitialize" />.</summary>
        #define SK_FLAGS_VALIDATE_EXPORTED_CA_CERTS         (0x02000000)

    #endif

    /// <summary>If specified when calling <see cref="SK_ApiContextInitialize"/>, any third-party dependencies will be initialized on the thread which calls this function.  This flag must only be used with the first call to <see cref="SK_ApiContextInitialize"/>.</summary>
    #define SK_FLAGS_APICONTEXTINITIALIZE_MAINTHREAD        (0x00000001)

    /// <summary>If specified when calling <see cref="SK_ApiContextDispose"/>, the PLUSNative API will shutdown and free all memory.</summary>
    /// <remarks>
    /// <note type="caution">
    /// <para>
    /// Use of this flag is not thread-safe. This should be used the last time <see cref="SK_ApiContextDispose"/> is called, from the application's main thread, after all other threads have finished processing.
    /// </para>
    /// </note>
    /// </remarks>
    #define SK_FLAGS_APICONTEXTDISPOSE_SHUTDOWN             (0x00000001)

    /// <summary>If specified when calling <see cref="SK_ApiContextDispose"/>, the libcrypto cryptography library will not be uninitialized.</summary>
    #define SK_FLAGS_APICONTEXTDISPOSE_CRYTPO_NOCLEANUP     (0x00000002)

    /// <summary>If specified when calling <see cref="SK_ApiContextDispose"/>, the libcurl HTTP library will not be uninitialized.</summary>
    #define SK_FLAGS_APICONTEXTDISPOSE_CURL_NOCLEANUP       (0x00000004)

    /// <summary>If specified when calling <see cref="SK_ApiContextDispose"/>, the libxml2 XML library will not be uninitialized.</summary>
    #define SK_FLAGS_APICONTEXTDISPOSE_XML2_NOCLEANUP       (0x00000008)

    /// <summary>If specified when an applicable Date-Time function is called, the operation will respect the system's date while effectively disregarding the time by always assuming midnight.</summary>
    #define SK_FLAGS_DATETIME_TIME_AT_MIDNIGHT              (0x00000004)

    /// <summary>If specified when calling <see cref="SK_PermissionsGrantControlToWorld"/> against a directory/folder, items created under the folder (after this call is made successfully) will inherit the new permissions.</summary>
    #define SK_FLAGS_PERMISSIONS_FOLDERINHERIT              (0x00000001)

    /// <summary>If specified when calling <see cref="SK_PermissionsGrantControlToWorld"/> against a file or directory/folder, items affected by the call will also gain execute/traverse folder privileges.</summary>
    #define SK_FLAGS_PERMISSIONS_ALLOWEXECUTE               (0x00000002)

    /// <summary>If specified when calling <see cref="SK_PLUS_NetworkSemaphoreOpen"/>, the function will not attempt to lock existing files (which could be orphaned).  When using this flag, it is necessary to also call <see cref="SK_PLUS_NetworkSemaphoreCleanup"/> to delete orphaned semaphore files; otherwise, <see cref="SK_PLUS_NetworkSemaphoreOpen"/> may prematurely report that no semaphores/seats are available.</summary>
    #define SK_FLAGS_NETWORK_SEMAPHORE_SKIPLOCKATTEMPT      (0x00000001)

    /// <summary>If specified when calling <see cref="SK_PLUS_NetworkSemaphoreCleanup"/> no thread is created, and the function only makes a single attempt to delete orphaned semaphore files.</summary>
    #define SK_FLAGS_NETWORK_SEMAPHORE_NOCLEANUPTHREAD      (0x00000002)

    /// <summary>If specified when calling <see cref="SK_PLUS_NetworkSessionLoad"/> or <see cref="SK_PLUS_NetworkSessionCheckout"/> the function will not attempt to lock the certificate file.</summary>
    #define SK_FLAGS_NETWORKSESSION_SKIPLOCKATTEMPT      (0x00000001)

    /// <summary>If specified when calling <see cref="SK_PLUS_SystemIdentifierAlgorithmAddCurrentIdentifiers" /> with the <see cref="SK_SystemIdentifierAlgorithm.SK_SYSTEM_IDENTIFIER_ALGORITHM_HARD_DISK_VOLUME_SERIAL" /> algorithm, the legacy Mac OS X implementation will be used.</summary>
    #define SK_FLAGS_SYSTEMIDENTIFIER_HARDDISKVOLUMESERIALS_LEGACYOSX (0x00000001)

    /// <summary>When calling SOLO Server web services, this flag will cause <see cref="SK_CallXmlWebService" /> to return the raw response (meaning the response is not decrypted for you automatically by this function).  This flag is necessary to use when calling the XmlNetworkFloatingService web service with PLUSNative.</summary>
    #define SK_FLAGS_WEBSERVICE_RAW_RESULT                  (0x00000100)

    /// <summary>Instant SOLO Server URL for the XmlActivationService web service's ActivateInstallationLicenseFile web method.</summary>
    #define SK_CONST_WEBSERVICE_ACTIVATEINSTALLATION_URL        "secure.softwarekey.com/solo/webservices/XmlActivationService.asmx/ActivateInstallationLicenseFile"

    /// <summary>Instant SOLO Server URL for the XmlActivationService web service's CheckInstallationStatus web method.</summary>
    #define SK_CONST_WEBSERVICE_CHECKINSTALLATION_URL           "secure.softwarekey.com/solo/webservices/XmlActivationService.asmx/CheckInstallationStatus"

    /// <summary>Instant SOLO Server URL for the XmlActivationService web service's DeactivateInstallation web method.</summary>
    #define SK_CONST_WEBSERVICE_DEACTIVATEINSTALLATION_URL      "secure.softwarekey.com/solo/webservices/XmlActivationService.asmx/DeactivateInstallation"

    /// <summary>Instant SOLO Server URL for the XmlLicenseFileService web service's GetLicenseFile web method.</summary>
    #define SK_CONST_WEBSERVICE_GETLICENSEFILE_URL              "secure.softwarekey.com/solo/webservices/XmlLicenseFileService.asmx/GetLicenseFile"

    /// <summary>Instant SOLO Server URL for the XmlNetworkFloatingService web service's OpenSession web method.</summary>
    /// <preliminary />
    #define SK_CONST_WEBSERVICE_OPENSESSION_URL                 "secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx/OpenSession"

    /// <summary>Instant SOLO Server URL for the XmlNetworkFloatingService web service's PollSession web method.</summary>
    /// <preliminary />
    #define SK_CONST_WEBSERVICE_POLLSESSION_URL                 "secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx/PollSession"

    /// <summary>Instant SOLO Server URL for the XmlNetworkFloatingService web service's OpenSession web method.</summary>
    /// <preliminary />
    #define SK_CONST_WEBSERVICE_CLOSESESSION_URL                "secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx/CloseSession"

    /// <summary>Instant SOLO Server URL for the XmlNetworkFloatingService web service's CheckoutSession web method.</summary>
    /// <preliminary />
    #define SK_CONST_WEBSERVICE_CHECKOUTSESSION_URL             "secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx/CheckoutSession"

    /// <summary>Instant SOLO Server URL for the XmlNetworkFloatingService web service's CheckinSession web method.</summary>
    /// <preliminary />
    #define SK_CONST_WEBSERVICE_CHECKINSESSION_URL              "secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx/CheckinSession"

    /// <summary>Instant SOLO Server URL for manually processing web service requests.</summary>
    #define SK_CONST_WEBSERVICE_MANUALREQUEST_URL               "https://secure.softwarekey.com/solo/customers/ManualRequest.aspx"

#endif

#if SK_REGION("Enumerations")
    /// <summary>Possible result/return codes for the PLUSNative API.</summary>
    /// <remarks>
    /// <para>The latest <see href="http://www.softwarekey.com/go/?ID=17" target="_blank">Protection PLUS 5 SDK result codes</see> are documented online.
    /// The latest <see href="http://www.softwarekey.com/go/?ID=20" target="_blank">SOLO Server result codes</see> (returned by SOLO Server web service method calls)
    /// are also documented online.</para>
    /// </remarks>
    typedef enum _SK_ResultCode
    {
        /// <summary>No error.</summary>
        SK_ERROR_NONE                                       = 0,
        /// <summary>The presence of invalid data has been detected.</summary>
        SK_ERROR_INVALID_DATA                               = 9001,
        /// <summary>The presence of an invalid server key has been detected.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_INVALID_SERVER_KEY                         = 9002,
        /// <summary>The presence of an invalid client key has been detected.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_INVALID_CLIENT_KEY                         = 9003,
        /// <summary>The requested decryption operation has failed.</summary>
        SK_ERROR_DECRYPTION_FAILED                          = 9004,
        /// <summary>The requested verification operation has failed.</summary>
        SK_ERROR_VERIFICATION_FAILED                        = 9005,
        /// <summary>The requested encryption operation has failed</summary>
        SK_ERROR_ENCRYPTION_FAILED                          = 9006,
        /// <summary>The requested signing operation has failed.</summary>
        SK_ERROR_SIGNING_FAILED                             = 9007,
        /// <summary>The requested session code verification has failed.</summary>
        SK_ERROR_SESSION_VERIFICATION_FAILED                = 9008,
        /// <summary>An Installation ID is required, but is not present.</summary>
        SK_ERROR_INSTALLATIONID_REQUIRED                    = 9009,
        /// <summary>An invalid "Activation Code 1" value was entered by the user.</summary>
        SK_ERROR_TRIGGER_CODE_INVALID                       = 9010,
        /// <summary>An invalid "Activation Code 2" value was entered by the user.</summary>
        SK_ERROR_TRIGGER_CODE_EVENT_DATA_INVALID            = 9011,
        /// <summary>The license type is invalid or not supported.</summary>
        SK_ERROR_INVALID_LICENSE_TYPE                       = 9012,
        /// <summary>The XML parser encountered an error.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_XML_PARSER_FAILED                          = 9013,
        /// <summary>The requested XML node could not be found.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_XML_NODE_MISSING                           = 9014,
        /// <summary>Some or all of the arguments are invalid.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_INVALID_ARGUMENTS                          = 9015,
        /// <summary>The API context passed into the function call is not valid.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_CONTEXT_INVALID                            = 9016,
        /// <summary>A string conversion operation failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_STRING_CONVERSION_FAILED                   = 9017,
        /// <summary>A date-time conversion operation failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_DATETIME_CONVERSION_FAILED                 = 9018,
        /// <summary>No error actually occurred, but that an evaluation envelope is being used.</summary>
        SK_ERROR_PLUS_EVALUATION_WARNING                    = 9019,
        /// <summary>The Protection PLUS 5 SDK evaluation is invalid or expired.</summary>
        SK_ERROR_PLUS_EVALUATION_INVALID                    = 9020,
        /// <summary>The Product ID is not valid.</summary>
        SK_ERROR_INVALID_PRODUCTID = 9021,
        /// <summary>The Product Option ID is not valid.</summary>
        SK_ERROR_INVALID_PRODUCTOPTIONID = 9022,
        /// <summary>The envelope is not valid.</summary>
        SK_ERROR_ENVELOPE_TYPE_INVALID = 9023,
        /// <summary>The configuration of the requested Web Service is invalid.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_WEBSERVICE_INVALID_CONFIGURATION           = 9100,
        /// <summary>An unexpected failure occurred during an attempt to call a Web Service.</summary>
        SK_ERROR_WEBSERVICE_CALL_FAILED                     = 9101,
        /// <summary>A call to a Web Service succeeded, but the functionality of the Web Service returned an indicator of failure.</summary>
        SK_ERROR_WEBSERVICE_RETURNED_FAILURE                = 9102,
        /// <summary>Validation against SOLO Server is required, but could not be completed.</summary>
        SK_ERROR_REQUIRED_SERVER_VALIDATION_FAILED          = 9103,
        /// <summary>The HTTP client failed to initialize.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_HTTP_INITIALIZATION_FAILED                 = 9104,
        /// <summary>The server could not be reached. Verify that the computer is connected to the Internet, and that the firewall/proxy is set-up properly.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_HTTP_CONNECTION_FAILED                     = 9105,
        /// <summary>The server could not be located. Verify that the computer is connected to the Internet, and that the firewall/proxy is set-up properly.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_HTTP_COULD_NOT_RESOLVE_HOST                = 9106,
        /// <summary>The HTTPS request failed due to an SSL related error.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_SSL_FAILED                                 = 9107,
        /// <summary>License could not be loaded.</summary>
        SK_ERROR_COULD_NOT_LOAD_LICENSE                     = 9200,
        /// <summary>License could not be saved.</summary>
        SK_ERROR_COULD_NOT_SAVE_LICENSE                     = 9201,
        /// <summary>License is not yet effective.</summary>
        SK_ERROR_LICENSE_NOT_EFFECTIVE_YET                  = 9202,
        /// <summary>License has expired.</summary>
        SK_ERROR_LICENSE_EXPIRED                            = 9203,
        /// <summary>Validation of license alias has failed.</summary>
        SK_ERROR_LICENSE_ALIAS_VALIDATION_FAILED            = 9204,
        /// <summary>Validation of license alias time has failed due to mismatch.</summary>
        SK_ERROR_LICENSE_ALIAS_VALIDATION_TIME_MISMATCH     = 9205,
        /// <summary>Network certificate could not be saved.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_SAVE_NETWORK_CERTIFICATE         = 9206,
        /// <summary>The Network Certificate path does not match the path specified during checkout.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_NETWORK_CERTIFICATE_INVALID_PATH           = 9207,
        /// <summary>A valid network session certificate is required, but is not present.</summary>
        /// <remarks>
        /// <para>Only used in PLUSManaged.</para>
        /// </remarks>
        SK_ERROR_NETWORK_CERTIFICATE_REQUIRED               = 9208,
        /// <summary>Could not delete file.</summary>
        SK_ERROR_COULD_NOT_DELETE_FILE                      = 9209,
        /// <summary>The network path is not valid or is unavailable.</summary>
        SK_ERROR_NETWORK_SEMAPHORE_INVALID_PATH             = 9210,
        /// <summary>The number of allowed concurrent users has been reached.</summary>
        SK_ERROR_NETWORK_LICENSE_FULL                       = 9211,
        /// <summary>Failed to create network semaphore.</summary>
        SK_ERROR_NETWORK_SEMAPHORE_LOCK_FAILED              = 9212,
        /// <summary>The activation was successful; however, another activation is required to enable use of this application.</summary>
        SK_ERROR_MODULE_NOT_ACTIVE                          = 9213,
        /// <summary>An attempt to open a file failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_OPEN_FILE                        = 9214,
        /// <summary>An attempt to read a file failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_READ_FILE                        = 9215,
        /// <summary>An attempt to write a file failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_WRITE_FILE                       = 9216,
        /// <summary>An attempt to open a registry key failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_OPEN_REGISTRY_KEY                = 9217,
        /// <summary>An attempt to read a registry key value failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_READ_REGISTRY_KEY                = 9218,
        /// <summary>An attempt to write a registry key value failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_WRITE_REGISTRY_KEY               = 9219,
        /// <summary>An attempt to perform an I/O operation failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_IO_OPERATION_FAILED                        = 9220,
        /// <summary>An attempt to read a file or registry key's permissions failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_READ_PERMISSIONS                 = 9221,
        /// <summary>An attempt to set a file or registry key's permissions failed.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_COULD_NOT_SET_PERMISSIONS                  = 9222,
        /// <summary>Failed to export the SSL client certificate bundle.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_SSL_CERTIFICATE_EXPORT_FAILED              = 9223,
        /// <summary>The client certificate for SSL communication could not be found.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_SSL_CERTIFICATE_UNAVAILABLE                = 9224,
        /// <summary>The volume or downloadable license file could not be found or loaded.</summary>
        SK_ERROR_COULD_NOT_LOAD_VOLUME_DOWNLOADABLE_LICENSE = 9225,
        /// <summary>Verification of system time has failed.</summary>
        SK_ERROR_SYSTEM_TIME_VERIFICATION_FAILED            = 9300,
        /// <summary>System time is not valid.</summary>
        SK_ERROR_SYSTEM_TIME_INVALID                        = 9301,
        /// <summary>The application determined it is running in a virtual machine.</summary>
        SK_ERROR_VIRTUAL_MACHINE_DETECTED                   = 9302,
        /// <summary>The application determined it is running in a remote session.</summary>
        SK_ERROR_REMOTE_SESSION_DETECTED                    = 9303,
        /// <summary>License system identifiers do not match.</summary>
        SK_ERROR_LICENSE_SYSTEM_IDENTIFIERS_DONT_MATCH      = 9400,
        /// <summary>Platform specific API or system call fails.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_PLATFORM_ERROR                             = 9401,
        /// <summary>The current operating system is not supported by this feature or function.</summary>
        SK_ERROR_UNSUPPORTED_OS                             = 9402,
        /// <summary>Memory could not be allocated.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_MEMORY_ALLOCATION                          = 9403,
        /// <summary>Required system library is missing for failed to load.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_LIBRARY_UNAVAILABLE                        = 9404,
        /// <summary>Required library function is missing.</summary>
        /// <remarks>
        /// <para>Only used in PLUSNative.</para>
        /// </remarks>
        SK_ERROR_LIBRARY_FUNCTION_UNAVAILABLE               = 9405
    } SK_ResultCode;

    /// <summary>Field IDs used to retrieve and manipulate integer fields in an <see cref="SK_ApiContext">API Context</see>.</summary>
    typedef enum _SK_ApiContext_IntFields
    {
        /// <summary>Product ID (from SOLO Server) of the application.</summary>
        SK_APICONTEXT_PRODUCTID = 0,
        /// <summary>Product Option ID (from SOLO Server) of the application.  Leave/set to zero (0) to disregard the Product Option ID during activation.</summary>
        SK_APICONTEXT_PRODUCTOPTIONID = 1,
        /// <summary>The amount of time (in seconds) to wait while trying to connect.  Set to zero (0) to wait indefinitely.</summary>
        SK_APICONTEXT_WEBSERVICE_CONNECTTIMEOUT = 2,
        /// <summary>The amount of time (in seconds) to wait while reading/downloading a web service response.  Set to zero (0) to wait indefinitely.</summary>
        SK_APICONTEXT_WEBSERVICE_READTIMEOUT = 3,
        /// <summary>Set to TRUE (1) to use a writable license, or FALSE (0) (which is the default) to use a read-only license.</summary>
        SK_APICONTEXT_LICENSE_WRITABLE = 4,
        /// <summary>Used with network floating sessions, this specifies the allowed amount of difference in time (in minutes) between the licensed system's time and SOLO Server's time.</summary>
        SK_APICONTEXT_NET_SESSION_DATE_TIME_THRESHOLD = 5,
        /// <summary>Used with network floating sessions.  Set to TRUE (1) to validate the network session system identifiers.  Set to FALSE (0) to omit the network session system identifiers validation.</summary>
        SK_APICONTEXT_NET_SESSION_VALIDATE_SYS_IDENTIFIERS = 6,
    } SK_ApiContext_IntFields;

    /// <summary>Field IDs used to retrieve and manipulate string fields in an <see cref="SK_ApiContext">API Context</see>.</summary>
    typedef enum _SK_ApiContext_StringFields
    {
        /// <summary>The version of the application/product.  (Must be formatted like x.x.x.x, where each 'x' represents a positive integer value no larger than 5 digits in length.)</summary>
        SK_APICONTEXT_PRODUCTVERSION = 0,
        /// <summary>The decryption key for the encryption key envelope (from SOLO Server).</summary>
        SK_APICONTEXT_ENVELOPEKEY = 1,
        /// <summary>Optional proxy server information used when calling web services (must be formatted like [host]:[port]).</summary>
        SK_APICONTEXT_WEBSERVICE_PROXY = 2,
        /// <summary>Optional username used for proxy server authentication when calling web services.</summary>
        SK_APICONTEXT_WEBSERVICE_PROXYUSERNAME = 3,
        /// <summary>Optional password used for proxy server authentication when calling web services.</summary>
        SK_APICONTEXT_WEBSERVICE_PROXYPASSWORD = 4
    } SK_ApiContext_StringFields;

    /// <summary>Enumeration for supported types of paths.</summary>
    typedef enum _SK_PathType
    {
        /// <summary>A file on the local system's file system.</summary>
        SK_PATH_TYPE_FILE = 0,
        /// <summary>A registry key value on the local system.</summary>
        SK_PATH_TYPE_REGISTRY = 1,
        /// <summary>A directory/folder on the local system's file system.</summary>
        SK_PATH_TYPE_FOLDER = 2

    } SK_PathType;

    /// <summary>Enumeration for supported SOLO Server product option types.</summary>
    typedef enum _SK_ProductOptionType
    {
        /// <summary>Standard Protection PLUS 4 and 5 activation code.</summary>
        SK_PRODUCT_OPTION_TYPE_ACTIVATIONCODE = 0,
        /// <summary>Protection PLUS 4 and 5 activation code with the quantity of items ordered.</summary>
        SK_PRODUCT_OPTION_TYPE_ACTIVATIONCODE_WITH_QUANTITY = 1,
        /// <summary>Protection PLUS 4 and 5 activation code with an additional, fixed value.</summary>
        SK_PRODUCT_OPTION_TYPE_ACTIVATIONCODE_WITH_FIXEDVALUE = 2,
        /// <summary>Protection PLUS 4 and 5 activation code with the number of days left until the license expires.</summary>
        SK_PRODUCT_OPTION_TYPE_ACTIVATIONCODE_WITH_DAYSLEFT = 3,
        /// <summary>Protection PLUS 5 volume license.</summary>
        SK_PRODUCT_OPTION_TYPE_VOLUME_LICENSE = 4,
        /// <summary>Protection PLUS 5 downloadable license with Protection PLUS 4 compatible trigger code validation.</summary>
        SK_PRODUCT_OPTION_TYPE_DOWNLOADABLE_LICENSE_WITH_TRIGGERCODE_VALIDATION = 5,
        /// <summary>An unknown or custom value. Read the <see cref="License_File_Schema#prodoption">OptionType field</see> to get this value.</summary>
        SK_PRODUCT_OPTION_TYPE_CUSTOM = 6
    } SK_ProductOptionType;

    /// <summary>Enumeration for supported types of string encodings.</summary>
    typedef enum _SK_StringEncoding
    {
        /// <summary>Encode/decode using the code page for the current system locale.</summary>
        SK_STRING_ENCODING_ANSI = 0,
        /// <summary>Encode/decode using UTF-8.</summary>
        SK_STRING_ENCODING_UTF8 = 1
    } SK_StringEncoding;

    /// <summary>Enumeration for built-in system identifier algorithms.</summary>
    typedef enum _SK_SystemIdentifierAlgorithm
    {
        /// <summary>Use for adding current identifiers for all relevant Network Interface Cards (NICs) using their MAC/physical addresses.</summary>
        SK_SYSTEM_IDENTIFIER_ALGORITHM_NIC = 10,
        /// <summary>Use for adding a current identifier for the computer's name.</summary>
        SK_SYSTEM_IDENTIFIER_ALGORITHM_COMPUTER_NAME = 20,
        /// <summary>Use for adding current identifiers for the volume format serials on all accessible partitions on all available hard drives which report themselves as "fixed."  (Note that some removable drives, such as some USB drives, or hard drives placed in USB enclosures, may still report themselves as being "fixed" drives.)</summary>
        SK_SYSTEM_IDENTIFIER_ALGORITHM_HARD_DISK_VOLUME_SERIAL = 30,
        /// <summary>Use for adding identifiers a network share location that use being used to store the applications license file and/or network semaphore files.</summary>
        SK_SYSTEM_IDENTIFIER_ALGORITHM_NETWORK_NAME = 40,
        /// <summary>Use for adding an identifier for the username of the user which launched the calling process (the real user for Linux and Mac OS X -- the effective user cannot be retrieved presently) or thread (Windows).</summary>
        SK_SYSTEM_IDENTIFIER_ALGORITHM_USER_NAME = 50
    } SK_SystemIdentifierAlgorithm;

    /// <summary>Enumeration for virtual machine types/hypervisors.</summary>
    typedef enum _SK_VirtualMachineTypes
    {
        ///<summary>None</summary>
        SK_VIRTUAL_MACHINE_NONE =           0x00000000,
        /// <summary>Microsoft Hyper V</summary>
        SK_VIRTUAL_MACHINE_HYPERV =         0x00000001,
        /// <summary>Microsoft Virtual PC</summary>
        SK_VIRTUAL_MACHINE_VIRTUALPC =      0x00000002,
        /// <summary>Oracle VirtualBox</summary>
        SK_VIRTUAL_MACHINE_VIRTUALBOX =     0x00000004,
        /// <summary>VMWare</summary>
        SK_VIRTUAL_MACHINE_VMWARE =         0x00000008,
        /// <summary>Parallels</summary>
        SK_VIRTUAL_MACHINE_PARALLELS =      0x00000010,
        /// <summary>Citrix XenServer</summary>
        SK_VIRTUAL_MACHINE_XENSERVER =      0x00000020
    } SK_VirtualMachineTypes;

    /// <summary>Enumeration for types of remote sessions.</summary>
    typedef enum _SK_RemoteSessionTypes
    {
        /// <summary>None</summary>
        SK_REMOTE_SESSION_NONE = 0x00000000,
        /// <summary>Terminal Services / Remote Desktop</summary>
        SK_REMOTE_SESSION_TERMINAL_SERVICES = 0x00000001
    } SK_RemoteSessionTypes;

#endif

#if SK_REGION("Function Prototypes")
    #ifdef __cplusplus
    extern "C" {
    #endif

    #if SK_REGION("API Context Functions")
        SK_EXTERN_PROTOTYPE(SK_ApiContextDispose)(int flags, SK_ApiContext *context);
        SK_EXTERN_PROTOTYPE(SK_ApiContextInitialize)(int flags, SK_BOOL licenseWritable, int prodId, int prodOptionId, const char *prodVersion, const char *envelope, SK_ApiContext *context);
        SK_EXTERN_PROTOTYPE(SK_ApiContextSetFieldInt)(SK_ApiContext context, int flags, SK_ApiContext_IntFields field, int value);
        SK_EXTERN_PROTOTYPE(SK_ApiContextSetFieldString)(SK_ApiContext context, int flags, SK_ApiContext_StringFields field, const char *value);
        SK_EXTERN_PROTOTYPE(SK_ApiResultCodeToString)(int flags, SK_ResultCode resultCode, char **description);

    #endif

    #if SK_REGION("Helper Functions")
        #if SK_REGION("String Helper Functions")
            SK_EXTERN_PROTOTYPE(SK_StringConvertMultiByteStringToWideString)(int flags, SK_StringEncoding encoding, const char *source, wchar_t **result);
            SK_EXTERN_PROTOTYPE(SK_StringConvertWideStringToMultiByteString)(int flags, SK_StringEncoding encoding, const wchar_t *source, char **result);
            SK_EXTERN_PROTOTYPE(SK_StringCopyToBuffer)(int flags, const char *source, char *destination, int destinationSize);
            SK_EXTERN_PROTOTYPE(SK_StringCopyToBufferW)(int flags, const wchar_t *source, wchar_t *destination, int destinationSize);
            SK_EXTERN_PROTOTYPE(SK_StringDispose)(int flags, char **ptr);
            SK_EXTERN_PROTOTYPE(SK_StringDisposeW)(int flags, wchar_t **ptr);
            SK_EXTERN_TYPED_PROTOTYPE(int, SK_StringGetLength)(const char *source);
            SK_EXTERN_TYPED_PROTOTYPE(int, SK_StringGetLengthW)(const wchar_t *source);
            SK_EXTERN_PROTOTYPE(SK_StringToVersionNumbers)(int flags, const char *version, int *major, int *minor, int *revision, int *build);

        #endif
    
        #if SK_REGION("File System and Registry Helper Functions")
            SK_EXTERN_PROTOTYPE(SK_FileDelete)(int flags, const char *path);
            SK_EXTERN_PROTOTYPE(SK_FilePathIsRemote)(int flags, const char *path, SK_BOOL *pathIsRemote);
            SK_EXTERN_PROTOTYPE(SK_FileReadAllText)(int flags, const char *path, char **text);
            SK_EXTERN_PROTOTYPE(SK_FileWriteAllText)(int flags, const char *path, const char *text);
            SK_EXTERN_PROTOTYPE(SK_PermissionsGrantControlToWorld)(int flags, SK_PathType type, const char *path);
            SK_EXTERN_PROTOTYPE(SK_RegistryValueGet)(int flags, const char *path, char **value);
            SK_EXTERN_PROTOTYPE(SK_RegistryValueSet)(int flags, const char *path, const char *value);

        #endif

        #if SK_REGION("XML Helper Functions")
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentCreateFromString)(int flags, const char *input, SK_XmlDoc *xml);
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentDecryptRsa)(SK_ApiContext context, int flags, SK_BOOL selfSigned, SK_XmlDoc cipherText, SK_XmlDoc *plainText);
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentDispose)(int flags, SK_XmlDoc *xml);
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentEncryptRsa)(SK_ApiContext context, int flags, SK_BOOL selfSign, SK_XmlDoc plainText, SK_XmlDoc *cipherText);
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentGetDocumentString)(int flags, SK_XmlDoc xml, char **content);
            SK_EXTERN_PROTOTYPE(SK_XmlDocumentLoadFile)(int flags, const char *path, SK_XmlDoc *xml);
            SK_EXTERN_PROTOTYPE(SK_XmlElementAddNew)(int flags, SK_XmlDoc xml, const char *xpath, const char *element, const char *value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeGetDocument)(int flags, SK_XmlDoc input, const char *xpath, SK_XmlDoc *output);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeGetValueDateTimeString)(int flags, SK_XmlDoc xml, const char *xpath, char **value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeGetValueDouble)(int flags, SK_XmlDoc xml, const char *xpath, double *value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeGetValueInt)(int flags, SK_XmlDoc xml, const char *xpath, int *value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeGetValueString)(int flags, SK_XmlDoc xml, const char *xpath, SK_BOOL includeInnerXML, char **value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeSetDocument)(int flags, SK_XmlDoc xml, const char *xpath, SK_XmlDoc subDoc);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeSetValueDateTimeString)(int flags, SK_XmlDoc xml, const char *xpath, const char *value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeSetValueDouble)(int flags, SK_XmlDoc xml, const char *xpath, double value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeSetValueInt)(int flags, SK_XmlDoc xml, const char *xpath, int value);
            SK_EXTERN_PROTOTYPE(SK_XmlNodeSetValueString)(int flags, SK_XmlDoc xml, const char *xpath, const char *value);

        #endif

        #if SK_REGION("Date-Time Helper Functions")
            SK_EXTERN_PROTOTYPE(SK_DateTimeAddDays)(int flags, char *dateTime, int days);
            SK_EXTERN_PROTOTYPE(SK_DateTimeAddMinutes)(int flags, char *dateTime, int minutes);
            SK_EXTERN_PROTOTYPE(SK_DateTimeCompareStrings)(int flags, const char *dateTime1, const char *dateTime2, int *comparison);
            SK_EXTERN_PROTOTYPE(SK_DateTimeDaysRemaining)(int flags, const char *dateTime, int *days);
            SK_EXTERN_PROTOTYPE(SK_DateTimeGetCurrentString)(int flags, char **dateTime);
            SK_EXTERN_PROTOTYPE(SK_DateTimeParseString)(int flags, const char *dateTime, time_t *t);
            SK_EXTERN_PROTOTYPE(SK_DateTimeTimeRemaining)(int flags, const char *dateTime, int *minutes);
            SK_EXTERN_PROTOTYPE(SK_DateTimeToFormattedString)(int flags, const char *dateTime, const char *format, char **value);
            SK_EXTERN_PROTOTYPE(SK_DateTimeToString)(int flags, time_t t, char **dateTime);
            SK_EXTERN_PROTOTYPE(SK_DateTimeValidateApi)(int flags, int testDuration, int thresholdPercent, int *change);

        #endif

        #if SK_REGION("Miscellaneous Helper Functions")
            SK_EXTERN_PROTOTYPE(SK_SessionCodeGenerate)(int flags, char **sessionCode);

        #endif

    #endif

    #if SK_REGION("Protection PLUS Functions")
        #if SK_REGION("License File Functions")
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseCreateNew)(SK_ApiContext context, int flags, int daysTillExpiration, SK_XmlDoc *license);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseDispose)(SK_ApiContext context, int flags);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseFileLoad)(SK_ApiContext context, int flags, const char *path);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseFileSave)(SK_ApiContext context, int flags, const char *path, SK_XmlDoc license);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseGetDocumentString)(SK_ApiContext context, int flags, char **content);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseGetXmlDocument)(SK_ApiContext context, int flags, SK_XmlDoc *license);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseLoad)(SK_ApiContext context, int flags, SK_XmlDoc license);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseProductOptionGetType)(SK_ApiContext context, int flags, SK_ProductOptionType *type);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseProductOptionSetType)(SK_ApiContext context, int flags, SK_ProductOptionType type);

        #endif

        #if SK_REGION("Network Session Functions")
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionOpen)(SK_ApiContext context, int flags, SK_XmlDoc certificate);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionPoll)(SK_ApiContext context, int flags, SK_XmlDoc certificate);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionClose)(SK_ApiContext context, int flags);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionCheckout)(SK_ApiContext context, int flags, SK_XmlDoc certificate);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionCheckin)(SK_ApiContext context, int flags, SK_XmlDoc certificate);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionLoad)(SK_ApiContext context, int flags, const char *path);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionGetCurrent)(SK_ApiContext context, int flags, SK_XmlDoc *certificate);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSessionDispose)(SK_ApiContext context, int flags);

        #endif

        #if SK_REGION("Network Semaphore Functions")
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSemaphoreCleanup)(SK_ApiContext context, int flags, const char *semaphorePath, const char *semaphorePrefix, int delay);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSemaphoreDispose)(SK_ApiContext context, int flags);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSemaphoreOpen)(SK_ApiContext context, int flags, int seatsAllowed, const char *seatsAllowedXPath, const char *semaphorePath, const char *semaphorePrefix, const char *optionalIdentifier, char **filePath);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSemaphoreStatistics)(SK_ApiContext context, int flags, const char *semaphorePath, const char *semaphorePrefix, int *seatsAllowed, int *seatsAllocated, int *seatsAvailable);
            SK_EXTERN_PROTOTYPE(SK_PLUS_NetworkSemaphoreVerify)(SK_ApiContext context, int flags);

        #endif

        #if SK_REGION("License Alias Functions")
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseAliasAdd)(SK_ApiContext context, int flags, const char *path);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseAliasGetCount)(SK_ApiContext context, int flags, int *count);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseAliasGetValidatedCount)(SK_ApiContext context, int flags, int *count);
            SK_EXTERN_PROTOTYPE(SK_PLUS_LicenseAliasGetTotal)(SK_ApiContext context, int flags, int *count);

        #endif

        #if SK_REGION("System Identifier Functions")
            SK_EXTERN_PROTOTYPE(SK_PLUS_SystemIdentifierAddCurrentIdentifier)(SK_ApiContext context, int flags, const char *name, const char *type, const char *value);
            SK_EXTERN_PROTOTYPE(SK_PLUS_SystemIdentifierAlgorithmAddCurrentIdentifiers)(SK_ApiContext context, int flags, SK_SystemIdentifierAlgorithm algorithm, const char *algorithmDataString, int *count);
            SK_EXTERN_PROTOTYPE(SK_PLUS_SystemIdentifierCompare)(SK_ApiContext context, int flags, const char *type, int *count, int *matches);
            SK_EXTERN_PROTOTYPE(SK_PLUS_SystemIdentifierCurrentGetContents)(SK_ApiContext context, int flags, SK_XmlDoc *identifiers);
            SK_EXTERN_PROTOTYPE(SK_PLUS_SystemIdentifierCurrentSetContents)(SK_ApiContext context, int flags, SK_XmlDoc identifiers);

        #endif

        #if SK_REGION("PLUS4 Functions")
            #if SK_REGION("Automation Client Functions")
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetLicenseStatusRequest)(SK_ApiContext context, int flags, const char *url, int licenseId, const char *password, int productId, char **requestUrl);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetLicenseStatusResponse)(SK_ApiContext context, int flags, const char *response, int *errorCode, char **licenseStatus, int *licenseReplacedBy, char **licenseUpdate);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetRegDataRequest)(SK_ApiContext context, int flags, const char *url, int licenseId, const char *password, char **requestUrl);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetRegDataResponse)(SK_ApiContext context, int flags, const char *response, int *errorCode, char **company, char ** contact, char **email, char **phone, char **userDefinedString1, char **userDefinedString2, char **userDefinedString3, char **userDefinedString4, char **userDefinedString5);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetTcDataRequest)(SK_ApiContext context, int flags, const char *url, int licenseId, const char *password, int userCode1, int userCode2, int productId, int productOptionId, char **requestUrl);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_GetTcDataResponse)(SK_ApiContext context, int flags, const char *response, int *errorCode, int *activationCode1, int *activationCode2, char **licenseUpdate);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_PostEvalDataRequest)(SK_ApiContext context, int flags, int productId, const char *email, const char *companyName, int distributorId, const char *firstName, const char *lastName, const char *address1, const char *address2, const char *city, const char *stateProvince, const char *postalCode, const char *country, const char *phone, const char *source, char **postString);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_PostEvalDataResponse)(SK_ApiContext context, int flags, const char *response, int *errorCode, int *registrationId);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_PostRegDataRequest)(SK_ApiContext context, int flags, int licenseId, const char *password,  const char *companyName, const char *firstName, const char *lastName, const char *address1, const char *address2, const char *city, const char *stateProvince, const char *postalCode, const char *country, const char *phone, const char *fax, const char *email, SK_BOOL overwrite, SK_BOOL notifyProduct, SK_BOOL notifyPartners, char **postString);
                SK_EXTERN_PROTOTYPE(SK_PLUS4_PostRegDataResponse)(SK_ApiContext context, int flags, const char *response, int *errorCode);

            #endif
            SK_EXTERN_PROTOTYPE(SK_PLUS4_GenerateUserCode1Value)(int flags, int *userCode1);
            SK_EXTERN_PROTOTYPE(SK_PLUS4_GenerateUserCode2Value)(SK_ApiContext context, int flags, int *userCode2);
            SK_EXTERN_PROTOTYPE(SK_PLUS4_NDecrypt)(int flags, int number, int seed, int *result);
            SK_EXTERN_PROTOTYPE(SK_PLUS4_NEncrypt)(int flags, int number, int seed, int *result);
            SK_EXTERN_PROTOTYPE(SK_PLUS4_ValidateTriggerCode)(int flags, int activationCode1, int activationCode2, int userCode1, int userCode2, int tcSeed, int dataSeed, int *code, int *tcData);

        #endif

    #endif

    #if SK_REGION("System Information Functions")
        SK_EXTERN_PROTOTYPE(SK_SystemRemoteSessionDetect)(SK_ApiContext context, int flags, SK_RemoteSessionTypes *sessionDetected);
        SK_EXTERN_PROTOTYPE(SK_SystemVirtualMachineDetect)(SK_ApiContext context, int flags, SK_VirtualMachineTypes *vmDetected);

    #endif

    #if SK_REGION("XML Web Service Functions")
        SK_EXTERN_PROTOTYPE(SK_CallXmlWebService)(SK_ApiContext context, int flags, const char *url, SK_XmlDoc request, SK_XmlDoc *response, int *resultCode, int *statusCode);
        SK_EXTERN_PROTOTYPE(SK_HttpConnectionTest)(SK_ApiContext context, int flags, const char *url, const char *searchString, char **response, int *statusCode);
        SK_EXTERN_PROTOTYPE(SK_HttpRequest)(SK_ApiContext context, int flags, const char *url, const char *postString, char **response, int *statusCode);
        SK_EXTERN_PROTOTYPE(SK_HttpUrlEncodeString)(int flags, char *input, const char *label, const char *separator, char **output);
        #if SK_REGION("SOLO Server Web Service Functions")
            SK_EXTERN_PROTOTYPE(SK_SOLO_ActivateInstallationGetRequest)(SK_ApiContext context, int flags, int licenseId, const char *pw, const char *serialNum, int userCode1, int userCode2, SK_BOOL requireRegistration, const char *installationName, const char *prevInstallationId, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_CheckInstallationStatusGetRequest)(SK_ApiContext context, int flags, const char *installationId, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_DeactivateInstallationGetRequest)(SK_ApiContext context, int flags, const char *installationId, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_GetLicenseFileGetRequest)(SK_ApiContext context, int flags, const char *installationId, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_ManualRequestFileLoad)(SK_ApiContext context, int flags, const char *path, SK_XmlDoc *response);
            SK_EXTERN_PROTOTYPE(SK_SOLO_ManualRequestFileSave)(SK_ApiContext context, int flags, const char *url, const char *customMarkupTop, const char *customMarkupBottom, const char *path, SK_XmlDoc request);
            SK_EXTERN_PROTOTYPE(SK_SOLO_NetworkSessionOpenGetRequest)(SK_ApiContext context, int flags, int licenseId, const char *pw, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_NetworkSessionPollGetRequest)(SK_ApiContext context, int flags, const char *sessionId, const char* certificatePath, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_NetworkSessionCloseGetRequest)(SK_ApiContext context, int flags, const char *sessionId, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_NetworkSessionCheckoutGetRequest)(SK_ApiContext context, int flags, const char *sessionId, const char* certificatePath, double requestedCheckoutDuration, SK_XmlDoc *request, char **sessionCode);
            SK_EXTERN_PROTOTYPE(SK_SOLO_NetworkSessionCheckinGetRequest)(SK_ApiContext context, int flags, const char *sessionId, SK_XmlDoc *request, char **sessionCode);

        #endif

    #endif

    #if SK_REGION("Debug Functions")
        SK_EXTERN_TYPED_PROTOTYPE(SK_BOOL, SK_Test)(const char *path, FILE *file);

    #endif

    #ifdef __cplusplus
    }
    #endif
#endif
#endif
