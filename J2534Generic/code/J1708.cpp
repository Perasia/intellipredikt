/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1708.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support J1708 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "J1708.h"

//-----------------------------------------------------------------------------
//  Function Name   : CJ1708
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CJ1708 class
//-----------------------------------------------------------------------------
CJ1708::CJ1708(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog) : CProtocolBase(pclsDevice, pclsDebugLog, pclsDataLog)
{
    // Write to Log File.
    WriteLogMsg("J1708.cpp", "CJ1708()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "CJ1708()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ1708
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CJ1708 class
//-----------------------------------------------------------------------------
CJ1708::~CJ1708()
{
    // Write to Log File.
    WriteLogMsg("J1708.cpp", "~CJ1708()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "~CJ1708()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vConnect(J2534_PROTOCOL  enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    /*Get the Check Sum Flag from the connection flags*/
    m_ulChecksumFlag = ((ulFlags >> 9) & 0x01);
//  ulFlags = m_ulChecksumFlag;
    ulFlags = (((ulFlags >> 12) & 0x01) << 1);
    ulFlags |= m_ulChecksumFlag;
    switch(enProtocolID)
    {
    case J1708_PS:
    case J1708_CH1:
        {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            if ((ulBaudRate != 9600))
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
#ifdef J2534_0305
            ulBaudRate = J1708_DATA_RATE_DEFAULT;
#endif
            m_ulDataRate = ulBaudRate;
            m_enJ1708Protocol = enProtocolID;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnJ1708RxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }                        

    m_ulJ1708PPSS = 0;
    m_bJ1708Pins = false; 
    m_ulJ1939PPSS = 0;
    m_bJ1939Pins = false;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vReadMsgs(PASSTHRU_MSG       *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Pins
    if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1708.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vWriteMsgs(PASSTHRU_MSG  *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1708Protocol)
        {
            // Write to Log File.
            WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    // Check Pins
    if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }   
    
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vStartPeriodicMsg(PASSTHRU_MSG       *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enJ1708Protocol)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Pins
    if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    } 

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1708::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::vStartMsgFilter(J2534_FILTER    enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enJ1708Protocol) ||
        (pstPattern->ulProtocolID != m_enJ1708Protocol))
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Pins
    if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetLastErrorText("J1708 : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration

            if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
            {
                WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
                return(J2534_ERR_PIN_INVALID);
            }
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
            {
                WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
                return(J2534_ERR_PIN_INVALID);
            }
            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
            {
                WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
                return(J2534_ERR_PIN_INVALID);
            }
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
            {
                WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
                return(J2534_ERR_PIN_INVALID);
            }
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
            {
                WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
                return(J2534_ERR_PIN_INVALID);
            }
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnJ1708Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    J1708 messages.
//-----------------------------------------------------------------------------
void OnJ1708RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CJ1708                  *pclsJ1708;
    FILTERMSG_CONFORM_REQ   stConformReq; 
    unsigned long   i, j;
    char            szChannelPins[MAX_PATH];
    char            szProtocol[MAX_PATH];
    unsigned long   ulRxTx;
    unsigned long   ulTimestamp;
    char            szID[MAX_PATH];
    char            szData[MAX_PATH];

    pclsJ1708 = (CJ1708 *) pVoid;

    // Check for NULL pointer.
    if (pclsJ1708 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CJ1708.cpp", "OnJ1708RxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    memset(szChannelPins,0,MAX_PATH);
    memset(szProtocol,0,MAX_PATH);
    ulRxTx = 0;
    ulTimestamp = 0;
    memset(szID,0,MAX_PATH);
    memset(szData,0,MAX_PATH);

    // Data Logging Protocol and its Channel/Pins
    switch(pstPassThruMsg->ulProtocolID)
    {
    case J1708_PS:
        if (pclsJ1708->m_bJ1708Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1708 Pins 0x%04X", pclsJ1708->m_ulJ1708PPSS);
        }
        if (pclsJ1708->m_bJ1939Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1939 Pins 0x%04X", pclsJ1708->m_ulJ1939PPSS);
        }
        sprintf_s(szProtocol,"J1708");
        break; 
    case J1708_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"J1708");
        break;
    default:
        break;
    }

    // Data Logging Rx/Tx
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        ulRxTx = DATALOG_TX;
    }
    else
    {
        ulRxTx = DATALOG_RX;
    }

    // Data Logging timestamp
    ulTimestamp = pstPassThruMsg->ulTimeStamp;

    // Data Logging ID
    if(pstPassThruMsg->ulDataSize > 0)
    {
        j = 0;

        for(i = 0; i < pstPassThruMsg->ulDataSize && i < 15; i++)
        {
            j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
        }

        if(i >= 15)
            j += sprintf_s(szData + j, sizeof(szData)-j, "...");
    }

    // Write to Data Log File.
    CProtocolBase::WriteDataLogMsg(szChannelPins, szProtocol, ulRxTx, ulTimestamp, szID, szData);

    // Write to Log File.
    CProtocolBase::WriteLogMsg("CJ1708.cpp", "OnJ1708RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Message CallBack");

    // Check if this is a Rx First Byte message.
    if (pstPassThruMsg->ulRxStatus & 0x02)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CJ1708.cpp", "OnJ1708RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx First Byte CALLBACK");

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        // Enqueue to Circ Buffer.
        pclsJ1708->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#endif
        return;
    }
    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CJ1708.cpp", "OnJ1708RxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsJ1708->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsJ1708->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Apply Filters and see if msg. is required.
    if (pclsJ1708->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsJ1708->IsMsgValid(pstPassThruMsg) &&
            pclsJ1708->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
#ifdef J2534_0305
            if (pclsJ1708->m_ulChecksumFlag)
                pstPassThruMsg->ulExtraDataIndex--;
#endif
            // Enqueue to Circ Buffer.
            pclsJ1708->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#ifdef J2534_0305
    else
    {
        if (pclsJ1708->IsMsgValid(pstPassThruMsg))
        {
            // Enqueue to Circ Buffer.
            pclsJ1708->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#endif
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1708::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J1708_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1708_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1708.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1708 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;        
                
            case J1708_PINS:

                pSconfig->ulValue = m_ulJ1708PPSS;

                break;     
                
            case J1939_PINS:

                pSconfig->ulValue = m_ulJ1939PPSS;

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue == 9600)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1708::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1708.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1708 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                            pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = DataRate(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1708.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback

                if((m_enJ1708Protocol == J1708_PS) && (!m_bJ1708Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Loopback(pSconfig->ulValue);
                }

                break;

            case J1708_PINS:
                if (m_enJ1708Protocol != J1708_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1708Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1708Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulJ1708PPSS = pSconfig->ulValue;
                    else
                        m_bJ1708Pins = false;
                }
                break;                                   

            case J1939_PINS:
                if (m_enJ1708Protocol != J1708_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1708Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1939Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("J1708.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulJ1939PPSS = pSconfig->ulValue;
                    else
                        m_bJ1939Pins = false;
                }
                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}  

J2534ERROR  CJ1708::J1708Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x0900) || ((ulValue & 0x00FF) > 0x0009))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 3) || (ulValue == 3) ||
        ((ulValue >> 8) == 5) || (ulValue == 5))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0102))
    {
        m_bJ1708Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}       

J2534ERROR  CJ1708::J1939Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x0900) || ((ulValue & 0x00FF) > 0x0009))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 1) || (ulValue == 1) ||
        ((ulValue >> 8) == 2) || (ulValue == 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0607))
    {
        m_bJ1939Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}
