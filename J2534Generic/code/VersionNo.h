#if defined (J2534_DEVICE_CARDONE)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2015 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        2,7,0,0
    #define PRODUCTVER     2,7,0,0
    #define STRFILEVER     "2.07\0"
    #define STRPRODUCTVER  "2.07\0"
#else
#if defined (J2534_DEVICE_VSI)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        2,9,0
    #define PRODUCTVER     2,9,0
    #define STRFILEVER     "2.09\0"
    #define STRPRODUCTVER  "2.09\0"
#else
#if defined (J2534_DEVICE_KRHTWIRE)
    #define CONAME         "KeylessRide HotWire\0"
    #define FILEDESC       "KeylessRide HotWire - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "KeylessRide HotWire - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,5,1
    #define PRODUCTVER     1,5,1
    #define STRFILEVER     "1.05.01\0"
    #define STRPRODUCTVER  "1.05.01\0"
#else
#if defined (J2534_DEVICE_DPA5)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,2,2
    #define PRODUCTVER     1,2,2
    #define STRFILEVER     "1.02.02\0"
    #define STRPRODUCTVER  "1.02.02\0"
#else
#if defined (J2534_DEVICE_NETBRIDGE)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        2,1,0
    #define PRODUCTVER     2,1,0
    #define STRFILEVER     "2.01\0"
    #define STRPRODUCTVER  "2.01\0"
#else
#if defined (J2534_DEVICE_SOFTBRIDGE)
    #define CONAME         "Autocom Diagnostic Partner AB\0"
    #define FILEDESC       "Autocom Diagnostic Partner AB - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "Autocom Diagnostic Partner AB - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,0,30
    #define PRODUCTVER     1,0,30
    #define STRFILEVER     "1.00.30\0"
    #define STRPRODUCTVER  "1.00.30\0"
#else
#if defined (J2534_DEVICE_WURTH)
    #define CONAME         "W�rth Online World!\0"
    #define FILEDESC       "W�rth Online World! - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2015 Dearborn Group, Inc.\0"
    #define PRODNAME       "W�rth Online World! - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,0,30
    #define PRODUCTVER     1,0,30
    #define STRFILEVER     "1.00.30\0"
    #define STRPRODUCTVER  "1.00.30\0"
#else
#if defined (J2534_DEVICE_DELPHI)
    #define CONAME         "Delphi\0"
    #define FILEDESC       "Delphi - J2534Generic DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "Delphi - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,0,30
    #define PRODUCTVER     1,0,30
    #define STRFILEVER     "1.00.30\0"
    #define STRPRODUCTVER  "1.00.30\0"
#else
#if defined (J2534_DEVICE_DBRIDGE) && !defined (J2534_DEVICE_SVCI)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534 d-briDGe DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,8,0
    #define PRODUCTVER     1,8,0
    #define STRFILEVER     "1.08\0"
    #define STRPRODUCTVER  "1.08\0"
#else
#if defined (J2534_DEVICE_SVCI)
    #define CONAME         "Zhiche Automotive Technologies\0"
    #define FILEDESC       "Zhiche - J2534 SVCI DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2017 Zhiche Automotive Technologies\0"
    #define PRODNAME       "Zhiche - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,7,4
    #define PRODUCTVER     1,7,4
    #define STRFILEVER     "1.07.04\0"
    #define STRPRODUCTVER  "1.07.04\0"
#else
#if defined (J2534_DEVICE_DBRIDGE0305)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534 d-briDGe 0305 DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,8,0
    #define PRODUCTVER     1,8,0
    #define STRFILEVER     "1.08\0"
    #define STRPRODUCTVER  "1.08\0"
#else
#if defined (J2534_DEVICE_DPA50305)
    #define CONAME         "Dearborn Group, Inc.\0"
    #define FILEDESC       "DG Technologies - J2534 DPA5 0305 DLL\0"
    #define INTERNALNAME   "J2534Generic DLL\0"
    #define LEGALCOPYRT    "Copyright � 2005-2017 Dearborn Group, Inc.\0"
    #define PRODNAME       "DG Technologies - J2534Generic Dynamic Link Library\0"
    #define FILEVER        1,2,2
    #define PRODUCTVER     1,2,2
    #define STRFILEVER     "1.02.02\0"
    #define STRPRODUCTVER  "1.02.02\0"
#else
#error  ERROR: VersionNo.h J2534 Device not Defined
#endif //dpa50305
#endif //dbridge0305
#endif //SVCI d-bridge
#endif //d-bridge
#endif //delphi
#endif //wurth
#endif //softbridge
#endif //netbridge
#endif //dpa5
#endif //krhw
#endif //vsi
#endif //cardone
