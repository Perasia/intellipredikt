/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: ProtocolManager.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of class used for
*              creating, deleting and management protocol objects.
* Note:
*
*******************************************************************************/

#ifndef _PROTOCOLMANAGER_H_
#define _PROTOCOLMANAGER_H_

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

#include "DebugLog.h"
#include "DataLog.h"
#include "FT_CAN.h"
#include "CAN.h"
#include "SWCAN.h"
#include "SWISO15765.h"
#include "GMUART.h"
#include "SCI.h"
#include "J1850.h"
#include "ISO9141.h"
#include "FT_ISO15765.h"
#include "ISO15765.h"
#include "TP2_0.h"
#include "ISO14230.h"
#include "J1850PWM.h"
#include "CCD.h"
#include "J1939.h"
#include "J1708.h"
#include "J2818.h"
#include "LIN.h"
#include "DIAGH.h"
#ifdef J2534_0500
#include "ISO15765_LOGICAL.h"
#endif

#include "..\..\DeviceCayman\code\DeviceCayman.h"


#define PROTOCOLMANAGER_OBJ_LIST_START_IDX  1
#define PROTOCOLMANAGER_OBJ_LIST_SIZE       100 // Size of Protocol List. This
                                                // is set to a high number that
                                                // may never be used and result
                                                // wasting memory. However, it
                                                // will only be allocating 400
                                                // bytes and that is not 
                                                // significant.
#define PROTOCOLMANAGER_ERROR_TEXT_SIZE     120

class CProtocolManager
{
public:
    CProtocolManager(CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
    ~CProtocolManager();
    // Creates & stores protocol object address
    bool                    IsProtocolConnected(J2534_PROTOCOL  enumProtocolID);
    J2534ERROR              CreateProtocolObject(
                                                J2534_PROTOCOL  enumProtocolID,
                                                CDeviceBase     *pclsDevice,
                                                unsigned long   *pulChannelID);
    J2534ERROR              DeleteProtocolObj(unsigned long ulChannelID);
    void                    DeleteProtocolObj();
    bool                    IsChannelIDValid(unsigned long ulChannelID);
    void                    WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);


    CDebugLog               *m_pclsLog;
    CDataLog                *m_pclsDataLog;
    // Contains pointers to Protocol Objects (index is 1 based). This array
    // should have been replaced by CList as we do not know the size until
    // runtime. The size of this array is currently set to a large number 
    // that may result in waste of memory. However, the reason we chose an
    // array instead of CList is because the performance is better although
    // by wasting some memory that is not significant.
    CProtocolBase           *m_pclsProtocolObject[PROTOCOLMANAGER_OBJ_LIST_SIZE];
    J2534_PROTOCOL          m_enProtocolConnectionList[PROTOCOLMANAGER_OBJ_LIST_SIZE];
	static CRITICAL_SECTION CProtocolManagerSection;

private:
    unsigned long           GetNewChannelID();
    void                    UngetChannelID(unsigned long ulChannelID);
//    void                    SetLastErrorText(char *pszErrorText);
    
    char                    m_szLastErrorText[PROTOCOLMANAGER_ERROR_TEXT_SIZE];

};

#endif
           