/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: CircBuffer.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of class that is
*              used for buffering messages. The class uses a
*              circular buffer to store messages.
* Note:
*
*******************************************************************************/

#ifndef _CIRCBUFFER_H_
#define _CIRCBUFFER_H_

#include <vector>

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

// Constants
#define CIRCBUFFER_CRITICAL_SECTION_TIMEOUT	5000

// CCircBuffer Class
class CCircBuffer
{
public:
	// Constructor
	CCircBuffer(unsigned long ulBufferSize, 
				bool *pbError);
	~CCircBuffer();
	bool						Write(unsigned char *pucMsgs, 
									unsigned long uiMsgLen);

	bool						Read(unsigned char *pucMsgs, 
									unsigned long *pulNumMsgs);
	void						ClearBuffer();
	bool						GetBufferFullStatus(bool* pbBufferFull);
	bool						GetBufferEmptyStatus(bool* pbBufferEmpty);
	bool						GetBufferOverflowStatus(bool* pbBufferOverflow);

	bool						m_bBufferFull;
	bool						m_bBufferEmpty;
	bool						m_bBufferOverWritten;

private:
	typedef struct {
		unsigned char	*m_pucData;
		unsigned long	m_ulLen;
	} MsgStruct;

	unsigned long				m_ulTotalSize;
	std::vector<MsgStruct *>	MsgList;
	//std::vector <PASSTHRU_MSG> MsgList;
	HANDLE						m_hCriticalSection;
};

#endif