
#include "StdAfx.h"
#include "License.h"

FILE _iob[] = { *stdin, *stdout, *stderr };

extern "C" FILE * __cdecl __iob_func(void)
{
	return _iob;
}

string FromUTF8(const char *in)
{
    SK_ResultCode result = SK_ERROR_NONE;
    string str = "";
    str = in;
/*    char_t *buf = NULL;
    string str = "";

    result = SK_StringConvertMultiByteStringToWideString(SK_FLAGS_NONE, SK_STRING_ENCODING_UTF8, in, &buf);

    if (SK_ERROR_NONE != result)
        return str;

    str = buf;

    SK_StringDisposeW(SK_FLAGS_NONE, &buf); */

    return str;
}

string ToUTF8(const string in)
{
    SK_ResultCode result = SK_ERROR_NONE;  
    string str = "";
    str = in;
/*    char *buf = NULL;
    string str = "";

    result = SK_StringConvertWideStringToMultiByteString(SK_FLAGS_NONE, SK_STRING_ENCODING_UTF8,    in.c_str(), &buf);

    if (SK_ERROR_NONE != result)
        return str;

    str = buf;

    SK_StringDispose(SK_FLAGS_NONE, &buf); */

    return str;
}

#if SK_REGION("Default constructor/destructor")

License::License(int productID, const string productVersion, const string envelope, const string envelopeKey)
{
    m_Context = NULL;
    m_CurrentIdentifier = NULL;
    m_ExtendedErrorNo = 0;
	m_StatusCode = 0;
    m_ExtendedErrorString = "";
    m_IsLoaded = false;
    m_IsWritable = false;
    m_LastErrorNo = SK_ERROR_NONE;
    m_LicenseFilePath = "";
    m_SessionCodePath = "";
    m_savedSessionCode = "";
    m_RefreshLicenseStatusCallback = NULL;
    m_TerminateApplicationCallback = NULL;

    try
    {
        // Initialize our API context
        CheckResult(SK_ApiContextInitialize(SK_FLAGS_USE_SSL | SK_FLAGS_USE_ENCRYPTION | SK_FLAGS_USE_SIGNATURE, SK_FALSE, productID, 0, ToUTF8(productVersion).c_str(), ToUTF8(envelope).c_str(), &m_Context));

        // Set the envelope key
        SK_ResultCode result = SK_ApiContextSetFieldString(m_Context, SK_FLAGS_NONE, SK_APICONTEXT_ENVELOPEKEY, ToUTF8(envelopeKey).c_str());
        if (SK_ERROR_NONE == result || SK_ERROR_PLUS_EVALUATION_WARNING == result)
        {
            InitializeSystemIdentifiers();
        }
        CheckResult(result);

    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }
}

License::~License()
{
    if (m_IsWritable)
        SetLastUpdatedDateTime();

    if (m_CurrentIdentifier)
    {
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &m_CurrentIdentifier);
    }

    // Free our API context
    SK_ApiContextDispose(SK_FLAGS_APICONTEXTDISPOSE_SHUTDOWN, &m_Context);
}

#endif // Default constructor/destructor

#if SK_REGION("Initialization methods")

void License::AddAlias(const string path)
{
    SK_ResultCode result = SK_ERROR_NONE;

    result = SK_PLUS_LicenseAliasAdd(m_Context, SK_FLAGS_NONE, ToUTF8(path).c_str());

    if (SK_ERROR_NONE != result)
    {
        m_LastErrorNo = result;

        TerminateApplication();
    }
}

void License::InitializeSystemIdentifiers()
{
    int count = 0;

    try
    {
/*        CheckResult(SK_PLUS_SystemIdentifierAlgorithmAddCurrentIdentifiers(m_Context, SK_FLAGS_NONE, SK_SYSTEM_IDENTIFIER_ALGORITHM_COMPUTER_NAME, NULL, &count));

        // Make sure we have a computer name identifier
        if (0 == count)
        {
            throw SK_ERROR_INVALID_DATA;
        }

        CheckResult(SK_PLUS_SystemIdentifierAlgorithmAddCurrentIdentifiers(m_Context, SK_FLAGS_NONE, SK_SYSTEM_IDENTIFIER_ALGORITHM_HARD_DISK_VOLUME_SERIAL, NULL, &count));

        // Make sure we have a hard disk volume serial identifier
        if (0 == count)
        {
            throw SK_ERROR_INVALID_DATA;
        }

        CheckResult(SK_PLUS_SystemIdentifierAlgorithmAddCurrentIdentifiers(m_Context, SK_FLAGS_NONE, SK_SYSTEM_IDENTIFIER_ALGORITHM_NIC, NULL, &count));
*/
        CheckResult(SK_PLUS_SystemIdentifierAddCurrentIdentifier(m_Context, SK_FLAGS_NONE, "ZHICHE", "DGTECH", "YANKEE"));
        CheckResult(SK_PLUS_SystemIdentifierCurrentGetContents(m_Context, SK_FLAGS_NONE, &m_CurrentIdentifier));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }
}

void License::SetLicenseFilePath(const string path)
{
    m_LicenseFilePath = path;
}

void License::SetRefreshStatusCallback(REFRESH_STATUS_CALLBACK callback)
{
    m_RefreshLicenseStatusCallback = callback;
}

void License::SetSessionCodePath(const string path)
{
    m_SessionCodePath = path;
}

void License::SetTerminateApplicationCallback(TERMINATE_APPLICATION_CALLBACK callback)
{
    m_TerminateApplicationCallback = callback;
}

#endif // Initialization methods

#if SK_REGION("Licensing related methods")

LicenseType::Enum License::DetermineType(SK_XmlDoc license)
{
    SK_ResultCode result = SK_ERROR_NONE;
    int value = 0;

    result = SK_XmlNodeGetValueInt(SK_FLAGS_NONE, license, "/SoftwareKey/PrivateData/License/TriggerCode", &value);

    if (SK_ERROR_NONE != result)
        return LicenseType::Unlicensed;

    switch (value)
    {
    case 1:
    case 18:
    case 28:
        return LicenseType::FullNonExpiring;
    case 10:
    case 11:
    case 29:
        return LicenseType::TimeLimited;
    default:
        return LicenseType::Unlicensed;
    }
}

LicenseType::Enum License::GetType()
{
    SK_XmlDoc license = NULL;
    LicenseType::Enum licenseType = LicenseType::Unlicensed;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        licenseType = DetermineType(license);
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

    return licenseType;
}

OptionType::Enum License::GetOptionType()
{
    SK_XmlDoc license = NULL;
    char *value = NULL;
    string option = "";
    OptionType::Enum optionType = OptionType::Custom;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        SK_XmlNodeGetValueString(SK_FLAGS_NONE, license, "/SoftwareKey/PrivateData/License/ProductOption/OptionType", SK_FALSE, &value);
        option = FromUTF8(value);

        if (option.compare("P5VL") == 0)
        {
            optionType = OptionType::VolumeLicense;
        }
        else if (option.compare("P5DL") == 0)
        {
            optionType = OptionType::DownloadableLicenseWithTriggerCodeValidation;
        }
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &value);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

    return optionType;
}

void License::Reload()
{
    SK_ResultCode result = SK_ERROR_NONE;

    m_IsLoaded = false;
    SetWritable(false);

    SetFileAttributes(m_LicenseFilePath.c_str(), FILE_ATTRIBUTE_NORMAL);
    SetFileAttributes(m_licenseAlias1Path, FILE_ATTRIBUTE_NORMAL);
    SetFileAttributes(m_licenseAlias2Path, FILE_ATTRIBUTE_NORMAL);

    // Try to load the license file as read-only
    result = SK_PLUS_LicenseFileLoad(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str());

    if (SK_ERROR_VERIFICATION_FAILED == result ||
        SK_ERROR_COULD_NOT_LOAD_LICENSE == result)
    {
        SetWritable();

        // Try to load the license file as writable
        result = SK_PLUS_LicenseFileLoad(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str());
    }

    if (SK_ERROR_COULD_NOT_LOAD_LICENSE == result)
    {
        // We don't have a license file so start a fresh trial
        //CreateFreshEvaluation(30);
        return;
    }
    else if (SK_ERROR_NONE == result)
    {
        m_IsLoaded = true;
        SetFileAttributes(m_LicenseFilePath.c_str(), FILE_ATTRIBUTE_NORMAL);
        SetFileAttributes(m_licenseAlias1Path, FILE_ATTRIBUTE_HIDDEN);
        SetFileAttributes(m_licenseAlias2Path, FILE_ATTRIBUTE_HIDDEN); 
    }
    else
    {
        m_LastErrorNo = result;

        //An error occurred while loading the license file, so set m_IsWritable to false 
        //to avoid further error while exiting the application
        m_IsWritable = false;
    }

    // Notify our application to update it's status
    if (m_RefreshLicenseStatusCallback)
        m_RefreshLicenseStatusCallback();
}

void License::SetWritable(bool val)
{
    SK_ResultCode result = SK_ERROR_NONE;

    m_IsWritable = val;

    result = SK_ApiContextSetFieldInt(m_Context, SK_FLAGS_NONE, SK_APICONTEXT_LICENSE_WRITABLE, val ? SK_TRUE : SK_FALSE);

    if (SK_ERROR_NONE != result)
    {
        TerminateApplication();
    }
}

bool License::Validate()
{
    SK_ResultCode result = SK_ERROR_NONE;
    int resultCode = 0;
    SK_XmlDoc request = NULL;
    SK_XmlDoc response = NULL;

    int days = 0;
    int count = 0;
    int matches = 0;

    if (!m_IsLoaded)
    {
        return false;
    }

    try
    {
        // Generate the online license refresh request
        CheckResult(SK_SOLO_GetLicenseFileGetRequest(m_Context, SK_FLAGS_NONE, ToUTF8(GetStringValue("/SoftwareKey/PrivateData/License/InstallationID")).c_str(), &request, NULL));

        // Call the license refresh web service (check and compare the PC and the server date and time stamp and make sure it is not way off)
        result = SK_CallXmlWebService(m_Context, SK_FLAGS_NONE, SK_CONST_WEBSERVICE_GETLICENSEFILE_URL, request, &response, &resultCode, &m_StatusCode);

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);

        if (SK_ERROR_WEBSERVICE_RETURNED_FAILURE == result)
        {
            m_ExtendedErrorNo = resultCode;
            return false;
        }

        CheckResult(SK_DateTimeValidateApi(SK_FLAGS_NONE, 0, 0, NULL));

        if (GetOptionType() == OptionType::VolumeLicense)
        {
            //This is a volume license, so add a identifier for the License ID.
            CheckResult(SK_PLUS_SystemIdentifierCurrentSetContents(m_Context, SK_FLAGS_NONE, m_CurrentIdentifier));
            CheckResult(SK_PLUS_SystemIdentifierAddCurrentIdentifier(m_Context, SK_FLAGS_NONE, "LicenseIDIdentifier1", "LicenseIDIdentifier", ToUTF8(GetStringValue("/SoftwareKey/PrivateData/License/LicenseID")).c_str()));
        }

        // Compare our current identifiers to those authorized in the license file
        CheckResult(SK_PLUS_SystemIdentifierCompare(m_Context, SK_FLAGS_NONE, "", &count, &matches));

        // Require at least one identifier to match
        if (matches < 1)
        {
            m_LastErrorNo = SK_ERROR_LICENSE_SYSTEM_IDENTIFIERS_DONT_MATCH;
            return false;
        }

        if (!IsDateTimePast("/SoftwareKey/PrivateData/License/EffectiveStartDate"))
        {
            m_LastErrorNo = SK_ERROR_LICENSE_NOT_EFFECTIVE_YET;
            return false;
        }

        if (IsEvaluation() || GetType() == LicenseType::TimeLimited)
        {
            // Make sure the clock isn't back-dated.
            if (!IsDateTimePast("/SoftwareKey/PrivateData/License/LastUpdated"))
            {
                m_LastErrorNo = SK_ERROR_SYSTEM_TIME_INVALID;
                return false;
            }

            if (GetDaysRemaining() <= 0)
            {
                m_LastErrorNo = SK_ERROR_LICENSE_EXPIRED;
                return false;
            }
        }

        if (!IsEvaluation())
        {
            // Calculate the number of days since the signature date
            CheckResult(SK_DateTimeDaysRemaining(SK_FLAGS_NONE, ToUTF8(GetDateTimeStringValue("/SoftwareKey/PrivateData/License/SignatureDate")).c_str(), &days));

            // Attempt to refresh the license if older than 7 days
            if (days < -7)
            {
                if (RefreshOnline())
                {
                    //MessageBox(NULL, L"The license has been successfully refreshed.", L"License Refresh", MB_OK);
                    Reload();
                }
            }
        }

        // Make sure at least one alias was opened
        if (m_IsWritable)
        {
            int aliasCount = 0, openedAliases = 0, savedAliases = 0;
            // Get the number of aliases that we *should* load
            CheckResult(SK_PLUS_LicenseAliasGetTotal(m_Context, SK_FLAGS_NONE, &aliasCount));
            if (aliasCount)
            {
                // Get the number of aliases we have saved.  
                CheckResult(SK_PLUS_LicenseAliasGetCount(m_Context, SK_FLAGS_NONE, &savedAliases));
                // Get the number of aliases that we *have* loaded
                CheckResult(SK_PLUS_LicenseAliasGetValidatedCount(m_Context, SK_FLAGS_NONE, &openedAliases));
                // Make sure at least one alias has been validated or one alias has been saved
                if ((openedAliases < 1) && (savedAliases < 1))
                {
                    m_LastErrorNo = SK_ERROR_LICENSE_ALIAS_VALIDATION_FAILED;
                    return false;
                }
            }
        }

        return true;
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        TerminateApplication();
    }

    return false;
}

#endif // Licensing related methods

#if SK_REGION("Evaluation methods")

void License::CreateFreshEvaluation(int daysTillExpiration)
{
    SK_XmlDoc license = NULL;

    try
    {
        SetWritable();
        CheckResult(SK_PLUS_LicenseCreateNew(m_Context, SK_FLAGS_NONE, daysTillExpiration, &license));
        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));

        m_IsLoaded = true;
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
}

bool License::IsEvaluation()
{
    //Always treat the license as an evaluation when no Installation ID is present.
    SK_ResultCode lastError = m_LastErrorNo;
    if (GetStringValue("/SoftwareKey/PrivateData/License/InstallationID").empty())
    {
        //Restore the last error number from before calling GetStringValue.  This should throw a SK_ERROR_XML_NODE_MISSING error during evaluations.
        m_LastErrorNo = lastError;
        return true;
    }

    //If an Installation ID is present, check the TriggerCode number to determine if the license type is an evaluation.
    LicenseType::Enum type = GetType();
    if (LicenseType::FullNonExpiring == type ||
        LicenseType::TimeLimited == type)
    {
        return false;
    }

    //LicenseType::Unlicensed or anything else not in the if statement above should be treated as an evaluation.
    return true;
}

#endif // Evaluation methods

#if SK_REGION("Web service methods")

bool License::ActivateOnline(int licenseID, const string password, const string installationName)
{
    SK_ResultCode result = SK_ERROR_NONE;
    int resultCode = 0;
    SK_XmlDoc request = NULL;
    SK_XmlDoc response = NULL;
    SK_XmlDoc license = NULL;
    SK_XmlDoc decLicense = NULL;
    char *errorMsg = NULL;

    try
    {
        // Generate the online activation request
        CheckResult(SK_SOLO_ActivateInstallationGetRequest(m_Context, SK_FLAGS_NONE, licenseID, ToUTF8(password).c_str(), NULL, 1000, 1000, SK_FALSE, ToUTF8(installationName).c_str(), NULL, &request, NULL));

        // Call the activation web service
        result = SK_CallXmlWebService(m_Context, SK_FLAGS_NONE, SK_CONST_WEBSERVICE_ACTIVATEINSTALLATION_URL, request, &response, &resultCode, &m_StatusCode);

        if (SK_ERROR_NONE != result)
        {
            m_LastErrorNo = result;

            if (SK_ERROR_WEBSERVICE_RETURNED_FAILURE == result)
            {
                m_ExtendedErrorNo = resultCode;

                CheckResult(SK_XmlNodeGetValueString(SK_FLAGS_NONE, response, "/ActivateInstallationLicenseFile/PrivateData/ErrorMessage", SK_FALSE, &errorMsg));

                m_ExtendedErrorString = FromUTF8(errorMsg);
            }

            throw result;
        }

        // Get the license node from the response
        CheckResult(SK_XmlNodeGetDocument(SK_FLAGS_NONE, response, "/ActivateInstallationLicenseFile/PrivateData/License", &license));

        // Decrypt the license contents so we can determine what type of license it is
        CheckResult(SK_XmlDocumentDecryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, license, &decLicense));

        // Use a writable license for expiring license types
        SetWritable(DetermineType(decLicense) != LicenseType::FullNonExpiring);

        // Save the license file
        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &decLicense);
    SK_StringDispose(SK_FLAGS_NONE, &errorMsg);

    return (SK_ERROR_NONE == GetLastErrorNo());
}

bool License::DeactivateOnline()
{
    SK_ResultCode result = SK_ERROR_NONE;
    int resultCode = 0;
    SK_XmlDoc request = NULL;
    SK_XmlDoc response = NULL;
    char *errorMsg = NULL;

    try
    {
        // Generate the online deactivation request
        CheckResult(SK_SOLO_DeactivateInstallationGetRequest(m_Context, SK_FLAGS_NONE, ToUTF8(GetStringValue("/SoftwareKey/PrivateData/License/InstallationID")).c_str(), &request, NULL));

        // Call the deactivation web service
        result = SK_CallXmlWebService(m_Context, SK_FLAGS_NONE, SK_CONST_WEBSERVICE_DEACTIVATEINSTALLATION_URL, request, &response, &resultCode, &m_StatusCode);

        if (SK_ERROR_NONE != result)
        {
            if (SK_ERROR_WEBSERVICE_RETURNED_FAILURE == result)
            {
                m_ExtendedErrorNo = resultCode;

                CheckResult(SK_XmlNodeGetValueString(SK_FLAGS_NONE, response, "/DeactivateInstallation/PrivateData/ErrorMessage", SK_FALSE, &errorMsg));

                m_ExtendedErrorString = FromUTF8(errorMsg);

                // Has the license or installation been revoked?
                if (ShouldLicenseBeRevoked(resultCode))
                {
                    // We deactivated so convert the license to an expired trial
                    CreateFreshEvaluation(-1);
                }
            }

            throw result;
        }

        // We deactivated so convert the license to an expired trial
        CreateFreshEvaluation(-1);
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);
    SK_StringDispose(SK_FLAGS_NONE, &errorMsg);

    return (SK_ERROR_NONE == GetLastErrorNo());
}

bool License:: RefreshOnline()
{
    SK_ResultCode result = SK_ERROR_NONE;
    int resultCode = 0;
    SK_XmlDoc request = NULL;
    SK_XmlDoc response = NULL;
    SK_XmlDoc license = NULL;
    SK_XmlDoc decLicense = NULL;
    char *errorMsg = NULL;

    try
    {
        // Generate the online license refresh request
        CheckResult(SK_SOLO_GetLicenseFileGetRequest(m_Context, SK_FLAGS_NONE, ToUTF8(GetStringValue("/SoftwareKey/PrivateData/License/InstallationID")).c_str(), &request, NULL));

        // Call the license refresh web service
        result = SK_CallXmlWebService(m_Context, SK_FLAGS_NONE, SK_CONST_WEBSERVICE_GETLICENSEFILE_URL, request, &response, &resultCode, &m_StatusCode);

        if (SK_ERROR_NONE != result)
        {
            m_LastErrorNo = result;

            if (SK_ERROR_WEBSERVICE_RETURNED_FAILURE == result)
            {
                m_ExtendedErrorNo = resultCode;

                CheckResult(SK_XmlNodeGetValueString(SK_FLAGS_NONE, response, "/GetLicenseFile/PrivateData/ErrorMessage", SK_FALSE, &errorMsg));

                m_ExtendedErrorString = FromUTF8(errorMsg);

                // Has the license or installation been revoked?
                if (ShouldLicenseBeRevoked(resultCode))
                {
                    // We deactivated so convert the license to an expired trial
                    CreateFreshEvaluation(-1);
                }
            }

            throw result;
        }

        // Get the license node from the response
        CheckResult(SK_XmlNodeGetDocument(SK_FLAGS_NONE, response, "/GetLicenseFile/PrivateData/License", &license));

        // Decrypt the license contents so we can determine what type of license it is
        CheckResult(SK_XmlDocumentDecryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, license, &decLicense));

        // Use a writable license for expiring license types
        SetWritable(DetermineType(decLicense) != LicenseType::FullNonExpiring);

        // Save the license file
        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &decLicense);
    SK_StringDispose(SK_FLAGS_NONE, &errorMsg);

    return (SK_ERROR_NONE == GetLastErrorNo());
}

// Set the proxy information in the context
bool License::SetProxyCredentials(string proxyUrl, string userName, string password)
{
	try
	{
		CheckResult(SK_ApiContextSetFieldString(m_Context, SK_FLAGS_NONE, (SK_ApiContext_StringFields)SK_APICONTEXT_WEBSERVICE_PROXY, ToUTF8(proxyUrl).c_str()));
		CheckResult(SK_ApiContextSetFieldString(m_Context, SK_FLAGS_NONE, (SK_ApiContext_StringFields)SK_APICONTEXT_WEBSERVICE_PROXYUSERNAME, ToUTF8(userName).c_str()));
		CheckResult(SK_ApiContextSetFieldString(m_Context, SK_FLAGS_NONE, (SK_ApiContext_StringFields)SK_APICONTEXT_WEBSERVICE_PROXYPASSWORD, ToUTF8(password).c_str()));
	}
	catch (SK_ResultCode& error)
	{
		m_LastErrorNo = error;
		return false;
	}
	
	return true;
}

#endif // Web service methods

#if SK_REGION("Manual activation methods")

SK_XmlDoc License::GetManualActivationRequest(int licenseID, const string password, const string installationName)
{
    string requestString;
    char* sessionCode = NULL;
    char* encryptedRequestString = NULL;
    SK_XmlDoc request = NULL;
    SK_XmlDoc encryptedRequest = NULL;

    try
    {
        // Generate the manual activation request
        CheckResult(SK_SOLO_ActivateInstallationGetRequest(m_Context, SK_FLAGS_NONE, licenseID, ToUTF8(password).c_str(), NULL, 1000, 1000, SK_FALSE, ToUTF8(installationName).c_str(), NULL, &request, &sessionCode));

        #ifdef WIN32
        // Save the session code so we can verify it later when processing the response
        CheckResult(SK_RegistryValueSet(SK_FLAGS_NONE, ToUTF8(m_SessionCodePath).c_str(), sessionCode));
        #endif
        m_savedSessionCode = FromUTF8(sessionCode);

        // Encrypt and sign the request
        CheckResult(SK_XmlDocumentEncryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, request, &encryptedRequest));

        // Get our request as a string
        CheckResult(SK_XmlDocumentGetDocumentString(SK_FLAGS_NONE, encryptedRequest, &encryptedRequestString));

        requestString = FromUTF8(encryptedRequestString);
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &sessionCode);
    SK_StringDispose(SK_FLAGS_NONE, &encryptedRequestString);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);

    return encryptedRequest;
}

// Get the stored manual activation session code
string License::GetSessionCode()
{
    SK_ResultCode result = SK_ERROR_NONE;
    char *sessionCode = NULL;
    string strSessionCode = "";

    #ifdef WIN32
    result = SK_RegistryValueGet(SK_FLAGS_NONE, ToUTF8(m_SessionCodePath).c_str(), &sessionCode);
    #endif

    if (SK_ERROR_NONE == result)
    {
        strSessionCode = FromUTF8(sessionCode);
    }
    else if (SK_ERROR_COULD_NOT_READ_REGISTRY_KEY != result)
    {
        m_LastErrorNo = result;
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &sessionCode);

    return strSessionCode;
}

bool License::ProcessManualResponse(const string responseString, const string rootNode)
{
    SK_ResultCode result = SK_ERROR_NONE;
    char *sessionCode = NULL;
    SK_XmlDoc response = NULL;
    SK_XmlDoc encryptedResponse = NULL;
    SK_XmlDoc license = NULL;
    SK_XmlDoc decLicense = NULL;
    string xPath;
    try
    {
        // Load the response string into an XML document
        result = SK_XmlDocumentCreateFromString(SK_FLAGS_NONE, ToUTF8(responseString).c_str(), &encryptedResponse);

        if (SK_ERROR_NONE != result)
        {
            // Handle invalid data
            if (SK_ERROR_XML_PARSER_FAILED == result)
            {
                throw SK_ERROR_INVALID_DATA;
            }
            else
            {
                throw result;
            }
        }

        // Decrypt and verify the response
        CheckResult(SK_XmlDocumentDecryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, encryptedResponse, &response));

        xPath = xPath.append(rootNode);
        xPath = xPath.append("/PrivateData/SessionCode");
        // Get the SessionCode from the response
        CheckResult(SK_XmlNodeGetValueString(SK_FLAGS_NONE, response, ToUTF8(xPath).c_str(), SK_FALSE, &sessionCode));

        string tmpSessionCode = FromUTF8(sessionCode);

#ifdef WIN32
        // Verify the session code matches our stored session code
        if (tmpSessionCode != GetSessionCode())
#else
        if (tmpSessionCode != m_savedSessionCode)
#endif
        {
            throw SK_ERROR_SESSION_VERIFICATION_FAILED;
        }
#ifdef WIN32
        // Clear the stored session code
        CheckResult(SK_RegistryValueSet(SK_FLAGS_NONE, ToUTF8(m_SessionCodePath).c_str(), ""));
#endif
        if (rootNode == "/DeactivateInstallation")
        {
            // Cleanup
            SK_XmlDocumentDispose(SK_FLAGS_NONE, &encryptedResponse);
            SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);
            SK_StringDispose(SK_FLAGS_NONE, &sessionCode);
            return true;  // return true, as we don't have License node in deactivation response
        }

        xPath = "";
        xPath = xPath.append(rootNode);
        xPath = xPath.append("/PrivateData/License");
        // Get the license node from the response
        CheckResult(SK_XmlNodeGetDocument(SK_FLAGS_NONE, response, ToUTF8(xPath).c_str(), &license));

        // Decrypt the license contents so we can determine what type of license it is
        CheckResult(SK_XmlDocumentDecryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, license, &decLicense));

        // Use a writable license for expiring license types
        SetWritable(DetermineType(decLicense) != LicenseType::FullNonExpiring);

        // Save the license file
        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &encryptedResponse);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &response);
    SK_XmlDocumentDispose (SK_FLAGS_NONE, &license);
    SK_XmlDocumentDispose (SK_FLAGS_NONE, &decLicense);
    SK_StringDispose(SK_FLAGS_NONE, &sessionCode);

    return (SK_ERROR_NONE == GetLastErrorNo());
}

SK_XmlDoc License::GetManualRefreshRequest(const string installationID)
{
    char* sessionCode = NULL;
    SK_XmlDoc request = NULL;
    SK_XmlDoc encryptedRequest = NULL;

    try
    {
        // Generate the manual activation request
        CheckResult(SK_SOLO_GetLicenseFileGetRequest(m_Context, SK_FLAGS_NONE, ToUTF8(installationID).c_str(), &request, &sessionCode));

        #ifdef WIN32
        // Save the session code so we can verify it later when processing the response
        CheckResult(SK_RegistryValueSet(SK_FLAGS_NONE, ToUTF8(m_SessionCodePath).c_str(), sessionCode));
        #endif
        m_savedSessionCode = FromUTF8(sessionCode);

        // Encrypt and sign the request
        CheckResult(SK_XmlDocumentEncryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, request, &encryptedRequest));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &sessionCode);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);

    return encryptedRequest;
}

SK_XmlDoc License::GetManualDeactivationRequest(const string installationID)
{
    char* sessionCode = NULL;
    SK_XmlDoc request = NULL;
    SK_XmlDoc encryptedRequest = NULL;

    try
    {
        // Generate the manual activation request
        CheckResult(SK_SOLO_DeactivateInstallationGetRequest(m_Context, SK_FLAGS_NONE, ToUTF8(installationID).c_str(), &request, &sessionCode));

        #ifdef WIN32
        // Save the session code so we can verify it later when processing the response
        CheckResult(SK_RegistryValueSet(SK_FLAGS_NONE, ToUTF8(m_SessionCodePath).c_str(), sessionCode));
        #endif
        m_savedSessionCode = FromUTF8(sessionCode);

        // Encrypt and sign the request
        CheckResult(SK_XmlDocumentEncryptRsa(m_Context, SK_FLAGS_NONE, SK_FALSE, request, &encryptedRequest));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &sessionCode);
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &request);

    return encryptedRequest;
}

#endif // Manual activation methods

#if SK_REGION("License file read/write methods")

string License::GetDateTimeStringValue(const string xpath)
{
    SK_XmlDoc license = NULL;
    char* value = NULL;
    string valueString;

    if (!m_IsLoaded)
    {
        return "";
    }

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeGetValueDateTimeString(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), &value));

        valueString = FromUTF8(value);
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
    SK_StringDispose(SK_FLAGS_NONE, &value);

    if (SK_ERROR_NONE == GetLastErrorNo())
    {
        return valueString;
    }
    else
    {
        return "";
    }
}

double License::GetDoubleValue(const string xpath)
{
    SK_XmlDoc license = NULL;
    double value;

    if (!m_IsLoaded)
    {
        return 0.0;
    }

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeGetValueDouble(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), &value));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

    if (SK_ERROR_NONE == GetLastErrorNo())
    {
        return value;
    }
    else
    {
        return 0.0;
    }
}

int License::GetIntegerValue(const string xpath)
{
    SK_XmlDoc license = NULL;
    int value;

    if (!m_IsLoaded)
    {
        return 0;
    }

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeGetValueInt(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), &value));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

    if (SK_ERROR_NONE == GetLastErrorNo())
    {
        return value;
    }
    else
    {
        return 0;
    }
}

string License::GetStringValue(const string xpath)
{
    SK_XmlDoc license = NULL;
    char* value = NULL;
    string valueString;

    if (!m_IsLoaded)
    {
        return "";
    }

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeGetValueString(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), SK_FALSE, &value));

        valueString = FromUTF8(value);
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
    SK_StringDispose(SK_FLAGS_NONE, &value);

    if (SK_ERROR_NONE == GetLastErrorNo())
    {
        return valueString;
    }
    else
    {
        return "";
    }
}

void License::SetDateTimeStringValue(const string xpath, const string value)
{
    SK_XmlDoc license = NULL;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeSetValueDateTimeString(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), ToUTF8(value).c_str()));

        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
}

void License::SetDoubleValue(const string xpath, double value)
{
    SK_XmlDoc license = NULL;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeSetValueDouble(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), value));

        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
}

void License::SetIntegerValue(const string xpath, int value)
{
    SK_XmlDoc license = NULL;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeSetValueInt(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), value));

        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
}

void License::SetStringValue(const string xpath, const string value)
{
    SK_XmlDoc license = NULL;

    try
    {
        CheckResult(SK_PLUS_LicenseGetXmlDocument(m_Context, SK_FLAGS_NONE, &license));

        CheckResult(SK_XmlNodeSetValueString(SK_FLAGS_NONE, license, ToUTF8(xpath).c_str(), ToUTF8(value).c_str()));

        CheckResult(SK_PLUS_LicenseFileSave(m_Context, SK_FLAGS_NONE, ToUTF8(m_LicenseFilePath).c_str(), license));
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);

        TerminateApplication();
    }

    // Cleanup
    SK_XmlDocumentDispose(SK_FLAGS_NONE, &license);
}

#endif // License file read/write methods

#if SK_REGION("Date-time methods")

int License::GetDaysRemaining()
{
    int daysLeft = 0;

    SK_DateTimeDaysRemaining(SK_FLAGS_NONE, ToUTF8(GetDateTimeStringValue("/SoftwareKey/PrivateData/License/EffectiveEndDate")).c_str(), &daysLeft);

    return daysLeft;
}

int License::GetDaysEffective()
{
    int daysEffective = 0;

    SK_DateTimeDaysRemaining(SK_FLAGS_NONE, ToUTF8(GetDateTimeStringValue("/SoftwareKey/PrivateData/License/EffectiveStartDate")).c_str(), &daysEffective);

    return daysEffective;
}

bool License::IsDateTimePast(const string xpath)
{
    bool ret_val = true;
    char *now = NULL;
    int comparison;

    try
    {
        CheckResult(SK_DateTimeGetCurrentString(SK_FLAGS_NONE, &now));

        string value = ToUTF8(GetDateTimeStringValue(xpath)).c_str();

        if (true == value.empty())
        {
            ret_val = true;
        }
        else
        {
            CheckResult(SK_DateTimeCompareStrings(SK_FLAGS_NONE, value.c_str(), now, &comparison));
            ret_val = (comparison <= 0);
        }
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_StringDispose(SK_FLAGS_NONE, &now);

        TerminateApplication();
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &now);

    return ret_val;
}

void License::SetLastUpdatedDateTime(bool force)
{
    char *now = NULL;
    int comparison;

    try
    {
        CheckResult(SK_DateTimeGetCurrentString(SK_FLAGS_NONE, &now));

        if (force)
        {
            // Set the LastUpdated date-time even if it's older than what we have in the license file
            SetDateTimeStringValue("/SoftwareKey/PrivateData/License/LastUpdated", FromUTF8(now));
        }
        else
        {
            CheckResult(SK_DateTimeCompareStrings(SK_FLAGS_NONE, ToUTF8(GetDateTimeStringValue("/SoftwareKey/PrivateData/License/LastUpdated")).c_str(), now, &comparison));

            if (comparison <= 0)
            {
                SetDateTimeStringValue("/SoftwareKey/PrivateData/License/LastUpdated", FromUTF8(now));
            }
            else
            {
                throw SK_ERROR_SYSTEM_TIME_INVALID;
            }
        }
    }
    catch (SK_ResultCode& error)
    {
        m_LastErrorNo = error;

        // Cleanup
        SK_StringDispose(SK_FLAGS_NONE, &now);

        TerminateApplication();
    }

    // Cleanup
    SK_StringDispose(SK_FLAGS_NONE, &now);
}

#endif // Date-time methods

#if SK_REGION("Error related methods")

string License::GetErrorMessage()
{
    string error = "Error ";
    char errorBuf[12] = { 0 };
    char extErrorBuf[12] = { 0 };

    sprintf_s(errorBuf, 12, "%d", (int)GetLastErrorNo());
    sprintf_s(extErrorBuf, 12, "%d", (int)GetExtendedErrorNo());

    if (SK_ERROR_WEBSERVICE_RETURNED_FAILURE == GetLastErrorNo())
    {
        error.append(extErrorBuf);
        error.append(": ");
        error.append(GetExtendedErrorString());
    }
    else
    {
        error.append(errorBuf);
        error.append(": ");
        error.append(GetLastErrorString());
    }

    return error;
}

int License::GetExtendedErrorNo()
{
    return m_ExtendedErrorNo;
}

int License::GetStatusCode()
{
	return m_StatusCode;
}

string License::GetExtendedErrorString()
{
    return m_ExtendedErrorString;
}

SK_ResultCode License::GetLastErrorNo()
{
    return m_LastErrorNo;
}

string License::GetLastErrorString()
{
    char* msg = NULL;
    SK_ApiResultCodeToString(SK_FLAGS_NONE, GetLastErrorNo(), &msg);
    string strErrorMsg = FromUTF8(msg);
    SK_StringDispose(SK_FLAGS_NONE, &msg);
    return strErrorMsg;
}

void License::TerminateApplication()
{
    // Notify the application to terminate
    if (m_TerminateApplicationCallback)
        m_TerminateApplicationCallback();
}

bool License::ShouldLicenseBeRevoked(const int result)
{
    if (5015 == result || 5016 == result || 5017 == result || 5010 == result)
        return true;

    return false;
}

#endif // Error related methods

#if SK_REGION("Private miscellaneous helper methods")

void License::CheckResult(SK_ResultCode result)
{
    m_LastErrorNo = result;

    if (SK_ERROR_NONE != result)
        throw result;
}
#endif // Private miscellaneous helper methods
