/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: main.c
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: (Some description of the functions contained in the file.  If the
*              text must wrap follow this convention.)
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h" 
#include "ProtocolManager.h"

CRITICAL_SECTION CProtocolManager::CProtocolManagerSection;

//-----------------------------------------------------------------------------
//  Function Name   : CProtocolManager()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This is a constructor. 
//-----------------------------------------------------------------------------
CProtocolManager::CProtocolManager(CDebugLog * pclsDebugLog, CDataLog * pclsDataLog)
{
    InitializeCriticalSection(&CProtocolManagerSection);        // Used in WriteLogMsg()
    
    // Save Log Class pointer.
    m_pclsLog = pclsDebugLog;
    m_pclsDataLog = pclsDataLog;

    // Write to Log File.
    WriteLogMsg("CProtocolManager()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize all the class pointers to NULL.
    // Remember the index is 1 based so the 0th
    // index is not used but I am going to initialize
    // that as well.
    for (int i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
    {
        m_pclsProtocolObject[i] = NULL;
        m_enProtocolConnectionList[i] = (J2534_PROTOCOL)0;
    }
    // Write to Log File.
    WriteLogMsg("CProtocolManager()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CProtocolManager()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This is a Destructor.
//-----------------------------------------------------------------------------
CProtocolManager::~CProtocolManager()
{
    // Write to Log File.
    WriteLogMsg("~CProtocolManager()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (int i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
    {
        if (IsChannelIDValid(i))
        {
            DeleteProtocolObj(i);
        }
    }

    // Write to Log File.
    WriteLogMsg("~CProtocolManager()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : IsProtocolConnected()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function checks to see if certain protocol is connected
//                    to avoid certain improper simutaneous communication.
//-----------------------------------------------------------------------------
bool CProtocolManager::IsProtocolConnected(J2534_PROTOCOL   enumProtocolID)
{
    int i;
    switch (enumProtocolID)
    {
        case J1850VPW:
        case J1850PWM:
            for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
            {
                if ((m_enProtocolConnectionList[i] == J1850VPW) ||
                    (m_enProtocolConnectionList[i] == J1850PWM))
                    return true;
            }
            break;
        case ISO9141:
        case ISO14230:
        case J1708_PS:
#ifdef J2534_DEVICE_NETBRIDGE
        case LIN:
#endif
#ifdef J2534_DEVICE_DBRIDGE
    case ISO9141_PS:
#endif
            for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
            {
                if ((m_enProtocolConnectionList[i] == SCI_A_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_A_TRANS) ||
                    (m_enProtocolConnectionList[i] == SCI_B_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_B_TRANS) ||
                    (m_enProtocolConnectionList[i] == ISO9141) ||
                    (m_enProtocolConnectionList[i] == ISO14230) ||
                    (m_enProtocolConnectionList[i] == J1708_PS) ||
                    (m_enProtocolConnectionList[i] == LIN) ||
                    (m_enProtocolConnectionList[i] == ISO9141_PS))
                    return true;
            }
            break;
        case CAN:
        case ISO15765:
        case J1939_PS:
            for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
            {
                if ((m_enProtocolConnectionList[i] == SCI_A_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_A_TRANS) ||
                    (m_enProtocolConnectionList[i] == SCI_B_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_B_TRANS) ||
                    (m_enProtocolConnectionList[i] == CAN) ||
                    (m_enProtocolConnectionList[i] == ISO15765) ||
                    (m_enProtocolConnectionList[i] == J1939_PS))
                    return true;
            }
            break;
        case SCI_A_ENGINE:
        case SCI_A_TRANS:
        case SCI_B_ENGINE:
        case SCI_B_TRANS:
            for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
            {
                if ((m_enProtocolConnectionList[i] == SCI_A_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_A_TRANS) ||
                    (m_enProtocolConnectionList[i] == SCI_B_ENGINE) ||
                    (m_enProtocolConnectionList[i] == SCI_B_TRANS) ||
                    (m_enProtocolConnectionList[i] == ISO9141) ||
                    (m_enProtocolConnectionList[i] == ISO14230) ||
                    (m_enProtocolConnectionList[i] == J1708_PS) ||
                    (m_enProtocolConnectionList[i] == LIN) ||
                    (m_enProtocolConnectionList[i] == CAN) ||
                    (m_enProtocolConnectionList[i] == ISO15765) ||
                    (m_enProtocolConnectionList[i] == J1939_PS))
                    return true;
            }
            break;
        default:
            break;
    }
    return false;
}
//-----------------------------------------------------------------------------
//  Function Name   : CreateProtocolObject()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function creates a Protocol specific object and 
//                    stores its address in a list.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolManager::CreateProtocolObject(
                                                J2534_PROTOCOL  enumProtocolID,
                                                CDeviceBase     *pclsDevice,
                                                unsigned long   *pulChannelID)
{
    CProtocolBase   *pTemp = NULL;
    unsigned long   ulNewChannelID = 0;
    int             i;

    // Write to Log File.
    WriteLogMsg("CreateProtocolObject()", DEBUGLOG_TYPE_COMMENT, "Start");
    if (enumProtocolID != 0)
    {
        for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
        {
            int PS_Count = 0;
            if (m_enProtocolConnectionList[i] == enumProtocolID)
            {
                if (enumProtocolID == CAN_PS || enumProtocolID == ISO15765_PS ||
                    enumProtocolID == J1939_PS || enumProtocolID == TP2_0_PS)
                {   // Allow for three _PS to be connected for this protocol
                    PS_Count++;     
                    if (PS_Count < 3)   // Allow for up to (3)
                        continue;

                }
                return (J2534_ERR_CHANNEL_IN_USE);
            }
        }

        if (IsProtocolConnected(enumProtocolID))
            return (J2534_ERR_INVALID_PROTOCOL_ID);
    }
    switch (enumProtocolID)
    {
    case J1850VPW:
            pTemp = new CJ1850VPW(pclsDevice, m_pclsLog);
            break;
    case ISO9141:
#ifdef J2534_DEVICE_DBRIDGE
    case ISO9141_PS:
#endif
            pTemp = new CISO9141(pclsDevice, m_pclsLog);
            break;
    case ISO15765:
    case ISO15765_PS:
    case ISO15765_CH1:
    case ISO15765_CH2:
    case ISO15765_CH3:
    case ISO15765_CH4:
#ifndef J2534_0500
            pTemp = new CISO15765(pclsDevice, m_pclsLog, m_pclsDataLog);
#endif
#ifdef J2534_0500
			pTemp = new CISO15765_LOGICAL(pclsDevice, m_pclsLog, m_pclsDataLog);
#endif
            break;
    case CAN:
    case CAN_PS:
    case CAN_CH1:
    case CAN_CH2:
    case CAN_CH3:
    case CAN_CH4:
#ifndef J2534_0500
            pTemp = new CCAN(pclsDevice, m_pclsLog, m_pclsDataLog);
#endif
#ifdef J2534_0500
			pTemp = new CISO15765_LOGICAL(pclsDevice, m_pclsLog, m_pclsDataLog);
#endif
            break;
    case J1939_PS:
    case J1939_CH1:
    case J1939_CH2:
    case J1939_CH3:
    case J1939_CH4:
            pTemp = new CJ1939(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
#ifdef J2534_DEVICE_SVCI1
    case TP2_0_PS:
    case TP2_0_CH1:
    //case TP2_0_CH2:
    //case TP2_0_CH3:
    //case TP2_0_CH4:
            pTemp = new CTP2_0(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
#endif
    case SW_CAN_PS:
    case SW_CAN_CAN_CH1:
            pTemp = new CSWCAN(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
    case SW_ISO15765_PS:
    case SW_CAN_ISO15765_CH1:
            pTemp = new CSWISO15765(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
#ifdef J2534_DEVICE_NETBRIDGE
    case FT_CAN_CH1:
            pTemp = new CFTCAN(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
    case FT_ISO15765_CH1:
            pTemp = new CFTISO15765(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
#endif
    case GM_UART:
            pTemp = new CGMUART(pclsDevice, m_pclsLog);
            break;
    case ISO14230:
            pTemp = new CISO14230(pclsDevice, m_pclsLog);
            break;
    case J1708_PS:
    case J1708_CH1:
            pTemp = new CJ1708(pclsDevice, m_pclsLog, m_pclsDataLog);
            break;
    case J1850PWM:
            pTemp = new CJ1850PWM(pclsDevice, m_pclsLog);
            break;
    case SCI_A_ENGINE:
    case SCI_A_TRANS:
    case SCI_B_ENGINE:
    case SCI_B_TRANS:
            pTemp = new CSCI(pclsDevice, m_pclsLog);
            break;
    case CCD:
            pTemp = new CCCD(pclsDevice, m_pclsLog);
            break;
#ifndef J2534_DEVICE_DPA5_EURO5
    case UART_ECHO_BYTE_PS:
            pTemp = new CJ2818(pclsDevice, m_pclsLog);
            break;
#endif
#ifdef J2534_DEVICE_DBRIDGE
    case UART_ECHO_BYTE_PS:
            pTemp = new CJ2818(pclsDevice, m_pclsLog);
            break;
    case HONDA_DIAGH_PS:
            pTemp = new CDIAGH(pclsDevice, m_pclsLog);
            break;
#endif
#ifdef J2534_DEVICE_NETBRIDGE
    case LIN:
            pTemp = new CLIN(pclsDevice, m_pclsLog);
            break;
#endif
    default:
        // Write to Log File.
        WriteLogMsg("CreateProtocolObject()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_PROTOCOL_ID);
        return(J2534_ERR_INVALID_PROTOCOL_ID);
        break;
    }

    // Check if the Protocol Object was created successfully.
    if (pTemp == NULL)
    {
        // Write to Log File.
        WriteLogMsg("CreateProtocolObject()", DEBUGLOG_TYPE_ERROR, "Cannot create Protocol Object");
        // Set right text to be retreived when GetLastErrorText() called.
        SetLastErrorText("Prot Manager : Cannot create Protocol Object");
        return(J2534_ERR_FAILED);
    }

    // Create new Channel to store the Protocol Object.
    if ((ulNewChannelID = GetNewChannelID()) == 0)
    {
        delete pTemp;
        pTemp = NULL;

        // Write to Log File.
        WriteLogMsg("CreateProtocolObject()", DEBUGLOG_TYPE_ERROR, "Cannot create new channel");
        // Set correct text to be retreived when GetLastErrorText() called.
        SetLastErrorText("Prot Mansger : Cannot create new channel");
        return(J2534_ERR_FAILED);
    }

    // Save the object address in the list.
    m_pclsProtocolObject[ulNewChannelID] = pTemp;

    // Assign new channnel ID for reference to this protocol.
    *pulChannelID = ulNewChannelID;

    m_enProtocolConnectionList[ulNewChannelID] = enumProtocolID;

    // Write to Log File.
    WriteLogMsg("CreateProtocolObject()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : DeleteProtocolObj()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function deletes a Protocol specific 
//                    object and removes its address from the list.
//-----------------------------------------------------------------------------
J2534ERROR CProtocolManager::DeleteProtocolObj(unsigned long ulChannelID)
{
    // Write to Log File.
    WriteLogMsg("DeleteProtocolObj(ChID)", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check if the Channel ID is valid.
    if (!IsChannelIDValid(ulChannelID))
    {
        // Write to Log File.
        WriteLogMsg("DeleteProtocolObj(ChID)", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
        return(J2534_ERR_INVALID_CHANNEL_ID);
    }

    // Delete the Protocol Object for this ChannelID.   
    if (m_pclsProtocolObject[ulChannelID] != NULL)
    {
        delete m_pclsProtocolObject[ulChannelID];   
        m_pclsProtocolObject[ulChannelID] = NULL;
        m_enProtocolConnectionList[ulChannelID] = (J2534_PROTOCOL)0;
    }

    // Mark this channel as unused.
    UngetChannelID(ulChannelID);

    // Write to Log File.
    WriteLogMsg("DeleteProtocolObj(ChID)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : DeleteProtocolObj()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function deletes a all active Protocol  
//                    objects and removes its address from the list.
//-----------------------------------------------------------------------------
void CProtocolManager::DeleteProtocolObj()
{
    // Write to Log File.
    WriteLogMsg("DeleteProtocolObj()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (int i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
    {
        // Delete each Protocol Object. 
        if (m_pclsProtocolObject[i] != NULL)
        {
            delete m_pclsProtocolObject[i]; 
            m_pclsProtocolObject[i] = NULL;
            m_enProtocolConnectionList[i] = (J2534_PROTOCOL)0;
        }
        // Mark this channel as unused.
        UngetChannelID(i);
    }

    // Write to Log File.
    WriteLogMsg("DeleteProtocolObj()", DEBUGLOG_TYPE_COMMENT, "End");

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : GetNewChannelID()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function searches through the list and returns the
//                    first unused ChannelID if successful else returns 0.
//-----------------------------------------------------------------------------
unsigned long CProtocolManager::GetNewChannelID()
{
    for (int i = PROTOCOLMANAGER_OBJ_LIST_START_IDX; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
    {
        if (m_pclsProtocolObject[i] == NULL)
            return(i);
    }

    return(0);
}

//-----------------------------------------------------------------------------
//  Function Name   : UngetChannelID()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function marks the channelID as not in use.
//-----------------------------------------------------------------------------
void CProtocolManager::UngetChannelID(unsigned long ulChannelID)
{
    if ((ulChannelID > 0) && (ulChannelID < PROTOCOLMANAGER_OBJ_LIST_SIZE))
        m_pclsProtocolObject[ulChannelID] = NULL;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsChannelIDValid()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This function checks to see if the channel ID is valid.
//-----------------------------------------------------------------------------
bool CProtocolManager::IsChannelIDValid(unsigned long ulChannelID)
{
    if ((ulChannelID > 0) && (ulChannelID < PROTOCOLMANAGER_OBJ_LIST_SIZE))
    {
        if (m_pclsProtocolObject[ulChannelID] != NULL)
        {
            return(true);
        }
    }

    return(false);
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CProtocolManager::WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
    if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
    {
        EnterCriticalSection(&CProtocolManagerSection); // Make processing parameters thread-safe
        char buf[1024];
        va_list ap;
        va_start(ap, chMsg);
        vsprintf_s(buf, sizeof(buf)-1, chMsg, ap);
        va_end(ap);
        buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
        m_pclsLog->Write("ProtocolManager.cpp", chFunctionName, MsgType, buf);
        LeaveCriticalSection(&CProtocolManagerSection);
    }
}




