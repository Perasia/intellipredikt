/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: FT_ISO15765.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support ISO15765 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#define _FT_ISO15765_
#include "FT_ISO15765.h"

//-----------------------------------------------------------------------------
//  Function Name   : CFTISO15765
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CFTISO15765 class
//-----------------------------------------------------------------------------
CFTISO15765::CFTISO15765(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog) : CProtocolBase(pclsDevice, pclsDebugLog, pclsDataLog)
{
    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "CFTISO15765()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;
    m_bLoopback = false;
	m_bErrorReporting = false;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "CFTISO15765()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CFTISO15765
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CFTISO15765 class
//-----------------------------------------------------------------------------
CFTISO15765::~CFTISO15765()
{
    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "~CFTISO15765()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "~CFTISO15765()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vConnect(J2534_PROTOCOL   enProtocolID,
                                unsigned long   ulFlags,
                                unsigned long   ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                                LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    m_ulCANHeaderSupport = ((ulFlags >> 11) & 0x01);
    m_ulCANIDtype = ((ulFlags >> 8) & 0x01);
    m_ulCANExtAddr = ((ulFlags >> 7) & 0x01);
    
    if (m_ulCANIDtype == 0) // Standard CAN
    {
        m_ulCANHeaderBits = FT_ISO15765_HEADER_SIZE;
//      ulFlags = 0;
    }
    else  // Extended CAN
    {       
        m_ulCANHeaderBits = FT_ISO15765_EXTENDED_HEADER_SIZE;
//      ulFlags = J2534_CONNECT_FLAGBIT_CAN29BIT;
    }

    if (!m_ulCANDataRateDefault)    // Over ride NOT specified in registry
    {
        m_ulCANDataRateDefault = FT_ISOCAN_DATA_RATE_DEFAULT;
    }

//    if(enProtocolID == ISO15765 )
    switch (enProtocolID)
    {
        case FT_ISO15765_CH1:

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            if ((ulBaudRate != FT_ISOCAN_DATA_RATE_LOW) && 
 #ifdef J2534_DEVICE_VSI
                (ulBaudRate !=95200) && 
 #endif  
 #ifdef J2534_DEVICE_NETBRIDGE
                (ulBaudRate !=50000) && 
 #endif
                (ulBaudRate !=95238)/* && 
               (ulBaudRate != FT_ISOCAN_DATA_RATE_DEFAULT) && ((ulBaudRate != FT_ISOCAN_DATA_RATE_MEDIUM))*/)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
#ifdef J2534_0305
            ulBaudRate = m_ulCANDataRateDefault;
            if ((ulBaudRate != FT_ISOCAN_DATA_RATE_DEFAULT) && 
#ifndef UD_TRUCK
                (ulBaudRate != FT_ISOCAN_DATA_RATE_MEDIUM))
#else
                (ulBaudRate != FT_ISOCAN_DATA_RATE_MEDIUM_HIGH))
#endif
            {
                WriteLogMsg("FT_ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "ulBaudRate=%d", ulBaudRate);
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
            m_ulDataRate = ulBaudRate;
            m_enISO15765Protocol = enProtocolID;
        default:
            break;
        
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "enProtocolID=%d, ulFlags=%0X, ulBaudRate=%d", enProtocolID, ulFlags, ulBaudRate);
    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate,
                                                OnFTISO15765RxMessage, this))
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    m_ulBlockSize = 0;
    m_ulBlockSizeTx = 0xFFFF;
    m_ulSTmin = 0;
    m_ulSTminTx = 0xFFFF;
    m_ulWftMax = 0;
	m_enCANMixedFormat = J2534_CAN_MIXED_FORMAT_OFF; 

    m_ulPPSS = 0;
    m_bJ1962Pins = false;
    m_ulJ1939PPSS = 0;
    m_bJ1939Pins = false;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vReadMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
/*
    if (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
        return(J2534_ERR_NO_FLOW_CONTROL);
    }
*/  
    // Check Pins
    if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Read using the generic Read.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        if (enJ2534Error == J2534_ERR_BUFFER_EMPTY)
        {
            if ((m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF) && 
                (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet()))))
            {
                // Write to Log File.
                WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
                return(J2534_ERR_NO_FLOW_CONTROL);
            }
        }
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
		if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
		{
			// Check Msg. Protocol ID.
			if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enISO15765Protocol)
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}

			if (((pstPassThruMsg + ulIdx1)->ulDataSize > FT_ISO15765_MSG_SIZE_MAX_SF) && 
				((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
				return(J2534_ERR_NO_FLOW_CONTROL);
			}

			// Check if msg. format is valid.
			if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
	#ifdef J2534_0305
			if (m_ulCANExtAddr)
			{
				(pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x80;
			}
	#endif
			if((pstPassThruMsg + ulIdx1)->ulDataSize > (FT_ISO15765_MSG_SIZE_MIN + 7)) //Check Only for long messages
			{
				if(!m_pclsFilterMsg->DoesFlowControlFilterMatch(pstPassThruMsg + ulIdx1))
				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
					return(J2534_ERR_NO_FLOW_CONTROL);
				}
			}
		}
		else
		{
			if ((m_enISO15765Protocol == FT_ISO15765_CH1 && (pstPassThruMsg + ulIdx1)->ulProtocolID == FT_CAN_CH1))
			{
			}
			else
			{
				// Check Msg. Protocol ID.
				if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enISO15765Protocol)
				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
				}
			}

			if ((pstPassThruMsg + ulIdx1)->ulProtocolID == FT_ISO15765_CH1)
			{
				if (((pstPassThruMsg + ulIdx1)->ulDataSize > FT_ISO15765_MSG_SIZE_MAX_SF) && 
					((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
					return(J2534_ERR_NO_FLOW_CONTROL);
				}

				// Check if msg. format is valid.
				if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
					return(J2534_ERR_INVALID_MSG);
				}
		#ifdef J2534_0305
				if (m_ulCANExtAddr)
				{
					(pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x80;
				}
		#endif
				if((pstPassThruMsg + ulIdx1)->ulDataSize > (FT_ISO15765_MSG_SIZE_MIN + 7)) //Check Only for long messages
				{
					if(!m_pclsFilterMsg->DoesFlowControlFilterMatch(pstPassThruMsg + ulIdx1))
					{
						// Write to Log File.
						WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
						return(J2534_ERR_NO_FLOW_CONTROL);
					}
				}
			}
			else
			{
				// Check if msg. format is valid.
				if (!IsCANMsgValid((pstPassThruMsg + ulIdx1)))
				{
					// Write to Log File.
					WriteLogMsg("CAN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
					return(J2534_ERR_INVALID_MSG);
				}
			}
		}
    }
    
    // Check Pins
    if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vStartPeriodicMsg(PASSTHRU_MSG    *pstPassThruMsg,
                                        unsigned long   *pulMsgID,
                                        unsigned long   ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
	{
		// Check Msg. Protocol ID.
		if (pstPassThruMsg->ulProtocolID != m_enISO15765Protocol)
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}
    
		if ((pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX_SF))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
		// Check if msg. format is valid.
		if (!IsMsgValid(pstPassThruMsg))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

	#ifdef J2534_0305
		if (m_ulCANExtAddr)
		{
			pstPassThruMsg->ulTxFlags |= 0x80;
		}
	#endif
	}
	else
	{
		if (m_enISO15765Protocol == FT_ISO15765_CH1 && pstPassThruMsg->ulProtocolID == FT_CAN_CH1)
		{
		}
		else
		{
			// Check Msg. Protocol ID.
			if (pstPassThruMsg->ulProtocolID != m_enISO15765Protocol)
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}
		}

		if (pstPassThruMsg->ulProtocolID == FT_ISO15765_CH1)
		{
			if ((pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX_SF))
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
			// Check if msg. format is valid.
			if (!IsMsgValid(pstPassThruMsg))
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}

		#ifdef J2534_0305
			if (m_ulCANExtAddr)
			{
				pstPassThruMsg->ulTxFlags |= 0x80;
			}
		#endif
		}
		else
		{
			// Check if msg. format is valid.
			if (!IsCANMsgValid(pstPassThruMsg))
			{
				// Write to Log File.
				WriteLogMsg("CAN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
				return(J2534_ERR_INVALID_MSG);
			}
		}
	}    
    
    // Check Pins
    if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CFTISO15765::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::vStartMsgFilter(J2534_FILTER enumFilterType,
                                       PASSTHRU_MSG     *pstMask,
                                       PASSTHRU_MSG     *pstPattern,
                                       PASSTHRU_MSG     *pstFlowControl,
                                       unsigned long    *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
	if (m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_OFF)
	{
		// Check Filter Type.
		if (enumFilterType != J2534_FILTER_FLOW_CONTROL)
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		    SetLastErrorText("ISO15765 : Invalid FilterType value");
            return(J2534_ERR_FAILED);
		}
		// Check Msg. Protocol ID.
		if ((pstMask->ulProtocolID != m_enISO15765Protocol) ||
			(pstPattern->ulProtocolID != m_enISO15765Protocol) || 
			(pstFlowControl->ulProtocolID != m_enISO15765Protocol))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}
	}
	else
	{
		if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
		{			
			// Check Msg. Protocol ID.
			if ((pstMask->ulProtocolID != m_enISO15765Protocol) ||
				(pstPattern->ulProtocolID != m_enISO15765Protocol) || 
				(pstFlowControl->ulProtocolID != m_enISO15765Protocol))
			{
				// Write to Log File.
				WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
				return(J2534_ERR_MSG_PROTOCOL_ID);
			}
		}
		else
		{
			if (m_enISO15765Protocol == FT_ISO15765_CH1 && pstMask->ulProtocolID == FT_CAN_CH1)
			{
			}
			else
			{
				// Check Msg. Protocol ID.
//				if ((pstMask->ulProtocolID != m_enISO15765Protocol))
//				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
//				}
			}
			if (m_enISO15765Protocol == FT_ISO15765_CH1 && pstPattern->ulProtocolID == FT_CAN_CH1)
			{
			}
			else
			{
				// Check Msg. Protocol ID.
//				if ((pstPattern->ulProtocolID != m_enISO15765Protocol))
//				{
					// Write to Log File.
					WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
					return(J2534_ERR_MSG_PROTOCOL_ID);
//				}
			}
		}
	}
#endif
#ifdef J2534_0305
    // Check Msg. Protocol ID.
    if (enumFilterType != J2534_FILTER_FLOW_CONTROL)
    {
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "PAR-TEST 464 Not Flow Control Filter");
    }
    if ((pstMask->ulProtocolID != m_enISO15765Protocol) ||
        (pstPattern->ulProtocolID != m_enISO15765Protocol) || 
        ((enumFilterType == J2534_FILTER_FLOW_CONTROL && pstFlowControl->ulProtocolID != m_enISO15765Protocol)) )
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
    if (m_ulCANExtAddr)
    {
    WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "476");
        pstMask->ulTxFlags |= 0x80;
        pstPattern->ulTxFlags |= 0x80;
        if (pstFlowControl)
        {
            pstFlowControl->ulTxFlags |= 0x80;
        }
    }
 #ifdef UD_TRUCK
    // Fix up for UDTruck application 
    if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
    {
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Fix up ulTxFlags J2534_CONNECT_FLAGBIT_CAN29BIT for UDTruck application");
        pstMask->ulTxFlags |= J2534_CONNECT_FLAGBIT_CAN29BIT;
        pstPattern->ulTxFlags |= J2534_CONNECT_FLAGBIT_CAN29BIT;
        if (pstFlowControl)
        {
            pstFlowControl->ulTxFlags |= J2534_CONNECT_FLAGBIT_CAN29BIT;
        }
    }
    if ( (pstPattern->ulTxFlags & 0x40) || (pstPattern->ulTxFlags & 0x40) || (pstPattern->ulTxFlags & 0x40))
    {
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Fix up ulTxFlags padding for UDTruck application");
        pstMask->ulTxFlags |= 0x40;
        pstPattern->ulTxFlags |= 0x40;
        if (pstFlowControl)
        {
            pstFlowControl->ulTxFlags |= 0x40;
        }
    }
 #endif
#endif
	if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
	{
		// Check Filter Type.

		// NOTE : In ISO15765, if pstFlowControl is not NULL then check for  
		//        J2534_ERR_MSG_PROTOCOL_ID.

		// NOTE : In ISO15765, pass the pstFlowControl through 
		//        IsMsgValid().
		// Check if msg. format is valid.
		if (!IsMsgValid(pstPattern, true))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "470 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

		if (!IsMsgValid(pstMask, true))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "477 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

	/* TODO - this is in SWFT_ISO15765.cpp but missing here, verify if we should have here it, too.
		if (!IsMsgValid(pstFlowControl, true))
		{
			// Write to Log File.
			WriteLogMsg("SWFT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	*/
	}
	else
	{
		if (!IsCANMsgValid(pstPattern, true))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "470 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}

		if (!IsCANMsgValid(pstMask, true))
		{
			// Write to Log File.
			WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "477 returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}
    // Check Pins
    if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }
    // NOTE : In ISO15765, check to see that the mask of 4 or 5 bytes are
    //        all 0xFF. The size of pstMask, pstPattern and pstFlowControl
    //        should be 4 or 5 bytes depanding on extended addressing or not
    //        respectively.

    // NOTE : In ISO15765, if the filter type is PASS or BLOCK return error.
    //        In other words only allow FLOW_CONTROL filter type.

    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::vIoctl(J2534IOCTLID enumIoctlID,
                              void *pInput,
                              void *pOutput)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "enumIoctlID=%d", enumIoctlID); // PAR-TEST
    // IoctlID values
    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration
        
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;
        
    case SET_CONFIG:            // Set configuration
        
        enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);

        break;
        
    case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue
        
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsTxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
        
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsRxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsPeriodicMsg != NULL)
        {
            delete m_pclsPeriodicMsg;
            m_pclsPeriodicMsg = NULL;
        }
        
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsFilterMsg != NULL)
        {
            delete m_pclsFilterMsg;
            m_pclsFilterMsg = NULL;
        }
        
        break;

#ifdef J2534_DEVICE_NETBRIDGE
	case CAN_SET_BTR:
        {
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
    case CAN_SET_ERROR_REPORTING:
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
		enumJ2534Error=SetErrorReporting((unsigned char *)pInput);
		break;
#endif

#ifdef J2534_DEVICE_DBRIDGE
	case START_REPEAT_MESSAGE:
	case QUERY_REPEAT_MESSAGE:
	case STOP_REPEAT_MESSAGE:
        {
        // Check Pins
        if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if(enumIoctlID == START_REPEAT_MESSAGE)
            {
                // Check if msg. format is valid.
                REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
                if ((!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]))))
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
                    return(J2534_ERR_INVALID_MSG);
                }
            }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        pOutput))                                                      
            {
                // Write to Log File.
                WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
#endif

    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }
    
    // Write to Log File.
    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnFTISO15765Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    FT_ISO15765 messages.
//-----------------------------------------------------------------------------
void OnFTISO15765RxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CFTISO15765                   *pclsISO15765;
    FILTERMSG_CONFORM_REQ   stConformReq;
    unsigned long ulSavedDataSize = pstPassThruMsg->ulDataSize;
    unsigned long   i, j;
    char            szChannelPins[MAX_PATH];
    char            szProtocol[MAX_PATH];
    unsigned long   ulRxTx;
    unsigned long   ulTimestamp;
    char            szID[MAX_PATH];
    char            szData[MAX_PATH];

    pclsISO15765 = (CFTISO15765 *) pVoid;

    // Check for NULL pointer.
    if (pclsISO15765 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    memset(szChannelPins,0,MAX_PATH);
    memset(szProtocol,0,MAX_PATH);
    ulRxTx = 0;
    ulTimestamp = 0;
    memset(szID,0,MAX_PATH);
    memset(szData,0,MAX_PATH);

    // Data Logging Protocol and its Channel/Pins
    switch(pstPassThruMsg->ulProtocolID)
    {
    case FT_CAN_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"FTCAN");
    case FT_ISO15765_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"FTISO15765");
        break;
    default:
        break;
    }

    // Data Logging Rx/Tx
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        ulRxTx = DATALOG_TX;
    }
    else
    {
        ulRxTx = DATALOG_RX;
    }

    if (!(pstPassThruMsg->ulProtocolID == FT_ISO15765_CH1))
	{
        if (pclsISO15765->m_pclsFilterMsg != NULL)
	    {
            // See if any received CAN segmented message is really a transmitted message
            if (pclsISO15765->m_pclsFilterMsg->DoesFlowControlFilterMatchRxFCID(pstPassThruMsg))
            {
                if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE) == 0)
                {
                    ulRxTx = DATALOG_TX;
                }
            }
        }
    }

    // Data Logging timestamp
    ulTimestamp = pstPassThruMsg->ulTimeStamp;

    // Data Logging ID
    if(pstPassThruMsg->ulDataSize > 0)
    {
        j = 0;

        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == J2534_RX_FLAGBIT_CAN29BIT)
        {
            for(i = 0; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }
        else
        {
            for(i = 2; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }

        j = 0;

        for(i = 4; i < pstPassThruMsg->ulDataSize && i < 15; i++)
        {
            j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
        }

        if(i >= 15)
            j += sprintf_s(szData + j, sizeof(szData)-j, "...");
    }

    if (((pstPassThruMsg->ulRxStatus & 2) == 0) &&
        ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_ISO15765_TIMEOUT) == 0))
    {
        // Write to Data Log File.
        CProtocolBase::WriteDataLogMsg(szChannelPins, szProtocol, ulRxTx, ulTimestamp, szID, szData);
    }

    // Check if the message is valid as per the Connect Flag is set.
    if ((pclsISO15765->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (pclsISO15765->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return;
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return;
            }
        }
    }

	if (pstPassThruMsg->ulProtocolID == FT_ISO15765_CH1)
	{
#ifdef J2534_DEVICE_NETBRIDGE
        // ISO15765 timeout
        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_ISO15765_TIMEOUT) && (pclsISO15765->m_bErrorReporting))
        {
 			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "ISO15765 Timeout");
			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulTxFlags 0x%X", pstPassThruMsg->ulTxFlags);
			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulDataSize 0x%X", pstPassThruMsg->ulDataSize);
			// Enqueue to Circ Buffer.
			pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
            return;
        }
#endif
        // Check if this is a Loopback message.
		if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
		{
			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");

			//if (pclsISO15765->m_bLoopback == false)
			//	return;

			pstPassThruMsg->ulDataSize = ulSavedDataSize;  // Restore ulDataSize
			pstPassThruMsg->ulExtraDataIndex = pstPassThruMsg->ulDataSize;

            if (pclsISO15765->m_bLoopback)
            {
                pstPassThruMsg->ulRxStatus &= (0xFFFFFFFF - 0x08);
			    // Enqueue to Circ Buffer.
			    pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
                pstPassThruMsg->ulRxStatus |= 0x08;
            }
			//return;
		}
		// Check if this is a Loopback message.
		if (pstPassThruMsg->ulRxStatus & (0x08 | J2534_RX_FLAGBIT_MSGTYPE))
		{
			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx Done CALLBACK");

	//      if (pclsISO15765->m_bLoopback == false)
	//          return;

			if (pstPassThruMsg->ulTxFlags & 0x80)
				pstPassThruMsg->ulDataSize = 5;
			else
				pstPassThruMsg->ulDataSize = 4;

			pstPassThruMsg->ulExtraDataIndex = 0;//pstPassThruMsg->ulDataSize;

			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulTxFlags 0x%X", pstPassThruMsg->ulTxFlags);
			// Write to Log File.
			CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulDataSize 0x%X", pstPassThruMsg->ulDataSize);
			// Enqueue to Circ Buffer.
			pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
			return;
		}
    
		// Write to Log File.
		CProtocolBase::WriteLogMsg("FT_ISO15765.cpp", "OnISO15765RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
#ifdef J2534_DEVICE_DBRIDGE
        // Repeat msg support
        if(pclsISO15765->m_pclsRepeatMsg != NULL)
        {
            pclsISO15765->m_pclsRepeatMsg->CompareMsg(pstPassThruMsg);
        }
#endif
		// Apply Filters and see if msg. is required.
		if (pclsISO15765->m_pclsFilterMsg != NULL)
		{
			stConformReq.bReqPass = false;
			stConformReq.bReqBlock = false;
			stConformReq.bReqFlowControl = true;
			if (pclsISO15765->IsMsgValidRx(pstPassThruMsg) &&
                pclsISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			{
				// Enqueue to Circ Buffer.
				pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
			}
		}
	#ifdef J2534_0305
		else
        {
            if (pclsISO15765->IsMsgValidRx(pstPassThruMsg))
            {
			    // Enqueue to Circ Buffer.
			    pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
            }
        }
	#endif
	}
	else
	{
        if (pclsISO15765->m_enCANMixedFormat != J2534_CAN_MIXED_FORMAT_OFF)
		{
            if (pclsISO15765->m_pclsFilterMsg != NULL)
		    {
                // See if any received CAN segmented message is really a transmitted message
                if (pclsISO15765->m_pclsFilterMsg->DoesFlowControlFilterMatchRxFCID(pstPassThruMsg))
                {
                    // Write to Log File.
			        CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "DoesFlowControlFilterMatchRxFCID Yes");
                    if (pclsISO15765->IsCANMsgValidRx(pstPassThruMsg) &&
                        pclsISO15765->m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_ON &&
                        (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE) == 0)
                    {
			            CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Thrown out");
                        return;
                    }
                    else
                    {
                        pstPassThruMsg->ulRxStatus |= J2534_RX_FLAGBIT_MSGTYPE;
                        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
                        {
                            pstPassThruMsg->ulTxFlags |= J2534_TX_FLAGBIT_CAN29BIT;
                        }
                        if ((pstPassThruMsg->ulRxStatus & (1 << 16)) != 0)
                        {
                            pstPassThruMsg->ulTxFlags |= (1 << 10);
                        }
			            CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Changed to Tx CALLBACK");
                    }
                }
            }
		    // Check if this is a Loopback message.
		    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
		    {
			    // Write to Log File.
			    CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
			    if (pclsISO15765->m_bLoopback == false)
				    return;
			    // Enqueue to Circ Buffer.
			    pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
						    pstPassThruMsg->ulDataSize));
			    return;
		    }
		    // Write to Log File.
		    CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
		    // Apply Filters and see if msg. is required.
		    if (pclsISO15765->m_pclsFilterMsg != NULL)
		    {
                stConformReq.bReqPass = false;
			    stConformReq.bReqBlock = false;
			    stConformReq.bReqFlowControl = true;
			    if (pclsISO15765->IsCANMsgValidRx(pstPassThruMsg) &&
                    pclsISO15765->m_enCANMixedFormat == J2534_CAN_MIXED_FORMAT_ON &&
                    pclsISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			    {
				    // If CAN Mixed Format On and rx message conforms to Flow Control Filter, then ignore
                    // Write to Log File.
				    CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Rx Message Ignored");
				    return;
			    }
			    stConformReq.bReqPass = true;
			    stConformReq.bReqBlock = true; 
			    stConformReq.bReqFlowControl = false;
			    if (pclsISO15765->IsCANMsgValidRx(pstPassThruMsg) &&
                    pclsISO15765->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
			    {
				    // Write to Log File.
				    CProtocolBase::WriteLogMsg("CAN.cpp", "OnCANRx()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK PreEnd");
				    // Enqueue to Circ Buffer.
				    pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
						    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
						    pstPassThruMsg->ulDataSize));
			    }
		    }
    #ifdef J2534_0305
		    else
            {
                if (pclsISO15765->IsCANMsgValidRx(pstPassThruMsg))
                {
                    // Enqueue to Circ Buffer.
                    pclsISO15765->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                        (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                        pstPassThruMsg->ulDataSize));
                }
            }
    #endif
        }
	}

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CFTISO15765::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    if ((pstPassThruMsg->ulTxFlags & 0x80) == 0)
    {
        if ((pstPassThruMsg->ulDataSize < FT_ISO15765_MSG_SIZE_MIN) || 
            (pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX))
        {
            return(false);
        }
    }
    else if ((pstPassThruMsg->ulTxFlags & 0x80) != 0)
    {
        if ((pstPassThruMsg->ulDataSize < FT_ISO15765_MSG_SIZE_MIN+1) || 
            (pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX+1))
        {
            return(false);
        }
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsCANMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CFTISO15765::IsCANMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    if ((pstPassThruMsg->ulDataSize < FT_ISO15765_CAN_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > FT_ISO15765_CAN_MSG_SIZE_MAX))
    {
        WriteLogMsg("CAN.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 715 ulDataSize=%d",pstPassThruMsg->ulDataSize); // PAR-TEST
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                WriteLogMsg("CAN.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 725 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                WriteLogMsg("CAN.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 733 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
    }
    
    return(true);
}     

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CFTISO15765::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulRxStatus & 0x80) == 0)
    {
        if ((pstPassThruMsg->ulDataSize < FT_ISO15765_MSG_SIZE_MIN) || 
            (pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX))
        {
            return(false);
        }
    }
    else if ((pstPassThruMsg->ulRxStatus & 0x80) != 0)
    {
        if ((pstPassThruMsg->ulDataSize < FT_ISO15765_MSG_SIZE_MIN+1) || 
            (pstPassThruMsg->ulDataSize > FT_ISO15765_MSG_SIZE_MAX+1))
        {
            return(false);
        }
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsCANMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CFTISO15765::IsCANMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < FT_ISO15765_CAN_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > FT_ISO15765_CAN_MSG_SIZE_MAX))
    {
        WriteLogMsg("CAN.cpp", "IsCANMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsCANMsgValidRx fail 715 ulDataSize=%d",pstPassThruMsg->ulDataSize); // PAR-TEST
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                WriteLogMsg("CAN.cpp", "IsCANMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsCANMsgValidRx fail 725 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                WriteLogMsg("CAN.cpp", "IsCANMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsCANMsgValidRx fail 733 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("FT_ISO15765.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("FT_ISO15765 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                pSconfig->ulValue = (unsigned long) m_fSamplePoint;

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                pSconfig->ulValue = (unsigned long) m_fJumpWidth;

                break; 
                
            case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS;    
                
                if (m_enISO15765Protocol != FT_ISO15765_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;      
                
            case J1939_PINS:

                pSconfig->ulValue = m_ulJ1939PPSS;

                if (m_enISO15765Protocol != FT_ISO15765_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case ISO15765_BS:       // Block Size

                pSconfig->ulValue = m_ulBlockSize;

                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case BS_TX:             // Block Size Tx

                pSconfig->ulValue = m_ulBlockSizeTx;

                break;
#endif
            case ISO15765_STMIN:        // STmin

                pSconfig->ulValue = m_ulSTmin;

                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case STMIN_TX:              // STminTx

                pSconfig->ulValue = m_ulSTminTx;

                break;

#ifdef J2534_0500
			case ISO15765_WAIT_LIMIT:
#endif
#ifdef J2534_0404
			case ISO15765_WFT_MAX:
#endif
                pSconfig->ulValue = m_ulWftMax;

                break;

            case CAN_MIXED_FORMAT:

                pSconfig->ulValue = m_enCANMixedFormat;

                break;
#endif
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;
    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("FT_ISO15765.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("FT_ISO15765 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Parameter is 0x%02X", pSconfig->Parameter);
        WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate
                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = DataRate(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback
    
                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Loopback(pSconfig->ulValue);
                }
                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                
                //enumJ2534Error = SamplePoint(pSconfig->ulValue);

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                
                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;
            
            case ISO15765_BS:       // Block Size

                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = BlockSize(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case BS_TX:             // Block Size Tx

                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = BlockSizeTx(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#endif
            case ISO15765_STMIN:        // STmin
                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = STmin(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case STMIN_TX:              // STminTx

                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = STminTx(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifdef J2534_0500
			case ISO15765_WAIT_LIMIT:
#else 
			case ISO15765_WFT_MAX:
			case W0:                // for BMW - par091214
#endif        
				if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = WftMax(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
			case CAN_MIXED_FORMAT:       // CAN Mixed Format
 #ifndef J2534_DEVICE_VSI
                // CAN_MIXED_FORMAT has only been added to VSI2534 (for now) par130904
                // remove when supported add for all DG tools
 //               return(J2534_ERR_NOT_SUPPORTED);
 #endif
                if((m_enISO15765Protocol == FT_ISO15765_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = CANMixedMode(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                break;
#endif          
            case J1962_PINS:
                if (m_enISO15765Protocol != FT_ISO15765_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1962Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    }
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulPPSS = pSconfig->ulValue;
                    else
                        m_bJ1962Pins = false;
                }
                break;                            

            case J1939_PINS:
                if (m_enISO15765Protocol != FT_ISO15765_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1939Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("FT_ISO15765.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulJ1939PPSS = pSconfig->ulValue;  
                    else
                        m_bJ1939Pins = false;
                }
                break;
            
            default:

                return(J2534_ERR_NOT_SUPPORTED);
                break;
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate for a ISO15765 channel.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    if ((ulValue < 5) || (ulValue > 1000000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Set DataRate to # of kbps
    if ((ulValue == FT_ISOCAN_DATA_RATE_LOW) 
#ifdef J2534_DEVICE_VSI
         || (ulValue ==95200) 
#endif      
#ifdef J2534_DEVICE_NETBRIDGE
         || (ulValue ==50000)
#endif
#ifndef UD_TRUCK
         || (ulValue ==95238)// || 
//        (ulValue == FT_ISOCAN_DATA_RATE_MEDIUM) || 
#else
//        (ulValue == FT_ISOCAN_DATA_RATE_MEDIUM_HIGH) || 
#endif
/*        (ulValue == FT_ISOCAN_DATA_RATE_DEFAULT)*/)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

J2534ERROR  CFTISO15765::BlockSize(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulBlockSize = ulValue;

    return(iReturn);
}

J2534ERROR  CFTISO15765::BlockSizeTx(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulBlockSizeTx = ulValue;

    return(iReturn);
}

J2534ERROR  CFTISO15765::STmin(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulSTmin = ulValue;

    return(iReturn);
}

J2534ERROR  CFTISO15765::STminTx(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulSTminTx = ulValue;

    return(iReturn);
}

J2534ERROR  CFTISO15765::WftMax(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
#ifndef J2534_DEVICE_DPA5_EURO5
    if (ulValue != 0)
        return(J2534_ERR_NOT_SUPPORTED);
#endif
    m_ulWftMax = ulValue;
    return(iReturn);
}

J2534ERROR  CFTISO15765::CANMixedMode(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

#ifndef J2534_DEVICE_VSI
    if (ulValue != J2534_CAN_MIXED_FORMAT_OFF && ulValue != J2534_CAN_MIXED_FORMAT_ON &&
        ulValue != J2534_CAN_MIXED_FORMAT_ALL_FRAMES)
#else
    if (ulValue != J2534_CAN_MIXED_FORMAT_OFF && ulValue != J2534_CAN_MIXED_FORMAT_ON)
#endif
        return(J2534_ERR_NOT_SUPPORTED);

	if (m_pclsPeriodicMsg != NULL)
    {
        m_pclsPeriodicMsg->StopCANPeriodic();
    }
        
    if (m_pclsFilterMsg != NULL)
    {
        m_pclsFilterMsg->StopPassAndBlockFilter();
    }

	m_pclsTxCircBuffer->ClearBuffer();
	m_pclsRxCircBuffer->ClearBuffer();

	m_enCANMixedFormat = (J2534_CAN_MIXED_FORMAT)ulValue;

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetErrorReporting
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set error level reporting.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::SetErrorReporting(unsigned char *pucValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
	// Make sure pucValue is not NULL
    if (pucValue == NULL)
        return(J2534_ERR_NULLPARAMETER);

    //Valid values for <parameter> are 0-1
    if(*pucValue < 0 || *pucValue > 1)
    {
        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
        return(enumJ2534Error);
    }

    m_bErrorReporting = (*pucValue == 1) ? true : false;

    return(enumJ2534Error);
} 

//-----------------------------------------------------------------------------
//  Function Name   : SetInternalTermination
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set internal termination.
//-----------------------------------------------------------------------------
J2534ERROR  CFTISO15765::SetInternalTermination(unsigned char *pucValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
 	// Make sure pucValue is not NULL
    if (pucValue == NULL)
        return(J2534_ERR_NULLPARAMETER);

	//Valid values for <parameter> are 0-1
    if(*pucValue < 0 || *pucValue > 1)
    {
        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
        return(enumJ2534Error);
    }

    return(enumJ2534Error);
}            

J2534ERROR  CFTISO15765::J1962Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x1000) || ((ulValue & 0x00FF) > 0x0010))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 4) || (ulValue == 4) ||
        ((ulValue >> 8) == 5) || (ulValue == 5) ||
        ((ulValue >> 8) == 16) || (ulValue == 16))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x060E) || 
        (ulValue == 0x030B) ||
        (ulValue == 0x030D)) // Honda specific CAN 2 (3&13)
    {
        m_bJ1962Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}          

J2534ERROR  CFTISO15765::J1939Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x0900) || ((ulValue & 0x00FF) > 0x0009))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 1) || (ulValue == 1) ||
        ((ulValue >> 8) == 2) || (ulValue == 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0304) || 
        (ulValue == 0x0809))
    {
        m_bJ1939Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}
