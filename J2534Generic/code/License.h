#ifndef LICENSE_H
#define LICENSE_H

#include <string>
#include "PLUSNative.h"

//#ifdef WIN32
//    #ifdef _DEBUG
//        #define new DEBUG_NEW
//    #endif
//#endif

using namespace std;

typedef void (*REFRESH_STATUS_CALLBACK)();
typedef void (*TERMINATE_APPLICATION_CALLBACK)();

namespace LicenseType
{
    enum Enum
    {
        Unlicensed = 0,
        FullNonExpiring = 1,
        TimeLimited = 10
    };
}

namespace OptionType
{
    enum Enum 
    {
        // Standard Protection PLUS 4 and 5 activation code.
        ActivationCode = 0,
        // Protection PLUS 4 and 5 activation code with the quantity of items ordered.
        ActivationCodeWithQuantity = 1,
        // Protection PLUS 4 and 5 activation code with an additional, fixed value.
        ActivationCodeWithFixedValue = 2,
        // Protection PLUS 4 and 5 activation code with the number of days left until the license expires.
        ActivationCodeWithDaysLeft = 3,
        // Protection PLUS 5 volume license.
        VolumeLicense = 4,
        // Protection PLUS 5 downloadable license with Protection PLUS 4 compatible trigger code validation.
        DownloadableLicenseWithTriggerCodeValidation = 5,
        // An unknown or Custom value.
        Custom = 6
    };
}

    string FromUTF8(const char *in);
    string ToUTF8(const string in);

class License
{

public:

    // Default constructor/destructor
    License(int productID, const string productVersion, const string envelope, const string envelopeKey);
    virtual ~License();

    // Initialization methods
    void AddAlias(const string path);
    void InitializeSystemIdentifiers();
    void SetLicenseFilePath(const string path);
    void SetRefreshStatusCallback(REFRESH_STATUS_CALLBACK callback);
    void SetSessionCodePath(const string path);
    void SetTerminateApplicationCallback(TERMINATE_APPLICATION_CALLBACK callback);

    // Licensing related methods
    static LicenseType::Enum DetermineType(SK_XmlDoc license);
    LicenseType::Enum GetType();
    OptionType::Enum GetOptionType();
    void Reload();
    bool Validate();

    // Evaluation methods
    void CreateFreshEvaluation(int daysTillExpiration = 30);
    bool IsEvaluation();

    // Web service methods
    bool ActivateOnline(int licenseID, const string password, const string installationName);
    bool DeactivateOnline();
    bool RefreshOnline();
	bool SetProxyCredentials(string proxyUrl, string userName, string password);

    // Manual activation methods
    SK_XmlDoc GetManualActivationRequest(int licenseID, const string password, const string installationName);
    SK_XmlDoc GetManualRefreshRequest(const string installationName);
    SK_XmlDoc GetManualDeactivationRequest(const string installationName);
    string GetSessionCode();
    bool ProcessManualResponse(const string responseString, const string rootNode);

    // License file read/write methods
    string GetDateTimeStringValue(const string xpath);
    double GetDoubleValue(const string xpath);
    int GetIntegerValue(const string xpath);
    string GetStringValue(const string xpath);
    void SetDateTimeStringValue(const string xpath, const string value);
    void SetDoubleValue(const string xpath, double value);
    void SetIntegerValue(const string xpath, int value);
    void SetStringValue(const string xpath, const string value);

    // Date-time methods
    int GetDaysRemaining();
    int GetDaysEffective();
    bool IsDateTimePast(const string xpath);
    void SetLastUpdatedDateTime(bool force = false);

    // Error related methods
    string GetErrorMessage();
    int GetExtendedErrorNo();
	int GetStatusCode();
    string GetExtendedErrorString();
    SK_ResultCode GetLastErrorNo();
    string GetLastErrorString();
    void TerminateApplication();
    static bool ShouldLicenseBeRevoked(const int result);
    string m_savedSessionCode;
    string m_SessionCodePath;
    CString m_licenseAlias1Path;
    CString m_licenseAlias2Path;

private:

    // Private member variables
    SK_ApiContext m_Context;
    int m_ExtendedErrorNo;
	int m_StatusCode;
    string m_ExtendedErrorString;
    bool m_IsLoaded;
    bool m_IsWritable;
    SK_ResultCode m_LastErrorNo;
    SK_XmlDoc m_CurrentIdentifier;
    string m_LicenseFilePath;
    REFRESH_STATUS_CALLBACK m_RefreshLicenseStatusCallback;
    TERMINATE_APPLICATION_CALLBACK m_TerminateApplicationCallback;

    // Private licensing related methods
    void SetWritable(bool val = true);

    // Private error related methods
    void CheckResult(SK_ResultCode result);
};

#endif
