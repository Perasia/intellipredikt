/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: LIN.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support LIN 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "LIN.h"

#ifdef J2534_DEVICE_NETBRIDGE
// LIN only implemented on Netbridge

//-----------------------------------------------------------------------------
//  Function Name   : CLIN
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CLIN class
//-----------------------------------------------------------------------------
CLIN::CLIN(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) : CProtocolBase(pclsDevice, pclsDebugLog)
{
    // Write to Log File.
    WriteLogMsg("LIN.cpp", "CLIN()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "CLIN()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CLIN
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CLIN class
//-----------------------------------------------------------------------------
CLIN::~CLIN()
{
    // Write to Log File.
    WriteLogMsg("LIN.cpp", "~CLIN()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "~CLIN()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vConnect(J2534_PROTOCOL  enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    switch(enProtocolID)
    {
    case LIN:
        {
#ifdef J2534_0305
            ulBaudRate = LIN_DATA_RATE_DEFAULT;
#endif
            m_ulDataRate = ulBaudRate;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnLINRxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vReadMsgs(PASSTHRU_MSG       *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vWriteMsgs(PASSTHRU_MSG  *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != LIN)
        {
            // Write to Log File.
            WriteLogMsg("LIN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("LIN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    
    
    
    
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
	,0
#endif 
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vStartPeriodicMsg(PASSTHRU_MSG       *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != LIN)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CLIN::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::vStartMsgFilter(J2534_FILTER    enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != LIN) ||
        (pstPattern->ulProtocolID != LIN))
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("LIN : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration

            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("LIN.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnLINRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    LIN messages.
//-----------------------------------------------------------------------------
void OnLINRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CLIN                  *pclsLIN;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsLIN = (CLIN *) pVoid;

    // Check for NULL pointer.
    if (pclsLIN == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CLIN.cpp", "OnLINRxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    // Write to Log File.
    CProtocolBase::WriteLogMsg("CLIN.cpp", "OnLINRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Message CallBack");

    // LIN error
    if (((pstPassThruMsg->ulRxStatus >> 24) & 0xFF) > 0)
    {
		// Write to Log File.
		CProtocolBase::WriteLogMsg("CLIN.cpp", "OnLINRxMessage()", DEBUGLOG_TYPE_COMMENT, "LIN Error");
		// Write to Log File.
        CProtocolBase::WriteLogMsg("CLIN.cpp", "OnLINRxMessage()", DEBUGLOG_TYPE_COMMENT, "pstPassThruMsg->ulRxStatus 0x%X", pstPassThruMsg->ulRxStatus);
		// Enqueue to Circ Buffer.
		pclsLIN->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
					(sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) + pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("CLIN.cpp", "OnLINRxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsLIN->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsLIN->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Apply Filters and see if msg. is required.
    if (pclsLIN->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsLIN->IsMsgValid(pstPassThruMsg) &&
            pclsLIN->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsLIN->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#ifdef J2534_0305
    else
    {
        if (pclsLIN->IsMsgValid(pstPassThruMsg))
        {
            // Enqueue to Circ Buffer.
            pclsLIN->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#endif
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CLIN::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < LIN_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > LIN_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("LIN.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("LIN : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulDataRate = ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Version
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the version.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::Version(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue != 0x13) && (ulValue != 0x20) && (ulValue != 0x21))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CLIN::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("LIN.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("LIN : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                            pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("LIN.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                enumJ2534Error = DataRate(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("LIN.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback

                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case LIN_VERSION:       // Version

                enumJ2534Error = Version(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("LIN.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
#endif