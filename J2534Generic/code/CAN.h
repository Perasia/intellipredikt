/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: CAN.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              CAN Class.
* Note:
*
*******************************************************************************/

#ifndef _CAN_H_
#define _CAN_H_


#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define CAN_ERROR_TEXT_SIZE     120

#define CAN_MSG_SIZE_MIN        4
#define CAN_MSG_SIZE_MAX        12
#define CAN_DATA_RATE_HIGH      1000000

#ifdef UD_TRUCK
#define CAN_DATA_RATE_DEFAULT   250000
#define CAN_DATA_RATE_MEDIUM_HIGH  500000
#else
#define CAN_DATA_RATE_DEFAULT   500000
#endif
#define CAN_DATA_RATE_MEDIUM    250000
#define CAN_DATA_RATE_LOW       125000

#define CAN_HEADER_SIZE                 11
#define CAN_EXTENDED_HEADER_SIZE        29

void OnCANRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CCAN Class
class CCAN : public CProtocolBase
{
public:
    CCAN(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
    ~CCAN();

    virtual J2534ERROR  vConnect(
                                J2534_PROTOCOL enProtocolID,
                                unsigned long ulFlags,
                                unsigned long ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
                                LPVOID        pVoid=NULL);
    virtual J2534ERROR  vDisconnect();
    virtual J2534ERROR  vReadMsgs(
                                PASSTHRU_MSG  *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vWriteMsgs(
                                PASSTHRU_MSG *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vStartPeriodicMsg(
                                PASSTHRU_MSG *pstrucJ2534Msg,
                                unsigned long *pulMsgID,
                                unsigned long ulTimeInterval
		);
    virtual J2534ERROR  vStopPeriodicMsg(unsigned long ulMsgID);

    virtual J2534ERROR  vStartMsgFilter(
                                J2534_FILTER enFilterType,
                                PASSTHRU_MSG *pstrucJ2534Mask,
                                PASSTHRU_MSG *pstrucJ2534Pattern,
                                PASSTHRU_MSG *pstrucJ2534FlowControl,
                                unsigned long *pulFilterID);
    virtual J2534ERROR  vStopMsgFilter(unsigned long ulFilterID);

    virtual J2534ERROR  vIoctl(J2534IOCTLID enumIoctlID,
                                void *pInput,
                                void *pOutput);

    bool                IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
    bool                IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);


public:
    J2534ERROR              GetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              SetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              DataRate(unsigned long ulValue);
    J2534ERROR              Loopback(unsigned long ulValue);
    J2534ERROR				SetInternalTermination(unsigned char *pucValue);
	J2534ERROR		        J1962Pins(unsigned long ulValue);
	J2534ERROR		        J1939Pins(unsigned long ulValue);

    float                   m_fSamplePoint;
    float                   m_fJumpWidth;
    unsigned long           m_ulCANIDtype;
    //J2534_PROTOCOL            m_enCANProtocol;
    J2534_PROTOCOL          m_enCANProtocol;    
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;                                  
	unsigned long			m_ulJ1939PPSS;
	bool					m_bJ1939Pins;
};

//#ifdef _CAN_
//J2534_PROTOCOL m_enCANProtocol;
//#else
//extern J2534_PROTOCOL m_enCANProtocol;
//#endif

#endif
