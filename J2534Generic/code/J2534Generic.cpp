// J2534Generic.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "J2534Generic.h"
#include "DebugLog.h"
#include "DataLog.h"

#include <Windows.h>    // 050613par
#include <Mmsystem.h>   // 050613par

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//  Note!
//
//      If this DLL is dynamically linked against the MFC
//      DLLs, any functions exported from this DLL which
//      call into MFC must have the AFX_MANAGE_STATE macro
//      added at the very beginning of the function.
//
//      For example:
//
//      extern "C" BOOL PASCAL EXPORT ExportedFunction()
//      {
//          AFX_MANAGE_STATE(AfxGetStaticModuleState());
//          // normal function body here
//      }
//
//      It is very important that this macro appear in each
//      function, prior to any calls into MFC.  This means that
//      it must appear as the first statement within the 
//      function, even before any object variable declarations
//      as their constructors may generate calls into the MFC
//      DLL.
//
//      Please see MFC Technical Notes 33 and 58 for additional
//      details.
//

/////////////////////////////////////////////////////////////////////////////
// CJ2534GenericApp

BEGIN_MESSAGE_MAP(CJ2534GenericApp, CWinApp)
    //{{AFX_MSG_MAP(CJ2534GenericApp)
        // NOTE - the ClassWizard will add and remove mapping macros here.
        //    DO NOT EDIT what you see in these blocks of generated code!
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJ2534GenericApp construction

CDebugLog   gclsLog;
CDataLog    gclsDataLog;
CRITICAL_SECTION CJ2534WriteLogMsgSection;
CRITICAL_SECTION CJ2534SetLastErrorTextSection;
void SetLastErrorText(char *pszErrorText, ...);

CJ2534GenericApp::CJ2534GenericApp()
{
    // Place all significant initialization in InitInstance

    // Set Multi-media timer to 1 when dll is attached to for the first time
    // then restore it back to what it was when the dll detaches.
    // This is an attempt to fix problem where their application doesn't
    // process messages as fast as possible unless this is done. 
    timeBeginPeriod(1);     // 050613par
    m_bMultiMediaTimerSet = true;

    InitializeCriticalSection(&CJ2534WriteLogMsgSection);        // Used in WriteLogMsg()
    InitializeCriticalSection(&CJ2534SetLastErrorTextSection);   // Used in SetLastErrorText()
    CString         cszLogFile = "";
    //CString         cszDataLogFile = "";
    CJ2534Registry  *pclsJ2534Registry;
    char            chMutexName[MAX_PATH];

    m_cszDataLogFile = "";

    // Initialize the Registry read flag.
    m_bRegistryRead = false;

    // Initialize the LastError text.
    SetLastErrorText("CJ2534GenericApp: Initialized at startup");

    // Create a Registry Class.
    if ((pclsJ2534Registry = new CJ2534Registry) == NULL)
    {
        return;
    }

    // Read the appropriate Registry for the Device for which this J2534 is built.
    memset(&m_J2534Registry, 0, sizeof(m_J2534Registry));		// Be sure we're clear of any left overs
    // Set the Device Type to read the J2534 registry for DPA device.
    m_J2534Registry.enJ2534RegistryDeviceType = J2534REGISTRY_DEVICE_GENERIC;
    // Read J25434 registry for DPA device.
    if (pclsJ2534Registry->GetRegistry(&m_J2534Registry) == J2534REGISTRY_ERR_SUCCESS) 
    {
		#ifdef J2534_DEVICE_CARDONE
				cszLogFile = "\\J2534Cayman.csv";
		#else
		#ifdef J2534_DEVICE_VSI
				cszLogFile = "\\J2534VSI.csv";
		#else
		#ifdef J2534_DEVICE_KRHTWIRE
				cszLogFile = "\\J2534KRHtWire.csv";
		#else
		#ifdef J2534_DEVICE_DPA5
				cszLogFile = "\\J2534DPA.csv";
		#else
		#ifdef J2534_DEVICE_NETBRIDGE
				cszLogFile = "\\J2534netbridge.csv";
		#else
		#ifdef J2534_DEVICE_SOFTBRIDGE
				cszLogFile = "\\SoftBridge.csv";
		#else
		#ifdef J2534_DEVICE_WURTH
				cszLogFile = "\\WoWFlashBox.csv";
		#else
		#ifdef J2534_DEVICE_DELPHI
				cszLogFile = "\\DelphiEuro5Diagnostics.csv";
		#else
		#ifdef J2534_DEVICE_DBRIDGE
				cszLogFile = "\\J2534d-briDGe.csv";
		#ifdef J2534_DEVICE_SVCI
				cszLogFile = "\\J2534SVCI.csv";
		#endif
		#else
		#ifdef J2534_DEVICE_DBRIDGE0305
				cszLogFile = "\\J2534d-briDGe0305.csv";
			#ifdef UD_TRUCK
				cszLogFile = "\\J2534d-briDGeUDT0305.csv";
			#endif
		#else
		#ifdef J2534_DEVICE_DPA50305
				cszLogFile = "\\J2534dpa50305.csv";
			#ifdef UD_TRUCK
				cszLogFile = "\\J2534dpa5UDT0305.csv";
			#endif
		#else
				#error J2534Generic.cpp Define manufacture log file name                    
		#endif //dpa50305
		#endif //dbridge0305
		#endif //dbridge
		#endif //delphi
		#endif //wurth
		#endif //softbridge
		#endif //netbridge
		#endif //dpa5
		#endif //krhw
		#endif //vsi
		#endif //cardone

        m_bRegistryRead = true;
    }
    
	// Delete the Registry Object as it is not needed anymore.
    if (pclsJ2534Registry != NULL)
    {
        delete pclsJ2534Registry;
        pclsJ2534Registry = NULL;

    }

    // If Registry is read successfully.
    if (m_bRegistryRead) 
    {
#ifdef UD_TRUCK
        //m_J2534Registry.dwLogging = 0x80000007;
        //m_J2534Registry.dwLoggingSizeMaxKB = 5120;
#endif
        cszLogFile = m_J2534Registry.chLoggingDirectory + cszLogFile;
        // Open the Log file if Logging is set to ON in the Registry.
        if ((cszLogFile != "") && (m_J2534Registry.dwLogging != 0))
        {
            gclsLog.Open(cszLogFile, m_J2534Registry.dwLogging, m_J2534Registry.chFunctionLibrary, m_J2534Registry.dwLoggingSizeMaxKB);
        }
    }
#ifdef UD_TRUCK
    else
    {
        cszLogFile = "C:\\Dearborn Group Products\\d-briDGe0305 J2534\\J2534d-briDGe0305.csv";
        m_J2534Registry.dwLogging = 0x00;
        strcpy_s(m_J2534Registry.chFunctionLibrary, "C:\\Windows\\system32\\dgpytb32.dll");
        m_J2534Registry.dwLoggingSizeMaxKB = 5120;
        gclsLog.Open(cszLogFile, m_J2534Registry.dwLogging, m_J2534Registry.chFunctionLibrary, m_J2534Registry.dwLoggingSizeMaxKB);
        gclsLog.Close();
    }
#endif

    sprintf_s(chMutexName, sizeof(chMutexName)-1, "DataLoggingCloseEvent%s", J2534REGISTRY_TOOL_NAME);
    m_hDataLoggingEvent[0] = CreateEvent(NULL, FALSE, FALSE, chMutexName);
    sprintf_s(chMutexName, sizeof(chMutexName)-1, "DataLoggingStopEvent%s", J2534REGISTRY_TOOL_NAME);
    m_hDataLoggingEvent[1] = CreateEvent(NULL, FALSE, FALSE, chMutexName);
    sprintf_s(chMutexName, sizeof(chMutexName)-1, "DataLoggingPauseEvent%s", J2534REGISTRY_TOOL_NAME);
    m_hDataLoggingEvent[2] = CreateEvent(NULL, FALSE, FALSE, chMutexName);
    sprintf_s(chMutexName, sizeof(chMutexName)-1, "DataLoggingStartEvent%s", J2534REGISTRY_TOOL_NAME);
    m_hDataLoggingEvent[3] = CreateEvent(NULL, FALSE, FALSE, chMutexName);
}

void CJ2534GenericAppPassThruClose();
CJ2534GenericApp::~CJ2534GenericApp()
{
    int i;

    // Restore Multi-media timer back - 050613par
    if (m_bMultiMediaTimerSet)
    {
        timeEndPeriod(1);
        m_bMultiMediaTimerSet = false;
    }
    CJ2534GenericAppPassThruClose(); // Let's clean up in case user didn't - par100319
    // If the Log File is open then Close it.
    if (gclsLog.m_pfdLogFile != NULL)
    {
        // Close the Log file.
        gclsLog.Close();
    }
    // If the Data Log File is open then Close it.
    if (gclsDataLog.m_pfdLogFile != NULL)
    {
        // Close the Log file.
        gclsDataLog.Close();
    }
    ResetEvent(m_hDataLoggingEvent[DATALOGGING_CLOSE]);
    for (i = 0; i < DATALOGGING_NUM_EVENTS; ++i)
        CloseHandle(m_hDataLoggingEvent[i]);
}

J2534REGISTRY_ERROR_TYPE CJ2534GenericApp::WriteToRegistry(J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig)
{

    CJ2534Registry  *pclsJ2534Registry;

    // Create a Registry Class.
    if (pstJ2534RegistryConfig == NULL)
    {
        return(J2534REGISTRY_ERR_NULL_PARAMETER);
    }
    if ((pclsJ2534Registry = new CJ2534Registry) == NULL)
    {
        return (J2534REGISTRY_ERR_INSUFFICIENT_WRITING_PRIVILEGE);
    }
    J2534REGISTRY_ERROR_TYPE ReturnStatus = J2534REGISTRY_ERR_SUCCESS; // default return status
    if (pclsJ2534Registry->WriteToRegistry(&m_J2534Registry) == J2534REGISTRY_ERR_SUCCESS) 
    {
        ReturnStatus = J2534REGISTRY_ERR_INSUFFICIENT_WRITING_PRIVILEGE;
    }
    // Delete the Registry Object as it is not needed anymore.
    delete pclsJ2534Registry;
    return (ReturnStatus);
}


//-----------------------------------------------------------------------------
//  Function Name   : fnWriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void fnWriteLogMsg(char * chFileName, char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
#define DEVICECAYMAN_ERROR_TEXT_SIZE		120
    if (gclsLog.m_pfdLogFile != NULL)
    {
        EnterCriticalSection(&CJ2534WriteLogMsgSection); // Make processing parameters thread-safe
        char buf[1024];
        va_list ap;
        va_start(ap, chMsg);
        vsprintf_s(buf, sizeof(buf)-1, chMsg, ap);
        va_end(ap);
        buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
        gclsLog.Write(chFileName, chFunctionName, MsgType, buf);
        LeaveCriticalSection(&CJ2534WriteLogMsgSection);
    }
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CJ2534GenericApp object

CJ2534GenericApp theApp;
