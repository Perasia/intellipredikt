/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1850PWM.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              J1850PWM Class.
* Note:
*
*******************************************************************************/

#ifndef _J1850PWM_H_
#define _J1850PWM_H_

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define J1850PWM_ERROR_TEXT_SIZE		120
#define J1850PWM_DATA_RATE_DEFAULT_1	41600
#define J1850PWM_DATA_RATE_DEFAULT	41666
#define J1850PWM_MAXDATA_RATE_1	83300
#define J1850PWM_MAXDATA_RATE	83333

#define J1850PWM_DATALINK_PARAM1	0xF1
#define J1850PWM_MAX_DATALENGTH		10

#define J1850PWM_83_3K				1
#define J1850PWM_41_6K				0

#define J1850PWM_MSG_SIZE_MIN		3
#define J1850PWM_MSG_SIZE_MAX		10
#define J1850PWM_MAILBOX_LIMIT		32

typedef struct
{
	unsigned char ucFuncID;
	bool		bValid;
}
J1850PWM_LOOKUP_TABLE;

void OnJ1850PWMRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CCAN Class
class CJ1850PWM : public CProtocolBase
{
public:
	CJ1850PWM(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CJ1850PWM();
		
	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
#ifdef J2534_0500
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulNumMsgs,
								unsigned long ulTimeout,
								unsigned long ulChannelId);
#endif
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);
	

	J2534ERROR				MessageValid(PASSTHRU_MSG	   *pstrucJ2534Msg,
										unsigned long  *pulNumMsgs);
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR				NodeAddress(unsigned long ulValue);
	J2534ERROR				NetworkLine(unsigned long ulValue);
	J2534ERROR				ClearTable();
	J2534ERROR				AddToTable(SBYTE_ARRAY *pInput);
	J2534ERROR				DeleteFromTable(SBYTE_ARRAY *pInput);
	J2534ERROR  vSetProgrammingVoltage(unsigned long ulPin,
										unsigned long ulVoltage);
	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);
	bool				IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);
private:
	PASSTHRU_MSG        structj1850;
	unsigned long m_ucIOdata;
	J2534_PROTOCOL		m_enJ1850Protocol;
	unsigned char			m_ucNodeAddress;
	unsigned long			m_ulNetworkLine;
	J1850PWM_LOOKUP_TABLE	m_FunctionTable[J1850PWM_MAILBOX_LIMIT];
	int m_iSetNODE_ADDRESScount;
};

#endif
