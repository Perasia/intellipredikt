// LicensingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "J2534Generic.h"
#include "LicensingDlg.h"
#include "Shlwapi.h"

// CLicensingDlg dialog

IMPLEMENT_DYNAMIC(CLicensingDlg, CDialog)

CLicensingDlg::CLicensingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicensingDlg::IDD, pParent)
{
    m_ShowDialog = true;
}

CLicensingDlg::~CLicensingDlg()
{
    delete m_License;
}

void CLicensingDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT_LICENSEID, txtLicenseID);
    DDX_Control(pDX, IDC_EDIT_PASSWORD, txtPassword);
    DDX_Control(pDX, IDOK, btnActivate);
    DDX_Control(pDX, IDC_STATIC_INSTRUCTIONS, lblInstructions);
}


BEGIN_MESSAGE_MAP(CLicensingDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CLicensingDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CLicensingDlg::OnBnClickedCancel)
    ON_WM_PAINT()
//    ON_WM_CREATE()
ON_WM_CREATE()
END_MESSAGE_MAP()


// CLicensingDlg message handlers



void CLicensingDlg::TerminateApplicationCallback()
{
   return;/*
    CLicensingDlg *dlg = this;

    if (dlg)
    {
        dlg->TerminateApplication();
    }   */  
}

void CLicensingDlg::TerminateApplication()
{
    //MessageBox(m_License->GetErrorMessage().c_str(), L"Error", MB_OK | MB_ICONERROR);
    OnCancel();
}

void CLicensingDlg::RefreshLicenseStatusCallback()
{    
    /*CLicensingDlg *dlg = ((CLicensingDlg *)::theApp.GetMainWnd());

    if (dlg)
    {
        dlg->RefreshLicenseStatus();
    }*/
}
/*
void CLicensingDlg::RefreshLicenseStatus()
{
    bool licenseValid = theApp.m_License->Validate();

    if (theApp.m_License->IsEvaluation())
    {
//        btnRefreshLicense.EnableWindow(false);
//        btnDeactivateLicense.EnableWindow(false);

        CString statusText = L"";
        if (licenseValid)
        {
            statusText.AppendFormat(L"Evaluation expires in %d days.", theApp.m_License->GetDaysRemaining());  
        }
        else
        {
            statusText.AppendFormat(L"Evaluation expired.");
           MessageBox(statusText, L"Warning", MB_OK | MB_ICONINFORMATION); 
        }
//        lblStatusText.SetWindowText(statusText);

        return;
    }

//    btnRefreshLicense.EnableWindow(true);
//    btnDeactivateLicense.EnableWindow(true);

    CString statusText = L"";

    if (licenseValid)
    {
        if (theApp.m_License->GetType() == LicenseType::TimeLimited)
        {
            statusText.AppendFormat(L"License expires in %d days.", theApp.m_License->GetDaysRemaining());
        }
        else
        {
            statusText.Append(L"Fully licensed.");
        } 
    }
    else
    {
        statusText.Append(L"The license is invalid or expired. ");
        MessageBox(statusText, L"Warning", MB_OK | MB_ICONINFORMATION);
    }

    CString firstName = theApp.m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/FirstName").c_str();
    CString lastName = theApp.m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/LastName").c_str();
    CString companyName = theApp.m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/CompanyName").c_str();
    CString licenseId = theApp.m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/LicenseID").c_str();
    CString registerInfo = L"";

    if (!firstName.IsEmpty()
        && firstName != L"UNREGISTERED"
        && !lastName.IsEmpty()
        && lastName != L"UNREGISTERED")
    {
        registerInfo.Append(L"Registered To: ");
        registerInfo.Append(firstName);
        registerInfo.Append(L" ");
        registerInfo.Append(lastName);
    }

    if (!companyName.IsEmpty() && companyName != L"UNREGISTERED")
    {
        if (registerInfo.IsEmpty())
        {
            registerInfo = L"Registered To: ";
        }
        else
        {
            registerInfo.Append(L" ");
        }

        registerInfo.Append(L"[");
        registerInfo.Append(companyName);
        registerInfo.Append(L"]");
    }

    if (!registerInfo.IsEmpty())
    {
        registerInfo.Append(L"\n");
    }

    registerInfo.Append(L"License ID: ");
    registerInfo.Append(licenseId);

    statusText.Append(L"\n");
    statusText.Append(registerInfo);

//    lblStatusText.SetWindowText(statusText);
}         */

void CLicensingDlg::OnBnClickedCancel()
{
    // TODO: Add your control notification handler code here
    //delete m_License;
    EndDialog(5);
    return;
    OnCancel();
}

void CLicensingDlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    CString licenseID;
    CString password;
    CString installationName;

    txtLicenseID.GetWindowText(licenseID);
    txtPassword.GetWindowText(password);
    //txtInstallationName.GetWindowText(installationName);
    installationName = "";

    licenseID.Trim();
    password.Trim();
    installationName.Trim();

    if (licenseID.IsEmpty())
    {
        MessageBox("Please enter a License ID.", "Online Activation", MB_OK | MB_ICONEXCLAMATION);
        txtLicenseID.SetFocus();
        return;
    }

    if (password.IsEmpty())
    {
        MessageBox("Please enter a Password.", "Online Activation", MB_OK | MB_ICONEXCLAMATION);
        txtPassword.SetFocus();
        return;
    }

    btnActivate.EnableWindow(false);

	CWaitCursor cur;

	//Check if we have a proxy
/*	if (Proxy::HaveProxy())
	{
		//Check if the credentials have already been set
		if (!Proxy::HaveCredentials())
		{
			//show the dilaog to get the proxy credentials
			CProxyCredentialsDlg proxyDlg(this);
			if (proxyDlg.DoModal() != IDOK)
			{
				cur.Restore();
				btnActivate.EnableWindow();
				return;
			}
		}

		//set the credentials in the license context for use when communicating with the webservices
		m_License->SetProxyCredentials(Proxy::GetProxyURL(SK_CONST_WEBSERVICE_ACTIVATEINSTALLATION_URL), 
			Proxy::GetProxyUserName(), 
			Proxy::GetProxyPassword());
	}
*/
    

    if (m_License->ActivateOnline(_ttoi(licenseID), password.GetString(), installationName.GetString()))
    {
        m_License->Reload();
        cur.Restore();
        MessageBox("Activation successful.", "Online Activation", MB_OK);
        licenseID.Empty();
        password.Empty();
        installationName.Empty();
        //delete m_License;
        EndDialog(0);
        return;
        OnOK();
        return;
    }
    else
    {
        cur.Restore();

		string errorMessage = m_License->GetErrorMessage();

        //Reset the proxy credentials
//        Proxy::SetProxyUserName(L"");
//        Proxy::SetProxyPassword(L"");

		//if the status code from the webservice call is 407 then the proxy authentication failed
		//and the credentials should be cleared so they can be re-entered
/*		if (theApp.m_License->GetStatusCode() == 407)
		{
			errorMessage = L"StatusCode 407: The proxy credentials were rejected.  Please retry with the correct credentials.";
		}*/

        errorMessage = "Online activation failed. Please make sure of the following and try again.\r\n1) The valid License ID and Password is entered.\r\n2) There is internet access.";
		MessageBox(errorMessage.c_str(), "Online Activation", MB_OK | MB_ICONERROR);

        //Invalid LicenseID/Password?
        if(5008 == m_License->GetExtendedErrorNo())
        {
            txtLicenseID.SetSel(0, -1);
            txtLicenseID.SetFocus();
        }

        btnActivate.EnableWindow();
    }
}

void CLicensingDlg::InitializeLicense()
{
    m_License = new License(212488, "1.0.0.0", "a9pbc8DqS6B8Kp8g5wkjF08s9h272bt8CsliasU8zavyvFsnFwCNEXZV7Qs0+KXOLfffEUTD6RDgLX6Xbkdx3YN2tkKrJcSFFzbU456iFcXKTub9tx1jBBuWhFNTXCB6QIg1VVMXmxKXPRkI1UElv1p2Am9xN6KNFccm0K67Q734u3D0xiNiroG8O5K1p6dW+3l74xPOXB8rDwRR9uMda4gB41a/KIZeOioXZhXf6nXYAh0vtXKz2IcucJNg2w4fZ67l3GjuKLxIzKcaJ76SoFBavRkUqPDq+zH1xC83S0ZEUpXfuKMwfgAF1bOhd3pDziaHU4obpF3QTS/PcAaApN2sgTiJCsyPkgFxahY/fZoDZWvozIvB120Lxo5VNjJ5SIatDqMVCe/K8PCXL6GqIv9RIdcx3x+QeMMK50d69MXLlAVytMJXngk2kJp9dJ5gjzu+qeFZzpQA93kVzepu5xTMjxZhy4oftSWj+MqRtRQr0E9EavyTZUQp/F+uNxPpTcL3skZAgwCLMESG40P25kYtAikenpVAasAoalbCb5LnkBQ5PXViiAax1qFff4inLN90kDG52do2evxvJu2Bxfpv0yBpi4DxTmBg3e3YgNs+Ck3tC5A/XxhuDMFXnSfb7WL29jKokRgqytzpacWSEM9TG8+Y/uJyLHjOYGSRMDi36Hs156u2uCxWqZoxD2wyQE9IZ5oI7kOOsRlvA2EmmePFcc7qNAxU659sOT/uWv0csQz9kWi++E7xBVfn50+ITlxBd2e4p95HLZn2/MMudwiP8YcVkM2ratPeh36QcWVFonQCRCcRci81GvCeJjdmqdE0z+gpU9V1wWn+M6yowCdYAJFG+5Q/aEDQTB7ISpo1YsHf2I4Kpp5K0Ph1jkdRfvP2b5RcuuiOYR7fHhb2T/ch3Tivnyc5O0eRN1Z/F+VO6wFiLtaWnMjDXfZP4WHFSGPmkFg7oxr3v0iU3ztvccE1fXCQ/szI41whhiRf00+WzUan2rJMSyx7TqS/8+MUjGFcYciYayZ1Xoxtn3XRIOjCJRUakNVyyNF1Vs9L77DqtWCdRrAEYsKKVZrJCk3r9wuWHgaaNVNB+13ixcXO11xXd53VZNzoosDjuK1XxRAZ8QWnwVhTkgZVfN4dICm/w8BitER8ghHjctNP/tu/ZUnDsi9oTKJ6pIIy877DzSI4p+DCkNpUcAUofKoZkkLi+jU+lOlw8sPNEBBanbp/2geDuWhuBplgjwmVyC4/cEbIrUWSSUUslp4x27EXw2J78OCvyvN3dVRK5yui7B9MxxvXnFBsyc9UdzBoPeGMFoqu8uvklkgMCM4ydshOfyoUlHk3ezmpxZs0SPQeOppsPmjlE5H5vq/0xRcxy5Nhq0AOIUrNqDk95Wu6Jjn1bSdgskmP3jRnXHEYtPxXevcYQomP0GR3O0T5kfHQUhD07Yy9Np/LJEgnagJ9CLxWHqEoF5VPTL2+Y1W/yQn5pek92dXU8a1hLbUTl0x1lyWWOkSAcW6GfI4NmW07DlWQurV8035lX6FSmd4kBZm/2w+kv9eR4Bd4G11se4fFa56TFPjkSPHDQqEL+Y26IoxJe2Dpe6IqCkt6gL+Br+KGDdWq1AXW4tlLmpBrQO2C2ycdIRkQMCead3ZpYadgiu2Su+pLAjR3m7Fj8NARHEwcnS1p885sVnyIFI/xKpHdVnSGRos3k+1y+vjVYs8U/S0fxUZZCLaV8Aw/mrQhzx5iyX6cJQ/d2x7Roa0zYSfKwh38X3aNIEM4dQrI4HpycpcNkPWLfZEEekkityXywgkms8JcCUVjUqz+YF1gi4w0RCwSZzUDUf/0w2FNyuvQetizXemT/gzOLIP4/rYdrsFWyMdl1hmE5q+NE/gd1FCs9N2HupNwndJqFQ4B0SBM0OTU1e2sxBP0ztk/FRpjzoyuZpcA8pmlK6WhlvkkG2u8xbmLa2+NhIuwO3GDHW/zbj1GbWw/Ez/RfH1WHI4Izv01vZKcJbR+ftZSoaAtCRfoHps/ar491dvn3EDkctktuKrwVcp8pYzY2Pxq44NR4Hh0LYE/5x7CFHKD7LLvT31F75iTExhfMkVrugfl4Mi96h0c66G7z4Li0XmVFCUwfYpY0FuA28px945cDT6zXAYHww392OttqZE4OkCWcE5hocH+wjTNyGvMF/MJ/5a/DRgOmYZX+p0RGsfQByOVp6qIfavHqIkVuNW5LMLTUHjBbAW7iv6XBSxOTgXMVB3An7lYZY7Wzyiyi9Ak3YXGMggvBSVsTUMindcf2NiruUJvj2h2gnhxkvXNyUj8tu0joG/1iFAf1LnqZ7NX8HBGRqgywIo1+ps+sNIuRAozKV7srWM1w1gJlq7gy7uvkvUKv6F/Brku3c1UXXu6glGHdrB2DcDSePhOuU267Nt2hVHOBPrk9HkcX9zB02W8ynHPrPXmriC1sYVTQ0N7WiKt/CW41QmhK41tsMk5jCHtaRvvI5Db8hM5t33r3dcJAG1TZyaf8mpDEk0Esd8LTUcQKxxVYRyucDOysHSQ/HKxmmAdKC3Is0JaDDehMrgmyP8AHK4r5t+aH+HIXHo/aw4w11nTqXeVnC0ryV4QU4kiC/3hHLITeZ5bmr7ebU+3M+H+lbXDMYg4OdwfTizYnX2ZtD66T4mV0tAPYyURu4EIDsJV42RyYcNLvarJmEwaonrGcHDGbC62GxlVedRCduAzSYbTgtOY/E++Ab1vfUYu519pyGk0VqMiR6g5YgdJ0flq1AjehV7q/Ar20K7nFVkqliAieNlxYdJI8uCOFcUwIl8VgOn83ENkIlZRQWzL+hNjsUrZFJHbSHL8JPdIl5JeDdNqkHWB0yhVVem8YCUciCa6WdXKJHTSsbj2Qs7WotHOzTF88fyfcQUgezVdNq8vmf5DK8yHwcXCVcSWdqbWNzZPH9tTkGiADg5pv9CMw6KFwNurJ7X/QOAFyN/lEPVK6BUtN0mNksKxXk5bL1HR25ysQQ8kJnVXHkv3o0wPupv0Szz330Qw4A==", "d+jLXiBpTMAqrunDrQb8kVGl+lCsZVg0zclbrQbXIXdrPU5EsaAKREBI0BMqHHvz");

    if (SK_ERROR_NONE != m_License->GetLastErrorNo())
    {
        if(SK_ERROR_PLUS_EVALUATION_WARNING == m_License->GetLastErrorNo())
        {
            CString message = "";
            message.Format("Warning : ");
            message.Append(CString(m_License->GetLastErrorString().c_str()));
            
            MessageBox(message, "Warning", MB_OK | MB_ICONASTERISK);
            m_License->InitializeSystemIdentifiers();
            message.Empty();
        }
        else
        {
            //TerminateApplication();
            MessageBox(m_License->GetErrorMessage().c_str(), "Error", MB_OK | MB_ICONERROR);
            //delete m_License;
            EndDialog(2);
            m_ShowDialog = false;
            return;
        }
    }

    m_License->SetRefreshStatusCallback(&CLicensingDlg::RefreshLicenseStatusCallback);
    m_License->SetTerminateApplicationCallback(&CLicensingDlg::TerminateApplicationCallback);

    m_License->SetSessionCodePath(":CU:Software\\Concept Software\\Protection PLUS\\Samples:SessionCode");

    CString licenseFilePath = "C:\\SVCI\\";
    CString licenseAlias1Path = "C:\\SVCI\\";
    CString licenseAlias2Path = "C:\\SVCI\\";
    licenseFilePath.Append(m_cszEncryptedDeviceSN);
    licenseFilePath.Append("LicenseFile.lfx");
    licenseAlias1Path.Append(m_cszEncryptedDeviceSN);
    licenseAlias1Path.Append("-005");
    licenseAlias2Path.Append(m_cszEncryptedDeviceSN);
    licenseAlias2Path.Append("-006");

    m_License->SetLicenseFilePath(string(licenseFilePath));
    m_License->AddAlias(string(licenseAlias1Path));
    m_License->AddAlias(string(licenseAlias2Path));
    //m_License->AddAlias(L":CU:Software\\Concept Software\\Protection PLUS\\Samples:LicenseAlias3");
    //m_License->AddAlias(L":CU:Software\\Concept Software\\Protection PLUS\\Samples:LicenseAlias4");

    m_License->m_licenseAlias1Path = licenseAlias1Path;
    m_License->m_licenseAlias2Path = licenseAlias2Path;

    licenseAlias1Path.Empty();
    licenseAlias2Path.Empty();

    m_License->Reload();

    if (PathFileExists(licenseFilePath))
    {
        licenseFilePath.Empty();
        RefreshLicenseStatus();
    }
    licenseFilePath.Empty();
    if (m_ShowDialog == true)
    {
        ShowWindow(SW_SHOW);
    }
}

void CLicensingDlg::RefreshLicenseStatus()
{
    bool licenseValid = m_License->Validate();
    CString statusText = "";

    if (m_License->IsEvaluation())
    {
//        btnRefreshLicense.EnableWindow(false);
//        btnDeactivateLicense.EnableWindow(false);

        if (licenseValid)
        {
            statusText.AppendFormat("Evaluation expires in %d days.", m_License->GetDaysRemaining()); 
            statusText.Empty();
        }
        else
        {
            statusText.AppendFormat("The license is invalid or expired.");
            MessageBox(statusText, "Warning", MB_OK | MB_ICONINFORMATION);  
            statusText.Empty();      
            //delete m_License;
            EndDialog(3);
            m_ShowDialog = false;
            return;
        }
//        lblStatusText.SetWindowText(statusText);

        //return;
    }
    else
    {
//    btnRefreshLicense.EnableWindow(true);
//    btnDeactivateLicense.EnableWindow(true);

        CString statusText = "";

        if (licenseValid)
        {
            if (m_License->GetType() == LicenseType::TimeLimited)
            {
                statusText.AppendFormat("License expires in %d days.", m_License->GetDaysRemaining());
            }
            else
            {
                statusText.Append("Fully licensed.");
            }
            statusText.Empty();
        }
        else
        {
            statusText.Append("The license is invalid or expired.");
            MessageBox(statusText, "Warning", MB_OK | MB_ICONINFORMATION);
            statusText.Empty();
            //delete m_License;
            EndDialog(4);
            m_ShowDialog = false;
            return;
        }
    }

    string licensedSerial = m_License->GetStringValue("/SoftwareKey/PrivateData/License/UserDefinedString1");

    if (licensedSerial.length() > 16)
    {
        statusText.Append("License verification failed.");
        MessageBox(statusText, "Warning", MB_OK | MB_ICONINFORMATION);
        statusText.Empty(); 
        licensedSerial.clear();
        //delete m_License;
        EndDialog(10);
        m_ShowDialog = false;
        return;
    }

    if (m_cszEncryptedDeviceSN.GetLength() > 16)
    {
        statusText.Append("License verification failed.");
        MessageBox(statusText, "Warning", MB_OK | MB_ICONINFORMATION);
        statusText.Empty();  
        licensedSerial.clear();
        //delete m_License;
        EndDialog(11);
        m_ShowDialog = false;
        return;
    }

    while (licensedSerial.length() < 16)
    {
        // Make sure to make it 8 byte hex value
        licensedSerial = "0" + licensedSerial;
    }

    while (m_cszEncryptedDeviceSN.GetLength() < 16)
    {
        // Make sure to make it 8 byte dec value
        m_cszEncryptedDeviceSN = "0" + m_cszEncryptedDeviceSN;
    }

    // Compare actual device serial number with serial number of the license file
    unsigned __int64 ui64DeviceSN = 0;
    unsigned __int64 ui64FileSN = 0;
    unsigned __int64 ui64Temp;
    char chTemp[255];

    char   chFileSN[MAX_PATH]; 
    sprintf_s(chFileSN,255,"%s",m_cszEncryptedDeviceSN.GetString());

    strncpy_s(chTemp, 255, chFileSN, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 100000000000000;
    strncpy_s(chTemp, 255, chFileSN+2, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 1000000000000;
    strncpy_s(chTemp, 255, chFileSN+4, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 10000000000;
    strncpy_s(chTemp, 255, chFileSN+6, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 100000000;
    strncpy_s(chTemp, 255, chFileSN+8, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 1000000;
    strncpy_s(chTemp, 255, chFileSN+10, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 10000;
    strncpy_s(chTemp, 255, chFileSN+12, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 100;
    strncpy_s(chTemp, 255, chFileSN+14, 2);
    ui64Temp = strtol(chTemp,  NULL, 10);
    ui64FileSN += ui64Temp * 1;

    char   chDeviceSN[MAX_PATH]; 
    sprintf_s(chDeviceSN,255,"%s",licensedSerial.c_str());

    strncpy_s(chTemp, 255, chDeviceSN, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 56;
    strncpy_s(chTemp, 255, chDeviceSN+2, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 48;
    strncpy_s(chTemp, 255, chDeviceSN+4, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 40;
    strncpy_s(chTemp, 255, chDeviceSN+6, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 32;
    strncpy_s(chTemp, 255, chDeviceSN+8, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 24;
    strncpy_s(chTemp, 255, chDeviceSN+10, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 16;
    strncpy_s(chTemp, 255, chDeviceSN+12, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 8;
    strncpy_s(chTemp, 255, chDeviceSN+14, 2);
    ui64Temp = strtol(chTemp,  NULL, 16);
    ui64DeviceSN += ui64Temp << 0;

    if (ui64DeviceSN != ui64FileSN)
    {
        statusText.Append("License verification failed.");
        MessageBox(statusText, "Warning", MB_OK | MB_ICONINFORMATION);
        statusText.Empty();
        licensedSerial.clear();
        //delete m_License;
        EndDialog(12);
        m_ShowDialog = false;
        return;
    }

    statusText.Empty();   
    licensedSerial.clear();
    //delete m_License;
    EndDialog(0);
    m_ShowDialog = false;
    return;
    /*
    CString firstName = m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/FirstName").c_str();
    CString lastName = m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/LastName").c_str();
    CString companyName = m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/Customer/CompanyName").c_str();
    CString licenseId = m_License->GetStringValue(L"/SoftwareKey/PrivateData/License/LicenseID").c_str();
    CString registerInfo = L"";

    if (!firstName.IsEmpty()
        && firstName != L"UNREGISTERED"
        && !lastName.IsEmpty()
        && lastName != L"UNREGISTERED")
    {
        registerInfo.Append(L"Registered To: ");
        registerInfo.Append(firstName);
        registerInfo.Append(L" ");
        registerInfo.Append(lastName);
    }

    if (!companyName.IsEmpty() && companyName != L"UNREGISTERED")
    {
        if (registerInfo.IsEmpty())
        {
            registerInfo = L"Registered To: ";
        }
        else
        {
            registerInfo.Append(L" ");
        }

        registerInfo.Append(L"[");
        registerInfo.Append(companyName);
        registerInfo.Append(L"]");
    }

    if (!registerInfo.IsEmpty())
    {
        registerInfo.Append(L"\n");
    }

    registerInfo.Append(L"License ID: ");
    registerInfo.Append(licenseId);

    statusText.Append(L"\n");
    statusText.Append(registerInfo);*/

//    lblStatusText.SetWindowText(statusText);
}

void CLicensingDlg::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    // TODO: Add your message handler code here
    // Do not call CDialog::OnPaint() for painting messages

    lblInstructions.SetWindowText("Please use the License ID and Password given to you to activate.  An Internet connection is required.  If you encounter an error, please double-check that the License ID and Password are correct, and temporarily disable any personal firewalls you may have installed.");

    // set textbox max lengths
    txtLicenseID.LimitText(10);
    txtPassword.LimitText(15);

    CString licenseID = m_License->GetStringValue("/SoftwareKey/PrivateData/License/LicenseID").c_str();

    if (!licenseID.IsEmpty() && licenseID != "0")
    {
        txtLicenseID.SetWindowText(licenseID);
        txtPassword.SetFocus();
    }
    else
    {
        txtLicenseID.SetWindowText("");
        txtLicenseID.SetFocus();
    }
}

int CLicensingDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDialog::OnCreate(lpCreateStruct) == -1)
        return -1;

    // TODO:  Add your specialized creation code here
    InitializeLicense();

    return 0;
}
