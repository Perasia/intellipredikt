/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: ISO9141.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support ISO9141 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "ISO9141.h"

//-----------------------------------------------------------------------------
//  Function Name   : CISO9141
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CISO9141 class
//-----------------------------------------------------------------------------
CISO9141::CISO9141(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) :
            CProtocolBase(pclsDevice, pclsDebugLog)
{
    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "CISO9141()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;
    m_enProtocol = ISO9141;
    m_bJ1962Pins = false;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "CISO9141()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CISO9141
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CISO9141 class
//-----------------------------------------------------------------------------
CISO9141::~CISO9141()
{
    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "~CISO9141()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "~CISO9141()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vConnect(J2534_PROTOCOL    enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    /*Get the Check Sum Flag from the connection flags*/
    m_ulChecksumFlag = ((ulFlags >> 9) & 0x01);
//  ulFlags = m_ulChecksumFlag;
    ulFlags = (((ulFlags >> 12) & 0x01) << 1);
    ulFlags |= m_ulChecksumFlag;
    switch(enProtocolID)
    {
    case ISO9141:
#ifdef J2534_DEVICE_DBRIDGE
    case ISO9141_PS:
#endif
        {
#ifdef J2534_0305
            ulBaudRate = ISO9141_DATA_RATE_DEFAULT;
#else
        if (IsUartBaudRateValid(ulBaudRate) == J2534_ERR_INVALID_BAUDRATE)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }

#endif
            m_ulDataRate = ulBaudRate;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnISO9141RxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    m_usP1MIN = ISO9141_P1MIN_DEFAULT;
    m_usP1MAX = ISO9141_P1MAX_DEFAULT;
    m_usP2MIN = ISO9141_P2MIN_DEFAULT;
    m_usP2MAX = ISO9141_P2MAX_DEFAULT;
    m_usP3MIN = ISO9141_P3MIN_DEFAULT;
    m_usP3MAX = ISO9141_P3MAX_DEFAULT;
    m_usP4MIN = ISO9141_P4MIN_DEFAULT;
    m_usP4MAX = ISO9141_P4MAX_DEFAULT;
    m_usW1MIN = ISO9141_W1MIN_DEFAULT;
    m_usW1MAX = ISO9141_W1MAX_DEFAULT;
    m_usW2MIN = ISO9141_W2MIN_DEFAULT;
    m_usW2MAX = ISO9141_W2MAX_DEFAULT;
    m_usW3MIN = ISO9141_W3MIN_DEFAULT;
    m_usW3MAX = ISO9141_W3MAX_DEFAULT;
    m_usW4MIN = ISO9141_W4MIN_DEFAULT;
    m_usW4MAX = ISO9141_W4MAX_DEFAULT;
    m_usW5MIN = ISO9141_W5MIN_DEFAULT;
    m_usW5MAX = ISO9141_W5MAX_DEFAULT;
    m_usTidle = ISO9141_TIDLE_DEFAULT;
    m_usTinil = ISO9141_TINIL_DEFAULT;
    m_usTwup = ISO9141_TWUP_DEFAULT;
    m_usParity = ISO9141_NO_PARITY;
    m_usDataBits = ISO9141_8_DATABITS;
    m_usFiveBaudMod = ISO9141_5_BAUD_REG;

    m_enProtocol = enProtocolID;
    m_bJ1962Pins = false;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vReadMsgs(PASSTHRU_MSG     *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
    if (m_enProtocol == ISO9141_PS)
    {
        if (!m_bJ1962Pins)
        {
            return(J2534_ERR_PIN_INVALID);
        }
    }
#endif

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}
#ifdef J2534_0500
//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
	unsigned long   *pulNumMsgs,
	unsigned long   ulTimeout,
	unsigned long ulChannelId)
{
	//*****************************IMPORTANT NOTE******************************* 
	// Perform all the protocol specific stuff in this function.
	//**************************************************************************

	J2534ERROR  enJ2534Error;
	unsigned long   ulIdx1;

	// Write to Log File.
	WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
	if (m_enProtocol == ISO9141_PS)
	{
		if (!m_bJ1962Pins)
		{
			return(J2534_ERR_PIN_INVALID);
		}
	}
#endif

	for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
	{
		// Check Msg. Protocol ID.
		if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enProtocol)
		{
			// Write to Log File.
			WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}

		// Check if msg. format is valid.
		if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
		{
			// Write to Log File.
			WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}

	// Write using the generic Write.
	if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
		pulNumMsgs,
		ulTimeout
#ifdef J2534_0500
		, 0
#endif
	))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

#endif

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
    if (m_enProtocol == ISO9141_PS)
    {
        if (!m_bJ1962Pins)
        {
            return(J2534_ERR_PIN_INVALID);
        }
    }
#endif

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enProtocol)
        {
            // Write to Log File.
            WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    
    
    
    
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vStartPeriodicMsg(PASSTHRU_MSG     *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");
#ifdef J2534_DEVICE_DBRIDGE
    if (m_enProtocol == ISO9141_PS)
    {
        if (!m_bJ1962Pins)
        {
            return(J2534_ERR_PIN_INVALID);
        }
    }
#endif

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enProtocol)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::vStartMsgFilter(J2534_FILTER  enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

#ifdef J2534_DEVICE_DBRIDGE
    if (m_enProtocol == ISO9141_PS)
    {
        if (!m_bJ1962Pins)
        {
            return(J2534_ERR_PIN_INVALID);
        }
    }
#endif

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != pstPattern->ulProtocolID) ||
        (pstPattern->ulProtocolID != m_enProtocol))
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetLastErrorText("ISO9141 : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

#ifdef J2534_DEVICE_DBRIDGE
    if (m_enProtocol == ISO9141_PS)
    {
        if ((!m_bJ1962Pins) && (enumIoctlID != SET_CONFIG)) 
        {
            return(J2534_ERR_PIN_INVALID);
        }
    }
#endif

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case FIVE_BAUD_INIT:        // CARB (5-baud) initialization

            enumJ2534Error = CarbInit((SBYTE_ARRAY *)pInput, 
                (SBYTE_ARRAY *)pOutput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        pOutput))                                                       
            {
                // Write to Log File.
                WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }
            break;

        case FAST_INIT:             // Fast initialization

//          enumJ2534Error = CarbInit((SBYTE_ARRAY *)pInput, 
//              (SBYTE_ARRAY *)pOutput);
            /*Check if the IOCTL value is not in range */
//          if(enumJ2534Error!= J2534_STATUS_NOERROR)
//              return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        pOutput))                                                       
            {
                // Write to Log File.
                WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }

            break;

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

#ifdef J2534_DEVICE_DBRIDGE
	case START_REPEAT_MESSAGE:
	case QUERY_REPEAT_MESSAGE:
	case STOP_REPEAT_MESSAGE:
        {
            if(enumIoctlID == START_REPEAT_MESSAGE)
            {
                // Check if msg. format is valid.
                REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
                if ((!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]))))
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
                    return(J2534_ERR_INVALID_MSG);
                }
            }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID, pInput, pOutput))                                                      
            {
                // Write to Log File.
                WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
#endif
        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnISO9141Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    ISO9141 messages.
//-----------------------------------------------------------------------------
void OnISO9141RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CISO9141                    *pclsISO9141;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsISO9141 = (CISO9141 *) pVoid;

    // Check for NULL pointer.
    if (pclsISO9141 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("ISO9141.cpp", "OnISO9141RxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    // Write to Log File.
    CProtocolBase::WriteLogMsg("ISO9141.cpp", "OnISO9141RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Message CallBack");

#ifdef J2534_DEVICE_DBRIDGE
    // Only one ISO9141 connection is aloud at a time:  classic ISO9141or ISO9141_PS
    // Set the protocol here not in DeviceCayman.cpp
    if(pclsISO9141->m_enProtocol == ISO9141_PS)
    {
        pstPassThruMsg->ulProtocolID = ISO9141_PS;
    }
#endif

    // Check if this is a Rx First Byte message.
    if (pstPassThruMsg->ulRxStatus & 0x02)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("ISO9141.cpp", "OnISO9141RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx First Byte CALLBACK");

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        // Enqueue to Circ Buffer.
        pclsISO9141->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#endif
#ifdef J2534_0500
		pclsISO9141->m_ulPhyChnRxCount++;
#endif
        return;
    }
    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("ISO9141.cpp", "OnISO9141RxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsISO9141->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsISO9141->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
		pclsISO9141->m_ulPhyChnRxCount++;
#endif
        return;
    }
#ifdef J2534_DEVICE_DBRIDGE
    // Repeat msg support
    if(pclsISO9141->m_pclsRepeatMsg != NULL)
    {
        pclsISO9141->m_pclsRepeatMsg->CompareMsg(pstPassThruMsg);
    }
#endif
    // Apply Filters and see if msg. is required.
    if (pclsISO9141->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsISO9141->IsMsgValid(pstPassThruMsg) &&
            pclsISO9141->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
#ifdef J2534_0305
            if (pclsISO9141->m_ulChecksumFlag)
                pstPassThruMsg->ulExtraDataIndex--;
#endif
            // Enqueue to Circ Buffer.
            pclsISO9141->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
			pclsISO9141->m_ulPhyChnRxCount++;
#endif
        }
    }
#ifdef J2534_0305
    else
    {
        if (pclsISO9141->IsMsgValid(pstPassThruMsg))
        {
            // Enqueue to Circ Buffer.
            pclsISO9141->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#endif
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CISO9141::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < ISO9141_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > ISO9141_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("ISO9141.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("ISO9141 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case P1_MIN:            // P1_MIN

                pSconfig->ulValue = m_usP1MIN;

                break;

            case P1_MAX:            // P1_MAX

                pSconfig->ulValue = m_usP1MAX;

                break;

            case P2_MIN:            // P2_MIN

                pSconfig->ulValue = m_usP2MIN;

                break;

            case P2_MAX:            // P2_MAX

                pSconfig->ulValue = m_usP2MAX;

                break;

            case P3_MIN:            // P3_MIN

                pSconfig->ulValue = m_usP3MIN;

                break;

            case P3_MAX:            // P3_MAX

                pSconfig->ulValue = m_usP3MAX;

                break;

            case P4_MIN:            // P4_MIN

                pSconfig->ulValue = m_usP4MIN;

                break;

            case P4_MAX:            // P4_MAX

                pSconfig->ulValue = m_usP4MAX;

                break;

#ifndef J2534_0500
            case W1 :                // W1
#else
			case W1_MAX:                // W1
#endif
                pSconfig->ulValue = m_usW1MAX;

                break;
#ifndef J2534_0500
            case W2:                // W2
#else
			case W2_MAX:
#endif
                pSconfig->ulValue = m_usW2MAX;

                break;
#ifndef J2534_0500
            case W3:                // W3
#else
			case W3_MAX:                // W3
#endif
                pSconfig->ulValue = m_usW3MAX;

                break;
#ifndef J2534_0500
            case W4:                // W4
				pSconfig->ulValue = m_usW4MAX;
#else
			case W4_MIN:                // W3
				pSconfig->ulValue = m_usW4MAX;
#endif
                break;
#ifndef J2534_0500
            case W0:                // W5
#else
			case W5_MIN:
#endif
                pSconfig->ulValue = m_usW5MIN;

                break;

            case TIDLE:

                pSconfig->ulValue = m_usTidle;

                break;

            case TINIL:

                pSconfig->ulValue = m_usTinil;

                break;

            case TWUP:

                pSconfig->ulValue = m_usTwup;

                break;

            case PARITY:

                pSconfig->ulValue = m_usParity;

                break;

            case DATA_BITS:

                pSconfig->ulValue = m_usDataBits;

                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case FIVE_BAUD_MOD:

                pSconfig->ulValue = m_usFiveBaudMod;

                break;
#endif
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (IsUartBaudRateValid(ulValue) == J2534_STATUS_NOERROR)
    {
        m_ulDataRate = (unsigned short) ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("ISO9141 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);

#ifdef J2534_DEVICE_DBRIDGE
        if (m_enProtocol == ISO9141_PS)
        {
            if ((!m_bJ1962Pins) && (pSconfig->Parameter != J1962_PINS)) 
            {
                return(J2534_ERR_PIN_INVALID);
            }
        }
#endif

        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                enumJ2534Error = DataRate(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback

                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case P1_MIN:            // P1_MIN

                enumJ2534Error = P1_Min(pSconfig->ulValue);

                break;

            case P1_MAX:            // P1_MAX

                enumJ2534Error = P1_Max(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case P2_MIN:            // P2_MIN

                enumJ2534Error = P2_Min(pSconfig->ulValue);

                break;

            case P2_MAX:            // P2_MAX

                enumJ2534Error = P2_Max(pSconfig->ulValue);

                break;

            case P3_MIN:            // P3_MIN

                enumJ2534Error = P3_Min(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case P3_MAX:            // P3_MAX

                enumJ2534Error = P3_Max(pSconfig->ulValue);

                break;

            case P4_MIN:            // P4_MIN

                enumJ2534Error = P4_Min(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case P4_MAX:            // P4_MAX

                enumJ2534Error = P4_Max(pSconfig->ulValue);

                break;
#ifndef J2534_0500
            case W1:                // W1
#else
			case W1_MAX:                // W1
#endif
                enumJ2534Error = w1(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifndef J2534_0500
			case W2:                // W1
#else
			case W2_MAX:                // W1
#endif
                enumJ2534Error = w2(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifndef J2534_0500
			case W3:                // W1
#else
			case W3_MAX:                // W1
#endif
                enumJ2534Error = w3(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifndef J2534_0500
			case W4:                // W1
#else
			case W4_MIN:                // W1
#endif
                enumJ2534Error = w4(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

#ifndef J2534_0500
			case W0:                // W1
			case W5:                // W1
#else
			case W5_MIN:                // W1
#endif                enumJ2534Error = w5(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case TIDLE:

                enumJ2534Error = FastTimingSet(pSconfig->ulValue, 1);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case TINIL:

                enumJ2534Error = FastTimingSet(pSconfig->ulValue, 2);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case TWUP:

                enumJ2534Error = FastTimingSet(pSconfig->ulValue, 4);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case PARITY:

                enumJ2534Error = Parity(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case DATA_BITS:

                enumJ2534Error = DataBits(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
            case FIVE_BAUD_MOD:

                enumJ2534Error = FiveBaudMod(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("ISO9141.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
#endif
            case J1962_PINS:
                if (m_enProtocol == ISO9141_PS)
                {
                    if ((enumJ2534Error = J1962Pins(pSconfig->ulValue)) == J2534_STATUS_NOERROR)
                    {
                        if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                        {
                            WriteLogMsg("ISO9141.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                        }
                    }
                    else
                        m_bJ1962Pins = false;
                }
                else
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                break;
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}


//-----------------------------------------------------------------------------
//  Function Name   : P1_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P1_Min
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P1_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP1MIN = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P1_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P1_Max
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P1_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP1MAX = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P2_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P2_Min
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P2_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP2MIN = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P2_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P2_Max
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P2_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFFFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP2MAX = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P3_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P3_Min
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P3_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP3MIN = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P3_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P3_Max
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P3_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFFFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP3MAX = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P4_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P4_Min
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P4_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP4MIN = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P4_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P4_Max
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::P4_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP4MAX = (unsigned short) ulValue;
    Ptimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : w1
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the w1
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::w1(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usW1MAX = (unsigned short) ulValue;
    Wtimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : w2
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the w2
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::w2(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usW2MAX = (unsigned short) ulValue;
    Wtimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : w3
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the w3
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::w3(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usW3MAX = (unsigned short) ulValue;
    Wtimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : w4
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the w4
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::w4(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usW4MAX = (unsigned short) ulValue;
    Wtimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : w5
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the w5
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::w5(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usW5MIN = (unsigned short) ulValue;
    Wtimedata();

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : CarbInit
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to initiate a carb initialization
//                    sequence from the pass-thru.
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::CarbInit(SBYTE_ARRAY *pInput, 
                               SBYTE_ARRAY *pOutput)
{
    J2534ERROR      enumJ2534Error;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    
    // Make sure pInput and pOutput are not NULL
    if (pInput == NULL || pOutput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    enumJ2534Error = J2534_STATUS_NOERROR;
//  pInput->pucBytePtr[0] = ISO9141_CARB_INIT_ADDR;
//  pOutput->ulNumOfBytes = 2;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Ptimedata
//  Input Params    : void
//  Output Params   : void
//  Description     : Function to set P-timing parameters
//-----------------------------------------------------------------------------
void CISO9141::Ptimedata()
{
    // Make sure P-timing equals its min time
    if (m_usP1 < m_usP1MIN)
        m_usP1 = m_usP1MIN;
    if (m_usP2 < m_usP2MIN)
        m_usP2 = m_usP2MIN;
    if (m_usP3 < m_usP3MIN)
        m_usP3 = m_usP3MIN;
    if (m_usP4 < m_usP4MIN)
        m_usP4 = m_usP4MIN;

    if (m_usP1 > m_usP1MAX)
        m_usP1 = m_usP1MAX;
    if (m_usP2 > m_usP2MAX)
        m_usP2 = m_usP2MAX;
    if (m_usP3 > m_usP3MAX)
        m_usP3 = m_usP3MAX;
    if (m_usP4 > m_usP4MAX)
        m_usP4 = m_usP4MAX;

    ISO9141Ptime[0] = LOBYTE(1 * m_usP1MIN);
    ISO9141Ptime[1] = HIBYTE(1 * m_usP1MIN);
    ISO9141Ptime[2] = LOBYTE(1 * m_usP1MAX);
    ISO9141Ptime[3] = HIBYTE(1 * m_usP1MAX);
    ISO9141Ptime[4] = LOBYTE(1 * m_usP1);
    ISO9141Ptime[5] = HIBYTE(1 * m_usP1);

    ISO9141Ptime[6] = LOBYTE(1 * m_usP2MIN);
    ISO9141Ptime[7] = HIBYTE(1 * m_usP2MIN);
    ISO9141Ptime[8] = LOBYTE(1 * m_usP2MAX);
    ISO9141Ptime[9] = HIBYTE(1 * m_usP2MAX);
    ISO9141Ptime[10] = LOBYTE(1 * m_usP2);
    ISO9141Ptime[11] = HIBYTE(1 * m_usP2);

    ISO9141Ptime[12] = LOBYTE(1 * m_usP3MIN);
    ISO9141Ptime[13] = HIBYTE(1 * m_usP3MIN);
    ISO9141Ptime[14] = LOBYTE(1 * m_usP3MAX);
    ISO9141Ptime[15] = HIBYTE(1 * m_usP3MAX);
    ISO9141Ptime[16] = LOBYTE(1 * m_usP3);
    ISO9141Ptime[17] = HIBYTE(1 * m_usP3);

    ISO9141Ptime[18] = LOBYTE(1 * m_usP4MIN);
    ISO9141Ptime[19] = HIBYTE(1 * m_usP4MIN);
    ISO9141Ptime[20] = LOBYTE(1 * m_usP4MAX);
    ISO9141Ptime[21] = HIBYTE(1 * m_usP4MAX);
    ISO9141Ptime[22] = LOBYTE(1 * m_usP4);
    ISO9141Ptime[23] = HIBYTE(1 * m_usP4);

    // Set P timings
    // Init timings
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : Wtimedata
//  Input Params    : void
//  Output Params   : void
//  Description     : Function to set W-timing parameters
//-----------------------------------------------------------------------------
void CISO9141::Wtimedata()
{
    // Make sure W timings equals Wmax and W5min
    m_usW1 = m_usW1MAX;
    m_usW2 = m_usW2MAX;
    m_usW1 = m_usW3MAX;
    m_usW2 = m_usW4MAX;
    m_usW5 = m_usW5MIN;

    ISO9141Wtime[0] = LOBYTE(2 * m_usW1MIN);
    ISO9141Wtime[1] = HIBYTE(2 * m_usW1MIN);
    ISO9141Wtime[2] = LOBYTE(2 * m_usW1MAX);
    ISO9141Wtime[3] = HIBYTE(2 * m_usW1MAX);
    ISO9141Wtime[4] = LOBYTE(2 * m_usW1);
    ISO9141Wtime[5] = HIBYTE(2 * m_usW1);

    ISO9141Wtime[6] = LOBYTE(2 * m_usW2MIN);
    ISO9141Wtime[7] = HIBYTE(2 * m_usW2MIN);
    ISO9141Wtime[8] = LOBYTE(2 * m_usW2MAX);
    ISO9141Wtime[9] = HIBYTE(2 * m_usW2MAX);
    ISO9141Wtime[10] = LOBYTE(2 * m_usW2);
    ISO9141Wtime[11] = HIBYTE(2 * m_usW2);

    ISO9141Wtime[12] = LOBYTE(2 * m_usW3MIN);
    ISO9141Wtime[13] = HIBYTE(2 * m_usW3MIN);
    ISO9141Wtime[14] = LOBYTE(2 * m_usW3MAX);
    ISO9141Wtime[15] = HIBYTE(2 * m_usW3MAX);
    ISO9141Wtime[16] = LOBYTE(2 * m_usW3);
    ISO9141Wtime[17] = HIBYTE(2 * m_usW3);

    ISO9141Wtime[18] = LOBYTE(2 * m_usW4MIN);
    ISO9141Wtime[19] = HIBYTE(2 * m_usW4MIN);
    ISO9141Wtime[20] = LOBYTE(2 * m_usW4MAX);
    ISO9141Wtime[21] = HIBYTE(2 * m_usW4MAX);
    ISO9141Wtime[22] = LOBYTE(2 * m_usW4);
    ISO9141Wtime[23] = HIBYTE(2 * m_usW4);

    ISO9141Wtime[24] = LOBYTE(2 * m_usW5MIN);
    ISO9141Wtime[25] = HIBYTE(2 * m_usW5MIN);
    ISO9141Wtime[26] = LOBYTE(2 * m_usW5MAX);
    ISO9141Wtime[27] = HIBYTE(2 * m_usW5MAX);
    ISO9141Wtime[28] = LOBYTE(2 * m_usW5);
    ISO9141Wtime[29] = HIBYTE(2 * m_usW5);

    // Set W timings
    
    // Init timings
    
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : FastInit
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to initiate a fast initialization
//                    sequence from the pass-thru.
//-----------------------------------------------------------------------------
J2534ERROR CISO9141::FastInit(PASSTHRU_MSG *pInput, PASSTHRU_MSG *pOutput)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

//  // Make sure pInput is not NULL
//  if (pInput == NULL)
//      return(J2534_ERR_NULLPARAMETER);

//  iReturn=g_Unat.FastInit(pInput, pOutput);

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : Parity
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Parity
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::Parity(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < ISO9141_NO_PARITY) || (ulValue > ISO9141_EVEN_PARITY))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
    {
//      g_Unat.Parity(ISO9141, ulValue, m_ulFlag);
        m_usParity = (unsigned short) ulValue;
    }

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataBits
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the DataBits
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::DataBits(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < ISO9141_8_DATABITS) || (ulValue > ISO9141_7_DATABITS))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
    {
//      g_Unat.DataBits(ISO9141, ulValue, m_ulFlag);
        m_usDataBits = (unsigned short) ulValue;
    }

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : FiveBaudMod
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Five Baud Mod
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::FiveBaudMod(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < ISO9141_5_BAUD_REG) || (ulValue > ISO9141_5_BAUD_ISO9141))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
        m_usFiveBaudMod = (unsigned short) ulValue;

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : FastTimingSet
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Fast Init Timings
//-----------------------------------------------------------------------------
J2534ERROR  CISO9141::FastTimingSet(unsigned long ulValue, unsigned char ucType)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    if (ucType == 1)
    {
        m_usTidle = (unsigned short) ulValue;
    }
    else if (ucType == 2)
    {
        m_usTinil = (unsigned short) ulValue;
    }
    else if (ucType == 4)
    {
        m_usTwup = (unsigned short) ulValue;
    }
//  FastInitTiming(ucType);

    return(iReturn);
}


J2534ERROR  CISO9141::J1962Pins(unsigned long ulValue)
{
    // Only K and/or Lline ISO9141_PS
    if ((ulValue == 0x0F00) || (ulValue == 0x0700) || (ulValue == 0x070F))
    {
        m_bJ1962Pins = true;
        return(J2534_STATUS_NOERROR);
    }
    else
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }
}
