/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: PeriodicMsg.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of the 
*		       CPeriodicMsg class
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "PeriodicMsg.h"

void fnWriteLogMsg(char * chFileName, char * chFunctionName, unsigned long MsgType, char * chMsg, ...);

//-----------------------------------------------------------------------------
//	Function Name	: CPeriodicMsg
//	Input Params	: void
//	Output Params	: void
//	Description		: Constructor for CPeriodicMsg class.
//-----------------------------------------------------------------------------
CPeriodicMsg::CPeriodicMsg(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID,
						   CDebugLog *pclsDebugLog)
{
	int	i;

	// Save pointers.
	m_pclsLog = pclsDebugLog;
	m_pclsDevice = pclsDevice;
	m_ulDevChannelID = ulDevChannelID;

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "CPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Initialize the PeriodicType portion of the list.
	for (i = 0; i <= PERIODICMSG_LIST_SIZE; i++)
	{
		m_stPeriodicList[i].pstMsg = NULL;
		m_stPeriodicList[i].ulTimeInterval = 0;
		m_stPeriodicList[i].ulDevPeriodicID = 0;
	}

	m_hSyncAccess = CreateEvent(NULL, FALSE, TRUE, NULL);

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "CPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//	Function Name	: ~CPeriodicMsg
//	Input Params	: void
//	Output Params	: void
//	Description		: Destructor for CPeriodicMsg class
//-----------------------------------------------------------------------------
CPeriodicMsg::~CPeriodicMsg()
{
	int i;

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "~CPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Stop all Periodic
	StopPeriodic();

	// Delete the resources.
	for (i = 0; i <= PERIODICMSG_LIST_SIZE; i++)
	{
		if (m_stPeriodicList[i].pstMsg != NULL)
		{	
			delete m_stPeriodicList[i].pstMsg;
			m_stPeriodicList[i].pstMsg = NULL;
		}

		m_stPeriodicList[i].ulTimeInterval = 0;
		m_stPeriodicList[i].ulDevPeriodicID = 0;
	}

	CloseHandle(m_hSyncAccess);

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "~CPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//	Function Name	: StartPeriodic
//	Input Params	: 
//	Output Params	: 
//	Description		: This function starts a given msg. periodic and returns a
//					  Periodic. ID for reference.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::StartPeriodic(
							   PASSTHRU_MSG		*pstMsg, 
							   unsigned long	*pulPeriodicID,
							   unsigned long	ulTimeInterval)

{
//	char			szBuffer[PERIODICMSG_ERROR_TEXT_SIZE];
	J2534ERROR		enJ2534Error;
	unsigned long	ulDevPeriodicID;

	// Synchronize Access to this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("PeriodicMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	}

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check if NULL pointers.
	if (pstMsg == NULL)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
		SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG);
	}

	// Is number of Periodic exceeded the maximum.
	if (IsListFull())
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		SetEvent(m_hSyncAccess);
		return(J2534_ERR_EXCEEDED_LIMIT);
	}

	// Check if the Datasize and TxFlags are the same for Pattern,
	// and Mask messages.
	if (pstMsg->ulDataSize > PERIODICMSG_MSGLEN_MAX)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
		SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG);
	}

	// Start Periodic on Device.
	if ((enJ2534Error = m_pclsDevice->vStartPeriodic(
										m_ulDevChannelID, pstMsg, 
										ulTimeInterval, &ulDevPeriodicID)) 
					  != J2534_STATUS_NOERROR)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		// It failed to set periodic in device so set the ID to 0.
		ulDevPeriodicID = 0;
		SetEvent(m_hSyncAccess);
		return(enJ2534Error);
	}

	// Add the periodic info. to our internal Database.
	if ((enJ2534Error = Add(pstMsg, ulTimeInterval, ulDevPeriodicID, pulPeriodicID)) != J2534_STATUS_NOERROR)
	{
		// Stop the Device Periodic.
		m_pclsDevice->vStopPeriodic(m_ulDevChannelID, ulDevPeriodicID);
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		SetEvent(m_hSyncAccess);
		return(enJ2534Error);
	}

	SetEvent(m_hSyncAccess);

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StartPeriodic()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: StopPeriodic
//	Input Params	: 
//	Output Params	: 
//	Description		: This function stops the periodic corresspond to a given ID.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::StopPeriodic(unsigned long ulPeriodicID)

{
//	char			szBuffer[PERIODICMSG_ERROR_TEXT_SIZE];
	J2534ERROR		enJ2534Error;

	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("PeriodicMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	}

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Validate if the periodic msg. for this PeriodicID was started earlier.
	if (!IsPeriodicIDValid(ulPeriodicID))
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
		SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG_ID);
	}

	// Stop the periodic in device if it were set earlier.
	if ((enJ2534Error = m_pclsDevice->vStopPeriodic(m_ulDevChannelID, m_stPeriodicList[ulPeriodicID].ulDevPeriodicID)) 
					  != J2534_STATUS_NOERROR)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		SetEvent(m_hSyncAccess);
		return(enJ2534Error);
	}

	// Delete the periodic
	Delete(ulPeriodicID);

	SetEvent(m_hSyncAccess);

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: StopPeriodic
//	Input Params	: 
//	Output Params	: 
//	Description		: This is an override function that stops all the periodics.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::StopPeriodic()

{
	unsigned long	i;

	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic(All)", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("PeriodicMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	}

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic(All)", DEBUGLOG_TYPE_COMMENT, "Start");

	// Run through all the Periodics and delete all of them.
	for (i = 1; i <= PERIODICMSG_LIST_SIZE; i++)
	{
		if (m_stPeriodicList[i].pstMsg != NULL)
		{
			m_pclsDevice->vStopPeriodic(m_ulDevChannelID, m_stPeriodicList[i].ulDevPeriodicID);
		}
		// Delete the periodic
		Delete(i);
	}

	SetEvent(m_hSyncAccess);
    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopPeriodic(All)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: StopCANPeriodic
//	Input Params	: 
//	Output Params	: 
//	Description		: This is an override function that stops all the CAN periodics.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::StopCANPeriodic()

{
	unsigned long	i;

	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "StopCANPeriodic(All)", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("PeriodicMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	}

    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopCANPeriodic(All)", DEBUGLOG_TYPE_COMMENT, "Start");

	// Run through all the Periodics and delete all of them.
	for (i = 1; i <= PERIODICMSG_LIST_SIZE; i++)
	{
		if (m_stPeriodicList[i].pstMsg != NULL)
		{
			if (m_stPeriodicList[i].pstMsg->ulProtocolID == SW_CAN_PS ||
                m_stPeriodicList[i].pstMsg->ulProtocolID == SW_CAN_CAN_CH1 ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN_PS ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN_CH1 ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN_CH2 ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN_CH3 ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == CAN_CH4 ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == FT_CAN_PS ||
				m_stPeriodicList[i].pstMsg->ulProtocolID == FT_CAN_CH1)
			{
				m_pclsDevice->vStopPeriodic(m_ulDevChannelID, m_stPeriodicList[i].ulDevPeriodicID);
				// Delete the periodic
				Delete(i);
			}
		}
	}

	SetEvent(m_hSyncAccess);
    // Write to Log File.
    fnWriteLogMsg("PeriodicMsg.cpp", "StopCANPeriodic(All)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: Add
//	Input Params	: void
//	Output Params	: void
//	Description		: This function assigns a Msg. ID, adds message to 
//					  internal buffer and returns the PeriodicID to user.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::Add(PASSTHRU_MSG *pstMsg, 
							 unsigned long ulTimeInterval,
							 unsigned long ulDevPeriodicID,
							 unsigned long *pulPeriodicID)
{
	unsigned long	ulPeriodicID;

	// Get new PeriodicID.
	if ((ulPeriodicID = GetNewPeriodicID()) == 0)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "Add()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		return(J2534_ERR_EXCEEDED_LIMIT);	
	}

	// Save Mask periodic in our internal buffer.
	if ((m_stPeriodicList[ulPeriodicID].pstMsg = new PASSTHRU_MSG) == NULL)
	{
        // Write to Log File.
        fnWriteLogMsg("PeriodicMsg.cpp", "Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Periodic Mask");
		SetLastErrorText("PeriodicMsg : Could not create Object for Periodic Msg.");
		return(J2534_ERR_FAILED);	
	}
	memcpy(m_stPeriodicList[ulPeriodicID].pstMsg, pstMsg, 
		   min(sizeof(PASSTHRU_MSG), sizeof(PASSTHRU_MSG)));

	// Save Time Interval.
	m_stPeriodicList[ulPeriodicID].ulTimeInterval = ulTimeInterval;

	// Save Device Periodic ID.
	m_stPeriodicList[ulPeriodicID].ulDevPeriodicID = ulDevPeriodicID;

	// Return the PeriodicID to user.
	*pulPeriodicID = ulPeriodicID;

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: Delete
//	Input Params	: void
//	Output Params	: void
//	Description		: This function removes the periodic from the Database that 
//					  was added earlier.
//-----------------------------------------------------------------------------
J2534ERROR CPeriodicMsg::Delete(unsigned long ulPeriodicID)
{
	if (m_stPeriodicList[ulPeriodicID].pstMsg != NULL)
	{
		delete m_stPeriodicList[ulPeriodicID].pstMsg;
		m_stPeriodicList[ulPeriodicID].pstMsg = NULL;
	}

	m_stPeriodicList[ulPeriodicID].ulTimeInterval = 0;

	m_stPeriodicList[ulPeriodicID].ulDevPeriodicID = 0;

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: GetNewPeriodicID
//	Input Params	: void
//	Output Params	: void
//	Description		: If all PeriodicIDs are used, returns 0 otherwise returns new
//					  PeriodicID to be used.
//-----------------------------------------------------------------------------
unsigned char CPeriodicMsg::GetNewPeriodicID()
{
	unsigned char	ucIdx;

	for (ucIdx = 1; ucIdx <= PERIODICMSG_LIST_SIZE; ucIdx++)
	{
		if (m_stPeriodicList[ucIdx].pstMsg == NULL)
			return(ucIdx);
	}
	
	return(0);
}

//-----------------------------------------------------------------------------
//	Function Name	: IsPeriodicIDValid
//	Input Params	: void
//	Output Params	: void
//	Description		: Checks to see if the given PeriodicID is valid.
//-----------------------------------------------------------------------------
bool CPeriodicMsg::IsPeriodicIDValid(unsigned long ulPeriodicID)
{
	if (ulPeriodicID > 0 && ulPeriodicID <= PERIODICMSG_LIST_SIZE)
	{
		if (m_stPeriodicList[ulPeriodicID].pstMsg != NULL)
			return(true);
	}
	
	return(false);
}

//-----------------------------------------------------------------------------
//	Function Name	: IsListFull
//	Input Params	: void
//	Output Params	: void
//	Description		: Checks to see if the list to store Periodic info. is full.
//-----------------------------------------------------------------------------
bool CPeriodicMsg::IsListFull()
{
	unsigned long ucIdx;

	for (ucIdx = 1; ucIdx <= PERIODICMSG_LIST_SIZE; ucIdx++)
	{
		if (m_stPeriodicList[ucIdx].pstMsg == NULL)
			return(false);
	}
	
	return(true);
}
