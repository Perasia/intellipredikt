/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2818.h
* Author(s):  Pat Ruelle  April 20, 2013
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              J2818 Class.
* Note:
*
*******************************************************************************/

#ifndef _J2818_H_
#define _J2818_H_

// Constants

#define J2818_ERROR_TEXT_SIZE		120
#define J2818_DATA_RATE_DEFAULT	10400
#define J2818_HEADER_SIZE			0		// Number of Header bits.

#define J2818_T0MIN_DEFAULT		2*5
#define J2818_T1MAX_DEFAULT		2*200
#define J2818_T2MAX_DEFAULT		2*100
#define J2818_T3MAX_DEFAULT		2*100
#define J2818_T4MIN_DEFAULT		1
#define J2818_T5MAX_DEFAULT		2*500
#define J2818_T6MAX_DEFAULT		2*100
#define J2818_T7MIN_DEFAULT		1
#define J2818_T7MAX_DEFAULT		2*40
#define J2818_T9MIN_DEFAULT		1

/*
#define J2818_P1_DEFAULT			2*0
#define J2818_P2_DEFAULT			2*25
#define J2818_P3_DEFAULT			2*55
#define J2818_P4_DEFAULT			2*5

#define J2818_W1MIN_DEFAULT		60
#define J2818_W1MAX_DEFAULT		300
#define J2818_W2MIN_DEFAULT		5
#define J2818_W2MAX_DEFAULT		20
#define J2818_W3MIN_DEFAULT		0
#define J2818_W3MAX_DEFAULT		20
#define J2818_W4MIN_DEFAULT		25
#define J2818_W4MAX_DEFAULT		50
#define J2818_W5MIN_DEFAULT		300
#define J2818_W5MAX_DEFAULT		600

#define J2818_W1_DEFAULT			300
#define J2818_W2_DEFAULT			20
#define J2818_W3_DEFAULT			20
#define J2818_W4_DEFAULT			50
#define J2818_W5_DEFAULT			300
*/

#define J2818_KEYBYTEID			0x55
#define J2818_TIMING_DATA_MAX		30

//#define J2818_CARB_INIT_ADDR		0x33
#define J2818_MSG_SIZE_MIN		4
#define J2818_MSG_SIZE_MAX		256
#define J2818_TIDLE_DEFAULT	300
#define J2818_TINIL_DEFAULT	25
#define J2818_TWUP_DEFAULT	50
#define J2818_NO_PARITY		0
#define J2818_ODD_PARITY		1
#define J2818_EVEN_PARITY		2
#define J2818_8_DATABITS		0
#define J2818_7_DATABITS		1
#define J2818_5_BAUD_REG		0
#define J2818_5_BAUD_J2818	    0xffff

typedef char  unsigned  dgUint8;

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnJ2818RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CJ2818 Class
class CJ2818 : public CProtocolBase
{
public:
	CJ2818(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CJ2818();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);
 	bool				IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				CarbInit(SBYTE_ARRAY *pInput, SBYTE_ARRAY *pOutput);
	J2534ERROR				Loopback(unsigned long ulValue);

	J2534ERROR				T0_Min(unsigned long ulValue),
							T1_Max(unsigned long ulValue),
							P2_Min(unsigned long ulValue),
							T2_Max(unsigned long ulValue),
							T3_Max(unsigned long ulValue),
							T4_Min(unsigned long ulValue),
							T5_Max(unsigned long ulValue),
							T6_Max(unsigned long ulValue),
							T7_Min(unsigned long ulValue),
							T7_Max(unsigned long ulValue),
							T9_Min(unsigned long ulValue);

	unsigned short			m_usT0MIN,
							m_usT1MAX,
							m_usT2MAX,
							m_usT3MAX,
							m_usT4MIN,
							m_usT5MAX,
							m_usT6MAX,
							m_usT7MIN,
							m_usT7MAX,
							m_usT9MIN;

	unsigned long			m_ulChecksumFlag;

	J2534ERROR				FastTimingSet(unsigned long ulValue, unsigned char ucType);
	unsigned short			m_usTidle,
							m_usTinil,
							m_usTwup;

	unsigned short			m_usParity,
							m_usDataBits,
							m_usFiveBaudMod;

	J2534ERROR				FastInit(PASSTHRU_MSG *pInput, PASSTHRU_MSG *pOutput);
	J2534ERROR				Parity(unsigned long ulValue);
	J2534ERROR				DataBits(unsigned long ulValue);
	J2534ERROR				FiveBaudMod(unsigned long ulValue);

public:
	unsigned char		m_ucIOdata;
	
};

#endif
