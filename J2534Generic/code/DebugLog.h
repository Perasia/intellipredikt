/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DebugLog.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of CDebugLog class.
* Note:
*
*******************************************************************************/

#ifndef _DEBUGLOG_H_
#define _DEBUGLOG_H_

#define DEBUGLOG_FILE_EXPORT            "J2534Export.cpp"
#define DEBUGLOG_FILE_ISO15765          "ISO15765.cpp"
#define DEBUGLOG_FILE_CAN               "Can.cpp"
#define DEBUGLOG_FILE_J1850VPW          "J1850VPW.cpp"
#define DEBUGLOG_FILE_J1850PWM          "J1850PWM.cpp"
#define DEBUGLOG_FILE_ISO9141           "ISO9141.cpp"
#define DEBUGLOG_FILE_ISO14230          "ISO14230.cpp"
#define DEBUGLOG_FILE_DEVICESTORE       "DeviceStore.cpp"
#define DEBUGLOG_FILE_BASE              "J2534Base.cpp"

// Logging Type
#define DEBUGLOG_TYPE_ERROR             0x01
#define DEBUGLOG_TYPE_COMMENT           0x02
#define DEBUGLOG_TYPE_DATA              0x04
#define DEBUGLOG_TYPE_APPEND            0x80000000

#define DEBUGLOG_WRITELOG_TIMEOUT       1000

class CDebugLog
{
    public:
        CDebugLog();
        ~CDebugLog();
//      bool Write(CString csFile, CString csFunc, unsigned long ulType, 
//                 CString csDesc);
        bool Write(char *csFile, char *csFunc, unsigned long ulType, char *csDesc);
        bool Open(CString csLogFileName, unsigned long ulLogType, char *csDllFileName, unsigned long ulLoggingSizeMaxKB);
        bool Close();

        FILE            *m_pfdLogFile;          // Log File Descriptor.

    private:
        HANDLE          m_hWriteLogSync;        // Handle for WriteToLog synch.
        unsigned long   m_ulLoggingType;        // Type of Logging.
        CString         m_strLogFileName;       // Log File Name
        long            m_lLoggingSizeMaxKB;    // Maximum size of log file in KB
        void            TrimLogFile();

};

#endif
