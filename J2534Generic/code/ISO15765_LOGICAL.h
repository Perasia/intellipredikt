#pragma once
#ifdef J2534_0500
#define ISO15765_ERROR_TEXT_SIZE    120

#define ISO15765_MSG_SIZE_MIN       4
#ifdef UD_TRUCK
#define ISOCAN_DATA_RATE_DEFAULT    250000
#define ISOCAN_DATA_RATE_MEDIUM_HIGH  500000
#else
#define ISOCAN_DATA_RATE_DEFAULT    500000
#endif

#define ISO15765_HEADER_SIZE                    11
#define ISO15765_EXTENDED_HEADER_SIZE           29 

#define ISO15765_MSG_SIZE_MAX_SF				11
#define ISO15765_MSG_SIZE_MAX					4099
#define ISOCAN_DATA_RATE_MEDIUM					250000
#define ISOCAN_DATA_RATE_LOW					125000

#define ISO15765_CAN_MSG_SIZE_MIN			    4
#define ISO15765_CAN_MSG_SIZE_MAX				12

#define ISO15765_FC_LIST            30

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnISO15765LogicalRxMessage(PASSTHRU_MSG *pstPassThruMsg,
	LPVOID pVoid);

typedef struct
{
	PASSTHRU_MSG stMask;
	PASSTHRU_MSG stPattern;
	PASSTHRU_MSG stFlowControl;
	bool bIsSet;
}
FC_FILTERMSGLIST_VSI;
class CISO15765_LOGICAL : public CProtocolBase
{
public:
	CISO15765_LOGICAL(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog = NULL);
	~CISO15765_LOGICAL();

	virtual J2534ERROR  vConnect(
		J2534_PROTOCOL enProtocolID,
		unsigned long ulFlags,
		unsigned long ulBaudRate,
		DEVICEBASE_CALLBACK_RX_FUNC pfnCallback = NULL,
		LPVOID        pVoid = NULL);
	virtual J2534ERROR  vDisconnect();
	virtual J2534ERROR  vReadMsgs(
		PASSTHRU_MSG  *pstrucJ2534Msg,
		unsigned long *pulNumMsgs,
		unsigned long ulTimeout);
	virtual J2534ERROR  vReadMsgs(
		PASSTHRU_MSG  *pstrucJ2534Msg,
		unsigned long *pulNumMsgs,
		unsigned long ulTimeout,
		unsigned long);
	virtual J2534ERROR  vWriteMsgs(
		PASSTHRU_MSG *pstrucJ2534Msg,
		unsigned long *pulNumMsgs,
		unsigned long ulTimeout
		,unsigned long
	);
	virtual J2534ERROR  vStartPeriodicMsg(
		PASSTHRU_MSG *pstrucJ2534Msg,
		unsigned long *pulMsgID,
		unsigned long ulTimeInterval,
		unsigned long   ulChannelId
		);
	virtual J2534ERROR  vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR  vStartMsgFilter(
		J2534_FILTER enFilterType,
		PASSTHRU_MSG *pstrucJ2534Mask,
		PASSTHRU_MSG *pstrucJ2534Pattern,
		PASSTHRU_MSG *pstrucJ2534FlowControl,
		unsigned long *pulFilterID);
	virtual J2534ERROR  vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR  vIoctl(J2534IOCTLID enumIoctlID,
		void *pInput,
		void *pOutput);
	virtual J2534ERROR  vIoctl(J2534IOCTLID enumIoctlID,
		void *pInput,
		void *pOutput,
		unsigned long ulChannelId);

	bool                IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
	bool                IsCANMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
	bool                IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);
	bool                IsCANMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
	J2534ERROR              GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR              SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR              DataRate(unsigned long ulValue);
	J2534ERROR              Loopback(unsigned long ulValue);
	J2534ERROR              BlockSize(unsigned long ulValue);
	J2534ERROR              BlockSizeTx(unsigned long ulValue);
	J2534ERROR              STmin(unsigned long ulValue);
	J2534ERROR              STminTx(unsigned long ulValue);
	J2534ERROR              WftMax(unsigned long ulValue);
	J2534ERROR              CANMixedMode(unsigned long ulValue);
	J2534ERROR				SetErrorReporting(unsigned char *pucValue);
	J2534ERROR				SetInternalTermination(unsigned char *pucValue);
	J2534ERROR		        J1962Pins(unsigned long ulValue);
	J2534ERROR		        J1939Pins(unsigned long ulValue);

	float                   m_fSamplePoint;
	float                   m_fJumpWidth;
	unsigned long           m_ulCANIDtype;
	unsigned long           m_ulCANExtAddr;
	// J2534_PROTOCOL           m_enISO15765Protocol;
	J2534_PROTOCOL           m_enISO15765Protocol;
	unsigned long           m_ulBlockSize,
		m_ulBlockSizeTx,
		m_ulSTmin,
		m_ulSTminTx,
		m_ulWftMax;
	J2534_CAN_MIXED_FORMAT	m_enCANMixedFormat;
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;
	unsigned long			m_ulJ1939PPSS;
	bool					m_bJ1939Pins;
	unsigned long			m_ulmsgHandle;
#ifdef J2534_DEVICE_VSI
	FC_FILTERMSGLIST_VSI        m_stFCFilterMsgList[ISO15765_FC_LIST];
#endif
};
#endif

