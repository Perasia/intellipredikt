/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: GMUART.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              GMUART Class.
* Note:
*
*******************************************************************************/

#ifndef _GMUART_H_
#define _GMUART_H_

// Constants

#define GMUART_ERROR_TEXT_SIZE		120
#define GMUART_DATA_RATE_DEFAULT	10400
#define GMUART_HEADER_SIZE			0		// Number of Header bits.
#define GMUART_MSG_SIZE_MIN			3
#define GMUART_MSG_SIZE_MAX			170

typedef char  unsigned  dgUint8;

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnGMUARTRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CGMUART Class
class CGMUART : public CProtocolBase
{
public:
	CGMUART(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CGMUART();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);
	J2534ERROR			SetPollResponse(SBYTE_ARRAY *pInput);
	J2534ERROR			BecomeMaster(unsigned char *pucPoll_ID);
	unsigned long		m_ucPollResponseMsg[100];
	unsigned long		m_ulPollResponseMsgLength;
	unsigned char		m_ucMsg_ID;
	unsigned char		m_ucDevice_ID;
	HANDLE				m_hPollMsg;
	bool				bIsPollMsgThreeByte;
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);


public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR				J1962Pins(unsigned long ulValue);

	unsigned long			m_ulChecksumFlag;

public:
	unsigned char		m_ucIOdata;
	
};

#endif
