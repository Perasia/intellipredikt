/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: SWCAN.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              SWCAN Class.
* Note:
*
*******************************************************************************/

#ifndef _SWCAN_H_
#define _SWCAN_H_

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define SWCAN_ERROR_TEXT_SIZE		120

#define SWCAN_MSG_SIZE_MIN		4
#define SWCAN_MSG_SIZE_MAX		12
#define SWCAN_DATA_RATE_DEFAULT 33333
#define SWCAN_DATA_RATE_MEDIUM	83333

#define SWCAN_HEADER_SIZE					11
#define SWCAN_EXTENDED_HEADER_SIZE		29

#define SWCAN_CAN_NS_RX         (1 << 18)
#define SWCAN_CAN_HS_RX         (1 << 17)

void OnSWCANRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CSWCAN Class
class CSWCAN : public CProtocolBase
{
public:
	CSWCAN(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
	~CSWCAN();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
	bool				IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR				HSDataRate(unsigned long ulValue);
	J2534ERROR				SpeedChange(unsigned long ulValue);
	J2534ERROR				ResSwitch(unsigned long ulValue);
	J2534ERROR				J1962Pins(unsigned long ulValue);

	float					m_fSamplePoint;
	float					m_fJumpWidth;
	unsigned long			m_ulSWCANIDtype;
	J2534_PROTOCOL			m_enSWCANProtocol;
	unsigned long			m_ulHSDataRate,
							m_ulSWCAN_SpeedChange_Enable,
							m_ulSWCAN_Res_Switch;
	unsigned char			m_ucNormalToHighSpeed;
	bool			        m_bBackToNormalSpeed;
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;
    bool                    m_bIsRunningHighSpeed;
};

#endif
