//*****************************************************************************
//					Dearborn Group Technology
//*****************************************************************************
// Project Name			: J2534 Generic
// File Name			: DIAGH.h
// Description			: This file contains the declaration of 
//						  DIAGH Class.
// Date                 : September, 2014
// Version              : 1.0
// Author               : Val Koycheva
// Revision				: 
//*****************************************************************************

#ifndef _DIAGH_H_
#define _DIAGH_H_

// Constants

#define DIAGH_ERROR_TEXT_SIZE   120
#define DIAGH_DATA_RATE_DEFAULT	9600
#define DIAGH_HEADER_SIZE       0		// Number of Header bits.

// TODO DEFAULT??
#define DIAGH_P1MAX_DEFAULT		2*20
#define DIAGH_P3MIN_DEFAULT		2*55
#define DIAGH_P4MIN_DEFAULT		2*5

#define DIAGH_KEYBYTEID         0x55
#define DIAGH_TIMING_DATA_MAX   30

#define DIAGH_MSG_SIZE_MIN		3   // Tx - 3 bytes, Rx - 1 byte
#define DIAGH_MSG_SIZE_MIN_RX	1   // Tx - 3 bytes, Rx - 1 byte
#define DIAGH_MSG_SIZE_MAX		255

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnDIAGHRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CDIAGH Class
class CDIAGH : public CProtocolBase
{
public:
	CDIAGH(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CDIAGH();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);
	bool				IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
	J2534ERROR			GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR			SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR			DataRate(unsigned long ulValue);
	J2534ERROR			Loopback(unsigned long ulValue);
	J2534ERROR		    J1962Pins(unsigned long ulValue);

    J2534ERROR			P1_Max(unsigned long ulValue),
						P3_Min(unsigned long ulValue),
						P4_Min(unsigned long ulValue);

    unsigned short		m_usP1MAX, 
						m_usP3MIN,
						m_usP4MIN;

	unsigned short		m_usP1,
						m_usP3,
						m_usP4;

	unsigned long		m_ulChecksumFlag;
	unsigned char	    m_ucIOdata;
    bool				m_bJ1962Pins;
    unsigned long		m_ulPPSS;
};

#endif
