/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2818.cpp
* Author(s):  Pat Ruelle  April 20, 2013
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support J2818/J2809 
*              protocol as required by J2534-2.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "J2818.h"

#if !defined(J2534_DEVICE_DPA5_EURO5) || defined(J2534_DEVICE_DBRIDGE)
// J2818 only implemented on Cayman style boards
//-----------------------------------------------------------------------------
//  Function Name   : CJ2818
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CJ2818 class
//-----------------------------------------------------------------------------
CJ2818::CJ2818(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) :
            CProtocolBase(pclsDevice, pclsDebugLog)
{
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "CJ2818()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "CJ2818()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ2818
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CJ2818 class
//-----------------------------------------------------------------------------
CJ2818::~CJ2818()
{
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "~CJ2818()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "~CJ2818()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vConnect(J2534_PROTOCOL    enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Get the Check Sum Flag from the connection flags
    m_ulChecksumFlag = ((ulFlags >> 9) & 0x01);
//  ulFlags = m_ulChecksumFlag;
    ulFlags = (((ulFlags >> 12) & 0x01) << 1);
    ulFlags |= m_ulChecksumFlag;
    switch(enProtocolID)
    {
    case UART_ECHO_BYTE_PS:     // aka J2818/J2809
        {
        if (IsUartBaudRateValid(ulBaudRate) == J2534_ERR_INVALID_BAUDRATE)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }

            m_ulDataRate = ulBaudRate;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnJ2818RxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    m_usT0MIN = J2818_T0MIN_DEFAULT;
    m_usT1MAX = J2818_T1MAX_DEFAULT;
    m_usT2MAX = J2818_T2MAX_DEFAULT;
    m_usT3MAX = J2818_T3MAX_DEFAULT;
    m_usT4MIN = J2818_T4MIN_DEFAULT;
    m_usT5MAX = J2818_T5MAX_DEFAULT;
    m_usT6MAX = J2818_T6MAX_DEFAULT;
    m_usT7MIN = J2818_T7MIN_DEFAULT;
    m_usT7MAX = J2818_T7MAX_DEFAULT;
    m_usT9MIN = J2818_T9MIN_DEFAULT;
    m_usTidle = J2818_TIDLE_DEFAULT;
    m_usTinil = J2818_TINIL_DEFAULT;
    m_usTwup = J2818_TWUP_DEFAULT;
    m_usParity = J2818_NO_PARITY;
    m_usDataBits = J2818_8_DATABITS;
    m_usFiveBaudMod = J2818_5_BAUD_REG;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vReadMsgs(PASSTHRU_MSG     *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (*pulNumMsgs != 1)
    {
            // Write to Log File.
            WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "Number of msg not 1, returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
            return(J2534_ERR_EXCEEDED_LIMIT);
    }

    // Check Msg. Protocol ID.
    if ((pstPassThruMsg)->ulProtocolID != UART_ECHO_BYTE_PS)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid((pstPassThruMsg)))
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vStartPeriodicMsg(PASSTHRU_MSG     *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::vStopPeriodicMsg(unsigned long ulMsgID)
{
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::vStartMsgFilter(J2534_FILTER  enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != UART_ECHO_BYTE_PS) ||
        (pstPattern->ulProtocolID != UART_ECHO_BYTE_PS))
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("J2818 : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration

            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case FIVE_BAUD_INIT:        // CARB (5-baud) initialization

            enumJ2534Error = CarbInit((SBYTE_ARRAY *)pInput, 
                (SBYTE_ARRAY *)pOutput);
            if(enumJ2534Error != J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        pOutput))                                                       
            {
                // Write to Log File.
                WriteLogMsg("J2818.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }
            break;

        // FAST_INIT Not supported - It is not tested nether mentioned in the Spec

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

#ifdef J2534_DEVICE_DBRIDGE
	    case START_REPEAT_MESSAGE:
	    case QUERY_REPEAT_MESSAGE:
	    case STOP_REPEAT_MESSAGE:
            {
                if(enumIoctlID == START_REPEAT_MESSAGE)
                {
                    // Check if msg. format is valid.
                    REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
                    if ((!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]))) ||
                        (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]))) ||
                        (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]))))
                    {
                        // Write to Log File.
                        WriteLogMsg("CAN.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
                        return(J2534_ERR_INVALID_MSG);
                    }
                }
                if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID, pInput, pOutput))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("CAN.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    return(enumJ2534Error);

                }   
            }
            break;
#endif
        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("J2818.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnJ2818Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    J2818 messages.
//-----------------------------------------------------------------------------
void OnJ2818RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CJ2818                    *pclsJ2818;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsJ2818 = (CJ2818 *) pVoid;

    // Check for NULL pointer.
    if (pclsJ2818 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("J2818.cpp", "OnJ2818RxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    // Write to Log File.
    CProtocolBase::WriteLogMsg("J2818.cpp", "OnJ2818RxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Message CallBack");

    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Loopback is not supported by this protocol
        return;
    }
#ifdef J2534_DEVICE_DBRIDGE
    // Repeat msg support
    if(pclsJ2818->m_pclsRepeatMsg != NULL)
    {
        pclsJ2818->m_pclsRepeatMsg->CompareMsg(pstPassThruMsg);
    }
#endif
    // Apply Filters and see if msg. is required.
    if (pclsJ2818->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsJ2818->IsMsgValidRx(pstPassThruMsg) &&
            pclsJ2818->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsJ2818->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ2818::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J2818_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J2818_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}     

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ2818::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J2818_MSG_SIZE_MIN-1) || 
        (pstPassThruMsg->ulDataSize > J2818_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J2818.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J2818 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case UEB_T0_MIN:        // UEB_T0_MIN

                pSconfig->ulValue = m_usT0MIN;

                break;

            case UEB_T1_MAX:        // UEB_T1_MAX

                pSconfig->ulValue = m_usT1MAX;

                break;

            case UEB_T2_MAX:        // UEB_T2_MAX

                pSconfig->ulValue = m_usT2MAX;

                break;

            case UEB_T3_MAX:        // UEB_T3_MAX

                pSconfig->ulValue = m_usT3MAX;

                break;

            case UEB_T4_MIN:        // UEB_T4_MIN

                pSconfig->ulValue = m_usT4MIN;

                break;

            case UEB_T5_MAX:        // UEB_T5_MAX

                pSconfig->ulValue = m_usT5MAX;

                break;

            case UEB_T6_MAX:        // UEB_T6_MAX

                pSconfig->ulValue = m_usT6MAX;

                break;

            case UEB_T7_MIN:        // UEB_T7_MIN

                pSconfig->ulValue = m_usT7MIN;

                break;

            case UEB_T7_MAX:        // UEB_T7_MAX

                pSconfig->ulValue = m_usT7MAX;

                break;

            case UEB_T9_MIN:        // UEB_T9_MIN

                pSconfig->ulValue = m_usT9MIN;

                break;

            case FIVE_BAUD_MOD:

                pSconfig->ulValue = m_usFiveBaudMod;

                break;

            case PARITY:

                pSconfig->ulValue = m_usParity;

                break;
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (IsUartBaudRateValid(ulValue) == J2534_STATUS_NOERROR)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J2818.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J2818 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    bool PeformIoctl;
    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        PeformIoctl = true;
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("J2818.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                enumJ2534Error = DataRate(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T0_MIN:        // UEB_T0_MIN

                enumJ2534Error = T0_Min(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T1_MAX:        // UEB_T1_MAX

                enumJ2534Error = T1_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T2_MAX:        // UEB_T2_MAX

                enumJ2534Error = T2_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T3_MAX:        // UEB_T3_MAX

                enumJ2534Error = T3_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T4_MIN:        // UEB_T4_MIN

                enumJ2534Error = T4_Min(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T5_MAX:        // UEB_T5_MAX

                enumJ2534Error = T5_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T6_MAX:        // UEB_T6_MAX

                enumJ2534Error = T6_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T7_MIN:        // UEB_T7_MIN

                enumJ2534Error = T7_Min(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T7_MAX:        // UEB_T7_MAX

                enumJ2534Error = T7_Max(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case UEB_T9_MIN:        // UEB_T9_MIN

                enumJ2534Error = T9_Min(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case FIVE_BAUD_MOD:

                enumJ2534Error = FiveBaudMod(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            case J1962_PINS:
                // continue to issue IOCTL command
                break;
 
            case PARITY:

                enumJ2534Error = Parity(pSconfig->ulValue);
                if(enumJ2534Error != J2534_STATUS_NOERROR)
                {
                    PeformIoctl = false;
                }
                break;

            default:
                return(J2534_ERR_NOT_SUPPORTED);

        }
        if (PeformIoctl)
        {
            enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL);
        }
        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            {
                // Write to Log File.
                WriteLogMsg("J2818.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }
        pSconfig++;
    }

    return(enumJ2534Error);
}


//-----------------------------------------------------------------------------
//  Function Name   : T0_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T0_Min
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T0_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT0MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T1_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T1_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T1_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT1MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T2_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T2_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T2_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT2MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T3_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T3_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T3_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFFFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT3MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T4_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T4_Min
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T4_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT4MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T5_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T5_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T5_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFFFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT5MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T6_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T6_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T6_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT6MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T7_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T7_Min
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T7_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT7MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T7_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T7_Max
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T7_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT7MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : T9_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the T9_Min
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::T9_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usT9MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}


//-----------------------------------------------------------------------------
//  Function Name   : CarbInit
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to initiate a carb initialization
//                    sequence from the pass-thru.
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::CarbInit(SBYTE_ARRAY *pInput, 
                               SBYTE_ARRAY *pOutput)
{
    J2534ERROR      enumJ2534Error;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    
    // Make sure pInput and pOutput are not NULL
    if (pInput == NULL || pOutput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    enumJ2534Error = J2534_STATUS_NOERROR;
//  pInput->pucBytePtr[0] = J2818_CARB_INIT_ADDR;
//  pOutput->ulNumOfBytes = 2;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : FastInit
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to initiate a fast initialization
//                    sequence from the pass-thru.
//-----------------------------------------------------------------------------
J2534ERROR CJ2818::FastInit(PASSTHRU_MSG *pInput, PASSTHRU_MSG *pOutput)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

//  // Make sure pInput is not NULL
//  if (pInput == NULL)
//      return(J2534_ERR_NULLPARAMETER);

//  iReturn=g_Unat.FastInit(pInput, pOutput);

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : Parity
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Parity
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::Parity(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < J2818_NO_PARITY) || (ulValue > J2818_EVEN_PARITY))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
    {
//      g_Unat.Parity(J2818, ulValue, m_ulFlag);
        m_usParity = (unsigned short) ulValue;
    }

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataBits
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the DataBits
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::DataBits(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < J2818_8_DATABITS) || (ulValue > J2818_7_DATABITS))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
    {
//      g_Unat.DataBits(J2818, ulValue, m_ulFlag);
        m_usDataBits = (unsigned short) ulValue;
    }

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : FiveBaudMod
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Five Baud Mod
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::FiveBaudMod(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < J2818_5_BAUD_REG) || (ulValue > J2818_5_BAUD_J2818))
        iReturn = J2534_ERR_INVALID_IOCTL_VALUE;
    else
        m_usFiveBaudMod = (unsigned short) ulValue;

    return(iReturn);
}

//-----------------------------------------------------------------------------
//  Function Name   : FastTimingSet
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the Fast Init Timings
//-----------------------------------------------------------------------------
J2534ERROR  CJ2818::FastTimingSet(unsigned long ulValue, unsigned char ucType)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    if (ucType == 1)
    {
        m_usTidle = (unsigned short) ulValue;
    }
    else if (ucType == 2)
    {
        m_usTinil = (unsigned short) ulValue;
    }
    else if (ucType == 4)
    {
        m_usTwup = (unsigned short) ulValue;
    }
//  FastInitTiming(ucType);

    return(iReturn);
}

#endif