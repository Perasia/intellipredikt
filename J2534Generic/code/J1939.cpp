/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1939.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support J1939 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "J1939.h"

//-----------------------------------------------------------------------------
//  Function Name   : CJ1939
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CJ1939 class
//-----------------------------------------------------------------------------
CJ1939::CJ1939(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog) : CProtocolBase(pclsDevice, pclsDebugLog, pclsDataLog)
{
    // Write to Log File.
    WriteLogMsg("J1939.cpp", "CJ1939()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    m_bLoopback = false;
    for (int i = 0; i < J1939_PROTECT_ADDRESS_MAX; i++)
    {
        m_ucProtectedJ1939Addr[i] = 0;
        m_bProtectedJ1939Addr[i] = false;
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "CJ1939()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ1939
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CJ1939 class
//-----------------------------------------------------------------------------
CJ1939::~CJ1939()
{
    // Write to Log File.
    WriteLogMsg("J1939.cpp", "~CJ1939()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "~CJ1939()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vConnect(J2534_PROTOCOL  enProtocolID,
                                unsigned long   ulFlags,
                                unsigned long   ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                                LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    m_ulCANHeaderSupport = ((ulFlags >> 11) & 0x01);
    m_ulCANIDtype = ((ulFlags >> 8) & 0x01);
    m_ulCANExtAddr = ((ulFlags >> 7) & 0x01);
    
    if (m_ulCANIDtype == 0) // Standard CAN
    {
        m_ulCANHeaderBits = J1939_HEADER_SIZE;
//      ulFlags = 0;
    }
    else  // Extended CAN
    {       
        m_ulCANHeaderBits = J1939_EXTENDED_HEADER_SIZE;
//      ulFlags = J2534_CONNECT_FLAGBIT_CAN29BIT;
    }

#if defined(J2534_DEVICE_NETBRIDGE) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305) || defined(J2534_DEVICE_DPA5) || defined(J2534_DEVICE_DPA50305)
    if(enProtocolID == J1939_PS ||
       enProtocolID == J1939_CH1 ||
       enProtocolID == J1939_CH2 ||
       enProtocolID == J1939_CH3 ||
       enProtocolID == J1939_CH4
       )
#else
    if(enProtocolID == J1939_PS ||
       enProtocolID == J1939_CH1 ||
       enProtocolID == J1939_CH2 )
#endif
    {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        if ((ulBaudRate != ISOCAN_DATA_RATE_LOW) && 
 #ifdef J2534_DEVICE_NETBRIDGE
                (ulBaudRate !=666666) && 
                (ulBaudRate !=666667) && 
 #endif
            (ulBaudRate != ISOCAN_DATA_RATE_DEFAULT) && ((ulBaudRate != ISOCAN_DATA_RATE_MEDIUM)))
        {
            return(J2534_ERR_INVALID_BAUDRATE);
        }
#endif
#ifdef J2534_0305
        ulBaudRate = ISOCAN_DATA_RATE_MEDIUM;
#endif
        m_ulDataRate = ulBaudRate;
        //m_enJ1939Protocol = J1939;
        m_enJ1939Protocol = enProtocolID;
        
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate,
                                                OnJ1939RxMessage, this))
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    m_ulT1 = 750; 
    m_ulT2 = 1250;
    m_ulT3 = 1250;
    m_ulT4 = 1050;
    m_ulBrdcst_min_delay = 50;  

    m_ulPPSS = 0;
    m_bJ1962Pins = false; 
    m_ulJ1939PPSS = 0;
    m_bJ1939Pins = false;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vReadMsgs(PASSTHRU_MSG   *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Pins
    if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }
/*
    if (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
        return(J2534_ERR_NO_FLOW_CONTROL);
    }
*/  // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
/*      if (enJ2534Error == J2534_ERR_BUFFER_EMPTY)
        {
            if (((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
            {
                // Write to Log File.
                WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
                return(J2534_ERR_NO_FLOW_CONTROL);
            }
        }
*/      return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vWriteMsgs(PASSTHRU_MSG  *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1, i;
    bool bIsAddressClaimed;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    bIsAddressClaimed = false;

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1939Protocol)
        {
            // Write to Log File.
            WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }
        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }

        // Long message
        if ((pstPassThruMsg + ulIdx1)->ulDataSize > 13)
        {
            // Check Source Address.
            for (i = 0; i < J1939_PROTECT_ADDRESS_MAX; i++)
            {
                if (m_bProtectedJ1939Addr[i] == true)
                {
                    if (m_ucProtectedJ1939Addr[i] == 
                        (pstPassThruMsg + ulIdx1)->ucData[3])
                    {
                        bIsAddressClaimed = true;
                        break;
                    }
                }
            }

            if (!bIsAddressClaimed)
            {
                // Write to Log File.
                WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_ADDRESS_NOT_CLAIMED);
                return(J2534_ERR_ADDRESS_NOT_CLAIMED);
            }
        }
/*
        if (((pstPassThruMsg + ulIdx1)->ulDataSize > J1939_MSG_SIZE_MAX_SF) && 
            ((m_pclsFilterMsg == NULL) || (!m_pclsFilterMsg->IsFlowControlFilterSet())))
        {
            // Write to Log File.
            WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_FLOW_CONTROL);
            return(J2534_ERR_NO_FLOW_CONTROL);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
#ifdef J2534_0305
        if (m_ulCANExtAddr)
        {
            (pstPassThruMsg + ulIdx1)->ulTxFlags |= 0x80;
        }
#endif*/
    }
    
    // Check Pins
    if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vStartPeriodicMsg(PASSTHRU_MSG   *pstPassThruMsg,
                                        unsigned long   *pulMsgID,
                                        unsigned long   ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enJ1939Protocol)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
    
    if ((pstPassThruMsg->ulDataSize > J1939_MSG_SIZE_MAX_SF))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Check Pins
    if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

#ifdef J2534_0305
    if (m_ulCANExtAddr)
    {
        pstPassThruMsg->ulTxFlags |= 0x80;
    }
#endif
    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1939::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::vStartMsgFilter(J2534_FILTER    enumFilterType,
                                       PASSTHRU_MSG     *pstMask,
                                       PASSTHRU_MSG     *pstPattern,
                                       PASSTHRU_MSG     *pstFlowControl,
                                       unsigned long    *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetLastErrorText("J1939 : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enJ1939Protocol) ||
        (pstPattern->ulProtocolID != m_enJ1939Protocol))// || 
//      (pstFlowControl->ulProtocolID != m_enJ1939Protocol))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
#endif
#ifdef J2534_0305
    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enJ1939Protocol) ||
        (pstPattern->ulProtocolID != m_enJ1939Protocol))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
#endif  // Check Filter Type.

    // Check Pins
    if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // NOTE : In J1939, if pstFlowControl is not NULL then check for  
    //        J2534_ERR_MSG_PROTOCOL_ID.

    // NOTE : In J1939, pass the pstFlowControl through 
    //        IsMsgValid().
    // Check if msg. format is valid.
    if (!IsMsgValid(pstPattern, true))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    if (!IsMsgValid(pstMask, true))
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // NOTE : In J1939, check to see that the mask of 4 or 5 bytes are
    //        all 0xFF. The size of pstMask, pstPattern and pstFlowControl
    //        should be 4 or 5 bytes depanding on extended addressing or not
    //        respectively.

    // NOTE : In J1939, if the filter type is PASS or BLOCK return error.
    //        In other words only allow FLOW_CONTROL filter type.

#ifdef J2534_0305
    if (m_ulCANExtAddr)
    {
        pstMask->ulTxFlags |= 0x80;
        pstPattern->ulTxFlags |= 0x80;
        pstFlowControl->ulTxFlags |= 0x80;
    }
#endif
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::vIoctl(J2534IOCTLID enumIoctlID,
                              void *pInput,
                              void *pOutput)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;
    int i;
    PASSTHRU_MSG stPassThruMsg;  
    unsigned long ulGetTimestamp;
    
    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
    // IoctlID values
    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration
        
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;
        
    case SET_CONFIG:            // Set configuration
        
        enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);

        break;
        
    case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue
        
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsTxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
        
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsRxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsPeriodicMsg != NULL)
        {
            delete m_pclsPeriodicMsg;
            m_pclsPeriodicMsg = NULL;
        }
        
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsFilterMsg != NULL)
        {
            delete m_pclsFilterMsg;
            m_pclsFilterMsg = NULL;
        }
        
        break;

    case PROTECT_J1939_ADDR:                // Fast initialization

        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            // Make sure pInput is not NULL
            if (pInput == NULL)
                return(J2534_ERR_NULLPARAMETER);

            //enumJ2534Error = FastInit();
            if ((((SBYTE_ARRAY *)pInput)->pucBytePtr[0] == 254) ||
                (((SBYTE_ARRAY *)pInput)->pucBytePtr[0] == 255))
            {
                // Write to Log File.
                WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_IOCTL_VALUE);
                return(J2534_ERR_INVALID_IOCTL_VALUE);
            }

            /*Check if the IOCTL value is not in range */
//          if(enumJ2534Error!= J2534_STATUS_NOERROR)
//              return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        pOutput))                                                       
            {
                // Write to Log File.
                WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                memset(&stPassThruMsg, 0, sizeof(PASSTHRU_MSG));
                stPassThruMsg.ulProtocolID = J1939_PS;
                stPassThruMsg.ulDataSize = 1;
                stPassThruMsg.ucData[0] = ((SBYTE_ARRAY *)pInput)->pucBytePtr[0];
                stPassThruMsg.ulExtraDataIndex = 0;
                stPassThruMsg.ulRxStatus = 0x20000;     
                if (CProtocolBase::vIoctl(GET_TIMESTAMP, NULL, &ulGetTimestamp) == J2534_STATUS_NOERROR)
                {
                    stPassThruMsg.ulTimeStamp = ulGetTimestamp * 1000;
                }
                OnJ1939RxMessage(&stPassThruMsg, this);
                return(enumJ2534Error);
            }

            if (memcmp(&((SBYTE_ARRAY *)pInput)->pucBytePtr[1], 
                "\x00\x00\x00\x00\x00\x00\x00\x00", 8) == 0)
            {
                // Cancel a protected address
                for (i = 0; i < J1939_PROTECT_ADDRESS_MAX; i++)
                {
                    if (m_bProtectedJ1939Addr[i] == true)
                    {
                        if (m_ucProtectedJ1939Addr[i] == 
                            ((SBYTE_ARRAY *)pInput)->pucBytePtr[0])
                        {
                            m_bProtectedJ1939Addr[i] = false;
                            // Write to Log File.
                            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Canceled address 0x%02X", m_ucProtectedJ1939Addr[i]);
                            break;
                        }
                    }
                }
            }
            else
            {
                // Claim a protected address
                for (i = 0; i < J1939_PROTECT_ADDRESS_MAX; i++)
                {
                    if (m_bProtectedJ1939Addr[i] == false)
                    {
                        m_ucProtectedJ1939Addr[i] = 
                            ((SBYTE_ARRAY *)pInput)->pucBytePtr[0];
                        m_bProtectedJ1939Addr[i] = true;
                        memset(&stPassThruMsg, 0, sizeof(PASSTHRU_MSG));
                        stPassThruMsg.ulProtocolID = J1939_PS;
                        stPassThruMsg.ulDataSize = 1;
                        stPassThruMsg.ucData[0] = ((SBYTE_ARRAY *)pInput)->pucBytePtr[0];
                        stPassThruMsg.ulExtraDataIndex = 0;
                        stPassThruMsg.ulRxStatus = 0x10000;      
                        if (CProtocolBase::vIoctl(GET_TIMESTAMP, NULL, &ulGetTimestamp) == J2534_STATUS_NOERROR)
                        {
                            stPassThruMsg.ulTimeStamp = ulGetTimestamp * 1000;
                        }
                        OnJ1939RxMessage(&stPassThruMsg, this);
                        break;
                    }
                }
            }

            break;
#ifdef J2534_DEVICE_NETBRIDGE
	case CAN_SET_INTERNAL_TERMINATION:
        {
        // Check Pins
        if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            enumJ2534Error = SetInternalTermination((unsigned char *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
#endif        
    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }
    
/*  // Write to Log File.
    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");
*/  
    
    // Write to Log File.
    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    WriteLogMsg("J1939.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnJ1939Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    J1939 messages.
//-----------------------------------------------------------------------------
void OnJ1939RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                         LPVOID pVoid)
{
    CJ1939                  *pclsJ1939;
    FILTERMSG_CONFORM_REQ   stConformReq;
    char    szBuffer[J1939_ERROR_TEXT_SIZE];    
    unsigned long   i, j;     
    char            szChannelPins[MAX_PATH];
    char            szProtocol[MAX_PATH];
    unsigned long   ulRxTx;
    unsigned long   ulTimestamp;
    char            szID[MAX_PATH];
    char            szData[MAX_PATH];

    pclsJ1939 = (CJ1939 *) pVoid;

    // Check for NULL pointer.
    if (pclsJ1939 == NULL)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        }
        return;
    }                   

    memset(szChannelPins,0,MAX_PATH);
    memset(szProtocol,0,MAX_PATH);
    ulRxTx = 0;
    ulTimestamp = 0;
    memset(szID,0,MAX_PATH);
    memset(szData,0,MAX_PATH);

    // Data Logging Protocol and its Channel/Pins
    switch(pstPassThruMsg->ulProtocolID)
    {
    case J1939_PS:
        if (pclsJ1939->m_bJ1962Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1962 Pins 0x%04X", pclsJ1939->m_ulPPSS);
        }
        if (pclsJ1939->m_bJ1939Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1939 Pins 0x%04X", pclsJ1939->m_ulJ1939PPSS);
        }
        sprintf_s(szProtocol,"DWCAN");
        break;
    case J1939_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"DWCAN");
        break;
    case J1939_CH2:
        sprintf_s(szChannelPins,"Channel 2");
        sprintf_s(szProtocol,"DWCAN");
        break;
    case J1939_CH3:
        sprintf_s(szChannelPins,"Channel 3");
        sprintf_s(szProtocol,"DWCAN");
        break;
    case J1939_CH4:
        sprintf_s(szChannelPins,"Channel 4");
        sprintf_s(szProtocol,"DWCAN");
        break;      
    default:
        break;
    }

    // Data Logging Rx/Tx
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        ulRxTx = DATALOG_TX;
    }
    else
    {
        ulRxTx = DATALOG_RX;
    }

    // Data Logging timestamp
    ulTimestamp = pstPassThruMsg->ulTimeStamp;

    // Data Logging ID
    if(pstPassThruMsg->ulDataSize > 0)
    {
        j = 0;

        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == J2534_RX_FLAGBIT_CAN29BIT)
        {
            for(i = 0; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }
        else
        {
            for(i = 2; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }

        j = 0;

        if (pstPassThruMsg->ulDataSize <= 13)
        {
            for(i = 5; i < pstPassThruMsg->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }

            if(i >= 15)
                j += sprintf_s(szData + j, sizeof(szData)-j, "...");
        }
        else
        {
            sprintf_s(szProtocol,"J1939");
            for(i = 4; i < pstPassThruMsg->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }

            if(i >= 15)
                j += sprintf_s(szData + j, sizeof(szData)-j, "...");
        }
    }

    if (((pstPassThruMsg->ulRxStatus & 0x10000) == 0) &&
        ((pstPassThruMsg->ulRxStatus & 0x20000) == 0))
    {
        // Write to Data Log File.
        CProtocolBase::WriteDataLogMsg(szChannelPins, szProtocol, ulRxTx, ulTimestamp, szID, szData);
    }

    // Check if this is a J1939 Address Lost Indication.
    if (pstPassThruMsg->ulRxStatus & 0x20000)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            sprintf_s(szBuffer, "Data0 0x%X", pstPassThruMsg->ucData[0]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "Address Lost");
        }

        // Enqueue to Circ Buffer.
        pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if this is a J1939 Address Claimed Indication.
    if (pstPassThruMsg->ulRxStatus & 0x10000)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            sprintf_s(szBuffer, "Data0 0x%X", pstPassThruMsg->ucData[0]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "Address Claimed");
        }

        // Enqueue to Circ Buffer.
        pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if the message is valid as per the Connect Flag is set.
    if ((pclsJ1939->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (pclsJ1939->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return;
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return;
            }
        }
    }
/*
    unsigned long ulDataSize;
    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & (0x08 | J2534_RX_FLAGBIT_MSGTYPE))
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "Tx Done CALLBACK");
        }

//      if (pclsJ1939->m_bLoopback == false)
//          return;

        ulDataSize = pstPassThruMsg->ulDataSize;

        if (pstPassThruMsg->ulTxFlags & 0x80)
            pstPassThruMsg->ulDataSize = 5;
        else
            pstPassThruMsg->ulDataSize = 4;

        pstPassThruMsg->ulExtraDataIndex = 0;//pstPassThruMsg->ulDataSize;

        // Write status to Log File.
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            sprintf_s(szBuffer, "pstPassThruMsg->ulTxFlags 0x%X", pstPassThruMsg->ulTxFlags);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            sprintf_s(szBuffer, "pstPassThruMsg->ulDataSize 0x%X", pstPassThruMsg->ulDataSize);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
/*          sprintf_s(szBuffer, "Data0 0x%X", pstPassThruMsg->ucData[0]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            sprintf_s(szBuffer, "Data1 0x%X", pstPassThruMsg->ucData[1]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            sprintf_s(szBuffer, "Data2 0x%X", pstPassThruMsg->ucData[2]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
            sprintf_s(szBuffer, "Data3 0x%X", pstPassThruMsg->ucData[3]);
            CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
                szBuffer);
*/  /*  }
        // Enqueue to Circ Buffer.
        pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        pstPassThruMsg->ulRxStatus &= (0xFF - 0x08);
    }*/
    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                             DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        }

        if (pclsJ1939->m_bLoopback == false)
            return;

        pstPassThruMsg->ulDataSize = pstPassThruMsg->ulDataSize;
        pstPassThruMsg->ulExtraDataIndex = pstPassThruMsg->ulDataSize;

        // Enqueue to Circ Buffer.
        pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }
    
    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
    {
        CProtocolBase::m_pclsLog->Write("CJ1939.cpp", "OnJ1939Rx()", 
                         DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
    }
    // Apply Filters and see if msg. is required.
    if (pclsJ1939->m_pclsFilterMsg != NULL)
    {
/*      sprintf_s(szBuffer, "Data0 0x%X", pstPassThruMsg->ucData[0]);
        CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
            szBuffer);
        sprintf_s(szBuffer, "Data1 0x%X", pstPassThruMsg->ucData[1]);
        CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
            szBuffer);
        sprintf_s(szBuffer, "Data2 0x%X", pstPassThruMsg->ucData[2]);
        CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
            szBuffer);
        sprintf_s(szBuffer, "Data3 0x%X", pstPassThruMsg->ucData[3]);
        CProtocolBase::m_pclsLog->Write("J1939.cpp", "OnJ1939Rx()", DEBUGLOG_TYPE_COMMENT, 
            szBuffer);
*/      stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsJ1939->IsMsgValidRx(pstPassThruMsg) &&
            pclsJ1939->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#ifdef J2534_0305
    else
    {
        if (pclsJ1939->IsMsgValidRx(pstPassThruMsg))
        {
            // Enqueue to Circ Buffer.
            pclsJ1939->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#endif

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1939::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    if ((pstPassThruMsg->ulDataSize < J1939_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1939_MSG_SIZE_MAX))
    {
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}             

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1939::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J1939_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1939_MSG_SIZE_MAX))
    {
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1939.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1939 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                pSconfig->ulValue = (unsigned long) m_fSamplePoint;

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                pSconfig->ulValue = (unsigned long) m_fJumpWidth;

                break;      
                
            case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS;

                if (m_enJ1939Protocol != J1939_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;     
                
            case J1939_PINS:

                pSconfig->ulValue = m_ulJ1939PPSS;

                if (m_enJ1939Protocol != J1939_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case J1939_T1:      // T1

                pSconfig->ulValue = m_ulT1;

                break;

            case J1939_T2:      // T2

                pSconfig->ulValue = m_ulT2;

                break;

            case J1939_T3:      // T3

                pSconfig->ulValue = m_ulT3;

                break;

            case J1939_T4:      // T4

                pSconfig->ulValue = m_ulT4;

                break;

            case J1939_BRDCST_MIN_DELAY:        // BRDCST_MIN_DELAY

                pSconfig->ulValue = m_ulBrdcst_min_delay;

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;
    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1939.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1939 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Parameter is 0x%02X", pSconfig->Parameter);
        WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = DataRate(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback
    
                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Loopback(pSconfig->ulValue);
                }

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                    
                //enumJ2534Error = SamplePoint(pSconfig->ulValue);

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                    
                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;
            
            case J1939_T1:      // T1

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T1(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case J1939_T2:      // T2

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T2(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
            case J1939_T3:      // T3

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T3(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
            case J1939_T4:      // T4

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T4(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
            case J1939_BRDCST_MIN_DELAY:        // BRDCST_MIN_DELAY

                if((m_enJ1939Protocol == J1939_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Brdcst_min_delay(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;
            case J1962_PINS:
                if (m_enJ1939Protocol != J1939_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1962Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulPPSS = pSconfig->ulValue;
                    else
                        m_bJ1962Pins = false;
                }
                break;                                   

            case J1939_PINS:
                if (m_enJ1939Protocol != J1939_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1939Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("J1939.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulJ1939PPSS = pSconfig->ulValue;
                    else
                        m_bJ1939Pins = false;
                }
                break;
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate for a J1939 channel.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    if ((ulValue < 5) || (ulValue > 1000000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Set DataRate to # of kbps
    if (//(ulValue == ISOCAN_DATA_RATE_LOW) || 
#ifdef J2534_DEVICE_NETBRIDGE
        (ulValue ==666666) || 
        (ulValue ==666667) || 
#endif
        (ulValue == ISOCAN_DATA_RATE_MEDIUM) /*|| 
        (ulValue == ISOCAN_DATA_RATE_DEFAULT)*/)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

J2534ERROR  CJ1939::T1(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulT1 = ulValue;

    return(iReturn);
}

J2534ERROR  CJ1939::T2(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulT2 = ulValue;

    return(iReturn);
}

J2534ERROR  CJ1939::T3(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulT3 = ulValue;

    return(iReturn);
}

J2534ERROR  CJ1939::T4(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulT4 = ulValue;

    return(iReturn);
}

J2534ERROR  CJ1939::Brdcst_min_delay(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulBrdcst_min_delay = ulValue;

    return(iReturn);
} 

//-----------------------------------------------------------------------------
//  Function Name   : SetInternalTermination
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set internal termination.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1939::SetInternalTermination(unsigned char *pucValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
 	// Make sure pucValue is not NULL
    if (pucValue == NULL)
        return(J2534_ERR_NULLPARAMETER);

	//Valid values for <parameter> are 0-1
    if(*pucValue < 0 || *pucValue > 1)
    {
        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
        return(enumJ2534Error);
    }

    return(enumJ2534Error);
}

J2534ERROR  CJ1939::J1962Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x1000) || ((ulValue & 0x00FF) > 0x0010))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 4) || (ulValue == 4) ||
        ((ulValue >> 8) == 5) || (ulValue == 5) ||
        ((ulValue >> 8) == 16) || (ulValue == 16))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x060E) || 
        (ulValue == 0x030B) || 
        (ulValue == 0x0C0D))
    {
        m_bJ1962Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}       

J2534ERROR  CJ1939::J1939Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x0900) || ((ulValue & 0x00FF) > 0x0009))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 1) || (ulValue == 1) ||
        ((ulValue >> 8) == 2) || (ulValue == 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0304) || 
        (ulValue == 0x0809))
    {
        m_bJ1939Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}