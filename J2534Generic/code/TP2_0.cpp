/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: TP2_0.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support TP2_0 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#define _TP2_0_
#include "TP2_0.h"

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
//-----------------------------------------------------------------------------
//  Function Name   : CTP2_0
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CTP2_0 class
//-----------------------------------------------------------------------------
CTP2_0::CTP2_0(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog, CDataLog * pclsDataLog) :CProtocolBase(pclsDevice, pclsDebugLog, pclsDataLog)
{
    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "CTP2_0()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "CTP2_0()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CTP2_0
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CTP2_0 class
//-----------------------------------------------------------------------------
CTP2_0::~CTP2_0()
{
    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "~CTP2_0()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "~CTP2_0()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vConnect(J2534_PROTOCOL    enProtocolID,
                        unsigned long   ulFlags,
                        unsigned long   ulBaudRate,
                        DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                        LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    m_ulCANHeaderSupport = ((ulFlags >> 11) & 0x01);
    m_ulTP2_0IDtype = ((ulFlags >> 8) & 0x01);
    
    if (m_ulTP2_0IDtype == 0) // Standard TP2_0
    {
        m_ulCANHeaderBits = TP2_0_HEADER_SIZE;
//      ulFlags = 0;
    }
    else  // Extended TP2_0
    {       
        m_ulCANHeaderBits = TP2_0_EXTENDED_HEADER_SIZE;
//      ulFlags = J2534_CONNECT_FLAGBIT_TP2_029BIT;
    }

    if (!m_ulCANDataRateDefault)    // Over ride NOT specified in registry
    {
        m_ulCANDataRateDefault = TP2_0_DATA_RATE_DEFAULT;
    }

    switch(enProtocolID)
    {
    case TP2_0_PS:
    case TP2_0_CH1:
    case TP2_0_CH2:
 #if defined(J2534_DEVICE_NETBRIDGE) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305) || defined(J2534_DEVICE_DPA5) || defined(J2534_DEVICE_DPA50305)
    case TP2_0_CH3:
    case TP2_0_CH4:
 #endif

        {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            if (ulBaudRate != TP2_0_DATA_RATE_DEFAULT &&
                ulBaudRate != TP2_0_DATA_RATE_MEDIUM)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
#ifdef J2534_0305
            ulBaudRate = m_ulCANDataRateDefault;
            if (ulBaudRate != TP2_0_DATA_RATE_DEFAULT &&
                ulBaudRate != TP2_0_DATA_RATE_MEDIUM)
            {
                WriteLogMsg("TP2_0.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "ulBaudRate=%d", ulBaudRate);
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
            m_ulDataRate = ulBaudRate;
            m_enTP2_0Protocol = enProtocolID;
        }
        break;
    default:
        break;
    }

    // Write to Log File.
    WriteLogMsg("ISO15765.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "enProtocolID=%d, ulFlags=%0X, ulBaudRate=%d", enProtocolID, ulFlags, ulBaudRate);
    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate,
                                                OnTP2_0RxMessage, this))
                      != J2534_STATUS_NOERROR)
    {
        // Write status to Log File.
        WriteLogMsg("TP2_0.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    m_ulPPSS = 0;
    m_bJ1962Pins = false;
    m_ulJ1939PPSS = 0;
    m_bJ1939Pins = false;

    m_ulTP2_0_T_BR_INT = TP2_0_T_BR_INT_DEFAULT;
	m_ulTP2_0_T_E = TP2_0_T_E_DEFAULT; 
	m_ulTP2_0_MNTC = TP2_0_MNTC_DEFAULT;
	m_ulTP2_0_T_CTA = TP2_0_T_CTA_DEFAULT;
	m_ulTP2_0_MNCT = TP2_0_MNCT_DEFAULT;
	m_ulTP2_0_MNTB = TP2_0_MNTB_DEFAULT; 
	m_ulTP2_0_MNT = TP2_0_MNT_DEFAULT;
	m_ulTP2_0_T_WAIT = TP2_0_T_WAIT_DEFAULT;
	m_ulTP2_0_T1 = TP2_0_T1_DEFAULT;
	m_ulTP2_0_T3 = TP2_0_T3_DEFAULT;
	m_ulTP2_0_IDENTIFIER = TP2_0_IDENTIFIER_DEFAULT; 
	m_ulTP2_0_RXIDPASSIVE = TP2_0_RXIDPASSIVE_DEFAULT;

    memset(m_stImplicitPassFilter, 0, sizeof(m_stImplicitPassFilter));

    // Write status to Log File.
    WriteLogMsg("TP2_0.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vReadMsgs(PASSTHRU_MSG     *pstPassThruMsg,
                                                                    unsigned long   *pulNumMsgs,
                                                                     unsigned long  ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Pins
    if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("TP2_0.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write status to Log File.
    WriteLogMsg("TP2_0.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                            unsigned long   *pulNumMsgs,
                            unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;
    int             i;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enTP2_0Protocol)
        {
            // Write to Log File.
            WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }

        if ((pstPassThruMsg + ulIdx1)->ulDataSize > TP2_0_MSG_SIZE_SHORT_MAX)
        {
            // Write to Log File.
            WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Long Message");
            for (i = 0; i < TP2_0_IMPLICIT_PASS_FILTER_MAX; i++)
            {
                if (m_stImplicitPassFilter[i].ulFilterID != -1)
                {
                    if (m_stImplicitPassFilter[i].bActive == false)
                    {
                        if (memcmp(m_stImplicitPassFilter[i].ucTxCANID, (pstPassThruMsg + ulIdx1)->ucData, 
                            sizeof(m_stImplicitPassFilter[i].ucTxCANID)) == 0)
                        {
                            WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "found established connection address");
                            break;
                        }
                    }
                    else
                    {
                        if (memcmp(m_stImplicitPassFilter[i].ucRxCANID, (pstPassThruMsg + ulIdx1)->ucData, 
                            sizeof(m_stImplicitPassFilter[i].ucRxCANID)) == 0)
                        {
                            WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "found established connection address");
                            break;
                        }
                    }
                }
            }

            // Can't find any established connection address
            if (i == TP2_0_IMPLICIT_PASS_FILTER_MAX)
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NO_CONNECTION_ESTABLISHED);
                return(J2534_ERR_NO_CONNECTION_ESTABLISHED);
            }
        }
    }

    // Check Pins
    if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
	)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write status to Log File.
    WriteLogMsg("TP2_0.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vStartPeriodicMsg(PASSTHRU_MSG     *pstPassThruMsg,
                                   unsigned long    *pulMsgID,
                                   unsigned long    ulTimeInterval
	)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enTP2_0Protocol)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Pins
    if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    if ((pstPassThruMsg->ulDataSize > TP2_0_MSG_SIZE_SHORT_MAX))
	{
		// Write to Log File.
		WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
		return(J2534_ERR_INVALID_MSG);
	}

    if ((pstPassThruMsg->ulTxFlags & J2534_TX_TP2_0_BROADCAST_MSG) != J2534_TX_TP2_0_BROADCAST_MSG)
    {
	    WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CTP2_0::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::vStartMsgFilter(J2534_FILTER      enumFilterType,
                                                                            PASSTHRU_MSG        *pstMask,
                                                                            PASSTHRU_MSG        *pstPattern,
                                                                            PASSTHRU_MSG        *pstFlowControl,
                                                                            unsigned long       *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device TP2_0not set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enTP2_0Protocol) ||
        (pstPattern->ulProtocolID != m_enTP2_0Protocol))
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);

    }

    // Check Pins
    if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
    {
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
        return(J2534_ERR_PIN_INVALID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("TP2_0 : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }

    // NOTE : In ISO15765, if pstFlowControl is not NULL then check for  
    //        J2534_ERR_MSG_PROTOCOL_ID.

    // NOTE : In ISO15765, pass the pstFlowControl through 
    //        IsMsgValid().
#ifdef UD_TRUCK
    // Fix up for UDTruck application 
    if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_TP2_029BIT)
    {
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Fix up ulTxFlags J2534_CONNECT_FLAGBIT_TP2_029BIT for UDTruck application");
        pstMask->ulTxFlags |= J2534_CONNECT_FLAGBIT_TP2_029BIT;
        pstPattern->ulTxFlags |= J2534_CONNECT_FLAGBIT_TP2_029BIT;
    }
#endif
    // Check if msg. format is valid.
    if (!IsMsgValid(pstPattern, true))
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "IsMsgValid Pattern test returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);

    }

    if (!IsMsgValid(pstMask, true))
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "IsMsgValid Mask test returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);

    }

    // NOTE : In ISO15765, check to see that the mask of 4 or 5 bytes are
    //        all 0xFF. The size of pstMask, pstPattern and pstFlowControl
    //        should be 4 or 5 bytes depanding on extended addressing or not
    //        respectively.

    // NOTE : In ISO15765, if the filter type is PASS or BLOCK return error.
    //        In other words only allow FLOW_CONTROL filter type.

    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);

    }

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);

    }

    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::vIoctl(J2534IOCTLID enumIoctlID,
                         void *pInput,
                         void *pOutput)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    // Write to Log File.
    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    // IoctlID values
    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration
 
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;
        
    case SET_CONFIG:            // Set configuration
        
        enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
        
        break;
        
    case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue
        
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsTxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue

        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        m_pclsRxCircBuffer->ClearBuffer();
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsPeriodicMsg != NULL)
        {
            delete m_pclsPeriodicMsg;
            m_pclsPeriodicMsg = NULL;
        }
        
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
        if (m_pclsFilterMsg != NULL)
        {
            delete m_pclsFilterMsg;
            m_pclsFilterMsg = NULL;
        }
        
        break;

    case READ_MEMORY:
        {
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }
        }
        break;

    case WRITE_MEMORY:
        {
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;

	case CAN_SET_BTR:
        {
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
	case CAN_SET_INTERNAL_TERMINATION:
        {
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            enumJ2534Error = SetInternalTermination((unsigned char *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;

#ifdef J2534_DEVICE_DBRIDGE
	case START_REPEAT_MESSAGE:
	case QUERY_REPEAT_MESSAGE:
	case STOP_REPEAT_MESSAGE:
        {
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            if(enumIoctlID == START_REPEAT_MESSAGE)
            {
                // Check if msg. format is valid.
                REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
                if ((!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]))) ||
                    (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]))))
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
                    return(J2534_ERR_INVALID_MSG);
                }
            }
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID, pInput, pOutput))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }   
        }
        break;
#endif
    case REQUEST_CONNECTION:      
 
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            enumJ2534Error = RequestConnection((SBYTE_ARRAY *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }
        //enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;

    case TEARDOWN_CONNECTION:
 
        // Check Pins
        if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
        {
            WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_INVALID);
            return(J2534_ERR_PIN_INVALID);
        }
            enumJ2534Error = TeardownConnection((SBYTE_ARRAY *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);   
            /*if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);

            }*/
        //enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
        
        break;

    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }
    
    if (enumJ2534Error != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
    }
    else
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
    }
    
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnTP2_0Rx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    TP2_0 messages.
//-----------------------------------------------------------------------------
void OnTP2_0RxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CTP2_0                    *pclsTP2_0;
    FILTERMSG_CONFORM_REQ   stConformReq;
    unsigned long   i, j;
    char            szChannelPins[MAX_PATH];
    char            szProtocol[MAX_PATH];
    unsigned long   ulRxTx;
    unsigned long   ulTimestamp;
    char            szID[MAX_PATH];
    char            szData[MAX_PATH];

    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;
    PASSTHRU_MSG stMask;
    PASSTHRU_MSG stPattern;
    unsigned long ulFilterID;

    pclsTP2_0 = (CTP2_0 *) pVoid;

    // Check for NULL pointer.
    if (pclsTP2_0 == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    memset(szChannelPins,0,MAX_PATH);
    memset(szProtocol,0,MAX_PATH);
    ulRxTx = 0;
    ulTimestamp = 0;
    memset(szID,0,MAX_PATH);
    memset(szData,0,MAX_PATH);

    // Data Logging Protocol and its Channel/Pins
    switch(pstPassThruMsg->ulProtocolID)
    {
    case TP2_0_PS:
        if (pclsTP2_0->m_bJ1962Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1962 Pins 0x%04X", pclsTP2_0->m_ulPPSS);
        }
        if (pclsTP2_0->m_bJ1939Pins)
        {
            sprintf_s(szChannelPins, sizeof(szChannelPins)-1, "J1939 Pins 0x%04X", pclsTP2_0->m_ulJ1939PPSS);
        }
        sprintf_s(szProtocol,"TP2_0");
        break;
    case TP2_0_CH1:
        sprintf_s(szChannelPins,"Channel 1");
        sprintf_s(szProtocol,"TP2_0");
        break;
    case TP2_0_CH2:
        sprintf_s(szChannelPins,"Channel 2");
        sprintf_s(szProtocol,"TP2_0");
        break;
    case TP2_0_CH3:
        sprintf_s(szChannelPins,"Channel 3");
        sprintf_s(szProtocol,"TP2_0");
        break;
    case TP2_0_CH4:
        sprintf_s(szChannelPins,"Channel 4");
        sprintf_s(szProtocol,"TP2_0");
        break;
    default:
        break;
    }

    // Data Logging Rx/Tx
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        ulRxTx = DATALOG_TX;
    }
    else
    {
        ulRxTx = DATALOG_RX;
    }

    // Data Logging timestamp
    ulTimestamp = pstPassThruMsg->ulTimeStamp;

    // Data Logging ID
    if(pstPassThruMsg->ulDataSize > 0)
    {
        j = 0;

        if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == J2534_RX_FLAGBIT_CAN29BIT)
        {
            for(i = 0; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }
        else
        {
            for(i = 2; i < pstPassThruMsg->ulDataSize && i < 4; i++)
            {
                j += sprintf_s(szID + j, sizeof(szID)-j, "%02X ",pstPassThruMsg->ucData[i]);                
            }
        }

        j = 0;

        for(i = 4; i < pstPassThruMsg->ulDataSize && i < 15; i++)
        {
            j += sprintf_s(szData + j, sizeof(szData)-j, "%02X ",pstPassThruMsg->ucData[i]);                
        }

        if(i >= 15)
            j += sprintf_s(szData + j, sizeof(szData)-j, "...");
    }

    if (((pstPassThruMsg->ulRxStatus & (1 << 16)) == 0) &&
        ((pstPassThruMsg->ulRxStatus & (1 << 17)) == 0))
    {
        // Write to Data Log File.
        CProtocolBase::WriteDataLogMsg(szChannelPins, szProtocol, ulRxTx, ulTimestamp, szID, szData);
    }

    // Check if the message is valid as per the Connect Flag is set.
    if ((pclsTP2_0->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (pclsTP2_0->m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                return;
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                return;
            }
        }
    }

    // Check if this is a Connection Established indication.
    if (pstPassThruMsg->ulRxStatus & (1 << 16))
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Connection Established indication");

        memset(&stMask, 0, sizeof(stMask));
        memset(&stPattern, 0, sizeof(stPattern));
        stMask.ulDataSize = 4;
        stMask.ulProtocolID = pclsTP2_0->m_enTP2_0Protocol;
        memcpy(stMask.ucData, "\x00\x00\x07\xFF", stMask.ulDataSize);
        stPattern.ulDataSize = 4;
        stPattern.ulProtocolID = pclsTP2_0->m_enTP2_0Protocol;
        memcpy(stPattern.ucData, pstPassThruMsg->ucData, stPattern.ulDataSize);
#ifdef J2534_DEVICE_SVCI1
        // StartMsgFilter using generic routine from base.
        if ((enumJ2534Error = pclsTP2_0->CProtocolBase::vStartMsgFilter(
                                              J2534_FILTER_PASS,
                                              &stMask,
                                              &stPattern,
                                              NULL,
                                              &ulFilterID,
                                              true)) 
                          != J2534_STATUS_NOERROR)
        {
            // Write to Log File.
            CProtocolBase::WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
            //return(enumJ2534Error);
            pstPassThruMsg->ulRxStatus &= 0xFFFEFFFF;
            pstPassThruMsg->ulRxStatus |= (1 << 17);
        }
        else
        {
            for (i = 0; i < TP2_0_IMPLICIT_PASS_FILTER_MAX; i++)
            {
                if (pclsTP2_0->m_stImplicitPassFilter[i].ulFilterID == -1)
                {
                    pclsTP2_0->m_stImplicitPassFilter[i].ulFilterID = ulFilterID;
                    memcpy(pclsTP2_0->m_stImplicitPassFilter[i].ucRxCANID, stPattern.ucData, 
                        sizeof(pclsTP2_0->m_stImplicitPassFilter[i].ucRxCANID));
                    memcpy(pclsTP2_0->m_stImplicitPassFilter[i].ucTxCANID, stPattern.ucData+4, 
                        sizeof(pclsTP2_0->m_stImplicitPassFilter[i].ucTxCANID));
                    break;
                }
            }
            // Enqueue to Circ Buffer.
            pclsTP2_0->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                        (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                        pstPassThruMsg->ulDataSize));
            return;
        }
#endif
    }

    // Check if this is a Connection Lost indication.
    if (pstPassThruMsg->ulRxStatus & (1 << 17))
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Connection Lost indication");

        // Enqueue to Circ Buffer.
        pclsTP2_0->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsTP2_0->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsTP2_0->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }
    // Write to Log File.
    CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
#ifdef J2534_DEVICE_DBRIDGE
    // Repeat msg support
    if(pclsTP2_0->m_pclsRepeatMsg != NULL)
    {
        pclsTP2_0->m_pclsRepeatMsg->CompareMsg(pstPassThruMsg);
    }
#endif
    // Apply Filters and see if msg. is required.
    if (pclsTP2_0->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsTP2_0->IsMsgValidRx(pstPassThruMsg) &&
            pclsTP2_0->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Write to Log File.
            CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK PreEnd");
            // Enqueue to Circ Buffer.
            pclsTP2_0->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#ifdef J2534_0305
    else
    {
        if (pclsTP2_0->IsMsgValidRx(pstPassThruMsg))
        {
            // Enqueue to Circ Buffer.
            pclsTP2_0->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
#endif
    // Write to Log File.
    CProtocolBase::WriteLogMsg("TP2_0.cpp", "OnTP2_0Rx()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK End");
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CTP2_0::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter)
{
    if (!bFilter)
    {
        if ((pstPassThruMsg->ulDataSize < TP2_0_MSG_SIZE_MIN) || 
            (pstPassThruMsg->ulDataSize > TP2_0_MSG_SIZE_MAX))
        {
            WriteLogMsg("TP2_0.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 715 ulDataSize=%d",pstPassThruMsg->ulDataSize); // PAR-TEST
            return(false);
        }
        if (pstPassThruMsg->ulDataSize >= TP2_0_MSG_SIZE_MIN+1)
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_TP2_0_BROADCAST_MSG) == J2534_TX_TP2_0_BROADCAST_MSG)
            {
                if (!(pstPassThruMsg->ucData[4] >= 0xF0 && pstPassThruMsg->ucData[4] <= 0xFF))
                {
                    WriteLogMsg("TP2_0.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 719 ulDataSize=%d",pstPassThruMsg->ulDataSize); // PAR-TEST
                    return(false);
                }
            }
        }
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) == 0)
            {
                WriteLogMsg("TP2_0.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 725 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulTxFlags & J2534_TX_FLAGBIT_CAN29BIT) != 0)
            {
                WriteLogMsg("TP2_0.cpp", "IsMsgValid()", DEBUGLOG_TYPE_COMMENT, "IsMsgValid fail 733 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
    }
    
    return(true);
}           

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CTP2_0::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < TP2_0_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > TP2_0_MSG_SIZE_MAX))
    {
        WriteLogMsg("TP2_0.cpp", "IsMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsMsgValidRx fail 715 ulDataSize=%d",pstPassThruMsg->ulDataSize); // PAR-TEST
        return(false);
    }

    if ((m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CANBOTH) == 0)
    {
        if (m_ulConnectFlag & J2534_CONNECT_FLAGBIT_CAN29BIT)
        {   
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) == 0)
            {
                WriteLogMsg("TP2_0.cpp", "IsMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsMsgValidRx fail 725 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
        else
        {
            if ((pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_CAN29BIT) != 0)
            {
                WriteLogMsg("TP2_0.cpp", "IsMsgValidRx()", DEBUGLOG_TYPE_COMMENT, "IsMsgValidRx fail 733 ulTxFlags=%x",pstPassThruMsg->ulTxFlags);; // PAR-TEST
                return(false);
            }
        }
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("TP2_0.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("TP2_0 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point

                pSconfig->ulValue = (unsigned long) m_fSamplePoint;

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width

                pSconfig->ulValue = (unsigned long) m_fJumpWidth;

                break;
                
            case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS; 
                
                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;       
                
            case J1939_PINS:

                pSconfig->ulValue = m_ulJ1939PPSS;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;                 
                
            case TP2_0_T_BR_INT:

                pSconfig->ulValue = m_ulTP2_0_T_BR_INT;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_T_E:

                pSconfig->ulValue = m_ulTP2_0_T_E;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_MNTC:

                pSconfig->ulValue = m_ulTP2_0_MNTC;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_T_CTA:

                pSconfig->ulValue = m_ulTP2_0_T_CTA;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_MNCT:

                pSconfig->ulValue = m_ulTP2_0_MNCT;
 
                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_MNTB:

                pSconfig->ulValue = m_ulTP2_0_MNTB;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_MNT:     

                pSconfig->ulValue = m_ulTP2_0_MNT;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_T_WAIT:      

                pSconfig->ulValue = m_ulTP2_0_T_WAIT;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_T1:           

                pSconfig->ulValue = m_ulTP2_0_T1;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_T3:       

                pSconfig->ulValue = m_ulTP2_0_T3;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_IDENTIFIER:  

                pSconfig->ulValue = m_ulTP2_0_IDENTIFIER;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            case TP2_0_RXIDPASSIVE: 

                pSconfig->ulValue = m_ulTP2_0_RXIDPASSIVE;

                if (m_enTP2_0Protocol != TP2_0_PS)
                    return(J2534_ERR_NOT_SUPPORTED);

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("TP2_0.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("TP2_0 : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                                pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = DataRate(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback
    
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Loopback(pSconfig->ulValue);
                }
                break;

            case BIT_SAMPLE_POINT:  // Bit Sample Point
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }

                //enumJ2534Error = SamplePoint(pSconfig->ulValue);

                break;

            case SYNC_JUMP_WIDTH:   // Sync Jump Width
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case J1962_PINS:
                if (m_enTP2_0Protocol != TP2_0_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1962Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulPPSS = pSconfig->ulValue; 
                    else
                        m_bJ1962Pins = false;
                }
                break;                          

            case J1939_PINS:
                if (m_enTP2_0Protocol != TP2_0_PS)
                {
                    enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
                }
                else
                {
                    if (m_bJ1962Pins || m_bJ1939Pins)
                        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                    else
                    enumJ2534Error = J1939Pins(pSconfig->ulValue);
                    if(enumJ2534Error!= J2534_STATUS_NOERROR)
                        return(enumJ2534Error);
                    if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG, pSconfig, NULL))
                    {
                        WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    } 
                    if(enumJ2534Error == J2534_STATUS_NOERROR)
                        m_ulJ1939PPSS = pSconfig->ulValue; 
                    else
                        m_bJ1939Pins = false;
                }
                break;

            case TP2_0_T_BR_INT:
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T_BR_INT(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_T_E:   
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T_E(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_MNTC:  
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = MNTC(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_T_CTA: 
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T_CTA(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_MNCT:  
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = MNCT(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_MNTB:   
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = MNTB(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_MNT:    
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = MNT(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_T_WAIT:    
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T_WAIT(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_T1:     
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T1(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_T3:         
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = T3(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_IDENTIFIER:  
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = IDENTIFIER(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            case TP2_0_RXIDPASSIVE:
                
                if((m_enTP2_0Protocol == TP2_0_PS) && (!m_bJ1962Pins) && (!m_bJ1939Pins))
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = RXIDPASSIVE(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("TP2_0.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                //enumJ2534Error = JumpWidth(pSconfig->ulValue);

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate for a TP2_0 channel.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 5) || (ulValue > 1000000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // Set DataRate to # of kbps
    if (ulValue == TP2_0_DATA_RATE_DEFAULT)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    unsigned char uchData[1];
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    uchData[0] = (unsigned char) ulValue;
    
    /*Send the device specific ioctl to set the loopback message*/
    //enumJ2534Error = vIOCTL(ulProtocol,LOOPBACK,uchData,NULL));
    
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetInternalTermination
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set internal termination.
//-----------------------------------------------------------------------------
J2534ERROR  CTP2_0::SetInternalTermination(unsigned char *pucValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
 	// Make sure pucValue is not NULL
    if (pucValue == NULL)
        return(J2534_ERR_NULLPARAMETER);

	//Valid values for <parameter> are 0-1
    if(*pucValue < 0 || *pucValue > 1)
    {
        enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
        return(enumJ2534Error);
    }

    return(enumJ2534Error);
}

J2534ERROR  CTP2_0::J1962Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x1000) || ((ulValue & 0x00FF) > 0x0010))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 4) || (ulValue == 4) ||
        ((ulValue >> 8) == 5) || (ulValue == 5) ||
        ((ulValue >> 8) == 16) || (ulValue == 16))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x060E)/* || 
        (ulValue == 0x030B) || 
        (ulValue == 0x0C0D)*/)  
    {
        m_bJ1962Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}           

J2534ERROR  CTP2_0::J1939Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x0900) || ((ulValue & 0x00FF) > 0x0009))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 1) || (ulValue == 1) ||
        ((ulValue >> 8) == 2) || (ulValue == 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0304)/* || 
        (ulValue == 0x0809)*/)
    {
        m_bJ1939Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}   

J2534ERROR  CTP2_0::T_BR_INT(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T_BR_INT = ulValue;

    return(enumJ2534Error);
}       

J2534ERROR  CTP2_0::T_E(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T_E = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::MNTC(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_MNTC = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::T_CTA(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T_CTA = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::MNCT(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_MNCT = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::MNTB(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_MNTB = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::MNT(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_MNT = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::T_WAIT(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T_WAIT = ulValue;

    return(enumJ2534Error);
}  

J2534ERROR  CTP2_0::T1(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T1 = ulValue;

    return(enumJ2534Error);
}           

J2534ERROR  CTP2_0::T3(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_T3 = ulValue;

    return(enumJ2534Error);
}

J2534ERROR  CTP2_0::IDENTIFIER(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue >= 0x200 && ulValue <= 0x2EF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_IDENTIFIER = ulValue;

    return(enumJ2534Error);
}

J2534ERROR  CTP2_0::RXIDPASSIVE(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue >= 0x300 && ulValue <= 0x7FF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_ulTP2_0_RXIDPASSIVE = ulValue;

    return(enumJ2534Error);
}

J2534ERROR  CTP2_0::RequestConnection(SBYTE_ARRAY *pInput)
{
    J2534ERROR      enumJ2534Error;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    
    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfBytes != 11)
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (pInput->pucBytePtr[5] != 0xC0)
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    enumJ2534Error = J2534_STATUS_NOERROR;
//  pInput->pucBytePtr[0] = ISO9141_CARB_INIT_ADDR;
//  pOutput->ulNumOfBytes = 2;

    return(enumJ2534Error);
}     
#ifdef J2534_0500
J2534ERROR  CTP2_0::vLogicalConnect(J2534_PROTOCOL  enProtocolID)
{
	return J2534_STATUS_NOERROR;
}
#endif
J2534ERROR  CTP2_0::TeardownConnection(SBYTE_ARRAY *pInput)
{
    int             i;
    J2534ERROR      enumJ2534Error;
    PASSTHRU_MSG    stPassThruMsg;
    unsigned long   ulGetTimestamp;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    
    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);    

    if (pInput->ulNumOfBytes != 4)
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    for (i = 0; i < TP2_0_IMPLICIT_PASS_FILTER_MAX; i++)
    {
        if (m_stImplicitPassFilter[i].ulFilterID != -1)
        {
            if (memcmp(m_stImplicitPassFilter[i].ucRxCANID, pInput->pucBytePtr, 
                sizeof(m_stImplicitPassFilter[i].ucRxCANID)) == 0)
            {
#ifdef J2534_DEVICE_SVCI1
                CProtocolBase::vStopMsgFilter(m_stImplicitPassFilter[i].ulFilterID, true);
#endif
                m_stImplicitPassFilter[i].ulFilterID = -1;
                break;
            }
        }
    }

    if (i == TP2_0_IMPLICIT_PASS_FILTER_MAX)
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    memset(&stPassThruMsg, 0, sizeof(stPassThruMsg));
    if (CProtocolBase::vIoctl(GET_TIMESTAMP, NULL, &ulGetTimestamp) == J2534_STATUS_NOERROR)
    {
        stPassThruMsg.ulTimeStamp = ulGetTimestamp * 1000;
    }
    stPassThruMsg.ulDataSize = 5;
    stPassThruMsg.ulProtocolID = m_enTP2_0Protocol;
    stPassThruMsg.ulRxStatus = (1 << 17);
    memcpy(stPassThruMsg.ucData, m_stImplicitPassFilter[i].ucRxCANID, sizeof(m_stImplicitPassFilter[i].ucRxCANID)); 
    OnTP2_0RxMessage(&stPassThruMsg, this);
    enumJ2534Error = J2534_STATUS_NOERROR;
//  pInput->pucBytePtr[0] = ISO9141_CARB_INIT_ADDR;
//  pOutput->ulNumOfBytes = 2;

    return(enumJ2534Error);
}
#endif