/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534Main.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of all the exported
*              API functions of this DLL.
* Note:
*
*******************************************************************************/

//****************************** IMPORTANT NOTE *******************************

// Please use J2534Main.h instead of J2534.h for internal use that is not 
// relevent to outside application. J2534.h is meant to be for users to include
// in their application and so it should have only what is relevant to them.
//
// When J2534_ERR_FAILED is returned, please call SetLastErrorText() function
// to set the appropriate error.
// When adding functions to be called from the DLL, add it to J2534Generic.def
//*****************************************************************************
 
//Includes
#include "StdAfx.h"
#include "J2534Generic.h"
#include "J2534Main.h"
#include "DebugLog.h"
#include "DataLog.h"
#include "VersionNo.h"
#include "LicensingDlg.h"

#include <Windows.h>    // 050613par
#include <Mmsystem.h>   // 050613par

// Extern Globals
extern CJ2534GenericApp theApp;
extern CDebugLog        gclsLog;
extern CDataLog         gclsDataLog;

CProtocolManager        gclsProtocolManager(&gclsLog, &gclsDataLog);
CDeviceBase             *gpclsDevice = NULL;
char                    gszLastErrorText[J2534MAIN_ERROR_TEXT_SIZE] = "\0";
char                    pErrorDescription[J2534MAIN_ERROR_TEXT_SIZE];
unsigned long           gulDeviceID = 0;
#ifdef J2534_DEVICE_DBRIDGE
unsigned long           gulDataStorage[J2534_DATA_STORAGE_SIZE];
bool                    gbIsDataStorageAvailable = false;
#endif
/*To support j2534 -1 V0500 API implementation*/
#ifdef J2534_0500
static unsigned long j2534_device_count = 0;
static SDEVICE j2534_device_list[25] = { 0 };
unsigned long GetPhysicalChannelIdFromLogicalId(unsigned long);
#endif
#ifdef J2534_0305
unsigned long           gChannelConnection = 0;
void Close0305Device(bool bConnected);	// For pre-0404 device: Close if opened here
#else
// Used only for pre-0404 devices, elliminates need for ifdefs when Close0305Device() is coded
#define Close0305Device(bool) ((void) 0)
#endif
void fnWriteLogMsg(char * chFileName, char * chFunctionName, unsigned long MsgType, char * chMsg, ...);

UINT DataLoggingThreadFunc(LPVOID lpParameter)
{
    J2534REGISTRY_CONFIGURATION		enJ2534Registry;
    CJ2534Registry  *pclsJ2534Registry;
    pclsJ2534Registry = new CJ2534Registry;

    memset(&enJ2534Registry, 0, sizeof(enJ2534Registry));

    // main loop
    while (true)
    {
        // wait for any event and break on kill signal
        DWORD dwRet = WaitForMultipleObjects(DATALOGGING_NUM_EVENTS, 
            theApp.m_hDataLoggingEvent, FALSE, INFINITE);
        if ((dwRet - WAIT_OBJECT_0) != theApp.m_ulDataLoggingState)
        {
            if (dwRet == WAIT_OBJECT_0 + DATALOGGING_START)
            {
                enJ2534Registry.enJ2534RegistryDeviceType = J2534REGISTRY_DEVICE_GENERIC;
                pclsJ2534Registry->GetDataRecorderSettings(&enJ2534Registry);
                theApp.m_cszDataLogFile.Format("%s", enJ2534Registry.chDataLoggingFileName);
                // If stopped
                if (theApp.m_ulDataLoggingState == DATALOGGING_STOP)
                {
                    if (enJ2534Registry.dwDataLoggingAppend == 0)
                    {
                        gclsDataLog.Open(theApp.m_cszDataLogFile, 
                            DATALOG_TYPE_COMMENT + DATALOG_TYPE_DATA + DATALOG_TYPE_ERROR, 
                            theApp.m_J2534Registry.chFunctionLibrary, enJ2534Registry.dwDataLoggingSizeMaxKB);
                    }
                    else
                    {
                        gclsDataLog.Open(theApp.m_cszDataLogFile, 
                            DATALOG_TYPE_APPEND + DATALOG_TYPE_COMMENT + DATALOG_TYPE_DATA + DATALOG_TYPE_ERROR, 
                            theApp.m_J2534Registry.chFunctionLibrary, enJ2534Registry.dwDataLoggingSizeMaxKB);      
                    }
                    gpclsDevice->vSetDataLogging(true);
                }
                // If paused
                if (theApp.m_ulDataLoggingState == DATALOGGING_PAUSE)
                {
                    gclsDataLog.Open(theApp.m_cszDataLogFile, 
                        DATALOG_TYPE_APPEND + DATALOG_TYPE_COMMENT + DATALOG_TYPE_DATA + DATALOG_TYPE_ERROR, 
                        theApp.m_J2534Registry.chFunctionLibrary, enJ2534Registry.dwDataLoggingSizeMaxKB);      
                    gpclsDevice->vSetDataLogging(true);
                }
            }
            else if (dwRet == WAIT_OBJECT_0 + DATALOGGING_STOP) 
            {
                gpclsDevice->vSetDataLogging(false);
                // If the Data Log File is open then Close it.
                if (gclsDataLog.m_pfdLogFile != NULL)
                {
                    // Close the Log file.
                    gclsDataLog.Close();
                }
            }
            else if (dwRet == WAIT_OBJECT_0 + DATALOGGING_PAUSE)  
            {
                gpclsDevice->vSetDataLogging(false);
                // If the Data Log File is open then Close it.
                if (gclsDataLog.m_pfdLogFile != NULL)
                {
                    // Close the Log file.
                    gclsDataLog.Close();
                }
            }
            else if (dwRet == WAIT_OBJECT_0 + DATALOGGING_CLOSE)  
            {
                break;
            }
            theApp.m_ulDataLoggingState = dwRet - WAIT_OBJECT_0;
        }
    }

    delete pclsJ2534Registry;
    pclsJ2534Registry = NULL;

    // exit thread
    AfxEndThread(0);
    return 0;
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruOpen()
//  Input Params    : None
//                    
//  Output Params   : None
//
//  Return          : Returns an Error Status.
//  Description     : This function opens connection to a device.
//                    This is a very first function that a user must call
//                    before using any other exported functions.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruOpen(void *pName, unsigned long *pulDeviceID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
#if (defined J2534_0404 || defined J2534_0500 )// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    
//    char                szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR          enJ2534Error;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_COMMENT, "Start");
    
    // Error out if J2534 Registry for the device is not read.
    if (!theApp.m_bRegistryRead)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "Registry not available or incorrect entries");
        // Set right text to be retreived when PassThruGetLastError() called.
        SetLastErrorText("Main : Registry not available or incorrect entries");
        return (J2534_ERR_FAILED);
    }
    // Check if Device is already open.
    if (gpclsDevice != NULL)
    {
        if (!gpclsDevice->vIsDeviceConnected())
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
            return (J2534_ERR_DEVICE_NOT_CONNECTED);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_IN_USE);
        SetLastErrorTextID(J2534_ERR_DEVICE_IN_USE);
        return (J2534_ERR_DEVICE_IN_USE);
    }

    // Check for NULL pointers
    if (pulDeviceID == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }

    // Create an appropriate Device Object.
    gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

    // Check if Device Object is created.
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
        // Set right text to be retreived when PassThruGetLastError() called.
        SetLastErrorText("Main : Device DPA not created");
        return(J2534_ERR_FAILED);
    }
    // Open the Device Connection and return the Device ID otherwise error out.
    if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                      == J2534_STATUS_NOERROR)
    {
        // Return Device ID and set the Device Open flag.
        gulDeviceID++;
        *pulDeviceID = gulDeviceID;
#ifdef J2534_DEVICE_SVCI1
//    char szLicensingInstalledDirectory[MAX_PATH];
        char szSerialNumb[255];
        char szLicenseSNArg[MAX_PATH];
        //DWORD dwExitCode;
        char temp[255];
        unsigned __int64 ui64SN = 0;
        unsigned __int64 ui64temp;
        if (gpclsDevice->vGetSerialNum(0,(char*)szSerialNumb) == J2534_STATUS_NOERROR)
        {
            strncpy_s(temp, 255, szSerialNumb+2, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 40;
            strncpy_s(temp, 255, szSerialNumb+4, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 32;
            strncpy_s(temp, 255, szSerialNumb+6, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 24;
            strncpy_s(temp, 255, szSerialNumb+8, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 16;
            strncpy_s(temp, 255, szSerialNumb+10, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 8;
            strncpy_s(temp, 255, szSerialNumb+12, 2);
            ui64temp = strtol(temp,  NULL, 16);
            ui64SN += ui64temp << 0;

            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_COMMENT, "vGetSerialNum Success");

            sprintf_s(szLicenseSNArg, MAX_PATH-1, "%016llu", ui64SN);
            
            CLicensingDlg clLicensingDlg;
            clLicensingDlg.m_cszEncryptedDeviceSN = szLicenseSNArg;
            INT_PTR nRet = -1;
            nRet = clLicensingDlg.DoModal();
            //clLicensingDlg.m_cszEncryptedDeviceSN = szLicenseSNArg;
            /*
            STARTUPINFO si;
            PROCESS_INFORMATION pi;

            ZeroMemory( &si, sizeof(si) );
            si.cb = sizeof(si);
            ZeroMemory( &pi, sizeof(pi) );

            GetPrivateProfileString("Installation", "Directory", "", szLicensingInstalledDirectory, MAX_PATH, "SVCILicensing.ini");

            sprintf_s(szLicenseSNArg, MAX_PATH-1, "%s\\SVCILicensing.exe %016llu", szLicensingInstalledDirectory, ui64SN);

            // Start the child process. 
            if( !CreateProcess( NULL,   // No module name (use command line)
                szLicenseSNArg,        // Command line
                NULL,           // Process handle not inheritable
                NULL,           // Thread handle not inheritable
                FALSE,          // Set handle inheritance to FALSE
                0,              // No creation flags
                NULL,           // Use parent's environment block
                NULL,           // Use parent's starting directory 
                &si,            // Pointer to STARTUPINFO structure
                &pi )           // Pointer to PROCESS_INFORMATION structure
            ) 
            {
                printf( "CreateProcess failed (%d).\n", GetLastError() );
                restoreDPA(dpaHandle);
                dpaHandle=NULL;
                SetLastErrorText("Error checking device licensing.");
                unLoadDLL();
                return(J2534_ERR_FAILED);
            }

            // Wait until child process exits.
            WaitForSingleObject( pi.hProcess, INFINITE );

            dwExitCode = -1;
            if (GetExitCodeProcess(pi.hProcess, &dwExitCode))
            {
            // successfully retrieved exit code
            }

            // Close process and thread handles. 
            CloseHandle( pi.hProcess );
            CloseHandle( pi.hThread );*/ 

            fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_COMMENT, "License Check App returned dwExitCode 0x%02X", nRet);
            if (nRet != 0)
            {
                // Close the Device Connection.
                gpclsDevice->vCloseDevice();
                
                // Delete Device Object.
                delete gpclsDevice;
                gpclsDevice = NULL;
                SetLastErrorText("Device License is not currently activated.");
                return(J2534_ERR_FAILED);
            }
        }
#endif
    }
    else
    {
          
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        delete gpclsDevice;
        gpclsDevice = NULL;
        return(enJ2534Error);
    }

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    // Capture version information - curious minds want to know
    char chFirmwareVersion[SMALL_TEXT_SIZE], chDllVersion[SMALL_TEXT_SIZE], chApiVersion[SMALL_TEXT_SIZE];
    PassThruReadVersion(gulDeviceID,chFirmwareVersion, chDllVersion, chApiVersion);
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruOpen()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    
    theApp.m_ulDataLoggingState = DATALOGGING_STOP;
    AfxBeginThread(DataLoggingThreadFunc, NULL);
#endif
#ifdef J2534_DEVICE_DBRIDGE
    gbIsDataStorageAvailable = false;
    memset(gulDataStorage, 0, sizeof(gulDataStorage));
#endif
    SetLastErrorTextID(J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruClose()
//  Input Params    : None
//                    
//  Output Params   : None
//
//  Return          : Returns an Error Status.
//  Description     : This function closes connection to a device.
//                    This is the very last function that a user must call
//                    before the application exits.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruClose(unsigned long ulDeviceID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
#if (defined J2534_0404 || defined J2534_0500 )

    int                 i;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruClose()", DEBUGLOG_TYPE_COMMENT, "Start");

#ifdef J2534_0500
	// Check for invalid Device ID
	if (ulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruClose()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruClose()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);

        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
        
    SetEvent(theApp.m_hDataLoggingEvent[DATALOGGING_CLOSE]);
 
    if (!gpclsDevice->vIsDeviceConnected(false))
    {
        // In case user did not call disconnect prior to calling PassThruClose(),
        // disconnect from all the actively connected channels first
        for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
        {
            if (gclsProtocolManager.m_pclsProtocolObject[i] != NULL)
            {
                gclsProtocolManager.m_pclsProtocolObject[i]->vDisconnect();

                // Delete all the Protocol Objects that were created.
                gclsProtocolManager.DeleteProtocolObj(i);
            }
        }

        // Close the Device Connection.
        gpclsDevice->vCloseDevice();
        
        // Delete Device Object.
        delete gpclsDevice;
        gpclsDevice = NULL;
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruClose()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    // In case user did not call disconnect prior to calling PassThruClose(),
    // disconnect from all the actively connected channels first
    for (i = 0; i < PROTOCOLMANAGER_OBJ_LIST_SIZE; i++)
    {
        if (gclsProtocolManager.m_pclsProtocolObject[i] != NULL)
        {
            gclsProtocolManager.m_pclsProtocolObject[i]->vDisconnect();

            // Delete all the Protocol Objects that were created.
            gclsProtocolManager.DeleteProtocolObj(i);
        }
    }

    // Close the Device Connection.
    gpclsDevice->vCloseDevice();
    
    // Delete Device Object.
    delete gpclsDevice;
    gpclsDevice = NULL;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruClose()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
#endif
	SetLastErrorTextID(J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);   
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruConnect()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function connects to a requested protoco.
//                    Before calling this function the user must call PassThruOpen
//--------------------------------------------------------------------------
#ifdef J2534_0404
extern "C" J2534ERROR WINAPI PassThruConnect(unsigned long ulDeviceID,
                                             J2534_PROTOCOL enProtocolID,
                                             unsigned long ulFlags,
                                             unsigned long ulBaudRate,
                                             unsigned long *pulChannelID)
#endif
#ifdef J2534_0305
extern "C" J2534ERROR WINAPI PassThruConnect(J2534_PROTOCOL enProtocolID,
                                             unsigned long ulFlags,
                                             unsigned long *pulChannelID)
#endif

	// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
/**
* 7.3.5 PassThruConnect
*
* This function shall establish a physical connection to the vehicle using the specified interface hardware in the designated
* Pass-Thru Device. If the function call is successful, the return value shall be STATUS_NOERROR, the value pointed to by
* <pChannelID> shall be used as a handle to the newly established physical communication channel and the channel shall
* be in an initialized state. Otherwise, the return value shall reflect the error detected and the value pointed to by
* <pChannelID> shall not be altered. The last action in the process of establishing a physical connection shall be to connect
* the specified interface hardware in the Pass-Thru Device to the vehicle.
*
* The initialized state for a physical communication channel shall be defined to be:
*   - No logical communication channels shall be associated with the physical communication channel
*   - All periodic messages shall be cleared and all Periodic Message IDs invalidated
*   - All filters shall be in the default state and all Filter IDs invalidated
*   - All transmit and receive queues shall be cleared
*   - Any active message transmission shall be terminated
*   - Any message reception in progress shall be terminated
*   - All configurable parameters for the associated physical communication channel shall be at the default values
*
* If the designated <DeviceID> is not currently open, the return value shall be ERR_DEVICE_NOT_OPEN. If
* PassThruOpen was successfully called but the <DeviceID> is currently not valid, the return value shall be
* ERR_INVALID_DEVICE_ID. If the Pass-Thru Device is not currently connected or was previously disconnected without
* being closed, the return value shall be ERR_DEVICE_NOT_CONNECTED. If the physical communication channel
* attempting to be established would cause a resource conflict, such as an attempt to use a pin on the SAE J1962
* connector that is already in use, the return value shall be ERR_RESOURCE_CONFLICT. If the designated pin(s) are not
* valid for the designated <ProtocolID> or the <Connector> is not J1962_CONNECTOR, the return value shall be
* ERR_PIN_NOT_SUPPORTED. If the number in <NumOfResources> is invalid, the return value shall be
* ERR_RESOURCE_CONFLICT. If either <pChannelID> or <ResourceStruct.ResourceListPtr> is NULL, the return value
* shall be ERR_NULL_PARAMETER. Attempts to use <ProtocolID> values designated as 'reserved' shall result in the
* return value ERR_PROTOCOL_ID_NOT_SUPPORTED. Attempts to use bits in <Flags> designated as 'reserved' or bits
* that are not applicable for the <ProtocolID> indicated shall result in the return value ERR_FLAG_NOT_SUPPORTED.
* Attempts to use a baud rate that is not specified for the <ProtocolID> designated shall result in the return value
* ERR_BAUDRATE_NOT_SUPPORTED. If the Pass-Thru Interface does not support this function call, then the return
* value shall be ERR_NOT_SUPPORTED. The return value ERR_FAILED is a mechanism to return other failures, but this
* is intended for use during development. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return
* ERR_FAILED or ERR_NOT_SUPPORTED.
*
* By default, the Pass-Thru Interface will block all received messages for this physical communication channel until a filter is
* set. However, this shall not block messages on logical communication channels when they are created on this physical
* communication channel (see PassThruStartMsgFilter for more details).
*
* Parameters:
*   <DeviceID>                      is an input, set by the application, which contains the Device ID returned from PassThruOpen.
*   <ProtocolID>                    is an input, set by the application, which contains the Protocol ID for the physical communication
*                                   channel to be connected (see Figure 27 for valid options). This ID defines how this physical
*                                   communication channel will interact with the vehicle. Figure 32 defines the J1962 pin usage for each
*                                   Protocol ID.
*   <Flags>                         is an input, set by the application, which contains the Flags for the physical communication channel
*                                   to be connected (see Figure 28 for valid options). These bits can be ORed together and represent
*                                   the desired physical communication channel configuration.
*   <BaudRate>                      is an input, set by the application, which contains the initial baud rate value for the physical
*                                   communication channel to be connected (see Figure 29 for valid options).
*   <ResourceStruct>                is an input structure, set by the application, which contains connector/pin-out information. Figure 32
*                                   identifies the specific values that shall be used by an SAE J2534-1 Interface.
*                                   The RESOURCE_STRUCT, defined in Section 9.18, where:
*       <Connector>                 is an input, set by the application, which identifies the connector to be used.
*                                   Figure 30 specifies the list of valid connectors.
*       <NumOfResources>            is an input, set by the application, which indicates the number of items in the array pointed to by <ResourceListPtr>.
*       <ResourceListPtr>           is an input, set by the application, which points to an array of unsigned longs also allocated by the application. This
									array represents the pins used by the protocol. In the array, offset 0 will contain the primary pin and the remainder of
*									the array (starting at offset 1) will contain the secondary/additional pins (if applicable). The contents of the array are
									protocol/connector specific. Figure 31 specifies the pin identification convention that shall be followed for each
*                                   protocol.
*   <pChannelID>                    is an input, which points to an unsigned long allocated by the application. Upon return, the unsigned
*                                   long shall contain the Channel ID, which is to be used as a handle to the physical communication
*                                   channel for future function calls.
* Return Values:
* 	 ERR_CONCURRENT_API_CALL        A J2534 API function has been called before the previous J2534 function call has completed
* 	 ERR_DEVICE_NOT_OPEN            PassThruOpen has not successfully been called
* 	 ERR_INVALID_DEVICE_ID          PassThruOpen has been successfully called, but the current <DeviceID> is not valid
* 	 ERR_DEVICE_NOT_CONNECTED       Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has,
* 	                                at some point, failed to communicate with the Pass-Thru Device � even though it may not currently be disconnected.
* 	 ERR_NOT_SUPPORTED              DLL does not support this API function. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.
* 	 ERR_PROTOCOL_ID_NOT_SUPPORTED  <ProtocolID> value is not supported
* 	 ERR_PIN_NOT_SUPPORTED          Pin number and/or connector specified is either invalid or unknown
* 	 ERR_RESOURCE_CONFLICT          Request causes a resource conflict (such as a pin on a connector is already in use, a data link controller is already in use, or requested
									resource is not present, etc.)
* 	 ERR_FLAG_NOT_SUPPORTED         <Flags> value(s) are either invalid, unknown, or not appropriate for the current channel (such as setting a flag that is only valid for ISO 9141 
									on a CAN channel)
* 	 ERR_BAUDRATE_NOT_SUPPORTED     <BaudRate> is either invalid or unachievable for the current channel)
* 	 ERR_NULL_PARAMETER             NULL pointer supplied where a valid pointer is required
* 	 ERR_FAILED                     Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED.
* 	 STATUS_NOERROR                 Function call was successful
*/
#ifdef J2534_0500
extern "C" J2534ERROR WINAPI PassThruConnect(unsigned long ulDeviceID,
											 J2534_PROTOCOL enProtocolID,
											 unsigned long ulFlags,
											 unsigned long ulBaudRate,
											 RESOURCE_STRUCT ResourceStruct,
											 unsigned long *pulChannelID)
#endif
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR      enJ2534Error;
    unsigned long   ulFlagChannel; 
    unsigned long   ulCANIDsupport, ulCANIDtype, ulChecksumFlag;
    unsigned long   ulLFollowK;
    bool            bConnected;

    bConnected = false;

    // Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check for NULL parameters.
	if (pulChannelID == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		return (J2534_ERR_NULLPARAMETER);
	}
#ifdef J2534_0500
	if (ResourceStruct.ResourceListPtr == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		return (J2534_ERR_NULLPARAMETER);
	}

#endif
#ifdef J2534_0404
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_DATA, "DevID=%u ProtID=%u Flags=0x%02X Baud=%u",
                ulDeviceID, enProtocolID, ulFlags, ulBaudRate);

#endif
#ifdef J2534_0305
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_DATA, "ProtID=%u Flags=0x%02X", enProtocolID, ulFlags);
#endif
#ifdef J2534_0500
	// Check for invalid Device ID
	if (ulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
	if (ResourceStruct.Connector != J1962_CONNECTOR)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_PIN_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_PIN_NOT_SUPPORTED);
		return (J2534_ERR_PIN_NOT_SUPPORTED);
	}

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_DATA, "DevID=%u ProtID=%u Flags=0x%02X Baud=%u",
		ulDeviceID, enProtocolID, ulFlags, ulBaudRate);
#endif
    // Check if ulFlags value is valid.
    ulFlagChannel = ((ulFlags >> 24) & 0xFF);

    // Getting the L follow K flag
    ulLFollowK = ((ulFlags >> 12) & 0x01);

    if ((enProtocolID != ISO9141 && enProtocolID != ISO14230) && 
#ifdef J2534_DEVICE_DBRIDGE
       (enProtocolID != ISO9141_PS) &&
#endif
        (ulLFollowK != 0))
    {
        enJ2534Error = J2534_ERR_INVALID_FLAGS;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_FLAGS);
    }

    // Getting the CAN ID type
    ulCANIDsupport = ((ulFlags >> 11) & 0x01);

    if ((ulCANIDsupport != 0) && 
        (enProtocolID != CAN) && (enProtocolID != ISO15765) &&
        (enProtocolID != CAN_PS) && (enProtocolID != ISO15765_PS) && 
        (enProtocolID != TP2_0_PS) &&
        (enProtocolID != SW_ISO15765_PS) && (enProtocolID != SW_CAN_PS) &&
        (enProtocolID != SW_CAN_ISO15765_CH1) && (enProtocolID != SW_CAN_CAN_CH1) &&
        (enProtocolID != CAN_CH1) && (enProtocolID != CAN_CH2) &&
        (enProtocolID != ISO15765_CH1) && (enProtocolID != ISO15765_CH2) &&
        (enProtocolID != TP2_0_CH1) && (enProtocolID != TP2_0_CH2) &&
        (enProtocolID != CAN_CH3) && (enProtocolID != CAN_CH4) &&
        (enProtocolID != ISO15765_CH3) && (enProtocolID != ISO15765_CH4) && 
        (enProtocolID != TP2_0_CH3) && (enProtocolID != TP2_0_CH4) && 
        (enProtocolID != FT_CAN_PS) && (enProtocolID != FT_ISO15765_PS) &&
        (enProtocolID != FT_CAN_CH1) && (enProtocolID != FT_ISO15765_CH1))
    {
        enJ2534Error = J2534_ERR_INVALID_FLAGS;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_FLAGS);
    }
    else
    {
        // This is a HACK for tech authority application because once it connects to ISO15765 with flags
        // set to only 11-bit CAN IDs, it tries to set flow control filters using 29-bit CAN IDs which would
        // return an error ERR_INVALID_MSG (0x0a)
        if (!((enProtocolID != CAN) && (enProtocolID != ISO15765) &&
            (enProtocolID != CAN_PS) && (enProtocolID != ISO15765_PS) &&
            (enProtocolID != SW_ISO15765_PS) && (enProtocolID != SW_CAN_PS) &&
            (enProtocolID != SW_CAN_ISO15765_CH1) && (enProtocolID != SW_CAN_CAN_CH1) &&
            (enProtocolID != CAN_CH1) && (enProtocolID != CAN_CH2) &&
            (enProtocolID != ISO15765_CH1) && (enProtocolID != ISO15765_CH2) &&
            (enProtocolID != CAN_CH3) && (enProtocolID != CAN_CH4) &&
            (enProtocolID != ISO15765_CH3) && (enProtocolID != ISO15765_CH4) && 
            (enProtocolID != FT_CAN_PS) && (enProtocolID != FT_ISO15765_PS) &&
            (enProtocolID != FT_CAN_CH1) && (enProtocolID != FT_ISO15765_CH1)))
        {
            ulFlags |= 0x800;
        }
    }

    // Getting the checksum flag
    ulChecksumFlag = ((ulFlags >> 9) & 0x01);

    if ((enProtocolID != ISO9141 && enProtocolID != ISO14230 && enProtocolID != J1708_PS && enProtocolID != J1708_CH1) && 
#ifdef J2534_DEVICE_DBRIDGE
       (enProtocolID != HONDA_DIAGH_PS) && (enProtocolID != UART_ECHO_BYTE_PS) && (enProtocolID != ISO9141_PS) &&
#endif
        (ulChecksumFlag != 0))
    {
        enJ2534Error = J2534_ERR_INVALID_FLAGS;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_FLAGS);
    }

    // Getting the CAN ID type
    ulCANIDtype = ((ulFlags >> 8) & 0x01);

    if ((ulCANIDtype != 0) && 
        (enProtocolID != CAN) && (enProtocolID != ISO15765) &&
        (enProtocolID != CAN_PS) && (enProtocolID != ISO15765_PS) &&
        (enProtocolID != SW_ISO15765_PS) && (enProtocolID != SW_CAN_PS) &&
        (enProtocolID != SW_CAN_ISO15765_CH1) && (enProtocolID != SW_CAN_CAN_CH1) &&
        (enProtocolID != CAN_CH1) && (enProtocolID != CAN_CH2) &&
        (enProtocolID != ISO15765_CH1) && (enProtocolID != ISO15765_CH2) &&
        (enProtocolID != CAN_CH3) && (enProtocolID != CAN_CH4) &&
        (enProtocolID != ISO15765_CH3) && (enProtocolID != ISO15765_CH4) &&
        (enProtocolID != J1939_CH1) && (enProtocolID != J1939_CH2) &&
        (enProtocolID != J1939_CH3) && (enProtocolID != J1939_CH4) &&
        (enProtocolID != J1939_PS) && 
        (enProtocolID != TP2_0_CH1) && (enProtocolID != TP2_0_CH2) &&
        (enProtocolID != TP2_0_CH3) && (enProtocolID != TP2_0_CH4) &&
        (enProtocolID != TP2_0_PS) && 
        (enProtocolID != FT_CAN_PS) && (enProtocolID != FT_ISO15765_PS) &&
        (enProtocolID != FT_CAN_CH1) && (enProtocolID != FT_ISO15765_CH1))
    {
        enJ2534Error = J2534_ERR_INVALID_FLAGS;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_FLAGS);
    }

    if ((ulCANIDtype == 0) && (enProtocolID == J1939_PS || 
		enProtocolID == J1939_CH1 || enProtocolID == J1939_CH2 || 
		enProtocolID == J1939_CH3 || enProtocolID == J1939_CH4))
    {
        enJ2534Error = J2534_ERR_NOT_SUPPORTED;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NOT_SUPPORTED);
    } 

    if ((ulCANIDsupport == 0) && (ulCANIDtype == 1) && (enProtocolID == TP2_0_PS || 
		enProtocolID == TP2_0_CH1 || enProtocolID == TP2_0_CH2 || 
		enProtocolID == TP2_0_CH3 || enProtocolID == TP2_0_CH4))
    {
        enJ2534Error = J2534_ERR_NOT_SUPPORTED;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NOT_SUPPORTED);
    }

#ifdef J2534_0305
    // Getting the ISO15765 Addressing Method
    unsigned long ulISO15765Addr = ((ulFlags >> 7) & 0x01);

    if ((ulISO15765Addr != 0) && (ulISO15765Addr != 1))
    {
        enJ2534Error = J2534_ERR_INVALID_FLAGS;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_FLAGS);
    }
#endif
#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;

            if(gclsLog.m_pfdLogFile)
            {
                char chFirmwareVersion[SMALL_TEXT_SIZE], chDllVersion[SMALL_TEXT_SIZE], chApiVersion[SMALL_TEXT_SIZE];
                PassThruReadVersion(chFirmwareVersion, chDllVersion, chApiVersion);
            }

        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif  
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }   
    // Create Protocol Object.
    if ((enJ2534Error = gclsProtocolManager.CreateProtocolObject(enProtocolID, gpclsDevice, pulChannelID)) != J2534_STATUS_NOERROR)
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }
    // Connect to Protocol
#if defined (J2534_0404) || defined (J2534_0500)
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[*pulChannelID]->vConnect(
                                enProtocolID, ulFlags, ulBaudRate)) != J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[*pulChannelID]->vConnect(
                                enProtocolID, ulFlags, 0)) != J2534_STATUS_NOERROR)
#endif
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        // Delete the Protocol Object that was created earlier.
        gclsProtocolManager.DeleteProtocolObj(*pulChannelID);
        return(enJ2534Error);
    }
    // TEMP-TEST does reseting the Multi Media Timer fix the OBDI issue Cardone is reporting? - par130717
    if (enProtocolID == GM_UART)
    {
        // Restore Multi-media timer back - 050613par
        if (theApp.m_bMultiMediaTimerSet)
        {
            timeEndPeriod(1);
            theApp.m_bMultiMediaTimerSet = false;
        }
    }

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
#ifdef J2534_0305
    gChannelConnection++;
#endif
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruDisconnect()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This disconnects the protocol connection established
//                    earlier.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruDisconnect(unsigned long ulChannelID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_DATA, "ulChannelID=%u", ulChannelID);

    // TEMP-TEST does reseting the Multi Media Timer fix the OBDI issue Cardone is reporting? - par130717
    J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
    if (enProtocolID == GM_UART)
    {
        // Restore Multi-media timer back - 050613par
        if (!theApp.m_bMultiMediaTimerSet)
        {
            timeBeginPeriod(1);
            theApp.m_bMultiMediaTimerSet = true;
        }
    }

#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
	// Check if the requested Channel ID is valid.
	if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
		SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}
    // Disconnect to Protocol
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->vDisconnect()) != J2534_STATUS_NOERROR)
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Delete Protocol Object for this channel.
    if ((enJ2534Error = gclsProtocolManager.DeleteProtocolObj(ulChannelID)) != J2534_STATUS_NOERROR)
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

#ifdef J2534_0305
gChannelConnection--;
if (gChannelConnection == 0)
{
    gpclsDevice->vCloseDevice();
    delete gpclsDevice;
    gpclsDevice = NULL;
}
#endif
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadMsgs()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function reads messages from the circular buffer of 
//                    the protocol requested.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadMsgs(unsigned long ulChannelID,
                                              PASSTHRU_MSG *pstrucJ2534Msg,
                                              unsigned long *pulNumMsgs,
                                              unsigned long ulTimeout)
{   
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    char            szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR      enJ2534Error = J2534_ERR_INVALID_CHANNEL_ID;
    unsigned long   i, j, k;
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (gclsLog.m_pfdLogFile)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Channel %lu NumMsg %lu Timeout %lu", 
                ulChannelID, *pulNumMsgs, ulTimeout);
    }

#if defined (J2534_0404) || defined (J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    // Check for NULL pointers
    if ((pstrucJ2534Msg == NULL) || (pulNumMsgs == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    if (*pulNumMsgs == 0)
    {
        enJ2534Error = J2534_ERR_FAILED;
        SetLastErrorText("Main : Invalid NumMsgs value");
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

	if (ulChannelID != 0 && ulChannelID < MAX_PHY_CHANNEL_ID)
	{
		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}

		if (!gpclsDevice->IsChannelConnected(ulChannelID))
		{
			return(J2534_ERR_PIN_INVALID);
		}

		// Write Msg.
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->vReadMsgs(pstrucJ2534Msg,
																							pulNumMsgs,
																							ulTimeout))
																							!= J2534_STATUS_NOERROR)
		{
#ifdef J2534_0500
			gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->m_ulPhyChnRxCount = 0;
#endif
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			// Write to Log File.
			if (enJ2534Error == J2534_ERR_TIMEOUT)
			{

				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "NumMsg %lu", *pulNumMsgs);
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
				for (k = 0; k < *pulNumMsgs; k++)
				{
					// Write to Log File.
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ProtocolID 0x%X", pstrucJ2534Msg[k].ulProtocolID);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Tx Flags 0x%X", pstrucJ2534Msg[k].ulTxFlags);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ExtraDataIndex 0x%X", pstrucJ2534Msg[k].ulExtraDataIndex);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "TimeStamp 0x%X", pstrucJ2534Msg[k].ulTimeStamp);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Rx Status 0x%X", pstrucJ2534Msg[k].ulRxStatus);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Data Size 0x%X", pstrucJ2534Msg[k].ulDataSize);

					if (pstrucJ2534Msg[k].ulDataSize > 0)
					{
						j = 0;
						for (i = 0; i < pstrucJ2534Msg[k].ulDataSize && i < 15; i++)
						{
							j += sprintf_s(szBuffer + j, sizeof(szBuffer) - j, "%02X ", pstrucJ2534Msg[k].ucData[i]);
						}

						if (i >= 15)
							j += sprintf_s(szBuffer + j, sizeof(szBuffer) - j, "...");
					}
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, szBuffer);
					//              fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "pstrucJ2534Msg->ucData %lu", &pstrucJ2534Msg->ucData);
				}
			}
			//return(enJ2534Error);
		}
#ifdef J2534_0500
		gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->m_ulPhyChnRxCount = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->m_ulPhyChnRxCount - *pulNumMsgs;
#endif
	}
	else
	{
#ifdef J2534_0500
		unsigned long ulPhysicalId = 0;
		ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ulChannelID);

		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		if (ulChannelID > MAX_PHY_CHANNEL_ID)
		{
			if (!gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->IsLogChannelValid(ulChannelID))
			{
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
				SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
				return(J2534_ERR_INVALID_CHANNEL_ID);
			}
		}

		// Write Msg.
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->vReadMsgs(pstrucJ2534Msg,
																							pulNumMsgs,
																							ulTimeout,
																							ulChannelID))
																							!= J2534_STATUS_NOERROR)
		{
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			// Write to Log File.
			if (enJ2534Error == J2534_ERR_TIMEOUT)
			{
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "NumMsg %lu", *pulNumMsgs);
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_TIMEOUT);
				for (k = 0; k < *pulNumMsgs; k++)
				{
					// Write to Log File.
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ProtocolID 0x%X", pstrucJ2534Msg[k].ulProtocolID);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Tx Flags 0x%X", pstrucJ2534Msg[k].ulTxFlags);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ExtraDataIndex 0x%X", pstrucJ2534Msg[k].ulExtraDataIndex);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "TimeStamp 0x%X", pstrucJ2534Msg[k].ulTimeStamp);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Rx Status 0x%X", pstrucJ2534Msg[k].ulRxStatus);
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Data Size 0x%X", pstrucJ2534Msg[k].ulDataSize);

					if (pstrucJ2534Msg[k].ulDataSize > 0)
					{
						j = 0;
						for (i = 0; i < pstrucJ2534Msg[k].ulDataSize && i < 15; i++)
						{
							j += sprintf_s(szBuffer + j, sizeof(szBuffer) - j, "%02X ", pstrucJ2534Msg[k].ucData[i]);
						}

						if (i >= 15)
							j += sprintf_s(szBuffer + j, sizeof(szBuffer) - j, "...");
					}
					fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, szBuffer);
					//              fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "pstrucJ2534Msg->ucData %lu", &pstrucJ2534Msg->ucData);
				}
			}
			//return(enJ2534Error);
		}
		//gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->ClearRxQueue(ulChannelID);
		//gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_ulPhyChnRxCount = 0;

#endif
	}
    if (gclsLog.m_pfdLogFile)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "NumMsg %lu", *pulNumMsgs);
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        for (k = 0; k < *pulNumMsgs; k++)
        {
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ProtocolID 0x%X", pstrucJ2534Msg[k].ulProtocolID);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Tx Flags 0x%X", pstrucJ2534Msg[k].ulTxFlags);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "ExtraDataIndex 0x%X", pstrucJ2534Msg[k].ulExtraDataIndex);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "TimeStamp 0x%X", pstrucJ2534Msg[k].ulTimeStamp);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Rx Status 0x%X", pstrucJ2534Msg[k].ulRxStatus);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Data Size 0x%X", pstrucJ2534Msg[k].ulDataSize);
            if(pstrucJ2534Msg[k].ulDataSize > 0)
            {
                j = 0;
                for(i = 0; i < pstrucJ2534Msg[k].ulDataSize && i < 15; i++)
                {
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534Msg[k].ucData[i]);                
                }

                if(i >= 15)
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
            }
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, szBuffer);
//          fnWriteLogMsg("J2534Main.cpp", "PassThruReadMsgs()", DEBUGLOG_TYPE_COMMENT, "pstrucJ2534Msg->ucData %lu", &pstrucJ2534Msg->ucData);
        }
    }

    SetLastErrorTextID(enJ2534Error);
    return(enJ2534Error);   
}

/*
// This allows for multiple CAN and ISO15765 channels to be connected to
void SetProtocolID(unsigned long ulChannelID)
{
    J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
    switch (enProtocolID)
    {
        case CAN:
        case CAN_PS:
        case CAN_CH1:
        case CAN_CH2:
        case CAN_CH3:
        case CAN_CH4:
//           gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->SetProtocolID(enProtocolID);
            m_enCANProtocol = enProtocolID;
            break;
        case ISO15765:
        case ISO15765_PS:
        case ISO15765_CH1:
        case ISO15765_CH2:
        case ISO15765_CH3:
        case ISO15765_CH4:
//            gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->SetProtocolID(enProtocolID);
            m_enISO15765Protocol = enProtocolID;
            break;
        default:
            break;
    }
}
*/

//-----------------------------------------------------------------------------
//  Function Name   : PassThruWriteMsgs()
//  Input Params    : 
//  Output Params   : 
//                    
//  Return          : Returns an Error Status.
//  Description     : This function writes the message into a circular buffer
//                    to schedule transmission of message on the bus by another
//                    thread. If Blocking requested, the function does not 
//                    return until it varifies that the msg. is successfully 
//                    transmitted or the error occurs. If Non-Blocking is set,
//                    the function simply puts the msg. in the circular buffer
//                    and returns.
//-----------------------------------------------------------------------------
#if defined (J2534_0404) || defined (J2534_0305) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
extern "C" J2534ERROR WINAPI PassThruWriteMsgs(unsigned long ulChannelID,
                                              PASSTHRU_MSG *pstrucJ2534Msg,
                                              unsigned long *pulNumMsgs,
                                              unsigned long ulTimeout)
#endif
#if defined J2534_0500
/**
* 7.3.11 PassThruQueueMsgs
*
* This function shall queue messages to the Pass-Thru Interface for transmission on the designated channel. If the function
* call is successful, the return value shall be STATUS_NOERROR and the value pointed to by <pNumMsgs> shall be
* updated to reflect the actual number of messages queued as a result of this function call. If an error occurs during this
* function call, the function shall immediately return with the applicable return value and the value pointed to by
* <pNumMsgs> shall be updated to reflect the actual number of messages queued as a result of this function call. The
* Pass-Thru Interface shall not modify structures pointed to by <pMsg>. This function replaces PassThruWriteMsgs, which
* was used in earlier versions of the SAE J2534 API.
*
* This function shall place messages in the transmit queue in the order they were passed in (that is, the message at offset 0
* shall be first, the message at offset 1 shall be second, and so on). These messages shall follow the format specified in
* Section 7.2.4, including the actions to be taken when messages violate the specified format.
*
* The entire message must fit in the transmit queue for it to be transferred. If all the requested messages do not fit in the
* queue, the return value shall be ERR_BUFFER_FULL and the value pointed to by <pNumMsgs> shall be updated to
* reflect the number of messages that were queued as a result of this function call. The function shall not wait for space to
* become available to queue messages.
*
* Note that messages passed to the Pass-Thru Interface using this function call have lower priority than those passed via
* PassThruStartPeriodicMsg (see Section 6.10.2 for more details). Also, some protocols will generate Indications when
* transmitting (see Section 7.2.6 for more details).
*
* ISO 15765 logical communication channels can queue a Single Frame whose network address or <TxFlags> do not
* match the <RemoteAddress> or <RemoteTxFlags> provided during channel creation. However, an attempt to queue a
* Segmented Message shall result in the return value ERR_MSG_NOT_ALLOWED.
*
* For SAE J2610 channels, it shall be an error if the programmable voltage generator is in use when a message with
* SCI_TX_VOLTAGE is set to 1 in <TxFlags> is becoming active. In this case, if a TxDone Indication was required for the
* associated message then a TxFailed Indication shall be generated.
*
* If the corresponding Pass-Thru Device is not currently open, the return value shall be ERR_DEVICE_NOT_OPEN. If
* <ChannelID> is not valid, the return value shall be ERR_INVALID_CHANNEL_ID. If the Pass-Thru Device is not
* currently connected or was previously disconnected without being closed, the return value shall be
* ERR_DEVICE_NOT_CONNECTED. If either <pMsg> or <pNumMsgs> are NULL, the return value shall be
* ERR_NULL_PARAMETER. If the <ProtocolID> in the PASSTHRU_MSG structure (for any message passed into this
* function) does not match the <ProtocolID> for the associated channel, the return value shall be
* ERR_MSG_PROTOCOL_ID. If any message passed into this function does not follow the format specified in Section
* 7.2.4, the return value shall be ERR_INVALID_MSG. If the initial value pointed to by <pNumMsgs> is 0 then the function
* shall take no action and immediately return the value STATUS_NOERROR. If this function call is not supported for this
* channel, then the return value shall be ERR_NOT_SUPPORTED. The return value ERR_FAILED is a mechanism to
* return other failures, but this is intended for use during development. A fully compliant SAE J2534-1 Pass-Thru Interface
* shall never return ERR_FAILED or ERR_NOT_SUPPORTED.
*
* Parameters:
*   <ChannelID>                     is an input, set by the application that contains the handle to the physical or logical
*                                   communication channel where the messages are to be queued. The Channel ID was assigned
*                                   by a previous call to PassThruConnect or PassThruLogicalConnect.
*   <pMsg>                          is an input, set by the application, which points to an array of message structure(s)
*                                   (allocated by the application) to be queued by the Pass-Thru Interface.
*   <pNumMsgs>                      is an input, set by the application, which initially points to an unsigned long that
*                                   contains the number of messages to be queued. Upon return, the unsigned long shall
*                                   contain the actual number of messages that were successfully queued.
*
* Return Values:
*   ERR_CONCURRENT_API_CALL         A J2534 API function has been called before the previous J2534 function call has completed
*   ERR_DEVICE_NOT_OPEN             PassThruOpen has not successfully been called
*   ERR_DEVICE_NOT_CONNECTED        Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has,
*                                   at some point, failed to communicate with the Pass-Thru Device � even though it may not
*                                   currently be disconnected.
*   ERR_NOT_SUPPORTED               Device does not support this API function for the associated <ChannelID>. A fully compliant
*                                   SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.
*   ERR_NULL_PARAMETER              NULL pointer supplied where a valid pointer is required
*   ERR_MSG_PROTOCOL_ID             <ProtocolID> in the PASSTHRU_MSG structure does not match the <ProtocolID> from the original
*                                   call to PassThruConnect for the <ChannelID>
*   ERR_INVALID_MSG                 Message structure is invalid for the given <ChannelID> (refer to Section 7.2.4 for more details)
*   ERR_MSG_NOT_ALLOWED             Attempting to queue a Segmented Message whose network address and/or <TxFlags> does not match
*                                   those defined for the <RemoteAddress> or <RemoteTxFlags> during channel creation on a logical
*                                   communication channel (This Return Value is only applicable to ISO 15765 logical communication channels)
*   ERR_BUFFER_FULL                 Transmit queue is full (<pNumMsgs> points to the actual number of messages queued)
*   ERR_FAILED                      Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1
*                                   Pass-Thru Interface shall never return ERR_FAILED.
*   STATUS_NOERROR                  Function call was successful
*/
extern "C" J2534ERROR WINAPI PassThruQueueMsgs(unsigned long ulChannelID,
											   PASSTHRU_MSG *pstrucJ2534Msg,
										       unsigned long *pulNumMsgs)
	
#endif
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    char            szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR      enJ2534Error;
    unsigned long   i, j, k;
    // Write to Log File.
    if (gclsLog.m_pfdLogFile)
    {
#if defined (J2534_0404) || defined (J2534_0305)
		// Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
        J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u", ulChannelID, enProtocolID);
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "pulNumMsgs %lu", *pulNumMsgs);
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Timeout %lu", ulTimeout);
#endif
#ifdef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
		J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u", ulChannelID, enProtocolID);
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "pulNumMsgs %lu", *pulNumMsgs);

#endif
        for (k = 0; k < *pulNumMsgs; k++)
        {
#if defined (J2534_0404) || defined (J2534_0305)
			fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Tx Flag 0x%X", pstrucJ2534Msg[k].ulTxFlags);
            fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "ulDataSize %lu", pstrucJ2534Msg[k].ulDataSize);
#endif
#ifdef J2534_0500
			pstrucJ2534Msg[k].ucData = new unsigned char[pstrucJ2534Msg[k].ulDataSize];
			fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "Tx Flag 0x%X", pstrucJ2534Msg[k].ulTxFlags);
			fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "ulDataSize %lu", pstrucJ2534Msg[k].ulDataSize);
#endif
            if(pstrucJ2534Msg[k].ulDataSize > 0)
            {
                j = 0;
                for(i = 0; i < pstrucJ2534Msg[k].ulDataSize && i < 15; i++)
                {
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534Msg[k].ucData[i]);                
                }

                if(i >= 15)
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
            }
#if defined (J2534_0404) || defined (J2534_0305)
            fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, szBuffer);
#endif
#ifdef J2534_0500
			fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, szBuffer);
#endif
        }
    }
#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
#ifdef J2534_0404
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
#endif
#ifdef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
#endif
		SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif

#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
#ifdef J2534_0500
		// Check for invalid Device ID
		if (gulDeviceID == 0)
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
			SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
			return (J2534_ERR_DEVICE_NOT_OPEN);
		}
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
#ifdef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
#endif
#ifndef J2534_0500
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
#endif        
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    // Check for NULL pointers
    if ((pstrucJ2534Msg == NULL) || (pulNumMsgs == NULL))
    {
        // Write to Log File.
#ifdef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
#endif
#ifndef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
#endif  
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }

    if (*pulNumMsgs == 0)
    {
        enJ2534Error = J2534_ERR_FAILED;
        SetLastErrorText("Main : Invalid NumMsgs value");
        // Write to Log File.
#ifdef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
#endif
#ifndef J2534_0500
		fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
#endif 
        return(enJ2534Error);
    }
    //SetProtocolID(ulChannelID);     // This allows for multiple CAN and ISO15765 channels to be connected to
#if defined (J2534_0404) || defined (J2534_0305)
    // Write Msg.
	// Check if the requested Channel ID is valid.
	if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
		SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}
	if (!gpclsDevice->IsChannelConnected(ulChannelID))
	{
		return(J2534_ERR_PIN_INVALID);
	}
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->vWriteMsgs(pstrucJ2534Msg,
                                                                                          pulNumMsgs,
                                                                                          ulTimeout))
                            != J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0500
	unsigned long ulPhysicalId = 0;
	ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ulChannelID);
	// Check if the requested Channel ID is valid.
	if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
		SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}
	if (!gpclsDevice->IsChannelConnected(ulPhysicalId))
	{
		return(J2534_ERR_PIN_INVALID);
	}
	if (ulChannelID > MAX_PHY_CHANNEL_ID)
	{
		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.m_pclsProtocolObject[GetPhysicalChannelIdFromLogicalId(ulChannelID)]->
			m_pclsLogicalChannel->IsLogChannelValid(ulChannelID))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
	}
	if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->vWriteMsgs(pstrucJ2534Msg,
														     							  pulNumMsgs,
														   								  0,
																						  ulChannelID
																								))
							!= J2534_STATUS_NOERROR)
#endif
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
#ifdef J2534_0500
			if (enJ2534Error == J2534_ERR_NO_FLOW_CONTROL)
				enJ2534Error = J2534_ERR_MSG_NOT_ALLOWED;
#endif
			SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
#ifndef J2534_0500
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
#else
		fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
#endif
        return(enJ2534Error);
    }

#ifndef J2534_0500
	fnWriteLogMsg("J2534Main.cpp", "PassThruWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
#else
	fnWriteLogMsg("J2534Main.cpp", "PassThruQueueMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
#endif
    // Write to Log File.

    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruStartPeriodicMsg()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function starts a given Periodic msg. on a specified
//                    channel.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruStartPeriodicMsg(
                                                  unsigned long ulChannelID,
                                                  PASSTHRU_MSG *pstrucJ2534Msg,
                                                  unsigned long *pulMsgID,
                                                  unsigned long ulTimeInterval)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    char        szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    if (gclsLog.m_pfdLogFile)
    {
        unsigned long   i, j;

        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");
        J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u TimeInterval=%u", ulChannelID, enProtocolID, ulTimeInterval);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "ulDataSize %lu", pstrucJ2534Msg->ulDataSize);

        szBuffer[0] = '\0';
        if(pstrucJ2534Msg->ulDataSize > 0)
        {
            j = 0;
            for(i = 0; i < pstrucJ2534Msg->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534Msg->ucData[i]);                
            }

            if(i >= 15)
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
        }
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, szBuffer);

    }
#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
#ifdef J2534_0500
	// Check for invalid Device ID
	if (gulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
    // Check for NULL pointers
    if ((pstrucJ2534Msg == NULL) || (pulMsgID == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
#if defined (J2534_0404) || defined (J2534_0305)
    // Check if the requested Channel ID is valid.
    if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
        return(J2534_ERR_INVALID_CHANNEL_ID);
    }

    if (!gpclsDevice->IsChannelConnected(ulChannelID))
    {
        return(J2534_ERR_PIN_INVALID);   
    }

    //SetProtocolID(ulChannelID);     // This allows for multiple CAN and ISO15765 channels to be connected to

    // Start Periodic Msg.
	if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->\
																				vStartPeriodicMsg(
																					pstrucJ2534Msg,
																					pulMsgID,
																					ulTimeInterval))
																				!= J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0500
		// Check if the requested Channel ID is valid.

		if (!gclsProtocolManager.IsChannelIDValid(GetPhysicalChannelIdFromLogicalId(ulChannelID)))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}

		if (!gpclsDevice->IsChannelConnected(GetPhysicalChannelIdFromLogicalId(ulChannelID)))
		{
			return(J2534_ERR_PIN_INVALID);
		}
		if (ulChannelID > MAX_PHY_CHANNEL_ID)
		{
			// Check if the requested Channel ID is valid.
			if (!gclsProtocolManager.m_pclsProtocolObject[GetPhysicalChannelIdFromLogicalId(ulChannelID)]->
				m_pclsLogicalChannel->IsLogChannelValid(ulChannelID))
			{
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
				SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
				return(J2534_ERR_INVALID_CHANNEL_ID);
			}
		}
		// Start Periodic Msg.
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[GetPhysicalChannelIdFromLogicalId(ulChannelID)]->\
											vStartPeriodicMsg(
												pstrucJ2534Msg,
												pulMsgID,
												ulTimeInterval,
#ifdef J2534_0500
												ulChannelID
#endif
												))
						!= J2534_STATUS_NOERROR)
#endif
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruStopPeriodicMsg()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function stops a given Periodic msg. on a specified
//                    channel.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruStopPeriodicMsg(
                                                  unsigned long ulChannelID,
                                                  unsigned long ulMsgID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    
//    char        szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");
    J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u", ulChannelID, enProtocolID);
#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif  
#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
#ifdef J2534_0500
	// Check for invalid Device ID
	if (gulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
	if (ulChannelID != 0 && ulChannelID < MAX_PHY_CHANNEL_ID)
	{
		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		// Stop Periodic Msg.
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->vStopPeriodicMsg(ulMsgID))
			!= J2534_STATUS_NOERROR)
		{
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			return(enJ2534Error);
		}
	}
	else
	{
#ifdef J2534_0500
		unsigned long ulPhysicalId = 0;
		ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ulChannelID);

		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		if (ulChannelID > MAX_PHY_CHANNEL_ID)
		{
			// Check if the requested Channel ID is valid.
			if (!gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->IsLogChannelValid
			(ulChannelID))
			{
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
				SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
				return(J2534_ERR_INVALID_CHANNEL_ID);
			}
		}
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->StopPeriodic(ulMsgID, ulChannelID))
			!= J2534_STATUS_NOERROR)
		{
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			return(enJ2534Error);
		}
#endif
	}

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruStartMsgFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function sets up a filter to filter Rx msgs.
//-----------------------------------------------------------------------------
#if (defined J2534_0404 || defined J2534_0305 )
extern "C" J2534ERROR WINAPI PassThruStartMsgFilter(
                                          unsigned long ulChannelID,
                                          J2534_FILTER  enumFilterType,
                                          PASSTHRU_MSG  *pstrucJ2534Mask,
                                          PASSTHRU_MSG  *pstrucJ2534Pattern,
                                          PASSTHRU_MSG  *pstrucJ2534FlowControl,
                                          unsigned long *pulFilterID)
#endif
#ifdef J2534_0500
extern "C" J2534ERROR WINAPI PassThruStartMsgFilter(
									      unsigned long  ulChannelID,
									      unsigned long  enumFilterType,
										  PASSTHRU_MSG  *pstrucJ2534Mask,
										  PASSTHRU_MSG  *pstrucJ2534Pattern,
										  unsigned long *pulFilterID)
#endif
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    char            szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR      enJ2534Error;
    unsigned long   i, j;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
    J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u", ulChannelID, enProtocolID);
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "enumFilterType=%d",enumFilterType);
#if (defined J2534_0404 || defined J2534_0305 )
#ifdef UD_TRUCK
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "PAR-TEST pMask=%p,pPat=%p,pFlow=%p",pstrucJ2534Mask,pstrucJ2534Pattern,pstrucJ2534FlowControl);
    // Fix up for UDTruck application 
    if ((pstrucJ2534FlowControl != NULL) && enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Fix up FlowControl->ulTxFlags for UDTruck application");
        if (pstrucJ2534Mask != NULL)
            pstrucJ2534Mask->ulTxFlags = pstrucJ2534FlowControl->ulTxFlags;
        if (pstrucJ2534Pattern != NULL)
            pstrucJ2534Pattern->ulTxFlags = pstrucJ2534FlowControl->ulTxFlags;
    }
#endif
#endif
    // Write to Log File.
    if (gclsLog.m_pfdLogFile)
    {
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Mask");
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "ProtocolID is %d", pstrucJ2534Mask->ulProtocolID);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "DataSize is %d", pstrucJ2534Mask->ulDataSize);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "TxFlags is %x", pstrucJ2534Mask->ulTxFlags);
        if(pstrucJ2534Mask->ulDataSize > 0)
        {
            j = 0;
            for(i = 0; i < pstrucJ2534Mask->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534Mask->ucData[i]);             
            }

            if(i >= 15)
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
        }
        else
        {
            sprintf_s(szBuffer, "DataSize is 0");
        }
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, szBuffer);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Pattern");
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "ProtocolID is %d", pstrucJ2534Pattern->ulProtocolID);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "DataSize is %d", pstrucJ2534Pattern->ulDataSize);
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "TxFlags is %x", pstrucJ2534Pattern->ulTxFlags);
        if(pstrucJ2534Pattern->ulDataSize > 0)
        {
            unsigned long i, j = 0;
            for(i = 0; i < pstrucJ2534Pattern->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534Pattern->ucData[i]);              
            }

            if(i >= 15)
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
        }
        else
        {
            sprintf_s(szBuffer, "DataSize is 0");
        }
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, szBuffer);

#if (defined J2534_0404 || defined J2534_0305 )
        if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
        {
            fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "FlowControl");
            fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "ProtocolID is %d", pstrucJ2534FlowControl->ulProtocolID);
            fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "DataSize is %d", pstrucJ2534FlowControl->ulDataSize);
            fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "TxFlags is %x", pstrucJ2534FlowControl->ulTxFlags);
            if(pstrucJ2534FlowControl->ulDataSize > 0)
            {
                j = 0;
                for(i = 0; i < pstrucJ2534FlowControl->ulDataSize && i < 15; i++)
                {
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pstrucJ2534FlowControl->ucData[i]);              
                }

                if(i >= 15)
                    j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "...");
            }
            else
            {
                sprintf_s(szBuffer, "DataSize is 0");
            }
            fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, szBuffer);
        }
#endif
    }

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
#ifdef J2534_0500
		// Check for invalid Device ID
		if (gulDeviceID == 0)
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
			SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
			return (J2534_ERR_DEVICE_NOT_OPEN);
		}
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    // Check for NULL pointers
    if ((pstrucJ2534Mask == NULL) || (pstrucJ2534Pattern == NULL) || (pulFilterID == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
#if (defined J2534_0404 || defined J2534_0305 )
    // Check for NULL pointers
    if ((pstrucJ2534FlowControl == NULL) && (enumFilterType == J2534_FILTER_FLOW_CONTROL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    // Check for NULL pointers
    if ((pstrucJ2534FlowControl != NULL) && (enumFilterType == J2534_FILTER_PASS || enumFilterType == J2534_FILTER_BLOCK))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        SetLastErrorTextID(J2534_ERR_INVALID_MSG);
        return (J2534_ERR_INVALID_MSG);
    }
#endif
#ifdef J2534_0500 //J2534 0500 supports only Pass and Block Filters 
	if (enumFilterType != PASS_FILTER && enumFilterType != BLOCK_FILTER)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_FILTER_TYPE_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_FILTER_TYPE_NOT_SUPPORTED);
		return (J2534_ERR_FILTER_TYPE_NOT_SUPPORTED);
	}
#endif
	if (pstrucJ2534Mask->ulDataSize != pstrucJ2534Pattern->ulDataSize ||
		pstrucJ2534Mask->ulTxFlags != pstrucJ2534Pattern->ulTxFlags)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
		SetLastErrorTextID(J2534_ERR_INVALID_MSG);
		return (J2534_ERR_INVALID_MSG);
	}
#if (defined J2534_0404 || defined J2534_0305 )
	if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
	{
#if defined(J2534_DEVICE_DBRIDGE0305) || defined(J2534_DEVICE_DPA50305)
#ifndef UD_TRUCK                   // This is a HACK
        pstrucJ2534FlowControl->ulTxFlags = pstrucJ2534Pattern->ulTxFlags;
#endif
#endif
		if (pstrucJ2534FlowControl->ulDataSize != pstrucJ2534Mask->ulDataSize ||
			pstrucJ2534FlowControl->ulDataSize != pstrucJ2534Pattern->ulDataSize ||
			pstrucJ2534FlowControl->ulTxFlags != pstrucJ2534Mask->ulTxFlags ||
			pstrucJ2534FlowControl->ulTxFlags != pstrucJ2534Pattern->ulTxFlags)
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			SetLastErrorTextID(J2534_ERR_INVALID_MSG);
			return (J2534_ERR_INVALID_MSG);
		}
	}

    if (enumFilterType < J2534_FILTER_PASS || enumFilterType > J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("J2534Main : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
#endif
    // Check if the requested Channel ID is valid.
    if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
        return(J2534_ERR_INVALID_CHANNEL_ID);
    }
   
    //SetProtocolID(ulChannelID);     // This allows for multiple CAN and ISO15765 channels to be connected to
#if (defined J2534_0404 || defined J2534_0305 )
    // Start Msg. Filter
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->\
                                vStartMsgFilter(
                                          enumFilterType,
                                          pstrucJ2534Mask,
                                          pstrucJ2534Pattern,
                                          pstrucJ2534FlowControl,
                                          pulFilterID))
                      != J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0500	

	if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->\
																	vStartMsgFilter(
																		(J2534_FILTER)enumFilterType,
																		pstrucJ2534Mask,
																		pstrucJ2534Pattern,
																		NULL,pulFilterID))
							!= J2534_STATUS_NOERROR)
#endif
{
        if (enJ2534Error != J2534_ERR_FAILED)
        {
			SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruStopMsgFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function gets a pointer to a specific Protocol 
//                    Object and uses the pointer to call the corressponding 
//                    virtual function of that class to stop filtering 
//                    messages for the given MsgID.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruStopMsgFilter(
                                                  unsigned long ulChannelID,
                                                  unsigned long ulFilterID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

//    char        szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR  enJ2534Error;


    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
    J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u", ulChannelID, enProtocolID);

#if defined (J2534_0404) || defined (J2534_0500)
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0500
	// Check for invalid Device ID
	if (gulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    // Check if the requested Channel ID is valid.
    if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
        return(J2534_ERR_INVALID_CHANNEL_ID);
    }
	
    // Start Msg. Filter
    if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->\
                                vStopMsgFilter(ulFilterID))
                      != J2534_STATUS_NOERROR)

    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetLastErrorTextID(enJ2534Error);
    return(J2534_STATUS_NOERROR);
} 

//-----------------------------------------------------------------------------
//  Function Name   : PassThruSetProgrammingVoltage()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function sets a programming voltage on a specific
//                    pin.
//-----------------------------------------------------------------------------
#ifdef J2534_0404
extern "C" J2534ERROR WINAPI PassThruSetProgrammingVoltage(unsigned long ulDeviceID,
					                                       unsigned long ulPin,
										                   unsigned long ulVoltage)
#endif

#ifdef J2534_0305
extern "C" J2534ERROR WINAPI PassThruSetProgrammingVoltage(unsigned long DeviceID,
												           unsigned long Voltage)
#endif

#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
/**
* 7.3.16 PassThruSetProgrammingVoltage
*
* This function shall set either a programming voltage on a single pin or short a single pin to ground on the designated
* Pass-Thru Device. If the function call is successful, the return value shall be STATUS_NOERROR and the associated pin
* shall be at the specified level. The default state for all pins shall be PIN_OFF. This function does not require a Channel ID,
* so it may be used after a call to PassThruOpen for the corresponding Pass-Thru Device.
*
* The Pass-Thru Interface contains exactly one programmable voltage generator and one programmable ground, which
* operate independently but cannot exist on the same pin (see Section 6.7 for more details). At any given time, the voltage
* can be routed to at-maximum one pin and the ground can be routed to at-maximum one pin. After applying a voltage or
* ground to a specific pin, that pin must be set to PIN_OFF before a new pin can be used. The pin must also be set to
* PIN_OFF when switching between voltage and ground on the same pin. However, there is no need to set a pin to
* PIN_OFF when selecting a different voltage on the same pin.
*
* The application programmer shall ensure that voltages are not applied to any pins that may cause damage to a vehicle or
* ECU. PassThruSetProgrammingVoltage does NOT protect from usage that might result in damage to a vehicle or ECU.
*
* SAE J2610 channels are unique, as they require the programmable voltage generator to be on the Rx pin. For SAE J2610
* channels ONLY, the programming voltage may be manually set with this function (for an immediate affect) on the Rx pin
* without getting the return value ERR_PIN_IN_USE. Otherwise, the programmable voltage generator shall be
* automatically controlled with the SCI_TX_VOLTAGE bit in <TxFlags> of the PASSTHRU_MSG structure during a call to
* PassThruQueueMsgs. However, if the programmable voltage generator is in use at that time, the call to
* PassThruQueueMsgs shall fail (see PassThruQueueMsgs for more details).
*
* Attempting to set an unused pin to PIN_OFF shall result in the return value STATUS_NOERROR. Attempting to set a pin
* that is already in use for a different purpose (except as noted above) shall result in the return value ERR_PIN_IN_USE.
* Attempting to set a voltage on a pin other than 0, 6, 9, 11, 12, 13, 14, or 15 of the J1962_CONNECTOR shall result in the
* return value ERR_PIN_NOT_SUPPORTED. Attempting to set a ground on a pin other than 15 of the
* J1962_CONNECTOR shall result in the return value ERR_PIN_NOT_SUPPORTED. Attempting to set
* <ResourceStruct.NumOfResources> to a value other 1 shall result in the return value ERR_PIN_NOT_SUPPORTED.
* Attempting to set a voltage on a valid pin when the voltage is currently being applied to another pin shall result in the
* return value ERR_VOLTAGE_IN_USE. Attempting to set a voltage that is out of range on a valid pin shall result in the
* return value ERR_EXCEEDED_LIMIT. If <ResourceStruct.ResourceListPtr> is NULL, the return value shall be
* ERR_NULL_PARAMETER.
*
* If the corresponding Pass-Thru Device is not currently open, the return value shall be ERR_DEVICE_NOT_OPEN. If
* <DeviceID> is not valid, the return value shall be ERR_INVALID_DEVICE_ID. If the Pass-Thru Device is not
* currently connected or was previously disconnected without being closed, the return value shall be
* ERR_DEVICE_NOT_CONNECTED. If this function call is not supported for this device, then the return value shall be
* ERR_NOT_SUPPORTED. The return value ERR_FAILED is a mechanism to return other failures, but this is intended for
* use during development. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED or
* ERR_NOT_SUPPORTED.
*
* Parameters:
*   <DeviceID>                          is an input, set by the application that contains the handle to the Pass-Thru Device whose programmable
*                                       voltage generator / programmable ground will be set. The Device ID was assigned by a previous call to
*                                       PassThruOpen.
*   <ResourceStruct>                    is an input, set by the application, that identifies the connector and pin on which the programmable
*                                       voltage generator / programmable ground will be set. This structure is defined in Section 9.18. Valid
*                                       values for each of the structure elements are:
*                                         <ResourceStruct.Connector> shall be set to J1962_CONNECTOR
*                                         <ResourceStruct.NumOfResources> shall be set to 1
*                                         <ResourceStruct.ResourceListPtr> shall point to an unsigned long that contains one of these values:
*                                           0 � Auxiliary output pin (the banana jack identified in Section 6.7)
*                                           6 � Pin 6 on the SAE J1962 connector.
*                                           9 � Pin 9 on the SAE J1962 connector.
*                                           11 � Pin 11 on the SAE J1962 connector.
*                                           12 � Pin 12 on the SAE J1962 connector.
*                                           13 � Pin 13 on the SAE J1962 connector.
*                                           14 � Pin 14 on the SAE J1962 connector.
*                                           15 � Pin 15 on the SAE J1962 connector (short to ground only).
* Return Values:
*   ERR_CONCURRENT_API_CALL             A J2534 API function has been called before the previous J2534 function call has completed
*   ERR_DEVICE_NOT_OPEN                 PassThruOpen has not successfully been called
*   ERR_INVALID_DEVICE_ID               Invalid <DeviceID> value
*   ERR_DEVICE_NOT_CONNECTED            Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has, at some point,
*                                       failed to communicate with the Pass-Thru Device � even though it may not currently be disconnected.
*   ERR_NOT_SUPPORTED                   Device does not support this API function for the associated <DeviceID>. A fully compliant SAE J2534-1
*                                       Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.
*   ERR_PIN_NOT_SUPPORTED               One or more of the following is either invalid or unknown: The pin number specified by <ResourceStruct.ResourceListPtr>,
*                                       the connector specified by <ResourceStruct.Connector>, or the number of resources.
*   ERR_VOLTAGE_IN_USE                  Programming voltage is currently being applied to another pin
*   ERR_PIN_IN_USE                      <PinNumber> specified is currently in use (either for voltage, ground, or by another channel)
*   ERR_EXCEEDED_LIMIT                  Exceeded the allowed limits for programmable voltage generator
*   ERR_NULL_PARAMETER                  NULL pointer supplied where a valid pointer is required
*   ERR_FAILED                          Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru
*                                       Interface shall never return ERR_FAILED.
*   STATUS_NOERROR                      Function call was successful
*/
extern "C" J2534ERROR WINAPI PassThruSetProgrammingVoltage(unsigned long ulDeviceID,
														   RESOURCE_STRUCT ResourceList,
														   unsigned long ulVoltage)

#endif

{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
	J2534ERROR          enJ2534Error;
    bool    bConnected;

#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
	//Attempting to set <ResourceList.NumOfResources> to a value other 1 shall result in the return value ERR_PIN_NOT_SUPPORTED.
	if (ResourceList.Connector!= J1962_CONNECTOR)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
		return (J2534_ERR_NOT_SUPPORTED);
	}
	if (ResourceList.NumOfResources != 1)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
		return (J2534_ERR_NOT_SUPPORTED);
	}
	if (ResourceList.ResourceListPtr == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		return (J2534_ERR_NULLPARAMETER);
	}
	unsigned long ulPin =  *(ResourceList.ResourceListPtr);
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "Start Pin %lu Voltage %lu", ulPin, ulVoltage);
#endif 

#ifdef J2534_0404
	// Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "Start Pin %lu Voltage %lu", ulPin, ulVoltage);
#endif

#ifdef J2534_0305
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "Device Id %lu Voltage %lu", DeviceID, Voltage);
#endif
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500 )// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0500
	// Check for invalid Device ID
	if (gulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

#if defined (J2534_0404) || defined (J2534_0500)
    if ((enJ2534Error = gpclsDevice->vProgrammingVoltage(ulDeviceID,ulPin,ulVoltage)) 
                      == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vProgrammingVoltage(gulDeviceID,ulPin,ulVoltage)) 
                      == J2534_STATUS_NOERROR)
#endif
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        SetLastErrorTextID(enJ2534Error);
    }
    else
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }
    }
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", 
    enJ2534Error == J2534_STATUS_NOERROR ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadVersion()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Dll & Api Version Numbers.
//  Description     : This function gets the Dll Version Number and Api Version
//                    Number.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadVersion(
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                                        unsigned long ulDeviceID,
#endif
                                        char *pchFirmwareVersion,
                                        char *pchDllVersion,
                                        char *pchApiVersion)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR          enJ2534Error;
    bool    bConnected;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_COMMENT, "Start");
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0500
	// Check for invalid Device ID
	if (gulDeviceID == 0)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    if (pchFirmwareVersion == NULL || pchDllVersion == NULL || pchApiVersion == NULL) 
    { 
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion() PTR NULL", DEBUGLOG_TYPE_ERROR, 
            "FMVer %s, DLLVer %s, APIVer %s", (pchFirmwareVersion == NULL) ? "NULL" : "OK", (pchDllVersion == NULL) ? "NULL" : "OK", (pchApiVersion == NULL) ? "NULL" : "OK");

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NULLPARAMETER; 
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NULLPARAMETER); 
    } 
    
#ifdef J2534_0404
    if ((enJ2534Error = gpclsDevice->vGetRevision(ulDeviceID,(char*)pchFirmwareVersion,(char*)pchDllVersion,(char*)pchApiVersion)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vGetRevision(gulDeviceID,(char*)pchFirmwareVersion,(char*)pchDllVersion,(char*)pchApiVersion)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
		if ((enJ2534Error = gpclsDevice->vGetRevision(gulDeviceID, (char*)pchFirmwareVersion, (char*)pchDllVersion, (char*)pchApiVersion)) == J2534_STATUS_NOERROR)
#endif
    {
#ifdef J2534_0305
        strcpy_s(pchApiVersion, SMALL_TEXT_SIZE, "03.05");
#endif
        // Write to Log File.
        if (gclsLog.m_pfdLogFile)
        {
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_COMMENT, "Firmware version %s", pchFirmwareVersion);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_COMMENT, "DLL version %s", pchDllVersion);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_COMMENT, "Api version %s", pchApiVersion);
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(enJ2534Error);
        return enJ2534Error;
    }
    else
    {
                      
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }

    }
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
    return(enJ2534Error);
}



//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadHWVersion()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns HW Version.
//  Description     : This function gets the HW Version.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadHWVersion(
                                        unsigned long ulDeviceID,
                                        char *pchHWVersion)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR          enJ2534Error;
//    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    bool    bConnected;
    
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_COMMENT, "Start");
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    if (pchHWVersion == NULL) 
    { 
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NULLPARAMETER; 
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NULLPARAMETER); 
    } 
    
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    if ((enJ2534Error = gpclsDevice->vGetHWVersion(ulDeviceID,(char*)pchHWVersion)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vGetHWVersion(gulDeviceID,(char*)pchHWVersion)) == J2534_STATUS_NOERROR)
#endif
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_COMMENT, "HW Version %s", pchHWVersion);
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(enJ2534Error);
        return enJ2534Error;
    }
    else
    {
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }

    }
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadHWVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

    return(enJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadKRSerialNum()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Serial Numbers.
//  Description     : This function gets the Serial
//                    Number.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadKRSerialNum(
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                                        unsigned long ulDeviceID,
#endif
                                        char *pchKRSerialNum)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR          enJ2534Error;
//    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    bool    bConnected;
    
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_COMMENT, "Start");
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    if (pchKRSerialNum == NULL) 
    { 
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NULLPARAMETER; 
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NULLPARAMETER); 
    } 
    
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    if ((enJ2534Error = gpclsDevice->vGetSerialNum(ulDeviceID,(char*)pchKRSerialNum)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vGetSerialNum(gulDeviceID,(char*)pchKRSerialNum)) == J2534_STATUS_NOERROR)
#endif
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_COMMENT, "KR Serial Number %s", pchKRSerialNum);
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(enJ2534Error);
        return enJ2534Error;
    }
    else
    {
                      
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }

    }
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRSerialNum()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

    return(enJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadSerialNum()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Serial Numbers.
//  Description     : This function gets the Serial
//                    Number.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadSerialNum(
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                                        unsigned long ulDeviceID,
#endif
                                        char *pchSerialNum)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    J2534ERROR enJ2534Error;
    char TmpBuff[J2534MAIN_ERROR_TEXT_SIZE];    // Should be big enough for S/N

    // Use existing private function to do the heavy lifting. We will strip of leading "SN" and 0's
    enJ2534Error = PassThruReadKRSerialNum(
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                            ulDeviceID,
#endif
                            TmpBuff);
    if (enJ2534Error == J2534_STATUS_NOERROR)
    {
        // 
        if (strncmp(TmpBuff, "SN", 2) == 0) // Does it start with "SN"?
        {
            // Starts with "SN" as we expected
            for (int i = 2; TmpBuff[i] != '\0'; i++)
            {
                if (TmpBuff[i] != '0')
                {
                    strcpy_s(pchSerialNum, 22, (const char *) &TmpBuff[i]);   // We have removed leading "SN" and '0's
                    break;
                }
            }
        }
        else
        {
            // Does not start with "SN" as we expected, something is wrong
            SetLastErrorText("%s Invalid SN response from tool", TmpBuff);
            enJ2534Error = J2534_ERR_FAILED;
        }
    }
    return(enJ2534Error);
}


//-----------------------------------------------------------------------------
//  Function Name   : PassThruWriteKRString()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Error Code.
//  Description     : This function writes the string onto the device.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruWriteKRString(
#if (defined J2534_0404 || defined J2534_0500 )// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                                        unsigned long ulDeviceID,
#endif
                                        char *pchKRString)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR          enJ2534Error;
//    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    bool    bConnected;
    
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_COMMENT, "Start");
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500 )// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    if (pchKRString == NULL) 
    { 
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NULLPARAMETER; 
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NULLPARAMETER); 
    } 
    
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    if ((enJ2534Error = gpclsDevice->vWriteKRString(ulDeviceID,(char*)pchKRString)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vWriteKRString(gulDeviceID,(char*)pchKRString)) == J2534_STATUS_NOERROR)
#endif
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "KR String %s", pchKRString);
        fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_STATUS_NOERROR);
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
	    SetLastErrorTextID(enJ2534Error);
        return enJ2534Error;
    }
    else
    {
                      
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }

    }
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);

    return(enJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruReadKRString()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Error Code.
//  Description     : This function reads the string from the device.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruReadKRString(
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
                                        unsigned long ulDeviceID,
#endif
                                        char *pchKRString)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    J2534ERROR          enJ2534Error;
//    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    bool    bConnected;
    
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_COMMENT, "Start");
    bConnected = false;

#if (defined J2534_0404 || defined J2534_0500 )// For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    // Check for invalid Device ID
    if ((ulDeviceID != gulDeviceID) || (gpclsDevice == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    if (pchKRString == NULL) 
    { 
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NULLPARAMETER; 
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_NULLPARAMETER); 
    } 
    
#if (defined J2534_0404 || defined J2534_0500) // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500)
    if ((enJ2534Error = gpclsDevice->vReadKRString(ulDeviceID,(char*)pchKRString)) == J2534_STATUS_NOERROR)
#endif
#ifdef J2534_0305
    if ((enJ2534Error = gpclsDevice->vReadKRString(gulDeviceID,(char*)pchKRString)) == J2534_STATUS_NOERROR)
#endif
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "KR String %s", pchKRString);
        fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_STATUS_NOERROR);
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(enJ2534Error);
        return enJ2534Error;
    }
    else
    {
                      
        if (enJ2534Error != J2534_ERR_FAILED)
        {
            SetLastErrorTextID(enJ2534Error);
        }

    }
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);

    return(enJ2534Error);
}

/*
//-----------------------------------------------------------------------------
//  Function Name   : PassThruLogging()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns Error Code.
//  Description     : This function turns the logging on or off.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruLogging(bool bLogging)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    J2534ERROR          enJ2534Error;
    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    bool    bConnected;
    char tempbuffer[MAX_PATH];
    CString strLogFilepath;
    
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruLogging()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (bLogging && 
        theApp.m_J2534Registry.dwLogging != 
        DEBUGLOG_TYPE_DATA | DEBUGLOG_TYPE_COMMENT | DEBUGLOG_TYPE_ERROR)
    {
        // Turn logging on
        strcpy_s(tempbuffer, theApp.m_J2534Registry.chLoggingDirectory);
        strLogFilepath = (LPCSTR)tempbuffer;
        theApp.m_J2534Registry.dwLogging = 
            (DEBUGLOG_TYPE_DATA | DEBUGLOG_TYPE_COMMENT | DEBUGLOG_TYPE_ERROR);
        gclsLog.Open(strLogFilepath + "\\J2534KRHtWire.csv", 
            theApp.m_J2534Registry.dwLogging);
        theApp.WriteToRegistry(&theApp.m_J2534Registry);
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruLogging()", DEBUGLOG_TYPE_ERROR, "Turned Logging On");
    }
    else if (!bLogging && theApp.m_J2534Registry.dwLogging != 0)
    {
        // Turn logging off
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruLogging()", DEBUGLOG_TYPE_ERROR, "Turned Logging Off");
        gclsLog.Close();
        theApp.m_J2534Registry.dwLogging = 0;
        theApp.WriteToRegistry(&theApp.m_J2534Registry);
    }

    enJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruLogging()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
    return(enJ2534Error);
}
*/

//-----------------------------------------------------------------------------
//  Function Name   : PassThruGetLastError()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Description.
//  Description     : This function gets an Error ID and matches it to an error 
//                    string.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruGetLastError(char *pchErrorDescription)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

    char    szBufferGetLastError[J2534MAIN_ERROR_TEXT_SIZE];
    

//  *pchErrorDescription = 0;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruGetLastError()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check for NULL pointers
    if (pchErrorDescription == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruGetLastError()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    memset(szBufferGetLastError, 0, J2534MAIN_ERROR_TEXT_SIZE);
    GetLastErrorText(szBufferGetLastError, J2534MAIN_ERROR_TEXT_SIZE);
    strcpy_s(pchErrorDescription, J2534MAIN_ERROR_TEXT_SIZE, szBufferGetLastError);

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruGetLastError()", DEBUGLOG_TYPE_ERROR, "returned %s", pchErrorDescription);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : PassThruIoctl()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function is used to read and write all the protocol 
//                    hardware and software configuration parameters for a 
//                    given ulIoctlID.
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI PassThruIoctl(
                                          unsigned long ulChannelID,
                                          J2534IOCTLID enumIoctlID,
                                          void *pInput,
                                          void *pOutput)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());

//    char    szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
    J2534ERROR  enJ2534Error = J2534_ERR_INVALID_CHANNEL_ID;
    bool    bConnected;

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "Start IoctlID %d", enumIoctlID);
    bConnected = false;
#ifdef J2534_0500
	if (enumIoctlID == BUS_ON)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
		return (J2534_ERR_NOT_SUPPORTED);
	}
#endif
#ifdef J2534_0305
    // Validate IoctlID
    if (((enumIoctlID < GET_CONFIG) || (enumIoctlID > READ_PROG_VOLTAGE)) &&
        (enumIoctlID != READ_MEMORY) && (enumIoctlID != WRITE_MEMORY))
    {
        enJ2534Error = J2534_ERR_INVALID_IOCTL_ID;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_IOCTL_ID);
    }
#endif
    
    if (
#ifndef J2534_0500
		(enumIoctlID == READ_VBATT)
#else
		(enumIoctlID == READ_PIN_VOLTAGE)
#endif

|| (enumIoctlID == READ_VWALLJACK) || 
        (enumIoctlID == READ_VPWRVEH) || (enumIoctlID == TURN_AUX_OFF) || 
        (enumIoctlID == TURN_AUX_ON) || (enumIoctlID == READ_MEMORY) || 
        (enumIoctlID == WRITE_MEMORY) || (enumIoctlID == START_PULSE) ||
        (enumIoctlID == GET_DEVICE_INFO) || (enumIoctlID == GET_PROTOCOL_INFO) ||
#ifdef J2534_DEVICE_DBRIDGE
        (enumIoctlID == SET_DEVICE_CONFIG) || (enumIoctlID == GET_DEVICE_CONFIG) ||
#endif
        (enumIoctlID == READ_VIGNITION))
    {
// THIS HACK IS FOR AN SCI MODULE ONLY
#if defined (J2534_0404) || defined (J2534_0500)
        // Check for invalid Device ID
        if ((ulChannelID != gulDeviceID) || (gpclsDevice == NULL))
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);

            SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
            return (J2534_ERR_INVALID_DEVICE_ID);
        }
#endif
    }
#ifdef J2534_DEVICE_NETBRIDGE
    if ((enumIoctlID == GET_TIMESTAMP) || (enumIoctlID == GET_SERIAL_NUMBER))
    {
// THIS HACK IS FOR AN SCI MODULE ONLY
#ifdef J2534_0404
        // Check for invalid Device ID
        if ((ulChannelID != gulDeviceID) || (gpclsDevice == NULL))
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);

            SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
            return (J2534_ERR_INVALID_DEVICE_ID);
        }
#endif
    }
#endif
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    // Check for invalid Device ID
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif
#ifdef J2534_0305
    if (gpclsDevice==NULL)
    {
        // Create an appropriate Device Object because device was not opened before coming here
        gpclsDevice = new CDeviceCayman(&gclsLog, &gclsDataLog);

        // Check if Device Object is created.
        if (gpclsDevice == NULL)
        {
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "Device DPA not created");
            // Set right text to be retreived when PassThruGetLastError() called.
            SetLastErrorText("Main : Device DPA not created");
            return(J2534_ERR_FAILED);
        }
        // Open the Device Connection and return the Device ID otherwise error out.
        if ((enJ2534Error = gpclsDevice->vOpenDevice(&theApp.m_J2534Registry)) 
                          == J2534_STATUS_NOERROR)
        {
            // Return Device ID and set the Device Open flag.
            gulDeviceID++;
        }
        else
        {
              
            if (enJ2534Error != J2534_ERR_FAILED)
            {
                SetLastErrorTextID(enJ2534Error);
            }
            // Write to Log File.
            fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            delete gpclsDevice;
            gpclsDevice = NULL;
            return(enJ2534Error);
        }
    }
    else
        bConnected = true;      // Indicate device opened prior to coming here
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);

        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }

    // If READ_VBATT, do it because it requires no connection

    if (
#ifndef J2534_0500
		(enumIoctlID == READ_VBATT) 
#else
		(enumIoctlID == READ_PIN_VOLTAGE)
#endif
		|| (enumIoctlID == READ_VWALLJACK) || 
        (enumIoctlID == GET_DEVICE_INFO) || 
        (enumIoctlID == READ_VPWRVEH) || (enumIoctlID == READ_VIGNITION))
    {
        // Make sure pOutput is not NULL
        if (pOutput == NULL)
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        // READ_VBATT, READ_VWALLJACK, READ_VPWRVEH,or READ_VIGNITION
        // NOTE: ulChannelID = 0 and pInput = NULL
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, NULL, pOutput);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }

    if ((enumIoctlID == TURN_AUX_OFF) || (enumIoctlID == TURN_AUX_ON))
    {
        // TURN_AUX_OFF or TURN_AUX_ON
        // NOTE: ulChannelID = 0 and pInput = NULL, and pOutput = NULL
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, NULL, NULL);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }

    // If READ_MEMORY, WRITE_MEMORY, or START_PULSE, do it because it requires no connection
    if ((enumIoctlID == READ_MEMORY) || (enumIoctlID == WRITE_MEMORY))
    {
        // Make sure pInput is not NULL
        if (pInput == NULL)
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        // READ_MEMORY or WRITE_MEMORY
        // NOTE: ulChannelID = 0 and pOutput = NULL
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, pInput, NULL);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }
    if ((enumIoctlID == START_PULSE)
         || (enumIoctlID == GET_PROTOCOL_INFO) 
        )
    {
        // Make sure pInput is not NULL
        if ((pInput == NULL) || (pOutput == NULL))
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        // NOTE: ulChannelID = 0
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, pInput, pOutput);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }
    if (enumIoctlID == READ_PROG_VOLTAGE)
    {
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_NOT_SUPPORTED;
        SetLastErrorTextID(enJ2534Error);
        return(enJ2534Error);
    }
#ifdef J2534_DEVICE_NETBRIDGE    
    if (enumIoctlID == DEVICE_RESET)
    {
        // NOTE: ulChannelID = 0
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, NULL, NULL);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }
    if ((enumIoctlID == GET_TIMESTAMP) || (enumIoctlID == GET_SERIAL_NUMBER))
    {
        // Make sure pOutput is not NULL
        if ((pOutput == NULL))
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        // NOTE: ulChannelID = 0
        enJ2534Error = gpclsDevice->vIoctl(0, enumIoctlID, NULL, pOutput);
        if (enJ2534Error == J2534_ERR_FAILED)
        {
            SetLastErrorText("Internal error occurred.");
        }
        else
        {
            SetLastErrorTextID(enJ2534Error);
        }
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        return(enJ2534Error);
    }
#endif
#ifdef J2534_DEVICE_DBRIDGE
    // J2534-2 extended PassThruIoctl
    if ((enumIoctlID == SET_DEVICE_CONFIG) || (enumIoctlID == GET_DEVICE_CONFIG))
    {
        // Check NULL pointer
        if (pInput == NULL)
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        // Check ulNumOfParams
        if (((SCONFIG_LIST *)pInput)->ulNumOfParams == 0)
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorText("Invalid NumOfParams value");
            return(J2534_ERR_FAILED);
        }
        // Check if at least one of the parameters is data storage
        int i = 0;
        unsigned long ulCount = 0;
        unsigned long ulNumOfParams = ((SCONFIG_LIST *)pInput)->ulNumOfParams;
        bool bIsDataStorage = false;
        bool bIsOther = false;
        SCONFIG *pSconfig = ((SCONFIG_LIST *)pInput)->pConfigPtr;
        for (ulCount = 0; ulCount < ulNumOfParams; ulCount++)
        {
            if ((pSconfig->Parameter >= NON_VOLATILE_STORE_1) && (pSconfig->Parameter <= NON_VOLATILE_STORE_10))
            {
                // We have at least one data storage parameter
                bIsDataStorage = true;
            }
            else
            {
                //Support for additional non-protocol-specific parameters
                bIsOther = true;
            }
            pSconfig++;
        }
        // Get the values in the device 
        if(enumIoctlID == GET_DEVICE_CONFIG)
        {
            // Support for non-volatile and re-writable data storage
            if (bIsDataStorage)
            {
                enJ2534Error = J2534_STATUS_NOERROR;
                // Get the values from the device, if we do not have them yet
                if(!gbIsDataStorageAvailable)
                {
                    enJ2534Error = gpclsDevice->vDataStorage(enumIoctlID, gulDataStorage, sizeof(gulDataStorage)/sizeof(gulDataStorage)[0]);
                    if (enJ2534Error != J2534_STATUS_NOERROR)
                    {
                        memset(gulDataStorage, 0, sizeof(gulDataStorage)); 
                        gbIsDataStorageAvailable = false;
                        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
                        return(enJ2534Error);
                    }
                    gbIsDataStorageAvailable = true;
                }
                // Return to the user the requested storage parameters values
                pSconfig = ((SCONFIG_LIST *)pInput)->pConfigPtr;
                for (ulCount = 0; ulCount < ulNumOfParams; ulCount++)
                {
                    // For storage parameters
                    if ((pSconfig->Parameter >= NON_VOLATILE_STORE_1) && (pSconfig->Parameter <= NON_VOLATILE_STORE_10))
                    {
                        i = ((int) pSconfig->Parameter) - ((int) NON_VOLATILE_STORE_1); 
                        pSconfig->ulValue = gulDataStorage[i];

                        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "Parameter=%d, Value=%lu", 
                            (int) pSconfig->Parameter, pSconfig->ulValue);
                    }
                    pSconfig++;
                }
                SetLastErrorTextID(enJ2534Error);
            }
            else
            {
                // Add here the support for additional non-protocol-specific parameters - if(bIsOther)
                SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
                Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
                return(J2534_ERR_NOT_SUPPORTED);
            }
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            return(enJ2534Error);
        }// End GET_DEVICE_CONFIG

        // Set the values in the device 
        if (enumIoctlID == SET_DEVICE_CONFIG)
        {
            if (bIsDataStorage)
            {
                if(!gbIsDataStorageAvailable)
                { 
                    // Get them first to prevent wiping out the rest of the values
                    enJ2534Error = gpclsDevice->vDataStorage(GET_DEVICE_CONFIG, gulDataStorage, sizeof(gulDataStorage)/sizeof(gulDataStorage)[0]);
                    if((enJ2534Error != J2534_STATUS_NOERROR) && (enJ2534Error != J2534_ERR_BUFFER_EMPTY))
                    {
                        memset(gulDataStorage, 0, sizeof(gulDataStorage)); 
                        gbIsDataStorageAvailable = false;
                        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
                        return(enJ2534Error);
                    }
                    // This is the first time. The user may set only some of the storage, so set the rest with 0xFFFFFFFF
                    // (Not clarified in J2534-2)
                    if(enJ2534Error == J2534_ERR_BUFFER_EMPTY)
                    {
                        for (i = 0; i < sizeof(gulDataStorage)/sizeof(gulDataStorage[0]); i++)
                        {  
                            gulDataStorage[i] = -1;
                        }
                        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "First time Set Data Storage");
                    }
                    gbIsDataStorageAvailable = true;
                }
                // Add the storage parameters values set by the user to gucDataStorage
                pSconfig = ((SCONFIG_LIST *)pInput)->pConfigPtr;
                for (ulCount = 0; ulCount < ulNumOfParams; ulCount++)
                {
                    if ((pSconfig->Parameter >= NON_VOLATILE_STORE_1) && (pSconfig->Parameter <= NON_VOLATILE_STORE_10))
                    {
                        i = ((int) pSconfig->Parameter) - ((int) NON_VOLATILE_STORE_1); 
                        gulDataStorage[i] = pSconfig->ulValue;

                        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "Parameter=%d, Value=%lu", 
                            (int) pSconfig->Parameter, pSconfig->ulValue);
                    }
                    pSconfig++;
                }
                //Set the values 
                enJ2534Error = gpclsDevice->vDataStorage(enumIoctlID, gulDataStorage, sizeof(gulDataStorage)/sizeof(gulDataStorage)[0]);
                if (enJ2534Error == J2534_ERR_FAILED)
                {
                    // Unknown state
                    memset(gulDataStorage, 0, sizeof(gulDataStorage)); 
                    gbIsDataStorageAvailable = false;
                    SetLastErrorText("Internal error occurred.");
                    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
                    return(enJ2534Error);
                }
                SetLastErrorTextID(enJ2534Error);
            }
            else
            {
                // Add here the support for additional non-protocol-specific parameters - if(bIsOther)
                SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
                Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
                return(J2534_ERR_NOT_SUPPORTED);
            }
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            return(enJ2534Error);
        }// End SET_DEVICE_CONFIG
    }
#endif
  
#ifdef J2534_DEVICE_DBRIDGE
    // J2534-2 extended PassThruIoctl: Repeat Messaging
    if (enumIoctlID == START_REPEAT_MESSAGE)
    {
        // Do the general err check:
        // - Make sure pInput and pOutput are not NULL
        if ((pInput == NULL) || (pOutput == NULL))
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
        REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
        // - Check Time Interval
        if((pRepeatMsgSetup->TimeInterval < J2534_REPEAT_MSG_TIME_INTERVAL_MIN) || 
           (pRepeatMsgSetup->TimeInterval > J2534_REPEAT_MSG_TIME_INTERVAL_MAX)) 
        {
            SetLastErrorTextID(J2534_ERR_INVALID_TIME_INTERVAL);
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            return(J2534_ERR_INVALID_TIME_INTERVAL);
        }
        // - Check Condition
        if((pRepeatMsgSetup->Condition != J2534_REPEAT_MSG_CONDITION_0) && 
           (pRepeatMsgSetup->Condition != J2534_REPEAT_MSG_CONDITION_1)) 
        {
            SetLastErrorTextID(J2534_ERR_INVALID_IOCTL_VALUE);
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            return(J2534_ERR_INVALID_IOCTL_VALUE);
        }
        // - Check Size 
        if((pRepeatMsgSetup->RepeatMsgData[0].ulDataSize > J2534_REPEAT_MSG_MAX_SIZE) ||
           (pRepeatMsgSetup->RepeatMsgData[0].ulDataSize == 0) ||
           (pRepeatMsgSetup->RepeatMsgData[1].ulDataSize != pRepeatMsgSetup->RepeatMsgData[2].ulDataSize) ||
           (pRepeatMsgSetup->RepeatMsgData[1].ulDataSize == 0))
        {
            SetLastErrorTextID(J2534_ERR_INVALID_MSG);
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            return(J2534_ERR_INVALID_MSG);
        } 
        // Log this
        J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulChannelID];
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl", DEBUGLOG_TYPE_COMMENT, "ChannelID=%u ProtocolID=%u TimeInterval=%u Condition=%lu", 
            ulChannelID, enProtocolID, pRepeatMsgSetup->TimeInterval, pRepeatMsgSetup->Condition);
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "RepeatMsg Size %lu", pRepeatMsgSetup->RepeatMsgData[0].ulDataSize);
        char szBuffer[J2534MAIN_ERROR_TEXT_SIZE];
        unsigned long j = 0, i = 0;
        for(i = 0; i < pRepeatMsgSetup->RepeatMsgData[0].ulDataSize; i++)
        {
            j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pRepeatMsgSetup->RepeatMsgData[0].ucData[i]);                
        }
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "RepeatMsg %s", szBuffer);
        j = 0;
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "Response Size %lu", pRepeatMsgSetup->RepeatMsgData[1].ulDataSize);
        for(i = 0; i < pRepeatMsgSetup->RepeatMsgData[1].ulDataSize; i++)
        {
            j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pRepeatMsgSetup->RepeatMsgData[1].ucData[i]);                
        }
        j = 0;
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT,"Response Mask %s", szBuffer);
        for(i = 0; i < pRepeatMsgSetup->RepeatMsgData[2].ulDataSize; i++)
        {
            j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j, "%02X ",pRepeatMsgSetup->RepeatMsgData[2].ucData[i]);                
        }
        fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT,"Response Ptrn %s", szBuffer);
    }
    if (enumIoctlID == QUERY_REPEAT_MESSAGE)
    {
        // Make sure pInput and pOutput are not NULL
        if ((pInput == NULL) || (pOutput == NULL))
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
    }
    if (enumIoctlID == STOP_REPEAT_MESSAGE)
    {
        // Make sure pInput and pOutput are not NULL
        if (pInput == NULL)
        {
            Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
            SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
            return(J2534_ERR_NULLPARAMETER);
        }
    }
#endif
    // Validate IoctlID
    if (((enumIoctlID < GET_CONFIG) || (enumIoctlID > READ_PROG_VOLTAGE)) &&
//      ((enumIoctlID < SW_CAN_HS) || (enumIoctlID > BECOME_MASTER)))
        ((enumIoctlID < SW_CAN_HS) || (enumIoctlID > TEARDOWN_CONNECTION)) &&
        ((enumIoctlID < CAN_SET_BTR) || (enumIoctlID > CAN_SET_ERROR_REPORTING)) &&
        (enumIoctlID != CAN_SET_INTERNAL_TERMINATION) &&
        (enumIoctlID != DEVICE_RESET))
    {
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_INVALID_IOCTL_ID;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_IOCTL_ID);
    }

    // Validate IoctlID
    if ((enumIoctlID == GET_TIMESTAMP) || (enumIoctlID == SET_TIMESTAMP) || 
        (enumIoctlID == GET_SERIAL_NUMBER) || (enumIoctlID == DEVICE_RESET))
    {
        Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
        enJ2534Error = J2534_ERR_INVALID_IOCTL_ID;
        SetLastErrorTextID(enJ2534Error);
        return(J2534_ERR_INVALID_IOCTL_ID);
    }

	if (ulChannelID != 0 && ulChannelID < MAX_PHY_CHANNEL_ID)
	{
		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulChannelID))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulChannelID]->vIoctl(enumIoctlID,
			pInput,
			pOutput))
			!= J2534_STATUS_NOERROR)
		{
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
			return(enJ2534Error);
		}
	}
	else
	{
#ifdef J2534_0500
		unsigned long ulPhysicalId = 0;
		ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ulChannelID);

		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		if ((enJ2534Error = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->vIoctl(enumIoctlID,
																							pInput,
																							pOutput,
																							ulChannelID))
																							!= J2534_STATUS_NOERROR)
		{
			if (enJ2534Error != J2534_ERR_FAILED)
			{
				SetLastErrorTextID(enJ2534Error);
			}
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
			return(enJ2534Error);
		}
#endif

	}

    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
    fnWriteLogMsg("J2534Main.cpp", "PassThruIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    Close0305Device(bConnected);    // For pre-0404 device: Close if opened here
    SetLastErrorTextID(enJ2534Error);
    return(enJ2534Error);
}
 
//-----------------------------------------------------------------------------
//  Function Name   : SetLastErrorText()
//  Input Params    : 
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function sets the error Text. The last error occured
//                    could be retreived by the user by calling GetLastErrorText().
//-----------------------------------------------------------------------------

/*----------------------------------------------------------------------------------------------------------------------------------------------------*/

/*													IMPLEMENTATION OF J2534 - 1 V500 SPEC NEW API'S                                                    */	

/*----------------------------------------------------------------------------------------------------------------------------------------------------*/
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
/**
* 7.3.1 PassThruScanForDevices
*
* This function shall generate a static list of Pass-Thru Devices that are accessible. If the function
* call is successful, the return value shall be STATUS_NOERROR and the value pointed to by <pDeviceCount>
* shall contain the total number of devices found. Otherwise, the return value shall reflect the error
* detected and the value pointed to by <pDeviceCount> shall not be altered. The application does NOT need
* to call PassThruOpen prior to calling this function. PassThruScanForDevices can be called at any time.
* As a guideline, this function should return within 5 seconds.
*
* Additionally, the list created by this function call shall contain devices supported by the manufacturer
* of the Pass-Thru Interface DLL.  * Applications should not expect this list to include device from other
* manufactures that may also be connected to the PC. This list can be accessed, one at a time, via a call
* to PassThruGetNextDevice.
*
* If no devices are found, the return value shall be STATUS_NOERROR and the value pointed to by <pDeviceCount> shall
* be 0. If <pDeviceCount> is NULL, then the return value shall be ERR_NULL_PARAMETER. If the Pass-Thru Interface
* does not support this function call, then the return value shall be ERR_NOT_SUPPORTED. The return value
* ERR_FAILED is a mechanism to return other failures, but this is intended for use during development. A fully compliant
* SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED or ERR_NOT_SUPPORTED.
*
* Parameters:
*   <pDeviceCount>            An input set by the application, which points to an unsigned long allocated by the application.
*                             Upon return, the unsigned long shall contain the actual number of devices found.
*
* Return Values:
*   ERR_CONCURRENT_API_CALL   DLL does not support this API function. A J2534 API function has been called before the
*                             previous J2534 function call has completed.
*   ERR_NOT_SUPPORTED         A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED
*   ERR_NULL_PARAMETER        NULL pointer supplied where a valid pointer is required
*   ERR_FAILED                Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1
*                             Pass-Thru Interface shall never return ERR_FAILED.
*   STATUS_NOERROR            Function call was successful
*/

extern "C" J2534ERROR WINAPI PassThruScanForDevices(unsigned long *pDeviceCount)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruScanForDevices()", DEBUGLOG_TYPE_COMMENT, "Start");

	//Check for NULL pointer exception 
	if (pDeviceCount == NULL)
	{
		enJ2534Error = J2534_ERR_NULLPARAMETER;
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	//Get the number of devices connected to the PC
	CJ2534Registry  *pclsJ2534Registry;
	pclsJ2534Registry = new CJ2534Registry;

	if (pclsJ2534Registry->GetDeviceCount(j2534_device_list, &j2534_device_count) == J2534REGISTRY_ERR_SUCCESS)
	{
		*pDeviceCount = j2534_device_count;
	}
	else
	{
		enJ2534Error = J2534_ERR_FAILED;
		SetLastErrorTextID(J2534_ERR_FAILED);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruScanForDevices()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruScanForDevices()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
	fnWriteLogMsg("J2534Main.cpp", "PassThruScanForDevices()", DEBUGLOG_TYPE_COMMENT, "End");
	SetLastErrorTextID(enJ2534Error);
	return(enJ2534Error);
}

/**
* 7.3.2 PassThruGetNextDevice
*
* This function shall, one at a time, return the list of devices found by the most recent call to PassThruScanForDevices.
* The list of devices may be in any order and the order may be different each time PassThruScanForDevices is called. If
* the function call PassThruGetNextDevice is successful, the return value shall be STATUS_NOERROR and the structure
* pointed to by psDevice shall contain the information about the next device. Otherwise, the return value shall reflect the
* error detected and the structure pointed to by psDevice shall not be altered.
*
* The application does NOT need to call PassThruOpen prior to calling this function; it need only call
* PassThruScanForDevices. Additionally, the application is not required to call PassThruGetNextDevice until the entire
* list has been returned, it may stop at any time. However, subsequent calls to PassThruGetNextDevice will continue to
* return the remainder of the list until the entire list has been returned, the DLL has been unloaded, or there has been
* another call to PassThruScanForDevices.
*
* If the call to PassThruScanForDevices indicated that more than one device is present, then each name in the list shall
* be unique. When generating device names, manufacturers should take into consideration that a given device may be
* available to more than one PC. These names should also be persistent, as calling
* PassThruScanForDevices/PassThruGetNextDevice is NOT a prerequisite for calling PassThruOpen. Manufacturers
* should consider documenting how their device names are formulated, so that time-critical applications can know the name
* in advance without calling PassThruScanForDevices/PassThruGetNextDevice.
*
* If <psDevice> is NULL, then the return value shall be ERR_NULL_PARAMETER. If there is no more device information to
* return, then the return value shall be ERR_EXCEEDED_LIMIT. If the last call to PassThruScanForDevices did not locate
* any devices or PassThruScanForDevices was never called, then the return value shall be ERR_BUFFER_EMPTY. If the
* Pass-Thru Interface does not support this function call, then the return value shall be ERR_NOT_SUPPORTED. The
* return value ERR_FAILED is a mechanism to return other failures, but this is intended for use during development. A fully
* compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED or ERR_NOT_SUPPORTED.
*
* Parameters:
*   <psDevice>                   An input, set by the application, which points to the structure SDEVICE allocated by the application.
*                                Upon return, the structure shall have been updated the as required.
*
* Return Values:
*   ERR_CONCURRENT_API_CALL      A J2534 API function has been called before the previous J2534 function call has completed
*   ERR_NOT_SUPPORTED            DLL does not support this API function. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.
*   ERR_NULL_PARAMETER           NULL pointer supplied where a valid pointer is required
*   ERR_EXCEEDED_LIMIT           All the device information collected by the last call to PassThruScanForDevices has been returned
*   ERR_BUFFER_EMPTY             The last call to PassThruScanForDevices found no devices to be present or PassThruScanForDevices was never called
*   ERR_FAILED                   Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED.
*   STATUS_NOERROR               Function call was successful
*/

extern "C" J2534ERROR WINAPI PassThruGetNextDevice(SDEVICE *psDevice)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (psDevice == NULL) 
	{
		enJ2534Error = J2534_ERR_NULLPARAMETER;
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	if (j2534_device_count == 0) 
	{
		enJ2534Error = J2534_ERR_BUFFER_EMPTY;
		SetLastErrorTextID(enJ2534Error);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	if (psDevice->DeviceName == NULL || (strncmp("", psDevice->DeviceName, 2) == 0)) 
	{
		*psDevice = j2534_device_list[0];	
		enJ2534Error = J2534_STATUS_NOERROR;
		SetLastErrorTextID(enJ2534Error);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	unsigned long i;
	for (i = 0; i<j2534_device_count; i++) 
	{
		if (strcmp(j2534_device_list[i].DeviceName, psDevice->DeviceName) == 0)
		{
			if (i + 1 >= j2534_device_count) 
			{
				enJ2534Error = J2534_ERR_EXCEEDED_LIMIT;
				SetLastErrorTextID(enJ2534Error);
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
				return(enJ2534Error);
			}
			*psDevice = j2534_device_list[i + 1];
			enJ2534Error = J2534_STATUS_NOERROR;
			SetLastErrorTextID(enJ2534Error);
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
			return(enJ2534Error);
		}
	}
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
	fnWriteLogMsg("J2534Main.cpp", "PassThruGetNextDevice()", DEBUGLOG_TYPE_COMMENT, "End");
	SetLastErrorTextID(enJ2534Error);
	return(enJ2534Error);
}

/**
* 7.3.7 PassThruLogicalConnect
*
* This function shall establish a logical communication channel to the vehicle on the designated Pass-Thru Device. This
* logical communication channel overlays an additional protocol scheme on an existing physical communication channel. If
* the function call is successful, the return value shall be STATUS_NOERROR, the value pointed to by <pChannelID> shall
* be used as a handle to the newly established channel and that channel shall be in an initialized state. Otherwise, the
* return value shall reflect the error detected and the value pointed to by <pChannelID> shall not be altered. There can be
* up to ten logical communication channels per physical communication channel. The act of establishing a logical
* communication channel shall have no effect on the operation the underlying physical communication channel or any of its
* currently associated logical communication channels.
*
* The initialized state for a logical communication channel shall be defined to be:
*   - No periodic messages or Periodic Message IDs shall be associated with this logical communication channel
*   - The transmit and receive queues for this logical communication channel shall be clear
*   - All configurable parameters for this logical communication channel shall be at the default values
*
* A protocol specific Channel Descriptor structure, pointed to by <pChannelDescriptor>, is used to define the two end points
* of the logical connection. If the <pChannelDescriptor> is NULL, then the return value shall be ERR_NULL_PARAMETER.
*
* If <PhysicalChannelID> is not valid, the return value shall be ERR_INVALID_CHANNEL_ID. If the Pass-Thru Device is
* not currently connected or was previously disconnected without being closed, the return value shall be
* ERR_DEVICE_NOT_CONNECTED. If <pChannelID> is NULL, the return value shall be ERR_NULL_PARAMETER. If the
* designated physical communication channel and <ProtocolID> combination does not allow the creation of a logical
* communication channel, the return value shall be ERR_LOG_CHAN_NOT_ALLOWED. If the maximum number of logical
* communication channels for the specified physical communication channel is exceeded, the return value shall be
* ERR_EXCEEDED_LIMIT. If the <ProtocolID> value is invalid, unknown, or designated as �reserved�, the return value shall
* be ERR_PROTOCOL_ID_NOT_SUPPORTED. If bits in the parameter Flags that are designated as �reserved� or that are
* not applicable for the <ProtocolID> specified are set, the return value shall be ERR_FLAG_NOT_SUPPORTED. If the
* network address information in the Channel Descriptor Structure duplicates any previous addresses in the list, the return
* value shall be ERR_NOT_UNIQUE. If the <LocalAddress> and <RemoteAddress> parameters in the Channel Descriptor
* Structure are the same, the return value shall be ERR_NOT_UNIQUE. If the Pass-Thru Interface does not support this
* function call, then the return value shall be ERR_NOT_SUPPORTED. The return value ERR_FAILED is a mechanism to
* return other failures, but this is intended for use during development. A fully compliant SAE J2534-1 Pass-Thru Interface
* shall never return ERR_FAILED or ERR_NOT_SUPPORTED.
*
* Parameters:
*   <PhysicalChannelID>             is an input, set by the application, which contains the physical Channel ID
*                                   returned from PassThruConnect.
*   <ProtocolID>                    is an input, set by the application, which contains the Protocol ID for the
*                                   logical communication channel to be connected (see Figure 35 for valid options).
*                                   This ID defines how this logical communication channel will interact with the vehicle.
*                                   This ID is also used to determine the type of structure being pointed to by the parameter pChannelDescriptor.
*   <Flags>                         is an input, set by the application, which contains the flags for logical communication channel to
*                                   be connected (see Figure 36 for valid options). These bits can be ORed together and represent
*                                   the desired logical communication channel configuration.
*   <pChannelDescriptor>            is an input, set by the application, which points to a structure that defines the logical
*                                   communication channel to be connected (see Section 7.3.7.5 for more details).
*   <pChannelID>                    is an input, set by the application, which points to an unsigned long allocated by the application.
*                                   Upon return, the unsigned long shall contain the Logical Communication Channel ID, which is to
*                                   be used as a handle to the logical communication channel for future function calls.
* Return Values: *
*   ERR_CONCURRENT_API_CALL         A J2534 API function has been called before the previous J2534 function call has completed
*   ERR_DEVICE_NOT_OPEN             PassThruOpen has not successfully been called
*   ERR_INVALID_CHANNEL_ID          Invalid <ChannelID> value
*   ERR_DEVICE_NOT_CONNECTED        Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has, at some point,
*                                   failed to communicate with the Pass-Thru Device � even though it may not currently be disconnected.
*   ERR_NOT_SUPPORTED               DLL does not support this API function. A fully compliant SAE J2534-1 PassThru Interface shall never return ERR_NOT_SUPPORTED.
*   ERR_LOG_CHAN_NOT_ALLOWED        Logical communication channel is not allowed for the designated physical communication channel and <ProtocolID> combination
*   ERR_PROTOCOL_ID_NOT_SUPPORTED   <ProtocolID> value is not supported (either invalid or unknown)
*   ERR_FLAG_NOT_SUPPORTED          <Flags> value(s) are either invalid, unknown, or not appropriate for the current channel (such as setting a flag that is only
*                                   valid for ISO 9141 on a CAN channel)
*   ERR_NULL_REQUIRED               A parameter that is required to be NULL is not set to NULL
*   ERR_NULL_PARAMETER              NULL pointer supplied where a valid pointer is required
*   ERR_NOT_UNIQUE                  Attempt was made to create a logical communication channel that duplicates the <SourceAddress> and/or the <TargetAddress> of
*                                   an existing logical communication channel for the specified physical communication channel
*   ERR_EXCEEDED_LIMIT              Exceeded the maximum number of logical communication channels for the designated physical communication channel
*   ERR_FAILED                      Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED.
*   STATUS_NOERROR                  Function call was successful
*/
extern "C" J2534ERROR WINAPI PassThruLogicalConnect(unsigned long ulPhysicalChannelID,
	unsigned long ulProtocolID,
	unsigned long ulFlags,
	void *pChannelDescriptor,
	unsigned long *pulChannelID)
{
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_COMMENT, "End");

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	J2534ERROR ReturnStatus = J2534_STATUS_NOERROR;

	if (gpclsDevice == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_OPEN);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_OPEN);
		return (J2534_ERR_DEVICE_NOT_OPEN);
	}
	if (!gpclsDevice->vIsDeviceConnected())
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
		SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
		return (J2534_ERR_DEVICE_NOT_CONNECTED);
	}
	// Check for NULL parameters.
	if (pulChannelID == NULL || pChannelDescriptor == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		return (J2534_ERR_NULLPARAMETER);
	}
	// Check if the requested Channel ID is valid.
	if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalChannelID))
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
		SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}

	J2534_PROTOCOL enProtocolID = gclsProtocolManager.m_enProtocolConnectionList[ulPhysicalChannelID];
	if ((enProtocolID != CAN) && (enProtocolID != ISO15765) &&
		(enProtocolID != CAN_PS) && (enProtocolID != ISO15765_PS) &&
		(enProtocolID != TP2_0_PS) &&
		(enProtocolID != SW_ISO15765_PS) && (enProtocolID != SW_CAN_PS) &&
		(enProtocolID != SW_CAN_ISO15765_CH1) && (enProtocolID != SW_CAN_CAN_CH1) &&
		(enProtocolID != CAN_CH1) && (enProtocolID != CAN_CH2) &&
		(enProtocolID != ISO15765_CH1) && (enProtocolID != ISO15765_CH2) &&
		(enProtocolID != CAN_CH3) && (enProtocolID != CAN_CH4) &&
		(enProtocolID != ISO15765_CH3) && (enProtocolID != ISO15765_CH4) &&
		(enProtocolID != FT_CAN_PS) && (enProtocolID != FT_ISO15765_PS) &&
		(enProtocolID != FT_CAN_CH1) && (enProtocolID != FT_ISO15765_CH1))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_LOG_CHAN_NOT_ALLOWED);
			SetLastErrorTextID(J2534_ERR_LOG_CHAN_NOT_ALLOWED);
			return (J2534_ERR_LOG_CHAN_NOT_ALLOWED);
		}

	if (ulProtocolID != ISO15765_LOGICAL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_LOG_CHAN_NOT_ALLOWED);
		SetLastErrorTextID(J2534_ERR_LOG_CHAN_NOT_ALLOWED);
		return (J2534_ERR_LOG_CHAN_NOT_ALLOWED);
	}

	if (enProtocolID == CAN)
		ulProtocolID = ISO15765;
	else if (enProtocolID == CAN_CH1)
		ulProtocolID = ISO15765_CH1;
	else if (enProtocolID == CAN_CH2)
		ulProtocolID = ISO15765_CH2;
	else if (enProtocolID == CAN_CH3)
		ulProtocolID = ISO15765_CH3;
	else if (enProtocolID == CAN_CH4)
		ulProtocolID = ISO15765_CH4;

	ReturnStatus = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalChannelID]->vLogicalChannelConnect(pChannelDescriptor,
																										(J2534_PROTOCOL)ulProtocolID,
																										pulChannelID);
	if (ReturnStatus != J2534_ERR_FAILED)
	{
		SetLastErrorTextID(ReturnStatus);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		return(ReturnStatus);
	}

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	SetLastErrorTextID(ReturnStatus);
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalConnect()", DEBUGLOG_TYPE_COMMENT, "End");
	return(J2534_STATUS_NOERROR);
}

/**
* 7.3.8 PassThruLogicalDisconnect
*
* This function shall terminate a logical connection to the vehicle on the designated Pass-Thru Device. If the function call is
* successful, the return value shall be STATUS_NOERROR and the logical communication channel shall be in a
* disconnected state.
*
* The disconnected state for a logical communication channel is defined to be:
*   - The associated Channel ID shall be invalidated
*   - All periodic messages shall be cleared and Periodic Message IDs invalidated
*   - All configurable parameters shall be at the default values
*   - All messages in the transmit queue shall be discarded and no TxFailed Indication shall be generated
*   - All messages in the receive queue shall be discarded
*   - Any active message transmission shall be terminated and no TxFailed Indication shall be generated
*
* If the corresponding Pass-Thru Device is not currently open, the return value shall be ERR_DEVICE_NOT_OPEN. If the
* logical communication channel attempting to be disconnected is invalid, the return value shall be
* ERR_INVALID_CHANNEL_ID. If the Pass-Thru Device is not currently connected or was previously disconnected without
* being closed, the return value shall be ERR_DEVICE_NOT_CONNECTED. If the Pass-Thru Interface does not support
* this function call, then the return value shall be ERR_NOT_SUPPORTED. The return value ERR_FAILED is a mechanism
* to return other failures, but this is intended for use during development. A fully compliant SAE J2534-1 Pass-Thru
* Interface shall never return ERR_FAILED or ERR_NOT_SUPPORTED.
*
* Parameters:
*   <ChannelID>                     is an input, set by the application, which contains the Logical Communication Channel ID returned from PassThruLogicalConnect.
*
* Return Values:
*   ERR_CONCURRENT_API_CALL         A J2534 API function has been called before the previous J2534 function call has completed
*   ERR_DEVICE_NOT_OPEN             PassThruOpen has not successfully been called
*   ERR_INVALID_CHANNEL_ID          Invalid <ChannelID> value
*   ERR_DEVICE_NOT_CONNECTED        Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has, at some point, failed to
*                                   communicate with the Pass-Thru Device � even though it may not currently be disconnected.
*   ERR_NOT_SUPPORTED               DLL does not support this API function. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.
*   ERR_FAILED                      Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_FAILED.
*   STATUS_NOERROR                  Function call was successful
*/
extern "C" J2534ERROR WINAPI PassThruLogicalDisconnect(unsigned long ulChannelID)
{
	J2534ERROR ReturnStatus = J2534_STATUS_NOERROR;
	unsigned long ulPhysicalId = 0;

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_DATA, "ulChannelID=%u", ulChannelID);

	AFX_MANAGE_STATE(AfxGetStaticModuleState());


	// Check if Device is open.
	if (gpclsDevice == NULL)
	{
		ReturnStatus = J2534_ERR_DEVICE_NOT_OPEN;
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		SetLastErrorTextID(ReturnStatus);
		return (ReturnStatus);
	}

	//Check if device is connected
	if (!gpclsDevice->vIsDeviceConnected())
	{
		// Write to Log File.
		ReturnStatus = J2534_ERR_DEVICE_NOT_CONNECTED;
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		SetLastErrorTextID(ReturnStatus);
		return (ReturnStatus);
	}

	//Get the physical channel Id
	ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ulChannelID);
	
	// Check if the requested Channel ID is valid.
	if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
		SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}

	//Disconnect the protocol 
	ReturnStatus = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->vLogicalChannelDisConnect(ulChannelID);

	if (ReturnStatus != J2534_ERR_FAILED)
	{
		SetLastErrorTextID(ReturnStatus);
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		return(ReturnStatus);
	}
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruLogicalDisconnect()", DEBUGLOG_TYPE_COMMENT, "End");
	return ReturnStatus;
}

/**
* 7.3.9 PassThruSelect
*
This function allows the application to select specific channels to be monitored for available messages (including
Indications). The application can specify any combination of Physical or Logical Communication Channels, the minimum
number of channels that must have at least one message available, and a timeout value. When PassThruSelect is
called, the function shall not return until one of the following happens:

- The <Timeout> expires.
- The number of channels from the <ChannelList> that have at least one message available to be read meets or
exceeds the number of channels specified in <ChannelThreshold>.
- An error occurs.

* If the function call is successful, the return value shall be STATUS_NOERROR and the channel(s) from the original list

* that contain at least one message shall be identified in the SCHANNELSET structure.

*

* The purpose of PassThruSelect is to allow the application to check and/or wait for messages to become available on a

* given set of channels without having to constantly call PassThruReadMsgs. This will minimize the transactions between

* the application and the Pass Thru Interface, thus improving performance. It is important to note, that PassThruSelect will

* NOT return the messages available, this must still be done with a call to PassThruReadMsgs. However, the advantage is

* that the application now knows which channels have messages to be read.

*

* If the corresponding Pass-Thru Device is not currently open, the return value shall be ERR_DEVICE_NOT_OPEN. If any

* communication channel attempting to be monitored is invalid, the return value shall be ERR_INVALID_CHANNEL_ID and

* the SCHANNELSET structure shall not be altered. If the Pass-Thru Device is not currently connected or was previously

* disconnected without being closed, the return value shall be ERR_DEVICE_NOT_CONNECTED. If the value of

* <SelectType> is not set to READABLE_TYPE, the return value shall be ERR_SELECT_TYPE_NOT_SUPPORTED. If the

* value of <ChannelThreshold> is greater than the value of <ChannelCount>, the return value shall be

* ERR_EXCEEDED_LIMIT. If either <ChannelSetPtr> or the structure element <ChannelList> is NULL, the the return value

* shall be ERR_NULL_PARAMETER. If no messages were available to be read on any of the channels specified in the

* <ChannelList>, the return value shall be ERR_BUFFER_EMPTY. If a non-zero <Timeout> value was specified, the

* <Timeout> expired, and the number of channels (from the <ChannelList>) that have at least one message available to be

* read is less than <ChannelThreshold>, then the return value shall be ERR_TIMEOUT. If the Pass-Thru Interface does not

* support this function call, then the return value shall be ERR_NOT_SUPPORTED. The return value ERR_FAILED is a

* mechanism to return other failures, but this is intended for use during development. A fully compliant SAE J2534-1 PassThru

* Interface shall never return ERR_FAILED or ERR_NOT_SUPPORTED.

*

* Parameters:

*   <ChannelSetPtr>                is an input, set by the application, which points to a SCHANNELSET structure (also allocated by the

*                                  application).

*   <SelectType>                   is an input, set by the application, which indicates the purpose of channel selection. The only

*                                  valid value is READABLE_TYPE.

*                                  READABLE_TYPE selects the associated channels to be monitored for available messages (either

*                                  incomming messages or Indications.

*   <Timeout>                      is an input, set by the application, which contains the minimum amount of time (in milliseconds) that the

*                                  application is willing to wait to see if the desired number of messages are available to be read. A value of 0

*                                  shall cause the function to return immediately with the list of channels that have messages to be read at

*                                  the time of the function call – this is equivalent to setting the < ChannelThreshold > to 0. The valid range is

*                                  0-30000 milliseconds. The precision of the timeout is at least 10 milliseconds and the tolerance is 0 to +50

*                                  milliseconds.

*

* The SCHANNELSET structure is defined in Section 9.20, where:

*

*   <ChannelCount>                 is an input, set by the application, which contains the number of Logical and/or Physical

*                                  Communication Channel IDs contained in the array <ChannelList>. Upon return, this element will contain

*                                  the number of channels that are left in the array <ChannelList>.

*   <ChannelThreshold>             is an input, set by the application, which contains the minimum number of channels

*                                  that have at least one message available to be read. Setting this to a value of 0 will cause the function to

*                                  return immediately with the list of channels that have at least one message available to be read at the time

*                                  of the function call – this is equivalent to setting the <Timeout> to 0. The value of this parameter shall be

*                                  less than or equal to the value of <ChannelCount>.

*   <ChannelList>                  is an input, set by the application, which is a pointer to an array (also allocated by the

*                                  application) that contains the list of Logical and/or Physical Communication Channel IDs (obtained from

*                                  previous calls to PassThruConnect and/or PassThruLogicalConnect) to be monitored. Upon return, this

*                                  array will contain a subset of the original list of Channel IDs (in no particular order) that have at least one

*                                  or more messages available to be read.

*

* Return Values:

*   ERR_CONCURRENT_API_CALL        A J2534 API function has been called before the previous J2534 function call has completed

*   ERR_DEVICE_NOT_OPEN            PassThruOpen has not successfully been called

*   ERR_INVALID_CHANNEL_ID         Invalid <ChannelID> value

*   ERR_DEVICE_NOT_CONNECTED       Pass-Thru Device communication error. This indicates that the Pass-Thru Interface DLL has, at some point,

*                                  failed to communicate with the Pass-Thru Device – even though it may not currently be disconnected.

*   ERR_NOT_SUPPORTED              DLL does not support this API function. A fully compliant SAE J2534-1 Pass-Thru Interface shall never return ERR_NOT_SUPPORTED.

*   ERR_NULL_PARAMETER             NULL pointer supplied where a valid pointer is required

*   ERR_SELECT_TYPE_NOT_SUPPORTED  <SelectType> is either invalid or unknown

*   ERR_EXCEEDED_LIMIT             Exceeded the allowed limits, the value of <ChannelThreshold> is greater than the value of <ChannelCount>

*   ERR_BUFFER_EMPTY               The receive queues for the requested channels are empty, no messages available to read

*   ERR_TIMEOUT                    Requested action could not be completed in the designated time NOTE: This only applies when Timeout is non-zero and at least

*                                  one message on a requested channel is available to be read.

*   ERR_FAILED                     Undefined error, use PassThruGetLastError for text description. A fully compliant SAE J2534-1 Pass-Thru Interface shall

*                                  never return ERR_FAILED.

*   STATUS_NOERROR                 Function call was successful

*/
extern "C" J2534ERROR WINAPI PassThruSelect(SCHANNELSET *ChannelSetPtr, 
											unsigned long SelectType, 
											unsigned long Timeout)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	J2534ERROR ReturnStatus = J2534_STATUS_NOERROR;
	unsigned long pulnumMsg;
	// Write to Log File.
	fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check if Device is open.
	if (gpclsDevice == NULL)
	{
		ReturnStatus = J2534_ERR_DEVICE_NOT_OPEN;
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		SetLastErrorTextID(ReturnStatus);
		return (ReturnStatus);
	}
	//Check if device is connected
	if (!gpclsDevice->vIsDeviceConnected())
	{
		// Write to Log File.
		ReturnStatus = J2534_ERR_DEVICE_NOT_CONNECTED;
		fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", ReturnStatus);
		SetLastErrorTextID(ReturnStatus);
		return (ReturnStatus);
	}
	if (SelectType != READABLE_TYPE)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_SELECT_TYPE_NOT_SUPPORTED);
		SetLastErrorTextID(J2534_ERR_SELECT_TYPE_NOT_SUPPORTED);
		return (J2534_ERR_SELECT_TYPE_NOT_SUPPORTED);
	}
	if (ChannelSetPtr == NULL)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
		SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
		return (J2534_ERR_NULLPARAMETER);
	}

	if (ChannelSetPtr->ChannelThreshold > ChannelSetPtr->ChannelCount)
	{
		// Write to Log File.
		fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		SetLastErrorTextID(J2534_ERR_EXCEEDED_LIMIT);
		return (J2534_ERR_EXCEEDED_LIMIT);
	}

	for (unsigned long i = 0; i < ChannelSetPtr->ChannelCount; i++)
	{
		if (ChannelSetPtr->ChannelList[i] == NULL)
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
			SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
			return (J2534_ERR_NULLPARAMETER);
		}
		unsigned long ulPhysicalId = 0;
		ulPhysicalId = GetPhysicalChannelIdFromLogicalId(ChannelSetPtr->ChannelList[i]);
		// Check if the requested Channel ID is valid.
		if (!gclsProtocolManager.IsChannelIDValid(ulPhysicalId))
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
			SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
			return(J2534_ERR_INVALID_CHANNEL_ID);
		}
		if (ChannelSetPtr->ChannelList[i] > MAX_PHY_CHANNEL_ID)
		{
			if (!gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->IsLogChannelValid(ChannelSetPtr->ChannelList[i]))
			{
				// Write to Log File.
				fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_CHANNEL_ID);
				SetLastErrorTextID(J2534_ERR_INVALID_CHANNEL_ID);
				return(J2534_ERR_INVALID_CHANNEL_ID);
			}
		}

		if (ChannelSetPtr->ChannelList[i] != 0 && ChannelSetPtr->ChannelList[i] < MAX_PHY_CHANNEL_ID)
		{
			ChannelSetPtr->ChannelThreshold = gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_ulPhyChnRxCount;
		}
		else
		{
			gclsProtocolManager.m_pclsProtocolObject[ulPhysicalId]->m_pclsLogicalChannel->GetRxCountOnLogicalChannel(ChannelSetPtr->ChannelList[i], &pulnumMsg);
			ChannelSetPtr->ChannelThreshold = pulnumMsg;
		}
		if (ChannelSetPtr->ChannelThreshold == 0)
		{
			// Write to Log File.
			fnWriteLogMsg("J2534Main.cpp", "PassThruStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_BUFFER_EMPTY);
			SetLastErrorTextID(J2534_ERR_BUFFER_EMPTY);
			return(J2534_ERR_BUFFER_EMPTY);
		}
	}
	fnWriteLogMsg("J2534Main.cpp", "PassThruSelect()", DEBUGLOG_TYPE_COMMENT, "End");
	return ReturnStatus;
}
#endif

void SetLastErrorText(char *pszErrorText, ...)
{
    // Update the global with the last Error text so that the right 
    // error text is returned when calling PassThruGetLastError() for 
    // J2534_ERR_FAILED.
    EnterCriticalSection(&CJ2534SetLastErrorTextSection); // Make processing parameters thread-safe
    char buf[DEVICEBASE_MAX_BUFFER_SIZE];
    va_list ap;
    va_start(ap, pszErrorText);
    vsprintf_s(buf, sizeof(buf)-1, pszErrorText, ap);
    va_end(ap);
    buf[sizeof(gszLastErrorText) + 1] = '\0'; // We have our limits
    int len = min((sizeof(gszLastErrorText) - 1), strlen(buf));
    strcpy_s(gszLastErrorText, buf); 
    gszLastErrorText[len] = '\0'; // Ensure it is ASCIIZ terminated
    LeaveCriticalSection(&CJ2534SetLastErrorTextSection);
}

void SetLastErrorTextID(J2534ERROR  enJ2534Error)
{

    // Update the global with the last Error text so that the right 
    // error text is returned when calling GetLastErrorText().
    EnterCriticalSection(&CJ2534SetLastErrorTextSection); // Make processing parameters thread-safe
    switch (enJ2534Error)
    {
        case 0:
            strcpy_s(gszLastErrorText, "Function call successful"); 
            break;
        case 1:
            strcpy_s(gszLastErrorText, "Device cannot support requested functionality mandated in this document."); 
            break;
        case 2:
            strcpy_s(gszLastErrorText, "Invalid ChannelID value"); 
            break;
        case 3:
            strcpy_s(gszLastErrorText, "Invalid ProtocolID value..unsupported ProtocolID or a resource conflict."); 
            break;
        case 4:
            strcpy_s(gszLastErrorText, "NULL pointer supplied where a valid pointer is required"); 
            break;
        case 5:
            strcpy_s(gszLastErrorText, "Invalid value for Ioctl parameter"); 
            break;
        case 6:
            strcpy_s(gszLastErrorText, "Invalid flag values"); 
            break;
        case 7:
            strcpy_s(gszLastErrorText, "Undefined error"); 
            break;
        case 8:
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            strcpy_s(gszLastErrorText, "Unable to communicate with device"); 
#endif
#ifdef J2534_0305
            strcpy_s(gszLastErrorText, "Device not connected to PC"); 
#endif
            break;
        case 9:
            strcpy_s(gszLastErrorText, "Timeout."); 
            break;
        case 10:
            strcpy_s(gszLastErrorText, "Invalid message structure pointed to by pMsg"); 
            break;
        case 11:
            strcpy_s(gszLastErrorText, "Invalid TimeInterval value."); 
            break;
        case 12:
            strcpy_s(gszLastErrorText, "Exceeded maximum number of message IDs or allocated space."); 
            break;
        case 13:
            strcpy_s(gszLastErrorText, "Invalid MsgID value."); 
            break;
        case 14:
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            strcpy_s(gszLastErrorText, "Device is currently open."); 
#endif
#ifdef J2534_0305
            strcpy_s(gszLastErrorText, "Invalid ErrorID value."); 
#endif
            break;
        case 15:
            strcpy_s(gszLastErrorText, "Invalid IoctlID value."); 
            break;
        case 16:
            strcpy_s(gszLastErrorText, "Protocol message buffer empty..no messages available to read."); 
            break;
        case 17:
            strcpy_s(gszLastErrorText, "Protocol message buffer full. "); 
            break;
        case 18:
            strcpy_s(gszLastErrorText, "Indicates a buffer overflow occurred and messages were lost."); 
            break;
        case 19:
            strcpy_s(gszLastErrorText, "Invalid pin number or voltage already applied to a pin."); 
            break;
        case 20:
            strcpy_s(gszLastErrorText, "Channel number is currently connected or pin is used by another protocol."); 
            break;
        case 21:
            strcpy_s(gszLastErrorText, "Protocol type in the message does not match the protocol of the Channel ID"); 
            break;
        case 22:
            strcpy_s(gszLastErrorText, "Invalid Filter ID value"); 
            break;
        case 23:
            strcpy_s(gszLastErrorText, "No flow control filter set or matched (for protocolID ISO15765 only)."); 
            break;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        case 24:
            strcpy_s(gszLastErrorText, "A CAN ID in pPatternMsg/pFlowControlMsg matches an ID in an existing filter"); 
            break;
        case 25:
            strcpy_s(gszLastErrorText, "The desired baud rate cannot be achieved within the tolerance"); 
            break;
        case 26:
            strcpy_s(gszLastErrorText, "Device ID Invalid"); 
            break;
        case 0x10000:
            strcpy_s(gszLastErrorText, "Address not claimed"); 
            break;
#endif
        default:
            break;
    }
    LeaveCriticalSection(&CJ2534SetLastErrorTextSection);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetLastErrorText()
//  Input Params    : szBuffer       - Buffer in which the Text is returned.
//                    ulSizeOfBuffer - Size of Buffer provided by the user.
//  Output Params   : 
//  Return          : Returns an Error Status.
//  Description     : This function returns the error Text that was set by 
//                    SetLastErrorText().
//-----------------------------------------------------------------------------
void GetLastErrorText(char *pszBuffer, unsigned long ulSizeOfBuffer)
{
    // Copy the Last Error Text that 
    strncpy_s(pszBuffer, J2534MAIN_ERROR_TEXT_SIZE, gszLastErrorText, 
            min(strlen(gszLastErrorText), (ulSizeOfBuffer - 1)));
}

//-----------------------------------------------------------------------------
//  Function Name   : DGPassThruCheckLock()
//  Input Params    : szBuffer       -
//                    ulSizeOfBuffer - Size of Buffer provided by the user.
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI DGPassThruCheckLock (char *pszLock,
                                                unsigned char *pucResult)
{
    J2534ERROR          enJ2534Error;

    // Check for invalid Device ID
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 - 1 0500 Spec Support 
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruCheckLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif

#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruCheckLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    
    // Check for NULL pointers
    if ((pszLock == NULL) || (pucResult == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruCheckLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    enJ2534Error = gpclsDevice->vCheckLock(pszLock,pucResult); 
    
    if (enJ2534Error == J2534_STATUS_NOERROR)                 
    {
        SetLastErrorTextID(enJ2534Error);
        return(J2534_STATUS_NOERROR);
    }
    else
    {
        SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
        return J2534_ERR_NOT_SUPPORTED;
    }

    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : DGPassThruAddLock()
//  Input Params    : szBuffer       -
//                    ulSizeOfBuffer - Size of Buffer provided by the user.
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI DGPassThruAddLock (char *pszLock,
                                                unsigned char *pucResult)
{
    J2534ERROR          enJ2534Error;

    // Check for invalid Device ID
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 - 1 0500 Spec Support 
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruAddLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif

#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruAddLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    
    // Check for NULL pointers
    if ((pszLock == NULL) || (pucResult == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruAddLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    enJ2534Error = gpclsDevice->vAddLock(pszLock,pucResult); 
    if (enJ2534Error == J2534_STATUS_NOERROR)                 
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruAddLock()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
        SetLastErrorTextID(enJ2534Error);
        return(J2534_STATUS_NOERROR);
    }
    else
    {
        SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
        return J2534_ERR_NOT_SUPPORTED;
    }
    
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : DGPassThruDeleteLock()
//  Input Params    : szBuffer       -
//                    ulSizeOfBuffer - Size of Buffer provided by the user.
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI DGPassThruDeleteLock (char *pszLock,
                                                unsigned char *pucResult)
{
    J2534ERROR          enJ2534Error;

    // Check for invalid Device ID
#if defined (J2534_0404) || defined (J2534_0500) // For J2534 - 1 0500 Spec Support 
    if (gpclsDevice == NULL)
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruDeleteLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_DEVICE_ID);
        SetLastErrorTextID(J2534_ERR_INVALID_DEVICE_ID);
        return (J2534_ERR_INVALID_DEVICE_ID);
    }
#endif

#ifdef J2534_0305
    // Check for invalid Device ID
    if (gpclsDevice != NULL)
#endif
    if (!gpclsDevice->vIsDeviceConnected())
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruDeleteLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_DEVICE_NOT_CONNECTED);
        SetLastErrorTextID(J2534_ERR_DEVICE_NOT_CONNECTED);
        return (J2534_ERR_DEVICE_NOT_CONNECTED);
    }
    
    // Check for NULL pointers
    if ((pszLock == NULL) || (pucResult == NULL))
    {
        // Write to Log File.
        fnWriteLogMsg("J2534Main.cpp", "DGPassThruDeleteLock()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetLastErrorTextID(J2534_ERR_NULLPARAMETER);
        return (J2534_ERR_NULLPARAMETER);
    }
    enJ2534Error = gpclsDevice->vDeleteLock(pszLock,pucResult); 
    
    if (enJ2534Error == J2534_STATUS_NOERROR)
    {
        SetLastErrorTextID(enJ2534Error);
        return(J2534_STATUS_NOERROR);
    }
    else
    {
        SetLastErrorTextID(J2534_ERR_NOT_SUPPORTED);
        return J2534_ERR_NOT_SUPPORTED;
    }
    
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetVersionStr()
//  Input Params    : pszVersionStr  - Pointer to buffer string
//                    lenVersionStr - Size of Buffer provided by the user.
//  Output Params   : 
//  Return          : 
//  Description     : Get version string; can be called w/o opening device
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI GetVersionStr(char *pszVersionStr, int lenVersionStr)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
 
    // Check for NULL pointer
    if (pszVersionStr == NULL)
    {
        return J2534_ERR_NULLPARAMETER;
    }
    if (lenVersionStr < (int) strlen(STRFILEVER) + 1)
    {
        return J2534_ERR_EXCEEDED_LIMIT;
    }
    strncpy_s(pszVersionStr, lenVersionStr, STRFILEVER, strlen(STRFILEVER) + 1);
    return(J2534_STATUS_NOERROR);
}

void CJ2534GenericAppPassThruClose()    // Let's clean up in case user didn't - par100319
{
    if (gpclsDevice != NULL) {          // Avoid logging an error
        PassThruClose(gulDeviceID);
    }
}

#ifdef J2534_0500
unsigned long GetPhysicalChannelIdFromLogicalId(unsigned long ulchannelId)
{
	if ((ulchannelId >= 11 && ulchannelId <= 20) || ulchannelId == 1)
		return 1;
	else if ((ulchannelId >= 21 && ulchannelId <= 30) || ulchannelId == 2)
		return 2;
	else if ((ulchannelId >= 31 && ulchannelId <= 40) || ulchannelId == 3)
		return 3;
	else if ((ulchannelId >= 41 && ulchannelId <= 50) || ulchannelId == 4)
		return 4;
	else
		return -1;
	return 0;
}
#endif

#ifdef J2534_0305
// For pre-0404 device: Close if opened here
void Close0305Device(bool bConnected)
{   // bConnected = TRUE indicates device opened by before
    // thus, it will be left OPEN
    if (bConnected == false)    
    {
        // Close the Device Connection.
        gpclsDevice->vCloseDevice();

        // Delete Device Object.
        delete gpclsDevice;
        gpclsDevice = NULL;
        gulDeviceID--;
        if (gulDeviceID < 0)
            gulDeviceID = 0;
    }
}
#endif

DWORD MilliSecondsNow()
{
#ifdef USE_QPF
    DWORD ms_now = 0;

    __int64 liStart, liFreq;

    if(QueryPerformanceFrequency((LARGE_INTEGER* ) &liFreq))
    {
        liFreq = liFreq / 1000; //convert time to ms

        if(QueryPerformanceCounter((LARGE_INTEGER* ) &liStart))
        {
            ms_now = (DWORD) (liStart/liFreq);
        }
    }
    if (!ms_now)
    {
        ms_now = GetTickCount();
    }
    return ms_now;
#else
    return(GetTickCount());
#endif

}

#if defined (J2534_0404) || defined (J2534_0500) // For J2534 - 1 0500 Spec Support 
// Provide functions for BusMaster support of DG devices - par130526

// TODO change J2534ERROR to DGBUSMASTERERROR
// TODO establish DGBUSMASTERERROR error type and text

unsigned long ulDeviceID = 0;

//-----------------------------------------------------------------------------
//  Function Name   : OpenDevice()
//  Input Params    : pszDeviceName  - Pointer to DG Device Name
//  Output Params   : none
//  Return          : 
//  Description     : OpenDG device for BusMaster
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI OpenDevice(char *pszDeviceName)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "OpenDevice()", DEBUGLOG_TYPE_COMMENT, "Start");
 
    if (memcmp(pszDeviceName, J2534REGISTRY_TOOL_NAME, strlen(J2534REGISTRY_TOOL_NAME)) != 0)
    {
        fnWriteLogMsg("J2534Main.cpp", "OpenDevice()", DEBUGLOG_TYPE_COMMENT, "%s not expected device", pszDeviceName);
        char TmpBuff[J2534MAIN_ERROR_TEXT_SIZE];
        sprintf_s(TmpBuff, "PassThruOpen() : %s not expected device", pszDeviceName);
        SetLastErrorText(TmpBuff);
        return (J2534_ERR_FAILED);
    }
	return(PassThruOpen(NULL, &ulDeviceID));
}

//-----------------------------------------------------------------------------
//  Function Name   : CloseDevice()
//  Input Params    : pszDeviceName  - Pointer to DG Device Name
//  Output Params   : none
//  Return          : 
//  Description     : CloseDG device for BusMaster
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI CloseDevice(char *pszDeviceName)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "CloseDevice()", DEBUGLOG_TYPE_COMMENT, "Start");
    if (!ulDeviceID)
    {
        fnWriteLogMsg("J2534Main.cpp", "CloseDevice()", DEBUGLOG_TYPE_ERROR, "Device not opened");
    }
    return(PassThruClose(ulDeviceID));
}


//-----------------------------------------------------------------------------
//  Function Name   : ConnectChannel()
//  Input Params    : pszDeviceName  - Pointer to DG Device Name
//  Output Params   : none
//  Return          : 
//  Description     : Connect CAN Channel of Open DG device for BusMaster
//-----------------------------------------------------------------------------
extern "C" J2534ERROR WINAPI ConnectChannel(int iChannel, unsigned long ulDataRate)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    J2534ERROR ReturnStatus = J2534_STATUS_NOERROR;
    // Write to Log File.
    fnWriteLogMsg("J2534Main.cpp", "ConnectChannel()", DEBUGLOG_TYPE_COMMENT, "Start");
    if (!ulDeviceID)
    {
        fnWriteLogMsg("J2534Main.cpp", "ConnectChannel()", DEBUGLOG_TYPE_ERROR, "Device not opened");
    }
    // Convert iChannel parm into protocol ID
    J2534_PROTOCOL iProtocolID;
    switch(iChannel)
    {
        case 1:
            iProtocolID = CAN_CH1;
            break;

        case 2:
            iProtocolID = CAN_CH2;
            break;

        case 3:
            iProtocolID = CAN_CH3;
            break;

        case 4:
            iProtocolID = CAN_CH4;
            break;

        default:
            ReturnStatus = J2534_ERR_INVALID_CHANNEL_ID;
            break;
    }
    if (ReturnStatus == J2534_STATUS_NOERROR)
    {
#ifdef J2534_0404
		unsigned long ulChannelID;
		if ((ReturnStatus = PassThruConnect(ulDeviceID, iProtocolID, 0, ulDataRate, &ulChannelID)) == J2534_STATUS_NOERROR)
#else 
#ifdef J2534_0500
#endif
#endif
        {
        // set J2534_FILTER_PASS
        }
    }
    return ReturnStatus;
}

#endif
