#pragma once
/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This is the main header file that contains the
*              datatypes and declarations for the J2534 APIs.
* Note:
*
*******************************************************************************/

#ifndef _J2534_H_
#define _J2534_H_

#include <vector>


#define PASSTHRU_MSG_DATA_SIZE          4128
#define MAX_PHY_CHANNEL_ID				10
#define MAX_LOG_PER_PHYSICAL_ID			10

#pragma pack(1)
typedef struct
{
	unsigned long ulProtocolID;
	unsigned long MsgHandle;
	unsigned long ulRxStatus;
	unsigned long ulTxFlags;
	unsigned long ulTimeStamp;
	unsigned long ulDataSize;
	unsigned long ulExtraDataIndex;
	unsigned char *ucData;
	unsigned long ulDataBufferSize;

} PASSTHRU_MSG;
#pragma pack()

// J2534-2 Repeat Messaging feature
typedef struct
{
	unsigned long TimeInterval; /* Repeat transmission rate */
	unsigned long Condition;
	PASSTHRU_MSG RepeatMsgData[3];
} REPEAT_MSG_SETUP;

#define J2534_TX_MSG_SIZE               sizeof(PASSTHRU_MSG)
#define J2534_RX_MSG_SIZE               sizeof(PASSTHRU_MSG)

// Connect Flag Bits
#define J2534_CONNECT_FLAGBIT_CAN29BIT  0x00000100 // Bit 8
#define J2534_CONNECT_FLAGBIT_NOCHKSUM  0x00000200 // Bit 9
#define J2534_CONNECT_FLAGBIT_CANBOTH   0x00000800 // Bit 11
#define J2534_CONNECT_FLAGBIT_KONLY     0x00001000 // Bit 12

// Tx Flag Bits
#define J2534_TX_FLAGBIT_CAN29BIT       0x00000100 // Bit 8
#define J2534_TX_TP2_0_BROADCAST_MSG    0x00010000 // Bit 16
#define J2534_TX_FLAGBIT_SLAVE  		0x01000000 // Bit 24
#define J2534_TX_FLAGBIT_SLAVE_ENABLE_SST  0x02000000 // Bit 25
#define J2534_TX_FLAGBIT_SLAVE_DISABLE_SST  0x06000000 // Bit 25 & 26
#define J2534_TX_FLAGBIT_SLAVE_ONE_SHOT 0x10000000 // Bit 28
#define J2534_TX_FLAGBIT_MASTER_TXIMMED 0x20000000 // Bit 29

// Rx Flag Bits
#define J2534_RX_FLAGBIT_MSGTYPE        0x00000001 // Bit 0
#define J2534_RX_FLAGBIT_CAN29BIT       0x00000100 // Bit 8
#define J2534_RX_LINK_FAULT             0x00020000
#define J2534_RX_FLAGBIT_ISO15765_TIMEOUT 0x80000000


//Reserved for SAE          1
#define FULL_DUPLEX         (0 << 0)

// Values for <SelectType>
#define READABLE_TYPE 0x00000001

// Values for <Connector>
#define J1962_CONNECTOR 0x00000001

//Reserved for physical communication channels 0x00000008 - 0x00000001FF
//#define ISO15765_LOGICAL   0x00000200
//Reserved for logical communication channels 0x00000201 - 0x000003FF
//Reserved for SAE J2534-1 0x00000400 - 0x00007FFF
//Reserved for SAE - 0x00008000 - 0xFFFFFFFF

// Values for <DeviceAvailable>
#define DEVICE_STATE_UNKNOWN  0x00
#define DEVICE_AVAILABLE      0x01
#define DEVICE_IN_USE         0x02

// Values for <DeviceDLLFWStatus>
#define DEVICE_DLL_FW_COMPATIBILTY_UNKNOWN  0x00000000
#define DEVICE_DLL_FW_COMPATIBLE            0x00000001
#define DEVICE_DLL_OR_FW_NOT_COMPATIBLE     0x00000002
#define DEVICE_DLL_NOT_COMPATIBLE           0x00000003
#define DEVICE_FW_NOT_COMPATIBLE            0x00000004

// Values for <DeviceConnectedMedia>
#define DEVICE_CONN_UNKNOWN  0x00000000
#define DEVICE_CONN_WIRELESS 0x00000001
#define DEVICE_CONN_WIRED    0x00000002

// Pre-defined values for GET_CONFIG / SET_CONFIG Parameters
#define BUS_NORMAL    0x00000000
#define BUS_PLUS      0x00000001
#define BUS_MINUS     0x00000002
#define DATA_BITS_8   0x00000000
#define DATA_BITS_7   0x00000001
#define ISO_STD_INIT  0x00000000
#define ISO_INV_KB2   0x00000001
#define ISO_INV_ADD   0x00000002

// Protocols supported
typedef enum
{
	J1850VPW = 0x01,                    // J1850VPW Protocol
	J1850PWM,                           // J1850PWM Protocol
	ISO9141,                            // ISO9141 Protocol
	ISO14230,                           // ISO14230 Protocol
	CAN,                                // CAN Protocol
	ISO15765,
	SCI_A_ENGINE,
	SCI_A_TRANS,
	SCI_B_ENGINE,
	SCI_B_TRANS,
	ISO9141_PS = 0x8002,
	ISO14230_PS = 0x8003,
	CAN_PS,
	ISO15765_PS,
	SW_ISO15765_PS = 0x8007,
	SW_CAN_PS,
	GM_UART,
	UART_ECHO_BYTE_PS = 0x800A,
	HONDA_DIAGH_PS = 0x800B,
	J1939_PS = 0x800c,
	J1708_PS,
	TP2_0_PS,
	FT_CAN_PS = 0x800f,
	FT_ISO15765_PS,
	CAN_CH1 = 0x9000,
	CAN_CH2,
	CAN_CH3,
	CAN_CH4,
	ISO15765_CH1 = 0x9280,
	ISO15765_CH2,
	ISO15765_CH3,
	ISO15765_CH4,
	SW_CAN_CAN_CH1 = 0x9300,
	SW_CAN_ISO15765_CH1 = 0x9380,
	FT_CAN_CH1 = 0x9480,
	FT_ISO15765_CH1 = 0x9500,
	J1939_CH1 = 0x9700,                 // Use 'Reserved' Protocol ID for now
	J1939_CH2,                          // Use 'Reserved' Protocol ID for now
	J1939_CH3,                          // Use 'Reserved' Protocol ID for now
	J1939_CH4,                          // Use 'Reserved' Protocol ID for now 
	J1708_CH1 = 0x9780,
	TP2_0_CH1 = 0x9800,
	TP2_0_CH2,
	TP2_0_CH3,
	TP2_0_CH4,
	CCD = 0x10000,
	LIN = 0x10101,
	ISO15765_LOGICAL = 0x00000200
}
J2534_PROTOCOL;

struct ProtocolEntry
{
	J2534_PROTOCOL J2534ProtocolID;
	char *Description;
};

const struct ProtocolEntry Protocols[] = {
	{ J1850VPW,      "J1850VPW" },
	{ J1850PWM,      "J1850PWM" },
	{ ISO9141,       "ISO9141" },
	{ ISO14230,      "ISO14230" },
	{ CAN,           "CAN" },
	{ ISO15765,      "ISO15765" },
	{ SCI_A_ENGINE,  "SCI_A_ENGINE" },
	{ SCI_A_TRANS,   "SCI_A_TRANS" },
	{ SCI_B_ENGINE,  "SCI_B_ENGINE" },
	{ SCI_B_TRANS,   "SCI_B_TRANS" },
	{ SW_ISO15765_PS,"SW_ISO15765_PS" },
	{ SW_CAN_PS,     "SW_CAN_PS" },
	{ GM_UART,       "GM_UART_PS" },
	{ ISO9141_PS,    "ISO9141_PS" },
	{ UART_ECHO_BYTE_PS,"UART_ECHO_BYTE_PS" },
	{ HONDA_DIAGH_PS,"HONDA_DIAGH_PS" },
	{ J1939_PS,      "J1939_PS" },
	{ J1939_CH1,     "J1939_CH1" },
	{ J1939_CH2,     "J1939_CH2" },
	{ J1939_CH3,     "J1939_CH3" },
	{ J1939_CH4,     "J1939_CH4" },
	{ J1708_PS,      "J1708_PS" },
	{ J1708_CH1,     "J1708_CH1" },
	{ CCD,           "CCD" },
	{ CAN_PS,        "CAN_PS" },
	{ ISO15765_PS,   "ISO15765_PS" },
	{ FT_ISO15765_PS, "FT_ISO15765_PS" },
	{ FT_CAN_PS,      "FT_CAN_PS" },
	{ FT_ISO15765_CH1, "FT_ISO15765_CH1" },
	{ FT_CAN_CH1,     "FT_CAN_CH1" },
	{ SW_CAN_ISO15765_CH1, "SW_CAN_ISO15765_CH1" },
	{ SW_CAN_CAN_CH1,    "SW_CAN_CAN_CH1" },
	{ CAN_CH1,        "CAN_CH1" },
	{ CAN_CH2,        "CAN_CH2" },
	{ CAN_CH3,        "CAN_CH3" },
	{ CAN_CH4,        "CAN_CH4" },
	{ ISO15765_CH1,   "ISO15765_CH1" },
	{ ISO15765_CH2,   "ISO15765_CH2" },
	{ ISO15765_CH3,   "ISO15765_CH3" },
	{ ISO15765_CH4,   "ISO15765_CH4" },
	{ TP2_0_PS,      "TP2_0_PS" },
	{ TP2_0_CH1,     "TP2_0_CH1" },
	{ TP2_0_CH2,     "TP2_0_CH2" },
	{ TP2_0_CH3,     "TP2_0_CH3" },
	{ TP2_0_CH4,     "TP2_0_CH4" },
	{ LIN,            "LIN" },
	{ISO15765_LOGICAL, "ISO15765_LOGICAL"},
};

#define PROTOCOL_ENTRY_COUNT sizeof(Protocols)/sizeof(ProtocolEntry)

// J2534 Error codes
typedef enum
{
	J2534_STATUS_NOERROR = 0,             // Function call successful.
	J2534_ERR_NOT_SUPPORTED,            // Function not supported.
	J2534_ERR_INVALID_CHANNEL_ID,       // Invalid ChannelID value.
	J2534_ERR_INVALID_PROTOCOL_ID,      // Invalid ProtocolID value.
	J2534_ERR_NULLPARAMETER,            // NULL pointer supplied where a valid 
	// pointer
	// is required.
	J2534_ERR_INVALID_IOCTL_VALUE,      // Invalid value for Ioctl parameter 
	J2534_ERR_INVALID_FLAGS,            // Invalid flag values.
	J2534_ERR_FAILED,                   // Undefined error, use 
	// PassThruGetLastError for description
	// of error.
	J2534_ERR_DEVICE_NOT_CONNECTED,     // Unable to communicate with device
	J2534_ERR_TIMEOUT,                  // Timeout. No message available to 
	// read or 
	// could not read the specified no. of 
	// msgs.
	J2534_ERR_INVALID_MSG,              // Invalid message structure pointed 
	// to by pMsg.
	J2534_ERR_INVALID_TIME_INTERVAL,    // Invalid TimeInterval value.
	J2534_ERR_EXCEEDED_LIMIT,           // ALL periodic message IDs have been 
	// used.
	J2534_ERR_INVALID_MSG_ID,           // Invalid MsgID value.
	J2534_ERR_DEVICE_IN_USE,            // Device already open and/or in use
	J2534_ERR_INVALID_IOCTL_ID,         // Invalid IoctlID value.
	J2534_ERR_BUFFER_EMPTY,             // Protocol message buffer empty.
	J2534_ERR_BUFFER_FULL,              // Protocol message buffer full.
	J2534_ERR_BUFFER_OVERFLOW,          // Protocol message buffer overflow.
	J2534_ERR_PIN_INVALID,              // Invalid pin number.
	J2534_ERR_CHANNEL_IN_USE,           // Channel already in use.
	J2534_ERR_MSG_PROTOCOL_ID,          // Protocol type does not match the
	// protocol associated with the 
	// Channel ID
	J2534_ERR_INVALID_FILTER_ID,        // Invalid MsgID value
	J2534_ERR_NO_FLOW_CONTROL,          // An attempt was made to send a message
	// on an ISO15765 ChannelID before a flow
	// control filter was established.  
	J2534_ERR_NOT_UNIQUE,               // A CAN ID in PatternMsg or FlowControlMsg
	// matches either ID in an existing Flow Control Filter
	J2534_ERR_INVALID_BAUDRATE,         // Desired baud rate cannot be achieved within tolerances           
	J2534_ERR_INVALID_DEVICE_ID,        // Device ID Invalid  
	J2534_ERR_PIN_NOT_SUPPORTED,
	J2534_ERR_FILTER_TYPE_NOT_SUPPORTED,
	J2534_ERR_DEVICE_NOT_OPEN,
	J2534_ERR_LOG_CHAN_NOT_ALLOWED,
	J2534_ERR_MSG_NOT_ALLOWED,
	J2534_ERR_SELECT_TYPE_NOT_SUPPORTED,
	J2534_ERR_ADDRESS_NOT_CLAIMED = 0x10000,
	J2534_ERR_NO_CONNECTION_ESTABLISHED
}
J2534ERROR;

// Filter Types
// Filter Message Error codes
typedef enum
{
	J2534_FILTER_NONE,
	J2534_FILTER_PASS,
	J2534_FILTER_BLOCK,
	J2534_FILTER_FLOW_CONTROL
}
J2534_FILTER;

// Values for <FilterType>
#define PASS_FILTER  0x0000001
#define BLOCK_FILTER 0x0000002

// CAN Mixed Mode Types
typedef enum
{
	J2534_CAN_MIXED_FORMAT_OFF,
	J2534_CAN_MIXED_FORMAT_ON,
	J2534_CAN_MIXED_FORMAT_ALL_FRAMES
}
J2534_CAN_MIXED_FORMAT;

// Ioctl ID Values
typedef enum
{
	GET_CONFIG = 0x01,
	SET_CONFIG,
	READ_PIN_VOLTAGE,
	FIVE_BAUD_INIT,
	FAST_INIT,
	SET_PIN_USE,
	CLEAR_TX_BUFFER,
	CLEAR_RX_BUFFER,
	CLEAR_PERIODIC_MSGS,
	CLEAR_MSG_FILTERS,
	CLEAR_FUNCT_MSG_LOOKUP_TABLE,
	ADD_TO_FUNCT_MSG_LOOKUP_TABLE,
	DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE,
	READ_PROG_VOLTAGE,
	BUS_ON,
	SW_CAN_HS = 0x8000,
	SW_CAN_NS,
	SET_POLL_RESPONSE,
	BECOME_MASTER,
	START_REPEAT_MESSAGE = 0x00008004,
	QUERY_REPEAT_MESSAGE = 0x00008005,
	STOP_REPEAT_MESSAGE = 0x00008006,
	GET_DEVICE_CONFIG = 0x8007,
	SET_DEVICE_CONFIG = 0x8008,
	PROTECT_J1939_ADDR = 0x8009,
	REQUEST_CONNECTION,
	TEARDOWN_CONNECTION,
	GET_DEVICE_INFO = 0x800C,
	GET_PROTOCOL_INFO = 0x800D,
	READ_MEMORY = 0x10000,
	WRITE_MEMORY = 0x10001,
	READ_VWALLJACK,
	READ_VPWRVEH,
	TURN_AUX_OFF,
	TURN_AUX_ON,
	START_PULSE,
	READ_VIGNITION,
	CAN_SET_BTR = 0x10100,
	GET_TIMESTAMP,
	SET_TIMESTAMP,
	GET_SERIAL_NUMBER,
	CAN_SET_LISTEN_MODE,
	CAN_SET_ERROR_REPORTING,
	CAN_SET_INTERNAL_TERMINATION = 0x10107,
	DEVICE_RESET = 0x10200
}
J2534IOCTLID;

// Ioctl Parameter ID Values
typedef enum
{
	DATA_RATE = 0x01,
	LOOPBACK = 0x03,
	NODE_ADDRESS,
	NETWORK_LINE,
	P1_MIN,
	P1_MAX,
	P2_MIN,
	P2_MAX,
	P3_MIN,
	P3_MAX,
	P4_MIN,
	P4_MAX,
	W1_MAX,
	W2_MAX,
	W3_MAX,
	W4_MIN,
	W5_MIN,
	TIDLE,
	TINIL,
	TWUP,
	PARITY,
	BIT_SAMPLE_POINT,
	SYNC_JUMP_WIDTH,
	W0_MIN,
	T1_MAX,
	T2_MAX,
	T4_MAX,
	T5_MAX,
	ISO15765_BS,
	ISO15765_STMIN,
	DATA_BITS,
	FIVE_BAUD_MOD,
	BS_TX,
	STMIN_TX,
	T3_MAX,
	ISO15765_WAIT_LIMIT,
    W1_MIN,
    W2_MIN,
	W3_MIN,                
	W4_MAX,                   
	N_BR_MIN,                 
	ISO15765_PAD_VALUE,       
	N_AS_MAX,                 
	N_AR_MAX,                
	N_BS_MAX,                
	N_CR_MAX,                
	N_CS_MIN,                
	CAN_MIXED_FORMAT = 0x8000,
	J1962_PINS = 0x8001,
	SWCAN_HS_DATA_RATE = 0x8010,
	SWCAN_SPEEDCHANGE_ENABLE,
	SWCAN_RES_SWITCH,
	UEB_T0_MIN = 0x8028,
	UEB_T1_MAX,
	UEB_T2_MAX,
	UEB_T3_MAX,
	UEB_T4_MIN,
	UEB_T5_MAX,
	UEB_T6_MAX,
	UEB_T7_MIN,
	UEB_T7_MAX,
	UEB_T9_MIN,
	J1939_PINS = 0x803d,
	J1708_PINS,
	J1939_T1,
	J1939_T2,
	J1939_T3,
	J1939_T4,
	J1939_BRDCST_MIN_DELAY,
	TP2_0_T_BR_INT,
	TP2_0_T_E,
	TP2_0_MNTC,
	TP2_0_T_CTA,
	TP2_0_MNCT,
	TP2_0_MNTB,
	TP2_0_MNT,
	TP2_0_T_WAIT,
	TP2_0_T1,
	TP2_0_T3,
	TP2_0_IDENTIFIER,
	TP2_0_RXIDPASSIVE,
	LIN_VERSION = 0x10001
}
J2534IOCTLPARAMID;

// GET_DEVICE_INFO ID Values
typedef enum
{
	SERIAL_NUMBER = 0x01,
	J1850PWM_SUPPORTED,
	J1850VPW_SUPPORTED,
	ISO9141_SUPPORTED,
	ISO14230_SUPPORTED,
	CAN_SUPPORTED,
	ISO15765_SUPPORTED,
	SCI_A_ENGINE_SUPPORTED,
	SCI_A_TRANS_SUPPORTED,
	SCI_B_ENGINE_SUPPORTED,
	SCI_B_TRANS_SUPPORTED,
	SW_ISO15765_SUPPORTED,
	SW_CAN_SUPPORTED,
	GM_UART_SUPPORTED,
	UART_ECHO_BYTE_SUPPORTED,
	HONDA_DIAGH_SUPPORTED,
	J1939_SUPPORTED,
	J1708_SUPPORTED,
	TP2_0_SUPPORTED,
	J2610_SUPPORTED,
	ANALOG_IN_SUPPORTED,
	MAX_NON_VOLATILE_STORAGE,
	SHORT_TO_GND_J1962,
	PGM_VOLTAGE_J1962,
	J1850PWM_PS_J1962,
	J1850VPW_PS_J1962,
	ISO9141_PS_K_LINE_J1962,
	ISO9141_PS_L_LINE_J1962,
	ISO14230_PS_K_LINE_J1962,
	ISO14230_PS_L_LINE_J1962,
	CAN_PS_J1962,
	ISO15765_PS_J1962,
	SW_CAN_PS_J1962,
	SW_ISO15765_PS_J1962,
	GM_UART_PS_J1962,
	UART_ECHO_BYTE_PS_J1962,
	HONDA_DIAGH_PS_J1962,
	J1939_PS_J1962,
	J1708_PS_J1962,
	TP2_0_PS_J1962,
	J2610_PS_J1962,
	J1939_PS_J1939,
	J1708_PS_J1939,
	ISO9141_PS_K_LINE_J1939,
	ISO9141_PS_L_LINE_J1939,
	ISO14230_PS_K_LINE_J1939,
	ISO14230_PS_L_LINE_J1939,
	J1708_PS_J1708,
	FT_CAN_SUPPORTED,
	FT_ISO15765_SUPPORTED,
	FT_CAN_PS_J1962,
	FT_ISO15765_PS_J1962,
	J1850PWM_SIMULTANEOUS,
	J1850VPW_SIMULTANEOUS,
	ISO9141_SIMULTANEOUS,
	ISO14230_SIMULTANEOUS,
	CAN_SIMULTANEOUS,
	ISO15765_SIMULTANEOUS,
	SCI_A_ENGINE_SIMULTANEOUS,
	SCI_A_TRANS_SIMULTANEOUS,
	SCI_B_ENGINE_SIMULTANEOUS,
	SCI_B_TRANS_SIMULTANEOUS,
	SW_ISO15765_SIMULTANEOUS,
	SW_CAN_SIMULTANEOUS,
	GM_UART_SIMULTANEOUS,
	UART_ECHO_BYTE_SIMULTANEOUS,
	HONDA_DIAGH_SIMULTANEOUS,
	J1939_SIMULTANEOUS,
	J1708_SIMULTANEOUS,
	TP2_0_SIMULTANEOUS,
	J2610_SIMULTANEOUS,
	ANALOG_IN_SIMULTANEOUS,
	PART_NUMBER,
	FT_CAN_SIMULTANEOUS,
	FT_ISO15765_SIMULTANEOUS
}
J2534GETDEVICEINFOPARAMID;

// GET_PROTOCOL_INFO ID Values
typedef enum
{
	MAX_RX_BUFFER_SIZE = 0x01,
	MAX_PASS_FILTER,
	MAX_BLOCK_FILTER,
	MAX_FILTER_MSG_LENGTH,
	MAX_PERIODIC_MSGS,
	MAX_PERIODIC_MSG_LENGTH,
	DESIRED_DATA_RATE,
	MAX_REPEAT_MESSAGING,
	MAX_REPEAT_MESSAGING_LENGTH,
	NETWORK_LINE_SUPPORTED,
	MAX_FUNCT_MSG_LOOKUP,
	PARITY_SUPPORTED,
	DATA_BITS_SUPPORTED,
	FIVE_BAUD_MOD_SUPPORTED,
	L_LINE_SUPPORTED,
	CAN_11_29_IDS_SUPPORTED,
	CAN_MIXED_FORMAT_SUPPORTED,
	MAX_FLOW_CONTROL_FILTER,
	MAX_ISO15765_WFT_MAX,
	MAX_AD_ACTIVE_CHANNELS,
	MAX_AD_SAMPLE_RATE,
	MAX_AD_SAMPLES_PER_READING,
	AD_SAMPLE_RESOLUTION,
	AD_INPUT_RANGE_LOW,
	AD_INPUT_RANGE_HIGH,
	RESOURCE_GROUP,
	TIMESTAMP_RESOLUTION
}
J2534GETPROTOCOLINFOPARAMID;

// SPARAM Support Values
typedef enum
{
	NOT_SUPPORTED,
	SUPPORTED
}
J2534SPARAMSUPPORTED;

// GET_DEVICE_CONFIG / SET_DEVICE_CONFIG Parameter Details
typedef enum
{
	NON_VOLATILE_STORE_1 = 0x0000C001,
	NON_VOLATILE_STORE_2 = 0x0000C002,
	NON_VOLATILE_STORE_3 = 0x0000C003,
	NON_VOLATILE_STORE_4 = 0x0000C004,
	NON_VOLATILE_STORE_5 = 0x0000C005,
	NON_VOLATILE_STORE_6 = 0x0000C006,
	NON_VOLATILE_STORE_7 = 0x0000C007,
	NON_VOLATILE_STORE_8 = 0x0000C008,
	NON_VOLATILE_STORE_9 = 0x0000C009,
	NON_VOLATILE_STORE_10 = 0x0000C00A
}
J2534DEVICECONFIGPARAMID;

// SPARAM Structure
typedef struct
{
	unsigned long ulParameter;          // name of parameter
	unsigned long ulValue;              // value of the parameter
	unsigned long ulSupported;   // support for parameter
}
SPARAM;

// SPARAM_LIST Structure
typedef struct
{
	unsigned long ulNumOfParams;        // number of SPARAM elements
	SPARAM *pParamPtr;                  // array of SPARAM
}
SPARAM_LIST;

// SCONFIG Structure
typedef struct
{
	J2534IOCTLPARAMID Parameter;        // name of parameter
	unsigned long ulValue;              // value of the parameter
}
SCONFIG;

// SCONFIG_LIST Structure
typedef struct
{
	unsigned long ulNumOfParams;        // number of SCONFIG elements
	SCONFIG *pConfigPtr;                // array of SCONFIG
}
SCONFIG_LIST;

// SBYTE_ARRAY Structure
typedef struct
{
	unsigned long ulNumOfBytes;         // number of bytes in the array
	unsigned char *pucBytePtr;          // array of bytes
}
SBYTE_ARRAY;

//SMEMORY_READ
typedef struct
{
	unsigned long ulFunctionCode;       //
	unsigned long ulNumOfBytes;         // number of bytes in the array
	unsigned char *pBytePtr;            // array of bytes
}
SMEMORY_READ;

//SMEMORY_WRITE
typedef struct
{
	char *pszPasscode;
	unsigned long ulFunctionCode;       //
	unsigned long ulNumOfBytes;         // number of bytes in the array
	unsigned char *pBytePtr;            // array of bytes
}
SMEMORY_WRITE;

/*                  J2534 -1 V0500 Spec New Additions                                     */
typedef struct 
{
	char DeviceName[80];
	unsigned long DeviceAvailable;
	unsigned long DeviceDLLFWStatus;
	unsigned long DeviceConnectMedia;
	unsigned long DeviceConnectSpeed;
	unsigned long DeviceSignalQuality;
	unsigned long DeviceSignalStrength;
} SDEVICE;

typedef struct 
{
	unsigned long Connector;        /* connector identifier */
	unsigned long NumOfResources;   /* number of resources pointed to by ResourceListPtr */
	unsigned long *ResourceListPtr; /* pointer to list of resources */
} RESOURCE_STRUCT;

typedef struct 
{
	unsigned long LocalTxFlags;      /* TxFlags for the Local Address */
	unsigned long RemoteTxFlags;     /* TxFlags for the Remote Address */
	char LocalAddress[5];            /* Address for J2534 Device ISO 15765 end point */
	char RemoteAddress[5];           /* Address for remote/vehicle ISO 15765 end point */
} ISO15765_CHANNEL_DESCRIPTOR;

typedef struct 
{
	unsigned long ChannelCount;     /* number of ChannelList elements */
	unsigned long ChannelThreshold; /* minimum number of channels that must have messages */
	unsigned long *ChannelList;     /* pointer to an array of Channel IDs to be monitored */
} SCHANNELSET;

// Declaration of Exported functions.
extern "C" J2534ERROR WINAPI PassThruOpen(void *pName, unsigned long *pulDeviceID);
extern "C" J2534ERROR WINAPI PassThruClose(unsigned long ulDeviceID);
extern "C" J2534ERROR WINAPI PassThruConnect(unsigned long ulDeviceID,
											 J2534_PROTOCOL enProtocolID,
											 unsigned long ulFlags,
											 unsigned long ulBaudRate,
											 RESOURCE_STRUCT ResourceStruct,
											 unsigned long *pulChannelID);
extern "C" J2534ERROR WINAPI PassThruDisconnect(unsigned long ulChannelID);
extern "C" J2534ERROR WINAPI PassThruReadMsgs(unsigned long ulChannelID,
											  PASSTHRU_MSG       *pstrucJ2534Msg,
											  unsigned long *pulNumMsgs,
											  unsigned long ulTimeout);
extern "C" J2534ERROR WINAPI PassThruStartPeriodicMsg(unsigned long ulChannelID,
														PASSTHRU_MSG *pMsgJ2534,
														unsigned long *pulMsgID,
														unsigned long ulTimeout);
extern "C" J2534ERROR WINAPI PassThruStopPeriodicMsg(unsigned long ulChannelID,unsigned long ulMsgID);
extern "C" J2534ERROR WINAPI PassThruStartMsgFilter(unsigned long   ulChannelID,
													unsigned long   enumFilterType,
													PASSTHRU_MSG     *pstrucJ2534Mask,
													PASSTHRU_MSG     *pstrucJ2534Pattern,
													unsigned long *pulFilterID);
extern "C" J2534ERROR WINAPI PassThruStopMsgFilter(unsigned long ulChannelID,unsigned long ulFilterID);
extern "C" J2534ERROR WINAPI PassThruSetProgrammingVoltage(unsigned long ulDeviceID,
	                                                       RESOURCE_STRUCT ResourceList,
	                                                       unsigned long ulVoltage);
extern "C" J2534ERROR WINAPI PassThruReadVersion(unsigned long ulDeviceID,
	                                             char *pchFirmwareVersion,
	                                             char *pchDllVersion,
	                                             char *pchApiVersion);
extern "C" J2534ERROR WINAPI PassThruGetLastError(char *pchErrorDescription);
extern "C" J2534ERROR WINAPI PassThruIoctl(unsigned long ulChannelID,J2534IOCTLID enumIoctlID,void *pInput,void *pOutput);
extern "C" J2534ERROR WINAPI PassThruReadKRSerialNum(unsigned long ulDeviceID,char *pchKRSerialNum);
extern "C" J2534ERROR WINAPI PassThruReadSerialNum(unsigned long ulDeviceID,char *pchSerialNum);
extern "C" J2534ERROR WINAPI PassThruWriteKRString(unsigned long ulDeviceID,char *pchKRString);
extern "C" J2534ERROR WINAPI PassThruReadKRString(unsigned long ulDeviceID,	char *pchKRString);
extern "C" J2534ERROR WINAPI DGPassThruCheckLock(char *pszLock,unsigned char *pucResult);
extern "C" J2534ERROR WINAPI DGPassThruAddLock(char *pszLock,unsigned char *pucResult);
extern "C" J2534ERROR WINAPI DGPassThruDeleteLock(char *pszLock,unsigned char *pucResult);
extern "C" long WINAPI DGSetDeviceAccess(char *pszAccessCode);
extern "C" J2534ERROR WINAPI GetVersionStr(char *pszVersionStr, int lenVersionStr);
extern "C" J2534ERROR WINAPI PassThruReadHWVersion(unsigned long ulDeviceID,char *pchHardwareVersion);

#endif
