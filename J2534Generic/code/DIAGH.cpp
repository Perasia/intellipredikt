//*****************************************************************************
//                  Dearborn Group Technology
//*****************************************************************************
// Project Name         : J2534 Generic
// File Name            : DIAGH.cpp
// Description          : This file contains the implementation of the class 
//                        that is derived from CProtocolBase to support DIAGH 
//                        protocol as required by J2534-2, based on ISO 9141.
// Date                 : September, 2014
// Version              : 1.0
// Author               : Val Koycheva
// Revision             : 
//*****************************************************************************

//Includes
#include "StdAfx.h"
#include "DIAGH.h"

#ifdef J2534_DEVICE_DBRIDGE

//-----------------------------------------------------------------------------
//  Function Name   : CDIAGH
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CDIAGH class
//-----------------------------------------------------------------------------
CDIAGH::CDIAGH(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) :
            CProtocolBase(pclsDevice, pclsDebugLog)
{
    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "CDIAGH()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;
    m_bLoopback = false;
    m_bJ1962Pins = false;
    m_ulPPSS = 0;
    m_usP1MAX = DIAGH_P1MAX_DEFAULT;
    m_usP3MIN = DIAGH_P3MIN_DEFAULT;
    m_usP4MIN = DIAGH_P4MIN_DEFAULT;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "CDIAGH()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CDIAGH
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CDIAGH class
//-----------------------------------------------------------------------------
CDIAGH::~CDIAGH()
{
    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "~CDIAGH()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "~CDIAGH()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vConnect(J2534_PROTOCOL    enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // It is the responsibility of the application to generate and check the checksum.
    m_ulChecksumFlag = ((ulFlags >> 9) & 0x01);
    ulFlags = m_ulChecksumFlag;
    switch(enProtocolID)
    {
    case HONDA_DIAGH_PS:
        {
            if (IsUartBaudRateValid(ulBaudRate) == J2534_ERR_INVALID_BAUDRATE)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
            if( ulBaudRate != DIAGH_DATA_RATE_DEFAULT)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
            m_ulDataRate = ulBaudRate;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnDIAGHRxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    m_ulPPSS = 0;
    m_bJ1962Pins = false;
    m_usP1MAX = DIAGH_P1MAX_DEFAULT;
    m_usP3MIN = DIAGH_P3MIN_DEFAULT;
    m_usP4MIN = DIAGH_P4MIN_DEFAULT;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vReadMsgs(PASSTHRU_MSG     *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
    {
        return(J2534_ERR_PIN_INVALID);
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vWriteMsgs(PASSTHRU_MSG    *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");
    
    if (!m_bJ1962Pins)
    {
        return(J2534_ERR_PIN_INVALID);
    }

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != HONDA_DIAGH_PS)
        {
            // Write to Log File.
            WriteLogMsg("DIAGH.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("DIAGH.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }
    
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg, pulNumMsgs, ulTimeout
#ifdef J2534_0500
		,0
#endif
	)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vStartPeriodicMsg(PASSTHRU_MSG     *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
    {
        return(J2534_ERR_PIN_INVALID);
    }

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != HONDA_DIAGH_PS)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CDIAGH::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::vStartMsgFilter(J2534_FILTER  enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
    {
        return(J2534_ERR_PIN_INVALID);
    }

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != HONDA_DIAGH_PS) ||
        (pstPattern->ulProtocolID != HONDA_DIAGH_PS))
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("DIAGH : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration

            if (!m_bJ1962Pins)
            {
                return(J2534_ERR_PIN_INVALID);
            }
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            if (!m_bJ1962Pins)
            {
                return(J2534_ERR_PIN_INVALID);
            }
            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            if (!m_bJ1962Pins)
            {
                return(J2534_ERR_PIN_INVALID);
            }
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if (!m_bJ1962Pins)
            {
                return(J2534_ERR_PIN_INVALID);
            }
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if (!m_bJ1962Pins)
            {
                return(J2534_ERR_PIN_INVALID);
            }
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

	    case START_REPEAT_MESSAGE:
	    case QUERY_REPEAT_MESSAGE:
	    case STOP_REPEAT_MESSAGE:
            {

                if (!m_bJ1962Pins)
                {
                    return(J2534_ERR_PIN_INVALID);
                }
                if(enumIoctlID == START_REPEAT_MESSAGE)
                {
                    // Check if msg. format is valid.
                    REPEAT_MSG_SETUP *pRepeatMsgSetup = (REPEAT_MSG_SETUP *)pInput;
                    if ((!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PERIODIC]))) ||
                        (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_MASK]))) ||
                        (!IsMsgValid(&(pRepeatMsgSetup->RepeatMsgData[INDEX_PATTERN]))))
                    {
                        // Write to Log File.
                        WriteLogMsg("CAN.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
                        return(J2534_ERR_INVALID_MSG);
                    }
                }
                if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID, pInput, pOutput))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("CAN.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                    return(enumJ2534Error);

                }   
            }
            break;
        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    
    // Write to Log File.
    WriteLogMsg("DIAGH.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnDIAGHRxMessage
//  Input Params    : pstPassThruMsg
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    DIAGH messages.
//-----------------------------------------------------------------------------
void OnDIAGHRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CDIAGH                    *pclsDIAGH;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsDIAGH = (CDIAGH *) pVoid;

    // Only the TX_MSG_TYPE RxStatus bit shall be used by this protocol.
    // START_OF_MESSAGE RxStatus indications shall not be generated for this protocol.

    // Check for NULL pointer.
    if (pclsDIAGH == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("DIAGH.cpp", "OnDIAGHRxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    // Write to Log File.
    CProtocolBase::WriteLogMsg("DIAGH.cpp", "OnDIAGHRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Message CallBack");

    if (!pclsDIAGH->m_bJ1962Pins)
        return;

    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("DIAGH.cpp", "OnDIAGHRxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsDIAGH->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsDIAGH->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }
    // Repeat msg support
    if(pclsDIAGH->m_pclsRepeatMsg != NULL)
    {
        pclsDIAGH->m_pclsRepeatMsg->CompareMsg(pstPassThruMsg);
    }

    // Apply Filters and see if msg. is required.
    if (pclsDIAGH->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsDIAGH->IsMsgValidRx(pstPassThruMsg) &&
            pclsDIAGH->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsDIAGH->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }
    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CDIAGH::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < DIAGH_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > DIAGH_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}      

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CDIAGH::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < DIAGH_MSG_SIZE_MIN_RX) || 
        (pstPassThruMsg->ulDataSize > DIAGH_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
        SetLastErrorText("DIAGH : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate (It is not included in Diag-H J2534 but it is used by HDS)

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case P1_MAX:            // P1_MAX

                pSconfig->ulValue = m_usP1MAX;

                break;

            case P3_MIN:            // P3_MIN

                pSconfig->ulValue = m_usP3MIN;

                break;

            case P4_MIN:            // P4_MIN

                pSconfig->ulValue = m_usP4MIN;

                break;

             case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS;

                break;

           default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::DataRate(unsigned long ulValue)
{
    if( ulValue != DIAGH_DATA_RATE_DEFAULT )
    {
        return(J2534_ERR_INVALID_BAUDRATE);
    }
    m_ulDataRate = (unsigned short) ulValue;
    return J2534_STATUS_NOERROR;
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
        SetLastErrorText("DIAGH : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("DIAGH.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate
                if (!m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = DataRate(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                // Do not proceed here only one speed is supported
                break;

            case LOOPBACK:          // Loopback
                if (!m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = Loopback(pSconfig->ulValue);
                }
                break;

            case P1_MAX:            // P1_MAX
                if (!m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = P1_Max(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("DIAGH.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                }
                break;

            case P3_MIN:            // P3_MIN
                if (!m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = P3_Min(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("DIAGH.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                }
                break;

            case P4_MIN:            // P4_MIN
                if (!m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                }
                else
                {
                    enumJ2534Error = P4_Min(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("DIAGH.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                }
                break;

            case J1962_PINS:    // Sync Jump Width
                if (m_bJ1962Pins)
                {
                    enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                }
                else
                {
                    enumJ2534Error = J1962Pins(pSconfig->ulValue);
                }
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                {
                    return(enumJ2534Error);
                }
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,pSconfig,NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("SWCAN.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                }
                if(enumJ2534Error == J2534_STATUS_NOERROR)
                {
                    m_ulPPSS = pSconfig->ulValue;
                }
                else
                    m_bJ1962Pins = false;
                break;

            default:
                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) 
        {
            // If something is wrong
            return(enumJ2534Error);
        }
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P1_Max
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P1_Max
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::P1_Max(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP1MAX = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P3_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P3_Min
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::P3_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP3MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : P4_Min
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the P4_Min
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::P4_Min(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0x0) || (ulValue > 0xFFFF))  // Invalid value
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }

    m_usP4MIN = (unsigned short) ulValue;

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : J1962Pins
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the J1962 Pins
//-----------------------------------------------------------------------------
J2534ERROR  CDIAGH::J1962Pins(unsigned long ulValue)
{
    // Only Pin 1 and Pin 14 are supported by Diag-H
    if ((ulValue == 0x0100) || (ulValue == 0x0E00))
    {
        m_bJ1962Pins = true;
        return(J2534_STATUS_NOERROR);
    }
    else
    {
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    }
}


#endif