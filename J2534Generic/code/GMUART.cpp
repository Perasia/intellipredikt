/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: GMUART.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support GMUART 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "GMUART.h"

//-----------------------------------------------------------------------------
//  Function Name   : CGMUART
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CGMUART class
//-----------------------------------------------------------------------------
CGMUART::CGMUART(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) : CProtocolBase(pclsDevice, pclsDebugLog)
{
    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "CGMUART()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "CGMUART()", DEBUGLOG_TYPE_COMMENT, "End");
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CGMUART
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CGMUART class
//-----------------------------------------------------------------------------
CGMUART::~CGMUART()
{
    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "~CGMUART()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "~CGMUART()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vConnect(J2534_PROTOCOL enProtocolID,
                              unsigned long   ulFlags,
                              unsigned long ulBaudRate,
                              DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                              LPVOID            pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    /*Get the Check Sum Flag from the connection flags*/
    m_ulChecksumFlag = ((ulFlags >> 9) & 0x01);
//  ulFlags = m_ulChecksumFlag;
    ulFlags = (((ulFlags >> 12) & 0x01) << 1);
    ulFlags |= m_ulChecksumFlag;
    switch(enProtocolID)
    {
    case GM_UART:
        {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            if (ulBaudRate != 8192)
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
#ifdef J2534_0305
            ulBaudRate = 8192;
#endif
            m_ulDataRate = ulBaudRate;
        }
        break;
    default:
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnGMUARTRxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    memcpy(m_ucPollResponseMsg, "\x00\x56\x08\x52", 4);
    m_ulPollResponseMsgLength = 4;
    m_ucMsg_ID = 0;
    m_hPollMsg = CreateEvent(NULL, TRUE, TRUE, NULL);
    m_ulPPSS = 0;
    m_bJ1962Pins = false;
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    CloseHandle(m_hPollMsg);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vReadMsgs(PASSTHRU_MSG      *pstPassThruMsg,
                               unsigned long    *pulNumMsgs,
                               unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vWriteMsgs(PASSTHRU_MSG *pstPassThruMsg,
                                unsigned long   *pulNumMsgs,
                                unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != GM_UART)
        {
            // Write to Log File.
            WriteLogMsg("GMUART.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }

        // Check if msg. format is valid.
        if (!IsMsgValid(pstPassThruMsg + ulIdx1))
        {
            // Write to Log File.
            WriteLogMsg("GMUART.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vStartPeriodicMsg(PASSTHRU_MSG      *pstPassThruMsg,
                                       unsigned long    *pulMsgID,
                                       unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
        return(J2534_ERR_PIN_INVALID);

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != GM_UART)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CGMUART::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::vStartMsgFilter(J2534_FILTER   enumFilterType,
                                      PASSTHRU_MSG  *pstMask,
                                      PASSTHRU_MSG  *pstPattern,
                                      PASSTHRU_MSG  *pstFlowControl,
                                      unsigned long *pulFilterID)
{
    J2534ERROR  enJ2534Error;

    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device cannot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    if (!m_bJ1962Pins)
    {
        return(J2534_ERR_PIN_INVALID);
    }

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != GM_UART) ||
        (pstPattern->ulProtocolID != GM_UART))
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		SetLastErrorText("GMUART : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function. This function is used to read
//                    and write all the protocol hardware and software
//                    configuration parameters for a given enumIoctlID.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::vIoctl(J2534IOCTLID enumIoctlID,
                                 void *pInput,
                                 void *pOutput)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    // IoctlID values
    switch(enumIoctlID)
    {
        case GET_CONFIG:            // Get configuration

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);

            break;

        case SET_CONFIG:            // Set configuration

            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            /*Check if the IOCTL value is not in range */
            break;

        case CLEAR_TX_BUFFER:       // Clear all messages in its transmit queue

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            m_pclsTxCircBuffer->ClearBuffer();

            break;

        case CLEAR_RX_BUFFER:       // Clear all messages in its receive queue
            
            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            m_pclsRxCircBuffer->ClearBuffer();

            break;

        case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }

            break;

        case CLEAR_MSG_FILTERS:     // Clear all message filters

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
            break;

        case SET_POLL_RESPONSE:     // SET_POLL_RESPONSE

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            enumJ2534Error = SetPollResponse((SBYTE_ARRAY *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            break;

        case BECOME_MASTER:         // BECOME_MASTER

            if (!m_bJ1962Pins)
                enumJ2534Error = J2534_ERR_PIN_INVALID;
            else
            enumJ2534Error = BecomeMaster((unsigned char *)pInput);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            break;

        default:                    // Others not supported

            enumJ2534Error = J2534_ERR_NOT_SUPPORTED;

            break;
    }

    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);

    // Write to Log File.
    WriteLogMsg("GMUART.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnGMUARTRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    GMUART messages.
//-----------------------------------------------------------------------------
void OnGMUARTRxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                        LPVOID pVoid)
{
    CGMUART                 *pclsGMUART;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsGMUART = (CGMUART *) pVoid;

    // Check for NULL pointer.
    if (pclsGMUART == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("GMUART.cpp", "OnGMUARTRxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    if (!pclsGMUART->m_bJ1962Pins)
        return;

    // Check if this is a Rx First Byte message.
    if (pstPassThruMsg->ulRxStatus & 0x02)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("GMUART.cpp", "OnGMUARTRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx First Byte CALLBACK");

        // Enqueue to Circ Buffer.
        pclsGMUART->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));

        return;
    }

    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        if (pstPassThruMsg->ulDataSize == 4)
        {
            if (pstPassThruMsg->ucData[1] == 0x56)
            {
                pclsGMUART->bIsPollMsgThreeByte = false;
                pclsGMUART->m_ucDevice_ID = pstPassThruMsg->ucData[2];
                SetEvent(pclsGMUART->m_hPollMsg);
            }
        }
        else if (pstPassThruMsg->ulDataSize == 3)
        {
            if (pstPassThruMsg->ucData[1] == 0x55)
            {
                pclsGMUART->bIsPollMsgThreeByte = true;
                SetEvent(pclsGMUART->m_hPollMsg);
            }
        }

        if (pclsGMUART->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsGMUART->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        return;
    }

    // Apply Filters and see if msg. is required.
    if (pclsGMUART->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsGMUART->IsMsgValid(pstPassThruMsg) &&
            pclsGMUART->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsGMUART->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
        }
    }

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CGMUART::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < GMUART_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > GMUART_MSG_SIZE_MAX))
    {
        return(false);
    }

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("GMUART.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("GMUART : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);

#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case J1962_PINS:

                pSconfig->ulValue = m_ulPPSS;

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
/*  if ((ulValue == 4800) || (ulValue == 9600) || (ulValue == 9615) || 
        (ulValue == 9800) || (ulValue == 10000) || (ulValue == 10400) ||
        (ulValue == 10870) || (ulValue == 11905) || (ulValue == 12500) || 
        (ulValue == 13158) || (ulValue == 13889) || (ulValue == 14706) ||
        (ulValue == 15625) || (ulValue == 19200))
*/  if (ulValue == 8192)
    {
        m_ulDataRate = ulValue;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);
    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;
    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("GMUART.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("GMUART : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);

#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    for (ulCount=0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                            pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("GMUART.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                if (!m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = DataRate(pSconfig->ulValue);
                /*Check if the IOCTL value is not in range */
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("GMUART.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                break;

            case LOOPBACK:          // Loopback

                if (!m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_PIN_INVALID;
                else
                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case J1962_PINS:    // Sync Jump Width

                if (m_bJ1962Pins)
                    enumJ2534Error = J2534_ERR_INVALID_IOCTL_VALUE;
                else
                enumJ2534Error = J1962Pins(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("GMUART.cpp", "SetConfig()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }
                if(enumJ2534Error == J2534_STATUS_NOERROR)
                    m_ulPPSS = pSconfig->ulValue; 
                else
                    m_bJ1962Pins = false;
                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetPollResponse
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to set poll response.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::SetPollResponse(SBYTE_ARRAY *pInput)
{
    J2534ERROR      enumJ2534Error;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    
    // Make sure pInput and pOutput are not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    enumJ2534Error = J2534_STATUS_NOERROR;
    memcpy(m_ucPollResponseMsg, pInput->pucBytePtr, pInput->ulNumOfBytes);
    m_ulPollResponseMsgLength = pInput->ulNumOfBytes;
    m_ucMsg_ID = (unsigned char) m_ucPollResponseMsg[0];

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : BecomeMaster
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is used to become master.
//-----------------------------------------------------------------------------
J2534ERROR  CGMUART::BecomeMaster(unsigned char *pucPoll_ID)
{
    J2534ERROR      enumJ2534Error;
    //unsigned char ucFivebaud;
    //unsigned char ucTester;
    //unsigned char ucKwp2000;
    PASSTHRU_MSG    stPassThruMsg;
    unsigned long   ulNumMsgs;
    unsigned long   ulTimeout;
    // Make sure pInput and pOutput are not NULL
    if (pucPoll_ID == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (*pucPoll_ID == 0)
    {
        ulNumMsgs = 1;
        ulTimeout = 0;
        memset(&stPassThruMsg, 0, sizeof(stPassThruMsg));
        stPassThruMsg.ulProtocolID = GM_UART;
        stPassThruMsg.ulDataSize = m_ulPollResponseMsgLength;
        memcpy(stPassThruMsg.ucData, m_ucPollResponseMsg, stPassThruMsg.ulDataSize);
//      if (m_ucMsg_ID == 0)
//      {
//          stPassThruMsg.ucData[0] = m_ucDevice_ID;
//      }
        vWriteMsgs(&stPassThruMsg, &ulNumMsgs, ulTimeout);
    }
    else
    {
        ResetEvent(m_hPollMsg);
        if (WaitForSingleObject(m_hPollMsg, 2000) == WAIT_OBJECT_0)
        {
            if (bIsPollMsgThreeByte && m_ucMsg_ID == 0)
            {
                SetLastErrorText("GMUART : Invalid Poll Msg");
                return(J2534_ERR_FAILED);

            }

            ulNumMsgs = 1;
            ulTimeout = 0;
            memset(&stPassThruMsg, 0, sizeof(stPassThruMsg));
            stPassThruMsg.ulProtocolID = GM_UART;
            stPassThruMsg.ulDataSize = m_ulPollResponseMsgLength;
            memcpy(stPassThruMsg.ucData, m_ucPollResponseMsg, stPassThruMsg.ulDataSize);
            if (!bIsPollMsgThreeByte && m_ucMsg_ID == 0)
            {
                stPassThruMsg.ucData[0] = m_ucDevice_ID;
            }
            vWriteMsgs(&stPassThruMsg, &ulNumMsgs, ulTimeout);
        }
        else
        {
            SetLastErrorText("GMUART : Failed to send Poll Msg");
            return(J2534_ERR_FAILED);

        }
    }

    enumJ2534Error = J2534_STATUS_NOERROR;

    return(enumJ2534Error);
}

J2534ERROR  CGMUART::J1962Pins(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
        
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    if ((ulValue < 0) || (ulValue > 0xFFFF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);


    if (((ulValue & 0xFF00) > 0x1000) || ((ulValue & 0x00FF) > 0x0010))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue != 0x0000) &&
        ((ulValue >> 8) & 0x00FF) == (ulValue & 0x00FF))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (((ulValue >> 8) == 4) || (ulValue == 4) ||
        ((ulValue >> 8) == 5) || (ulValue == 5) ||
        ((ulValue >> 8) == 16) || (ulValue == 16))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    // J1962 Pins
    if (//(ulValue == 0x0000) || 
        (ulValue == 0x0900) || 
        (ulValue == 0x0901))
    {
        m_bJ1962Pins = true;
    }
    else
    {
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
    }
    return(enumJ2534Error);
}
