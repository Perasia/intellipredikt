/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: CircBuffer.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of class that is
*              used for buffering messages. The class uses a
*              circular buffer to store messages.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "CircBuffer.h"



//-----------------------------------------------------------------------------
//  Function Name   : CCircBuffer()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This is a constructor.
//-----------------------------------------------------------------------------
CCircBuffer::CCircBuffer(unsigned long ulBufferSize,
	bool *pbError)
{
	m_bBufferEmpty = true;
	m_bBufferOverWritten = false;
	m_bBufferFull = false;
	m_ulTotalSize = ulBufferSize;
	*pbError = false;

	// Event for Synchronization.
	m_hCriticalSection = CreateMutex(NULL, FALSE, NULL);
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CCircBuffer()
//  Input Params    : void
//  Output Params   : void
//  Return          : None
//  Description     : This is a destructor.
//-----------------------------------------------------------------------------
CCircBuffer::~CCircBuffer()
{
	MsgStruct   *msg;

	// Clear buffer
	while (MsgList.size() > 0)
	{
		msg = MsgList[0];
		if (MsgList[0]->m_pucData != NULL) {
			free(MsgList[0]->m_pucData);
			MsgList[0]->m_pucData = NULL;
		}
		MsgList.erase(MsgList.begin());
		delete msg;
		msg = NULL;
	}

	if (m_hCriticalSection != NULL)
	{
		CloseHandle(m_hCriticalSection);
		m_hCriticalSection = NULL;
	}
}

//-----------------------------------------------------------------------------
//  Function Name   : GetBufferFullStatus()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Checks the buffer to see if it full. The parameter will
//                    contain the status. If full, true is sent otherwise false.
//-----------------------------------------------------------------------------
bool CCircBuffer::GetBufferFullStatus(bool* pbBufferFull)
{
	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		return(false);
	}

	*pbBufferFull = m_bBufferFull;
	ReleaseMutex(m_hCriticalSection);
	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetBufferEmptyStatus()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Checks the buffer to see if it Empty. The parameter will
//                    contain the status. If Empty, true is sent otherwise false.
//-----------------------------------------------------------------------------
bool CCircBuffer::GetBufferEmptyStatus(bool* pbBufferEmpty)
{
	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		return(false);
	}

	*pbBufferEmpty = m_bBufferEmpty;
	ReleaseMutex(m_hCriticalSection);
	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetBufferOverflowStatus()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Checks the buffer to see if it overflowed. The parameter 
//                    will contain the status. If overflowed, true is sent 
//                    otherwise false.
//-----------------------------------------------------------------------------
bool CCircBuffer::GetBufferOverflowStatus(bool* pbBufferOverflow)
{
	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		return(false);
	}

	*pbBufferOverflow = m_bBufferOverWritten;
	ReleaseMutex(m_hCriticalSection);
	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : ClearBuffer()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Clears the circular buffer by resetting the pointers
//                    and flags.
//-----------------------------------------------------------------------------
void CCircBuffer::ClearBuffer()
{
	MsgStruct   *msg;

	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		return;
	}

	// Clear buffer
	while (MsgList.size() > 0)
	{
		msg = MsgList[0];
		if (MsgList[0]->m_pucData != NULL) {
			free(MsgList[0]->m_pucData);
			MsgList[0]->m_pucData = NULL;
		}
		MsgList.erase(MsgList.begin());
		delete msg;
		msg = NULL;
	}

	m_bBufferEmpty = true;
	m_bBufferOverWritten = false;
	m_bBufferFull = false;

	ReleaseMutex(m_hCriticalSection);
}

//-----------------------------------------------------------------------------
//  Function Name   : Write()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Writes to circular buffer. Each message in the 
//                    buffer is a fixed size. 
//-----------------------------------------------------------------------------
bool CCircBuffer::Write(unsigned char *pucMsgs, unsigned long uiMsgLen)
{
	MsgStruct   *msg;

	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		return(false);
	}

	if (pucMsgs == NULL)
	{
		ReleaseMutex(m_hCriticalSection);
		return(false);
	}

	// copy Data to memory at write pointer.
	while (MsgList.size() >= m_ulTotalSize)
	{
		msg = MsgList[0];
		if (MsgList[0]->m_pucData != NULL) {
			free(MsgList[0]->m_pucData);
			MsgList[0]->m_pucData = NULL;
		}
		MsgList.erase(MsgList.begin());
		delete msg;
		msg = NULL;
		m_bBufferOverWritten = true;
	}

	msg = new MsgStruct;

	if (msg == NULL)
	{
		ReleaseMutex(m_hCriticalSection);
		return(false);
	}

	msg->m_pucData = (unsigned char *)malloc(uiMsgLen * sizeof(char) + 1);

	if (msg->m_pucData == NULL)
	{
		delete msg;
		msg = NULL;
		ReleaseMutex(m_hCriticalSection);
		return(false);
	}
#if (defined J2534_0404 || defined J2534_0305 )

	msg->m_ulLen = uiMsgLen;
	memcpy(msg->m_pucData, pucMsgs, uiMsgLen);
	MsgList.push_back(msg);
	if (MsgList.size() == m_ulTotalSize)
	{
		m_bBufferFull = true;
	}
#endif

#ifdef J2534_0500
	PASSTHRU_MSG *tmp = (PASSTHRU_MSG*)(pucMsgs);
	memset(msg->m_pucData, 0, (uiMsgLen * sizeof(char) + 1));
	msg->m_ulLen = uiMsgLen;
	//copy first 7 bytes
	int len = 0;
	memcpy(( msg->m_pucData + len ), (tmp), (7 * sizeof ( unsigned long)));
	len += (7 * sizeof(unsigned long));
	memcpy((msg->m_pucData + len), (tmp->ucData), tmp->ulDataSize); // Extract buffer
	len += tmp->ulDataSize;
	memcpy((msg->m_pucData + len), &(tmp->ulDataBufferSize), sizeof (unsigned long)); // copy last byte
	MsgList.push_back(msg);

	if (MsgList.size() == m_ulTotalSize)
	{
		m_bBufferFull = true;
	}
#endif
	m_bBufferEmpty = false;

	ReleaseMutex(m_hCriticalSection);
	return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : Read()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : Reads from the circular buffer. Each message in the 
//                    buffer is a fixed size. 
//-----------------------------------------------------------------------------
bool CCircBuffer::Read(unsigned char *pucMsgs, unsigned long *pulNumMsgs)
{
	unsigned long   ulNumMsgsRead;
	MsgStruct       *msg;

	ulNumMsgsRead = 0;

	if (WaitForSingleObject(m_hCriticalSection, CIRCBUFFER_CRITICAL_SECTION_TIMEOUT) != WAIT_OBJECT_0)
	{
		pulNumMsgs = 0;
		return(false);
	}

	if (pucMsgs == NULL)
	{
		pulNumMsgs = 0;
		ReleaseMutex(m_hCriticalSection);
		return(false);
	}
#if (defined J2534_0404 || defined J2534_0305 )
	// Read requested number of msgs. or until the buffer
	// is empty.
	while (!m_bBufferEmpty && (ulNumMsgsRead < *pulNumMsgs))
	{
		// copy Data to memory at write pointer.
		memcpy(pucMsgs, MsgList[0]->m_pucData, MsgList[0]->m_ulLen);

		// Advance the User buffer for next message.
		pucMsgs = pucMsgs + sizeof(PASSTHRU_MSG);

		msg = MsgList[0];
		if (MsgList[0]->m_pucData != NULL) {
			free(MsgList[0]->m_pucData);
			MsgList[0]->m_pucData = NULL;
		}
		MsgList.erase(MsgList.begin());
		delete msg;
		msg = NULL;

		m_bBufferOverWritten = false;

		ulNumMsgsRead++;

		if (MsgList.empty())
			m_bBufferEmpty = true;
	}
#endif
#ifdef J2534_0500
	// Read requested number of msgs. or until the buffer
	// is empty.
	while (!m_bBufferEmpty && (ulNumMsgsRead < *pulNumMsgs))
	{

		unsigned int buflen = 0;
		PASSTHRU_MSG* tmpMsg = (PASSTHRU_MSG*)pucMsgs;
		PASSTHRU_MSG *dataBuff = (PASSTHRU_MSG*) (MsgList[0]->m_pucData);
		tmpMsg->ucData = new unsigned char[dataBuff->ulDataSize + 1];

		memset(tmpMsg->ucData, 0, dataBuff->ulDataSize + 1);
		memcpy(tmpMsg, (MsgList[0]->m_pucData + buflen), (7 * sizeof(unsigned long)));
		buflen += (7 * sizeof(unsigned long));
		memcpy(tmpMsg->ucData, (MsgList[0]->m_pucData + buflen), dataBuff->ulDataSize); // Extract buffer
		buflen += dataBuff->ulDataSize;
		memcpy(&tmpMsg->ulDataBufferSize, (MsgList[0]->m_pucData + buflen), sizeof(unsigned long)); // copy last byte	
		msg = MsgList[0];
		if (MsgList[0]->m_pucData != NULL) {
			free(MsgList[0]->m_pucData);
			MsgList[0]->m_pucData = NULL;
		}
		MsgList.erase(MsgList.begin());
		delete msg;
		msg = NULL;

		m_bBufferOverWritten = false;

		ulNumMsgsRead++;

		if (MsgList.empty())
			m_bBufferEmpty = true;
	}
#endif
	//return no of messages read
	*pulNumMsgs = ulNumMsgsRead;

	ReleaseMutex(m_hCriticalSection);
	return(true);
}