/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: ProtocolBase.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              ProtocolBase Class.
* Note:
*
*******************************************************************************/

#ifndef _PROTOCOLBASE_H_
#define _PROTOCOLBASE_H_

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500

#include "J2534_0500.h"
#include "LogicalChannelList.h"
#endif

#include "..\..\DeviceCayman\code\DeviceCayman.h"


#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"
#include "RepeatMsg.h"
#include "DebugLog.h"
#include "DataLog.h"

#define PROTOCOLBASE_ERROR_TEXT_SIZE            120
#define PROTOCOLBASE_PUMPTHREAD_END_TIMEOUT     5000
#define PROTOCOLBASE_PUMP_SYNC_TIMEOUT          3000

typedef struct
{
    CCircBuffer     *pclsCircBuffer;
    CDeviceBase     *pclsDevice;
    HANDLE          hPendingEvent;
    HANDLE          hBuffEmptyEvent;
    unsigned long   ulDevChannelRef;
}
PROTOCOLBASE_PUMP_DATA;

UINT WritePumpThread(LPVOID lpVoid);

// CProtocolBase Class
class CProtocolBase
{
public:
    CProtocolBase(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
    ~CProtocolBase();

    virtual J2534ERROR                  vConnect(
                                                J2534_PROTOCOL  enProtocolID,
                                                unsigned long   ulFlags,
                                                unsigned long   ulBaudRate,
                                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
                                                LPVOID          pVoid=NULL);
#ifdef J2534_0500
	virtual J2534ERROR                  vLogicalChannelConnect(
												void *,
												J2534_PROTOCOL,
												unsigned long   *pulChannelID);

	virtual J2534ERROR                  vLogicalChannelDisConnect(unsigned long   pulChannelID);

	virtual J2534ERROR                  vReadMsgs(PASSTHRU_MSG *pstrucJ2534Msg,
												  unsigned long *pulNumMsgs,
												  unsigned long ulTimeout,
												  unsigned long ulChannelId);

	virtual J2534ERROR                  vIoctl(J2534IOCTLID enumIoctlID,
											   void *pInput,
											   void *pOutput,
											   unsigned long ulChannelID);

#endif
    virtual J2534ERROR                  vDisconnect(bool bAuto=false);
    virtual J2534ERROR                  vReadMsgs(
                                                PASSTHRU_MSG *pstrucJ2534Msg, 
                                                unsigned long *pulNumMsgs, 
                                                unsigned long ulTimeout
												);
    virtual J2534ERROR                  vWriteMsgs(
                                                PASSTHRU_MSG *pstrucJ2534Msg, 
                                                unsigned long *pulNumMsgs, 
                                                unsigned long ulTimeout
#ifdef J2534_0500				
												,unsigned long  ulChannelId
#endif
															);
    virtual J2534ERROR                  vStartPeriodicMsg(
                                                PASSTHRU_MSG    *pstrucJ2534Msg,
                                                unsigned long *pulMsgID,
                                                unsigned long ulTimeInterval
#ifdef J2534_0500
		,unsigned long    ulChannelId
#endif
		);

    virtual J2534ERROR                  vStopPeriodicMsg(unsigned long ulMsgID);

    virtual J2534ERROR                  vStartMsgFilter(
                                                J2534_FILTER    enFilterType,
                                                PASSTHRU_MSG    *pstrucJ2534Mask,
                                                PASSTHRU_MSG    *pstrucJ2534Pattern,
                                                PASSTHRU_MSG    *pstrucJ2534FlowControl,
                                                unsigned long   *pulFilterID
#ifdef J2534_DEVICE_SVCI1
                                                ,
                                                bool            bTP2_0_Implicit = false,
                                                bool            bTP2_0_Implicit_Passive = false
#endif
                                                );
    virtual J2534ERROR                  vStopMsgFilter(unsigned long ulFilterID
#ifdef J2534_DEVICE_SVCI1
                                                ,
                                                bool            bTP2_0_Implicit = false,
                                                bool            bTP2_0_Implicit_Passive = false
#endif
                                                );

    virtual J2534ERROR                  vIoctl(J2534IOCTLID enumIoctlID,
                                                void *pInput,
                                                void *pOutput
												);

    static void WriteLogMsg(char *chFileName, char *chFunctionName, unsigned long MsgType, char * chMsg, ...);
    static void WriteDataLogMsg(char *csChannelPins, char *csProtocol, unsigned long ulRxTx, unsigned long ulTimestamp, char* csID, char *csData);
    bool                                AddToPumpDatabase(
                                                PROTOCOLBASE_PUMP_DATA *pstAddData,
                                                unsigned long          *pulPumpRefID);
    bool                                DeleteFromPumpDatabase(
                                                unsigned long ulPumpRefID, 
                                                bool          bRemoveAll=false);
    bool                                IsEmptyPumpDatabase();
    bool                                StartWritePump();
    bool                                StopWritePump();
    J2534ERROR                          SetMsgPending(unsigned long ulPumpRefID, 
                                                      unsigned long *pulTimeout);
    J2534ERROR                          IsMsgSent(unsigned long ulPumpRefID, 
                                                  unsigned long *pulTimeout);
    J2534ERROR vProgrammingVoltage(unsigned long ulPin,
                                                unsigned long ulVoltage);
#ifdef J2534_DEVICE_DBRIDGE
    J2534ERROR                          IoctlRepeatMsg(J2534IOCTLID enumIoctlID,
                                                void *pInput,
                                                void *pOutput);
#endif

    HANDLE                              m_hBuffEmptyEvent;
    HANDLE                              m_hPendingEvent;
    CDeviceBase                         *m_pclsDevice;
    CCircBuffer                         *m_pclsRxCircBuffer;
    CCircBuffer                         *m_pclsTxCircBuffer;
#ifdef J2534_0500
	CLogicalChannelList					*m_pclsLogicalChannel;
	unsigned long						m_ulPhyChnRxCount;
#endif
    CFilterMsg                          *m_pclsFilterMsg;
    CPeriodicMsg                        *m_pclsPeriodicMsg;
    CRepeatMsg                          *m_pclsRepeatMsg; 
    bool                                m_bConnected;
    unsigned long                       m_ulConnectFlag;
    unsigned long                       m_ulDevChannelRef;
    unsigned long                       m_ulPumpRefID;
    static J2534ERROR                   m_J2534WriteError;
    static CDebugLog                    *m_pclsLog;
    static CDataLog                     *m_pclsDataLog;
    static CMap<unsigned long, unsigned long, PROTOCOLBASE_PUMP_DATA *, PROTOCOLBASE_PUMP_DATA *> m_stPumpDatabase;
    static HANDLE                       m_hTxBuffer;
    static HANDLE                       m_hWritePumpSync;
    static HANDLE                       m_hWritePumpExit;
    static HANDLE                       m_hWritePumpExited;
    static CWinThread                   *m_pclsWritePumpThread;
    static unsigned long                m_ulInstance;
    unsigned long                       m_ulDataRate;
    unsigned long                       m_ulCANDataRateDefault;
    bool                                m_bLoopback;
	bool							    m_bErrorReporting;
    unsigned long                       m_ulCANHeaderSupport,
                                        m_ulCANHeaderBits;
    unsigned long                       m_ulChecksumFlag;
    static CRITICAL_SECTION             CProtocolBaseSection;
private:
    char                                m_szLastErrorText[PROTOCOLBASE_ERROR_TEXT_SIZE];

    J2534ERROR                          TranslateDeviceError(CDeviceBase *pclsDevice, 
                                                            J2534ERROR enDeviceError);
};

extern void SetLastErrorText(char *pszErrorText, ...);
extern void GetLastErrorText(char *pszErrorText, ...);
#endif
