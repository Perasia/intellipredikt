/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534Registry.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of the 
*              Logging implementation
* Note:
*
*******************************************************************************/

#include "StdAfx.h"
#include "J2534Registry.h"


//-----------------------------------------------------------------------------
//  Function Name   : CJ2534Registry()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a CJ2534Registry class constructor
//-----------------------------------------------------------------------------
CJ2534Registry::CJ2534Registry()
{
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ2534Registry()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a CJ2534Registry class destructor
//-----------------------------------------------------------------------------
CJ2534Registry::~CJ2534Registry()
{
}

//-----------------------------------------------------------------------------
//  Function Name   : FindJ2534Entry()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : This is a function to find the J2534 device entry in
//                    the registry.
//-----------------------------------------------------------------------------
HKEY* CJ2534Registry::FindJ2534Entry(char chJ2534DeviceName[J2534REGISTRY_MAX_STRING_LENGTH])
{
	DWORD			dwDword, dwString;
	char			chTempstr  [J2534REGISTRY_MAX_STRING_LENGTH];
	unsigned char   ucValueread[J2534REGISTRY_MAX_STRING_LENGTH];
	char            chValueset[J2534REGISTRY_MINI_STRING_LENGTH];   // used to set String values
	HKEY            hMainkey;
    HKEY            hTempkey;           // handle for key
    HKEY            *phTempkey;
    int             i, iHighdevice, iHighvendor;
	long            lSuccess;
	DWORD           dwDatasize;

    dwDword = REG_DWORD, dwString = REG_SZ;
    i=0, iHighdevice=1, iHighvendor=1;
    hMainkey = NULL;
    hTempkey = NULL;
    phTempkey = NULL;

#if defined (J2534_0404) || defined (J2534_0500) // To implement J2534_0500 Spec implementation 

	/*Block is required to check Windows OS > 10 and < 10*/
	strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH);  
	lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0, KEY_ALL_ACCESS, &hMainkey);
	if (lSuccess != ERROR_SUCCESS)
	{
		strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH_6432);  
		lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0, KEY_ALL_ACCESS, &hMainkey);
	}
    if(lSuccess==ERROR_SUCCESS) 
    {
        i=0;
        dwDatasize=1024;
        lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwDatasize);
        if(lSuccess!=ERROR_SUCCESS)
        {
            RegCloseKey(hMainkey);
            return NULL;
        }
        if(chTempstr[strlen(chTempstr)-1]-'0'==iHighvendor)
            iHighvendor++;

        lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 0, KEY_READ, &hTempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwDatasize = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwDatasize);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, sizeof(chValueset)-1, "%s", ucValueread);
                if(!strcmp(chValueset, chJ2534DeviceName))
                {
                    dwDatasize = sizeof(ucValueread);
                    lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_VENDOR, NULL, &dwString, ucValueread, &dwDatasize);
        
                    if(lSuccess==ERROR_SUCCESS)
                    {
                        sprintf_s(chValueset, sizeof(chValueset)-1, "%s", ucValueread);
                        if(!strcmp(chValueset, J2534REGISTRY_VENDOR_NAME))
                        {
                            phTempkey = &hTempkey;
                            break;
                        }
                    }
                }
            }

            RegCloseKey(hTempkey);
            hTempkey = NULL;
            i++;
            dwDatasize=1024;
            lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwDatasize);
            if(lSuccess!=ERROR_SUCCESS)
            {
                break;
            }
            if(chTempstr[strlen(chTempstr)-1]-'0'==iHighvendor)
                iHighvendor++;

            lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 0, KEY_READ, &hTempkey);
        }
    }
#endif
#ifdef J2534_0305
    // Look at each VendorX entry (hMainkey) to match J2534REGISTRY_VENDOR_NAME
    //   then, verify next entry is "Devices"
    //   loop through all 
    HKEY hTempkey2;                     // handle for key
    int j, FoundEntry;
    FoundEntry = FALSE;
    hTempkey2 = NULL;
    strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH);
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0, KEY_QUERY_VALUE | KEY_READ, &hMainkey);

    if(lSuccess==ERROR_SUCCESS)
    {
        i = 0;
        dwDatasize=1024;
        lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwDatasize);
        if(lSuccess!=ERROR_SUCCESS)
        {
            RegCloseKey(hMainkey);
            return NULL;
        }
        lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 0, KEY_QUERY_VALUE | KEY_READ, &hTempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwDatasize = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwDatasize);
        
            if(lSuccess == ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset,J2534REGISTRY_VENDOR_NAME))
                {
                    // Did we find our vendor of choice (us)?
                    if(lSuccess == ERROR_SUCCESS)
                    {   // We found our vendor of choice (us), we will look for a Device that matches
                        j = 0;
                        dwDatasize=1024;
                        lSuccess = RegOpenKeyEx(hTempkey, J2534REGISTRY_DEVICES, 0, KEY_QUERY_VALUE | KEY_READ, &hTempkey2);

                        if(lSuccess == ERROR_SUCCESS)
                            lSuccess = RegEnumKey(hTempkey2, j, chTempstr2, dwDatasize);

                        if(lSuccess!=ERROR_SUCCESS)
                        {
                            break;  // Didn't find our vendor of choice with the device we were looking for - bail out
                        }
                        RegCloseKey(hTempkey);
                        hTempkey = NULL;
                        lSuccess = RegOpenKeyEx(hTempkey2, chTempstr2, 0, KEY_QUERY_VALUE | KEY_READ, &hTempkey);
                        // Did we find a 'Devices' entry?
                        while(lSuccess==ERROR_SUCCESS)
                        {   // We found a 'Devices' entry, look for a Device that matches
                            dwDatasize = sizeof(ucValueread);
                            lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_NAME, NULL, &dwString, ucValueread, &dwDatasize);

                            if(lSuccess==ERROR_SUCCESS)
                            {   // Is this the Device that we are looking for?
                                sprintf_s(chValueset, "%s", ucValueread);
                                if(!strcmp(chValueset,chJ2534DeviceName))
                                {   // This the Device that we are looking for
                                    phTempkey = &hTempkey;
                                    FoundEntry = TRUE;     // Stop looking
                                    break;
                                }
                            }

                            RegCloseKey(hTempkey);
                            hTempkey = NULL;
                            j++;
                            dwDatasize=1024;
                            lSuccess = RegEnumKey(hTempkey2, j, chTempstr2, dwDatasize);
                            if(lSuccess != ERROR_SUCCESS)
                            {   // No device match within our vendor of choice (which is us)
                                lSuccess = ERROR_SUCCESS; // Allow for continued looking at vendors entries for us
                                break;
                            }
                            lSuccess = RegOpenKeyEx(hTempkey2, chTempstr2, 0, KEY_QUERY_VALUE | KEY_READ, &hTempkey);
                        }
                    }
                }
            }
            if (FoundEntry) // No need to look any further, our job is done here
                break;

            i++;
            lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwDatasize);
            if(lSuccess != ERROR_SUCCESS)
            {
                break; // No more vendors to look at, we need to bail out of here
            }
            lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 0, KEY_QUERY_VALUE | KEY_READ, &hTempkey);
        }
    }

    if (hTempkey2 != NULL)
    {
        RegCloseKey(hTempkey2);
    }
#endif
    if (hMainkey != NULL)
    {
        RegCloseKey(hMainkey);
    }
    return phTempkey;
}

void CJ2534Registry::GetDataRecorderSettings(
                        J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig)
{
    CString strDeviceNum;
    int iNumDevices;
    int a;
    char szTemp[MAX_PATH];
    char szDataRecorderInstalledDirectory[MAX_PATH];
    int i;

    i = GetPrivateProfileString("Installation", "Directory", "", szDataRecorderInstalledDirectory, MAX_PATH, "DataRecorder.ini");

    if (i > 0)
    {
        strcpy_s(pstJ2534RegistryConfig->chDataLoggingDirectory, szDataRecorderInstalledDirectory);
    }
    else
    {
        sprintf_s(pstJ2534RegistryConfig->chDataLoggingDirectory, "C:\\Dearborn Group Products\\Data Recorder");
    }

    i = GetPrivateProfileString("Devices", "NumDevices", "", szTemp, MAX_PATH, "DataRecorder.ini");

    if (i > 0)
    {
        iNumDevices = atoi(szTemp);
    }
    else
    {
        iNumDevices = 0;
    }

    for (a = 0; a < iNumDevices; a++)
    {
        strDeviceNum.Format("Device%d", a);
        i = GetPrivateProfileString(strDeviceNum, "Name", "", szTemp, MAX_PATH, "DataRecorder.ini");

        if (i > 0)
        {
            if (strcmp(szTemp, J2534REGISTRY_TOOL_NAME) == 0)
            {
                i = GetPrivateProfileString(strDeviceNum, "FileName", "", szTemp, MAX_PATH, "DataRecorder.ini");

                if (i > 0)
                {
                    strcpy_s(pstJ2534RegistryConfig->chDataLoggingFileName, szTemp);
                }
                else
                {
                    sprintf_s(pstJ2534RegistryConfig->chDataLoggingFileName, "%s\\Logs\\%s",
                        pstJ2534RegistryConfig->chDataLoggingDirectory,
                        J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME);
                }
                i = GetPrivateProfileString(strDeviceNum, "MaxKB", "", szTemp, MAX_PATH, "DataRecorder.ini");

                if (i > 0)
                {
                    if (atol(szTemp) > 0)
                    {
                        pstJ2534RegistryConfig->dwDataLoggingSizeMaxKB = atol(szTemp);
                    }
                    else
                    {
                        pstJ2534RegistryConfig->dwDataLoggingSizeMaxKB = 1024;
                    }
                }
                else
                {
                    pstJ2534RegistryConfig->dwDataLoggingSizeMaxKB = 1024;
                }
                i = GetPrivateProfileString(strDeviceNum, "Append", "", szTemp, MAX_PATH, "DataRecorder.ini");

                if (i > 0)
                {
                    if (atol(szTemp) == 0)
                    {
                        pstJ2534RegistryConfig->dwDataLoggingAppend = 0;
                    }
                    else
                    {
                        pstJ2534RegistryConfig->dwDataLoggingAppend = 1;
                    }
                }
                else
                {
                    pstJ2534RegistryConfig->dwDataLoggingAppend = 0;
                }
                break;
            }
        }
    }
    if (a == iNumDevices)
    {
        sprintf_s(pstJ2534RegistryConfig->chDataLoggingFileName, "%s\\Logs\\%s",
            pstJ2534RegistryConfig->chDataLoggingDirectory,
            J2534REGISTRY_DATARECORDER_DEFAULT_FILENAME);
        pstJ2534RegistryConfig->dwDataLoggingSizeMaxKB = 1024;
        pstJ2534RegistryConfig->dwDataLoggingAppend = 0;
    }
}

//-----------------------------------------------------------------------------
//  Function Name   : GetRegistry()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : This is a function to record Gryphon registry information.
//-----------------------------------------------------------------------------
J2534REGISTRY_ERROR_TYPE CJ2534Registry::GetRegistry(
                        J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig)
{
    HKEY            *phKey;                         // handle for key
    long            lSuccess;
    unsigned char   ucValueread[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD           dwValueread[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD           dwDword, dwString;              
    DWORD           dwDatasize; 
    CString         cszIP;

    dwDword = REG_DWORD, dwString = REG_SZ;
    dwDatasize = sizeof(ucValueread);
    phKey = NULL;

    if (pstJ2534RegistryConfig == NULL)
    {
        return(J2534REGISTRY_ERR_NULL_PARAMETER);
    }
    if (pstJ2534RegistryConfig->enJ2534RegistryDeviceType == J2534REGISTRY_DEVICE_GENERIC)
    {
        phKey = FindJ2534Entry(J2534REGISTRY_TOOL_NAME);
    }
    if(phKey==NULL)
    {
        return(J2534REGISTRY_ERR_DEVICE_NOT_FOUND);
    }

    // DeviceId
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_DEVICE_ID, NULL, &dwDword, (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwDeviceId = dwValueread[0];
#ifndef J2534_0305
    // Vendor
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_VENDOR, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chVendor, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
	//strcpy(pstJ2534RegistryConfig->chVendor, ucValueread);
    //sprintf_s(pstJ2534RegistryConfig->chVendor, "%s", ucValueread);
#endif
    // Name
   dwDatasize = sizeof(ucValueread);
   lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_NAME, NULL, &dwString, 
	   (unsigned char *)pstJ2534RegistryConfig->chName, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    //sprintf_s(pstJ2534RegistryConfig->chName, "%s", ucValueread);
#ifdef J2534_0305
    // ProtocolsSupported
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_PROTOCOLSSUPPORT, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chProtocolSupported, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    sprintf_s(pstJ2534RegistryConfig->chProtocolSupported, "%s", ucValueread);
#endif
    // ConfigApplication
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_CONFIGAPP, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chConfigApplication, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
   // sprintf_s(pstJ2534RegistryConfig->chConfigApplication, "%s", ucValueread);

    // LoggingDirectory
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_LOGGINGDIRECTORY, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chLoggingDirectory, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    //sprintf_s(pstJ2534RegistryConfig->chLoggingDirectory, "%s", ucValueread);

    // LoggingSizeMaxKB
    pstJ2534RegistryConfig->dwLoggingSizeMaxKB = 0;     // Set default
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_LOGGING_MAXSIZE, NULL, &dwDword, 
		(unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess == ERROR_SUCCESS)  // This is optional, so no error if not found in registry
    {
        pstJ2534RegistryConfig->dwLoggingSizeMaxKB = dwValueread[0];
    }
    // FunctionLibrary
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_FUNCLIB, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chFunctionLibrary, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    //sprintf_s(pstJ2534RegistryConfig->chFunctionLibrary, "%s", ucValueread);

    // APIVersion //RMR1
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_APIVERSION, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chAPIVersion, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
   // sprintf_s(pstJ2534RegistryConfig->chAPIVersion, "%s", ucValueread);

    // ProductVersion //RMR1
    dwDatasize = sizeof(ucValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_PRODUCTVERSION, NULL, &dwString, 
		(unsigned char *)pstJ2534RegistryConfig->chProductVersion, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    //sprintf_s(pstJ2534RegistryConfig->chProductVersion, "%s", ucValueread);

#if defined (J2534_0404) || defined (J2534_0500)
    // CAN
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_CAN, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwCAN = dwValueread[0];

    // ISO15765
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_ISO15765, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwISO15765 = dwValueread[0];

#if !defined(J2534_DEVICE_SOFTBRIDGE) && !defined(J2534_DEVICE_WURTH) && !defined(J2534_DEVICE_DELPHI) && !defined(J2534_DEVICE_DPA5) && !defined(J2534_DEVICE_DPA50305) && !defined(J2534_DEVICE_NETBRIDGE) && !defined(J2534_DEVICE_DBRIDGE) && !defined(J2534_DEVICE_DBRIDGE0305)
    // J1850PWM
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_J1850PWM, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwJ1850PWM = dwValueread[0];
#endif

 #if !defined(J2534_DEVICE_SOFTBRIDGE) && !defined(J2534_DEVICE_WURTH) && !defined(J2534_DEVICE_DELPHI) && !defined(J2534_DEVICE_DPA5) && !defined(J2534_DEVICE_DPA50305) && !defined(J2534_DEVICE_NETBRIDGE) && !defined(J2534_DEVICE_DBRIDGE) && !defined(J2534_DEVICE_DBRIDGE0305)
    // J1850VPW
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_J1850VPW, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwJ1850VPW = dwValueread[0];
#endif

#if !defined(J2534_DEVICE_NETBRIDGE)
    // ISO9141
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_ISO9141, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwISO9141 = dwValueread[0];
#endif
    // ISO14230
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_ISO14230, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwISO14230 = dwValueread[0];
#ifndef J2534_DEVICE_DPA5_EURO5
    // SCI_A_ENGINE
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SCI_A_ENGINE, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSCIAEng = dwValueread[0];

    // SCI_A_TRANS
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SCI_A_TRANS, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSCIATrans = dwValueread[0];

    // SCI_B_ENGINE
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SCI_B_ENGINE, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSCIBEng = dwValueread[0];

    // SCI_B_TRANS
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SCI_B_TRANS, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSCIBTrans = dwValueread[0];

    // SW_ISO15765_PS
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SW_ISO15765_PS, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSWISO15765PS = dwValueread[0];

    // SW_CAN_PS
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_SW_CAN_PS, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwSWCANPS = dwValueread[0];

    // GM_UART_PS
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_GM_UART_PS, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwGMUARTPS = dwValueread[0];
#endif
#endif
    // Logging
    dwDatasize = sizeof(dwValueread);
#ifndef J2534_DEVICE_KRHTWIRE_LOG_ALL
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_LOGGING, NULL, &dwDword, 
        (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess!=ERROR_SUCCESS)
    {
        RegCloseKey(*phKey);
        return(J2534REGISTRY_ERR_NO_DATA_KEY_VALUE);
    }
    pstJ2534RegistryConfig->dwLogging = dwValueread[0];
#else
    HANDLE hMutex = CreateMutex(NULL, FALSE, "KeylessRide_Log_On");
    DWORD dwError = GetLastError();
    if(dwError !=ERROR_ALREADY_EXISTS)
    {
        pstJ2534RegistryConfig->dwLogging = 0;
    }
    else
    {
        pstJ2534RegistryConfig->dwLogging = 7;
    }
    CloseHandle(hMutex);
#endif
    // CANDefaultBaudRate override - Initial used in Python support
    dwDatasize = sizeof(dwValueread);
    lSuccess = RegQueryValueEx(*phKey, J2534REGISTRY_CANDEFAULTBAUDRATE, NULL, &dwDword, (unsigned char *)dwValueread, &dwDatasize);
    if (lSuccess == ERROR_SUCCESS)  // This is optional, so no error if not found in registry
    {
        pstJ2534RegistryConfig->dwDefaultCANDataRate = dwValueread[0];
    }
    RegCloseKey(*phKey);
    return(J2534REGISTRY_ERR_SUCCESS);
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteToRegistry()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : This is a function to write registry information.
//-----------------------------------------------------------------------------
J2534REGISTRY_ERROR_TYPE CJ2534Registry::WriteToRegistry(
                        J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig)
{
    long            lSuccess;
    char            chTempstr[J2534REGISTRY_MINI_STRING_LENGTH];
    DWORD           dwSzdata;  
    unsigned char   ucValueread[J2534REGISTRY_MINI_STRING_LENGTH];  // read in String value
    DWORD           dwDword, dwString; 
    char            chValueset[J2534REGISTRY_MINI_STRING_LENGTH];   // used to set String values
    HKEY            hMainkey, hTempkey;
    int             i, iHighdevice, iHighvendor;

    dwDword = REG_DWORD; 
    dwString = REG_SZ; 
    i=0;
    iHighdevice=1; 
    iHighvendor=1;

    if (pstJ2534RegistryConfig == NULL)
    {
        return(J2534REGISTRY_ERR_NULL_PARAMETER);
    }

    strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH);
    lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 
            0, KEY_ALL_ACCESS, &hMainkey);

    if(lSuccess==ERROR_SUCCESS)
    {
        i=0;
        dwSzdata=J2534REGISTRY_MINI_STRING_LENGTH;
        lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwSzdata);
        if(lSuccess!=ERROR_SUCCESS)
        {
            strcpy_s(chTempstr, pstJ2534RegistryConfig->chVendor);
            strcat_s(chTempstr, J2534REGISTRY_HYPHEN_SPACE);
            strcat_s(chTempstr, pstJ2534RegistryConfig->chName);
            RegCreateKey(hMainkey, chTempstr, &hTempkey);
            CreateWriteKeyValues(&hTempkey, *pstJ2534RegistryConfig);
            RegCloseKey(hMainkey);
            RegCloseKey(hTempkey);
            return (J2534REGISTRY_ERR_SUCCESS);
        }
        lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 
                0, KEY_ALL_ACCESS, &hTempkey);
        while(lSuccess==ERROR_SUCCESS)
        {
            dwSzdata = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_NAME, NULL, 
                &dwString, ucValueread, &dwSzdata);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                sprintf_s(chValueset, "%s", ucValueread);
                if(!strcmp(chValueset, pstJ2534RegistryConfig->chName))
                {
                    dwSzdata = sizeof(ucValueread);
                    lSuccess = RegQueryValueEx(hTempkey, J2534REGISTRY_VENDOR, 
                        NULL, &dwString, ucValueread, &dwSzdata);
                
                    if(lSuccess==ERROR_SUCCESS)
                    {
                        sprintf_s(chValueset, "%s", ucValueread);
                        if(!strcmp(chValueset, pstJ2534RegistryConfig->chVendor))
                        {
                            CreateWriteKeyValues(&hTempkey, *pstJ2534RegistryConfig);
                            RegCloseKey(hMainkey);
                            RegCloseKey(hTempkey);
                            break;
                        }
                    }
                }
            }

            i++;
            dwSzdata=J2534REGISTRY_MINI_STRING_LENGTH;
            lSuccess = RegEnumKey(hMainkey, i, chTempstr, dwSzdata);
            if(lSuccess!=ERROR_SUCCESS)
            {
                strcpy_s(chTempstr, pstJ2534RegistryConfig->chVendor);
                strcat_s(chTempstr, J2534REGISTRY_HYPHEN_SPACE);
                strcat_s(chTempstr, pstJ2534RegistryConfig->chName);
                RegCreateKey(hMainkey, chTempstr, &hTempkey);
	            CreateWriteKeyValues(&hTempkey, *pstJ2534RegistryConfig);
                RegCloseKey(hMainkey);
                RegCloseKey(hTempkey);
                break;
            }
            lSuccess = RegOpenKeyEx(hMainkey, chTempstr, 
                    0, KEY_ALL_ACCESS, &hTempkey);
        }
    }
    else
    {
        strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH);
        strcat_s(chTempstr, J2534REGISTRY_DOUBLE_BACKSLASH);
        strcat_s(chTempstr, pstJ2534RegistryConfig->chVendor);
        strcat_s(chTempstr, J2534REGISTRY_HYPHEN_SPACE);
        strcat_s(chTempstr, pstJ2534RegistryConfig->chName);
        lSuccess = RegCreateKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0,0,0,
            KEY_ALL_ACCESS, 0, &hTempkey, 0);

        if (lSuccess != ERROR_SUCCESS)
        {
            return (J2534REGISTRY_ERR_INSUFFICIENT_WRITING_PRIVILEGE);
        }

        CreateWriteKeyValues(&hTempkey, *pstJ2534RegistryConfig);
        RegCloseKey(hTempkey);
    }

    return (J2534REGISTRY_ERR_SUCCESS);
}

void CJ2534Registry::CreateWriteKeyValues(HKEY *phThiskey,
                                J2534REGISTRY_CONFIGURATION stJ2534RegistryConfig)
{
    char    chTempstr[J2534REGISTRY_MINI_STRING_LENGTH];
    DWORD   dwDatasize, dwWorddata;
    CString cszString;

    strcpy_s(chTempstr, stJ2534RegistryConfig.chConfigApplication);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_CONFIGAPP, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwDeviceId;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_DEVICE_ID, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    strcpy_s(chTempstr, stJ2534RegistryConfig.chFunctionLibrary);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_FUNCLIB, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    strcpy_s(chTempstr, stJ2534RegistryConfig.chVendor);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_VENDOR, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    strcpy_s(chTempstr, stJ2534RegistryConfig.chName);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_NAME, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);
    strcpy_s(chTempstr, stJ2534RegistryConfig.chLoggingDirectory);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_LOGGINGDIRECTORY, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwCAN;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_CAN, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwISO15765;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_ISO15765, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwJ1850PWM;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_J1850PWM, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwJ1850VPW;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_J1850VPW, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwISO9141;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_ISO9141, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwISO14230;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_ISO14230, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSCIAEng;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SCI_A_ENGINE, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSCIATrans;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SCI_A_TRANS, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSCIBEng;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SCI_B_ENGINE, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSCIBTrans;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SCI_B_TRANS, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSWISO15765PS;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SW_ISO15765_PS, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwSWCANPS;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_SW_CAN_PS, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwGMUARTPS;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_GM_UART_PS, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);
#ifdef J2534_0305
    strcpy_s(chTempstr, stJ2534RegistryConfig.chProtocolSupported);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_PROTOCOLSSUPPORT, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);
#endif
    strcpy_s(chTempstr, stJ2534RegistryConfig.chAPIVersion);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_APIVERSION, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    strcpy_s(chTempstr, stJ2534RegistryConfig.chProductVersion);
    dwDatasize=strlen(chTempstr)+1;
    RegSetValueEx(*phThiskey, J2534REGISTRY_PRODUCTVERSION, 0, REG_SZ, 
        (unsigned char*)chTempstr, dwDatasize);

    dwWorddata=stJ2534RegistryConfig.dwLogging;
    dwDatasize=4;
    RegSetValueEx(*phThiskey, J2534REGISTRY_LOGGING, 0, REG_DWORD, 
        (unsigned char*)&dwWorddata, dwDatasize);

}

//-----------------------------------------------------------------------------
//  Function Name   : GetDeviceCount()
//  Input Params    : 
//  Output Params   : ulDeviceCount
//  Return          : 
//  Description     : This is a function to get the device count in the PC
//-----------------------------------------------------------------------------
#ifdef J2534_0500
J2534REGISTRY_ERROR_TYPE CJ2534Registry::GetDeviceCount(
	                            SDEVICE *j2534_device_list,
								unsigned long *ulDeviceCount)
{
	long            lSuccess;
	char            chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH];
	DWORD           dwDatasize;
	DWORD           dwDword, dwString;
	HKEY            hMainkey;
	HKEY            hTempkey;//, hTempkey2;         // handle for key
	int             i;

	dwDword = REG_DWORD, dwString = REG_SZ;
	i = 0;
	hTempkey = NULL;
	strcpy_s(chTempstr1, J2534REGISTRY_KEY_PATH);
	lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr1,
		0, KEY_READ, &hMainkey);

	if (lSuccess == ERROR_SUCCESS)
	{
		i = 0;
		while (lSuccess == ERROR_SUCCESS)
		{
			dwDatasize = J2534REGISTRY_MAX_STRING_LENGTH;
			lSuccess = RegEnumKey(hMainkey, i, chTempstr1, dwDatasize);
			if (lSuccess == ERROR_SUCCESS)
			{
				strcpy_s(j2534_device_list[i].DeviceName, chTempstr1);
				j2534_device_list[i].DeviceAvailable = DEVICE_AVAILABLE;
				j2534_device_list[i].DeviceDLLFWStatus = DEVICE_DLL_FW_COMPATIBLE;
				j2534_device_list[i].DeviceConnectMedia = DEVICE_CONN_WIRED;
				j2534_device_list[i].DeviceConnectSpeed = 100000;
				j2534_device_list[i].DeviceSignalQuality = 100;
				j2534_device_list[i].DeviceSignalStrength = 100;
				i++;
			}
		}
		RegCloseKey(hMainkey);
		*ulDeviceCount = i;
	}
	return(J2534REGISTRY_ERR_SUCCESS);
}
#endif