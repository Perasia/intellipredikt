#include "stdafx.h"
#include "LogicalChannelList.h"

#include <algorithm>
#ifdef J2534_0500
CLogicalChannelList::CLogicalChannelList(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID, 
										 CPeriodicMsg *pclsPeriodicMsg, CDebugLog * pclsDebugLog)
{
	int i;

	m_pclsLog = pclsDebugLog;
	m_pclsDevice = pclsDevice;
	m_ulDevChannelID = ulDevChannelID;
	m_pclsPeriodicMsg = pclsPeriodicMsg;

	// Write to Log File.
	WriteLogMsg("CLogicalChannelList()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Initialize the Log Channel Types portion of the list.
	//memset(m_stLogChannelList, 0, sizeof(m_stLogChannelList));	
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		m_stLogChannelList[i].ulBlockSize = 0;
		m_stLogChannelList[i].ulBlockSizeTx = 0xFFFF;
		m_stLogChannelList[i].ulSTmin = 0;
		m_stLogChannelList[i].ulSTminTx = 0xFFFF;
		m_stLogChannelList[i].ulWftMax = 0;
		m_stLogChannelList[i].ulPeriodicId = 0;
		m_stLogChannelList[i].ulLogChannelId = 0;
		m_stLogChannelList[i].RxQueue.clear();
		m_stLogChannelList[i].TxQueue.clear();
		memset(&m_stLogChannelList[i].structChan, 0, sizeof(m_stLogChannelList[i].structChan));
	}
}

CLogicalChannelList::~CLogicalChannelList()
{
	// Stop all Filters
	StopFilter();

	int i;
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{	
		// Clear buffer
		m_stLogChannelList[i].TxQueue.clear();
		m_stLogChannelList[i].RxQueue.clear();

	}
	// Initialize the Log Channel Types portion of the list.
	memset(m_stLogChannelList, 0, sizeof(m_stLogChannelList));
}

J2534ERROR CLogicalChannelList::CreateLogicalChannel(void *struDesc, 
													 J2534_PROTOCOL enProtocolID,
													 unsigned long *ulChannelId)
{
	J2534ERROR      enJ2534Error;
	unsigned long   ulDevFilterID = 0;

	PASSTHRU_MSG *ptrMask, *ptrPattern, *ptrFC;
	ptrMask = new PASSTHRU_MSG;
	ptrPattern = new PASSTHRU_MSG;
	ptrFC = new PASSTHRU_MSG;

	memset(ptrMask, 0, sizeof(PASSTHRU_MSG));
	memset(ptrPattern, 0, sizeof(PASSTHRU_MSG));
	memset(ptrFC, 0, sizeof(PASSTHRU_MSG));

	ptrMask->ucData = new unsigned char[4];
	ptrPattern->ucData = new unsigned char[4];
	ptrFC->ucData = new unsigned char[4];

	memset(ptrMask->ucData, 0, 4);
	memset(ptrPattern->ucData, 0, 4);
	memset(ptrFC->ucData, 0, 4);

	// Write to Log File.
	WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "Start");

	ptrMask->ulProtocolID = enProtocolID;
	ptrMask->ulDataSize = 4;
	memcpy(&ptrMask->ucData[0], ((ISO15765_CHANNEL_DESCRIPTOR*)struDesc)->LocalAddress, 4);

	ptrPattern->ulProtocolID = enProtocolID;
	ptrPattern->ulDataSize = 4;
	memcpy(&ptrPattern->ucData[0], ((ISO15765_CHANNEL_DESCRIPTOR*)struDesc)->LocalAddress, 4);

	ptrFC->ulProtocolID = enProtocolID;
	ptrFC->ulDataSize = 4;
	memcpy(&ptrFC->ucData[0], ((ISO15765_CHANNEL_DESCRIPTOR*)struDesc)->RemoteAddress, 4);

	// Is number of Channels exceeded the maximum.
	if (IsListFull())
	{
		// Write to Log File.
		WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		return(J2534_ERR_EXCEEDED_LIMIT);
	}
	// Check to see if the channel is Unique.
	if (IsLogicalChannelUnique(ptrPattern, ptrFC))
	{
		// Write to Log File.
		WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_UNIQUE);
		return(J2534_ERR_CHANNEL_IN_USE);
	}

	if ((enJ2534Error = m_pclsDevice->vStartFilter(m_ulDevChannelID, 
													J2534_FILTER_FLOW_CONTROL, 
													ptrMask,
													ptrPattern, 
													ptrFC,
													&ulDevFilterID))
													!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		ulDevFilterID = 0;
		return(enJ2534Error);
	}

	// Add the logical channel info. to our internal Database.
	if ((enJ2534Error = Add(ptrPattern,ptrFC, ulDevFilterID, ulChannelId)) !=
							J2534_STATUS_NOERROR)
	{
		// Stop the Device Filter.
		m_pclsDevice->vStopFilter(m_ulDevChannelID, ulDevFilterID);
		// Write to Log File.
		WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

J2534ERROR CLogicalChannelList::DisconnectLogicalChannel(unsigned long ulChannelId)
{
	J2534ERROR      enJ2534Error = J2534_STATUS_NOERROR;
	unsigned long   ulDevFilterID;
	int             i;

	// Write to Log File.
	WriteLogMsg("DisconnectLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Validate if the channel started earlier.
	if (!IsLogChannelValid(ulChannelId))
	{
		// Write to Log File.
		WriteLogMsg("DisconnectLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			ulDevFilterID = m_stLogChannelList[i].ulFilterId;
			break;
		}
	}

	// Stop the filter in device if it were set earlier.
	if (ulDevFilterID != -1)
	{
		if ((enJ2534Error = m_pclsDevice->vStopFilter(m_ulDevChannelID, ulDevFilterID))
			!= J2534_STATUS_NOERROR)
		{
			// Write to Log File.
			WriteLogMsg("DisconnectLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
			//          SetEvent(m_hSyncAccess);
			return(enJ2534Error);
		}
	}

	// Delete the channel
	Delete(ulChannelId);

	// Write to Log File.
	WriteLogMsg("DisconnectLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(enJ2534Error);
}

J2534ERROR CLogicalChannelList::Delete(unsigned long ulChannelId)
{
	int i;
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			memset(&m_stLogChannelList[i], 0, sizeof(m_stLogChannelList[i]));
			break;
		}
	}

	return(J2534_STATUS_NOERROR);
}

bool CLogicalChannelList::IsListFull()
{
	unsigned long ucIdx;

	for (ucIdx = 0; ucIdx < MAX_LOG_PER_PHYSICAL_ID; ucIdx++)
	{
		if (m_stLogChannelList[ucIdx].ulLogChannelId == 0)
			return(false);
	}

	return(true);
}

bool CLogicalChannelList::IsLogicalChannelUnique(PASSTHRU_MSG *pstPattern,
												 PASSTHRU_MSG *pstFlowControl)
{
	bool                bUnique = false;
	unsigned long       i;

	// Run through all the Filters to see if the requested FLOW_CONTROL_FILTER is unique.
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if ((memcmp(m_stLogChannelList[i].structChan.LocalAddress,
			pstPattern->ucData, 4) == 0) && (memcmp(m_stLogChannelList[i].structChan.RemoteAddress,
				pstFlowControl->ucData, 4) == 0))
		{
			bUnique = true;
			break;
		}
	}
	return(bUnique);
}

J2534ERROR	CLogicalChannelList::Add(PASSTHRU_MSG *pstPattern,
									PASSTHRU_MSG *pstFlowControl,
									unsigned long ulDevFilterID,
									unsigned long *puChannelId)
{
	unsigned long   ulChannelId;

	// Get new FilterID.
	if ((ulChannelId = GetNewChannelID()) == MAX_LOG_PER_PHYSICAL_ID)
	{
		// Write to Log File.
		WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		return(J2534_ERR_EXCEEDED_LIMIT);
	}

	// Save Mask filter in our internal buffer.
	memcpy(m_stLogChannelList[ulChannelId].structChan.LocalAddress, pstPattern->ucData,4);

	// Save Pattern filter in our internal buffer.
	memcpy(m_stLogChannelList[ulChannelId].structChan.RemoteAddress, pstFlowControl->ucData, 4);

	if (m_ulDevChannelID == 1)
		// Save Device Channel ID.
		m_stLogChannelList[ulChannelId].ulLogChannelId = ulChannelId + 11;
	else if (m_ulDevChannelID == 2)
		// Save Device Channel ID.
		m_stLogChannelList[ulChannelId].ulLogChannelId = ulChannelId + 21;
	else if (m_ulDevChannelID == 3)
		// Save Device Channel ID.
		m_stLogChannelList[ulChannelId].ulLogChannelId = ulChannelId + 31;
	else if (m_ulDevChannelID == 4)
		// Save Device Channel ID.
		m_stLogChannelList[ulChannelId].ulLogChannelId = ulChannelId + 41;

	// Save Device Filter ID.
	m_stLogChannelList[ulChannelId].ulFilterId = ulDevFilterID;

	// Return the Channel Id to user.
	*puChannelId = m_stLogChannelList[ulChannelId].ulLogChannelId;

	return(J2534_STATUS_NOERROR);
}
//-----------------------------------------------------------------------------
//  Function Name   : GetNewChannelID
//  Input Params    : void
//  Output Params   : void
//  Description     : If all FilterIDs are used, returns 0 otherwise returns new
//                    FilterID to be used.
//-----------------------------------------------------------------------------
unsigned char CLogicalChannelList::GetNewChannelID()
{
	unsigned char   ucIdx;

	for (ucIdx = 0; ucIdx < MAX_LOG_PER_PHYSICAL_ID; ucIdx++)
	{
		if (m_stLogChannelList[ucIdx].ulLogChannelId == 0)
		{
			return ucIdx;
		}
	}
	return(0);	
}
//-----------------------------------------------------------------------------
//  Function Name   : IsFilterIDValid
//  Input Params    : void
//  Output Params   : void
//  Description     : Checks to see if the given FilterID is valid.
//-----------------------------------------------------------------------------
bool CLogicalChannelList::IsLogChannelValid(unsigned long ulChannelId)
{
	int i;
	if (ulChannelId > 0)
	{
		// Run through all the Channels.
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
				return(true);
		}	
	}
	return(false);
}
//-----------------------------------------------------------------------------
//  Function Name   : StopFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This is an override function that stops all the filters.
//-----------------------------------------------------------------------------
J2534ERROR CLogicalChannelList::StopFilter()
{
	unsigned long   i;

	// Write to Log File.
	WriteLogMsg("CLogicalChannelList", DEBUGLOG_TYPE_COMMENT, "Start");
	// Run through all the Filters and delete all of them.
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if ((m_stLogChannelList[i].ulFilterId != -1) && (m_stLogChannelList[i].ulLogChannelId != 0))
		{
			//m_pclsFilterMsg->StopFilter(m_stLogChannelList[i].ulFilterId);
			m_pclsDevice->vStopFilter(m_ulDevChannelID, m_stLogChannelList[i].ulFilterId);
			// Delete the filter
			Delete(m_stLogChannelList[i].ulLogChannelId);
		}
	}

	// Write to Log File.
	WriteLogMsg("CLogicalChannelList", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}

bool CLogicalChannelList::IsMessageAllowedOnLogChannel(PASSTHRU_MSG *pstrucMsg, bool bRxTx, unsigned long ulChannelId)
{
	bool                bUnique = false;
	unsigned long       i;

	if (bRxTx)
	{
		// Run through all the channels to see if the requested channel is unique.
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if ((memcmp(m_stLogChannelList[i].structChan.LocalAddress,
				pstrucMsg->ucData, 4) == 0) && m_stLogChannelList[i].ulLogChannelId == ulChannelId)
			{
				bUnique = true;
				break;
			}
		}
	}
	else
	{
		// Run through all the channels to see if the requested channel is unique.
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if ((memcmp(m_stLogChannelList[i].structChan.RemoteAddress,
				pstrucMsg->ucData, 4) == 0))
			{
				bUnique = true;
				break;
			}
		}
	}	
	return (bUnique);
}
//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------

J2534ERROR	CLogicalChannelList::StartPerioidic(PASSTHRU_MSG	*pstMsg,
												unsigned long *pulPeriodicID,
												unsigned long  ulTimeInterval,
												unsigned long  ulLogChannelId)
{
	J2534ERROR      enJ2534Error = J2534_ERR_MSG_NOT_ALLOWED;
	int i;

	// Check if PeriodicMsg NULL pointers.
	if (m_pclsPeriodicMsg == NULL)
	{
		// Write to Log File.
		WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_FAILED);
		return(J2534_ERR_FAILED);
	}
	// Is number of Channels exceeded the maximum.
	if (IsListFull())
	{
		// Write to Log File.
		WriteLogMsg("CreateLogicalChannel()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		return(J2534_ERR_EXCEEDED_LIMIT);
	}

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if ((memcmp(m_stLogChannelList[i].structChan.LocalAddress,
			pstMsg->ucData, 4) == 0) && m_stLogChannelList[i].ulLogChannelId == ulLogChannelId)
		{
				// Start Periodic on Device.
			/*if ((enJ2534Error = m_pclsDevice->vStartPeriodic(m_ulDevChannelID, pstMsg,
															 ulTimeInterval, pulPeriodicID)) != J2534_STATUS_NOERROR)
			{
				pulPeriodicID = 0;
				return(enJ2534Error);
			}*/
			unsigned long ulPeriodicID = 0;
			if ((enJ2534Error = m_pclsPeriodicMsg->StartPeriodic(pstMsg, pulPeriodicID, ulTimeInterval)) != J2534_STATUS_NOERROR)
			{
				// Write to Log File.
				WriteLogMsg("StartPerioidic()", DEBUGLOG_TYPE_ERROR, "StartPeriodic returned 0x%02X", enJ2534Error);
				return(enJ2534Error);
			}
			//Add the periodic id to the internal database 
			m_stLogChannelList[i].ulPeriodicId = *pulPeriodicID;
			break;
		}
	}

	WriteLogMsg("StartPerioidic()", DEBUGLOG_TYPE_COMMENT, "PeriodicID=%lu LogChannel ID=%lu", *pulPeriodicID, ulLogChannelId);
	WriteLogMsg("StartPerioidic()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enJ2534Error);

	return enJ2534Error;
}

J2534ERROR CLogicalChannelList::StopPeriodic(unsigned long pulRepeatID, unsigned long ulChannelId)
{
	J2534ERROR      enJ2534Error = J2534_ERR_INVALID_MSG;
	int i;

	// Write to Log File.
	WriteLogMsg("StopPeriodic()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Periodic NULL check
	if (m_pclsPeriodicMsg == NULL)
	{
		// Write to Log File.
		WriteLogMsg("StopPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
		return(J2534_ERR_INVALID_MSG_ID);
	}

	// Stop the Periodinc only if not stoped already 
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			// Stop the periodic in device if it were set earlier.
			/*if ((enJ2534Error = m_pclsDevice->vStopPeriodic(m_ulDevChannelID, m_stLogChannelList[i].ulPeriodicId))
				!= J2534_STATUS_NOERROR)
			{
				return(enJ2534Error);
			}*/
			if ((enJ2534Error = m_pclsPeriodicMsg->StopPeriodic(pulRepeatID))
				!= J2534_STATUS_NOERROR)
			{
				// Write to Log File.
				WriteLogMsg("StopPeriodic", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
				return(enJ2534Error);
			}
			m_stLogChannelList[i].ulPeriodicId = -1;
			break;
		}
	}

	// Write to Log File.
	WriteLogMsg("StopPeriodic()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return enJ2534Error;
}

J2534ERROR CLogicalChannelList::ReadFromLogicalChannel(unsigned long ulChannelId,
													   unsigned long* pulNumMsgs,
													   PASSTHRU_MSG *pucMsgs)
{
	J2534ERROR      enJ2534Error = J2534_STATUS_NOERROR;
	int i;
	unsigned long ulNumMsgsRead = 0;

	// Write to Log File.
	WriteLogMsg("ReadFromLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "Start");

	if (pucMsgs == NULL)
		return J2534_ERR_NULLPARAMETER;

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			if (m_stLogChannelList[i].RxQueue.empty())
				return J2534_ERR_BUFFER_EMPTY;

			unsigned int index = 0; // dummy index to pop first element
			while (ulNumMsgsRead < *pulNumMsgs && !m_stLogChannelList[i].RxQueue.empty())
			{
				if ((NULL != m_stLogChannelList[i].RxQueue[index]->ucData) &&
					(NULL != (pucMsgs + ulNumMsgsRead)->ucData))
				{
					(pucMsgs + ulNumMsgsRead)->ulProtocolID = m_stLogChannelList[i].RxQueue[index]->ulProtocolID;
					(pucMsgs + ulNumMsgsRead)->MsgHandle = m_stLogChannelList[i].RxQueue[index]->MsgHandle;
					(pucMsgs + ulNumMsgsRead)->ulDataBufferSize = m_stLogChannelList[i].RxQueue[index]->ulDataBufferSize;
					(pucMsgs + ulNumMsgsRead)->ulDataSize = m_stLogChannelList[i].RxQueue[index]->ulDataSize;
					(pucMsgs + ulNumMsgsRead)->ulExtraDataIndex = m_stLogChannelList[i].RxQueue[index]->ulExtraDataIndex;
					(pucMsgs + ulNumMsgsRead)->ulRxStatus = m_stLogChannelList[i].RxQueue[index]->ulRxStatus;
					(pucMsgs + ulNumMsgsRead)->ulTimeStamp = m_stLogChannelList[i].RxQueue[index]->ulTimeStamp;
					(pucMsgs + ulNumMsgsRead)->ulTxFlags = m_stLogChannelList[i].RxQueue[index]->ulTxFlags;
					
					memcpy((pucMsgs + ulNumMsgsRead)->ucData, m_stLogChannelList[i].RxQueue[index]->ucData,
						m_stLogChannelList[i].RxQueue[index]->ulDataSize);
					
					if (m_stLogChannelList[i].RxQueue[index]->ucData != NULL)
					{
						delete[] m_stLogChannelList[i].RxQueue[index]->ucData;
						m_stLogChannelList[i].RxQueue[index]->ucData = NULL;
					}
					m_stLogChannelList[i].RxQueue.erase(m_stLogChannelList[i].RxQueue.begin());
				}
				else
				{
					WriteLogMsg("Invalide Msg pointer ", DEBUGLOG_TYPE_COMMENT, "Start");

				}
				ulNumMsgsRead++;
			}
			*pulNumMsgs = ulNumMsgsRead;
			break;
		}
	}
	return enJ2534Error;
}

J2534ERROR CLogicalChannelList::GetRxCountOnLogicalChannel(unsigned long ulChannelId, unsigned long* pulNumMsgs)
{
	J2534ERROR      enJ2534Error = J2534_STATUS_NOERROR;
	int i;

	// Write to Log File.
	WriteLogMsg("GetRxCountOnLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "Start");

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			if (m_stLogChannelList[i].RxQueue.size() == 0)
				enJ2534Error = J2534_ERR_BUFFER_EMPTY;
			else if (m_stLogChannelList[i].RxQueue.size() == PASSTHRU_MSG_DATA_SIZE)
				enJ2534Error = J2534_ERR_BUFFER_FULL;
			else if (m_stLogChannelList[i].RxQueue.size() > 0)
				enJ2534Error = J2534_STATUS_NOERROR;
			break;
		}
	}
	*pulNumMsgs = m_stLogChannelList[i].RxQueue.size();
	// Write to Log File.
	WriteLogMsg("GetRxCountOnLogicalChannel()", DEBUGLOG_TYPE_COMMENT, "End");
	return enJ2534Error;
}

J2534ERROR CLogicalChannelList::AddMsgsToLogicalChannelQueue(PASSTHRU_MSG *pstMsg, bool bTxRx)
{
	J2534ERROR      enJ2534Error = J2534_ERR_INVALID_MSG;
	int i;
	if (bTxRx)
	{
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if ((memcmp(m_stLogChannelList[i].structChan.RemoteAddress, pstMsg->ucData, 4) == 0))
			{
				m_stLogChannelList[i].RxQueue.push_back(pstMsg);
				enJ2534Error = J2534_STATUS_NOERROR;
				break;
			}
		}
	}
	else
	{
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if ((memcmp(m_stLogChannelList[i].structChan.LocalAddress, pstMsg->ucData, 4) == 0))
			{
				m_stLogChannelList[i].TxQueue.push_back(pstMsg);
				enJ2534Error = J2534_STATUS_NOERROR;
				break;
			}
		}
	}
	return enJ2534Error;
}
J2534ERROR	CLogicalChannelList::ClearRxQueue(unsigned long ulChannelId)
{
	J2534ERROR      enumJ2534Error = J2534_STATUS_NOERROR;
	int i;

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			m_stLogChannelList[i].RxQueue.clear();
			break;
		}
	}
	return enumJ2534Error;
}

J2534ERROR CLogicalChannelList::vIoctl(unsigned long ulChannelId, J2534IOCTLID enumIoctlID, void *pInput, void *pOutput)
{
	J2534ERROR      enumJ2534Error = J2534_STATUS_NOERROR;
	unsigned long i;
	// Validate if the channel started earlier.
	if (!IsLogChannelValid(ulChannelId))
	{
		// Write to Log File.
		WriteLogMsg("vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
		return(J2534_ERR_INVALID_CHANNEL_ID);
	}
	switch (enumIoctlID)
	{
	case GET_CONFIG:            
		enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput, ulChannelId);
		break;

	case SET_CONFIG:            // Set configuration
		enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput, ulChannelId);
		break;

	case CLEAR_TX_BUFFER:
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
			{
				m_stLogChannelList[i].TxQueue.clear();
			}
			break;
		}
		break;

	case CLEAR_RX_BUFFER: 
		for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
		{
			if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
			{
				m_stLogChannelList[i].RxQueue.clear();
			}
			break;
		}
		break;

	case CLEAR_PERIODIC_MSGS: 
		break;

	case CLEAR_MSG_FILTERS:     
		break;
	default:                    // Others not supported
		enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
		break;
	}
	return enumJ2534Error;
}

J2534ERROR  CLogicalChannelList::GetConfig(SCONFIG_LIST *pInput, unsigned long ulChannelId)
{
	J2534ERROR      enumJ2534Error = J2534_STATUS_NOERROR;
	SCONFIG         *pSconfig;
	unsigned long   ulCount;
	int  i;

	// Make sure pInput is not NULL
	if (pInput == NULL)
		return(J2534_ERR_NULLPARAMETER);

	if (pInput->ulNumOfParams == 0)
	{
#ifdef UD_TRUCK
		WriteLogMsg("GetConfig", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
		SetLastErrorText("ISO15765 : Invalid NumOfParams value");
		return(J2534_ERR_FAILED);
#endif
	}
	else
	{
		pSconfig = pInput->pConfigPtr;
	}
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelId)
		{
			for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
			{
				switch (pSconfig->Parameter)
				{
				case LOOPBACK:          // Loopback
					//pSconfig->ulValue = m_bLoopback;
					break;
				case ISO15765_BS:       // Block Size
					pSconfig->ulValue = m_stLogChannelList[i].ulBlockSize;
					return J2534_STATUS_NOERROR;
					break;
				case BS_TX:             // Block Size Tx
					pSconfig->ulValue = m_stLogChannelList[i].ulBlockSize;
					return J2534_STATUS_NOERROR;
					break;
				case ISO15765_STMIN:        // STmin
					pSconfig->ulValue = m_stLogChannelList[i].ulSTmin;
					return J2534_STATUS_NOERROR;
					break;
				case STMIN_TX:              // STminTx
					pSconfig->ulValue = m_stLogChannelList[i].ulSTminTx;
					return J2534_STATUS_NOERROR;
					break;
				case ISO15765_WAIT_LIMIT:
					pSconfig->ulValue = m_stLogChannelList[i].ulWftMax;
					return J2534_ERR_NOT_SUPPORTED;
					break;
				case N_AS_MAX:
				case N_AR_MAX:
				case N_BS_MAX:
				case N_CR_MAX:
				case N_CS_MIN:
					return J2534_ERR_NOT_SUPPORTED;
					break;
				default:
					return(J2534_ERR_NOT_SUPPORTED);
				}

				if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
					return(enumJ2534Error);

				pSconfig++;
			}
			break;
		}
	}
	return(enumJ2534Error);
}

J2534ERROR  CLogicalChannelList::SetConfig(SCONFIG_LIST *pInput, unsigned long ulChannelId)
{
	J2534ERROR      enumJ2534Error;
	SCONFIG         *pSconfig;
	unsigned long   ulCount;
	enumJ2534Error = J2534_STATUS_NOERROR;

	// Make sure pInput is not NULL
	if (pInput == NULL)
		return(J2534_ERR_NULLPARAMETER);

	if (pInput->ulNumOfParams == 0)
	{
#ifdef UD_TRUCK
		WriteLogMsg("ISO15765_LOGICAL.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
		SetLastErrorText("ISO15765 : Invalid NumOfParams value");
		return(J2534_ERR_FAILED);
#endif
	}
	else
	{
		pSconfig = pInput->pConfigPtr;
	}

	for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
	{
		switch (pSconfig->Parameter)
		{
		case LOOPBACK:          // Loopback
				//enumJ2534Error = Loopback(pSconfig->ulValue);
			break;
		case ISO15765_BS:       // Block Size
			enumJ2534Error = BlockSize(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);
			if(enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case BS_TX:             // Block Size Tx
			enumJ2534Error = BlockSizeTx(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);
			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case ISO15765_STMIN:        // STmin
			enumJ2534Error = STmin(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);
			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}	
			break;
		case STMIN_TX:              // STminTx
			enumJ2534Error = STminTx(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);
			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case ISO15765_WAIT_LIMIT:
			enumJ2534Error = WftMax(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case N_AS_MAX:
			enumJ2534Error = NasMAX(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case N_AR_MAX:
			enumJ2534Error = NarMAX(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case N_BS_MAX:
			enumJ2534Error = NbsMAX(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case N_CR_MAX:
			enumJ2534Error = NcrMAX(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;
		case N_CS_MIN:
			enumJ2534Error = NcsMIN(pSconfig->ulValue, ulChannelId);
			if (enumJ2534Error != J2534_STATUS_NOERROR)
				return(enumJ2534Error);

			if (enumJ2534Error = m_pclsDevice->vIoctl(m_ulDevChannelID, SET_CONFIG, pSconfig, NULL))
			{
				// Write to Log File.
				WriteLogMsg("SetConfig()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", enumJ2534Error);
				return(enumJ2534Error);
			}
			break;			
		default:
			return(J2534_ERR_NOT_SUPPORTED);
			break;
		}

		if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
			return(enumJ2534Error);

		pSconfig++;
	}

	return(enumJ2534Error);
}
J2534ERROR  CLogicalChannelList::BlockSize(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulBlockSize = ulValue;
		break;
	}
	return(iReturn);
}

J2534ERROR  CLogicalChannelList::NasMAX(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulN_AS_MAX = ulValue;
		break;
	}

	return(iReturn);
}
J2534ERROR  CLogicalChannelList::NarMAX(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulN_AR_MAX = ulValue;
		break;
	}
	return(iReturn);
}
J2534ERROR  CLogicalChannelList::NbsMAX(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);

	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulN_BS_MAX = ulValue;
		break;
	}
	return(iReturn);
}
J2534ERROR  CLogicalChannelList::NcsMIN(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulN_CS_MIN = ulValue;
		break;
	}

	return(iReturn);
}
J2534ERROR  CLogicalChannelList::NcrMAX(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulN_CR_MAX = ulValue;
		break;
	}

	return(iReturn);
}

J2534ERROR  CLogicalChannelList::BlockSizeTx(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulBlockSizeTx = ulValue;
		break;
	}

	return(iReturn);
}

J2534ERROR  CLogicalChannelList::STmin(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulSTmin = ulValue;
		break;
	}

	return(iReturn);
}

J2534ERROR  CLogicalChannelList::STminTx(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if (((ulValue < 0) || (ulValue > 0xFF)) && (ulValue != 0xFFFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulSTminTx = ulValue;
		break;
	}

	return(iReturn);
}

J2534ERROR  CLogicalChannelList::WftMax(unsigned long ulValue, unsigned long ulChannelID)
{
	J2534ERROR iReturn = J2534_STATUS_NOERROR;
	int i;

	if ((ulValue < 0) || (ulValue > 0xFF))
		return(J2534_ERR_INVALID_IOCTL_VALUE);
#ifndef J2534_DEVICE_DPA5_EURO5
	if (ulValue != 0)
		return(J2534_ERR_NOT_SUPPORTED);
#endif
	for (i = 0; i < MAX_LOG_PER_PHYSICAL_ID; i++)
	{
		if (m_stLogChannelList[i].ulLogChannelId == ulChannelID)
			m_stLogChannelList[i].ulWftMax = ulValue;
		break;
	}
	return(iReturn);
}

void CLogicalChannelList::WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
	if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
	{
		char buf[1024];
		va_list ap;
		va_start(ap, chMsg);
		vsprintf_s(buf, sizeof(buf) - 1, chMsg, ap);
		va_end(ap);
		buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
		m_pclsLog->Write("LogicalChannelList.cpp", chFunctionName, MsgType, buf);
	}
}
#endif
