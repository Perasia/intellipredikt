/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DataLog.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of CDataLog class.
* Note:
*
*******************************************************************************/

#ifndef _DATALOG_H_
#define _DATALOG_H_

#define DATALOG_FILE_EXPORT            "J2534Export.cpp"
#define DATALOG_FILE_ISO15765          "ISO15765.cpp"
#define DATALOG_FILE_CAN               "Can.cpp"
#define DATALOG_FILE_J1850VPW          "J1850VPW.cpp"
#define DATALOG_FILE_J1850PWM          "J1850PWM.cpp"
#define DATALOG_FILE_ISO9141           "ISO9141.cpp"
#define DATALOG_FILE_ISO14230          "ISO14230.cpp"
#define DATALOG_FILE_DEVICESTORE       "DeviceStore.cpp"
#define DATALOG_FILE_BASE              "J2534Base.cpp"

// Data Logging Type
#define DATALOG_TYPE_ERROR             0x01
#define DATALOG_TYPE_COMMENT           0x02
#define DATALOG_TYPE_DATA              0x04
#define DATALOG_TYPE_APPEND            0x80000000
                       
// Rx/Tx
#define DATALOG_RX                     1
#define DATALOG_TX                     2

#define DATALOG_WRITELOG_TIMEOUT       1000

class CDataLog
{
    public:
        CDataLog();
        ~CDataLog();
//      bool Write(CString csFile, CString csFunc, unsigned long ulType, 
//                 CString csDesc);
        bool Write(char *csChannelPins, char *csProtocol, unsigned long ulRxTx, unsigned long ulTimestamp, char* csID, char *csData);
        bool Open(CString csLogFileName, unsigned long ulLogType, char *csDllFileName, unsigned long ulLoggingSizeMaxKB);
        bool Close();

        FILE            *m_pfdLogFile;          // Log File Descriptor.

    private:
        HANDLE          m_hWriteLogSync;        // Handle for WriteToLog synch.
        unsigned long   m_ulLoggingType;        // Type of Logging.
        CString         m_strLogFileName;       // Log File Name
        long            m_lLoggingSizeMaxKB;    // Maximum size of log file in KB
        void            TrimLogFile();

};

#endif
