/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: FilterMsg.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of the 
*              CFilterMsg class
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "FilterMsg.h"
CRITICAL_SECTION CFilterMsgSection;

//-----------------------------------------------------------------------------
//  Function Name   : CFilterMsg
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CFilterMsg class.
//-----------------------------------------------------------------------------
CFilterMsg::CFilterMsg(CDeviceBase *pclsDevice, unsigned long   ulDevChannelID, CDebugLog *pclsDebugLog)
{
    int i;
    InitializeCriticalSection(&CFilterMsgSection);      // Used in WriteLogMsg()

    // Save pointers.
    m_pclsLog = pclsDebugLog;
    m_pclsDevice = pclsDevice;
    m_ulDevChannelID = ulDevChannelID;

    // Write to Log File.
    WriteLogMsg("CFilterMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize the FilterType portion of the list.
    for (i = 0; i <= FILTERMSG_LIST_SIZE; i++)
    {
        m_stFilterList[i].enFilterType = J2534_FILTER_NONE;
        m_stFilterList[i].pstMask = NULL;
        m_stFilterList[i].pstPattern = NULL;
        m_stFilterList[i].pstFlowControl = NULL;
        m_stFilterList[i].ulDevFilterID = 0;
        m_stFilterList[i].ulDevFilterID = -1;
    }

    for (i = 0; i < FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE; i++)
    {
        m_stFilterListTP2_0[i].bPassive = false;
        memset(m_stFilterListTP2_0[i].ucCANID, 0, sizeof(m_stFilterListTP2_0[i].ucCANID));
        m_stFilterListTP2_0[i].ulFilterID = -1;
    }

    m_ulOldestFlowControlFilterID = 1;

    m_hSyncAccess = CreateEvent(NULL, FALSE, TRUE, NULL);
    m_hSyncTableAccess = CreateEvent(NULL, FALSE, TRUE, NULL);

    // Write to Log File.
    WriteLogMsg("CFilterMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CFilterMsg
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CFilterMsg class
//-----------------------------------------------------------------------------
CFilterMsg::~CFilterMsg()
{
    int i;

    // Write to Log File.
    WriteLogMsg("~CFilterMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop all Filters
    StopFilter();

    // Delete the resources.
    for (i = 0; i <= FILTERMSG_LIST_SIZE; i++)
    {
        if (m_stFilterList[i].pstMask != NULL)
        {   
            delete m_stFilterList[i].pstMask;
            m_stFilterList[i].pstMask = NULL;
        }

        if (m_stFilterList[i].pstPattern != NULL)
        {   
            delete m_stFilterList[i].pstPattern;
            m_stFilterList[i].pstPattern = NULL;
        }

        if (m_stFilterList[i].pstFlowControl != NULL)
        {   
            delete m_stFilterList[i].pstFlowControl;
            m_stFilterList[i].pstFlowControl = NULL;
        }

        m_stFilterList[i].enFilterType = J2534_FILTER_NONE;
        m_stFilterList[i].ulDevFilterID = -1;
    }

    CloseHandle(m_hSyncAccess);
    CloseHandle(m_hSyncTableAccess);

    // Write to Log File.
    WriteLogMsg("~CFilterMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : StartFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function starts a given msg. filter and returns a
//                    Msg. ID for reference.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::StartFilter(
                               J2534_FILTER     enFilterType, 
                               PASSTHRU_MSG     *pstMask, 
                               PASSTHRU_MSG     *pstPattern,
                               PASSTHRU_MSG     *pstFlowControl,
                               unsigned long    *pulFilterID,
                               bool             bTP2_0_Implicit,
                               bool             bTP2_0_Implicit_Passive)

{
    J2534ERROR      enJ2534Error;
    unsigned long   ulDevFilterID = 0;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality 
    int             i;
#endif

    // Synchronize Access to this function.
    if (WaitForSingleObject(m_hSyncAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(J2534_ERR_FAILED);   
    }

    // Write to Log File.
    WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check if NULL pointers.
    if ((pstPattern == NULL) || (pstMask == NULL))
    {
        // Write to Log File.
        WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
        SetEvent(m_hSyncAccess);
        return(J2534_ERR_NULLPARAMETER);
    }
    
    // If Flow Control Filter
    if (enFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        if (pstFlowControl == NULL)
        {
            // Write to Log File.
            WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NULLPARAMETER);
            SetEvent(m_hSyncAccess);
            return(J2534_ERR_NULLPARAMETER);
        }
    }
    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
#ifdef J2534_DEVICE_VSI
    if (pstPattern->ulProtocolID == ISO15765)
    {
        // HACK: This is to accommodate Chrysler application that sets about 20 ISO15765 flow control filters

        // Is number of flow control filter maxed out
        if (IsFlowControlListFull())
        {
            // Check to see if the Filter is Unique.
            if (!IsFlowControlUnique(pstPattern, pstFlowControl))
            {
                // Write to Log File.
                WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_UNIQUE);
                SetEvent(m_hSyncTableAccess);
                SetEvent(m_hSyncAccess);
                return(J2534_ERR_NOT_UNIQUE);
            }

            SetEvent(m_hSyncTableAccess);

            // If so, delete the oldest one before setting the newest one
            m_pclsDevice->vStopFilter(m_ulDevChannelID, m_stFilterList[m_ulOldestFlowControlFilterID].ulDevFilterID);

            WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
            // Delete the filter
            Delete(m_ulOldestFlowControlFilterID);
            m_ulOldestFlowControlFilterID += 1;
            if (m_ulOldestFlowControlFilterID == 11)
                m_ulOldestFlowControlFilterID = 1;
        }
    }
#endif
#endif
    // Is number of Filters exceeded the maximum.
    if (IsListFull())
    {
        // Write to Log File.
        WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
        SetEvent(m_hSyncTableAccess);
        SetEvent(m_hSyncAccess);
        return(J2534_ERR_EXCEEDED_LIMIT);
    }
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    if (bTP2_0_Implicit)
    {
        for (i = 0; i < FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE; i++)
        {
            if (m_stFilterListTP2_0[i].ulFilterID != -1)
            {
                if (m_stFilterListTP2_0[i].bPassive == false)
                {
                    if (memcmp(m_stFilterListTP2_0[i].ucCANID, pstPattern->ucData, 
                        sizeof(m_stFilterListTP2_0[i].ucCANID)) == 0)
                    {
                        // Write to Log File.
                        WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_UNIQUE);
                        SetEvent(m_hSyncTableAccess);
                        SetEvent(m_hSyncAccess);
                        return(J2534_ERR_NOT_UNIQUE);
                    }
                }
            }
        }
    }
#endif
    SetEvent(m_hSyncTableAccess);

    // If Flow Control Filter
    if ((enFilterType == J2534_FILTER_FLOW_CONTROL) && (pstFlowControl != NULL))
    {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        // Check if the Datasize and TxFlags are the same for Pattern,
        // Mask and FlowControl messages.
        if ((pstPattern->ulDataSize != FILTERMSG_ISO15765_MSGLEN && 
            (pstPattern->ulTxFlags & FILTERMSG_FLAG_ISO15765_EXT_ADDR) == 0) ||
            (pstPattern->ulDataSize != FILTERMSG_ISO15765_MSGLEN + 1 && 
            (pstPattern->ulTxFlags & FILTERMSG_FLAG_ISO15765_EXT_ADDR) == FILTERMSG_FLAG_ISO15765_EXT_ADDR) ||
            (pstFlowControl->ulDataSize != pstMask->ulDataSize) ||
            (pstFlowControl->ulDataSize != pstPattern->ulDataSize) ||
            (pstFlowControl->ulTxFlags != pstMask->ulTxFlags) ||
            (pstFlowControl->ulTxFlags != pstPattern->ulTxFlags))

        {
            // Write to Log File.
            WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            SetEvent(m_hSyncAccess);
            return(J2534_ERR_INVALID_MSG);
        }

        WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
        // Check to see if the Filter is Unique.
        if (!IsFlowControlUnique(pstPattern, pstFlowControl))
        {
            // Write to Log File.
            WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_UNIQUE);
            SetEvent(m_hSyncTableAccess);
            SetEvent(m_hSyncAccess);
            return(J2534_ERR_NOT_UNIQUE);
        }
        SetEvent(m_hSyncTableAccess);
#endif
    }
    else
    {
        // Check if the Datasize and TxFlags are the same for Pattern,
        // and Mask messages.
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        if ((pstPattern->ulDataSize > FILTERMSG_MSGLEN_MAX) || 
            (pstMask->ulDataSize != pstPattern->ulDataSize) ||
            (pstMask->ulTxFlags != pstPattern->ulTxFlags) ||
            (pstFlowControl != NULL))
#endif
#ifdef J2534_0305
        if (pstFlowControl != NULL)
#endif
        {
            // Write to Log File.
            WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            SetEvent(m_hSyncAccess);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    // Set Filter in Device as well if it is not a PASS or FLOW_CONTROL filter.
    if ((enFilterType == J2534_FILTER_PASS) || (enFilterType == J2534_FILTER_FLOW_CONTROL))
    {
#ifdef J2534_0305
        if ((enFilterType == J2534_FILTER_PASS) && (pstPattern->ulProtocolID == ISO15765))
        {
        }
        else
#endif
        if ((enJ2534Error = m_pclsDevice->vStartFilter(m_ulDevChannelID, enFilterType, pstMask, 
                                                     pstPattern, pstFlowControl, 
                                                     &ulDevFilterID)) 
                          != J2534_STATUS_NOERROR)
        {
            // Write to Log File.
            WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            // Return error only if it is a Flow Control filter.
            // If it is any other filter do not return an error as
            // some devices may not be capable of setting at all
            // or so many filters. Anyway in cases like that the
            // software filter will take care of it.
            if (enFilterType == J2534_FILTER_FLOW_CONTROL)
            {
                SetEvent(m_hSyncAccess);
                return(enJ2534Error);
            }
            // It failed to set filter in device so set the ID to 0.
            ulDevFilterID = 0;
            SetEvent(m_hSyncAccess);
            return(enJ2534Error);
        }
    }

    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
    // Add the filter info. to our internal Database.
    if ((enJ2534Error = Add(enFilterType, pstMask, pstPattern, 
                            pstFlowControl, ulDevFilterID, pulFilterID)) !=
                        J2534_STATUS_NOERROR)
    {
        SetEvent(m_hSyncTableAccess);
        // Stop the Device Filter.
        m_pclsDevice->vStopFilter(m_ulDevChannelID, ulDevFilterID);
        // Write to Log File.
        WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        SetEvent(m_hSyncAccess);
        return(enJ2534Error);
    }

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    if (bTP2_0_Implicit)
    {
        for (i = 0; i < FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE; i++)
        {
            if (m_stFilterListTP2_0[i].ulFilterID == -1)
            {
                m_stFilterListTP2_0[i].ulFilterID = *pulFilterID;
                memcpy(m_stFilterListTP2_0[i].ucCANID, pstPattern->ucData, sizeof(m_stFilterListTP2_0[i].ucCANID));
                m_stFilterListTP2_0[i].bPassive = bTP2_0_Implicit_Passive;
                break;
            }
        }
    }
#endif

    SetEvent(m_hSyncTableAccess);
    SetEvent(m_hSyncAccess);

    // Write to Log File.
    WriteLogMsg("StartFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : StopFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops the filter corresspond to a given ID.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::StopFilter(unsigned long ulFilterID,
                                    bool bTP2_0_Implicit,
                                    bool bTP2_0_Implicit_Passive)

{
    J2534ERROR      enJ2534Error;
    unsigned long   ulDevFilterID;
    J2534_FILTER    enFilterType;
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    int             i;
#endif

    // Write to Log File.
    WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(J2534_ERR_FAILED);   
    }

    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);

#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    if (!bTP2_0_Implicit)
    {
        WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_COMMENT, "Not checking TP2.0 implicit pass filter");
        for (i = 0; i < FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE; i++)
        {
            if (m_stFilterListTP2_0[i].ulFilterID == ulFilterID)
            {
                // Write to Log File.
                WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
                SetEvent(m_hSyncTableAccess);
                SetEvent(m_hSyncAccess);
                return(J2534_ERR_INVALID_FILTER_ID);
            }
        }
    }
#endif

    // Validate if the periodic msg. for this FilterID was started earlier.
    if (!IsFilterIDValid(ulFilterID))
    {
        // Write to Log File.
        WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetEvent(m_hSyncTableAccess);
        SetEvent(m_hSyncAccess);
        return(J2534_ERR_INVALID_FILTER_ID);
    }

    ulDevFilterID = m_stFilterList[ulFilterID].ulDevFilterID;
    enFilterType = m_stFilterList[ulFilterID].enFilterType;
    SetEvent(m_hSyncTableAccess);
    SetEvent(m_hSyncAccess);

    // Stop the filter in device if it were set earlier.
    if ((ulDevFilterID != -1) && (enFilterType != J2534_FILTER_BLOCK))
    {
#ifdef J2534_0305
        if ((enFilterType == J2534_FILTER_PASS) && 
            (m_stFilterList[ulFilterID].pstPattern->ulProtocolID == ISO15765))
        {
        }
        else
#endif
        if ((enJ2534Error = m_pclsDevice->vStopFilter(m_ulDevChannelID, m_stFilterList[ulFilterID].ulDevFilterID)) 
                          != J2534_STATUS_NOERROR)
        {
            // Write to Log File.
            WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
//          SetEvent(m_hSyncAccess);
            return(enJ2534Error);
        }
    }

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(J2534_ERR_FAILED);   
    }

    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
    // Delete the filter
    Delete(ulFilterID);

    SetEvent(m_hSyncTableAccess);
    SetEvent(m_hSyncAccess);

    // Write to Log File.
    WriteLogMsg("StopFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : StopFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This is an override function that stops all the filters.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::StopFilter()

{
    unsigned long   i;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(J2534_ERR_FAILED);   
    }

    // Write to Log File.
    WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_COMMENT, "Start");  
    
    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);

    // Run through all the Filters and delete all of them.
    for (i = 0; i <= FILTERMSG_LIST_SIZE; i++)
    {
        if ((m_stFilterList[i].ulDevFilterID != -1) && (m_stFilterList[i].enFilterType != J2534_FILTER_BLOCK))
        {
#ifdef J2534_0305
        if ((m_stFilterList[i].enFilterType == J2534_FILTER_PASS) && 
            (m_stFilterList[i].pstPattern->ulProtocolID == ISO15765))
        {
        }
        else
#endif  
        {
            SetEvent(m_hSyncTableAccess);
            m_pclsDevice->vStopFilter(m_ulDevChannelID, m_stFilterList[i].ulDevFilterID);
            WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
        }
        }
        // Delete the filter
        Delete(i);
    }

    SetEvent(m_hSyncTableAccess);
    SetEvent(m_hSyncAccess);

    // Write to Log File.
    WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}
//-----------------------------------------------------------------------------
//  Function Name   : StopPassAndBlockFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This is an override function that stops all the pass and block filters.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::StopPassAndBlockFilter()

{
    unsigned long   i;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(J2534_ERR_FAILED);   
    }

    // Write to Log File.
    WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_COMMENT, "Start");

    WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);

    // Run through all the Filters and delete all of them.
    for (i = 0; i <= FILTERMSG_LIST_SIZE; i++)
    {
        if ((m_stFilterList[i].ulDevFilterID != -1) && (m_stFilterList[i].enFilterType != J2534_FILTER_BLOCK))
        {
#ifdef J2534_0305
        if ((m_stFilterList[i].enFilterType == J2534_FILTER_PASS) && 
            (m_stFilterList[i].pstPattern->ulProtocolID == ISO15765))
        {
        }
        else
#endif  
			if (m_stFilterList[i].enFilterType == J2534_FILTER_PASS)
            {
                SetEvent(m_hSyncTableAccess);
				m_pclsDevice->vStopFilter(m_ulDevChannelID, m_stFilterList[i].ulDevFilterID);
                WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT);
            }
        }

        if (m_stFilterList[i].enFilterType == J2534_FILTER_PASS || 
			m_stFilterList[i].enFilterType == J2534_FILTER_BLOCK)
		{
			// Delete the filter
			Delete(i);
		}
    }

    SetEvent(m_hSyncTableAccess);
    SetEvent(m_hSyncAccess);

    // Write to Log File.
    WriteLogMsg("StopFilter(All)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}
//-----------------------------------------------------------------------------
//  Function Name   : Add
//  Input Params    : void
//  Output Params   : void
//  Description     : This function assigns a Msg. ID, adds message to 
//                    internal buffer and returns the FilterID to user.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::Add(J2534_FILTER enFilterType, 
                               PASSTHRU_MSG *pstMask, 
                               PASSTHRU_MSG *pstPattern,
                               PASSTHRU_MSG *pstFlowControl,
                               unsigned long ulDevFilterID,
                               unsigned long *pulFilterID)
{
    unsigned long   ulFilterID;

    // Get new FilterID.
    if ((ulFilterID = GetNewFilterID()) == 0)
    {
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
        return(J2534_ERR_EXCEEDED_LIMIT);   
    }

    // Save Mask filter in our internal buffer.
    if ((m_stFilterList[ulFilterID].pstMask = new FILTERMSG_MSG) == NULL)
    {
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Filter Mask");
        SetLastErrorText("FilterMsg : Could not create Object for Filter Mask");
        return(J2534_ERR_FAILED);   
    }
    memcpy(m_stFilterList[ulFilterID].pstMask, pstMask, 
           min(sizeof(FILTERMSG_MSG), sizeof(PASSTHRU_MSG)));
#ifdef J2534_0500
	m_stFilterList[ulFilterID].pstMask->ulDataSize = pstMask->ulDataSize;
#endif 

    // Save Pattern filter in our internal buffer.
    if ((m_stFilterList[ulFilterID].pstPattern = new FILTERMSG_MSG) == NULL)
    {
        delete m_stFilterList[ulFilterID].pstMask;
        m_stFilterList[ulFilterID].pstMask = NULL;
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Filter Pattern");
        SetLastErrorText("FilterMsg : Could not create Object for Filter Pattern");
        return(J2534_ERR_FAILED);   
    }
    memcpy(m_stFilterList[ulFilterID].pstPattern, pstPattern, 
           min(sizeof(FILTERMSG_MSG), sizeof(PASSTHRU_MSG)));
#ifdef J2534_0500
	m_stFilterList[ulFilterID].pstPattern->ulDataSize = pstPattern->ulDataSize;
#endif 
    // If the filter to be added is a FlowControl Filter.
    if (enFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Save FlowControl filter in our internal buffer.
        if ((m_stFilterList[ulFilterID].pstFlowControl = new FILTERMSG_MSG) == NULL)
        {
            delete m_stFilterList[ulFilterID].pstMask;
            m_stFilterList[ulFilterID].pstMask = NULL;
            delete m_stFilterList[ulFilterID].pstPattern;
            m_stFilterList[ulFilterID].pstPattern = NULL;
            // Write to Log File.
            WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Filter FlowControl");
            SetLastErrorText("FilterMsg : Could not create Object for Filter FlowControl");
            return(J2534_ERR_FAILED);   

        }
        memcpy(m_stFilterList[ulFilterID].pstFlowControl, pstFlowControl, 
               min(sizeof(FILTERMSG_MSG), sizeof(PASSTHRU_MSG)));
#ifdef J2534_0500
		m_stFilterList[ulFilterID].pstFlowControl->ulDataSize = pstFlowControl->ulDataSize;
#endif 
    }

    // Save Filter Type.
    m_stFilterList[ulFilterID].enFilterType = enFilterType;

    // Save Device Filter ID.
    m_stFilterList[ulFilterID].ulDevFilterID = ulDevFilterID;

    // Return the FilterID to user.
    *pulFilterID = ulFilterID;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : Delete
//  Input Params    : void
//  Output Params   : void
//  Description     : This function removes the filter from the Database that 
//                    was added earlier.
//-----------------------------------------------------------------------------
J2534ERROR CFilterMsg::Delete(unsigned long ulFilterID)
{
    int i;

    if (m_stFilterList[ulFilterID].pstMask != NULL)
    {
        delete m_stFilterList[ulFilterID].pstMask;
        m_stFilterList[ulFilterID].pstMask = NULL;
    }

    if (m_stFilterList[ulFilterID].pstPattern != NULL)
    {
        delete m_stFilterList[ulFilterID].pstPattern;
        m_stFilterList[ulFilterID].pstPattern = NULL;
    }

    if (m_stFilterList[ulFilterID].pstFlowControl != NULL)
    {
        delete m_stFilterList[ulFilterID].pstFlowControl;
        m_stFilterList[ulFilterID].pstFlowControl = NULL;
    }

    m_stFilterList[ulFilterID].enFilterType = J2534_FILTER_NONE;

    m_stFilterList[ulFilterID].ulDevFilterID = 0;

    for (i = 0; i < FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE; i++)
    {
        if (m_stFilterListTP2_0[i].ulFilterID == ulFilterID)
        {
            m_stFilterListTP2_0[i].ulFilterID = -1;
            break;
        }
    }

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : GetNewFilterID
//  Input Params    : void
//  Output Params   : void
//  Description     : If all FilterIDs are used, returns 0 otherwise returns new
//                    FilterID to be used.
//-----------------------------------------------------------------------------
unsigned char CFilterMsg::GetNewFilterID()
{
    unsigned char   ucIdx;

    for (ucIdx = 1; ucIdx <= FILTERMSG_LIST_SIZE; ucIdx++)
    {
        if (m_stFilterList[ucIdx].enFilterType == J2534_FILTER_NONE)
            return(ucIdx);
    }
    
    return(0);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsFilterIDValid
//  Input Params    : void
//  Output Params   : void
//  Description     : Checks to see if the given FilterID is valid.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsFilterIDValid(unsigned long ulFilterID)
{
    if (ulFilterID > 0 && ulFilterID <= FILTERMSG_LIST_SIZE)
    {
        if (m_stFilterList[ulFilterID].enFilterType != J2534_FILTER_NONE)
            return(true);
    }
    
    return(false);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsFlowControlListFull
//  Input Params    : void
//  Output Params   : void
//  Description     : Checks to see if the list to store Flow Control Filter info. is full.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsFlowControlListFull()
{
    unsigned long ucIdx;

    for (ucIdx = 1; ucIdx <= 10; ucIdx++)
    {
        if (m_stFilterList[ucIdx].enFilterType != J2534_FILTER_FLOW_CONTROL)
            return(false);
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsListFull
//  Input Params    : void
//  Output Params   : void
//  Description     : Checks to see if the list to store Filter info. is full.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsListFull()
{
    unsigned long ucIdx;

    for (ucIdx = 1; ucIdx <= FILTERMSG_LIST_SIZE; ucIdx++)
    {
        if (m_stFilterList[ucIdx].enFilterType == J2534_FILTER_NONE)
            return(false);
    }
    
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : DoesFlowControlFilterMatchRxFCID
//  Input Params    : void
//  Output Params   : void
//  Description     : This function checks if Message has a match with an existing
//                    flow control filter: 1. If the CAN_29BIT_ID match; 
//                    2. If the extended address bits match and 3. If the header
//                    bytes match (included extended address byte if set)
//                    This is to see in case of any erroneous CAN loopback message
//                    being treated like received message
//-----------------------------------------------------------------------------
bool    CFilterMsg::DoesFlowControlFilterMatchRxFCID(PASSTHRU_MSG *pstrucMsg)
{
    int i;
    bool bRequired = false, bExtendedAddress = false, bMatch = false;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("DoesFlowControlFilterMatchRxFCID()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(false);  
    }

    for (i = 1; i <= FILTERMSG_LIST_SIZE; i++)
    {   
        if(m_stFilterList[i].enFilterType == J2534_FILTER_FLOW_CONTROL)
        {
            // Check if the CAN_29BIT_ID of TxFlags match
            if (((pstrucMsg->ulRxStatus >> 8) & 1) == 
                ((m_stFilterList[i].pstFlowControl->ulTxFlags >> 8) & 1))           
                bRequired = true;
            else
                bRequired = false;
        
            // Check if extended address byte is used for that flow control ID
            if(bRequired)
            {
                if ((m_stFilterList[i].pstFlowControl->ulTxFlags >> 7) & 1)
                    bExtendedAddress = true;
                else
                    bExtendedAddress = false;
            
                if (bExtendedAddress) 
                {
                    //Check 5 bytes of hdr
                    if(pstrucMsg->ulDataSize >= 5 && memcmp(pstrucMsg->ucData,
                        m_stFilterList[i].pstFlowControl->ucData,5) == 0)
                    {
                        bMatch = true;
                        break;
                    }
                }
                else                
                {
                    //Check 4 bytes of hdr
                    if(pstrucMsg->ulDataSize >= 4 && memcmp(pstrucMsg->ucData,
                        m_stFilterList[i].pstFlowControl->ucData,4) == 0)
                    {
                        bMatch = true;
                        break;
                    }
                }
            }
        }
    }
    SetEvent(m_hSyncTableAccess);
    return(bMatch);
}

//-----------------------------------------------------------------------------
//  Function Name   : DoesFlowControlFilterMatch
//  Input Params    : void
//  Output Params   : void
//  Description     : This function checks if Message has a match with an existing
//                    flow control filter: 1. If the CAN_29BIT_ID match; 
//                    2. If the extended address bits match and 3. If the header
//                    bytes match (included extended address byte if set)
//-----------------------------------------------------------------------------
bool    CFilterMsg::DoesFlowControlFilterMatch(PASSTHRU_MSG *pstrucMsg)
{
    int i;
    bool bRequired = false, bExtendedAddress = false, bMatch = false;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("DoesFlowControlFilterMatch()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(false);  
    }

    for (i = 1; i <= FILTERMSG_LIST_SIZE; i++)
    {   
        if(m_stFilterList[i].enFilterType == J2534_FILTER_FLOW_CONTROL)
        {
            // Check if the CAN_29BIT_ID of TxFlags match
            if (((pstrucMsg->ulTxFlags >> 8) & 1) == 
                ((m_stFilterList[i].pstFlowControl->ulTxFlags >> 8) & 1))           
                bRequired = true;
            else
                bRequired = false;
        
            // Check if extended address bits match
            if(bRequired)
            {
                if (((pstrucMsg->ulTxFlags >> 7) & 1) == 
                    ((m_stFilterList[i].pstFlowControl->ulTxFlags >> 7) 
                    & 1))           
                    bRequired = true;
                else
                    bRequired = false;
            }

            if(bRequired)
            {
                if ((pstrucMsg->ulTxFlags >> 7) & 1)
                    bExtendedAddress = true;
                else
                    bExtendedAddress = false;
            
                    if (bExtendedAddress) 
                    {
                        //Check 5 bytes of hdr
                        if(memcmp(pstrucMsg->ucData,
                            m_stFilterList[i].pstFlowControl->ucData,5) == 0)
                        {
                            bMatch = true;
                            break;
                        }
                    }
                    else                
                    {
                        //Check 4 bytes of hdr
                        if(memcmp(pstrucMsg->ucData,
                            m_stFilterList[i].pstFlowControl->ucData,4) == 0)
                        {
                            bMatch = true;
                            break;
                        }
                    }
            }
        }
    }
    SetEvent(m_hSyncTableAccess);
    return(bMatch);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsFlowControlUnique
//  Input Params    : void
//  Output Params   : void
//  Description     : This functions iterates through all the current Filters
//                    and checks to see if the Flow Control ID is unique. If 
//                    it is unique, it returns true otherwise returns false.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsFlowControlUnique(PASSTHRU_MSG *pstPattern,
                                     PASSTHRU_MSG *pstFlowControl)
{
    bool                bUnique = true;
    unsigned long       i, j;

    // Run through all the Filters to see if the requested FLOW_CONTROL_FILTER is unique.
    for (i = 1; i <= FILTERMSG_LIST_SIZE; i++)
    {
        if (m_stFilterList[i].enFilterType == J2534_FILTER_FLOW_CONTROL)
        {
            if ((pstFlowControl->ulTxFlags & FILTERMSG_FLAG_29_BIT_CANID) ==
			    (m_stFilterList[i].pstFlowControl->ulTxFlags & FILTERMSG_FLAG_29_BIT_CANID))
            {
                //if (m_stFilterList[i].pstFlowControl->ulDataSize == pstFlowControl->ulDataSize)
                //{
                    // Check all the Header bytes of given FlowControl Msg. to see 
                    // if they match with the Filter already in Database.
                    for (j = 0; j < m_stFilterList[i].pstFlowControl->ulDataSize && 
                        j < pstFlowControl->ulDataSize; j++)
                    {
                        if (m_stFilterList[i].pstFlowControl->ucData[j] == 
                            pstFlowControl->ucData[j])
                        {
                            if (bUnique)
                                bUnique = false;
                        }
                        else
                        {
                            bUnique = true;
                            break;
                        }
                    }
                    if (!bUnique)
                        return(false);
                //}

                    for (j = 0; j < m_stFilterList[i].pstFlowControl->ulDataSize && 
                        j < pstPattern->ulDataSize; j++)
                    {
                        if (m_stFilterList[i].pstFlowControl->ucData[j] == 
                            pstPattern->ucData[j])
                        {
                            if (bUnique)
                                bUnique = false;
                        }
                        else
                        {
                            bUnique = true;
                            break;
                        }
                    }
                    if (!bUnique)
                        return(false);
                    
                //if (m_stFilterList[i].pstPattern->ulDataSize == pstPattern->ulDataSize)
                //{
                    // Check all the Header bytes of given Pattern Msg. to see 
                    // if they match with the Filter already in Database.
                    for (j = 0; j < m_stFilterList[i].pstPattern->ulDataSize && 
                        j < pstPattern->ulDataSize; j++)
                    {
                        if (m_stFilterList[i].pstPattern->ucData[j] == pstPattern->ucData[j])
                        {
                            if (bUnique)
                                bUnique = false;
                        }
                        else
                        {
                            bUnique = true;
                            break;
                        }
                    }
                    if (!bUnique)
                        return(false);
                //}

                    for (j = 0; j < m_stFilterList[i].pstPattern->ulDataSize && 
                        j < pstFlowControl->ulDataSize; j++)
                    {
                        if (m_stFilterList[i].pstPattern->ucData[j] == pstFlowControl->ucData[j])
                        {
                            if (bUnique)
                                bUnique = false;
                        }
                        else
                        {
                            bUnique = true;
                            break;
                        }
                    }
                    if (!bUnique)
                        return(false);
            }
        }
    }
        
    return(bUnique);
}

//-----------------------------------------------------------------------------
//  Function Name   : MsgConform
//  Input Params    : void
//  Output Params   : void
//  Description     : Run Filters on a given msg. and return whether the msg. 
//                    conforms to the given Filter type or not.
//-----------------------------------------------------------------------------
FILTERMSGCONFORM CFilterMsg::MsgConform(PASSTHRU_MSG *pstMsg, 
                                        J2534_FILTER enFilterType)
{
    FILTERMSGCONFORM    enFilterConform = FILTERMSG_CONFORM_NO;
    unsigned long       i, j;

    // Run through all the Filters
    for (i = 1; i <= FILTERMSG_LIST_SIZE; i++)
    {
        if (m_stFilterList[i].enFilterType == enFilterType)
        {
			// Check to see if CAN ID type of receive message matches 
			// the CAN ID type of the set filter
			if ((pstMsg->ulProtocolID == CAN) ||
				(pstMsg->ulProtocolID == CAN_PS) ||
				(pstMsg->ulProtocolID == CAN_CH1) ||
				(pstMsg->ulProtocolID == CAN_CH2) ||
				(pstMsg->ulProtocolID == CAN_CH3) ||
				(pstMsg->ulProtocolID == CAN_CH4) ||
				(pstMsg->ulProtocolID == ISO15765) ||
				(pstMsg->ulProtocolID == ISO15765_PS) ||
				(pstMsg->ulProtocolID == ISO15765_CH1) ||
				(pstMsg->ulProtocolID == ISO15765_CH2) ||
				(pstMsg->ulProtocolID == ISO15765_CH3) ||
				(pstMsg->ulProtocolID == ISO15765_CH4) ||
				(pstMsg->ulProtocolID == SW_CAN_PS) ||
				(pstMsg->ulProtocolID == SW_ISO15765_PS) ||
				(pstMsg->ulProtocolID == SW_CAN_CAN_CH1) ||
				(pstMsg->ulProtocolID == SW_CAN_ISO15765_CH1) ||
				(pstMsg->ulProtocolID == FT_CAN_PS) ||
				(pstMsg->ulProtocolID == FT_ISO15765_PS) ||
				(pstMsg->ulProtocolID == FT_CAN_CH1) ||
				(pstMsg->ulProtocolID == FT_ISO15765_CH1) ||
				(pstMsg->ulProtocolID == J1939_PS) ||
				(pstMsg->ulProtocolID == J1939_CH1) ||
				(pstMsg->ulProtocolID == J1939_CH2) ||
				(pstMsg->ulProtocolID == J1939_CH3) ||
				(pstMsg->ulProtocolID == J1939_CH4) ||
				(pstMsg->ulProtocolID == TP2_0_PS) ||
				(pstMsg->ulProtocolID == TP2_0_CH1) ||
				(pstMsg->ulProtocolID == TP2_0_CH2) ||
				(pstMsg->ulProtocolID == TP2_0_CH3) ||
				(pstMsg->ulProtocolID == TP2_0_CH4))
			{
				if ((m_stFilterList[i].pstPattern->ulTxFlags & FILTERMSG_FLAG_29_BIT_CANID) !=
					(pstMsg->ulRxStatus & FILTERMSG_FLAG_29_BIT_CANID))
				{
					enFilterConform = FILTERMSG_CONFORM_NO;
					continue;
				}
			}
            if (m_stFilterList[i].pstPattern->ulDataSize <= pstMsg->ulDataSize)
            {
                for (j = 0; j < m_stFilterList[i].pstPattern->ulDataSize; j++)
                {
                    if ((m_stFilterList[i].pstMask->ucData[j] & pstMsg->ucData[j]) ==
                        (m_stFilterList[i].pstMask->ucData[j] & m_stFilterList[i].pstPattern->ucData[j]))
                    {
                        if (enFilterConform != FILTERMSG_CONFORM_YES)
                            enFilterConform = FILTERMSG_CONFORM_YES;
                    }
                    else
                    {
                        enFilterConform = FILTERMSG_CONFORM_NO;
                        break;
                    }
                }
                if (enFilterConform == FILTERMSG_CONFORM_YES)
                    return(FILTERMSG_CONFORM_YES);
            }
        }
    }
        
    return(enFilterConform);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgRequired
//  Input Params    : void
//  Output Params   : void
//  Description     : This function applies all the currently set filters for 
//                    and determines if the msg. is required. If returned true
//                    msg. is required (passed) otherwise not.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsMsgRequired(PASSTHRU_MSG             *pstMsg,
                               FILTERMSG_CONFORM_REQ    *pConformReq)
{
    FILTERMSGCONFORM    enConformPass = FILTERMSG_CONFORM_NO;
    FILTERMSGCONFORM    enConformBlock = FILTERMSG_CONFORM_NO;
    FILTERMSGCONFORM    enConformFlowControl = FILTERMSG_CONFORM_NO;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("IsMsgRequired()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(false);  
    }

    // Find out if the msg. conforms to all current Pass Filters if requested.
    if (pConformReq->bReqPass)
        enConformPass = MsgConform(pstMsg, J2534_FILTER_PASS);

    // Find out if the msg. conforms to all current Block Filter if requrested.
    if (pConformReq->bReqBlock)
        enConformBlock = MsgConform(pstMsg, J2534_FILTER_BLOCK);

    // Find out if the msg. conforms to all current Block Filter if requested.
    if (pConformReq->bReqFlowControl)
        enConformFlowControl = MsgConform(pstMsg, J2534_FILTER_FLOW_CONTROL);

    // Check the results of PASS. BLOCK and FLOW_CONTROL filters to decide
    // as per J2534 standard whether the message is required or not.
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
    if (((enConformPass == FILTERMSG_CONFORM_YES) ||
         (enConformFlowControl == FILTERMSG_CONFORM_YES)) &&
        (enConformBlock != FILTERMSG_CONFORM_YES))
    {
        SetEvent(m_hSyncTableAccess);
        return(true);
    }
#endif
#ifdef J2534_0305
    if (IsPassFilterSet())
    {
        if (enConformPass == FILTERMSG_CONFORM_YES)
        {
            SetEvent(m_hSyncTableAccess);
            return(true);
        }
        else
        {
            SetEvent(m_hSyncTableAccess);
            return(false);
        }
    }
/*
    if (IsFlowControlFilterSet())
    {
        if (enConformPass == FILTERMSG_CONFORM_YES)
        {
            SetEvent(m_hSyncAccess);
            return(true);
        }
        else
        {
            SetEvent(m_hSyncAccess);
            return(false);
        }
    }
*/
    if (IsBlockFilterSet())
    {
        if (enConformBlock == FILTERMSG_CONFORM_YES)
        {
            SetEvent(m_hSyncTableAccess);
            return(false);
        }
        else
        {
            SetEvent(m_hSyncTableAccess);
            return(true);
        }
    }

    if ((enConformPass != FILTERMSG_CONFORM_YES) &&
        (enConformBlock != FILTERMSG_CONFORM_YES))
    {
        SetEvent(m_hSyncTableAccess);
        return(true);
    }
/*  
    if (((enConformPass != FILTERMSG_CONFORM_YES) ||
         (enConformFlowControl != FILTERMSG_CONFORM_YES)) &&
        (enConformBlock != FILTERMSG_CONFORM_YES))
    {
        SetEvent(m_hSyncAccess);
        return(true);
    }*/
#endif

    SetEvent(m_hSyncTableAccess);
    return(false);
}
//-----------------------------------------------------------------------------
//  Function Name   : IsFlowControlFilterSet
//  Input Params    : void
//  Output Params   : void
//  Description     : Checks to see if any flow control filter being set.
//-----------------------------------------------------------------------------
bool CFilterMsg::IsFlowControlFilterSet()
{
    unsigned long ulFilterID;

    // Synchronize Access this function.
    if (WaitForSingleObject(m_hSyncTableAccess, FILTERMSG_SYNC_TIMEOUT) 
                            != WAIT_OBJECT_0)
    {
        // Write to Log File.
        WriteLogMsg("IsFlowControlFilterSet()", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
        SetLastErrorText("FilterMsg : Failed Synchronization");
        return(false);  
    }

    for (ulFilterID = 1; ulFilterID <= FILTERMSG_LIST_SIZE; ulFilterID++)
    {
        if (m_stFilterList[ulFilterID].enFilterType == J2534_FILTER_FLOW_CONTROL)
        {
            SetEvent(m_hSyncTableAccess);
            return(true);
        }
    }

    SetEvent(m_hSyncTableAccess);
    return(false);
}

bool CFilterMsg::IsPassFilterSet()
{
    unsigned long ulFilterID;

    for (ulFilterID = 1; ulFilterID <= FILTERMSG_LIST_SIZE; ulFilterID++)
    {
        if (m_stFilterList[ulFilterID].enFilterType == J2534_FILTER_PASS)
            return(true);
    }
    
    return(false);
}

bool CFilterMsg::IsBlockFilterSet()
{
    unsigned long ulFilterID;

    for (ulFilterID = 1; ulFilterID <= FILTERMSG_LIST_SIZE; ulFilterID++)
    {
        if (m_stFilterList[ulFilterID].enFilterType == J2534_FILTER_BLOCK)
            return(true);
    }
    
    return(false);
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CFilterMsg::WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
    if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
    {
        EnterCriticalSection(&CFilterMsgSection); // Make processing parameters thread-safe
        char buf[1024];
        va_list ap;
        va_start(ap, chMsg);
        vsprintf_s(buf, sizeof(buf)-1, chMsg, ap);
        va_end(ap);
        buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
        m_pclsLog->Write("FilterMsg.cpp", chFunctionName, MsgType, buf);
        LeaveCriticalSection(&CFilterMsgSection);
    }
}
