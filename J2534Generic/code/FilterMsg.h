/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: FilterMsg.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains declaration of the CFilterMsg 
*              class.
* Note:
*
*******************************************************************************/


#ifndef _FILTERMSG_H_
#define _FILTERMSG_H_



#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

#ifdef J2534_0404
#include "J2534.h"
#endif

#ifdef J2534_0305
#include "J2534_0305.h"
#endif

#include "DebugLog.h"

#include "..\..\DeviceCayman\code\DeviceCayman.h"



#define FILTERMSG_ERROR_TEXT_SIZE		120

#define FILTERMSG_LIST_SIZE				20	// Maximum # of Filters in the list.
#define FILTERMSG_MSGLEN_MAX			12
#define FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE 4

#define FILTERMSG_ISO15765_MSGLEN		4	
#define FILTERMSG_FLAG_29_BIT_CANID		0x100	
#define FILTERMSG_FLAG_ISO15765_EXT_ADDR		0x80	

#define FILTERMSG_SYNC_TIMEOUT			5000

typedef enum
{
	FILTERMSG_CONFORM_YES = 1,			// Msg. conforms to given Filter.
	FILTERMSG_CONFORM_NO,				// Msg. does not conform to Filter.
} 
FILTERMSGCONFORM;
#ifndef J2534_0500
typedef struct
{
	unsigned long ulProtocolID;
	unsigned long ulRxStatus;
	unsigned long ulTxFlags;
	unsigned long ulTimeStamp;
	unsigned long ulDataSize;
	unsigned long ulExtraDataIndex;
	unsigned char ucData[FILTERMSG_MSGLEN_MAX];
}
FILTERMSG_MSG;
#endif
#ifdef J2534_0500
typedef struct
{
	unsigned long ulProtocolID;
	unsigned long MsgHandle;
	unsigned long ulRxStatus;
	unsigned long ulTxFlags;
	unsigned long ulTimeStamp;
	unsigned long ulDataSize;
	unsigned long ulExtraDataIndex;
	unsigned char *ucData;
	unsigned long ulDataBufferSize;
} 
FILTERMSG_MSG;
#endif

typedef struct
{
	bool	bReqPass;
	bool	bReqBlock;
	bool	bReqFlowControl;
}
FILTERMSG_CONFORM_REQ;

typedef struct
{
	J2534_FILTER  enFilterType;
	FILTERMSG_MSG *pstMask;
	FILTERMSG_MSG *pstPattern;
	FILTERMSG_MSG *pstFlowControl;
	unsigned long ulDevFilterID;
}
FILTERMSGLIST;

typedef struct
{
    unsigned char ucCANID[4];
	unsigned long ulFilterID;
    bool bPassive;
}
FILTERMSGLIST_TP2_0_IMPLICIT_PASS;

// CPeriodicMsg Class
class CFilterMsg 
{
public:
	// Constructor
	CFilterMsg(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID,
			   CDebugLog * pclsDebugLog = NULL);
	~CFilterMsg();

    FILTERMSGLIST_TP2_0_IMPLICIT_PASS m_stFilterListTP2_0[FILTERMSG_TP2_0_IMPLICIT_PASS_LIST_SIZE]; 

	J2534ERROR			StartFilter(
							J2534_FILTER	enFilterType, 
							PASSTHRU_MSG	*pstMask, 
							PASSTHRU_MSG	*pstPattern,
							PASSTHRU_MSG	*pstFlowControl,
							unsigned long	*pulFilterID,
                            bool            bTP2_0_Implicit = false,
                            bool            bTP2_0_Implicit_Passive = false);
	J2534ERROR			StopFilter(
							unsigned long	ulFilterID,
                            bool            bTP2_0_Implicit = false,
                            bool            bTP2_0_Implicit_Passive = false);
	J2534ERROR			StopFilter();
	J2534ERROR			StopPassAndBlockFilter();

	bool				IsMsgRequired(
							PASSTHRU_MSG			*pstMsg,
							FILTERMSG_CONFORM_REQ	*pConformReq);
	FILTERMSGLIST		m_stFilterList[FILTERMSG_LIST_SIZE+1];
	bool				IsPassFilterSet();
	bool				IsBlockFilterSet();
	bool				IsFlowControlFilterSet();
	bool				DoesFlowControlFilterMatch(PASSTHRU_MSG *pstrucMsg);
	bool				DoesFlowControlFilterMatchRxFCID(PASSTHRU_MSG *pstrucMsg);
    void                WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);

private:

	HANDLE				m_hSyncAccess;
	HANDLE				m_hSyncTableAccess;
	CDebugLog			*m_pclsLog;
	CDeviceBase			*m_pclsDevice;
	unsigned long		m_ulDevChannelID;
	char				m_szLastErrorText[FILTERMSG_ERROR_TEXT_SIZE];

	J2534ERROR			Add(J2534_FILTER enFilterType, 
							PASSTHRU_MSG *pstMask, 
							PASSTHRU_MSG *pstPattern,
							PASSTHRU_MSG *pstFlowControl,
							unsigned long ulDevFilterID,
						    unsigned long *pulFilterID);
	J2534ERROR			Delete(unsigned long ulFilterID);
	bool				IsFlowControlUnique(
							PASSTHRU_MSG *pstPattern,
							PASSTHRU_MSG *pstFlowControl);
	bool				IsFilterIDValid(unsigned long ulFilterID);
	bool				IsListFull();
	bool				IsFlowControlListFull();
    unsigned long       m_ulOldestFlowControlFilterID;
	unsigned char		GetNewFilterID();
	FILTERMSGCONFORM	MsgConform(
								PASSTHRU_MSG *pstMsg, 
								J2534_FILTER enFilterType);	
};

#endif