/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534Main.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This is the header file for J2534Main.cpp for 
*              internal use and of no relevence to outside 
*              application.
* Note:
*
*******************************************************************************/

#ifndef _J2534MAIN_H_
#define _J2534MAIN_H_

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif
#include "ProtocolManager.h"
#include "ProtocolBase.h"

// Create an appropriate Device Object.
#include "..\..\DeviceCayman\code\DeviceCayman.h"

//****************************** IMPORTANT NOTE *******************************

// Please use this file instead of J2534.h for internal use in J2534Main.cpp
// that is not relevent to outside application. J2534.h is meant to be for 
// users to include in their application and so it should have only what is 
// relevant to them.
//*****************************************************************************

#define J2534MAIN_ERROR_TEXT_SIZE		120

// Set the correct Device ID.
#define J2534_DEVICE_ID					701

#ifdef J2534_DEVICE_DBRIDGE
#define J2534_DATA_STORAGE_SIZE            10 
#define J2534_REPEAT_MSG_TIME_INTERVAL_MIN 5
#define J2534_REPEAT_MSG_TIME_INTERVAL_MAX 65535 
#define J2534_REPEAT_MSG_CONDITION_0       0 
#define J2534_REPEAT_MSG_CONDITION_1       1 
#define J2534_REPEAT_MSG_MAX_SIZE          12 
#endif

#define J2534_DATARECORDERFILENAME   "FileName"
#define J2534_DATARECORDERMAXSIZE    "MaxKB"
#define J2534_DATARECORDERAPPEND     "Append"

// Local functions that are for internal use.
void SetLastErrorText(char *pszErrorText, ...);
void SetLastErrorTextID(J2534ERROR	enJ2534Error);
void GetLastErrorText(char *pszBuffer, unsigned long ulSizeOfBuffer);
extern CRITICAL_SECTION CJ2534SetLastErrorTextSection;
#endif