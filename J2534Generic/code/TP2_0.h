/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: TP2_0.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              TP2_0 Class.
* Note:
*
*******************************************************************************/

#ifndef _TP2_0_H_
#define _TP2_0_H_

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define TP2_0_ERROR_TEXT_SIZE     120

#define TP2_0_MSG_SIZE_MIN        4
#define TP2_0_MSG_SIZE_SHORT_MAX  12
#define TP2_0_MSG_SIZE_MAX        4096

#define TP2_0_DATA_RATE_DEFAULT   500000
#define TP2_0_DATA_RATE_MEDIUM    250000

#define TP2_0_HEADER_SIZE                 11
#define TP2_0_EXTENDED_HEADER_SIZE        29

#define TP2_0_T_BR_INT_DEFAULT      20
#define TP2_0_T_E_DEFAULT           100
#define TP2_0_MNTC_DEFAULT          10
#define TP2_0_T_CTA_DEFAULT         1000
#define TP2_0_MNCT_DEFAULT          5
#define TP2_0_MNTB_DEFAULT          5
#define TP2_0_MNT_DEFAULT           2
#define TP2_0_T_WAIT_DEFAULT        100
#define TP2_0_T1_DEFAULT            100
#define TP2_0_T3_DEFAULT            0
#define TP2_0_IDENTIFIER_DEFAULT    0
#define TP2_0_RXIDPASSIVE_DEFAULT   0

#define TP2_0_IMPLICIT_PASS_FILTER_MAX  4

typedef struct
{
    unsigned char ucRxCANID[4];
    unsigned char ucTxCANID[4];
	unsigned long ulFilterID;
    bool bActive;
}
TP2_0_IMPLICIT_PASS_FILTER;

void OnTP2_0RxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CTP2_0 Class
class CTP2_0 : public CProtocolBase
{
public:
    CTP2_0(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
    ~CTP2_0();

    virtual J2534ERROR  vConnect(
                                J2534_PROTOCOL enProtocolID,
                                unsigned long ulFlags,
                                unsigned long ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
                                LPVOID        pVoid=NULL);
#ifdef J2534_0500
	virtual J2534ERROR					vLogicalConnect(J2534_PROTOCOL  enProtocolID);
#endif
    virtual J2534ERROR  vDisconnect();
    virtual J2534ERROR  vReadMsgs(
                                PASSTHRU_MSG  *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vWriteMsgs(
                                PASSTHRU_MSG *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vStartPeriodicMsg(
                                PASSTHRU_MSG *pstrucJ2534Msg,
                                unsigned long *pulMsgID,
                                unsigned long ulTimeInterval);
    virtual J2534ERROR  vStopPeriodicMsg(unsigned long ulMsgID);

    virtual J2534ERROR  vStartMsgFilter(
                                J2534_FILTER enFilterType,
                                PASSTHRU_MSG *pstrucJ2534Mask,
                                PASSTHRU_MSG *pstrucJ2534Pattern,
                                PASSTHRU_MSG *pstrucJ2534FlowControl,
                                unsigned long *pulFilterID);
    virtual J2534ERROR  vStopMsgFilter(unsigned long ulFilterID);

    virtual J2534ERROR  vIoctl(J2534IOCTLID enumIoctlID,
                                void *pInput,
                                void *pOutput);

    bool                IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
    bool                IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);


public:
    J2534ERROR              GetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              SetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              DataRate(unsigned long ulValue);
    J2534ERROR              Loopback(unsigned long ulValue);
    J2534ERROR				SetInternalTermination(unsigned char *pucValue);
	J2534ERROR		        J1962Pins(unsigned long ulValue);
	J2534ERROR		        J1939Pins(unsigned long ulValue);
    J2534ERROR				T_BR_INT(unsigned long ulValue),
							T_E(unsigned long ulValue),
							MNTC(unsigned long ulValue),
							T_CTA(unsigned long ulValue),
							MNCT(unsigned long ulValue),
							MNTB(unsigned long ulValue),
							MNT(unsigned long ulValue),
							T_WAIT(unsigned long ulValue),
							T1(unsigned long ulValue),
							T3(unsigned long ulValue),
							IDENTIFIER(unsigned long ulValue),
							RXIDPASSIVE(unsigned long ulValue);

    J2534ERROR              RequestConnection(SBYTE_ARRAY *pInput);
    J2534ERROR              TeardownConnection(SBYTE_ARRAY *pInput);

    float                   m_fSamplePoint;
    float                   m_fJumpWidth;
    unsigned long           m_ulTP2_0IDtype;
    J2534_PROTOCOL          m_enTP2_0Protocol;    
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;                                  
	unsigned long			m_ulJ1939PPSS;
	bool					m_bJ1939Pins;

    unsigned long			m_ulTP2_0_T_BR_INT,
							m_ulTP2_0_T_E, 
							m_ulTP2_0_MNTC,
							m_ulTP2_0_T_CTA,
							m_ulTP2_0_MNCT,
							m_ulTP2_0_MNTB, 
							m_ulTP2_0_MNT,
							m_ulTP2_0_T_WAIT,
							m_ulTP2_0_T1,
							m_ulTP2_0_T3,
							m_ulTP2_0_IDENTIFIER, 
							m_ulTP2_0_RXIDPASSIVE;

    TP2_0_IMPLICIT_PASS_FILTER m_stImplicitPassFilter[TP2_0_IMPLICIT_PASS_FILTER_MAX];

};

//#ifdef _TP2_0_
//J2534_PROTOCOL m_enTP2_0Protocol;
//#else
//extern J2534_PROTOCOL m_enTP2_0Protocol;
//#endif

#endif
