//*****************************************************************************
//					Dearborn Group Technology
//*****************************************************************************
// Project Name			: J2534 Generic
// File Name			: RepeatMsg.cpp
// Description			: This file contains the definition of the 
//					      CRepeatMsg class
// Date					: April 22, 2005
// Version				: 1.0
// Author				: Sanjay Mehta
// Revision				: 
//*****************************************************************************

//Includes
#include "StdAfx.h"
#include "RepeatMsg.h"
CRITICAL_SECTION CRepeatMsgSection;

//-----------------------------------------------------------------------------
//  Function Name: StopPeriodicThread
//  Input Params    : 
//  Output Params   : 
//  Description     : This thread 
//-----------------------------------------------------------------------------
UINT StopPeriodicThread(LPVOID lpVoid)
{
    CRepeatMsg * pCRepeatMsg = (CRepeatMsg *)lpVoid;
    pCRepeatMsg->WriteLogMsg("StopPeriodicThread", DEBUGLOG_TYPE_COMMENT, "Start");
    unsigned long i = 0;
    // Set this event to non-signal to indicate that the thread is running.
    ResetEvent(pCRepeatMsg->m_hThreadExited);
    // Set this event to non-signal. When signaled, it will exit the thread.
    ResetEvent(pCRepeatMsg->m_hThreadExit);    

    while ( WaitForSingleObject(pCRepeatMsg->m_hThreadExit, 0) != WAIT_OBJECT_0)
    {
        // Synchronize Access to the Repeat list
	    if(WaitForSingleObject(pCRepeatMsg->m_hSyncAccess,REPEATMSG_SYNC_TIMEOUT) == WAIT_OBJECT_0)
        {
            // Check only if there are any transmissions in progress
            if(pCRepeatMsg->m_iProgressingRepeatMsgs > 0)
            {
	            for (i = 1; i <= REPEATMSG_LIST_SIZE; i++)
	            {
                    // The thread is commanded to stop the transmission
                    // (Keep the RepatID - a call to IOCTL STOP_REPEAT_MESSAGE is always required) 
                    if(pCRepeatMsg->m_stRepeatList[i].bStopPeriodic)
                    {
                        if (pCRepeatMsg->m_pclsPeriodicMsg->StopPeriodic(pCRepeatMsg->m_stRepeatList[i].ulPeriodicID)!= J2534_STATUS_NOERROR)
                        {
                            // Log this and continue
                            pCRepeatMsg->WriteLogMsg("StopPeriodicThread", DEBUGLOG_TYPE_ERROR, "Failed to stop periodic");
                        }
                        else
                        {
                            // Update Command
                            pCRepeatMsg->m_stRepeatList[i].bStopPeriodic = false;
                             
                            // Update Status
                            pCRepeatMsg->m_stRepeatList[i].ulStatus = TRANSMISSION_CEASED;
                            pCRepeatMsg->m_stRepeatList[i].ulPeriodicID = 0;
                                    
                            // Decrease the Number of transmissions in progress
                            pCRepeatMsg->m_iProgressingRepeatMsgs--;
                            
                            pCRepeatMsg->WriteLogMsg("StopPeriodicThread", DEBUGLOG_TYPE_COMMENT, "Stoped periodic for RepeatID=%lu", pCRepeatMsg->m_stRepeatList[i].ulRepeatID);
                        }
                    }
                }
            }
            SetEvent(pCRepeatMsg->m_hSyncAccess);
        }
        Sleep(1);
    }
    // Set this event to Signal to indicate that thread exited.
    pCRepeatMsg->WriteLogMsg("StopPeriodicThread", DEBUGLOG_TYPE_COMMENT, "End");
    SetEvent(pCRepeatMsg->m_hThreadExited);
    return 1;
}

//-----------------------------------------------------------------------------
//	Function Name	: CRepeatMsg
//	Input Params	: void
//	Output Params	: void
//	Description		: Constructor for CRepeatMsg class.
//-----------------------------------------------------------------------------
CRepeatMsg::CRepeatMsg(CDeviceBase *pclsDevice, unsigned long	ulDevChannelID, CPeriodicMsg *pclsPeriodicMsg,
						   CDebugLog *pclsDebugLog)
{
	int	i;
    InitializeCriticalSection(&CRepeatMsgSection);      // Used in WriteLogMsg()

	// Save pointers.
    m_pclsLog = pclsDebugLog;
	m_pclsDevice = pclsDevice;
	m_ulDevChannelID = ulDevChannelID;
    m_pclsPeriodicMsg = pclsPeriodicMsg;

    // Write to Log File.
    WriteLogMsg("CRepeatMsg(4)", DEBUGLOG_TYPE_COMMENT, "Start");

	// Initialize the RepeatType portion of the list.
	for (i = 0; i <= REPEATMSG_LIST_SIZE; i++)
	{
		m_stRepeatList[i].pstMask = NULL;
		m_stRepeatList[i].pstPtrn = NULL;
		m_stRepeatList[i].ulCondition = 0;
		m_stRepeatList[i].ulPeriodicID = 0;
		m_stRepeatList[i].ulRepeatID = 0;
        m_stRepeatList[i].ulStatus = TRANSMISSION_IN_PROGRESS;
        m_stRepeatList[i].bStopPeriodic = false;
	}
    m_iProgressingRepeatMsgs = 0;
	m_hSyncAccess = CreateEvent(NULL, FALSE, TRUE, NULL);
    m_hThreadExit = CreateEvent(NULL, TRUE, TRUE, NULL);
    m_hThreadExited = CreateEvent(NULL, TRUE, TRUE, NULL);
    m_pclsStopPeriodicThread = AfxBeginThread((AFX_THREADPROC)StopPeriodicThread, this);

    // Write to Log File.
    WriteLogMsg("CRepeatMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//	Function Name	: ~CRepeatMsg
//	Input Params	: void
//	Output Params	: void
//	Description		: Destructor for CRepeatMsg class
//-----------------------------------------------------------------------------
CRepeatMsg::~CRepeatMsg()
{
    // Write to Log File.
    WriteLogMsg("~CRepeatMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Stop all Repeat
	StopRepeat();

    //Stop the Thread
    SetEvent(m_hThreadExit); 
    if (WaitForSingleObject(m_hThreadExited, THREAD_END_TIMEOUT) != WAIT_OBJECT_0)
    {
        DWORD dwExitCode;
        GetExitCodeThread(m_pclsStopPeriodicThread->m_hThread, &dwExitCode);
        TerminateThread(m_pclsStopPeriodicThread->m_hThread, dwExitCode);
        SetEvent(m_hSyncAccess);
        SetEvent(m_hThreadExited);
    }
    m_pclsStopPeriodicThread = NULL;

	// Delete the resources.
	for (int i = 0; i <= REPEATMSG_LIST_SIZE; i++)
	{
		if (m_stRepeatList[i].pstMask != NULL)
		{	
			delete m_stRepeatList[i].pstMask;
			m_stRepeatList[i].pstMask = NULL;
		}
		if (m_stRepeatList[i].pstPtrn != NULL)
		{	
			delete m_stRepeatList[i].pstPtrn;
			m_stRepeatList[i].pstPtrn = NULL;
		}
        m_iProgressingRepeatMsgs = 0;
		m_stRepeatList[i].ulCondition = 0;
		m_stRepeatList[i].ulPeriodicID = 0;
		m_stRepeatList[i].ulRepeatID = 0;
        m_stRepeatList[i].ulStatus = TRANSMISSION_IN_PROGRESS;
        m_stRepeatList[i].bStopPeriodic = false;
	}

    // Write to Log File.
    WriteLogMsg("~CRepeatMsg()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CRepeatMsg::WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
    if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
    {
        EnterCriticalSection(&CRepeatMsgSection); // Make processing parameters thread-safe
        char buf[1024];
        va_list ap;
        va_start(ap, chMsg);
        vsprintf_s(buf, sizeof(buf)-1, chMsg, ap);
        va_end(ap);
        buf[DEVICECAYMAN_ERROR_TEXT_SIZE] = '\0'; // We have our limits
        m_pclsLog->Write("RepeatMsg.cpp", chFunctionName, MsgType, buf);
        LeaveCriticalSection(&CRepeatMsgSection);
    }
}

//-----------------------------------------------------------------------------
//	Function Name	: StartRepeat
//	Input Params	: Condition, pstMask, pstPttrn, ulRepeatID
//	Output Params	: pulRepeatID
//	Description		: This function starts a repeat msg and returns 
//					  pulRepeatID for reference.
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::StartRepeat(
                            unsigned long   ulTimeInterval,
                            unsigned long   ulCondition,
							PASSTHRU_MSG	*pstMsg, 
							PASSTHRU_MSG	*pstMask, 
							PASSTHRU_MSG	*pstPttrn, 
							unsigned long	*pulRepeatID)
{
 	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        WriteLogMsg("StartRepeat", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("RepeatMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	} 
	J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_COMMENT, "Start");

	// Check if PeriodicMsg NULL pointers.
	if (m_pclsPeriodicMsg == NULL)
	{
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_FAILED);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_FAILED);
	}

	// Check if NULL pointers.
	if ((pstMask == NULL) || (pstPttrn == NULL))
	{
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG);
	}

	// Is number of Repeat exceeded the maximum.
	if (IsListFull())
	{
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_EXCEEDED_LIMIT);
	}

	// Check the Datasize
    if ((pstMask->ulDataSize > REPEATMSG_LIST_SIZE) || 
        (pstPttrn->ulDataSize > REPEATMSG_LIST_SIZE) || 
        (pstMask->ulDataSize != pstPttrn->ulDataSize))
	{
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG);
	}

    // Start Periodic Msg.
    unsigned long ulPeriodicID = 0;
    if ((enJ2534Error = m_pclsPeriodicMsg->StartPeriodic(pstMsg, &ulPeriodicID, ulTimeInterval))!= J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "StartPeriodic returned 0x%02X", enJ2534Error);
        SetEvent(m_hSyncAccess);
        return(enJ2534Error);
    }

	// Add the Repeat info. to our internal Database.
	if ((enJ2534Error = Add(ulCondition, pstMask, pstPttrn, pulRepeatID, ulPeriodicID)) != J2534_STATUS_NOERROR)
	{
        if(m_pclsPeriodicMsg != NULL)
        {
		    // Stop Periodic Msg
		    enJ2534Error = m_pclsPeriodicMsg->StopPeriodic(ulPeriodicID);
        }
        // Write to Log File.
        WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        SetEvent(m_hSyncAccess);
		return(enJ2534Error);
	}

    // Write to Log File.
    WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_COMMENT, "PeriodicID=%lu RepeatID=%lu", ulPeriodicID, *pulRepeatID);
    WriteLogMsg("StartRepeat()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetEvent(m_hSyncAccess);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: StopRepeat
//	Input Params	: 
//	Output Params	: 
//	Description		: This function stops the Repeat corresspond to a given ID.
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::StopRepeat(unsigned long * pulRepeatID)

{
	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        WriteLogMsg("StopRepeat", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("RepeatMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	} 

    J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;
    unsigned long ulRepeatID = *pulRepeatID;
    
    // Write to Log File.
    WriteLogMsg("StopRepeat()", DEBUGLOG_TYPE_COMMENT, "Start");
    WriteLogMsg("StopRepeat()", DEBUGLOG_TYPE_COMMENT, "RepeatID=%lu", ulRepeatID);

	// Validate if the Repeat msg. for this RepeatID was started earlier.
	if (!IsRepeatIDValid(ulRepeatID))
	{
        // Write to Log File.
        WriteLogMsg("StopRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG_ID);
	}

	// Periodic NULL check
    if(m_pclsPeriodicMsg == NULL)
    {
        // Write to Log File.
        WriteLogMsg("StopRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG_ID);
    }

	// Stop the Periodinc only if not stoped already 
    if(m_stRepeatList[ulRepeatID].ulStatus == TRANSMISSION_IN_PROGRESS)
    {
        if ((enJ2534Error = m_pclsPeriodicMsg->StopPeriodic(m_stRepeatList[ulRepeatID].ulPeriodicID)) 
                          != J2534_STATUS_NOERROR)
        {
            // Write to Log File.
            WriteLogMsg("StopRepeat", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
            SetEvent(m_hSyncAccess);
            return(enJ2534Error);
        }
        // Decrease the Number of transmissions in progress
        m_iProgressingRepeatMsgs--;
    }

	// Delete the Repeat
	Delete(ulRepeatID);

    // Write to Log File.
    WriteLogMsg("StopRepeat()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetEvent(m_hSyncAccess);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: StopRepeat
//	Input Params	: 
//	Output Params	: 
//	Description		: This is an override function that stops all the Repeats.
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::StopRepeat()
{
	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        WriteLogMsg("StopRepeat(All)", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("RepeatMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	} 
	J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;
    
    // Write to Log File.
    WriteLogMsg("StopRepeat(All)", DEBUGLOG_TYPE_COMMENT, "Start");

    if(m_pclsPeriodicMsg == NULL)
    {
        // Write to Log File.
        WriteLogMsg("StopRepeat(All)", DEBUGLOG_TYPE_ERROR, "NULL Periodic", J2534_STATUS_NOERROR);
        SetEvent(m_hSyncAccess);
	    return(J2534_ERR_FAILED);
    }
	// Run through all the Repeats and delete all of them.
	for (unsigned long i = 1; i <= REPEATMSG_LIST_SIZE; i++)
	{
        if((m_stRepeatList[i].pstMask != NULL) && (m_stRepeatList[i].ulStatus == TRANSMISSION_IN_PROGRESS))
	    {
            if ((enJ2534Error = m_pclsPeriodicMsg->StopPeriodic(m_stRepeatList[i].ulPeriodicID)) 
                              != J2534_STATUS_NOERROR)
            {
                // Write to Log File.
                WriteLogMsg("StopRepeat(All)", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
                SetEvent(m_hSyncAccess);
                return(enJ2534Error);
            }
	    }
	    // Delete the Repeat Msg
	    Delete(i);
	}

    // Write to Log File.
    WriteLogMsg("StopRepeat(All)", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    SetEvent(m_hSyncAccess);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: QueryRepeat
//	Input Params	: pulRepeatID
//	Output Params	: ulCondition
//	Description		: This function returns the Repeat Msg Condition
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::QueryRepeat(
                            unsigned long	ulRepeatID,
                            unsigned long   *ulStatus)
{
	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // Write to Log File.
        WriteLogMsg("QueryRepeat", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("RepeatMsg : Failed Synchronization");
		return(J2534_ERR_FAILED);	
	} 

    if(m_pclsPeriodicMsg == NULL)
    {
        // Write to Log File.
        WriteLogMsg("QueryRepeat", DEBUGLOG_TYPE_ERROR, "NULL Periodic", J2534_STATUS_NOERROR);
        SetEvent(m_hSyncAccess);
	    return(J2534_ERR_FAILED);
    }

	// Validate if the Repeat msg. for this RepeatID was started earlier.
	if (!IsRepeatIDValid(ulRepeatID))
	{
        // Write to Log File.
        WriteLogMsg("QueryRepeat()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG_ID);
        SetEvent(m_hSyncAccess);
		return(J2534_ERR_INVALID_MSG_ID);
	}
    // Get the Condition
    *ulStatus = m_stRepeatList[ulRepeatID].ulStatus;

    // This function will be called a lot, so log only the status value
    WriteLogMsg("QueryRepeat()", DEBUGLOG_TYPE_COMMENT, "RepeatID=%lu Status=%lu", ulRepeatID, *ulStatus);
    SetEvent(m_hSyncAccess);
	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: CompareMsg
//	Input Params	: pstMsg
//	Output Params	: -
//	Description		: This function compare Msg and updates Matching repeat msg Status 
//                    accordingly (Used by Protocol Callback) 
//-----------------------------------------------------------------------------
void CRepeatMsg::CompareMsg(PASSTHRU_MSG *pstMsg)
{
 	// Synchronize Access this function.
	if (WaitForSingleObject(m_hSyncAccess, PERIODICMSG_SYNC_TIMEOUT) 
							!= WAIT_OBJECT_0)
	{
        // This function will be called a lot, so log only the err 
        WriteLogMsg("CompareMsg", DEBUGLOG_TYPE_ERROR, "Failed Synchronization");
		SetLastErrorText("RepeatMsg : Failed Synchronization");
		return;	
	} 
         
    WriteLogMsg("CompareMsg", DEBUGLOG_TYPE_COMMENT, "Start N=%d",m_iProgressingRepeatMsgs );
  
    // Check for NULL
    if(m_pclsPeriodicMsg == NULL)
    {
        SetEvent(m_hSyncAccess);
	    return;
    }

    // If there are not any transmissions in progress return immediately
    if(m_iProgressingRepeatMsgs < 1)
    {
        SetEvent(m_hSyncAccess);
	    return;
    }

    bool bMatch = true;
	J2534ERROR enJ2534Error = J2534_STATUS_NOERROR;

	// Run through all the Repeats and compare Msg
	for (unsigned long i = 1; i <= REPEATMSG_LIST_SIZE; i++)
	{
        bMatch = false;

        // Check NULL and if the criteria is not yet met
	    if ((m_stRepeatList[i].pstMask != NULL) && (m_stRepeatList[i].ulStatus == TRANSMISSION_IN_PROGRESS))
	    {
            // Check Size
            if (m_stRepeatList[i].pstPtrn->ulDataSize <= pstMsg->ulDataSize)
            {
                // Do not even start to compare if the CAN ID is different
			    if ((!IsCAN(pstMsg->ulProtocolID)) || 
                    (IsCAN(pstMsg->ulProtocolID) &&
			        ((m_stRepeatList[i].pstPtrn->ulTxFlags & J2534_CONNECT_FLAGBIT_CAN29BIT) ==
					    (pstMsg->ulRxStatus & J2534_CONNECT_FLAGBIT_CAN29BIT))))
                {
                    // Only the first DataSize bytes of each received message shall be evaluated
                    for (unsigned long j = 0; j < m_stRepeatList[i].pstPtrn->ulDataSize; j++)
                    {
                        if ((m_stRepeatList[i].pstMask->ucData[j] & pstMsg->ucData[j]) ==
                            (m_stRepeatList[i].pstMask->ucData[j] & m_stRepeatList[i].pstPtrn->ucData[j]))
                        {
                            bMatch = true;
                        }
                        else
                        {
                            bMatch = false;
                            break;
                        }
                    }
                }
            }
            // Criteria is met  
            if(((m_stRepeatList[i].ulCondition == CONDITION_CONTINUE_UNTIL) &&   bMatch) ||
               ((m_stRepeatList[i].ulCondition == CONDITION_CONTINUE_WHILE) && (!bMatch)))
            {
                // Command the thread to stop the periodic
                m_stRepeatList[i].bStopPeriodic = true;
            }
	    }
	} // end for(i)

    WriteLogMsg("CompareMsg", DEBUGLOG_TYPE_COMMENT, "End");
    SetEvent(m_hSyncAccess);
    return;
}

//-----------------------------------------------------------------------------
//	Function Name	: Add
//	Input Params	: void
//	Output Params	: void
//	Description		: This function assigns a Msg. ID, adds message to 
//					  internal buffer and returns the PeriodicID to user.
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::Add(
                            unsigned long   ulCondition,
							PASSTHRU_MSG	*pstMask, 
							PASSTHRU_MSG	*pstPtrn, 
							unsigned long	*pulRepeatID,
							unsigned long	ulPeriodicID)

{
	unsigned long	ulRepeatID;

	// Get new Repeat ID.
	if ((ulRepeatID = GetNewRepeatID()) == 0)
	{
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_EXCEEDED_LIMIT);
		return(J2534_ERR_EXCEEDED_LIMIT);	
	}

	// Save Mask in our internal buffer.
	if ((m_stRepeatList[ulRepeatID].pstMask = new PASSTHRU_MSG) == NULL)
	{
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Mask");
		SetLastErrorText("RepeatMsg : Could not create Object for Mask");
		return(J2534_ERR_FAILED);	
	}
	memcpy(m_stRepeatList[ulRepeatID].pstMask, pstMask, sizeof(PASSTHRU_MSG));

	// Save Pattern in our internal buffer.
	if ((m_stRepeatList[ulRepeatID].pstPtrn = new PASSTHRU_MSG) == NULL)
	{
        // Write to Log File.
        WriteLogMsg("Add()", DEBUGLOG_TYPE_ERROR, "Could not create Object for Pattern");
		SetLastErrorText("RepeatMsg : Could not create Object for Pattern");
		return(J2534_ERR_FAILED);	
	}
	memcpy(m_stRepeatList[ulRepeatID].pstPtrn, pstPtrn, sizeof(PASSTHRU_MSG));
	 
    // Save Condition
	m_stRepeatList[ulRepeatID].ulCondition = ulCondition;

    // Save Periodic ID
	m_stRepeatList[ulRepeatID].ulPeriodicID = ulPeriodicID;

	// Save Repeat ID.
	m_stRepeatList[ulRepeatID].ulRepeatID = ulRepeatID;

    // Set status
    m_stRepeatList[ulRepeatID].ulStatus = TRANSMISSION_IN_PROGRESS;

    // Set Command
    m_stRepeatList[ulRepeatID].bStopPeriodic = false;

	// Return the RepeatID to user.
	*pulRepeatID = ulRepeatID;

    // Increase  the  Number of transmissions in progress
    m_iProgressingRepeatMsgs++;

	return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: Delete
//	Input Params	: void
//	Output Params	: void
//	Description		: This function removes the Repeat from the Database that 
//					  was added earlier.
//-----------------------------------------------------------------------------
J2534ERROR CRepeatMsg::Delete(unsigned long ulRepeatID)
{
	if (m_stRepeatList[ulRepeatID].pstMask != NULL)
	{
		delete m_stRepeatList[ulRepeatID].pstMask;
		m_stRepeatList[ulRepeatID].pstMask = NULL;
	}
	if (m_stRepeatList[ulRepeatID].pstPtrn != NULL)
	{
		delete m_stRepeatList[ulRepeatID].pstPtrn;
		m_stRepeatList[ulRepeatID].pstPtrn = NULL;
	}
	m_stRepeatList[ulRepeatID].ulCondition = 0;
	m_stRepeatList[ulRepeatID].ulPeriodicID = 0;
	m_stRepeatList[ulRepeatID].ulRepeatID = 0;
    m_stRepeatList[ulRepeatID].ulStatus = TRANSMISSION_IN_PROGRESS;
    m_stRepeatList[ulRepeatID].bStopPeriodic = false;

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//	Function Name	: GetNewRepeatID
//	Input Params	: void
//	Output Params	: void
//	Description		: If all RepeatIDs are used, returns 0 otherwise returns new
//					  RepeatID to be used.
//-----------------------------------------------------------------------------
unsigned char CRepeatMsg::GetNewRepeatID()
{
	unsigned char	ucIdx;

	for (ucIdx = 1; ucIdx <= REPEATMSG_LIST_SIZE; ucIdx++)
	{
		if (m_stRepeatList[ucIdx].pstMask == NULL)
			return(ucIdx);
	}
	
	return(0);
}

//-----------------------------------------------------------------------------
//	Function Name	: IsRepeatIDValid
//	Input Params	: void
//	Output Params	: void
//	Description		: Checks to see if the given RepeatID is valid.
//-----------------------------------------------------------------------------
bool CRepeatMsg::IsRepeatIDValid(unsigned long ulRepeatID)
{
	if (ulRepeatID > 0 && ulRepeatID <= REPEATMSG_LIST_SIZE)
	{
		if (m_stRepeatList[ulRepeatID].pstMask != NULL)
			return(true);
	}
	
	return(false);
}

//-----------------------------------------------------------------------------
//	Function Name	: IsListFull
//	Input Params	: void
//	Output Params	: void
//	Description		: Checks to see if the list to store Repeat info. is full.
//-----------------------------------------------------------------------------
bool CRepeatMsg::IsListFull()
{
	unsigned long ucIdx;

	for (ucIdx = 1; ucIdx <= REPEATMSG_LIST_SIZE; ucIdx++)
	{
		if (m_stRepeatList[ucIdx].pstMask == NULL)
			return(false);
	}
	
	return(true);
}


//-----------------------------------------------------------------------------
//	Function Name	: IsCAN
//	Input Params	: void
//	Output Params	: void
//	Description		: Checks to see if the Protocol is CAN based.
//-----------------------------------------------------------------------------
bool CRepeatMsg::IsCAN(unsigned long ulProtocolID)
{
    if ((ulProtocolID == CAN) ||
        (ulProtocolID == CAN_PS) ||
        (ulProtocolID == CAN_CH1) ||
        (ulProtocolID == CAN_CH2) ||
        (ulProtocolID == CAN_CH3) ||
        (ulProtocolID == CAN_CH4) ||
        (ulProtocolID == ISO15765) ||
        (ulProtocolID == ISO15765_PS) ||
        (ulProtocolID == ISO15765_CH1) ||
        (ulProtocolID == ISO15765_CH2) ||
        (ulProtocolID == ISO15765_CH3) ||
        (ulProtocolID == ISO15765_CH4) ||
        (ulProtocolID == SW_CAN_PS) ||
        (ulProtocolID == SW_ISO15765_PS) ||
        (ulProtocolID == SW_CAN_CAN_CH1) ||
        (ulProtocolID == SW_CAN_ISO15765_CH1) ||
        (ulProtocolID == FT_CAN_PS) ||
        (ulProtocolID == FT_ISO15765_PS) ||
        (ulProtocolID == FT_CAN_CH1) ||
        (ulProtocolID == FT_ISO15765_CH1) ||
        (ulProtocolID == J1939_PS) ||
        (ulProtocolID == J1939_CH1) ||
        (ulProtocolID == J1939_CH2) ||
        (ulProtocolID == J1939_CH3) ||
        (ulProtocolID == J1939_CH4) ||
        (ulProtocolID == TP2_0_PS) ||
        (ulProtocolID == TP2_0_CH1) ||
        (ulProtocolID == TP2_0_CH2) ||
        (ulProtocolID == TP2_0_CH3) ||
        (ulProtocolID == TP2_0_CH4))
    {
        return true;
    }
    else
    {
         return false;
    }
}
