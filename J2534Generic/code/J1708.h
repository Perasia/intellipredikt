/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1708.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              J1708 Class.
* Note:
*
*******************************************************************************/

#ifndef _J1708_H_
#define _J1708_H_

// Constants

#define J1708_ERROR_TEXT_SIZE		120
#define J1708_DATA_RATE_DEFAULT		9600
#define J1708_HEADER_SIZE			0		// Number of Header bits.
#define J1708_MSG_SIZE_MIN			1
#define J1708_MSG_SIZE_MAX			4095

typedef char  unsigned  dgUint8;

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnJ1708RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
							LPVOID pVoid);

// CJ1708 Class
class CJ1708 : public CProtocolBase
{
public:
	CJ1708(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
	~CJ1708();

	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);

	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);


public:
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);   
	J2534ERROR		        J1708Pins(unsigned long ulValue);    
	J2534ERROR		        J1939Pins(unsigned long ulValue);

	unsigned long			m_ulChecksumFlag;	
    J2534_PROTOCOL          m_enJ1708Protocol;    
	unsigned long			m_ulJ1708PPSS;
	bool					m_bJ1708Pins;                                    
	unsigned long			m_ulJ1939PPSS;
	bool					m_bJ1939Pins;
};

#endif
