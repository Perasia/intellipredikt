/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DebugLog.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the definition of the functions
*              used for logging debug information. This Class could
*              be used with any project to be handy to log the 
*              traces in the code.
* Note:
*
*******************************************************************************/

#include "StdAfx.h"
#include "DebugLog.h"
#include "Time.h"
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

DWORD MilliSecondsNow();

//-----------------------------------------------------------------------------
//  Function Name   : CDebugLog()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a CDebugLog class constructor
//-----------------------------------------------------------------------------
CDebugLog::CDebugLog()
{
    m_hWriteLogSync = NULL;
    m_pfdLogFile = NULL;
    m_ulLoggingType = 0;
    m_strLogFileName = "";
    m_lLoggingSizeMaxKB = 0;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CDebugLog()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a CDebugLog class destructor
//-----------------------------------------------------------------------------
CDebugLog::~CDebugLog()
{
    // If the Log file is open, close it.
    if (m_pfdLogFile != NULL)
    {
        Close();
    }

    // Initialize the Logging Type. (e.g. COMMENT, ERROR, DATA, ...)
    m_ulLoggingType = 0;
}

//-----------------------------------------------------------------------------
//  Function Name   : Write()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a function that writes arguments to log file
//-----------------------------------------------------------------------------
//bool CDebugLog::Write(CString csFile, CString csFunc, unsigned long ulType, CString csDesc)
bool CDebugLog::Write(char *csFile, char *csFunc, unsigned long ulType, char *csDesc)
{
    bool    bRet;
    char *ErrorDesc;

    // If the File is not open (File Descriptor is NULL).
    if (m_pfdLogFile == NULL)
    {
        return(false);
    }
    if (WaitForSingleObject(m_hWriteLogSync, DEBUGLOG_WRITELOG_TIMEOUT) == WAIT_OBJECT_0)
    {
        ResetEvent(m_hWriteLogSync);
        bRet = true;
        ErrorDesc = NULL;

        switch(ulType)
        {
        case DEBUGLOG_TYPE_ERROR:
            if (m_ulLoggingType & DEBUGLOG_TYPE_ERROR)
            {
                ErrorDesc = "ERROR";
            }
            break;
        case DEBUGLOG_TYPE_COMMENT:
            if (m_ulLoggingType & DEBUGLOG_TYPE_COMMENT)
            {
                ErrorDesc = "COMMENT";
            }
            break;
        case DEBUGLOG_TYPE_DATA:
            if (m_ulLoggingType & DEBUGLOG_TYPE_DATA)
            {
                ErrorDesc = "DATA";
            }
            break;
        default:
            bRet = false;
            break;
        }   

    }
    else
    {
        bRet = false;
        ErrorDesc = NULL;
    }
    if (ErrorDesc)
    {
        fprintf_s(m_pfdLogFile, "%-20s, %-30s, %s, %-80s, %u\n", csFile, csFunc, ErrorDesc, csDesc, MilliSecondsNow());
        fflush(m_pfdLogFile);
    }

    SetEvent(m_hWriteLogSync);
    return(bRet);
}

//-----------------------------------------------------------------------------
//  Function Name   : Open()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a function that opens a given log file
//-----------------------------------------------------------------------------
bool CDebugLog::Open(CString csLogFileName, unsigned long ulLogType, char *csDllFileName, unsigned long ulLoggingSizeMaxKB)
{
    FILE * fstream = NULL;
    char ct_buff[40];

    // If Log File already open.
    if (m_pfdLogFile != NULL)
    {
        return(true);
    }
      
    m_strLogFileName = csLogFileName;
    m_lLoggingSizeMaxKB = ulLoggingSizeMaxKB;
    // Open the file.
    if (ulLogType & DEBUGLOG_TYPE_APPEND)
    {
        fopen_s(&fstream, csLogFileName, "a");
    }
    else
    {
        fopen_s(&fstream, csLogFileName, "w");
    }

    if (fstream == NULL)
    {
        return(false);
    }

    //Get Current Time
    CTime curTime = CTime::GetCurrentTime();
    
    CString csMeridian = "", csTime = "";
    
    unsigned int iHr  = curTime.GetHour();
    unsigned int iMin = curTime.GetMinute();
    unsigned int iSec = curTime.GetSecond();
    unsigned int iMonth  = curTime.GetMonth();
    unsigned int iDay = curTime.GetDay();
    unsigned int iYear = curTime.GetYear();
    
    if(iHr < 12)
    {       
        csMeridian = "AM";      
    }
    else    
    {       
        if(iHr >=13)
            iHr %= 12;

        csMeridian = "PM";
    }
    
    csTime.Format("%2d:%02d:%02d %s - %02d/%02d/%d",  iHr, iMin, iSec, csMeridian, iMonth, iDay, iYear);

    //Print Headers
    if(fstream != NULL)
    {
		_ftprintf(fstream,"Opening Log File. Current time is %s", (LPCTSTR)csTime);
        CFile stFile;
        CString cszFile;
        CFileStatus status;
        unsigned char *ucBuffer;
        LPVOID *pBuffer;
        unsigned int uiSize;
        unsigned char ucDllVersion[100];
        time_t t;
//        char tempstr[1024];
        DWORD dwBufferSize;
        //strcpy(tempstr, theApp.m_J2534Registry.chLoggingDirectory);
/*        GetSystemDirectory(tempstr, 1024);
        strcat(tempstr, "\\");
        strcat(tempstr, DLL_NAME);
*/
        stFile.GetStatus(csDllFileName, status);
        dwBufferSize = GetFileVersionInfoSize(csDllFileName, 0);
        ucBuffer = (unsigned char *)malloc((dwBufferSize+1)*sizeof(char));
        GetFileVersionInfo(csDllFileName, 0, dwBufferSize, (LPVOID)ucBuffer);
        // Query Version Number
        VerQueryValue((LPVOID) ucBuffer, 
            TEXT("\\StringFileInfo\\040904B0\\ProductVersion"), 
            (void**)&pBuffer, &uiSize);

        // Getting ucDllVersion
        strncpy_s((char*)ucDllVersion, sizeof(ucDllVersion)-1, (LPCTSTR) pBuffer, uiSize);
        fprintf_s(fstream,"\n%s Version %s", csDllFileName, ucDllVersion);
        fprintf_s(fstream,"\n%s File Size %ld bytes", csDllFileName, status.m_size);

        t = status.m_mtime.GetTime();
        ctime_s(ct_buff, sizeof(ct_buff), &t);
        fprintf_s(fstream, "\n%s Time modified : %s", csDllFileName, ct_buff);
        if(ucBuffer != NULL){
            free(ucBuffer);
            ucBuffer = NULL;
        }
        fprintf_s(fstream,"================================================");
        fprintf_s(fstream,"==================================================\n");
        fflush(m_pfdLogFile);
    }

    // Create Event for synchronization.
    if ((m_hWriteLogSync = CreateEvent(NULL, TRUE, TRUE, NULL)) == NULL)
    {
        // Close file.
        fclose(fstream);
        fstream = NULL;
        return(false);
    }

    // Save the Logging type requested so that Write() will only log those
    // type.
    m_ulLoggingType = ulLogType;

    m_pfdLogFile = fstream;

    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : Close()
//  Input Params    : void
//  Output Params   : void
//  Return          : bool
//  Description     : This is a function that closes the given log file
//-----------------------------------------------------------------------------
bool CDebugLog::Close()
{
    bool    bRet = true;

    if (WaitForSingleObject(m_hWriteLogSync, DEBUGLOG_WRITELOG_TIMEOUT) ==
                            WAIT_OBJECT_0)
    {
        ResetEvent(m_hWriteLogSync);

        if (m_pfdLogFile != NULL)
        {       
            fprintf_s(m_pfdLogFile,"\n================================================"); 
            fprintf_s(m_pfdLogFile,"==================================================\n");
            
            //Get Current Time
            CTime curTime = CTime::GetCurrentTime();
            
            CString csMeridian = "", csTime = "";
            unsigned int iHr = curTime.GetHour();
            unsigned int iMin = curTime.GetMinute();
            unsigned int iSec = curTime.GetSecond();
            unsigned int iMonth  = curTime.GetMonth();
            unsigned int iDay = curTime.GetDay();
            unsigned int iYear = curTime.GetYear();
            
            if(iHr < 12)
            {       
                csMeridian = "AM";      
            }
            else    
            {       
                if(iHr >=13)
                    iHr %= 12;

                csMeridian = "PM";
            }
            
            csTime.Format("%2d:%02d:%02d %s - %02d/%02d/%d",  iHr, iMin, iSec, csMeridian, iMonth, iDay, iYear);

            fprintf_s(m_pfdLogFile,"Closing Log File. Current time is %s\n\n",csTime);
            
            if (!fclose(m_pfdLogFile))
            {
                m_pfdLogFile = NULL;
                m_ulLoggingType = 0;
                if (m_lLoggingSizeMaxKB)
                {
                    // Trim log file if needed
                    TrimLogFile();
                }
            }
            else
            {
                bRet = false;
            }
        }

        SetEvent(m_hWriteLogSync);
    }
    else
    {
        SetEvent(m_hWriteLogSync);
        ResetEvent(m_hWriteLogSync);
        if (!fclose(m_pfdLogFile))
        {
            m_pfdLogFile = NULL;
            m_ulLoggingType = 0;
        }
        else
        {
            bRet = false;
        }
        SetEvent(m_hWriteLogSync);
    }

    if (bRet)
    {
        // Close the handle for synchronization.
        if (!CloseHandle(m_hWriteLogSync))
            bRet = false;
        else
            m_hWriteLogSync = NULL;
    }

    return(bRet);
}

//-----------------------------------------------------------------------------
//  Function Name   : TrimLogFile()
//  Input Params    : void
//  Output Params   : void
//  Return          : void
//  Description     : This is a function that trims the given log file
//-----------------------------------------------------------------------------
void CDebugLog::TrimLogFile()
{
    int i;
    struct stat buf;
    if (stat(m_strLogFileName, &buf) == 0)
    {
        long LoggingSizeMax = m_lLoggingSizeMaxKB * 1024 + 144; // Add length of one log-line
        if (buf.st_size > LoggingSizeMax)
        {
            long StartFileOffset = buf.st_size - LoggingSizeMax;
            FILE * fstream;
            fopen_s(&fstream, m_strLogFileName, "r");
            if (fstream == NULL)
            {
                return; // Just bail-out if we have an error
            }
            CString tmp_strLogFileName = m_strLogFileName + ".tmp";
            FILE * fstream_tmp;
            fopen_s(&fstream_tmp, tmp_strLogFileName, "w");
            if (fstream_tmp == NULL)
            {
                fclose(fstream);
                return; // Just bail-out if we have an error
            }           
            char buffer[256];
            for (i = 0; i < 5; i++)  // Keep the header information (log date/time stamps, J2534 DLL, version, date modified)
            {
                fgets(buffer, sizeof(buffer) -1, fstream);
                fputs(buffer, fstream_tmp);
            }
            if (fseek(fstream, StartFileOffset, SEEK_SET))
            {
                fclose(fstream);
                fclose(fstream_tmp);
                DeleteFile(tmp_strLogFileName);
                return; // Just bail-out if we have an error
            }
            fgets(buffer, sizeof(buffer) -1, fstream);  // Throw away what could be a partial line
            while (fgets(buffer, sizeof(buffer) - 1, fstream))
            {
                fputs(buffer, fstream_tmp);
            }
            fclose(fstream);
            fclose(fstream_tmp);
            if (DeleteFile(m_strLogFileName) == 0)
            {
                return; // Just bail-out if we have an error
            }
            rename(tmp_strLogFileName, m_strLogFileName);
        }
    }
}

