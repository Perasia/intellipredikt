/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1850.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              CAN Class.
* Note:
*
*******************************************************************************/

#ifndef _J1850VPW_H_
#define _J1850VPW_H_

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define J1850VPW_ERROR_TEXT_SIZE		120
#define J1850VPW_DATA_RATE_DEFAULT	10416
#define J1850VPW_MAXDATA_RATE	41666
#define J1850VPW_DATA_RATE_DEFAULT_1	10400
#define J1850VPW_MAXDATA_RATE_1	41600

#define J1850VPW_MAX_DATALENGTH		4128

#define J1850VPW_10_4K				0
#define J1850VPW_41_6K				1

#define J1850VPW_MSG_SIZE_MIN		1
#define J1850VPW_MSG_SIZE_MAX		4128

void OnJ1850VPWRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CCAN Class
class CJ1850VPW : public CProtocolBase
{
public:
	CJ1850VPW(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CJ1850VPW();
		
	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
#ifdef J2534_0500
	virtual J2534ERROR  vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulNumMsgs,
								unsigned long ulTimeout, 
								unsigned long);
#endif
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);
	

	J2534ERROR				MessageValid(PASSTHRU_MSG	   *pstrucJ2534Msg,
										unsigned long  *pulNumMsgs);
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
	J2534ERROR  vSetProgrammingVoltage(unsigned long ulPin,
										unsigned long ulVoltage);
	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);
private:
	PASSTHRU_MSG        structj1850;
	unsigned long m_ucIOdata;
	J2534_PROTOCOL		m_enJ1850Protocol;
};

#endif
