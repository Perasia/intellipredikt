/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1850.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support J1850VPW 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "J1850.h"

//-----------------------------------------------------------------------------
//  Function Name   : CJ1850VPW
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CJ1850VPW class
//-----------------------------------------------------------------------------
CJ1850VPW::CJ1850VPW(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) : CProtocolBase(pclsDevice, pclsDebugLog)
{
    
    //  m_pCFilterMsg = NULL;   
    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "CJ1850VPW()", DEBUGLOG_TYPE_COMMENT, "Start");
    
    // Initialize.
    m_bConnected = false;
    
    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "CJ1850VPW()", DEBUGLOG_TYPE_COMMENT, "End");
    
    //Initialize the speed
    m_ucIOdata = J1850VPW_10_4K;
    m_bLoopback = false;
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ1850VPW
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CJ1850VPW class
//-----------------------------------------------------------------------------
CJ1850VPW::~CJ1850VPW()
{
    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "~CJ1850VPW()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "~CJ1850VPW()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vConnect(
                        J2534_PROTOCOL  enProtocolID,
                        unsigned long   ulFlags,
                        unsigned long   ulBaudRate,
                        DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                        LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    switch(enProtocolID)
    {
        case J1850VPW:
        {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
            if ((ulBaudRate != J1850VPW_DATA_RATE_DEFAULT) && (ulBaudRate != J1850VPW_MAXDATA_RATE) &&
                (ulBaudRate != J1850VPW_DATA_RATE_DEFAULT_1) && (ulBaudRate != J1850VPW_MAXDATA_RATE_1))
            {
                return(J2534_ERR_INVALID_BAUDRATE);
            }
#endif
#ifdef J2534_0305
            ulBaudRate = J1850VPW_DATA_RATE_DEFAULT_1;
#endif
            m_ulDataRate = ulBaudRate;
            m_enJ1850Protocol = J1850VPW;
        }
        break;
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, 
                                                ulFlags, 
                                                ulBaudRate,
                                                OnJ1850VPWRxMessage, 
                                                this)) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vDisconnect()
{
    J2534ERROR      enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vReadMsgs(PASSTHRU_MSG        *pstPassThruMsg,
                           unsigned long    *pulNumMsgs,
                           unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

#ifdef J2534_0500
//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
								 unsigned long   *pulNumMsgs,
								 unsigned long   ulTimeout,
								 unsigned long ulChannelId)
{
	//*****************************IMPORTANT NOTE******************************* 
	// Perform all the protocol specific stuff in this function.
	//**************************************************************************

	J2534ERROR  enJ2534Error;
	unsigned long   ulIdx1;

	// Write to Log File.
	WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
	{
		// Check Msg. Protocol ID.
		if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1850Protocol)
		{
			// Write to Log File.
			WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}

		// Check if msg. format is valid.
		if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
		{
			// Write to Log File.
			WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}

	// Write using the generic Write.
	if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
		pulNumMsgs,
		ulTimeout
		, 0
	))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

	return(J2534_STATUS_NOERROR);
}
#endif
//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
                            unsigned long   *pulNumMsgs,
                            unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1850Protocol)
        {
            // Write to Log File.
            WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }
        
        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vStartPeriodicMsg(PASSTHRU_MSG        *pstPassThruMsg,
                                   unsigned long    *pulMsgID,
                                   unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enJ1850Protocol)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStartPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStopPeriodicMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850VPW::vStartMsgFilter(J2534_FILTER     enumFilterType,
                                      PASSTHRU_MSG      *pstMask,
                                      PASSTHRU_MSG      *pstPattern,
                                      PASSTHRU_MSG      *pstFlowControl,
                                      unsigned long     *pulFilterID)
{
    J2534ERROR  enJ2534Error;
    unsigned long   ulOne;
    ulOne = 1;


    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device J1850VPWnot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");
/*
    // Check to see if message structure is valid
    enJ2534Error = MessageValid(pstPattern, &ulOne);

    if (enJ2534Error != J2534_STATUS_NOERROR)
        return(enJ2534Error);
*/
    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enJ1850Protocol) ||
        (pstPattern->ulProtocolID != m_enJ1850Protocol))
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }
    
    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetLastErrorText("J1850VPW : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850VPW::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);

    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function which should be implemented by
//                    the class derived from this base class.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850VPW::vIoctl(J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput)
{
    J2534ERROR enumJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration    
        {
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
            
        }
        break;
        
    case SET_CONFIG:            // Set configuration
        {
            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            
            /*Check if the IOCTL value is not in range */
/*          if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("J1850VPW.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
                return(enumJ2534Error);
            }
*/
        }
        
        break;
        
    case CLEAR_TX_BUFFER:   // Clear all messages in its transmit queue
        {           
            
            m_pclsTxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_RX_BUFFER:// Clear all messages in its receive queue
        {       
            m_pclsRxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        {           
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }
        }
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        {       
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
        }
        break;
        
    case READ_MEMORY:
        {
            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("J1850VPW.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
                return(enumJ2534Error);
            }   
        }
        break;
    case WRITE_MEMORY:
        {
            break;
        }

    default:                    // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }


    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);

    // Write to Log File.
    WriteLogMsg("J1850VPW.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnJ1850VPWRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    J1850VPW messages.
//-----------------------------------------------------------------------------
void OnJ1850VPWRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CJ1850VPW                   *pclsJ1850VPW;
    FILTERMSG_CONFORM_REQ   stConformReq;
    char            szBuffer[J1850VPW_ERROR_TEXT_SIZE];
    unsigned long   i, j;

    pclsJ1850VPW = (CJ1850VPW *) pVoid;

    // Check for NULL pointer.
    if (pclsJ1850VPW == NULL)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        return;
    }

    // Check if this is a Rx Break message.
    if (pstPassThruMsg->ulRxStatus & 0x04)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Break CALLBACK");

        // Enqueue to Circ Buffer.
        pclsJ1850VPW->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));

        pclsJ1850VPW->m_ulDataRate = J1850VPW_DATA_RATE_DEFAULT;

#ifdef J2534_0500
		pclsJ1850VPW->m_ulPhyChnRxCount++;
#endif
        return;
    }
    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        if (pclsJ1850VPW->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsJ1850VPW->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
		pclsJ1850VPW->m_ulPhyChnRxCount++;
#endif
        return;
    }

    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
    {
        // Write to Log File.
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "Rx Status 0x%X", pstPassThruMsg->ulRxStatus);
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, "Data Size 0x%X", pstPassThruMsg->ulDataSize);

        if(pstPassThruMsg->ulDataSize > 0)
        {
            j = 0;
            for(i = 0; i < pstPassThruMsg->ulDataSize && i < 15; i++)
            {
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j-1, "%02X ",pstPassThruMsg->ucData[i]);              
            }

            if(i >= 15)
                j += sprintf_s(szBuffer + j, sizeof(szBuffer)-j-1, "...");
        }
        CProtocolBase::WriteLogMsg("J1850.cpp", "OnJ1850VPWRxMessage()", DEBUGLOG_TYPE_COMMENT, szBuffer);
    }

    // Apply Filters and see if msg. is required.
    if (pclsJ1850VPW->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsJ1850VPW->IsMsgValid(pstPassThruMsg) &&
            pclsJ1850VPW->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsJ1850VPW->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
			pclsJ1850VPW->m_ulPhyChnRxCount++;
#endif
        }
    }

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1850VPW::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J1850VPW_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1850VPW_MAX_DATALENGTH))
    {
        return(false);
    }
    return(true);
}

J2534ERROR CJ1850VPW::MessageValid(PASSTHRU_MSG    *pstrucJ2534Msg,
                                   unsigned long  *pulNumMsgs)
{
    unsigned long   ulIdx1;
    
    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {       
        if ((pstrucJ2534Msg + ulIdx1)->ulDataSize == 0 ||
            (pstrucJ2534Msg + ulIdx1)->ulDataSize > J1850VPW_MAX_DATALENGTH)
        {
            // Message with data length being 0 bytes or 
            // greater than 4128 bytes
            *pulNumMsgs = 0;
            return(J2534_ERR_INVALID_MSG);
        }
    }

    return(J2534_STATUS_NOERROR);
}


//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1850VPW.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1850VPW : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        //memset(&pSconfig,0,sizeof(SCONFIG));
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;
                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;
                break;

            default:
                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;
    enumJ2534Error = J2534_STATUS_NOERROR;
    
//  pSconfig = new SCONFIG;
//  pSconfig = pInput->pConfigPtr;
    
    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);
    
    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1850VPW.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1850VPW : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", 
                                pSconfig->Parameter, pSconfig->ulValue);
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
        case DATA_RATE:         // Data Rate
            
            enumJ2534Error = DataRate(pSconfig->ulValue);
            if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);
            if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                        pSconfig,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("J1850VPW.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//              return(enumJ2534Error);
            }
            break;
            
        case LOOPBACK:          // Loopback
            
            enumJ2534Error = Loopback(pSconfig->ulValue);
            
            break;
            
        default:
            
            return(J2534_ERR_NOT_SUPPORTED);
        }
        
        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::DataRate(unsigned long ulValue)
{
    
    J2534ERROR enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if ((ulValue == 10400) || (ulValue == 10416))
    {
        m_ucIOdata = J1850VPW_10_4K;
        
    }
    else if ((ulValue == 41600) || (ulValue == 41666))
    {
        m_ucIOdata = J1850VPW_41_6K;
    }
    else
    {
        return(J2534_ERR_NOT_SUPPORTED);
    }

    m_ulDataRate = ulValue;
    
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850VPW::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

/*J2534ERROR CJ1850VPW::vSetProgrammingVoltage(unsigned long ulPin,
                                                unsigned long ulVoltage)
{
    char    szBuffer[J1850VPW_ERROR_TEXT_SIZE];
    J2534ERROR enJ2534Error;
    unsigned char ucPin;
    unsigned char ucVoltage;

    ucPin = (unsigned char)ulPin;

    //Voltage shall be in decivolts
    if(ulVoltage == 0xFFFFFFFE) //SHORT TO GROUND
    {
        ucVoltage = 0xFE;
    }
    else if(ulVoltage == 0xFFFFFFFF) // VOLTAGE OFF
    {
        ucVoltage = 0xFF;
    }
    else // Decivolts
    {
        ucVoltage = (unsigned char)(ulVoltage / 100); //Convert from millivolts to decivolts    
    }
    
    if ((enJ2534Error = CProtocolBase::vSetProgrammingVoltage( ulPin,ulVoltage))
        != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850VPW.cpp", "vSetProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
    //  return(enJ2534Error);
    }
    
    return enJ2534Error;
}*/
