/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: CCD.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              CCD Class.
* Note:
*
*******************************************************************************/

#ifndef _CCD_H_
#define _CCD_H_

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

#define CCD_ERROR_TEXT_SIZE		120

#define CCD_DATA_RATE_DEFAULT	7812
#define CCD_MIDDATA_RATE		62500
#define CCD_MAXDATA_RATE		125000

#define CCD_MAX_DATALENGTH		4128

#define CCD_10_4K				0
#define CCD_41_6K				1

#define CCD_MSG_SIZE_MIN		1
#define CCD_MSG_SIZE_MAX		4128

void OnCCDRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid);

// CCAN Class
class CCCD : public CProtocolBase
{
public:
	CCCD(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL);
	~CCCD();
	unsigned long m_ulDataRate;
	bool m_bLoopback;
	virtual J2534ERROR	vConnect(
								J2534_PROTOCOL enProtocolID,
								unsigned long ulFlags,
								unsigned long ulBaudRate,
								DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
								LPVOID		  pVoid=NULL);
	virtual J2534ERROR	vDisconnect();
	virtual J2534ERROR	vReadMsgs(
								PASSTHRU_MSG  *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vWriteMsgs(
								PASSTHRU_MSG *pstrucJ2534Msg, 
								unsigned long *pulNumMsgs, 
								unsigned long ulTimeout);
	virtual J2534ERROR	vStartPeriodicMsg(
								PASSTHRU_MSG *pstrucJ2534Msg,
								unsigned long *pulMsgID,
								unsigned long ulTimeInterval);
	virtual J2534ERROR	vStopPeriodicMsg(unsigned long ulMsgID);

	virtual J2534ERROR	vStartMsgFilter(
								J2534_FILTER enFilterType,
								PASSTHRU_MSG *pstrucJ2534Mask,
								PASSTHRU_MSG *pstrucJ2534Pattern,
								PASSTHRU_MSG *pstrucJ2534FlowControl,
								unsigned long *pulFilterID);
	virtual J2534ERROR	vStopMsgFilter(unsigned long ulFilterID);

	virtual J2534ERROR	vIoctl(J2534IOCTLID enumIoctlID,
								void *pInput,
								void *pOutput);
	

	J2534ERROR				MessageValid(PASSTHRU_MSG	   *pstrucJ2534Msg,
										unsigned long  *pulNumMsgs);
	J2534ERROR				GetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				SetConfig(SCONFIG_LIST *pInput);
	J2534ERROR				DataRate(unsigned long ulValue);
	J2534ERROR				Loopback(unsigned long ulValue);
/*	J2534ERROR  vSetProgrammingVoltage(unsigned long ulPin,
										unsigned long ulVoltage);

	J2534ERROR				T1_Max(unsigned long ulValue),
							T2_Max(unsigned long ulValue),
							T3_Max(unsigned long ulValue),
							T4_Max(unsigned long ulValue),
							T5_Max(unsigned long ulValue);

	unsigned short			m_usT1MAX,
							m_usT2MAX,
							m_usT3MAX,
							m_usT4MAX,
							m_usT5MAX;*/

	bool				IsMsgValid(PASSTHRU_MSG *pstPassThruMsg);
private:
	PASSTHRU_MSG        structsci;
	unsigned long m_ucIOdata;
	J2534_PROTOCOL		m_enCCDProtocol;
};

#endif
