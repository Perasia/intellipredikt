// J2534Generic.h : main header file for the J2534GENERIC DLL
//

#if !defined(AFX_J2534GENERIC_H__351DDE25_907A_11D9_94E3_0040F6B43A4C__INCLUDED_)
#define AFX_J2534GENERIC_H__351DDE25_907A_11D9_94E3_0040F6B43A4C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "J2534Registry.h"

#define SMALL_TEXT_SIZE 80
#define DATALOGGING_CLOSE       0
#define DATALOGGING_STOP        1
#define DATALOGGING_PAUSE       2
#define DATALOGGING_START       3
#define DATALOGGING_NUM_EVENTS  4

/////////////////////////////////////////////////////////////////////////////
// CJ2534GenericApp
// See J2534Generic.cpp for the implementation of this class
//

class CJ2534GenericApp : public CWinApp
{
public:
	CJ2534GenericApp();
	~CJ2534GenericApp();
	J2534REGISTRY_ERROR_TYPE WriteToRegistry(
			J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);

	bool							m_bRegistryRead;
	bool							m_bMultiMediaTimerSet;
	J2534REGISTRY_CONFIGURATION		m_J2534Registry;
    HANDLE                          m_hDataLoggingEvent[DATALOGGING_NUM_EVENTS];
    unsigned long                   m_ulDataLoggingState;
    CString                         m_cszDataLogFile;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJ2534GenericApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CJ2534GenericApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_J2534GENERIC_H__351DDE25_907A_11D9_94E3_0040F6B43A4C__INCLUDED_)
