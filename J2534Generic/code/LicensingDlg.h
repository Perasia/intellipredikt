#pragma once
#include "afxwin.h"
#include "License.h"

// CLicensingDlg dialog

class CLicensingDlg : public CDialog
{
	DECLARE_DYNAMIC(CLicensingDlg)

public:
	CLicensingDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLicensingDlg();
    bool m_ShowDialog;

    // Members
    //License *m_License;
    CString m_cszEncryptedDeviceSN;

    // Members
    License *m_License;

    // Licensing related methods
    void InitializeLicense();
    void RefreshLicenseStatus();

    // License class callbacks
    static void RefreshLicenseStatusCallback();
    static void TerminateApplicationCallback();
    
    // Licensing related methods
    //void InitializeLicense();
    //void RefreshLicenseStatus();
    void TerminateApplication();

// Dialog Data
	enum { IDD = IDD_LICENSING_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CEdit txtLicenseID;
    CEdit txtPassword;
    CButton btnActivate;
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    CStatic lblInstructions;
    afx_msg void OnPaint();
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
