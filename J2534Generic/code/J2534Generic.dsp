# Microsoft Developer Studio Project File - Name="J2534Generic" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=J2534Generic - Win32 J2534_Netbridge_Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "J2534Generic.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "J2534Generic.mak" CFG="J2534Generic - Win32 J2534_Netbridge_Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "J2534Generic - Win32 J2534_DPA_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_DPA_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_CAYMAN_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_CAYMAN_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_VSI_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_VSI_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_KRHTWIRE_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_KRHTWIRE_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_BlueVCI_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_BlueVCI_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_PX3_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_PX3_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_DPA5_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_DPA5_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_SoftBridge_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_SoftBridge_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Wurth_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Wurth_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Delphi_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Delphi_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_UDTruck_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_UDTruck_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_dbriDGe_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_dbriDGe_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Netbridge_Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "J2534Generic - Win32 J2534_Netbridge_Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DEI2005003/J2534Generic/Src", SAGAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_DPA_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_DPA_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_DPA_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_DPA_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_DPA" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA4" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 DeviceDPA/Debug/DeviceDPA.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug/dgDPA32.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceDPA.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgDPA532.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=J2534Generic___Win32_J2534_DPA_Debug\dgDPA532.pdb .\Debug
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_DPA_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_DPA_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_DPA_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_DPA_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_DPA" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA4" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 DeviceDPA/Release/DeviceDPA.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release/dgDPA32.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceDPA.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgDPA532.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_CAYMAN_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_CAYMAN_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_CAYMAN_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_DPA" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_CARDONE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ../FlashWrite/Debug/DeviceDPA.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"../FlashWrite/Debug/dgDPA32.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceCayman.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgFlsh32.dll " /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\dgFlsh32.dll C:\Windows\System32	copy Debug\dgFlsh32.pdb C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_CAYMAN_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_CAYMAN_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_CAYMAN_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_DPA" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_CARDONE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 DeviceDPA/Release/DeviceDPA.lib /nologo /subsystem:windows /dll /machine:I386 /out:"J2534_DPA_Release/dgDPA32.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceCayman.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgFlsh32.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_VSI_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_VSI_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_VSI_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_VSI_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_VSI_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\Flash2Console\Src\Debug\DeviceCayman.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"..\..\Flash2Console\Src\Debug\Cayman32.dll " /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceVSI.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgVSI32.dll " /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy J2534Generic___Win32_J2534_VSI_Debug\dgVSI32.pdb .\Debug	copy .\Debug\dgVSI32.pdb \Windows\System32	copy .\Debug\dgVSI32.dll \Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_VSI_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_VSI_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_VSI_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_VSI_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_VSI_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\Flash2Console\Src\Release\DeviceCayman.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"..\..\Flash2Console\Src\Release\Cayman32.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceVSI.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /map /machine:I386 /out:"Release\dgVSI32.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_KRHTWIRE_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MDd /W3 /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_KRHTWIRE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceVSI.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgVSI32.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceKRHtWire.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /incremental:yes /debug /machine:I386 /out:"Debug\KRHtWire.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy J2534Generic___Win32_J2534_KRHTWIRE_Debug\KRHtWire.pdb "C:\Program Files\KeylessRide HotWire"	copy Debug\KRHtWire.dll "C:\Program Files\KeylessRide HotWire"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_KRHTWIRE_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_KRHTWIRE_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_KRHTWIRE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceVSI.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgVSI32.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceKRHtWire.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\KRHtWire.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_BlueVCI_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_BlueVCI_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_BlueVCI_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_BlueVCI_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_BlueVCI_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /D "J2534_DEVICE_KRHTWIRE" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MDd /W3 /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_BLUE_VCI" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceKRHtWire.lib version.lib /nologo /subsystem:windows /dll /incremental:yes /debug /machine:I386 /out:"Debug\KRHtWire.dll"
# ADD LINK32 ..\..\DeviceVCI\Debug\BlueVCI32.lib version.lib /nologo /subsystem:windows /dll /incremental:yes /debug /machine:I386 /out:"Debug\BlueVCI32.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=J2534Generic___Win32_J2534_BlueVCI_Debug\BlueVCI32.pdb .\Debug
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_BlueVCI_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_BlueVCI_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_BlueVCI_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_BlueVCI_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_BlueVCI_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_VSI" /D "J2534_DEVICE_KRHTWIRE" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_BLUE_VCI" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceKRHtWire.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\KRHtWire.dll"
# ADD LINK32 ..\..\DeviceVCI\Release\BlueVCI32.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\BlueVCI32.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_PX3_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_PX3_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_PX3_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_PX3_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_PX3_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX2" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DevicePx-2.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgPx-232.dll " /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgPx-3.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\dgPx-3.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_PX3_Debug\dgPx-3.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_PX3_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_PX3_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_PX3_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_PX3_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_PX3_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX2" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DevicePx-2.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgPx-232.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgPx-3.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA5_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_DPA5_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_DPA5_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_DPA5_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_DPA5_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgPx-3.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceDPA5.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgDPA532.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\dgDPA532.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_DPA5_Debug\dgDPA532.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA5_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_DPA5_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_DPA5_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_DPA5_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_DPA5_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgPx-3.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceDPA5.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgDPA532.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_SoftBridge_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_SoftBridge_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_SoftBridge_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_SoftBridge_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_SoftBridge_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgPx-3.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceSoftBridge.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\SoftBridge.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\SoftBridge.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_SoftBridge_Debug\SoftBridge.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_SoftBridge_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_SoftBridge_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_SoftBridge_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_SoftBridge_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_SoftBridge_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DevicePx-3.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgPx-3.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceSoftBridge.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\SoftBridge.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Wurth_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Wurth_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Wurth_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_Wurth_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Wurth_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceSoftBridge.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\SoftBridge.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceWurth.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\WoWFlashBox.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\WoWFlashBox.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_Wurth_Debug\WoWFlashBox.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Wurth_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Wurth_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Wurth_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_Wurth_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Wurth_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceSoftBridge.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\SoftBridge.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceWurth.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\WoWFlashBox.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Delphi_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Delphi_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Delphi_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_Delphi_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Delphi_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DELPHI" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceSoftBridge.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\SoftBridge.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceDelphi.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\DelphiEuro5.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\DelphiEuro5.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_Delphi_Debug\DelphiEuro5.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Delphi_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Delphi_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Delphi_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_Delphi_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Delphi_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DELPHI" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceSoftBridge.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\SoftBridge.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceDelphi.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\DelphiEuro5.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_UDTruck_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_UDTruck_Debug0"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_UDTruck_Debug0"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_UDTruck_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_UDTruck_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE0305" /D "J2534_0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceWurth.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\WoWFlashBox.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DevicedbriDGe0305.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dbriDGe0305.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - UDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\dbriDGe0305.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_UDTruck_Debug\dbriDGe0305.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_UDTruck_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_UDTruck_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_UDTruck_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_UDTruck_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_UDTruck_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE0305" /D "J2534_0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceWurth.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\WoWFlashBox.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DevicedbriDGe0305.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dbriDGe0305.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - UDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_dbriDGe_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_dbriDGe_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_dbriDGe_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_dbriDGe_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_dbriDGe_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE0305" /D "J2534_0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceWurth.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dbriDGe0305.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DevicedbriDGe.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dbridge.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\dbriDGe.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_dbriDGe_Debug\dbriDGe.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_dbriDGe_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_dbriDGe_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_dbriDGe_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_dbriDGe_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_dbriDGe_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE0305" /D "J2534_0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceWurth.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dbriDGe0305.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DevicedbriDGe.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dbridge.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Netbridge_Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Netbridge_Debug"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Netbridge_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "J2534Generic___Win32_J2534_Netbridge_Debug"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Netbridge_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_NETBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Debug\DeviceDPA5.lib version.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\dgDPA532.dll" /pdbtype:sept
# ADD LINK32 ..\..\DeviceCayman\code\Debug\DeviceNetbridge.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug\nbridgej.dll" /pdbtype:sept
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
PostBuild_Desc=copy Debug files
PostBuild_Cmds=copy Debug\nbridgej.dll C:\Windows\System32	copy J2534Generic___Win32_J2534_Netbridge_Debug\nbridgej.pdb  C:\Windows\System32
# End Special Build Tool

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Netbridge_Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "J2534Generic___Win32_J2534_Netbridge_Release"
# PROP BASE Intermediate_Dir "J2534Generic___Win32_J2534_Netbridge_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "J2534Generic___Win32_J2534_Netbridge_Release"
# PROP Intermediate_Dir "J2534Generic___Win32_J2534_Netbridge_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /D "J2534_DEVICE_CAYMAN" /D "J2534_DEVICE_NETBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\..\DeviceCayman\code\Release\DeviceDPA5.lib version.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\dgDPA532.dll"
# ADD LINK32 ..\..\DeviceCayman\code\Release\DeviceNetbridge.lib version.lib Winmm.lib /nologo /subsystem:windows /dll /machine:I386 /out:"Release\nbridgej.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PreLink_Desc=Copy LINK definition file
PreLink_Cmds=copy "J2534Generic - NonUDTruck.def" J2534Generic.def
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "J2534Generic - Win32 J2534_DPA_Debug"
# Name "J2534Generic - Win32 J2534_DPA_Release"
# Name "J2534Generic - Win32 J2534_CAYMAN_Debug"
# Name "J2534Generic - Win32 J2534_CAYMAN_Release"
# Name "J2534Generic - Win32 J2534_VSI_Debug"
# Name "J2534Generic - Win32 J2534_VSI_Release"
# Name "J2534Generic - Win32 J2534_KRHTWIRE_Debug"
# Name "J2534Generic - Win32 J2534_KRHTWIRE_Release"
# Name "J2534Generic - Win32 J2534_BlueVCI_Debug"
# Name "J2534Generic - Win32 J2534_BlueVCI_Release"
# Name "J2534Generic - Win32 J2534_PX3_Debug"
# Name "J2534Generic - Win32 J2534_PX3_Release"
# Name "J2534Generic - Win32 J2534_DPA5_Debug"
# Name "J2534Generic - Win32 J2534_DPA5_Release"
# Name "J2534Generic - Win32 J2534_SoftBridge_Debug"
# Name "J2534Generic - Win32 J2534_SoftBridge_Release"
# Name "J2534Generic - Win32 J2534_Wurth_Debug"
# Name "J2534Generic - Win32 J2534_Wurth_Release"
# Name "J2534Generic - Win32 J2534_Delphi_Debug"
# Name "J2534Generic - Win32 J2534_Delphi_Release"
# Name "J2534Generic - Win32 J2534_UDTruck_Debug"
# Name "J2534Generic - Win32 J2534_UDTruck_Release"
# Name "J2534Generic - Win32 J2534_dbriDGe_Debug"
# Name "J2534Generic - Win32 J2534_dbriDGe_Release"
# Name "J2534Generic - Win32 J2534_Netbridge_Debug"
# Name "J2534Generic - Win32 J2534_Netbridge_Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CAN.cpp
# End Source File
# Begin Source File

SOURCE=.\CCD.cpp
# End Source File
# Begin Source File

SOURCE=.\CircBuffer.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugLog.cpp
# End Source File
# Begin Source File

SOURCE=.\FilterMsg.cpp
# End Source File
# Begin Source File

SOURCE=.\GMUART.cpp
# End Source File
# Begin Source File

SOURCE=.\ISO14230.cpp
# End Source File
# Begin Source File

SOURCE=.\ISO15765.cpp
# End Source File
# Begin Source File

SOURCE=.\ISO9141.cpp
# End Source File
# Begin Source File

SOURCE=.\J1708.cpp
# End Source File
# Begin Source File

SOURCE=.\J1850.cpp
# End Source File
# Begin Source File

SOURCE=.\J1850PWM.cpp
# End Source File
# Begin Source File

SOURCE=.\J1939.cpp
# End Source File
# Begin Source File

SOURCE=.\J2534Generic.cpp
# End Source File
# Begin Source File

SOURCE=.\J2534Generic.def
# End Source File
# Begin Source File

SOURCE=.\J2534Main.cpp
# End Source File
# Begin Source File

SOURCE=.\J2818.cpp
# End Source File
# Begin Source File

SOURCE=.\PeriodicMsg.cpp
# End Source File
# Begin Source File

SOURCE=.\ProtocolBase.cpp
# End Source File
# Begin Source File

SOURCE=.\ProtocolManager.cpp
# End Source File
# Begin Source File

SOURCE=.\SCI.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SWCan.cpp
# End Source File
# Begin Source File

SOURCE=.\SWISO15765.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CAN.h
# End Source File
# Begin Source File

SOURCE=.\CCD.h
# End Source File
# Begin Source File

SOURCE=.\CircBuffer.h
# End Source File
# Begin Source File

SOURCE=.\DebugLog.h
# End Source File
# Begin Source File

SOURCE=.\FilterMsg.h
# End Source File
# Begin Source File

SOURCE=.\GMUART.h
# End Source File
# Begin Source File

SOURCE=.\ISO14230.h
# End Source File
# Begin Source File

SOURCE=.\ISO15765.h
# End Source File
# Begin Source File

SOURCE=.\ISO9141.h
# End Source File
# Begin Source File

SOURCE=.\J1708.h
# End Source File
# Begin Source File

SOURCE=.\J1850.h
# End Source File
# Begin Source File

SOURCE=.\J1850PWM.h
# End Source File
# Begin Source File

SOURCE=.\J1939.h
# End Source File
# Begin Source File

SOURCE=.\J2534.h
# End Source File
# Begin Source File

SOURCE=.\J2534_0305.h
# End Source File
# Begin Source File

SOURCE=.\J2534Generic.h
# End Source File
# Begin Source File

SOURCE=.\J2534Main.h
# End Source File
# Begin Source File

SOURCE=.\J2534Registry.cpp
# End Source File
# Begin Source File

SOURCE=.\J2534Registry.h
# End Source File
# Begin Source File

SOURCE=.\J2818.h
# End Source File
# Begin Source File

SOURCE=.\PeriodicMsg.h
# End Source File
# Begin Source File

SOURCE=.\ProtocolBase.h
# End Source File
# Begin Source File

SOURCE=.\ProtocolManager.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SCI.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SWCan.h
# End Source File
# Begin Source File

SOURCE=.\SWISO15765.h
# End Source File
# Begin Source File

SOURCE=.\VersionNo.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\J2534Generic.rc

!IF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA_Debug"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA_Release"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_CAYMAN_Debug"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_CARDONE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_CAYMAN_Release"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_CARDONE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_VSI_Debug"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_VSI"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_VSI_Release"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_VSI"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_KRHTWIRE_Debug"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_KRHTWIRE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_KRHTWIRE_Release"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_KRHTWIRE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_BlueVCI_Debug"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_BlueVCI_Release"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_PX3_Debug"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_PX3"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_PX3_Release"

# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /d "J2534_DEVICE_PX3"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA5_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5_EURO5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DPA5"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_DPA5_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5_EURO5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DPA5"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_SoftBridge_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5_EURO5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_SoftBridge_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5_EURO5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Wurth_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_WURTH"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Wurth_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_WURTH"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Delphi_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DELPHI"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Delphi_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DELPHI"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_UDTruck_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_UDTruck_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_dbriDGe_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_dbriDGe_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_SOFTBRIDGE"
# ADD RSC /l 0x409 /d "J2534_DEVICE_DBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Netbridge_Debug"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_NETBRIDGE"

!ELSEIF  "$(CFG)" == "J2534Generic - Win32 J2534_Netbridge_Release"

# ADD BASE RSC /l 0x409 /d "J2534_DEVICE_DPA5"
# ADD RSC /l 0x409 /d "J2534_DEVICE_NETBRIDGE"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\res\J2534Generic.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
