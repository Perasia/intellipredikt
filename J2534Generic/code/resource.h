//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by J2534Generic.rc
//
#define IDD_LICENSING_DIALOG            9
#define IDC_STATIC2                     1007
#define IDC_STATIC3                     1008
#define IDC_EDIT_LICENSEID              1009
#define IDC_EDIT_PASSWORD               1010
#define IDC_STATIC_INSTRUCTIONS         1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1004
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
