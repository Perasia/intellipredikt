#pragma once
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif

#ifdef J2534_0404
#include "J2534.h"
#endif

#ifdef J2534_0305
#include "J2534_0305.h"
#endif

#include "DebugLog.h"
#include "..\..\DeviceCayman\code\DeviceCayman.h"
#include "PeriodicMsg.h"
#include "FilterMsg.h"

#include "vector"
#ifdef J2534_0500

typedef struct
{
	unsigned long ulLogChannelId;
	unsigned long ulFilterId;
	unsigned long ulPeriodicId;
	unsigned long ulBlockSize, ulBlockSizeTx,
		ulSTmin, ulSTminTx, ulWftMax,
		ulN_BR_MIN,ulISO15765_PAD_VALUE,
		ulN_AS_MAX,ulN_AR_MAX,
		ulN_BS_MAX,ulN_CR_MAX,ulN_CS_MIN;
	std::vector <PASSTHRU_MSG*> TxQueue;
	std::vector <PASSTHRU_MSG*> RxQueue;
	ISO15765_CHANNEL_DESCRIPTOR structChan;
}LOGCHANNELLIST;

class CLogicalChannelList
{
public:
	CLogicalChannelList(CDeviceBase *pclsDevice , unsigned long	ulDevChannelID, CPeriodicMsg *pclsPeriodicMsg,
						 CDebugLog * pclsDebugLog = NULL);
	~CLogicalChannelList();

	J2534ERROR CreateLogicalChannel(void*, J2534_PROTOCOL enProtocolID, unsigned long*);
	J2534ERROR DisconnectLogicalChannel(unsigned long);
	J2534ERROR StopFilter();
	J2534ERROR StartPerioidic(PASSTHRU_MSG	*pstMsg,
										unsigned long	*pulPeriodicID,
										unsigned long	ulTimeInterval,
										unsigned long   ulLogChannelId);

	J2534ERROR StopPeriodic  (unsigned long pulRepeatID, unsigned long);

	void       WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);
	bool	   IsMessageAllowedOnLogChannel(PASSTHRU_MSG *pstrucMsg,bool, unsigned long);
	J2534ERROR GetRxCountOnLogicalChannel(unsigned long ulChannelId, unsigned long* pulNumMsgs);
	J2534ERROR ReadFromLogicalChannel(unsigned long ulChannelId, unsigned long* pulNumMsgs,PASSTHRU_MSG *pucMsgs);
	J2534ERROR AddMsgsToLogicalChannelQueue(PASSTHRU_MSG	*pstMsg, bool bTxRx);
	J2534ERROR vIoctl(unsigned long ulChannelId, J2534IOCTLID enumIoctlID, void *pInput, void *pOutput);

	bool				IsLogChannelValid(unsigned long);

	LOGCHANNELLIST		m_stLogChannelList[MAX_LOG_PER_PHYSICAL_ID];
	J2534ERROR			ClearRxQueue(unsigned long);

private:

	CDebugLog			*m_pclsLog;
	CDeviceBase			*m_pclsDevice;
	CPeriodicMsg        *m_pclsPeriodicMsg;
	CFilterMsg          *m_pclsFilterMsg;
	unsigned long		 m_ulDevChannelID;

	J2534ERROR			Add(PASSTHRU_MSG *pstPattern,
							PASSTHRU_MSG *pstFlowControl,
							unsigned long ulDevFilterID,
							unsigned long *puChannelId);

	J2534ERROR          Delete(unsigned long ulChannelId);

	unsigned char		GetNewChannelID();
	bool				IsLogicalChannelUnique(PASSTHRU_MSG *pstPattern,
											   PASSTHRU_MSG *pstFlowControl);
	bool				IsListFull();
	J2534ERROR              GetConfig(SCONFIG_LIST *pInput, unsigned long);
	J2534ERROR              SetConfig(SCONFIG_LIST *pInput, unsigned long);
	J2534ERROR              Loopback(unsigned long ulValue);
	J2534ERROR              BlockSize(unsigned long ulValue, unsigned long);
	J2534ERROR              BlockSizeTx(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR              STmin(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR              STminTx(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR              WftMax(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR				NasMAX(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR				NarMAX(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR				NbsMAX(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR			    NcsMIN(unsigned long ulValue, unsigned long ulChannelID);
	J2534ERROR              NcrMAX(unsigned long ulValue, unsigned long ulChannelID);
};

#endif


