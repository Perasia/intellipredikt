/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1939.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the declaration of 
*              J1939 Class.
* Note:
*
*******************************************************************************/

#ifndef _J1939_H_
#define _J1939_H_

#define J1939_ERROR_TEXT_SIZE       120

#define J1939_MSG_SIZE_MIN      5
#ifdef UD_TRUCK
#define ISOCAN_DATA_RATE_DEFAULT    250000
#else
#define ISOCAN_DATA_RATE_DEFAULT    500000
#endif

#define J1939_HEADER_SIZE                   11
#define J1939_EXTENDED_HEADER_SIZE          29 

#define J1939_MSG_SIZE_MAX_SF   13
#define J1939_MSG_SIZE_MAX      1790
#define ISOCAN_DATA_RATE_MEDIUM     250000
#define ISOCAN_DATA_RATE_LOW        125000
#define J1939_PROTECT_ADDRESS_MAX       10

#include "ProtocolBase.h"
#include "CircBuffer.h"
#include "FilterMsg.h"
#include "PeriodicMsg.h"

void OnJ1939RxMessage(PASSTHRU_MSG *pstPassThruMsg, 
                            LPVOID pVoid);

// CJ1939 Class
class CJ1939 : public CProtocolBase
{
public:
    CJ1939(CDeviceBase *pclsDevice, CDebugLog * pclsDebugLog = NULL, CDataLog * pclsDataLog=NULL);
    ~CJ1939();

    virtual J2534ERROR  vConnect(
                                J2534_PROTOCOL enProtocolID,
                                unsigned long ulFlags,
                                unsigned long ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback=NULL,
                                LPVOID        pVoid=NULL);
    virtual J2534ERROR  vDisconnect();
    virtual J2534ERROR  vReadMsgs(
                                PASSTHRU_MSG  *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vWriteMsgs(
                                PASSTHRU_MSG *pstrucJ2534Msg, 
                                unsigned long *pulNumMsgs, 
                                unsigned long ulTimeout);
    virtual J2534ERROR  vStartPeriodicMsg(
                                PASSTHRU_MSG *pstrucJ2534Msg,
                                unsigned long *pulMsgID,
                                unsigned long ulTimeInterval);
    virtual J2534ERROR  vStopPeriodicMsg(unsigned long ulMsgID);

    virtual J2534ERROR  vStartMsgFilter(
                                J2534_FILTER enFilterType,
                                PASSTHRU_MSG *pstrucJ2534Mask,
                                PASSTHRU_MSG *pstrucJ2534Pattern,
                                PASSTHRU_MSG *pstrucJ2534FlowControl,
                                unsigned long *pulFilterID);
    virtual J2534ERROR  vStopMsgFilter(unsigned long ulFilterID);

    virtual J2534ERROR  vIoctl(J2534IOCTLID enumIoctlID,
                                void *pInput,
                                void *pOutput);

    bool                IsMsgValid(PASSTHRU_MSG *pstPassThruMsg, bool bFilter = false);
    bool                IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg);

public:
    J2534ERROR              GetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              SetConfig(SCONFIG_LIST *pInput);
    J2534ERROR              DataRate(unsigned long ulValue);
    J2534ERROR              Loopback(unsigned long ulValue);
    J2534ERROR              T1(unsigned long ulValue);
    J2534ERROR              T2(unsigned long ulValue);
    J2534ERROR              T3(unsigned long ulValue);
    J2534ERROR              T4(unsigned long ulValue);
    J2534ERROR              Brdcst_min_delay(unsigned long ulValue);
    J2534ERROR				SetInternalTermination(unsigned char *pucValue); 
	J2534ERROR		        J1962Pins(unsigned long ulValue);    
	J2534ERROR		        J1939Pins(unsigned long ulValue);

    float                   m_fSamplePoint;
    float                   m_fJumpWidth;
    unsigned long           m_ulCANIDtype;
    unsigned long           m_ulCANExtAddr;
    J2534_PROTOCOL          m_enJ1939Protocol;
    unsigned long           m_ulT1, 
                            m_ulT2, 
                            m_ulT3, 
                            m_ulT4, 
                            m_ulBrdcst_min_delay;
    unsigned char           m_ucProtectedJ1939Addr[J1939_PROTECT_ADDRESS_MAX];
    bool                    m_bProtectedJ1939Addr[J1939_PROTECT_ADDRESS_MAX];
	unsigned long			m_ulPPSS;
	bool					m_bJ1962Pins;                                    
	unsigned long			m_ulJ1939PPSS;
	bool					m_bJ1939Pins;
};

#endif
