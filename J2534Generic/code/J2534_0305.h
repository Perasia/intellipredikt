/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534_0305.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This is the main header file that contains the 
*              datatypes and declarations for the J2534 APIs.
* Note:
*
*******************************************************************************/

#ifndef _J2534_H_
#define _J2534_H_

#define PASSTHRU_MSG_DATA_SIZE          4128

// J2534 Message Structure
typedef struct
{
    unsigned long ulProtocolID;
    unsigned long ulRxStatus;
    unsigned long ulTxFlags;
    unsigned long ulTimeStamp;
    unsigned long ulDataSize;
    unsigned long ulExtraDataIndex;
    unsigned char ucData[PASSTHRU_MSG_DATA_SIZE];
}
PASSTHRU_MSG;

#define J2534_TX_MSG_SIZE               sizeof(PASSTHRU_MSG)
#define J2534_RX_MSG_SIZE               sizeof(PASSTHRU_MSG)

// Connect Flag Bits
#define J2534_CONNECT_FLAGBIT_CAN29BIT  0x00000100 // Bit 8
#define J2534_CONNECT_FLAGBIT_NOCHKSUM  0x00000200 // Bit 9
#define J2534_CONNECT_FLAGBIT_CANBOTH   0x00000800 // Bit 11
#define J2534_CONNECT_FLAGBIT_KONLY     0x00001000 // Bit 12

// Tx Flag Bits
#define J2534_TX_FLAGBIT_CAN29BIT       0x00000100 // Bit 8

// Rx Flag Bits
#define J2534_RX_FLAGBIT_MSGTYPE        0x00000001 // Bit 0
#define J2534_RX_FLAGBIT_CAN29BIT       0x00000100 // Bit 0 
#define J2534_RX_LINK_FAULT             0x00020000
#define J2534_RX_FLAGBIT_ISO15765_TIMEOUT 0x80000000

// Protocols supported
typedef enum
{
    J1850VPW = 0x01,                    // J1850VPW Protocol
    J1850PWM,                           // J1850PWM Protocol
    ISO9141,                            // ISO9141 Protocol
    ISO14230,                           // ISO14230 Protocol
    CAN,                                // CAN Protocol
    ISO15765,
    SCI_A_ENGINE,
    SCI_A_TRANS,
    SCI_B_ENGINE,
    SCI_B_TRANS,
    ISO9141_PS = 0x8002,
    ISO14230_PS = 0x8003,
    CAN_PS,
    ISO15765_PS,
    SW_ISO15765_PS = 0x8007,
    SW_CAN_PS,
    GM_UART,
    UART_ECHO_BYTE_PS = 0x800A,         
    HONDA_DIAGH_PS = 0x800B,
    J1939_PS = 0x800c,
    J1708_PS,     
    TP2_0_PS,  
    FT_CAN_PS = 0x800f,
    FT_ISO15765_PS,
    CAN_CH1 = 0x9000,
    CAN_CH2,
    CAN_CH3,
    CAN_CH4,
    ISO15765_CH1 = 0x9400,
    ISO15765_CH2,
    ISO15765_CH3,
    ISO15765_CH4,
    SW_CAN_CAN_CH1 = 0x9480,
    SW_CAN_ISO15765_CH1 = 0x9560,     
    FT_CAN_CH1 = 0x9720,
	FT_ISO15765_CH1 = 0x9800,
    J1939_CH1 = 0x97a0,                 // Use 'Reserved' Protocol ID for now
    J1939_CH2,                          // Use 'Reserved' Protocol ID for now
    J1939_CH3,                          // Use 'Reserved' Protocol ID for now
    J1939_CH4,                          // Use 'Reserved' Protocol ID for now    
    J1708_CH1 = 0x9780, 
    TP2_0_CH1 = 0x9800,   
    TP2_0_CH2,
    TP2_0_CH3,
    TP2_0_CH4,
    CCD = 0x10000,
    LIN = 0x10101
} 
J2534_PROTOCOL;

struct ProtocolEntry
{
    J2534_PROTOCOL J2534ProtocolID;
    char *Description;
};

const struct ProtocolEntry Protocols[] = {
    {J1850VPW,      "J1850VPW"},
    {J1850PWM,      "J1850PWM"},
    {ISO9141,       "ISO9141"},
    {ISO14230,      "ISO14230"},
    {CAN,           "CAN"},
    {ISO15765,      "ISO15765"},
    {SCI_A_ENGINE,  "SCI_A_ENGINE"},
    {SCI_A_TRANS,   "SCI_A_TRANS"},
    {SCI_B_ENGINE,  "SCI_B_ENGINE"},
    {SCI_B_TRANS,   "SCI_B_TRANS"},
    {SW_ISO15765_PS,"SW_ISO15765_PS"},
    {SW_CAN_PS,     "SW_CAN_PS"},
    {GM_UART,       "GM_UART_PS"},     
    {ISO9141_PS,    "ISO9141_PS"},
    {UART_ECHO_BYTE_PS,"UART_ECHO_BYTE_PS"}, 
    {HONDA_DIAGH_PS,"HONDA_DIAGH_PS"},
    {J1939_PS,      "J1939_PS"},
    {J1939_CH1,     "J1939_CH1"},
    {J1939_CH2,     "J1939_CH2"},
    {J1939_CH3,     "J1939_CH3"},
    {J1939_CH4,     "J1939_CH4"},
    {J1708_PS,      "J1708_PS"},
    {J1708_CH1,     "J1708_CH1"},
    {CCD,           "CCD"},
    {CAN_PS,        "CAN_PS"},
    {ISO15765_PS,   "ISO15765_PS"},
    {FT_ISO15765_PS, "FT_ISO15765_PS"},
    {FT_CAN_PS,      "FT_CAN_PS"},
    {FT_ISO15765_CH1, "FT_ISO15765_CH1"},
    {FT_CAN_CH1,     "FT_CAN_CH1"}, 
    {SW_CAN_ISO15765_CH1, "SW_CAN_ISO15765_CH1"},
    {SW_CAN_CAN_CH1,    "SW_CAN_CAN_CH1"},
    {CAN_CH1,        "CAN_CH1"},
    {CAN_CH2,        "CAN_CH2"},
    {CAN_CH3,        "CAN_CH3"},
    {CAN_CH4,        "CAN_CH4"},
    {ISO15765_CH1,   "ISO15765_CH1"},
    {ISO15765_CH2,   "ISO15765_CH2"},
    {ISO15765_CH3,   "ISO15765_CH3"},
    {ISO15765_CH4,   "ISO15765_CH4"},   
    {TP2_0_PS,      "TP2_0_PS"},
    {TP2_0_CH1,     "TP2_0_CH1"},
    {TP2_0_CH2,     "TP2_0_CH2"},
    {TP2_0_CH3,     "TP2_0_CH3"},
    {TP2_0_CH4,     "TP2_0_CH4"},
    {LIN,            "LIN"},
};

#define PROTOCOL_ENTRY_COUNT sizeof(Protocols)/sizeof(ProtocolEntry)

// J2534 Error codes
typedef enum
{
    J2534_STATUS_NOERROR=0,             // Function call successful.
    J2534_ERR_NOT_SUPPORTED,            // Function not supported.
    J2534_ERR_INVALID_CHANNEL_ID,       // Invalid ChannelID value.
    J2534_ERR_INVALID_PROTOCOL_ID,      // Invalid ProtocolID value.
    J2534_ERR_NULLPARAMETER,            // NULL pointer supplied where a valid 
                                        // pointer
                                        // is required.
    J2534_ERR_INVALID_IOCTL_VALUE,      // Invalid value for Ioctl parameter 
    J2534_ERR_INVALID_FLAGS,            // Invalid flag values.
    J2534_ERR_FAILED,                   // Undefined error, use 
                                        // PassThruGetLastError for description
                                        // of error.
//  J2534_ERR_INVALID_DEVICE_ID,        // Device not connected to PC                                       
    J2534_ERR_DEVICE_NOT_CONNECTED,     // Device not connected to PC
    J2534_ERR_TIMEOUT,                  // Timeout. No message available to 
                                        // read or 
                                        // could not read the specified no. of 
                                        // msgs.
    J2534_ERR_INVALID_MSG,              // Invalid message structure pointed 
                                        // to by pMsg.
    J2534_ERR_INVALID_TIME_INTERVAL,    // Invalid TimeInterval value.
    J2534_ERR_EXCEEDED_LIMIT,           // ALL periodic message IDs have been 
                                        // used.
    J2534_ERR_INVALID_MSG_ID,           // Invalid MsgID value.
//  J2534_ERR_DEVICE_IN_USE,            // Device already open and/or in use
    J2534_ERR_ERROR_ID,                 // Invalid ErrorID value
    J2534_ERR_INVALID_IOCTL_ID,         // Invalid IoctlID value.
    J2534_ERR_BUFFER_EMPTY,             // Protocol message buffer empty.
    J2534_ERR_BUFFER_FULL,              // Protocol message buffer full.
    J2534_ERR_BUFFER_OVERFLOW,          // Protocol message buffer overflow.
    J2534_ERR_PIN_INVALID,              // Invalid pin number.
    J2534_ERR_CHANNEL_IN_USE,           // Channel already in use.
    J2534_ERR_MSG_PROTOCOL_ID,          // Protocol type does not match the
                                        // protocol associated with the 
                                        // Channel ID
    J2534_ERR_INVALID_FILTER_ID,        // Invalid MsgID value
    J2534_ERR_NO_FLOW_CONTROL,          // An attempt was made to send a message
                                        // on an ISO15765 ChannelID before a flow
                                        // control filter was established.  
//  J2534_ERR_NOT_UNIQUE,               // A CAN ID in PatternMsg or FlowControlMsg
                                        // matches either ID in an existing Flow Control Filter
    J2534_ERR_INVALID_BAUDRATE,         // Desired baud rate cannot be achieved within tolerances           
//  J2534_ERR_DEVICE_NOT_CONNECTED  = 0x20// Device not connected
    J2534_ERR_ADDRESS_NOT_CLAIMED = 0x10000,
    J2534_ERR_NO_CONNECTION_ESTABLISHED
}   
J2534ERROR;

// Filter Types
// Filter Message Error codes
typedef enum
{
    J2534_FILTER_NONE,
    J2534_FILTER_PASS,
    J2534_FILTER_BLOCK,
    J2534_FILTER_FLOW_CONTROL
} 
J2534_FILTER;

// CAN Mixed Mode Types
typedef enum
{
    J2534_CAN_MIXED_FORMAT_OFF,
    J2534_CAN_MIXED_FORMAT_ON,
    J2534_CAN_MIXED_FORMAT_ALL_FRAMES
} 
J2534_CAN_MIXED_FORMAT;

// Ioctl ID Values
typedef enum
{
    GET_CONFIG = 0x01,
    SET_CONFIG,
    READ_VBATT,
    FIVE_BAUD_INIT,
    FAST_INIT,
    SET_PIN_USE,
    CLEAR_TX_BUFFER,
    CLEAR_RX_BUFFER,
    CLEAR_PERIODIC_MSGS,
    CLEAR_MSG_FILTERS,
    CLEAR_FUNCT_MSG_LOOKUP_TABLE,
    ADD_TO_FUNCT_MSG_LOOKUP_TABLE,
    DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE,
    READ_PROG_VOLTAGE,
    SW_CAN_HS = 0x8000,
    SW_CAN_NS,
    SET_POLL_RESPONSE,
    BECOME_MASTER,
    PROTECT_J1939_ADDR = 0x8009,     
    REQUEST_CONNECTION,
    TEARDOWN_CONNECTION,
    GET_DEVICE_INFO = 0x800C,
    GET_PROTOCOL_INFO = 0x800D,
    READ_MEMORY = 0x10000,
    WRITE_MEMORY =0x10001,
    READ_VWALLJACK,
    READ_VPWRVEH,
    TURN_AUX_OFF,
    TURN_AUX_ON,
    START_PULSE,
    READ_VIGNITION,
    CAN_SET_BTR = 0x10100,
	GET_TIMESTAMP,
	SET_TIMESTAMP,
	GET_SERIAL_NUMBER,
	CAN_SET_LISTEN_MODE,
    CAN_SET_ERROR_REPORTING,
    CAN_SET_INTERNAL_TERMINATION = 0x10107,
    DEVICE_RESET = 0x10200
}
J2534IOCTLID;

// Ioctl Parameter ID Values
typedef enum
{
    DATA_RATE = 0x01,
    LOOPBACK = 0x03,
    NODE_ADDRESS,
    NETWORK_LINE,
    P1_MIN,
    P1_MAX,
    P2_MIN,
    P2_MAX,
    P3_MIN,
    P3_MAX,
    P4_MIN,
    P4_MAX,
    W1,
    W2,
    W3,
    W4,
    W5,
    TIDLE,
    TINIL,
    TWUP,
    PARITY,
    BIT_SAMPLE_POINT,
    SYNC_JUMP_WIDTH,
    W0,
    T1_MAX,
    T2_MAX,
    T4_MAX,
    T5_MAX,
    ISO15765_BS,
    ISO15765_STMIN,
    DATA_BITS,
    FIVE_BAUD_MOD,
    BS_TX,
    STMIN_TX,
    T3_MAX,
    ISO15765_WFT_MAX,
    CAN_MIXED_FORMAT = 0x8000,
    J1962_PINS = 0x8001,
    SWCAN_HS_DATA_RATE = 0x8010,
    SWCAN_SPEEDCHANGE_ENABLE,
    SWCAN_RES_SWITCH, 
    UEB_T0_MIN = 0x8028,
    UEB_T1_MAX,
    UEB_T2_MAX,
    UEB_T3_MAX,
    UEB_T4_MIN,
    UEB_T5_MAX,
    UEB_T6_MAX,
    UEB_T7_MIN,
    UEB_T7_MAX,
    UEB_T9_MIN,
    J1939_PINS = 0x803d,
    J1708_PINS,
    J1939_T1 = 0x803f,
    J1939_T2,
    J1939_T3,
    J1939_T4,
    J1939_BRDCST_MIN_DELAY,     
    TP2_0_T_BR_INT,
    TP2_0_T_E,
    TP2_0_MNTC,
    TP2_0_T_CTA,
    TP2_0_MNCT,
    TP2_0_MNTB,
    TP2_0_MNT,
    TP2_0_T_WAIT,
    TP2_0_T1,
    TP2_0_T3,
    TP2_0_IDENTIFIER,
    TP2_0_RXIDPASSIVE,
    LIN_VERSION = 0x10000
}
J2534IOCTLPARAMID;

// SPARAM Structure
typedef struct
{
    unsigned long ulParameter;          // name of parameter
    unsigned long ulValue;              // value of the parameter
    unsigned long ulSupported;   // support for parameter
}
SPARAM;

// SPARAM_LIST Structure
typedef struct
{
    unsigned long ulNumOfParams;        // number of SPARAM elements
    SPARAM *pParamPtr;                  // array of SPARAM
}
SPARAM_LIST;

// SCONFIG Structure
typedef struct
{
    J2534IOCTLPARAMID Parameter;        // name of parameter
    unsigned long ulValue;              // value of the parameter
}
SCONFIG;

// SCONFIG_LIST Structure
typedef struct
{
    unsigned long ulNumOfParams;        // number of SCONFIG elements
    SCONFIG *pConfigPtr;                // array of SCONFIG
}
SCONFIG_LIST;

// SBYTE_ARRAY Structure
typedef struct
{
    unsigned long ulNumOfBytes;         // number of bytes in the array
    unsigned char *pucBytePtr;          // array of bytes
}
SBYTE_ARRAY;

//SMEMORY_READ
typedef struct
{
    unsigned long ulFunctionCode;       //
    unsigned long ulNumOfBytes;         // number of bytes in the array
    unsigned char *pBytePtr;            // array of bytes
}
SMEMORY_READ;

//SMEMORY_WRITE
typedef struct
{
    char *pszPasscode;
    unsigned long ulFunctionCode;       //
    unsigned long ulNumOfBytes;         // number of bytes in the array
    unsigned char *pBytePtr;            // array of bytes
}
SMEMORY_WRITE;

// Declaration of Exported functions.
//extern "C" J2534ERROR WINAPI PassThruOpen(void *pName, unsigned long *pulDeviceID);

//extern "C" J2534ERROR WINAPI PassThruClose(unsigned long ulDeviceID);

extern "C" J2534ERROR WINAPI PassThruConnect(
//                                      unsigned long ulDeviceID,
                                        J2534_PROTOCOL enumProtocolID,
                                        unsigned long ulFlags,
//                                      unsigned long ulBaudRate,
                                        unsigned long *pulChannelID);
extern "C" J2534ERROR WINAPI PassThruDisconnect(
                                        unsigned long ulChannelID);
extern "C" J2534ERROR WINAPI PassThruReadMsgs(
                                        unsigned long ulChannelID,
                                        PASSTHRU_MSG       *pstrucJ2534Msg,
                                        unsigned long *pulNumMsgs,
                                        unsigned long ulTimeout);
extern "C" J2534ERROR WINAPI PassThruWriteMsgs(
                                        unsigned long ulChannelID,
                                        PASSTHRU_MSG       *pstrucJ2534Msg,
                                        unsigned long *pulNumMsgs,
                                        unsigned long ulTimeout);
extern "C" J2534ERROR WINAPI PassThruStartPeriodicMsg(
                                        unsigned long ulChannelID, 
                                        PASSTHRU_MSG *pMsgJ2534, 
                                        unsigned long *pulMsgID, 
                                        unsigned long ulTimeout);
extern "C" J2534ERROR WINAPI PassThruStopPeriodicMsg(
                                        unsigned long ulChannelID,
                                        unsigned long ulMsgID);
extern "C" J2534ERROR WINAPI PassThruStartMsgFilter(
                                        unsigned long   ulChannelID,
                                        J2534_FILTER    enumFilterType,
                                        PASSTHRU_MSG        *pstrucJ2534Mask,
                                        PASSTHRU_MSG        *pstrucJ2534Pattern,
                                        PASSTHRU_MSG        *pstrucJ2534FlowControl,
                                        unsigned long *pulFilterID);
extern "C" J2534ERROR WINAPI PassThruStopMsgFilter(
                                        unsigned long ulChannelID,
                                        unsigned long ulFilterID);
extern "C" J2534ERROR WINAPI PassThruSetProgrammingVoltage(
//                                      unsigned long ulDeviceID,
                                        unsigned long ulPin,
                                        unsigned long ulVoltage);
extern "C" J2534ERROR WINAPI PassThruReadVersion(
#if !defined(J2534_0305)
                                         unsigned long ulDeviceID,
#endif
                                        char *pchFirmwareVersion,
                                        char *pchDllVersion,
                                        char *pchApiVersion);
extern "C" J2534ERROR WINAPI PassThruGetLastError(
                                        //unsigned long ulErrorID,
                                        char *pchErrorDescription);
extern "C" J2534ERROR WINAPI PassThruIoctl(
                                        unsigned long ulChannelID,
                                        J2534IOCTLID enumIoctlID,
                                        void *pInput,
                                        void *pOutput);

extern "C" J2534ERROR WINAPI PassThruReadKRSerialNum(
//                                      unsigned long ulDeviceID,
                                        char *pchKRSerialNum);
extern "C" J2534ERROR WINAPI PassThruReadSerialNum(
//                                      unsigned long ulDeviceID,
                                        char *pchSerialNum);
extern "C" J2534ERROR WINAPI PassThruWriteKRString(
//                                      unsigned long ulDeviceID,
                                        char *pchKRString);
extern "C" J2534ERROR WINAPI PassThruReadKRString(
//                                      unsigned long ulDeviceID,
                                        char *pchKRString);
//extern "C" J2534ERROR WINAPI PassThruLogging(bool bLogging);
extern "C" J2534ERROR WINAPI DGPassThruCheckLock(
                                                 char *pszLock,
                                                unsigned char *pucResult);
extern "C" J2534ERROR WINAPI DGPassThruAddLock(
                                                 char *pszLock,
                                                unsigned char *pucResult);
extern "C" J2534ERROR WINAPI DGPassThruDeleteLock(
                                                 char *pszLock,
                                                unsigned char *pucResult);

#endif