/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J1850PWM.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the implementation of the class 
*              that is derived from CProtocolBase to support J1850PWM 
*              protocol as required by J2534.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "J1850PWM.h"

//-----------------------------------------------------------------------------
//  Function Name   : CJ1850PWM
//  Input Params    : void
//  Output Params   : void
//  Description     : Constructor for CJ1850PWM class
//-----------------------------------------------------------------------------
CJ1850PWM::CJ1850PWM(CDeviceBase *pclsDevice, CDebugLog *pclsDebugLog) : CProtocolBase(pclsDevice, pclsDebugLog)
{

    //  m_pCFilterMsg = NULL;   
    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "CJ1850PWM()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Initialize.
    m_bConnected = false;

    //Initialize the speed
    m_ucIOdata = J1850PWM_41_6K;
    m_bLoopback = false;
    m_ucNodeAddress = J1850PWM_DATALINK_PARAM1;
    m_iSetNODE_ADDRESScount = 0;    // TEMP-TEST 130129par
    m_ulNetworkLine = 0;
    for (int i = 0; i < J1850PWM_MAILBOX_LIMIT; i++)
    {
        // Clear all data entries
        m_FunctionTable[i].ucFuncID = NULL;
        m_FunctionTable[i].bValid = false;
    }
    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "CJ1850PWM()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CJ1850PWM
//  Input Params    : void
//  Output Params   : void
//  Description     : Destructor for CJ1850PWM class
//-----------------------------------------------------------------------------
CJ1850PWM::~CJ1850PWM()
{
    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "~CJ1850PWM()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect() incase not called yet.
    vDisconnect();

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "~CJ1850PWM()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function establishes connection to the proctol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vConnect(J2534_PROTOCOL   enProtocolID,
                                unsigned long   ulFlags,
                                unsigned long   ulBaudRate,
                                DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                                LPVOID          pVoid)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR      enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    if(enProtocolID == J1850PWM)
    {
#if defined (J2534_0404) || defined (J2534_0500) // TO support J2534-1 500 Spec functionality
        if ((ulBaudRate != J1850PWM_DATA_RATE_DEFAULT_1) && 
            (ulBaudRate != J1850PWM_DATA_RATE_DEFAULT) && (ulBaudRate != J1850PWM_MAXDATA_RATE))
        {
            return(J2534_ERR_INVALID_BAUDRATE);
        }
#endif
#ifdef J2534_0305
        ulBaudRate = J1850PWM_DATA_RATE_DEFAULT_1;
#endif
        m_ulDataRate = ulBaudRate;
        m_enJ1850Protocol = J1850PWM;
        m_iSetNODE_ADDRESScount = 0;    // TEMP-TEST 130129par
    }

    // Call Connect of Base.
    if ((enJ2534Error = CProtocolBase::vConnect(enProtocolID, ulFlags, ulBaudRate, OnJ1850PWMRxMessage, this))!= J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vConnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vConnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnect
//  Input Params    : 
//  Output Params   : 
//  Description     : This function disconnects the connection to a protocol.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vDisconnect()
{
    J2534ERROR enJ2534Error;

    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Disconnect this protocol.
    if ((enJ2534Error = CProtocolBase::vDisconnect()) != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vDisconnect()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vDisconnect()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function reads the messages out of a circular buffer
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here. 
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vReadMsgs(PASSTHRU_MSG        *pstPassThruMsg,
                           unsigned long    *pulNumMsgs,
                           unsigned long    ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vReadMsgs(pstPassThruMsg,
                                                 pulNumMsgs,
                                                 ulTimeout)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vReadMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vReadMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}
#ifdef J2534_0500
//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
	unsigned long   *pulNumMsgs,
	unsigned long   ulTimeout,
	unsigned long    ulChannelId)
{
	//*****************************IMPORTANT NOTE******************************* 
	// Perform all the protocol specific stuff in this function.
	//**************************************************************************

	J2534ERROR  enJ2534Error;
	unsigned long   ulIdx1;

	// Write to Log File.
	WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

	for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
	{
		// Check Msg. Protocol ID.
		if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1850Protocol)
		{
			// Write to Log File.
			WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
			return(J2534_ERR_MSG_PROTOCOL_ID);
		}

		// Check if msg. format is valid.
		if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
		{
			// Write to Log File.
			WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
			return(J2534_ERR_INVALID_MSG);
		}
	}
	// Write using the generic Write.
	if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
		pulNumMsgs,
		ulTimeout
#ifdef J2534_0500
		, 0
#endif
	))
		!= J2534_STATUS_NOERROR)
	{
		// Write to Log File.
		WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
		return(enJ2534Error);
	}

	// Write to Log File.
	WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
	return(J2534_STATUS_NOERROR);
}
#endif

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs
//  Input Params    : 
//  Output Params   : 
//  Description     : This function writes the message out to a circular buffer
//                    and waits until it is transmitted out on the bus or exits
//                    immediately after writing to buffer if it is non-blocking.
//                    The message is Blocking if the given timeout value is 
//                    greater than 0. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vWriteMsgs(PASSTHRU_MSG   *pstPassThruMsg,
                            unsigned long   *pulNumMsgs,
                            unsigned long   ulTimeout)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;
    unsigned long   ulIdx1;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {
        // Check Msg. Protocol ID.
        if ((pstPassThruMsg + ulIdx1)->ulProtocolID != m_enJ1850Protocol)
        {
            // Write to Log File.
            WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
            return(J2534_ERR_MSG_PROTOCOL_ID);
        }
        
        // Check if msg. format is valid.
        if (!IsMsgValid((pstPassThruMsg + ulIdx1)))
        {
            // Write to Log File.
            WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
            return(J2534_ERR_INVALID_MSG);
        }
    }
    // Write using the generic Write.
    if ((enJ2534Error = CProtocolBase::vWriteMsgs(pstPassThruMsg,
                                                  pulNumMsgs,
                                                  ulTimeout
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This funtions starts a Periodic msg. on a given channel
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vStartPeriodicMsg(PASSTHRU_MSG        *pstPassThruMsg,
                                   unsigned long    *pulMsgID,
                                   unsigned long    ulTimeInterval)
{
    //*****************************IMPORTANT NOTE******************************* 
    // Perform all the protocol specific stuff in this function.
    //**************************************************************************

    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check Msg. Protocol ID.
    if (pstPassThruMsg->ulProtocolID != m_enJ1850Protocol)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check if msg. format is valid.
    if (!IsMsgValid(pstPassThruMsg))
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_MSG);
        return(J2534_ERR_INVALID_MSG);
    }

    // Start Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartPeriodicMsg(pstPassThruMsg,
                                                         pulMsgID,
                                                         ulTimeInterval
#ifdef J2534_0500
		,0
#endif
		)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStartPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodicMsg
//  Input Params    : 
//  Output Params   : 
//  Description     : This functions stops the Periodic Msg. that was started
//                    earlier. The base class implements the generic 
//                    functionality. Any specific functionality to this derived
//                    class is implented here.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::vStopPeriodicMsg(unsigned long ulMsgID)
{
    J2534ERROR  enJ2534Error;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Stop Periodic using the generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopPeriodicMsg(ulMsgID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStopPeriodicMsg()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets up a msg. filter as requested.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850PWM::vStartMsgFilter(J2534_FILTER     enumFilterType,
                                      PASSTHRU_MSG      *pstMask,
                                      PASSTHRU_MSG      *pstPattern,
                                      PASSTHRU_MSG      *pstFlowControl,
                                      unsigned long     *pulFilterID)
{
    J2534ERROR  enJ2534Error;
    unsigned long   ulOne;
    ulOne = 1;


    // NOTE : If request is for PASS filter, try setting this filter in device 
    //        as well. If the device J1850PWMnot set and returns an 
    //        error, ignore it. Some device drivers may not be able to set 
    //        hardware filters. Anyway it will be filtered by our software
    //        filter.  

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Check to see if message structure is valid
    enJ2534Error = MessageValid(pstPattern, &ulOne);

    if (enJ2534Error != J2534_STATUS_NOERROR)
        return(enJ2534Error);

    // Check Msg. Protocol ID.
    if ((pstMask->ulProtocolID != m_enJ1850Protocol) ||
        (pstPattern->ulProtocolID != m_enJ1850Protocol))
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_MSG_PROTOCOL_ID);
        return(J2534_ERR_MSG_PROTOCOL_ID);
    }

    // Check Filter Type.
    if (enumFilterType == J2534_FILTER_FLOW_CONTROL)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_INVALID_FILTER_ID);
        SetLastErrorText("J1850PWM : Invalid FilterType value");
        return(J2534_ERR_FAILED);
    }
    // StartMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStartMsgFilter(
                                          enumFilterType,
                                          pstMask,
                                          pstPattern,
                                          pstFlowControl,
                                          pulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStartMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopMsgFilter
//  Input Params    : 
//  Output Params   : 
//  Description     : This function stops a msg. filter that was set earlier.
//                    The base class implements the generic functionality. Any
//                    specific functionality to this derived class is implented
//                    here.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850PWM::vStopMsgFilter(unsigned long ulFilterID)
{
    J2534ERROR  enJ2534Error;

        /*FILTERMSGERROR    enumFilterMsgError;

    // See if the Class object is already created.
    if (m_pCFilterMsg == NULL)
        return(J2534_ERR_INVALID_MSG_ID);

    // Stop the periodic message started earlier.
    enumFilterMsgError = m_pCFilterMsg->Delete(ulMsgID);

    if (enumFilterMsgError == FILTERMSG_MSGID_INVALID_ERROR)
        return(J2534_ERR_INVALID_MSG_ID);

    return(J2534_STATUS_NOERROR);*/

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // StopMsgFilter using generic routine from base.
    if ((enJ2534Error = CProtocolBase::vStopMsgFilter(ulFilterID)) 
                      != J2534_STATUS_NOERROR)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
        return(enJ2534Error);
    }

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vStopMsgFilter()", DEBUGLOG_TYPE_COMMENT, "returned 0x%02X", J2534_STATUS_NOERROR);
    return(J2534_STATUS_NOERROR);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIoctl
//  Input Params    : 
//  Output Params   : 
//  Description     : This is a virtual function which should be implemented by
//                    the class derived from this base class.
//-----------------------------------------------------------------------------
J2534ERROR  CJ1850PWM::vIoctl(J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput)
{
    J2534ERROR enumJ2534Error = J2534_STATUS_NOERROR;

    // Write to Log File.
    WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    switch(enumIoctlID)
    {
    case GET_CONFIG:            // Get configuration    
        {
            enumJ2534Error = GetConfig((SCONFIG_LIST *)pInput);
            
        }
        break;
        
    case SET_CONFIG:            // Set configuration
        {
            enumJ2534Error = SetConfig((SCONFIG_LIST *)pInput);
            
            /*Check if the IOCTL value is not in range */
/*          if(enumJ2534Error!= J2534_STATUS_NOERROR)
                return(enumJ2534Error);

            if (enumJ2534Error = CProtocolBase::vIoctl(enumIoctlID,
                                                        pInput,
                                                        NULL))                                                      
            {
                // Write to Log File.
                WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enJ2534Error);
                return(enumJ2534Error);
            }
*/
        }
        
        break;
        
    case CLEAR_TX_BUFFER:   // Clear all messages in its transmit queue
        {           
            
            m_pclsTxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_RX_BUFFER:// Clear all messages in its receive queue
        {       
            m_pclsRxCircBuffer->ClearBuffer();
        }
        
        break;
        
    case CLEAR_PERIODIC_MSGS:   // Clear all periodic messages
        {           
            if (m_pclsPeriodicMsg != NULL)
            {
                delete m_pclsPeriodicMsg;
                m_pclsPeriodicMsg = NULL;
            }
        }
        break;
        
    case CLEAR_MSG_FILTERS:     // Clear all message filters
        {       
            if (m_pclsFilterMsg != NULL)
            {
                delete m_pclsFilterMsg;
                m_pclsFilterMsg = NULL;
            }
        }
        break;

    case CLEAR_FUNCT_MSG_LOOKUP_TABLE:  // Clear the whole functional message
                                        // look-up table
        enumJ2534Error = ClearTable();
        
        break;
        
    case ADD_TO_FUNCT_MSG_LOOKUP_TABLE: // Add functional address(es) to the
                                        // functional message look-up table
        enumJ2534Error = AddToTable((SBYTE_ARRAY *)pInput);
        
        break;
        
    case DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE: // Delete functional address(es) 
                                            // from the functional message 
                                            // look-up table 
        enumJ2534Error = DeleteFromTable((SBYTE_ARRAY *)pInput);
        
        break;
        
    default:                            // Others not supported
        enumJ2534Error = J2534_ERR_NOT_SUPPORTED;
        break;
    }


    // Write to Log File.
    int MsgType = (enumJ2534Error == J2534_STATUS_NOERROR) ? DEBUGLOG_TYPE_COMMENT : DEBUGLOG_TYPE_ERROR;
    WriteLogMsg("J1850PWM.cpp", "vIoctl()", MsgType, "returned 0x%02X", enumJ2534Error);
    WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_COMMENT, "End");
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : OnJ1850PWMRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This is a callback routine that is called upon receiving
//                    J1850PWM messages.
//-----------------------------------------------------------------------------
void OnJ1850PWMRxMessage(PASSTHRU_MSG *pstPassThruMsg, LPVOID pVoid)
{
    CJ1850PWM                   *pclsJ1850PWM;
    FILTERMSG_CONFORM_REQ   stConformReq;

    pclsJ1850PWM = (CJ1850PWM *) pVoid;

    // Check for NULL pointer.
    if (pclsJ1850PWM == NULL)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CJ1850PWM.cpp", "OnJ1850PWMRx()", 
                             DEBUGLOG_TYPE_COMMENT, "NULL pointer");
        }
        return;
    }


    // Check if this is a Loopback message.
    if (pstPassThruMsg->ulRxStatus & J2534_RX_FLAGBIT_MSGTYPE)
    {
        if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
        {
            CProtocolBase::m_pclsLog->Write("CJ1850PWM.cpp", "OnJ1850PWMRx()", 
                             DEBUGLOG_TYPE_COMMENT, "Tx CALLBACK");
        }
        if (pclsJ1850PWM->m_bLoopback == false)
            return;
        // Enqueue to Circ Buffer.
        pclsJ1850PWM->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
		pclsJ1850PWM->m_ulPhyChnRxCount++;
#endif
        return;
    }

    if ((CProtocolBase::m_pclsLog != NULL) && (CProtocolBase::m_pclsLog->m_pfdLogFile != NULL))
    {
        CProtocolBase::m_pclsLog->Write("CJ1850PWM.cpp", "OnJ1850PWMRx()", 
                         DEBUGLOG_TYPE_COMMENT, "Rx CALLBACK");
    }

    // Apply Filters and see if msg. is required.
    if (pclsJ1850PWM->m_pclsFilterMsg != NULL)
    {
        stConformReq.bReqPass = true;
        stConformReq.bReqBlock = true;
        stConformReq.bReqFlowControl = false;
        if (pclsJ1850PWM->IsMsgValidRx(pstPassThruMsg) &&
            pclsJ1850PWM->m_pclsFilterMsg->IsMsgRequired(pstPassThruMsg, &stConformReq))
        {
            // Enqueue to Circ Buffer.
            pclsJ1850PWM->m_pclsRxCircBuffer->Write((unsigned char *) pstPassThruMsg, 
                    (sizeof(PASSTHRU_MSG) - sizeof(pstPassThruMsg->ucData) +
                    pstPassThruMsg->ulDataSize));
#ifdef J2534_0500
			pclsJ1850PWM->m_ulPhyChnRxCount++;
#endif
        }
    }

    return;
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValid
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1850PWM::IsMsgValid(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J1850PWM_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1850PWM_MAX_DATALENGTH))
    {
        return(false);
    }
    if (pstPassThruMsg->ucData[2] != m_ucNodeAddress)
    {
        // Message with wrong node ID in the 3rd data byte
        return(false);
    }
    return(true);
}

//-----------------------------------------------------------------------------
//  Function Name   : IsMsgValidRx
//  Input Params    : void
//  Output Params   : void
//  Description     : This checks the validity of message structure as required
//                    by J2534 standard.
//-----------------------------------------------------------------------------
bool CJ1850PWM::IsMsgValidRx(PASSTHRU_MSG *pstPassThruMsg)
{
    if ((pstPassThruMsg->ulDataSize < J1850PWM_MSG_SIZE_MIN) || 
        (pstPassThruMsg->ulDataSize > J1850PWM_MAX_DATALENGTH))
    {
        return(false);
    }
    return(true);
}

J2534ERROR CJ1850PWM::MessageValid(PASSTHRU_MSG    *pstrucJ2534Msg,
                                   unsigned long  *pulNumMsgs)
{
    unsigned long   ulIdx1;
    
    for (ulIdx1 = 0; ulIdx1 < *pulNumMsgs; ulIdx1++)
    {       
        if ((pstrucJ2534Msg + ulIdx1)->ulDataSize == 0 ||
            (pstrucJ2534Msg + ulIdx1)->ulDataSize > J1850PWM_MAX_DATALENGTH+1)
        {
            // Message with data length being 0 bytes or 
            // greater than 4128 bytes
            *pulNumMsgs = 0;
            return(J2534_ERR_INVALID_MSG);
        }
    }

    return(J2534_STATUS_NOERROR);
}


//-----------------------------------------------------------------------------
//  Function Name   : GetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function gets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::GetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1850PWM.cpp", "GetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1850PWM : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }
    
    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        switch (pSconfig->Parameter)
        {
            case DATA_RATE:         // Data Rate

                pSconfig->ulValue = m_ulDataRate;

                break;

            case LOOPBACK:          // Loopback

                pSconfig->ulValue = m_bLoopback;

                break;

            case NODE_ADDRESS:      // Node Address

                pSconfig->ulValue = m_ucNodeAddress;

                break;

            case NETWORK_LINE:      // Network Line

                pSconfig->ulValue = m_ulNetworkLine;

                break;
            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : SetConfig
//  Input Params    : 
//  Output Params   : 
//  Description     : This function sets the configuration for selected 
//                    parameters for a given channel card.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::SetConfig(SCONFIG_LIST *pInput)
{
    J2534ERROR      enumJ2534Error;
    SCONFIG         *pSconfig;
    unsigned long   ulCount;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    if (pInput->ulNumOfParams == 0)
    {
#ifdef UD_TRUCK
        WriteLogMsg("J1850PWM.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "NOTE: NumOfParams = 0, no action taken");
#else
        SetLastErrorText("J1850PWM : Invalid NumOfParams value");
        return(J2534_ERR_FAILED);
#endif
    }
    else
    {
        pSconfig = pInput->pConfigPtr;
    }

    for (ulCount = 0; ulCount < pInput->ulNumOfParams; ulCount++)
    {
        // Write to Log File.
        WriteLogMsg("J1850PWM.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "SetConfig Parameter %lu Value %lu", pSconfig->Parameter, pSconfig->ulValue);
        WriteLogMsg("J1850PWM.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "Value is 0x%08X", pSconfig->ulValue);
        switch (pSconfig->Parameter)
        {
        case DATA_RATE:         // Data Rate

                enumJ2534Error = DataRate(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            case LOOPBACK:          // Loopback

                enumJ2534Error = Loopback(pSconfig->ulValue);

                break;

            case NODE_ADDRESS:      // Node Address

                enumJ2534Error = NodeAddress(pSconfig->ulValue);
                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);
                
                m_iSetNODE_ADDRESScount++;                                                                                                  // TEMP-TEST 130129par
                WriteLogMsg("J1850PWM.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, "m_iSetNODE_ADDRESScount = %i", m_iSetNODE_ADDRESScount); // TEMP-TEST 130129par
                if (m_iSetNODE_ADDRESScount > 1)                                                                                            // TEMP-TEST 130129par  
                {                                                                                                                           // TEMP-TEST 130129par
                    WriteLogMsg("J1850PWM.cpp", "SetConfig()", DEBUGLOG_TYPE_COMMENT, " been here before, ignore  set NODE_ADDRESS");       // TEMP-TEST 130129par
                    break;                                                                                                                  // TEMP-TEST 130129par
                }                                                                                                                           // TEMP-TEST 130129par  
                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;
            case NETWORK_LINE:      // Network Line

                enumJ2534Error = NetworkLine(pSconfig->ulValue);

                if(enumJ2534Error!= J2534_STATUS_NOERROR)
                    return(enumJ2534Error);

                if (enumJ2534Error = CProtocolBase::vIoctl(SET_CONFIG,
                                                            pSconfig,
                                                            NULL))                                                      
                {
                    // Write to Log File.
                    WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//                  return(enumJ2534Error);
                }

                break;

            default:

                return(J2534_ERR_NOT_SUPPORTED);
        }

        if (enumJ2534Error != J2534_STATUS_NOERROR) // If something is wrong
            return(enumJ2534Error);
        
        pSconfig++;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : DataRate
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the datarate.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::DataRate(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    enumJ2534Error = J2534_STATUS_NOERROR;

    if ((ulValue < 5) || (ulValue > 500000))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue == J1850PWM_DATA_RATE_DEFAULT_1)
    {
        // Set DataRate to 41.6 kbps
        m_ucIOdata = J1850PWM_41_6K;
    }
    else if (ulValue == J1850PWM_DATA_RATE_DEFAULT)
    {
        // Set DataRate to 41.6 kbps
        m_ucIOdata = J1850PWM_41_6K;
    }
    else if (ulValue == J1850PWM_MAXDATA_RATE_1)
    {
        // Set DataRate to 83.3 kbps
        m_ucIOdata = J1850PWM_83_3K;
    }
    else if (ulValue == J1850PWM_MAXDATA_RATE)
    {
        // Set DataRate to 83.3 kbps
        m_ucIOdata = J1850PWM_83_3K;
    }
    else
    {
        return(J2534_ERR_NOT_SUPPORTED);
    }

    m_ulDataRate = ulValue;

    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : Loopback
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the loopback to on/off.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::Loopback(unsigned long ulValue)
{
    J2534ERROR  enumJ2534Error;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    if ((ulValue < 0) || (ulValue > 1))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    if (ulValue)
    {
        m_bLoopback = true;
    }
    else
    {
        m_bLoopback = false;
    }

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : NodeAddress
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to set the node address for SCP channel.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::NodeAddress(unsigned long ulValue)
{
    J2534ERROR      enumJ2534Error;
    unsigned char   ucNodeAddress;

    enumJ2534Error = J2534_STATUS_NOERROR;
    ucNodeAddress = (unsigned char) ulValue;

    if (ucNodeAddress >= 0x00 && ucNodeAddress <= 0xFF)
    {
        // Set node address
        m_ucNodeAddress = ucNodeAddress;
    }
    else
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : ClearTable
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to clear the function table.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::ClearTable()
{
    J2534ERROR enumJ2534Error;

    enumJ2534Error = J2534_STATUS_NOERROR;

    for (int i = 0; i < J1850PWM_MAILBOX_LIMIT; i++)
    {
        // Clear all data entries
        m_FunctionTable[i].ucFuncID = NULL;
        m_FunctionTable[i].bValid = false;
    }

    if (enumJ2534Error = CProtocolBase::vIoctl(CLEAR_FUNCT_MSG_LOOKUP_TABLE,
                                                m_FunctionTable,
                                                NULL))                                                      
    {
        // Write to Log File.
//      WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//      return(enumJ2534Error);
    }

    // Clear Func ID table
    return(enumJ2534Error);
}

//-----------------------------------------------------------------------------
//  Function Name   : AddToTable
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to add functional address(es) to the 
//                    function table.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::AddToTable(SBYTE_ARRAY *pInput)
{
    J2534ERROR      enumJ2534Error;
    unsigned long   i, j;
    
    enumJ2534Error = J2534_STATUS_NOERROR;
    
    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);
    
    for (j = 0; j < pInput->ulNumOfBytes; j++)
    {
        for (i = 0; i < J1850PWM_MAILBOX_LIMIT; i++)
        {
            if (m_FunctionTable[i].bValid == false)
            {
                m_FunctionTable[i].ucFuncID = pInput->pucBytePtr[j];
                
                // Set Func ID table
                m_FunctionTable[i].bValid = true;
                // Write to Log File.
                WriteLogMsg("J1850PWM.cpp", "AddToTable()", DEBUGLOG_TYPE_COMMENT, "Added 0x%02X", pInput->pucBytePtr[j]);
                break;
            }
            
            if (m_FunctionTable[i].ucFuncID == pInput->pucBytePtr[j])
                break;
        }
        
        // Limit is already reached, no more mailboxes can be added
        if (i == J1850PWM_MAILBOX_LIMIT)
        {
            enumJ2534Error = J2534_ERR_EXCEEDED_LIMIT;
            return(enumJ2534Error);
        }
    }
    
    if (enumJ2534Error = CProtocolBase::vIoctl(ADD_TO_FUNCT_MSG_LOOKUP_TABLE,
                                                m_FunctionTable,
                                                NULL))                                                      
    {
        // Write to Log File.
//      WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//      return(enumJ2534Error);
    }
    return(enumJ2534Error);
}
//-----------------------------------------------------------------------------
//  Function Name   : DeleteFromTable
//  Input Params    : 
//  Output Params   : 
//  Description     : This function is to delete functional address(es) from  
//                    the function table.
//-----------------------------------------------------------------------------
J2534ERROR CJ1850PWM::DeleteFromTable(SBYTE_ARRAY *pInput)
{
    J2534ERROR      enumJ2534Error;
    bool            bDelete;
    unsigned long   i, j;

    enumJ2534Error = J2534_STATUS_NOERROR;

    // Make sure pInput is not NULL
    if (pInput == NULL)
        return(J2534_ERR_NULLPARAMETER);

    for (j = 0; j < pInput->ulNumOfBytes; j++)
    {
        bDelete = false;
        for (i = 0; i < J1850PWM_MAILBOX_LIMIT; i++)
        {
            if (m_FunctionTable[i].ucFuncID == pInput->pucBytePtr[j])
            {
                // Clear the specified mailbox
                m_FunctionTable[i].ucFuncID = NULL;
                m_FunctionTable[i].bValid = false;
                bDelete = true;
            }

            // Shift all entries following the deleted entry by one slot
            if (bDelete == true)
            {
                m_FunctionTable[i] = m_FunctionTable[i+1];
        
                if (i == J1850PWM_MAILBOX_LIMIT - 1) 
                {
                    // Reset the last entry in DB
                    m_FunctionTable[i].ucFuncID = NULL;
                    m_FunctionTable[i].bValid = false;
                }
            }
        }
    }

    // Clear Func ID table
    
    for (i = 0; i < J1850PWM_MAILBOX_LIMIT; i++)
    {
        if (m_FunctionTable[i].ucFuncID != NULL)
        {
            // Set Func ID table
        
        }
    }

    if (enumJ2534Error = CProtocolBase::vIoctl(DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE,
                                                m_FunctionTable,
                                                NULL))                                                      
    {
        // Write to Log File.
//      WriteLogMsg("J1850PWM.cpp", "vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", enumJ2534Error);
//      return(enumJ2534Error);
    }
    return(enumJ2534Error);
}
J2534ERROR CJ1850PWM::NetworkLine(unsigned long ulValue)
{
    J2534ERROR iReturn = J2534_STATUS_NOERROR;

    if ((ulValue < 0) || (ulValue > 2))
        return(J2534_ERR_INVALID_IOCTL_VALUE);

    m_ulNetworkLine = ulValue;

    return J2534_STATUS_NOERROR;
}
