/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DeviceCayman.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This the header file for CDeviceCayman.cpp
* Note:
*
*******************************************************************************/

#ifndef _DEVICECAYMAN_H_
#define _DEVICECAYMAN_H_

#include "DeviceBase.h"

//constants and header files to include depending on vendor lib being built
// We always want our structures on a 1 byte boundary
#pragma pack(push)  //push current alignment rules to internal stack
#pragma pack(1)     // Our native drivers need 1-byte alignment

#ifdef J2534_DEVICE_CARDONE
#include "DGVSIM32Gen.h"
#else
#ifdef J2534_DEVICE_VSI
#include "DGVSIM32Gen.h"
#else
#ifdef J2534_DEVICE_KRHTWIRE
#include "DGVSIM32Gen.h"
#else
#if defined(J2534_DEVICE_DPA5) || defined(J2534_DEVICE_DPA50305) || defined(J2534_DEVICE_NETBRIDGE)
#include "DPAM32.h"
#else
#if defined(J2534_DEVICE_SOFTBRIDGE) || defined(J2534_DEVICE_WURTH) || defined(J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
#include "ISeriesM32.h"
#else
#error  ERROR: DeviceCayman.h Define include file and native driver
#endif //px3 - softbridge - wurth - delphi - dbridge - dbridge0305
#endif//dpa5 - netbridge
#endif//krhw
#endif//vsi
#endif//cardone
#pragma pack(pop)   //restore original alignment rules from stack

#include "MyDec.h"

//****************************** IMPORTANT NOTE ******************************

// This file shall be created by the implementer of each device and the class
// shall be derived from the CDeviceBase. This file shall be included by the
// outside application and so it should not contain any device specific 
// information that may not be relevent to the application. Create a separate
// header file for the device specific information that is required internally.

// The creator of this class will implement each virtual function as required 
// for a specific device. The function returning DEVICEBASE_ERR_FAILED must 
// call SetLastErrorText() to set the appropriate Error Text before returnin 
// from the failed function. It is recommended that since the derived class 
// header file is included by the external application, it should not contain 
// device specific information that is for internal use and not relevent to 
// external applications.

// Use the following convention to name the derived Class:
// CDevice<Name of Device>

//****************************************************************************

#define DEVICECAYMAN_MAX_BUFFER_SIZE        1024
#define DEVICECAYMAN_ERROR_TEXT_SIZE        120
#define RX_BREAK           0x00000004
#define DG_API WINAPI
#define CAN_29BIT_ID            0x00000100
#define CAN_ID_BOTH             0x00000800

#define ISO15765_RXSTATUS_TIMEOUT			0x80000000	

#define J1850VPW_DATA_RATE_10416            10416
#define J1850VPW_MAXDATA_RATE_41666         41666
#define J1850VPW_DATA_RATE_10400            10400
#define J1850VPW_MAXDATA_RATE_41600         41600
#define J1850PWM_DATA_RATE_41666            41666
#define J1850PWM_MAXDATA_RATE_83333         83333           

#define NUM_FILTERS_PERIODICS_MAX           20
#define CDEVICECAYMAN_J1850PWM_MAILBOX_LIMIT 32

typedef struct
{
    unsigned char ucFuncID;
    bool bValid;
}
CDEVICECAYMAN_J1850PWM_LOOKUP_TABLE;

#ifdef J2534_DEVICE_DBRIDGE
typedef enum
{
	eNotSet = 0,
	eGrounded,
	eVoltageOff,
    ePrgmgVoltageApplied
}eProgrammingVoltage;
#endif


#ifdef J2534_DEVICE_CARDONE

#define MAX_NUMBER_MUTEX_APP                10
#define MAX_NUMBER_VEHICLE_REPEAT_LIST      10
#define MAX_NUMBER_REPEAT_LIST              10
#define MAX_VEHICLE_NAME                    30
#define MAX_APP_NAME                        512

typedef struct
{
    char chName[MAX_VEHICLE_NAME];
    int iNumberApp;
    char chApp[MAX_NUMBER_MUTEX_APP][MAX_APP_NAME];
}
MUTEX_APP_LIST;

typedef struct
{
    char chRepeatApp[MAX_APP_NAME];
    int iNumberVehicles;
    char chVehicleNames[MAX_NUMBER_VEHICLE_REPEAT_LIST][MAX_VEHICLE_NAME];
}
REPEAT_LIST;

#endif

typedef struct
{
    J2534_PROTOCOL  enProtocolID;
    BYTE bProtocol;
    bool bIsProtocolConnected;
    unsigned long ulPins;
    unsigned long ulJ1939Pins;
    unsigned long ulJ1708Pins;
    void *vpInitDataLinkData; // address of m_InitDataLinkDataExt for PS protocols connect pins with ioctl
    void *vpInitDataLinkDataCAN; // address of m_InitDataLinkDataExt for PS protocols connect pins with ioctl
}
CDEVICECAYMAN_PROTOCOL_CONNECTION;

#ifdef J2534_DEVICE_DBRIDGE
#define EURO5_AREA_SIZE         255 // All 4 areas in the Euro5 hardware are 255 bytes long
#define BRAND_DATA_STORAGE      4 // Euro5 Area number for Data Storage
#define STORE_SEPARATOR         ",\0" // Euro5 Area number for Data Storage
#endif


class CDeviceCayman : public CDeviceBase
{
private:
    ReturnStatusType loadDLL(void);
    void unLoadDLL(void);
    bool m_bIsDeviceConnected;
    char szDllVersion[40];
    char szApiVersion[40];
    unsigned long m_ulJ1962Pins;
    unsigned long m_ulJ1939Pins;
    unsigned long m_ulJ1708Pins;
    HINSTANCE dllPointer;
    bool bFullSpeedSCIA;
    bool m_bCANCH3Used;
    bool m_bCANCH4Used;
    bool bIsProtocolBrandingSupported(J2534_PROTOCOL  enProtocolID);

#ifdef J2534_DEVICE_CARDONE
    int m_iNumberVehicles;
    MUTEX_APP_LIST * m_sMutexAppList;
    int m_iNumberRepeatApps;
    REPEAT_LIST m_sRepeatAppList[MAX_NUMBER_REPEAT_LIST];
    void ReadMutexNamesFromINI();
    void GetMutexName(char * chProcessName, char * chMutexName, unsigned int iMutexName_Size);
#endif

public:
    CDeviceCayman(CDebugLog * pclsDebugLog=NULL, CDataLog * pclsDataLog=NULL);
    ~CDeviceCayman();
    virtual J2534ERROR vOpenDevice(J2534REGISTRY_CONFIGURATION * punDeviceSettings);
    virtual J2534ERROR vCloseDevice();
    virtual J2534ERROR vConnectProtocol(
                            J2534_PROTOCOL  enProtocolID,
                            unsigned long   ulFlags, 
                            unsigned long   ulBaudRate,
                            DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                            LPVOID          pVoid,
                            unsigned long   *pulChannelID);

    virtual J2534ERROR vStartAllPassFilter(unsigned long    ulChannelID,
                                            unsigned long ulFlags);
    virtual J2534ERROR vStopAllPassFilter(unsigned long ulChannelID);
    virtual J2534ERROR vDisconnectProtocol(unsigned long    ulChannelID);
    virtual J2534ERROR vGetRevision(unsigned long ulDeviceID,
                                        char *pchFirmwareVersion,
                                        char *pchDllVersion,
                                        char *pchApiVersion);
    virtual J2534ERROR vGetSerialNum(unsigned long ulDeviceID,
                                        char *pchKRSerialNum);
    virtual J2534ERROR vGetHWVersion(unsigned long ulDeviceID,
                                        char *pchHWVersion);
    virtual J2534ERROR vWriteKRString(unsigned long ulDeviceID,
                                        char *pchKRString);
    virtual J2534ERROR vReadKRString(unsigned long ulDeviceID,
                                        char *pchKRString);
    virtual J2534ERROR vWriteMsgs(
                            unsigned long   ulChannelID,
                            PASSTHRU_MSG    *pstPassThruMsg,
                            unsigned long   *pulNumMsgs);
    virtual J2534ERROR vStartPeriodic(
                            unsigned long   ulChannelID, 
                            PASSTHRU_MSG    *pstMsg, 
                            unsigned long   ulTimeInterval, 
                            unsigned long   *pulPeriodicRefID);
    virtual J2534ERROR vStopPeriodic(
                            unsigned long   ulChannelID,
                            unsigned long   ulPeriodicRefID);
    virtual J2534ERROR vStartFilter(
                            unsigned long   ulChannelID,
                            J2534_FILTER    enFilterType, 
                            PASSTHRU_MSG    *pstMask, 
                            PASSTHRU_MSG    *pstPattern,
                            PASSTHRU_MSG    *pstFlowControl,
                            unsigned long   *pulFilterRefID);
    virtual J2534ERROR vStopFilter(
                            unsigned long   ulChannelID,
                            unsigned long   ulFilterRefID);
    virtual J2534ERROR vIoctl(unsigned long ulChannelID,
                            J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput);
    virtual J2534ERROR vProgrammingVoltage(unsigned long ulDeviceID,
                                        unsigned long ulPin,
                                        unsigned long ulVoltage);
    virtual bool       vIsDeviceConnected(bool bFlag = true);        
    virtual bool       vSetDataLogging(bool bFlag);

    virtual J2534ERROR vCheckLock(char *pszLock,unsigned char *pucResult);
    virtual J2534ERROR vAddLock(char *pszLock,unsigned char *pucResult);
    virtual J2534ERROR vDeleteLock(char *pszLock,unsigned char *pucResult);
    virtual J2534ERROR vTurnAuxOff();
    virtual J2534ERROR vTurnAuxOn();
#ifdef J2534_DEVICE_DBRIDGE
    virtual J2534ERROR vDataStorage(J2534IOCTLID enumIoctlID, unsigned long *pulDataStorage, int iDataSize);
#endif
    virtual bool IsUartBaudRateValid(unsigned long ulBaudRate);
    virtual bool IsProtocolSupported(J2534_PROTOCOL enProtocolID);
    virtual int GetProtocolIndex(long enProtocolID);
    virtual unsigned long GetChannelID(BYTE bProtocol);
    virtual unsigned long GetChannelIDCAN(BYTE bProtocol);
    virtual J2534_PROTOCOL SetProtocolID(int iProtocolIndex);
    virtual J2534ERROR ConfigProtocol(ULONG ulChannelID);
    virtual void WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);
    virtual J2534ERROR vMakePhysicalConnection(unsigned long  ulChannelID, J2534IOCTLPARAMID enIoctlParameter, unsigned long ulPins);
    virtual J2534ERROR vMakePhysicalConnectionJ1939(unsigned long  ulChannelID, J2534IOCTLPARAMID enIoctlParameter, unsigned long ulPins);
    virtual J2534ERROR vMakePhysicalConnectionJ1708(unsigned long  ulChannelID, J2534IOCTLPARAMID enIoctlParameter, unsigned long ulPins);
    virtual bool IsChannelConnected(unsigned long ulChannelID);
#ifdef J2534_DEVICE_DPA5_EURO5
    virtual void ReloadMailBoxes(unsigned long ulChannelID);
#endif

public:
    
    short dpaHandle;
    HANDLE  m_hMutex;
    unsigned long m_ulLastErrorCode;
    char szSerialNumb[120];
    BYTE m_byCANTermination[4];
#ifdef J2534_DEVICE_DBRIDGE
    eProgrammingVoltage eCurrVoltagePin9;
    eProgrammingVoltage eCurrVoltagePin15;
#endif

    InitDataLinkType m_InitDataLinkData;
    InitDataLinkType m_InitDataLinkDataSWCAN;
#ifdef J2534_DEVICE_DPA5_EURO5
    InitDataLinkTypeExt m_InitDataLinkDataExt;
    InitDataLinkTypeExt m_InitDataLinkDataExtSWISO15765;

    MailBoxTypeExt *m_pTransmitHandleExt[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pTransmitHandleExtCAN[PROTOCOL_ENTRY_COUNT];
    MailBoxTypeExt *m_pRxHandleExt[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxTypeExt *m_pCallBkReceiveHandleExt[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    unsigned long m_pCallBkReceiveHandleExtISO15765[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxTypeExt *m_pPassAllFilterHandleExt[PROTOCOL_ENTRY_COUNT];

    LoadMailBoxTypeExt m_pRxMailBoxExt;
    LoadMailBoxTypeExt m_pTxMailBoxExt;
    LoadMailBoxType m_pTxMailBoxExtCAN;
    LoadMailBoxTypeExt m_pTxPeriodicMailBoxExt;
    LoadMailBoxTypeExt m_pFilterMailBoxExt;

    LoadMailBoxTypeExt m_pTxMailBoxExtCopy[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxType m_pTxMailBoxExtCANCopy[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxTypeExt m_pTxPeriodicMailBoxExtCopy[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    LoadMailBoxType m_pTxPeriodicMailBoxExtCANCopy[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    unsigned char   uchPeriodicDataBuffer[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX][512];
    LoadMailBoxTypeExt m_pRxMailBoxExtCopy[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    LoadMailBoxType m_pRxMailBoxCopy[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    unsigned char ucExtAddrCopy[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
#endif
    LoadMailBoxType m_pRxMailBox11bitLogger[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxType m_pRxMailBox29bitLogger[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxType m_pJ1708RxMailBoxLogger[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxType m_pRxMailBox;
    LoadMailBoxType m_pJ1939RxMailBox;
    LoadMailBoxType m_pTxMailBox;
    LoadMailBoxType m_pTxMailBoxCAN;
    LoadMailBoxType m_pTxPeriodicMailBox;
    LoadMailBoxType m_pFilterMailBox;
    LoadMailBoxType m_pLINTxMailBox;
    LoadMailBoxType m_pLINRxMailBox;

    LoadMailBoxType m_pRxMailBoxCAN11bitLoggerCopy[PROTOCOL_ENTRY_COUNT];
    LoadMailBoxType m_pRxMailBoxCAN29bitLoggerCopy[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pCANRxHandle11bitLogger[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pCANRxHandle29bitLogger[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pJ1708RxHandleLogger[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pLINTransmitHandle[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxType *m_pLINRxHandle[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxType *m_pTransmitHandle[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pTransmitHandleCAN[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pRxHandle[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxType *m_pCallBkReceiveHandle[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    unsigned long m_pCallBkReceiveHandleISO15765[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxType *m_pPassAllFilterHandle[PROTOCOL_ENTRY_COUNT];
    MailBoxType *m_pJ1939CallBkReceiveHandle[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    MailBoxType *m_pJ1939BAMCallBkReceiveHandle[PROTOCOL_ENTRY_COUNT];
    unsigned char m_ucJ1939ProtectedAddr[PROTOCOL_ENTRY_COUNT][NUM_FILTERS_PERIODICS_MAX];
    unsigned long m_ulNumJ1939ProtectedAddr[PROTOCOL_ENTRY_COUNT];
    
//  J2534_PROTOCOL  ProtocolID;
    unsigned long m_ulCANIdType[PROTOCOL_ENTRY_COUNT], m_ulFilterCANIdType[PROTOCOL_ENTRY_COUNT];
    unsigned long m_ulChecksumFlag;
//    bool m_bSWISO15765Flag;

    SPARAM_LIST *pIptSParamPtr;
    SPARAM *pIptSParamPtrConfig;
    SCONFIG_LIST *pIptPtr;
    SCONFIG *pIptPtrConfig;
    SBYTE_ARRAY *pIptSBytePtr, *pOptSBytePtr;
    PASSTHRU_MSG *pIptPassThruMsgPtr, *pOptPassThruMsgPtr;
    SMEMORY_READ *pMemPtr;
	SMEMORY_WRITE *pMemWritePtr;

    HANDLE				m_hDataLoggingSyncAccess;
    bool        m_bIsDataLoggingOn;
    HANDLE m_hTxProtocol[PROTOCOL_ENTRY_COUNT];
    unsigned long m_ulSWDataRate, m_ulSWHSDataRate;

    unsigned long m_ulISO15765_BS, m_ulISO15765_STMIN, m_ulBS_TX, m_ulSTMIN_TX, m_ulWftMax;
    ConfigureISO15765Type m_stISO15765Config[PROTOCOL_ENTRY_COUNT];
#ifdef J2534_DEVICE_DPA5_EURO5
    ConfigureTransportType m_stJ1939Config[PROTOCOL_ENTRY_COUNT];
#else
    ConfigureJ1939Type m_stJ1939Config[PROTOCOL_ENTRY_COUNT];
#endif
    unsigned long m_ulJ1939_T1, m_ulJ1939_T2, m_ulJ1939_T3, m_ulJ1939_T4, 
        m_ulJ1939_BDST_MIN;
    unsigned long m_ulISO9141_P1MAX, m_ulISO9141_P3MIN, m_ulISO9141_P4MIN, 
        m_ulISO9141_W0, m_ulISO9141_W1, m_ulISO9141_W2, m_ulISO9141_W3, m_ulISO9141_W4,
        m_ulISO9141_W5, m_ulTIDLE, m_ulTINIL, m_ulTWUP;
    unsigned long m_ulJ2818_wT0_MIN, m_ulJ2818_wT1_MAX, m_ulJ2818_wT2_MAX, 
        m_ulJ2818_wT3_MAX, m_ulJ2818_wT4_MIN, m_ulJ2818_wT5_MAX, m_ulJ2818_wT6_MAX,
        m_ulJ2818_wT7_MIN, m_ulJ2818_wT8_MAX, m_ulJ2818_wT9_MIN;
    unsigned long m_ulJ2809_wT0_MIN, m_ulJ2809_wT1_MAX, m_ulJ2809_wT2_MAX, 
        m_ulJ2809_wT3_MAX, m_ulJ2809_wT4_MIN, m_ulJ2809_wT5_MAX, m_ulJ2809_wT6_MAX,
        m_ulJ2809_wT7_MIN, m_ulJ2809_wT8_MAX, m_ulJ2809_wT9_MIN;
    unsigned long m_ulParity, m_ulDataBits, m_ul5BaudMod;
    unsigned long m_ulISO9141DataRate, m_utJ2818DataRate, m_utJ2809DataRate;
    unsigned long m_ulLINDataRate, m_ulLINVersion;
    ConfigureISO9141Type m_stISO9141Config;
    unsigned long m_ulTP2_0_T_BR_INT;
    unsigned long m_ulTP2_0_T_E;
    unsigned long m_ulTP2_0_MNTC;
    unsigned long m_ulTP2_0_T_CTA;
    unsigned long m_ulTP2_0_MNCT;
    unsigned long m_ulTP2_0_MNTB;
    unsigned long m_ulTP2_0_MNT;
    unsigned long m_ulTP2_0_T_WAIT;
    unsigned long m_ulTP2_0_T1;
    unsigned long m_ulTP2_0_T3;
    unsigned long m_ulTP2_0_IDENTIFER;
    unsigned long m_ulTP2_0_RXIDPASSIVE;
#ifndef J2534_DEVICE_DPA5_EURO5
    ConfigureJ2818Type m_stJ2818Config;
#endif
#ifndef J2534_DEVICE_DPA5_EURO5
    ConfigureSCIType m_stSCIConfig;
    //ConfigureKWP1281Type m_stKWP1281Config;
#endif
#ifdef J2534_DEVICE_DBRIDGE
    ConfigureJ2809Type m_stJ2809Config;
    J2534ERROR SetDataStorage(unsigned long *pulDataStorage, int iDataSize);
#ifdef J2534_DEVICE_SVCI
    ConfigureJ2819Type m_stJ2819Config;
#endif
#endif
    bool TestFeature(long lFeatureValue);
    unsigned long m_ulSCI_T1MAX, m_ulSCI_T2MAX, m_ulSCI_T3MAX, m_ulSCI_T4MAX, m_ulSCI_T5MAX;

    unsigned long m_ulISO9141KOnly;
    unsigned long m_ulNodeAddress, m_ulNetworkLine, m_ulJ1850PWMDataRate;
    CDEVICECAYMAN_J1850PWM_LOOKUP_TABLE m_FunctionTable[CDEVICECAYMAN_J1850PWM_MAILBOX_LIMIT];
    unsigned long m_ulNumJ1850PWMFuncMsg;
    unsigned char m_ucJ1850PWMFuncMsg[CDEVICECAYMAN_J1850PWM_MAILBOX_LIMIT];
    bool ReadOEMLicense(char *chInput, unsigned char *uchOutput);
    bool WriteOEMLicense(char *chInput, unsigned char *uchOutput);
    bool WriteToLockString(char *chInput, char *chWriteInput, unsigned char *uchOutput);
    bool WriteToLockBufferString(char *chInput, char *chWriteInput, unsigned char *uchOutput);
    bool WriteToDeleteLockBufferString(char *chInput, char *chWriteInput, unsigned char *uchOutput);

public:

    bool LoadReceivemailbox(J2534_PROTOCOL  enProtocolID,
                            J2534_FILTER enFilterType,
                            unsigned long ulMskessageId,
                            unsigned long ulPatternMsg,
                            unsigned long ulFlowCtrlMsg,
                            bool bEAEnabled,
                            unsigned char ucExtAddr,
                            unsigned long *pulFilterRefID,
                            unsigned long ulChannelID);

    unsigned long FormatMessageHeader(PASSTHRU_MSG  *pstPassThruMsg);

};

extern void SetLastErrorText(char *pszErrorText, ...);

#ifdef _DEVICECAYMAN_
CDEVICECAYMAN_PROTOCOL_CONNECTION g_ProtocolConnectionList[PROTOCOL_ENTRY_COUNT];
CDEVICECAYMAN_PROTOCOL_CONNECTION g_ProtocolConnectionListCAN[PROTOCOL_ENTRY_COUNT];
#endif
J2534ERROR IsUartBaudRateValid(unsigned long ulBaudRate);
BYTE       GetUartBaudRateParm(unsigned long ulBaudRate);
#endif
