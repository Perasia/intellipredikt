/**************************************/
/*                                    */
/* Dearborn Group,Copyright (c) 2005  */
/* Dearborn Cayman Adapter            */
/* Version 1.00                       */
/*                                    */
/**************************************/

#ifndef _CAYMAN1XH
#define _CAYMAN1XH

#include "Cayman1xT.h"

/***********************/
/* function prototypes */
/***********************/
#ifdef __cplusplus
extern "C" {
    ReturnStatusType DllExport WINAPI InitDPA (short *dpaHandle,short dpaNumber);
    ReturnStatusType  DllExport WINAPI InitCommLink (short *dpaHandle, CommLinkType *CommLinkData);
    ReturnStatusType  DllExport WINAPI InitTCP_IP (short *dpaHandle,TCP_IPType *TCP_IPData);
    ReturnStatusType DllExport WINAPI InitUSBLink (short *dpaHandle,USBLinkType *USBData);
    ReturnStatusType DllExport WINAPI RestoreDPA (short dpaHandle);
    ReturnStatusType DllExport WINAPI RestoreCommLink (short dpaHandle);
    ReturnStatusType  DllExport WINAPI RestoreTCP_IP (short dpaHandle);
    ReturnStatusType DllExport WINAPI RestoreUSBLink (short dpaHandle);

    ReturnStatusType DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
    ReturnStatusType DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString,BYTE *bFound);
    ReturnStatusType DllExport WINAPI ConfigureProtocol (short dpaHandle,BYTE bProtocol, void  *ConfigureData);
    ReturnStatusType DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
    ReturnStatusType DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
    ReturnStatusType DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName,BYTE *bseed);
    ReturnStatusType DllExport WINAPI GetSerialNumber ( short dpaHandle, char  *pData);
    ReturnStatusType DllExport WINAPI GoToLoader(short dpaHandle);
    ReturnStatusType DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
    ReturnStatusType DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
    ReturnStatusType DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
    ReturnStatusType DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
    ReturnStatusType DllExport WINAPI PauseTimer (short dpaHandle);
    ReturnStatusType DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
    ReturnStatusType DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
    ReturnStatusType DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
    ReturnStatusType DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
    ReturnStatusType DllExport WINAPI ResetDPA(short dpaHandle,ResetType *ResetData);
    ReturnStatusType DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI ResumeTimer (short dpaHandle);
    ReturnStatusType DllExport WINAPI SetBaudRate (short dpaHandle,SetBaudRateType *SetBaudRateData);
    ReturnStatusType DllExport WINAPI SetMsgLookupTable ( short dpaHandle, BYTE bDataCount, unsigned char *pData);
    ReturnStatusType DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
    ReturnStatusType DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);  
    ReturnStatusType DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bProtocol, BYTE *bTens, BYTE *bHundredths);
    ReturnStatusType DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);
}
#else
    ReturnStatusType DllExport WINAPI InitDPA (short *dpaHandle,short dpaNumber);
    ReturnStatusType DllExport WINAPI InitCommLink (short *dpaHandle, CommLinkType *CommLinkData);
    ReturnStatusType  DllExport WINAPI InitTCP_IP (short *dpaHandle,TCP_IPType *TCP_IPData);
    ReturnStatusType DllExport WINAPI InitUSBLink (short *dpaHandle,USBLinkType *USBData);
    ReturnStatusType DllExport WINAPI RestoreDPA (short dpaHandle);
    ReturnStatusType DllExport WINAPI RestoreCommLink (short dpaHandle);
    ReturnStatusType  DllExport WINAPI RestoreTCP_IP (short dpaHandle);
    ReturnStatusType DllExport WINAPI RestoreUSBLink (short dpaHandle);

    ReturnStatusType DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
    ReturnStatusType DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString,BYTE *bFound);
    ReturnStatusType DllExport WINAPI ConfigureProtocol (short dpaHandle,BYTE bProtocol, void  *ConfigureData);
    ReturnStatusType DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
    ReturnStatusType DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
    ReturnStatusType DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName,BYTE *bseed);
    ReturnStatusType DllExport WINAPI GetSerialNumber ( short dpaHandle, char  *pData);
    ReturnStatusType DllExport WINAPI GoToLoader(short dpaHandle);
    ReturnStatusType DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
    ReturnStatusType DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
    ReturnStatusType DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
    ReturnStatusType DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
    ReturnStatusType DllExport WINAPI PauseTimer (short dpaHandle);
    ReturnStatusType DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
    ReturnStatusType DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
    ReturnStatusType DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
    ReturnStatusType DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
    ReturnStatusType DllExport WINAPI ResetDPA(short dpaHandle,ResetType *ResetData);
    ReturnStatusType DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI ResumeTimer (short dpaHandle);
    ReturnStatusType DllExport WINAPI SetBaudRate (short dpaHandle,SetBaudRateType *SetBaudRateData);
    ReturnStatusType DllExport WINAPI SetMsgLookupTable ( short dpaHandle, BYTE bDataCount, unsigned char *pData);
    ReturnStatusType DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
    ReturnStatusType DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
    ReturnStatusType DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
    ReturnStatusType DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);
    ReturnStatusType DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,WORD wUpdateFlag);  
    ReturnStatusType DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bProtocol, BYTE *bTens, BYTE *bHundredths);
    ReturnStatusType DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);
#endif

#endif
