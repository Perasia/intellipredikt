/**************************************/
/*                                    */
/* Dearborn Group, Inc.               */
/* Copyright (c) 2011                 */
/* Dearborn Protocol Adapter          */
/* Version 10.03                      */
/*                                    */
/**************************************/


#ifndef _   
#define _DPAM10XH

#include "dpa10xt.h"

/***********************/
/* function prototypes */
/***********************/
#ifdef __cplusplus
extern "C" {
	ReturnStatusType  DllExport WINAPI InitDPA (short *dpaHandle,short dpaNumber);
	ReturnStatusType  DllExport WINAPI InitCommLink (short *dpaHandle,CommLinkType *CommLinkData);
	ReturnStatusType  DllExport WINAPI InitPCCard (short *dpaHandle,PCCardType *PCCardData);
	ReturnStatusType  DllExport WINAPI InitTCP_IP (short *dpaHandle,TCP_IPType *TCP_IPData);
	ReturnStatusType  DllExport WINAPI InitUSBLink (short *dpaHandle,USBLinkType *USBData);
	ReturnStatusType  DllExport WINAPI InitUSBDPA (short *dpaHandle, short sUSBDPAType);
	ReturnStatusType  DllExport WINAPI RestoreDPA (short dpaHandle);
	ReturnStatusType  DllExport WINAPI RestoreCommLink (short dpaHandle);
	ReturnStatusType  DllExport WINAPI RestorePCCard (short dpaHandle);
	ReturnStatusType  DllExport WINAPI RestoreTCP_IP (short dpaHandle);
	ReturnStatusType  DllExport WINAPI RestoreUSBLink (short dpaHandle);
	ReturnStatusType  DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
	ReturnStatusType  DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
	ReturnStatusType  DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
	ReturnStatusType  DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
	ReturnStatusType  DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
	ReturnStatusType  DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString,BYTE *bFound);
	ReturnStatusType  DllExport WINAPI GoToLoader(short dpaHandle);
	ReturnStatusType  DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
	ReturnStatusType  DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
	ReturnStatusType  DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
	ReturnStatusType  DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
	ReturnStatusType  DllExport WINAPI PauseTimer (short dpaHandle);
	ReturnStatusType  DllExport WINAPI ResumeTimer (short dpaHandle);
	ReturnStatusType  DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
	ReturnStatusType  DllExport WINAPI ConfigureTransportProtocol (short dpaHandle,ConfigureTransportType  *ConfigureTransportTypeData);
	ReturnStatusType  DllExport WINAPI SetBaudRate (short dpaHandle,SetBaudRateType *SetBaudRateData);
	ReturnStatusType  DllExport WINAPI ResetDPA(short dpaHandle,ResetType *ResetData);
	ReturnStatusType  DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName,BYTE *bseed);
	ReturnStatusType  DllExport WINAPI WriteDisplay (short dpaHandle,DisplayType *displayData);
	ReturnStatusType  DllExport WINAPI InitDisplay (short dpaHandle,InitDisplayType *InitDisplayData );
	ReturnStatusType  DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
	//extended definitions
	ReturnStatusType  DllExport WINAPI InitDataLinkExt (short dpaHandle,InitDataLinkTypeExt *InitDataLinkData);
	ReturnStatusType  DllExport WINAPI LoadMailBoxExt (short dpaHandle,LoadMailBoxTypeExt *pLoadMailBoxData);
	ReturnStatusType  DllExport WINAPI UnloadMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI ReceiveMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI ConfigureProtocol (short dpaHandle,BYTE bProtocol, void  *ConfigureData);
	ReturnStatusType  DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
	ReturnStatusType DllExport WINAPI GetSerialNumber ( short dpaHandle, char  *pData);
	ReturnStatusType  DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bVoltageType, BYTE *bTens, BYTE *bHundredths);
	ReturnStatusType  DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);

}
#else
ReturnStatusType  DllExport WINAPI InitDPA (short *dpaHandle,short dpaNumber);
ReturnStatusType  DllExport WINAPI InitCommLink (short *dpaHandle,CommLinkType *CommLinkData);
ReturnStatusType  DllExport WINAPI InitPCCard (short *dpaHandle,PCCardType *PCCardData);
ReturnStatusType  DllExport WINAPI InitTCP_IP (short *dpaHandle,TCP_IPType *TCP_IPData);
ReturnStatusType  DllExport WINAPI InitUSBLink (short *dpaHandle,USBLinkType *USBData);
ReturnStatusType  DllExport WINAPI InitUSBDPA (short *dpaHandle, short sUSBDPAType);
ReturnStatusType  DllExport WINAPI InitBluetoothLink (short *dpaHandle, char *bName);
ReturnStatusType  DllExport WINAPI RestoreDPA (short dpaHandle);
ReturnStatusType  DllExport WINAPI RestoreCommLink (short dpaHandle);
ReturnStatusType  DllExport WINAPI RestorePCCard (short dpaHandle);
ReturnStatusType  DllExport WINAPI RestoreTCP_IP (short dpaHandle);
ReturnStatusType  DllExport WINAPI RestoreUSBLink (short dpaHandle);
ReturnStatusType  DllExport WINAPI RestoreBluetoothLink (short dpaHandle);
ReturnStatusType  DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
ReturnStatusType  DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
ReturnStatusType  DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
ReturnStatusType  DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
ReturnStatusType  DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData,WORD wLength,WORD wOffset);
ReturnStatusType  DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString,BYTE *bFound);
ReturnStatusType  DllExport WINAPI GoToLoader(short dpaHandle);
ReturnStatusType  DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
ReturnStatusType  DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle,BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
ReturnStatusType  DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
ReturnStatusType  DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
ReturnStatusType  DllExport WINAPI PauseTimer (short dpaHandle);
ReturnStatusType  DllExport WINAPI ResumeTimer (short dpaHandle);
ReturnStatusType  DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
ReturnStatusType  DllExport WINAPI ConfigureTransportProtocol (short dpaHandle,ConfigureTransportType  *ConfigureTransportTypeData);
ReturnStatusType  DllExport WINAPI SetBaudRate (short dpaHandle,SetBaudRateType *SetBaudRateData);
ReturnStatusType  DllExport WINAPI ResetDPA(short dpaHandle,ResetType *ResetData);
ReturnStatusType  DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName,BYTE *bseed);
ReturnStatusType  DllExport WINAPI WriteDisplay (short dpaHandle,DisplayType *displayData);
ReturnStatusType  DllExport WINAPI InitDisplay (short dpaHandle,InitDisplayType *InitDisplayData );
ReturnStatusType  DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
ReturnStatusType  DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);
ReturnStatusType  DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
ReturnStatusType  DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
//extended definitions
ReturnStatusType  DllExport WINAPI InitDataLinkExt (short dpaHandle,InitDataLinkTypeExt *InitDataLinkData);
ReturnStatusType  DllExport WINAPI LoadMailBoxExt (short dpaHandle,LoadMailBoxTypeExt *pLoadMailBoxData);
ReturnStatusType  DllExport WINAPI UnloadMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsyncExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI ReceiveMailBoxExt (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI ConfigureProtocol (short dpaHandle,BYTE bProtocol, void  *ConfigureData);
ReturnStatusType  DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
ReturnStatusType  DllExport WINAPI GetSerialNumber ( short dpaHandle, char  *pData);
ReturnStatusType  DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bVoltageType, BYTE *bTens, BYTE *bHundredths);
ReturnStatusType  DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);

#endif

#endif
