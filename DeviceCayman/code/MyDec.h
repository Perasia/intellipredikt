#ifndef MYDEC_H
#define MYDEC_H

#ifdef J2534_DEVICE_DPA5_EURO5
#if defined(J2534_DEVICE_DPA5) || defined(J2534_DEVICE_DPA50305) || defined(J2534_DEVICE_NETBRIDGE)
#include "DPAM32.h"
#else
#if defined(J2534_DEVICE_SOFTBRIDGE) || defined(J2534_DEVICE_WURTH) || defined(J2534_DEVICE_DELPHI) || defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305)
#include "ISeriesM32.h"
#endif
#endif
#else
#include "DGVSIM32Gen.h"
#endif

#define DG_API  WINAPI

typedef ReturnStatusType (DG_API  *DG_INIT_DPA ) (short *dpaHandle, short dpaNumber);
typedef ReturnStatusType (DG_API  *DG_INIT_USB_DPA ) (short *dpaHandle, short dpaNumber);
typedef ReturnStatusType (DG_API  *DG_INIT_USB_LINK ) (short *dpaHandle, USBLinkType *USBData);
typedef ReturnStatusType (DG_API  *DG_RESTORE_DPA ) (short dpaHandle);
typedef ReturnStatusType (DG_API  *DG_INIT_DATA_LINK ) ( short dpaHandle, InitDataLinkType *InitDataLinkData );
typedef ReturnStatusType (DG_API  *DG_CHECK_DATA_LINK ) ( short dpaHandle, char *cVersion );
typedef ReturnStatusType (DG_API  *DG_LOAD_DPA_BUFFER )   ( short dpaHandle, unsigned char *bData, unsigned short wLength, unsigned short wOffset );
typedef ReturnStatusType (DG_API  *DG_READ_DPA_BUFFER )   ( short dpaHandle, unsigned char *bData, unsigned short wLength, unsigned short wOffset );
typedef ReturnStatusType (DG_API  *DG_LOAD_MAILBOX ) ( short dpaHandle, LoadMailBoxType *pLoadMailBoxData );
typedef ReturnStatusType (DG_API  *DG_UNLOAD_MAILBOX )  ( short dpaHandle, MailBoxType *pMailBoxHandle );
typedef ReturnStatusType (DG_API  *DG_TRANSMIT_MAILBOX_ASYNC ) ( short dpaHandle, MailBoxType *pMailBoxHandle );
typedef ReturnStatusType (DG_API  *DG_TRANSMIT_MAILBOX ) ( short dpaHandle, MailBoxType *pMailBoxHandle );
typedef ReturnStatusType (DG_API  *DG_UPDATE_MAILBOX ) ( short dpaHandle, MailBoxType *pMailBoxHandle, unsigned short flag );
typedef ReturnStatusType (DG_API  *DG_UPDATE_MAILBOX_ASYNC ) ( short dpaHandle, MailBoxType *pMailBoxHandle, unsigned short flag );

typedef ReturnStatusType (DG_API  *DG_READ_TIMER) ( short dpaHandle, DWORD *time );
typedef ReturnStatusType (DG_API  *DG_RECEIVE_MAILBOX ) ( short dpaHandle, MailBoxType *pMailBoxHandle ); 

typedef ReturnStatusType (DG_API  *DG_DISABLEDATA_LINK ) (short dpaHandle, BYTE bProtocol);
typedef ReturnStatusType (DG_API  *DG_RESET_DPA )(short dpaHandle,ResetType *ResetData);
typedef ReturnStatusType (DG_API  *DG_READDATALINK_DPA) (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
typedef ReturnStatusType (DG_API  *DG_GETSERIALNUMBER_DPA) ( short dpaHandle, char  *pData);
typedef ReturnStatusType (DG_API  *DG_STOREDATALINK)(short dpaHandle, BYTE bProtocol);
typedef ReturnStatusType (DG_API  *DG_FNIT)(short dpaHandle, FInitType *pFInitData);
typedef ReturnStatusType (DG_API  *DG_CHECKLOCK)(short dpaHandle,char *szSearchString,BYTE *bFound);
typedef ReturnStatusType (DG_API  *DG_READVOLTAGE)(short dpaHandle, BYTE bProtocol, BYTE *bTens, BYTE *bHundredths);
typedef ReturnStatusType (DG_API  *DG_CONFIGUREPROTOCOL)(short dpaHandle,BYTE bProtocol, void  *ConfigureData);
typedef ReturnStatusType (DG_API  *DG_SETMSGLOOKUPTABLE)( short dpaHandle, BYTE bDataCount, unsigned char *pData);
typedef ReturnStatusType (DG_API  *DG_GETLOCKS) ( short dpaHandle, char  *locks);
typedef ReturnStatusType (DG_API  *DG_WRITELOCKS) ( short dpaHandle, char  *locks);
typedef ReturnStatusType (DG_API  *DG_GETCONNECTSTATUS) ( short dpaHandle, short *c);
//extended definitions
#ifdef J2534_DEVICE_DPA5_EURO5
typedef ReturnStatusType (DG_API  *DG_CONFIGURETRANSPORTPROTOCOL)(short dpaHandle, ConfigureTransportType*);
typedef ReturnStatusType (DG_API  *DG_INIT_DATA_LINK_EXT ) ( short dpaHandle, InitDataLinkTypeExt *InitDataLinkData );
typedef ReturnStatusType (DG_API  *DG_LOADMAILBOXEXT) (short dpaHandle,LoadMailBoxTypeExt *pLoadMailBoxData);
typedef ReturnStatusType (DG_API  *DG_UNLOADMAILBOXEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
typedef ReturnStatusType (DG_API  *DG_TRANSMITMAILBOXEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
typedef ReturnStatusType (DG_API  *DG_TRANSMITMAILBOXASYNCEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
typedef ReturnStatusType (DG_API  *DG_UPDATETRANSMITMAILBOXEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
typedef ReturnStatusType (DG_API  *DG_UPDATETRANSMITMAILBOXASYNCEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
typedef ReturnStatusType (DG_API  *DG_UPDATETRECEIVEMAILBOXEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
typedef ReturnStatusType (DG_API  *DG_UPDATETRECEIVEMAILBOXASYNCEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
typedef ReturnStatusType (DG_API  *DG_RECEIVEMAILBOXEXT) (short dpaHandle,MailBoxTypeExt *pMailBoxHandle);
#endif
#if defined(J2534_DEVICE_DBRIDGE) || defined(J2534_DEVICE_DBRIDGE0305) || defined(J2534_DEVICE_NETBRIDGE)
typedef ReturnStatusType (WINAPI  *DG_READ_AREA) ( short dpaHandle, BYTE bArea, char *data );
typedef ReturnStatusType (WINAPI  *DG_CLEAR_AREA) ( short dpaHandle, BYTE bArea );
typedef ReturnStatusType (WINAPI  *DG_WRITE_AREA) ( short dpaHandle, BYTE bArea, char *data );
#if defined(J2534_DEVICE_SVCI)
typedef ReturnStatusType (WINAPI  *DG_ESTABLISH_COMMUNICATION) ( short dpaHandle, EstablishCommunicationType *pECData );
#endif
#endif
#endif

