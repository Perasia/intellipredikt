/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DeviceBase.cpp
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This is the base class for deriving the device 
*              specific Class.
* Note:
*
*******************************************************************************/

//Includes
#include "StdAfx.h"
#include "DeviceBase.h"
#include "Cayman1xT.h"

#define DEVICEBASE_ERROR_TEXT_SIZE        120

//constants and header files to include depending on vendor lib being built
#ifndef J2534_DEVICE_DPA5_EURO5
/* Enumerations for UART baud rates 
   support by Dearborn Cayman Adapters */
typedef enum UARTBaud
{
    UART_BAUD_4800 = 0x00,
    UART_BAUD_7812 = 0x01,
    UART_BAUD_8192 = 0x02, 
    UART_BAUD_9600 = 0x03,
    UART_BAUD_9615 = 0x04,
    UART_BAUD_9800 = 0x05,
    UART_BAUD_10000 = 0x06,
    UART_BAUD_10400 = 0x07,
    UART_BAUD_10416 = 0x07,
    UART_BAUD_10870 = 0x08,
    UART_BAUD_11905 = 0x09,
    UART_BAUD_12500 = 0x0a,
    UART_BAUD_13158 = 0x0b,
    UART_BAUD_13889 = 0x0c,
    UART_BAUD_14706 = 0x0d,
    UART_BAUD_15625 = 0x0e,
    UART_BAUD_19200 = 0x0f,
    UART_BAUD_38400 = 0x10,
    UART_BAUD_62500 = 0x11,
    UART_BAUD_125000 = 0x12,
    UART_BAUD_57600 = 0x13,
    UART_BAUD_115200 = 0x14
} UARTBaud;
#else
/* Enumerations for UART baud rates 
   support by Dearborn PX3 and DPA5 adapters */
typedef enum UARTBaud
{
    UART_BAUD_4800 = 0x07,
    UART_BAUD_7812 = 0x05,
    UART_BAUD_8192 = 0x04, 
    UART_BAUD_9600 = 0x00,
    UART_BAUD_9615 = 0x0f,
    UART_BAUD_9800 = 0x10,
    UART_BAUD_10000 = 0x06,
    UART_BAUD_10400 = 0x02,
    UART_BAUD_10416 = 0x02,
    UART_BAUD_10870 = 0x08,
    UART_BAUD_11905 = 0x09,
    UART_BAUD_12500 = 0x0a,
    UART_BAUD_13158 = 0x0b,
    UART_BAUD_13889 = 0x0c,
    UART_BAUD_14706 = 0x0d,
    UART_BAUD_15625 = 0x0e,
    UART_BAUD_19200 = 0x01,
    UART_BAUD_38400 = 0x03,
    UART_BAUD_62500 = 0x11,
    UART_BAUD_125000 = 0x12,
    UART_BAUD_57600 = 0x13,
    UART_BAUD_115200 = 0x14
} UARTBaud;
#endif


CRITICAL_SECTION CDeviceBaseSection;


struct UartEntry
{
    unsigned long BaudRate;
    UARTBaud bParam0;
};

struct UartEntry aUartBaudRate[] = {
    {4800,   UART_BAUD_4800},
    {7812,   UART_BAUD_7812},
    {8192,   UART_BAUD_8192}, 
    {9600,   UART_BAUD_9600},
    {9615,   UART_BAUD_9615},
    {9800,   UART_BAUD_9800},
    {10000,  UART_BAUD_10000},
    {10400,  UART_BAUD_10400},
    {10416,  UART_BAUD_10416},
    {10870,  UART_BAUD_10870},
    {11905,  UART_BAUD_11905},
    {12500,  UART_BAUD_12500},
    {13158,  UART_BAUD_13158},
    {13889,  UART_BAUD_13889},
    {14706,  UART_BAUD_14706},
    {15625,  UART_BAUD_15625},
    {19200,  UART_BAUD_19200},
    {38400,  UART_BAUD_38400},
    {62500,  UART_BAUD_62500},
    {125000, UART_BAUD_125000},
    {57600,  UART_BAUD_57600},
    {115200, UART_BAUD_115200}
};

//-----------------------------------------------------------------------------
//  Function Name   : CDeviceBase()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
CDeviceBase::CDeviceBase(CDebugLog * pclsDebugLog, CDataLog * pclsDataLog)
{
       
    InitializeCriticalSection(&CDeviceBaseSection);           // Used in SetLastErrorText() and WriteLogMsg()
    m_pclsLog = pclsDebugLog;
    m_pclsDataLog = pclsDataLog;

    // Write to Log File.
    WriteLogMsg("CDeviceBase()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("CDeviceBase()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : ~CDeviceBase()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
CDeviceBase::~CDeviceBase()
{
    // Write to Log File.
    WriteLogMsg("~CDeviceBase()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("~CDeviceBase()", DEBUGLOG_TYPE_COMMENT, "End");
}

//-----------------------------------------------------------------------------
//  Function Name   : vOpenDevice()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vOpenDevice(J2534REGISTRY_CONFIGURATION * punDeviceSettings)
{
    //*************************** IMPORTANT NOTE ******************************
    // Implement the following:
    // 1. Reset internal Data Structure if any.
    // 2. Reset Device Driver specific Data Structures if any. 
    // 3. If device already opened, return J2534_ERR_DEVICE_IN_USE.
    // 4. Open the Connection to Device otherwise return 
    //    J2534_ERR_DEVICE_NOT_CONNECTED.
    // 5. Hard Reset the Devive if feature supported by Device Driver.
    // 6. If no error, return J2534_STATUS_NOERROR.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vOpenDevice()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vOpenDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);

    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vCloseDevice()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vCloseDevice()
{
    //**************************** IMPORTANT NOTE *****************************
    // Always close the device and cleanup or whatever
    // it takes to restore the state before the OpenDevice was called.
    // If the device is not physically connected, still
    // reset all the internal data structures and cleanup so that the 
    // next call to OpenDevice() could be carried out successfully in an ideal
    // situation.
    //
    // Implement the following:
    // 1. Check if the device is open. If not, return J2534_STATUS_NOERROR.
    // 2. Close connection to Device. (Even if the device is not physically
    //    connected. 
    // 3. Release and cleanup any resource that were created after call to 
    //    vOpenDevice() even if the device is not physically connected.
    // 4. Return an appropriate error if there is any, otherwise return
    //    J2534_STATUS_NOERROR. Always cleanup and release any resource
    //    even if you are returning "Device not Connected". In other words 
    //    restore the state prior to vOpenDevice() was called. If for some reason
    //    the state cannot be restored (e.g potential memory leak, etc.) then
    //    never return J2534_STATUS_NOERROR or J2534_DEVICE_NOT_CONNECTED.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vCloseDevice()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vCloseDevice()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vConnectProtocol()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : This function shall connect to a requested Protocol
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vConnectProtocol(
                                        J2534_PROTOCOL  enProtocolID,
                                        unsigned long   ulFlags,
                                        unsigned long   ulBaudRate,
                                        DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
                                        LPVOID          pVoid,
                                        unsigned long   *pulChannelID
                                        )
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. enProtocolID - Refer to J2534 Documentation
    // 2. ulFlags      - Refer to J2534 Documentation (Manuf. specific bits could
    //                   be used for device specific e.g.- Channel # incase of
    //                   more than one channel of same Protocol is supported by
    //                   a device).
    // 3. ulBaudRate   - Refer to J2534 Documentation.
    // 4. pfnCallback  - Callback to be called when a message is received.
    // 5. pVoid        - This pointer should be saved along with the Callback
    //                   so as to be able to send this pointer value back as
    //                   a parameter when the Callback is called.
    // 6. pulChannelID - ChannelID to be returned by this function for future
    //                   references to the channel connected.
    //
    // IMPLEMENT THE FOLLOWING:
    //
    // 1. Check to see if the protocol is supported by this device otherwise
    //    return J2534_ERR_NOT_SUPPORTED.
    // 2. Check to if requested protocol is mutually exclusive. e.g. if CAN
    //    is already connected and user requested ISO15765 on the same pin
    //    or vice versa. If there is a resource conflict return 
    //    J2534_INVALID_PROTOCOL_ID.
    // 3. Check to see if the requested Baud Rate is supported by this device 
    //    otherwise return J2534_ERR_NOT_SUPPORTED. 
    // 4. Check to see if the feature requested in Flags that are relevent to
    //    the requested protocol are supported otherwise error out appropriatly.
    //    by this device otherwise return J2534_ERR_NOT_SUPPORTED.
    // 5. Check to see manufacturer (device) specific flag bits. If not correct,
    //    return J2534_ERR_INVALID_FLAGS.
    // 6. Check to see if the given channel is in use. If yes, return 
    //    J2534_ERR_CHANNEL_IN_USE.
    // 7. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 8. Open and Initialize the Protocol Connection with the Device Driver as
    //    as per the request. Check the flag bits that is relevent to the 
    //    requested protocol and process it accordingly. Perform Go On Bus at
    //    the requested Baud Rate.
    // 9. Setup a callback to be called when a message for this protocol and
    //    and channel is received. The callback when called upon receiving a 
    //    message shall send the PASSTHRU_MSG structure completely filled and 
    //    formatted as desired by the J2534 standard for each protocol.
    // 10.Default Block all messages if supported by the driver otherwise it 
    //    will be blocked anyways by the software Filter in J2534 layer.
    // 11.If successful send the Channel ID back to caller
    //    for future reference and return J2534_STATUS_NOERROR. 
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vConnectProtocol()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vConnectProtocol()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}
//-----------------------------------------------------------------------------
//  Function Name   : vStartAllPassFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStartAllPassFilter(unsigned long   ulChannelID,
                                            unsigned long ulFlags)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID -  Channel ID returned by vConnectProtocol() for reference.
    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Check to see if the ChannelID is valid. If not, return
    //    J2534_ERR_INVALID_CHANNEL_ID.
    // 3. Disconnect this protocol and clean up and release any resource that
    //    were created after call to vConnectProtocol() for this ChannelID.
    //    If successful, return J2534_STATUS_NOERROR.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************
    // Write to Log File.
    WriteLogMsg("vStartAllPassFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStartAllPassFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopAllPassFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStopAllPassFilter(unsigned long    ulChannelID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID -  Channel ID returned by vConnectProtocol() for reference.
    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Check to see if the ChannelID is valid. If not, return
    //    J2534_ERR_INVALID_CHANNEL_ID.
    // 3. Disconnect this protocol and clean up and release any resource that
    //    were created after call to vConnectProtocol() for this ChannelID.
    //    If successful, return J2534_STATUS_NOERROR.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vStopAllPassFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStopAllPassFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vDisconnectProtocol()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vDisconnectProtocol(unsigned long   ulChannelID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID -  Channel ID returned by vConnectProtocol() for reference.
    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Check to see if the ChannelID is valid. If not, return
    //    J2534_ERR_INVALID_CHANNEL_ID.
    // 3. Disconnect this protocol and clean up and release any resource that
    //    were created after call to vConnectProtocol() for this ChannelID.
    //    If successful, return J2534_STATUS_NOERROR.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vDisconnectProtocol()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vDisconnectProtocol()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vGetRevision()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vGetRevision(unsigned long ulDeviceID,
                                        char *pchFirmwareVersion,
                                        char *pchDllVersion,
                                        char *pchApiVersion)
{
    // Write to Log File.
    WriteLogMsg("vGetRevision()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vGetRevision()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vGetSerialNum()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vGetSerialNum(unsigned long ulDeviceID,
                                        char *pchKRSerialNum)
{
    // Write to Log File.
    WriteLogMsg("vGetSerialNum()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vGetSerialNum()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}   

//-----------------------------------------------------------------------------
//  Function Name   : vGetHWVersion()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vGetHWVersion(unsigned long ulDeviceID,
                                        char *pchHWVersion)
{
    // Write to Log File.
    WriteLogMsg("vGetHWVersion()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vGetHWVersion()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteKRString()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vWriteKRString(unsigned long ulDeviceID,
                                        char *pchKRString)
{
    // Write to Log File.
    WriteLogMsg("vWriteKRString()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vWriteKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vReadKRString()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vReadKRString(unsigned long ulDeviceID,
                                        char *pchKRString)
{
    // Write to Log File.
    WriteLogMsg("vReadKRString()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vReadKRString()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vWriteMsgs()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vWriteMsgs(unsigned long    ulChannelID,
                                   PASSTHRU_MSG     *pstPassThruMsg,
                                   unsigned long    *pulNumMsgs)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID    - Channel ID returned by vConnectProtocol() for reference.
    // 2. pstPassThruMsg - Pointer to the PASSTHRU_MSG structure that contains the
    //                     information of the message to be sent out.
    // 3. pulNumMsgs     - Number of msgs. contained in the buffer pointed by
    //                     pstPassThruMsg.
    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Write the requested msg. out on the Device Bus. If the message cannot 
    //    be written out on the Device Bus within a constant timeout value, 
    //    return J2534_ERR_TIMEOUT. Always confirm that the requested msgs. 
    //    have been transmitted successfully on the Bus before returning
    //    J2534_STATUS_NOERROR. If the msg. is not successfully transmitted
    //    retry until IOCTL with CLEAR_TX_BUFFER or PassThruDisconnect or 
    //    PassThruClose is called.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vWriteMsgs()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vWriteMsgs()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : StartPeriodic()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStartPeriodic(unsigned long    ulChannelID, 
                                       PASSTHRU_MSG     *pstMsg, 
                                       unsigned long    ulTimeInterval, 
                                       unsigned long    *pulPeriodicRefID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID      - Channel ID returned by vConnectProtocol() for reference.
    // 2. pstMsg           - Points to message for Periodic.
    // 3. ulTimeInterval   - Time Interval for Periodic msg..
    // 4. pulPeriodicRefID - Return the Periodic ID for reference.

    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Check to see if the Time Interval is valid. If not, return 
    //    J2534_ERR_INVALID_TIME_INTERVAL.
    // 3. If the device does not support any functionality required by J2534, 
    //    return J2534_ERR_NOT_SUPPORTED.
    // 4. If successfully started Periodic, return PeriodicID for reference and
    //    return J2534_STATUS_NOERROR. 
    //    THE VALID PERIODIC ID RETURNED SHOULD NOT BE 0.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vStartPeriodic()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStartPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStopPeriodic()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStopPeriodic(unsigned long ulChannelID,
                                      unsigned long ulPeriodicRefID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID      - Channel ID returned by vConnectProtocol() for 
    //                       reference.
    // 2. ulPeriodicRefID  - Periodic ID returned by vStartMsgFilter() for 
    //                       reference.

    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check if the Device is connected. If not, return 
    //    J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Stop the Periodic referenced by ulPeriodicRefID and ulChannelID.
    // 3. Return J2534_STATUS_NOERROR if successful.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vStopPeriodic()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStopPeriodic()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vStartFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStartFilter(
                               unsigned long    ulChannelID,
                               J2534_FILTER     enFilterType, 
                               PASSTHRU_MSG     *pstMask, 
                               PASSTHRU_MSG     *pstPattern,
                               PASSTHRU_MSG     *pstFlowControl,
                               unsigned long    *pulFilterRefID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID    - Channel ID returned by vConnectProtocol() for reference.
    // 2. enFilterType   - Filter Type (PASS, BLOCK or FLOW_CONTROL).
    // 3. pstMask        - Points to message mask.
    // 4. pstPattern     - Points to message pattern.
    // 5. pstFlowControl - Points to message FlowControl.
    // 6. pulFilterRefID - Return the Filter ID for reference.

    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check to see Physical Connection by calling IsDeviceConnected(). If
    //    not connected, J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Only implement PASS or FLOW_CONTROL filter. If the device is not 
    //    capable of setting PASS filter, it is still not a problem because the 
    //    messages will be software filtered by J2534 layer. 
    //    THE VALID FILTER ID RETURNED SHOULD NOT BE 0.
    // 3. Return J2534_STATUS_NOERROR if successful.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vStartFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStartFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : StopFilter()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vStopFilter(
                               unsigned long    ulChannelID,
                               unsigned long    ulFilterRefID)
{
    //*************************** IMPORTANT NOTE ******************************
    // PARAMETER DESCRIPTION:
    //
    // 1. ulChannelID    - Channel ID returned by vConnectProtocol() for reference.
    // 2. ulFilterRefID  - Filter ID returned by vStartMsgFilter() for reference.

    //
    // IMPLEMENT THE FOLLOWING:
    
    // 1. Check if the Device is connected. If not, return 
    //    J2534_ERR_DEVICE_NOT_CONNECTED.
    // 2. Stop the Filter referenced by ulFilterRefID and ulChannelID.
    // 3. Return J2534_STATUS_NOERROR if successful.
    // (Anytime if there is an error that is not defined in J2534ERROR,
    //  call SetLastErrorText() to set the error text and return 
    //  J2534_ERR_FAILED)
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vStopFilter()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vStopFilter()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : Ioctl()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vIoctl(unsigned long    ulChannelID,
                               J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput)
{
    // Write to Log File.
    WriteLogMsg("vIoctl()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vIoctl()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vProgrammingVoltage()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
J2534ERROR CDeviceBase::vProgrammingVoltage(unsigned long ulDeviceID,
                                        unsigned long ulPin,
                                        unsigned long ulVoltage)
{
    // Write to Log File.
    WriteLogMsg("vProgrammingVoltage()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vProgrammingVoltage()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(J2534_ERR_NOT_SUPPORTED);
}

//-----------------------------------------------------------------------------
//  Function Name   : vIsDeviceConnected()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : 
//-----------------------------------------------------------------------------
bool CDeviceBase::vIsDeviceConnected(bool bFlag)
{
    // *************************** IMPORTANT NOTE *****************************
    // This is like a ping like function to see if the Device is responding.
    // If there is no way of pinging the device because of the Driver 
    // constraints, please always return true.
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vIsDeviceConnected()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vIsDeviceConnected()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(true);
}

bool CDeviceBase::vSetDataLogging(bool bFlag)
{
    // *************************** IMPORTANT NOTE *****************************
    // This is like a ping like function to see if the Device is responding.
    // If there is no way of pinging the device because of the Driver 
    // constraints, please always return true.
    //*************************************************************************

    // Write to Log File.
    WriteLogMsg("vSetDataLogging()", DEBUGLOG_TYPE_COMMENT, "Start");

    // Write to Log File.
    WriteLogMsg("vSetDataLogging()", DEBUGLOG_TYPE_ERROR, "returned 0x%02X", J2534_ERR_NOT_SUPPORTED);
    return(true);
}

J2534ERROR CDeviceBase::vCheckLock(char *pszLock,unsigned char *pucResult) 
{

    return(J2534_ERR_NOT_SUPPORTED);

}

J2534ERROR CDeviceBase::vAddLock(char *pszLock,unsigned char *pucResult) 
{

    return(J2534_ERR_NOT_SUPPORTED);

}

J2534ERROR CDeviceBase::vDeleteLock(char *pszLock,unsigned char *pucResult) 
{

    return(J2534_ERR_NOT_SUPPORTED);

}

J2534ERROR CDeviceBase::vTurnAuxOff() 
{

    return(J2534_ERR_NOT_SUPPORTED);

}

J2534ERROR CDeviceBase::vTurnAuxOn() 
{

    return(J2534_ERR_NOT_SUPPORTED);

}

#ifdef J2534_DEVICE_DBRIDGE
J2534ERROR CDeviceBase::vDataStorage(J2534IOCTLID enumIoctlID, unsigned long *pulDataStorage, int iDataSize)
{
    return(J2534_ERR_NOT_SUPPORTED);
}
#endif


bool CDeviceBase::IsChannelConnected(unsigned long ulChannelID)
{

    return(J2534_ERR_NOT_SUPPORTED);

}


//-----------------------------------------------------------------------------
//  Function Name   : WriteLogMsg()
//  Input Params    : chFunctionName, MsgType, chMsg
//  Output Params   : -
//  Return          : -
//  Description     : Write message to log file
//-----------------------------------------------------------------------------
void CDeviceBase::WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...)
{
    if (m_pclsLog != NULL && m_pclsLog->m_pfdLogFile != NULL)
    {
        EnterCriticalSection(&CDeviceBaseSection); // Make processing parameters thread-safe
        char buf[DEVICEBASE_MAX_BUFFER_SIZE];
        va_list ap;
        va_start(ap, chMsg);
        vsprintf_s(buf, chMsg, ap);
        va_end(ap);
        buf[DEVICEBASE_ERROR_TEXT_SIZE] = '\0'; // We have our limits
        m_pclsLog->Write("DeviceBase.cpp", chFunctionName, MsgType, buf);
        LeaveCriticalSection(&CDeviceBaseSection);
    }
}

//-----------------------------------------------------------------------------
//  Function Name   : IsUartBaudRateValid
//  Input Params    : UART Baud Rate
//  Output Params   : Returns true/false if requested UART baud rate is valid
//  Description     : This function checks the entries in the zzz structure  
//                    for a match of the requested baud rate. If present, returns
//                    true; othere wise returns false.
//-----------------------------------------------------------------------------
J2534ERROR IsUartBaudRateValid(unsigned long ulBaudRate)
{
    int i;
    int iUartBaudRateCount = sizeof(aUartBaudRate)/sizeof(UartEntry);
    for (i = 0; i < iUartBaudRateCount; i++)
    {
        if(aUartBaudRate[i].BaudRate == ulBaudRate)
        {
            break;

        }
    }
    return i < iUartBaudRateCount ? J2534_STATUS_NOERROR : J2534_ERR_INVALID_BAUDRATE; // Guard against 'i' being out-of-range
}

//-----------------------------------------------------------------------------
//  Function Name   : GetUartBaudRateParm
//  Input Params    : UART Baud Rate
//  Output Params   : Returns parm used by the tool firmware corisponding to the
//                    requested UART baud rate.
//  Description     : This function checks the entries in the zzz structure  
//                    for a match of the requested baud rate. If present, returns
//                    parm-value; otherewise returns and invalid value.
//-----------------------------------------------------------------------------
BYTE GetUartBaudRateParm(unsigned long ulBaudRate)
{
    int i;
    int iUartBaudRateCount = sizeof(aUartBaudRate)/sizeof(UartEntry);
    for (i = 0; i < iUartBaudRateCount; i++)
    {
        if(aUartBaudRate[i].BaudRate == ulBaudRate)
        {
            break;

        }
    }
    return i < iUartBaudRateCount ? aUartBaudRate[i].bParam0 : -1; // Guard against 'i' being out-of-range
}
