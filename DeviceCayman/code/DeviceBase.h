/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: DeviceBase.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This the header file for CDeviceBase.cpp
* Note:
*
*******************************************************************************/

#ifndef _CDEVICEBASE_H_
#define _CDEVICEBASE_H_

#ifdef J2534_0404
#include "J2534.h"
#endif
#ifdef J2534_0305
#include "J2534_0305.h"
#endif
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#include "J2534_0500.h"
#endif'

#include "DebugLog.h"
#include "DataLog.h"
#include "J2534Registry.h"

//****************************** IMPORTANT NOTE ******************************

// This file contains the declaration base class CDeviceBase which will be 
// used by the derived class to implement device specifics. 

// The deriving class will implement each virtual function as required for a 
// specific device. The function returning DEVICEBASE_ERR_FAILED must call 
// SetLastErrorText() to set the appropriate Error Text before returnin from 
// the failed function. It is recommended that since the derived class header 
// file is included by the external application, it should not contain device 
// specific information that is for internal use and not relevent to external 
// applications. Please create a separate header file for internal use. Refer 
// to derived class for DPA device to use as a template or to see an example.

// Use the following convention to name the class derived from this Class:
// CDevice<Device Name>

//****************************************************************************

// Extended Errors for this device
//#define DEVICECAYMAN_STATUS_NOERROR   0

#define DEVICEBASE_MAX_BUFFER_SIZE      1024
#define DEVICEBASE_ERROR_TEXT_SIZE      120 // Error text size.
// typedef the Rx Callback.
typedef void (*DEVICEBASE_CALLBACK_RX_FUNC)(PASSTHRU_MSG *pstPassThruMsg, LPVOID lpVoid);
// SCONFIG Structur

// CDeviceCayman Class
class CDeviceBase
{
public:
    SCONFIG_LIST *pInput;
    CDeviceBase(CDebugLog * pclsDebugLog=NULL, CDataLog * pclsDataLog=NULL);
    ~CDeviceBase();
    virtual J2534ERROR vOpenDevice(J2534REGISTRY_CONFIGURATION * punDeviceSettings);

    virtual J2534ERROR vCloseDevice();

	virtual J2534ERROR vConnectProtocol(J2534_PROTOCOL  enProtocolID,
		unsigned long   ulFlags,
		unsigned long   ulBaudRate,
		DEVICEBASE_CALLBACK_RX_FUNC pfnCallback,
		LPVOID          pVoid,
		unsigned long   *pulChannelID);

    virtual J2534ERROR vStartAllPassFilter(unsigned long    ulChannelID,
                                            unsigned long ulFlags);
    virtual J2534ERROR vStopAllPassFilter(unsigned long ulChannelID);
    virtual J2534ERROR vDisconnectProtocol(unsigned long    ulChannelID);

    virtual J2534ERROR vGetRevision(unsigned long ulDeviceID,
                                    char *pchFirmwareVersion,
                                    char *pchDllVersion,
                                    char *pchApiVersion);
    virtual J2534ERROR vGetSerialNum(unsigned long ulDeviceID,
                                        char *pchKRSerialNum);
    virtual J2534ERROR vGetHWVersion(unsigned long ulDeviceID,
                                        char *pchHWVersion);
    virtual J2534ERROR vWriteKRString(unsigned long ulDeviceID,
                                        char *pchKRString);
    virtual J2534ERROR vReadKRString(unsigned long ulDeviceID,
                                        char *pchKRString);

    virtual J2534ERROR vWriteMsgs(unsigned long ulChannelID,
                                  PASSTHRU_MSG  *pstPassThruMsg, 
                                  unsigned long *pulNumMsgs);

    virtual J2534ERROR vStartPeriodic(unsigned long ulChannelID, 
                                      PASSTHRU_MSG  *pstMsg, 
                                      unsigned long ulTimeInterval, 
                                      unsigned long *pulPeriodicRefID);

    virtual J2534ERROR vStopPeriodic(unsigned long  ulChannelID,
                                     unsigned long  ulPeriodicRefID);

    virtual J2534ERROR vStartFilter(unsigned long   ulChannelID,
                                    J2534_FILTER    enFilterType, 
                                    PASSTHRU_MSG    *pstMask, 
                                    PASSTHRU_MSG    *pstPattern,
                                    PASSTHRU_MSG    *pstFlowControl,
                                    unsigned long   *pulFilterRefID);
    
    virtual J2534ERROR vStopFilter(unsigned long    ulChannelID,
                                   unsigned long    ulFilterRefID);
    
    virtual J2534ERROR vIoctl(unsigned long ulChannelID,
                                J2534IOCTLID enumIoctlID,
                               void *pInput,
                               void *pOutput);

    virtual J2534ERROR vProgrammingVoltage(unsigned long ulDeviceID,
                                        unsigned long ulPin,
                                        unsigned long ulVoltage);

    virtual bool       vIsDeviceConnected(bool bFlag = true);
    virtual bool       vSetDataLogging(bool bFlag);

    virtual J2534ERROR vCheckLock(char *pszLock,unsigned char *pucResult); 
    virtual J2534ERROR vAddLock(char *pszLock,unsigned char *pucResult);
    virtual J2534ERROR vDeleteLock(char *pszLock,unsigned char *pucResult);
    virtual J2534ERROR vTurnAuxOff(); 
    virtual J2534ERROR vTurnAuxOn();
#ifdef J2534_DEVICE_DBRIDGE
    virtual J2534ERROR vDataStorage(J2534IOCTLID enumIoctlID, unsigned long *pulDataStorage, int iDataSize);
#endif
    virtual void WriteLogMsg(char * chFunctionName, unsigned long MsgType, char * chMsg, ...);

    virtual bool IsChannelConnected(unsigned long ulChannelID);

    CDebugLog          *m_pclsLog;
    CDataLog           *m_pclsDataLog;
};

J2534ERROR IsUartBaudRateValid(unsigned long ulBaudRate);
BYTE       GetUartBaudRateParm(unsigned long ulBaudRate);
#endif
