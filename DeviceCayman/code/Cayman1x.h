/**************************************/
/*                                    */
/* Dearborn Group, Copyright (c) 2005 */
/* Dearborn Cayman Adapter          */
/* Version 1.0                        */
/*                                    */
/**************************************/

#ifndef _CAYMAN1XH
#define _CAYMAN1XH

#include "Cayman1xT.h"

/***********************/
/* function prototypes */
/***********************/
#ifdef __cplusplus
extern "C" {
     ReturnStatusType DllExport WINAPI InitCommLink (short *dpaHandle,CommLinkType *CommLinkData);
     ReturnStatusType DllExport WINAPI InitUSBLink (short *dpaHandle, USBLinkType *USBLinkData);
	 ReturnStatusType DllExport WINAPI RestoreUSBLink (short dpaHandle);
	 ReturnStatusType DllExport WINAPI RestoreCommLink (short dpaHandle);
     
     ReturnStatusType DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
     ReturnStatusType DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
     ReturnStatusType DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
     ReturnStatusType DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData, WORD wLength, WORD wOffset);
     ReturnStatusType DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData, WORD wLength, WORD wOffset);
     ReturnStatusType DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString, BYTE *bFound);
     ReturnStatusType DllExport WINAPI GoToLoader(short dpaHandle);
     ReturnStatusType DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
     ReturnStatusType DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
     ReturnStatusType DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
     ReturnStatusType DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
     ReturnStatusType DllExport WINAPI PauseTimer (short dpaHandle);
     ReturnStatusType DllExport WINAPI ResumeTimer (short dpaHandle);
     ReturnStatusType DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
     ReturnStatusType DllExport WINAPI ConfigureProtocol (short dpaHandle, BYTE bProtocol, void  *ConfigureData);
	 ReturnStatusType DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
     ReturnStatusType DllExport WINAPI SetBaudRate (short dpaHandle, SetBaudRateType *SetBaudRateData);
     ReturnStatusType DllExport WINAPI ResetDPA(short dpaHandle, ResetType *ResetData);
     ReturnStatusType DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName, BYTE *bseed);
	 ReturnStatusType DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bProtocol, BYTE *bTens, BYTE *bHundredths);
     
	 ReturnStatusType DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
	 ReturnStatusType DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);	 
	 ReturnStatusType DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
	 ReturnStatusType DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
	 ReturnStatusType DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);
}
#else

     ReturnStatusType DllExport WINAPI InitCommLink (short *dpaHandle,CommLinkType *CommLinkData);
     ReturnStatusType DllExport WINAPI InitUSBLink (short *dpaHandle, USBLinkType *USBLinkData);
	 ReturnStatusType DllExport WINAPI RestoreUSBLink (short dpaHandle);

     ReturnStatusType DllExport WINAPI RestoreCommLink (short dpaHandle);

     ReturnStatusType DllExport WINAPI InitDataLink (short dpaHandle,InitDataLinkType *InitDataLinkData);
     ReturnStatusType DllExport WINAPI CheckDataLink (short dpaHandle,char *cVersion);
     ReturnStatusType DllExport WINAPI ReadDPAChecksum (short dpaHandle,unsigned int *uiDPAChecksum);
     ReturnStatusType DllExport WINAPI LoadDPABuffer (short dpaHandle,BYTE *bData, WORD wLength, WORD wOffset);
     ReturnStatusType DllExport WINAPI ReadDPABuffer (short dpaHandle,BYTE *bData, WORD wLength, WORD wOffset);
     ReturnStatusType DllExport WINAPI CheckLock (short dpaHandle,char *szSearchString, BYTE *bFound);
     ReturnStatusType DllExport WINAPI GoToLoader(short dpaHandle);
     ReturnStatusType DllExport WINAPI LoadMailBox (short dpaHandle,LoadMailBoxType *pLoadMailBoxData);
     ReturnStatusType DllExport WINAPI TransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI TransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransMailBoxData (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransMailBoxDataAsync (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UpdateTransmitMailBox (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateTransmitMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI UpdateReceiveMailBoxAsync (short dpaHandle,MailBoxType *pMailBoxHandle, WORD wUpdateFlag);
     ReturnStatusType DllExport WINAPI ReceiveMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI UnloadMailBox (short dpaHandle,MailBoxType *pMailBoxHandle);
     ReturnStatusType DllExport WINAPI LoadTimer (short dpaHandle,DWORD dwTime);
     ReturnStatusType DllExport WINAPI EnableTimerInterrupt (short dpaHandle,EnableTimerInterruptType *pEnableTimerInterruptData);
     ReturnStatusType DllExport WINAPI SuspendTimerInterrupt (short dpaHandle);
     ReturnStatusType DllExport WINAPI PauseTimer (short dpaHandle);
     ReturnStatusType DllExport WINAPI ResumeTimer (short dpaHandle);
     ReturnStatusType DllExport WINAPI RequestTimerValue (short dpaHandle,DWORD *dwTimerValue);
     ReturnStatusType DllExport WINAPI ConfigureProtocol (short dpaHandle, BYTE bProtocol, void  *ConfigureData);
	 ReturnStatusType DllExport WINAPI FInit (short dpaHandle, FInitType *pFInitData);
     ReturnStatusType DllExport WINAPI SetBaudRate (short dpaHandle, SetBaudRateType *SetBaudRateData);
     ReturnStatusType DllExport WINAPI ResetDPA(short dpaHandle, ResetType *ResetData);
	 ReturnStatusType DllExport WINAPI GetLockSeed (short dpaHandle,char *lockName, BYTE *bseed);
	 ReturnStatusType DllExport WINAPI ReadVoltage (short dpaHandle, BYTE bProtocol, BYTE *bTens, BYTE *bHundredths);

     ReturnStatusType DllExport WINAPI StoreDataLink (short dpaHandle, BYTE bProtocol);
	 ReturnStatusType DllExport WINAPI RestoreDataLink (short dpaHandle, BYTE bProtocol);
	 ReturnStatusType DllExport WINAPI DisableDataLink (short dpaHandle, BYTE bProtocol);
	 ReturnStatusType DllExport WINAPI ReadDataLink (short dpaHandle, BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
	 ReturnStatusType DllExport WINAPI GetConnectStatus (short dpaHandle, short *c);

#endif


#endif

