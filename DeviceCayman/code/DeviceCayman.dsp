# Microsoft Developer Studio Project File - Name="DeviceCayman" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=DeviceCayman - Win32 J2534_Netbridge_Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DeviceCayman.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DeviceCayman.mak" CFG="DeviceCayman - Win32 J2534_Netbridge_Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DeviceCayman - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_CAYMAN_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_CAYMAN_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_VSI_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_VSI_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_KRHTWIRE_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_KRHTWIRE_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_DPA_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_DPA_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_PX3_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_PX3_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_DPA5_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_DPA5_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_SoftBridge_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_SoftBridge_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Wurth_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Wurth_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Delphi_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Delphi_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_UDTruck_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_UDTruck_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_dbriDGe_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_dbriDGe_Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Netbridge_Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DeviceCayman - Win32 J2534_Netbridge_Release" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DEI2005003/DeviceCayman/Src", RAGAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DeviceCayman - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\Debug\DeviceCayman.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_CAYMAN_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_CAYMAN_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_CAYMAN_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CARDONE" /D "NPROD_TEST" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\..\FlashWrite\Debug\DeviceCayman.lib"
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_CAYMAN_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_CAYMAN_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_CAYMAN_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CARDONE" /D "NPROD_TEST" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_VSI_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_VSI_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_VSI_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_VSI_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_VSI_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\..\Debug\DeviceCayman.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceVSI.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_VSI_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_VSI_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_VSI_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_VSI_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_VSI_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_CAYMAN" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Release\DeviceVSI.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_KRHTWIRE_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_KRHTWIRE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceVSI.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceKRHtWire.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_KRHTWIRE_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_KRHTWIRE_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_KRHTWIRE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceVSI.lib"
# ADD LIB32 /nologo /out:"Release\DeviceKRHtWire.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_DPA_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_DPA_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_DPA_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_DPA_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_DPA_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceVSI.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceDPA.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_DPA_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_DPA_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_DPA_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_DPA_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_DPA_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_VSI" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceVSI.lib"
# ADD LIB32 /nologo /out:"Release\DeviceDPA.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_PX3_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_PX3_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_PX3_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_PX3_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_PX3_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX2" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DevicePx-2.lib"
# ADD LIB32 /nologo /out:"Debug\DevicePx-3.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_PX3_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_PX3_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_PX3_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_PX3_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_PX3_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX2" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DevicePx-2.lib"
# ADD LIB32 /nologo /out:"Release\DevicePx-3.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_DPA5_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_DPA5_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_DPA5_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_DPA5_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_DPA5_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DevicePx-3.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceDPA5.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_DPA5_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_DPA5_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_DPA5_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_DPA5_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_DPA5_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DevicePx-3.lib"
# ADD LIB32 /nologo /out:"Release\DeviceDPA5.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_SoftBridge_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_SoftBridge_Debug0"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_SoftBridge_Debug0"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_SoftBridge_Debug0"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_SoftBridge_Debug0"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DevicePx-3.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceSoftBridge.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_SoftBridge_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_SoftBridge_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_SoftBridge_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_SoftBridge_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_SoftBridge_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_PX3" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DevicePx-3.lib"
# ADD LIB32 /nologo /out:"Release\DeviceSoftBridge.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Wurth_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Wurth_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Wurth_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_Wurth_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Wurth_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceSoftBridge.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceWurth.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Wurth_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Wurth_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Wurth_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_Wurth_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Wurth_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_WURTH" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceSoftBridge.lib"
# ADD LIB32 /nologo /out:"Release\DeviceWurth.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Delphi_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Delphi_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Delphi_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_Delphi_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Delphi_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DELPHI" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceSoftBridge.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceDelphi.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Delphi_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Delphi_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Delphi_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_Delphi_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Delphi_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_SOFTBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DELPHI" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceSoftBridge.lib"
# ADD LIB32 /nologo /out:"Release\DeviceDelphi.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_UDTruck_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_UDTruck_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_UDTruck_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_UDTruck_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_UDTruck_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_UDTRUCK" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceWurth.lib"
# ADD LIB32 /nologo /out:"Debug\DevicedbriDGe0305.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_UDTruck_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_UDTruck_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_UDTruck_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_UDTruck_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_UDTruck_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_UDTRUCK" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceWurth.lib"
# ADD LIB32 /nologo /out:"Release\DevicedbriDGe0305.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_dbriDGe_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_dbriDGe_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_dbriDGe_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_dbriDGe_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_dbriDGe_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceWurth.lib"
# ADD LIB32 /nologo /out:"Debug\DevicedbriDGe.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_dbriDGe_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_dbriDGe_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_dbriDGe_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_dbriDGe_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_dbriDGe_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE0305" /D "UD_TRUCK" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT BASE CPP /Fr
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceWurth.lib"
# ADD LIB32 /nologo /out:"Release\DevicedbriDGe.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Netbridge_Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Netbridge_Debug"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Netbridge_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DeviceCayman___Win32_J2534_Netbridge_Debug"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Netbridge_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\J2534Generic\code" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_NETBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Debug\DeviceDPA5.lib"
# ADD LIB32 /nologo /out:"Debug\DeviceNetbridge.lib"

!ELSEIF  "$(CFG)" == "DeviceCayman - Win32 J2534_Netbridge_Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DeviceCayman___Win32_J2534_Netbridge_Release"
# PROP BASE Intermediate_Dir "DeviceCayman___Win32_J2534_Netbridge_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DeviceCayman___Win32_J2534_Netbridge_Release"
# PROP Intermediate_Dir "DeviceCayman___Win32_J2534_Netbridge_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_DPA5" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\J2534Generic\code" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "J2534_DEVICE_NETBRIDGE" /D "J2534_DEVICE_DPA5_EURO5" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"Release\DeviceDPA5.lib"
# ADD LIB32 /nologo /out:"Release\DeviceNetbridge.lib"

!ENDIF 

# Begin Target

# Name "DeviceCayman - Win32 Debug"
# Name "DeviceCayman - Win32 Release"
# Name "DeviceCayman - Win32 J2534_CAYMAN_Debug"
# Name "DeviceCayman - Win32 J2534_CAYMAN_Release"
# Name "DeviceCayman - Win32 J2534_VSI_Debug"
# Name "DeviceCayman - Win32 J2534_VSI_Release"
# Name "DeviceCayman - Win32 J2534_KRHTWIRE_Debug"
# Name "DeviceCayman - Win32 J2534_KRHTWIRE_Release"
# Name "DeviceCayman - Win32 J2534_DPA_Debug"
# Name "DeviceCayman - Win32 J2534_DPA_Release"
# Name "DeviceCayman - Win32 J2534_PX3_Debug"
# Name "DeviceCayman - Win32 J2534_PX3_Release"
# Name "DeviceCayman - Win32 J2534_DPA5_Debug"
# Name "DeviceCayman - Win32 J2534_DPA5_Release"
# Name "DeviceCayman - Win32 J2534_SoftBridge_Debug"
# Name "DeviceCayman - Win32 J2534_SoftBridge_Release"
# Name "DeviceCayman - Win32 J2534_Wurth_Debug"
# Name "DeviceCayman - Win32 J2534_Wurth_Release"
# Name "DeviceCayman - Win32 J2534_Delphi_Debug"
# Name "DeviceCayman - Win32 J2534_Delphi_Release"
# Name "DeviceCayman - Win32 J2534_UDTruck_Debug"
# Name "DeviceCayman - Win32 J2534_UDTruck_Release"
# Name "DeviceCayman - Win32 J2534_dbriDGe_Debug"
# Name "DeviceCayman - Win32 J2534_dbriDGe_Release"
# Name "DeviceCayman - Win32 J2534_Netbridge_Debug"
# Name "DeviceCayman - Win32 J2534_Netbridge_Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\DeviceBase.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceCayman.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE="..\..\..\..\..\..\..\Program Files\Microsoft Visual Studio\VC98\Include\BASETSD.H"
# End Source File
# Begin Source File

SOURCE=.\Cayman1xT.h
# End Source File
# Begin Source File

SOURCE=.\CaymanM32.h
# End Source File
# Begin Source File

SOURCE=.\DebugLog.h
# End Source File
# Begin Source File

SOURCE=.\DeviceBase.h
# End Source File
# Begin Source File

SOURCE=.\DeviceCayman.h
# End Source File
# Begin Source File

SOURCE=.\DPA32.h
# End Source File
# Begin Source File

SOURCE=.\DPA8xT.h
# End Source File
# Begin Source File

SOURCE=.\DPA9xT.h
# End Source File
# Begin Source File

SOURCE=.\DPAM32.h
# End Source File
# Begin Source File

SOURCE=.\HotWireM32.h
# End Source File
# Begin Source File

SOURCE=..\..\J2534Generic\code\J2534.h
# End Source File
# Begin Source File

SOURCE=.\J2534Registry.h
# End Source File
# Begin Source File

SOURCE=.\MyDec.h
# End Source File
# Begin Source File

SOURCE=.\Px2M32.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\VSIJ1xT.h
# End Source File
# Begin Source File

SOURCE=.\VSIJM32.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\Readme.txt
# End Source File
# End Target
# End Project
