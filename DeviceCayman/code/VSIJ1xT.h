/**************************************/
/*                                    */
/* Dearborn Group, Copyright (c) 2005 */
/* Dearborn Cayman Adapter            */
/* Version 1.00                       */
/*                                    */
/**************************************/

#ifndef _CAYMAN1XTH
#define _CAYMAN1XTH

#ifdef  _DLTSRC
  #define SCOPE
  #define DllExport   __declspec( dllexport )
#else
  #define SCOPE extern
  #define DllExport
#endif

/***********/
/* defines */
/***********/
#define DllImport       __declspec( dllimport )
//#define DllExport       __declspec( dllexport )

/* maximum number of DPA open at any point in time */
#define MAX_DPAS  8
/* max size of data buffer for mailbox */
#define MAILBOX_BUFFER_SIZE       4128
#define MAILBOX_J1939_BUFFER_SIZE 1785
#define MAILBOX_ISO15765_BUFFER_SIZE 4097
#define MAX_TRANSFER_SIZE         4150

#define MAX_BUFFER_SIZE           4200

/* max size of check datalink string */
#define MAX_CHECKDATALINK_SIZE    120

/* update mailbox flag definitions */
//valid for BOTH transmit and receive
#define UPDATE_DATA_LOCATION    0x0080 // update data location
#define UPDATE_DATA_COUNT       0x0020 // update data count
#define UPDATE_TIME_STAMP       0x0004 // update time
#define UPDATE_ID               0x0002 // updates ID, priority, size
#define UPDATE_FCID				0x0100 // update Flow Control ID for ISO15765
#define UPDATE_CAN_CONFIG		0x0800 // update CAN padding and extended addressing parameters

//ONLY valid for transmit
#define TRANSMIT_IMMEDIATE      0x0040 // transmit immediatly
#define UPDATE_BROADCAST_TIME   0x0010 // update broadcast time
#define UPDATE_BROADCAST_COUNT  0x0008 // update broadcast count
#define UPDATE_SCI_CONFIG		0x0200 // update SCI config byte
#define UPDATE_DATA             0x0001 // update data
#define SEND_HIGH_VOLTAGE		0x0400 // send the message using high voltage wakeup

// ONLY valid for receive
#define UPDATE_ID_MASK          0x0010 // update ID MASK


/* inihibit flags */
#define TimeStamp_Inhibit       0x20
#define ID_Inhibit              0x40
#define Data_Inhibit            0x80

/************/
/* typedefs */
/************/

/* Enumerations for serial communication ports
        supported by Dearborn Cayman Adapter */
typedef enum CommPortType
{
  eComm1,
  eComm2,
  eComm3,
  eComm4,
  eComm5,
  eComm6,
  eComm7,
  eComm8,
  eComm9
} CommPortType;

/* Enumerations for serial communication baudrate
        supported by Dearborn Cayman Adapter (DPA II, II+, III, IV) */
typedef enum BaudRateType
{
  eB9600,
  eB19200,
  eB28800,
  eB38400,
  eB57600,
  eB115200,
  eB230400,
  eB460800
} BaudRateType;

/* Enumerations for UART baud rates 
   support by Dearborn Cayman Adapter */
typedef enum UARTBaud
{
	UART_BAUD_4800 = 0x00,
	UART_BAUD_7812 = 0x01,
	UART_BAUD_8192 = 0x02, 
	UART_BAUD_9600 = 0x03,
	UART_BAUD_9615 = 0x04,
	UART_BAUD_9800 = 0x05,
	UART_BAUD_10000 = 0x06,
	UART_BAUD_10400 = 0x07,
	UART_BAUD_10870 = 0x08,
	UART_BAUD_11905 = 0x09,
	UART_BAUD_12500 = 0x0a,
	UART_BAUD_13158 = 0x0b,
	UART_BAUD_13889 = 0x0c,
	UART_BAUD_14706 = 0x0d,
	UART_BAUD_15625 = 0x0e,
	UART_BAUD_19200 = 0x0f,
	UART_BAUD_38400 = 0x10,
	UART_BAUD_62500 = 0x11,
	UART_BAUD_125000 = 0x12,
	UART_BAUD_250000 = 0x13
} UARTBaud;

/* Enumerations for protocol catagories
support by Dearborn Cayman Adapter */
typedef enum ProtocolCategory
{
	eUART 		= 0x10,
	eJ1850 		= 0x20,
	eCAN		= 0x30,  
	eVoltage	= 0x50
} ProtocolCategory;

/* Enumerations for UART protocol types
support by Dearborn Cayman Adapter */
typedef enum UARTType
{
	eJ1708		= 0x00,
	eUARTPass	= 0x01,
	eALDL 		= 0x02,
	eISO9141 	= 0x03,
	eISO14230 	= 0x04,
	eCCD		= 0x05,
	eSCI_Eng_A 	= 0x07,
	eSCI_Trans_A = 0x08,
	eSCI_Eng_B	= 0x09,
	eSCI_Trans_B = 0x0a
} UARTType;

/* Enumerations for J1850 protocol types
support by Dearborn Cayman Adapter */
typedef enum J1850Type
{
	eJ1850VPW	= 0x01,
	eJ1850PWM	= 0x02,
} J1850Type;

/* Enumerations for CAN protocol types
support by Dearborn Cayman Adapter */
typedef enum CANType
{
	eSWCAN		= 0x00,
	eJ1939		= 0x01,
	eDWCAN_2 	= 0x02,
	eDWCAN_1	= 0x05,
	eISO15765	= 0x06,
} CANType;

/* Enumerations for Voltage protocol types
support by Dearborn Cayman Adapter */
typedef enum VoltageType
{
	eVPP		= 0x00,
	eVBAT		= 0x01,
	eVAUX		= 0x02,
	eVIGN		= 0x03
} VoltageType;

typedef enum ReceiveFilterType
{
  ePass,
  eBlock
} ReceiveFilterType;


/* Enumerations for return status
   from Dearborn Cayman Adapter API calls */
typedef enum ReturnStatusType
{
  eNoError,	//Success
  eDeviceTimeout,	//Unable to communicate with DPA before timout expired
  eProtocolNotSupported,	//Invalid protocol 
  eBaudRateNotSupported,	//Invalid baud rate
  eInvalidIdentSize,	//Invalid identifier size
  eInvalidDataCount,	//Invalid data count
  eInvalidMailBox,	//Attemption to TX or RX with an unopened mailbox
  eNoDataAvailable,	//No data in mailbox
  eMailBoxInUse,	//No longer used
  eMailBoxNotActive, 		//Mailbox was not open
  eMailBoxNotAvailable,	//All available mailboxes are in use
  eTimerNotSupported,	//Timer not supported
  eTimeoutValueOutOfRange,	//A period greater than 1 minute (60,000ms) was specified
  eInvalidTimerValue,	//Timer value out of range
  eInvalidMailBoxDirection, //No longer used
  eSerialIncorrectPort,	//Invalid com port
  eSerialIncorrectBaud,	//Invalid baud rate
  eSerialPortNotFound,	//Com port not found
  eSerialPortTimeout,	//No longer used
  eCommLinkNotInitialized, //Serial port not found
  eAsyncCommBusy,	//Waiting for a pervious async command to finish processing
  eSyncCommInCallBack,	//No longer used
  eAsyncCommandNotAllowed,	//No longer used
  eSyncCommandNotAllowed,	//Cannot call command from within callback (ISR)
  eLinkedMailBox,	//Mailbox is linked
  eInvalidExtendedFlags,	//No longer used
  eInvalidCommand,	//No longer used
  eInvalidTransportType,	//Invalid transport type
  eSerialOuputError,	//Could not set the PC baud
  eInvalidBufferOffset, 	//Invalid buffer offset
  eInvalidBufferLocation,	//Invalid buffer location
  eOutOfMemory,	//Driver needs more memory on computer
  eInvalidDpa,	//Invalid DPA type
  eInvalidDPAHandle,	//Invalid DPA handle being passed to command
  eInvalidPointer,	//Invalid pointer
  eBaudRateConflict,	//Trying to open a com port that is already initialized 
  eIrqConflict,	//IRQ in use
  eIncorrectDriver,	//The driver selected is the wrong driver
  eInvalidDriverSetup,	//The driver is setup incorrectly
  eInvalidBaseAddress, 	//The base address is invalid
  eInvalidINI,	//The INI entry could not be found
  eInvalidDll,	//There is a bad or missing dll
  eCommVerificationFailed,	//Could not communicate after change
  eInvalidLock,	//Lock not found
  eServerDisconnect,	//The TCP/IP server disconnected
  eInvalidSocket,	//Invalid socket
  eWinSockError,	//Win socket error occurred
  eInvalidDisplayType,	//Invalid display type
  eModemError,	//A modem error occurred
  eInvalidResetType, 	//Invalid reset type
  eProtocolNotInitialzed,	//Protocol not initialized
  eOperatingSystemNotSupported, //Operating system not supported
  eInvalidFMode, //Invalid FMode Type  
  eVoltageNotSupported, //Voltage level out of range
  eInvalidPinUsage, //Hardware pin shared between 2 protocols is already being used
  eInvalidSerialNumber, //Serial number couldn't be retreived from device
} ReturnStatusType;


/* Enumerations for MailBox direction */
typedef enum MailBoxDirectionType
{
  eRemoteMailBox,       /* used for receiving data from link */
  eDataMailBox          /* used for sending data across link */

} MailBoxDirectionType;

/* Enumerations for Transport Laye type */
typedef enum TransportLayerType
{
  eTransportNone = 0x1,       /* No Transmort Layer */
  eTransportBAM  = 0x2,       /* Use Broadcast Anouncment Message */
  eTransportRTS  = 0x4,       /* Use Request to Send */
  eISO15765Transport = 0x8    /* ISO15765 Transport */
} TransportLayerType;

/* Enumerations for Transmit MailBox type */
typedef enum TransmitMailBoxType
{
  eResident,            /* MailBox remain active and can be used again  */
  eRelease              /* MailBox is automatically unloaded after used */
} TransmitMailBoxType;

/* Enumerations for Dearborn Cayman Adapter Errors */
typedef enum DataLinkCANErrorCodeType
{
  eBusOff = 1,
  eCanOverRun = 2,
  eErrorSendingAsync = 3,
  eInvalidUARTChecksum = 4,
  eInvalidUARTMessage = 5,
} DataLinkCANErrorCodeType;


typedef enum
{
	eFullReset,
	eCommReset
}eResetType;

typedef enum FModeType
{
  eFastInit 	= 0x01,
  eFiveBaudInit = 0x02
} FModeType;

typedef enum FiveBaudType
{
  eFBISO91412 	= 0x00,
  eFBInverseKey2 = 0x01,
  eFBInverseAddress  = 0x02,
  eFBISO9141 	 = 0x03,
} FiveBaudType;



typedef enum
{
	eFullSpeed,
	eHighSpeed
}eUSBSpeed;

/**************/
/* structures */
/**************/
/****************************************************************************/
/* Structure for PC copy of MailBox                                         */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
                                            /* mailbox                      */

  BYTE                  bActive;            /* Active or inuse flag         */

  BYTE                  bIdentSize;			/* specifies the length of the  */
                                            /* MailBox identifier   */

  BYTE                  bMailBoxNumber;     /* Handle used for communicat-  */
                                            /* ion between PC and DPA       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier Sixe      */

  BYTE                  bCTSSource;         /* Destination Address to be    */
											/* for RTS Transport layer      */


  DWORD					dwFCIdent;			/* flow contol identifier for	*/
											/* ISO15765 msgs				*/											

  DWORD					dwMailBoxIdentMask; /* MailBox Identifier mask      */ 
                                              /* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Pass or Bloxk filtering      */

  BYTE                  bMailBoxDirectionData; /* used to identify MailBox  */
                                            /* direction                    */

  BYTE                  bTransportType;     /* Type of transport to use     */

  BYTE					bHighVoltage;		/* High Voltage messaging		*/

  BYTE					bSCIConfig;			/* SCI message config duplex	*/
                                            /* mode and Vpp flag			*/

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
                                            /* false = relative             */
                                            
  void (CALLBACK *pfApplicationRoutine)(void *); /* address of callback     */
                                            /* routine NULL disables        */

                                            
  void (CALLBACK *pfMailBoxReleased)(void *);/* address of callback			*/
                                            /* routine NULL disables        */

  DWORD                 dwTimeStamp;        /* time of received message     */

  DWORD                 dwTransmitTimeStamp;/* time of transmited message   */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
                                            /* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
                                            /* message is to be sent for    */
                                            /* broadcast messages           */

  WORD                  wDataCount;         /* Number of bytes per message  */
                                            /* maximum of                   */
                                            /* MAILBOX_BUFFER_SIZE bytes    */

  BYTE                  bData[MAILBOX_BUFFER_SIZE];
                                            /* temp holding buffer for      */
                                            /* message data                 */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
                                                  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
																																						/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

 BYTE					bDataPadInhibit;	/* ISO15765 msgs padding		*/
											/* 1 = do not pad message		*/
											/* 0 = padding enabled			*/
 
 BYTE					bEAEnabled;			/* ISO15765 Extended Addressing	*/
											/* (1 = enabled, 0 = disabled)  */

  BYTE                  bPriority;          /* UART message priority        */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  BYTE                  bDataRequested;     /* flag indicates that data has */
											/* been requested from the DPA  */

  BYTE                  bReceiveFlags;      /* flag indicating requested    */
                                            /* data                         */

  BYTE                  bDataUpdated;       /* flag indicates that data has */
                                            /* been updated                 */

  void                 *vpData;  /* address of users copy of    */
                                            /* message data                */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */
} MailBoxType;

/****************************************************************************/
/* Structure for InitCommLink                                               */
/****************************************************************************/
typedef struct
{
  BYTE                  bCommPort;          /* Comm: prot to use			*/
  BYTE                  bBaudRate;          /* Baud rate to communicate		*/
  char					cModem[80];			/* DPA/H Modem parameters	   */
} CommLinkType;

/****************************************************************************/
/* Structure for TCP_IPType                                                 */
/****************************************************************************/
typedef struct
{
	unsigned char IP1;
	unsigned char IP2;
	unsigned char IP3;
	unsigned char IP4;
	short portNumber;
} TCP_IPType;  

/****************************************************************************/
/* Structure for USBLinkType                                               */
/****************************************************************************/
typedef struct
{
  WORD 					wInterface;  /*The interface we should init from the DPA */

  BYTE                  bHS_FS;      /* High speed or  Full speed USB communications*/
									 /* Default = eFullSpeed*/
} USBLinkType;


/****************************************************************************/
/* Structure for Dearborn Cayman Adapter Error                            */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol that     */
                                            /* error occured               */

  BYTE                  bErrorCode;         /* specifies error code        */

  DWORD					dwTimeStamp;        /* time that error occured     */
} DataLinkErrorType;

/****************************************************************************/
/* structure used for timer interrupts                                      */
/****************************************************************************/
typedef struct
{
  DWORD                 dwTimeOut;			/* specifies the period             */
											/* in milliseconds of the interrupt */

  void (CALLBACK *pTimerFunction)(DWORD);	/* address of callback routine for  */
											/* timer                            */
                                            /* NULL disables                    */
} EnableTimerInterruptType;


/****************************************************************************/
/* Structure for initializing Dearborn Cayman Adapter Datalink            */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Cayman Adapter    */

  BYTE                  bParam0; /* specifies parameter 0 - 5    */
  BYTE                  bParam1; /* protocol specific definitions*/
  BYTE                  bParam2;
  BYTE                  bParam3;
  BYTE                  bParam4;
  BYTE                  bParam5;
  void (CALLBACK *pfDataLinkError)( DataLinkErrorType *);
                                            /* address of routine to call   */
                                            /* in case of Dearborn Cayman   */
                                            /* Adapter error                */

  void (CALLBACK *pfTransmitVector)(MailBoxType *);
                                            /* address of routine to call   */
                                            /* when message is transmitted  */

  void (CALLBACK *pfReceiveVector)(void);   /* address of routine to call   */
                                            /* when message is received     */

  void (CALLBACK *pfReceiveFirstByteVector)(MailBoxType *); /* Address of routine to call */
											/* when first byte of ISO 9141/14230*/
											/* msg is received				*/
} InitDataLinkType;


/****************************************************************************/
/* Structure for reading in protocol power up parameters from the DPA		*/
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* protocol to that's params    */
											/* are that are to be read		*/

  BYTE                  bParamCount;        /* the number of params used for*/
											/* the given protocol			*/

  BYTE                  bParam0; /* specifies parameters 0-5 for */                                            
  BYTE                  bParam1;/* specified protocol            */
  BYTE                  bParam2;
  BYTE                  bParam3;
  BYTE                  bParam4;
  BYTE                  bParam5;
} ReadDataLinkType;

/****************************************************************************/
/* Structure for loading Dearborn Cayman Adapter Mailbox                  */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Cayman Adapter    */

  MailBoxType          *pMailBoxHandle;     /* address of MailBox handle    */
											/* returned if load was success */

  BYTE                  bRemote_Data;       /* used to identify MailBox     */
											/* direction                    */

  BYTE					bTransportType;     /* Transport Type to be used    */

  BYTE					bHighVoltage;		/* High Voltage messaging		*/

  BYTE					bSCIConfig;			/* SCI message config duplex	*/
                                            /* mode and Vpp flag			*/

  BYTE                  bResidentOrRelease; /* Auto Release Data mailbox    */

  BYTE                  bIdentSize;			/* Mailbox Idenivier size       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier 11/29     */

  BYTE                  bCTSSource;         /* Destination Address to be    */
                                            /* for RTS Transport session    */

  DWORD					dwFCIdent;			/* flow contol identifier for	*/
											/* ISO15765 msgs				*/

  BYTE					bDataPadInhibit;	/* ISO15765 msgs padding		*/
											/* 1 = do not pad message		*/
											/* 0 = padding enabled			*/

  BYTE					bEAEnabled;			/* ISO15765 Extended Addressing	*/
											/* (1 = enabled, 0 = disabled)  */

  DWORD                 dwMailBoxIdentMask; /* MailBox Identifier mask      */
											/* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Match or Reject filter       */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
												  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
											/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
											/* false = relative             */

  DWORD                 dwTimeStamp;        /* specifies if time transmit   */
											/* message is to be sent        */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
											/* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
											/* message is to be sent for    */
											/* broadcast messages           */

  BYTE                  bPriority;          /* UART priority               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  void (CALLBACK *pfApplicationRoutine)(MailBoxType *); /* address of       */
											/* callback routine             */

  BYTE                  bMailBoxReleased;   /* Mailbox Releases flag        */

  void (CALLBACK *pfMailBoxReleased)(MailBoxType *);  /* Address of Mailbox */
											/* Release callback routine     */

    void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */

  WORD                  wDataCount;         /* Number of bytes per message */
                                            /* maximum of                  */
                                            /* MAILBOX_BUFFER_SIZE bytes   */

  void                 *vpData;  /* address of message data     */

} LoadMailBoxType;

/****************************************************************************/
/* structure used for J1939 Transpot protocol configuration                 */
/****************************************************************************/
typedef struct
{
  WORD  wBamTimeout; /* Inter message timeout for BAM receive    */
  WORD  wBAM_BAMTXTime;         /* Time between BAM and first data packet   */
  WORD  wBAM_DataTXTime;         /* Time between BAM Data packets           */
  WORD  wRTS_Retry;  /* Number of times to send RTS without CTS  */
  WORD  wRTS_RetryTransmitTime; /* Time between retries of RTS TX           */
  WORD  wRTS_TX_Timeout;        /* Time to wait for a CTS after RTS TX      */
  WORD  wRTS_TX_TransmitTime;   /* Time between data packets on RTS TX      */
  WORD  wRTS_RX_TimeoutData;    /* Timeout between data packets on RTS RX   */
  WORD  wRTS_RX_TimeoutCMD;     /* Timeout between CTS and 1st data packet  */
  BYTE  bRTS_RX_CTS_Count;      /* Number of packets to CTS for             */
  BYTE  bRTS_TX_CTS_Count;      /* Number of packets to CTS for             */
} ConfigureJ1939Type;

/****************************************************************************/
/* structure used for ISO15765 protocol configuration						*/
/****************************************************************************/
typedef struct
{
BYTE bISO15765_BS;
BYTE bISO15765_STMIN;
WORD wBS_TX;
WORD wSTMIN_TX;
} ConfigureISO15765Type; 

/****************************************************************************/
/* structure used for ISO9141 protocol configuration						*/
/****************************************************************************/
typedef struct
{
WORD wP1_MAX;	//Max inter-byte time for ECU responses
WORD wP3_MIN;	//Min tme between end of ECU req and start of new tester req
WORD wP4_MIN;	//Min inter-byte time for tester req
WORD wW0;	//Min bus idle time before tester starts to tx address byte
WORD wW1; 	//Max time from end of address byte to start of sync pattern
WORD wW2; 	//Max time from end of sync pattern to start of key byte 1
WORD wW3; 	//Max time between key byte 1 and key byte 2
WORD wW4; 	//Min time between key byte 2 and its inversion from tester
WORD wW5; 	//Min bus idle time before the tester starts to tx add byte
WORD wTIDLE; 	//Min bus idle ime that is needed before a fast init begins
WORD wTINIL; 	//Duration of low pulse in fast init
WORD wTWUP; 	//Duration of wake-up pulse for fast init
} ConfigureISO9141Type;

/****************************************************************************/
/* structure used for SIC protocol configuration							*/
/****************************************************************************/
typedef struct
{
	WORD wT1_MAX;	//ECU Inter-Frame �Response� Delay
	WORD wT2_MAX;	//Tester Inter-Frame �Request� Delay
	WORD wT3_MAX;	//ECU Inter-Message �Processing� Delay for Initial Data Transmission
	WORD wT4_MAX;	//ECU Inter-Frame �Response� Delay for Subsequent Data Trasnmission(s)
	WORD wT5_MAX;	//Tester Inter-Frame �Request� Delay for Subsequent Data Transmission(s)
} ConfigureSCIType; 

/****************************************************************************/
/* Structure for issuing Fast or Five Baud Init								*/
/****************************************************************************/

typedef struct
{
  WORD                  wDataCount;         /* length of response */

  BYTE                  *pData; /* pointer to response data   */
                                            
} FInitResponse;


typedef struct
{
  BYTE                  bFInitType;         /* specifies fast or five baud Init */


  BYTE                  *pData; /* pointer to data   */
  
  void (CALLBACK *pfReceiveKeyBytes)(FInitResponse *FInitResp);   /* address of routine to call   */
                                            /* when response message is received */
} FInitType;

typedef struct
{
  BYTE  bPCBaud;
  BYTE  bDPABaud;
} SetBaudRateType;

typedef struct
{
  BYTE  bResetType;
} ResetType;


#undef SCOPE
#endif
