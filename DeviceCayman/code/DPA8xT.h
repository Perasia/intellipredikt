/**************************************/
/*                                    */
/* Dearborn Group, Copyright (c) 1996 */
/* Dearborn Protocol Adapter          */
/* Version 8.10                       */
/*                                    */
/**************************************/

#ifndef _DPA8XTH
#define _DPA8XTH

#ifdef  _DLTSRC
  #define SCOPE
  #define DllExport   __declspec( dllexport )
#else
  #define SCOPE extern
  #define DllExport
#endif

/***********/
/* defines */
/***********/
#define DllImport       __declspec( dllimport )
//#define DllExport       __declspec( dllexport )

/* maximum number of DPA open at any point in time */
#define MAX_DPAS  8
/* max size of data buffer for mailbox */
#define MAILBOX_BUFFER_SIZE       2048
#define MAILBOX_J1939_BUFFER_SIZE 1785
#define MAX_TRANSFER_SIZE         2000

#define MaxBufferSize             2048

/* max size of check datalink string */
#define MAX_CHECKDATALINK_SIZE    80

/* update transmit mailbox flag definitions */
#define UPDATE_DATA_LOCATION    0x80 // update data location
#define TRANSMIT_IMMEDIATE      0x40 // transmit immediatly
#define UPDATE_DATA_COUNT       0x20 // update data count
#define UPDATE_BROADCAST_TIME   0x10 // update broadcast time
#define UPDATE_BROADCAST_COUNT  0x08 // update broadcast count
#define UPDATE_TIME_STAMP       0x04 // update time
#define UPDATE_ID               0x02 // update ID / (MID-PID-Priority)
#define UPDATE_ID_MASK          0x10 // update ID MASK / (MID-PID-Priority)
// ONLY valid for receive
#define UPDATE_DATA             0x01 // update data

/* inihibit flags */
#define TimeStamp_Inhibit       0x20
#define ID_Inhibit              0x40
#define Data_Inhibit            0x80

/************/
/* typedefs */
/************/

/* Enumerations for serial communication ports
        supported by Dearborn Protocol Adapter */
typedef enum CommPortType
{
  eComm1,
  eComm2,
  eComm3,
  eComm4,
  eComm5,
  eComm6,
  eComm7,
  eComm8,
  eComm9
} CommPortType;

/* Enumerations for serial communication baudrate
        supported by Dearborn Protocol Adapter (DPA II, II+, III, IV) */
typedef enum BaudRateType
{
  eB9600,
  eB19200,
  eB28800,
  eB38400,
  eB57600,
  eB115200,
  eB230400,
  eB460800
} BaudRateType;

/* Enumerations for serial communication baudrate
        supported by Industrial Dearborn Protocol Adapter (DPA III/i)*/
typedef enum IndustrialBaudRateType
{
	eIB1200,
	eIB2400,
	eIB4800,
	eIB9600,
	eIB14400,
	eIB19200,
	eIB28800,
	eIB38400,
	eIB57600,
	eIB115200,
} IndustrialBaudRateType;

/* Enumerations for protocols
   support by Dearborn Protocol Adapter */
typedef enum ProtocolType
{
  eISO9141,
  eJ1708,
  eJ1850,
  eJ1939,
  ePassThru, 
  eUSB, 
  eRS232,
  eCAN = 3
} ProtocolType;

typedef enum ReceiveFilterType
{
  ePass,
  eBlock
} ReceiveFilterType;


/* Enumerations for return status
   from Dearborn Protocol Adapter API calls */
typedef enum ReturnStatusType
{
  eNoError,
  eDeviceTimeout,
  eProtocolNotSupported,
  eBaudRateNotSupported,
  eInvalidBitIdentSize,
  eInvalidDataCount,
  eInvalidMailBox,
  eNoDataAvailable,
  eMailBoxInUse,
  eMailBoxNotActive, 
  eMailBoxNotAvailable, //10
  eTimerNotSupported,
  eTimeoutValueOutOfRange,
  eInvalidTimerValue,
  eInvalidMailBoxDirection,
  eSerialIncorrectPort,
  eSerialIncorrectBaud,
  eSerialPortNotFound,
  eSerialPortTimeout,
  eCommLinkNotInitialized, 
  eAsyncCommBusy, //20
  eSyncCommInCallBack,
  eAsyncCommandNotAllowed,
  eSyncCommandNotAllowed,
  eLinkedMailBox,
  eInvalidExtendedFlags,
  eInvalidCommand,
  eInvalidTransportType,
  eSerialOuputError,
  eInvalidBufferOffset, 
  eInvalidBufferLocation, //30
  eOutOfMemory,
  eInvalidDpa,
  eInvalidDPAHandle,
  eInvalidPointer,
  eBaudRateConflict,
  eIrqConflict,
  eIncorrectDriver,
  eInvalidDriverSetup,
  eInvalidBaseAddress, 
  eInvalidINI,//40
  eInvalidDll,
  eCommVerificationFailed,
  eInvalidLock,
  eServerDisconnect,
  eInvalidSocket,
  eWinSockError,
  eInvalidDisplayType,
  eModemError,
  eInvalidResetType, 
  eProtocolNotInitialzed,//50
  eOperatingSystemNotSupported
} ReturnStatusType;

/* Enumerations for MailBox direction */
typedef enum MailBoxDirectionType
{
  eRemoteMailBox,       /* used for receiving data from link */

  eDataMailBox          /* used for sending data across link */

} MailBoxDirectionType;

/* Enumerations for Transport Laye type */
typedef enum TransportLayerType
{
  eTransportNone = 0x1,       /* No Transmort Layer */
  eTransportBAM  = 0x2,       /* Use Broadcast Anouncment Message */
  eTransportRTS  = 0x4        /* Use Request to Send */
} TransportLayerType;

/* Enumerations for Transmit MailBox type */
typedef enum TransmitMailBoxType
{
  eResident,            /* MailBox remain active and can be used again  */
                        /* TransmitMailBox function                     */

  eRelease              /* MailBox is automatically unloaded after used */

} TransmitMailBoxType;

/* Enumerations for Dearborn Protocol Adapter Errors */
typedef enum DataLinkCANErrorCodeType
{
  eBusOff = 1,
  eCanOverRun = 2,
  eErrorSendingAsync = 3,
  eInvalidJ1708Checksum = 4,
  eInvalidJ1708Message = 5,
} DataLinkCANErrorCodeType;

typedef enum LinkType
{
  eNoLink,
  eLinkHead,
  eLinkBody,
  eLinkTail
} LinkType;

typedef enum 
{
 eIRQ5 = 5,
 eIRQ7 = 7,
 eIRQ10 = 10,
}PCIrqType;


typedef enum
{
	eFullReset,
	eCommReset
}eResetType;

typedef enum
{
	eFullSpeed,
	eHighSpeed
}eUSBSpeed;

/**************/
/* structures */
/**************/
/****************************************************************************/
/* Structure for PC copy of MailBox                                         */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
                                            /* mailbox                      */

  BYTE                  bActive;            /* Active or inuse flag         */

  BYTE                  bBitIdentSize;      /* specifies the length of the  */
                                            /* MailBox identifier, max 32   */

  BYTE                  bMailBoxNumber;     /* Handle used for communicat-  */
                                            /* ion between PC and DPA       */

  short                 iVBBufferNumber;

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier Sixe      */

  BYTE                  bCTSSource;         /* Destination Address to be    */
																																						/* for RTS Transport layer      */

  long unsigned int     dwMailBoxIdentMask; /* MailBox Identifier mask      */ 
                                              /* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Pass or Bloxk filtering      */

  BYTE                  bMailBoxDirectionData; /* used to identify MailBox  */
                                            /* direction                    */

  BYTE                  bTransportType;     /* Type of transport to use     */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
                                            /* false = relative             */

                                            /* address of callback routine  */
                                            /* NULL disables                */
  void (CALLBACK *pfApplicationRoutine)(void *);

                                            /* address of callback routine  */
                                            /* NULL disables                */
  void (CALLBACK *pfMailBoxReleased)(void *);


  DWORD                 dwTimeStamp;        /* time of received message     */

  DWORD                 dwTransmitTimeStamp;/* time of transmited message   */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
                                            /* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
                                            /* message is to be sent for    */
                                            /* broadcast messages           */

  WORD                  wDataCount;         /* Number of bytes per message  */
                                            /* maximum of                   */
                                            /* MAILBOX_BUFFER_SIZE bytes    */

  BYTE                  bData[MAILBOX_BUFFER_SIZE];
                                            /* temp holding buffer for      */
                                            /* message data                 */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
                                                  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
																																						/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bLinkType;          /* J1708 link type for multiple */
                                            /* PID's                        */

  BYTE                  bLink;              /* J1708 link for multiple PID's*/

  BYTE                  bPriority;          /* J1708 priority               */

  BYTE                  bMID;               /* J1708 MID                    */

  BYTE                  bPID;               /* J1708 PID                    */

  BYTE                  bMIDMask;           /* J1708 MID mask               */

  BYTE                  bPIDMask;           /* J1708 PID mask               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  BYTE                  bDataRequested;     /* flag indicates that data has */
											/* been requested from the DPA  */

  BYTE                  bReceiveFlags;      /* flag indicating requested    */
                                            /* data                         */

  BYTE                  bDataUpdated;       /* flag indicates that data has */
                                            /* been updated                 */

  void                 *vpData;             /* address of users copy of    */
                                            /* message data                */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */
} MailBoxType;

/****************************************************************************/
/* Structure for InitCommLink                                               */
/****************************************************************************/
typedef struct
{
  BYTE                  bCommPort;          /* Comm: prot to use			*/

  BYTE                  bBaudRate;          /* Baud rate to communicate		*/

  BYTE					bPolledPort;        /* set to 1 if this is a polled port */
  
  char					cModem[80];			/* DPA/H Modem parameters	   */

} CommLinkType;

/****************************************************************************/
/* Structure for PCCardLink                                               */
/****************************************************************************/
typedef struct
{
  WORD 					bBaseAddress;  /*The base address of the card		*/

  BYTE                  bIrq;          /* The Irq of the isa card		    */
                                       /* with the DPA						*/
} PCCardType;

/****************************************************************************/
/* Structure for TCP_IPType                                                 */
/****************************************************************************/
typedef struct
{
	unsigned char IP1;
	unsigned char IP2;
	unsigned char IP3;
	unsigned char IP4;
	short portNumber;
} TCP_IPType;  


/****************************************************************************/
/* Structure for USBLinkType                                               */
/****************************************************************************/
typedef struct
{
  WORD 					wInterface;  /*The interface we should init from the DPA */

  BYTE                  bHS_FS;      /* High speed or  Full speed USB communications*/
									 /* Default = eFullSpeed*/
} USBLinkType;


/****************************************************************************/
/* Structure for Dearborn Protocol Adapter Error                            */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol that     */
                                            /* error occured               */

  BYTE                  bErrorCode;         /* specifies error code        */
} DataLinkErrorType;

/****************************************************************************/
/* structure used for timer interrupts                                      */
/****************************************************************************/
typedef struct
{
  DWORD                 dwTimeOut;			/* specifies the period             */
											/* in milliseconds of the interrupt */

  void (CALLBACK *pTimerFunction)(DWORD);
											/* address of callback routine for  */
											/* timer                            */
                                            /* NULL disables                    */
} EnableTimerInterruptType;


/****************************************************************************/
/* Structure for initializing Dearborn Protocol Adapter Datalink            */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  BYTE                  bParam0;            /* specifies parameter 0 for    */
                                            /* specified protocol           */

  BYTE                  bParam1;            /* specifies parameter 1 for    */
                                            /* specified protocol           */

  BYTE                  bParam2;            /* specifies parameter 2 for    */
                                            /* specified protocol           */

  BYTE                  bParam3;            /* specifies parameter 3 for    */
                                            /* specified protocol           */

  BYTE                  bParam4;            /* specifies parameter 4 for    */
                                            /* specified protocol           */

  void (CALLBACK *pfDataLinkError)(MailBoxType *, DataLinkErrorType *);
                                            /* address of routine to call   */
                                            /* in case of Dearborn Protocol */
                                            /* Adapter error                */

  void (CALLBACK *pfTransmitVector)(MailBoxType *);
                                            /* address of routine to call   */
                                            /* when message is transmitted  */

  void (CALLBACK *pfReceiveVector)(void);   /* address of routine to call   */
                                            /* when message is received     */
} InitDataLinkType;


/****************************************************************************/
/* Structure for reading in protocol power up parameters from the DPA		*/
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  BYTE                  bParamCount;        /* the number of params used for*/
											/* the given protocol			*/

  BYTE                  bParam0;            /* specifies parameter 0 for    */
                                            /* specified protocol           */

  BYTE                  bParam1;            /* specifies parameter 1 for    */
                                            /* specified protocol           */

  BYTE                  bParam2;            /* specifies parameter 2 for    */
                                            /* specified protocol           */

  BYTE                  bParam3;            /* specifies parameter 3 for    */
                                            /* specified protocol           */

  BYTE                  bParam4;            /* specifies parameter 4 for    */
                                            /* specified protocol           */
} ReadDataLinkType;

/****************************************************************************/
/* Structure for loading Dearborn Protocol Adapter Mailbox                  */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  MailBoxType          *pMailBoxHandle;     /* address of MailBox handle    */
											/* returned if load was success */

  BYTE                  bRemote_Data;       /* used to identify MailBox     */
											/* direction                    */

  unsigned char         bTransportType;     /* Transport Type to be used    */

  BYTE                  bResidentOrRelease; /* Auto Release Data mailbox    */

  BYTE                  bBitIdentSize;      /* Mailbox Idenivier size       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier 11/29     */

  BYTE                  bCTSSource;         /* Destination Address to be    */
                                            /* for RTS Transport session    */

  DWORD                 dwMailBoxIdentMask; /* MailBox Identifier mask      */
											/* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Match or Reject filter       */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
												  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
											/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
											/* false = relative             */

  DWORD                 dwTimeStamp;        /* specifies if time transmit   */
											/* message is to be sent        */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
											/* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
											/* message is to be sent for    */
											/* broadcast messages           */

  BYTE                  bLinkType;          /* J1708 link type for multiple */
                                            /* PID's                        */

  BYTE                  bLink;              /* J1708 link for multiple PID's*/

  BYTE                  bPriority;          /* J1708 priority               */

  BYTE                  bMID;               /* J1708 MID                    */

  BYTE                  bPID;               /* J1708 PID                    */

  BYTE                  bMIDMask;           /* J1708 MID mask               */

  BYTE                  bPIDMask;           /* J1708 PID mask               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  void (CALLBACK *pfApplicationRoutine)(MailBoxType *); /* address of       */
											/* callback routine             */

  BYTE                  bMailBoxReleased;   /* Mailbox Releases flag        */

  void (CALLBACK *pfMailBoxReleased)(MailBoxType *);  /* Address of Mailbox */
											/* Release callback routine     */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */

  WORD                  wDataCount;         /* Number of bytes per message */
                                            /* maximum of                  */
                                            /* MAILBOX_BUFFER_SIZE bytes   */

  void                 *vpData;             /* address of message data     */

} LoadMailBoxType;

/****************************************************************************/
/* structure used for J1939 Transpot protocol configuration                 */
/****************************************************************************/
typedef struct
{
  BYTE  bProtocol;
  WORD  iBamTimeout;            /* Inter message timeout for BAM receive    */
  WORD  iBAM_BAMTXTime;         /* Time between BAM and first data packet   */
  WORD  iBAM_DataTXTime;         /* Time between BAM Data packets           */
  WORD  iRTS_Retry;             /* Number of times to send RTS without CTS  */
  WORD  iRTS_RetryTransmitTime; /* Time between retries of RTS TX           */
  WORD  iRTS_TX_Timeout;        /* Time to wait for a CTS after RTS TX      */
  WORD  iRTS_TX_TransmitTime;   /* Time between data packets on RTS TX      */
  WORD  iRTS_RX_TimeoutData;    /* Timeout between data packets on RTS RX   */
  WORD  iRTS_RX_TimeoutCMD;     /* Timeout between CTS and 1st data packet  */
  BYTE  iRTS_RX_CTS_Count;      /* Number of packets to CTS for             */
  BYTE  iRTS_TX_CTS_Count;      /* Number of packets to CTS for             */
} ConfigureTransportType;

typedef struct
{
  BYTE  bPCBaud;
  BYTE  bDPABaud;
} SetBaudRateType;

typedef struct
{
  BYTE  bResetType;
} ResetType;

typedef enum 
{
  eDash,
  eServiceBay,
} eDisplayType;

/****************************************************************************/
/* Structure for Display                                                    */
/****************************************************************************/
typedef struct
{
	unsigned char eDisplayType;  //eDash or eServiceBay
	unsigned char *line1;
	unsigned char *line2;
	unsigned char *line3;
	unsigned char *line4;
} DisplayType;

typedef struct
{
    unsigned char eDisplayType;
	void (CALLBACK *displayCallback)( short buttonNumber, void *userPointer);
	void *userPointer;
} InitDisplayType;



/****************************************************************************/
/* Structure for Internal Use                                               */
/****************************************************************************/

typedef struct DG_Proto
{
  WORD            wDataCount;
  WORD            wReadIndex;
  BYTE            bEchoChar;
  BYTE            bRetryCount;
  void           *MailBoxHandle;
  BYTE            bData[MaxBufferSize];
} DG_Proto;



#undef SCOPE
#endif
