/**************************************/
/*                                    */
/* Dearborn Group, Inc. (c)2016       */
/* Version 11.xx                      */
/*                                    */
/**************************************/

#ifndef _ISERIESM_11XH
#define _ISERIESM_11XH

#include "ISeries11xt.h"

/***********************/
/* function prototypes */
/***********************/
#ifdef __cplusplus
extern "C" {
	ReturnStatusType  DllExport WINAPI InitDPA (short dpaNumber);
	ReturnStatusType  DllExport WINAPI InitCommLink (CommLinkType *CommLinkData);
	ReturnStatusType  DllExport WINAPI InitPCCard (PCCardType *PCCardData);
	ReturnStatusType  DllExport WINAPI InitTCP_IP (TCP_IPType *TCP_IPData);
	ReturnStatusType  DllExport WINAPI InitUSBLink (USBLinkType *USBData);
	ReturnStatusType  DllExport WINAPI InitUSBDPA (short sUSBDPAType);
	ReturnStatusType  DllExport WINAPI RestoreDPA (void);
	ReturnStatusType  DllExport WINAPI RestoreCommLink (void);
	ReturnStatusType  DllExport WINAPI RestorePCCard (void);
	ReturnStatusType  DllExport WINAPI RestoreTCP_IP (void);
	ReturnStatusType  DllExport WINAPI RestoreUSBLink (void);
	ReturnStatusType  DllExport WINAPI InitDataLink (InitDataLinkType *InitDataLinkData);
	ReturnStatusType  DllExport WINAPI CheckDataLink (char *cVersion);
	ReturnStatusType  DllExport WINAPI ReadDPAChecksum (unsigned int *uiDPAChecksum);
	ReturnStatusType  DllExport WINAPI LoadDPABuffer (BYTE *bData, WORD wLength, WORD wOffset);
	ReturnStatusType  DllExport WINAPI ReadDPABuffer (BYTE *bData, WORD wLength, WORD wOffset);
	ReturnStatusType  DllExport WINAPI CheckLock (char *szSearchString, BYTE *bFound);
	ReturnStatusType  DllExport WINAPI GoToLoader(void);
	ReturnStatusType  DllExport WINAPI LoadMailBox (LoadMailBoxType *pLoadMailBoxData);
	ReturnStatusType  DllExport WINAPI TransmitMailBox (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxAsync (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransMailBoxData (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransMailBoxDataAsync (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBox (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsync (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBox (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsync (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
	ReturnStatusType  DllExport WINAPI ReceiveMailBox (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UnloadMailBox (MailBoxType *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI LoadTimer (DWORD dwTime);
	ReturnStatusType  DllExport WINAPI EnableTimerInterrupt (EnableTimerInterruptType *pEnableTimerInterruptData);
	ReturnStatusType  DllExport WINAPI SuspendTimerInterrupt (void);
	ReturnStatusType  DllExport WINAPI PauseTimer (void);
	ReturnStatusType  DllExport WINAPI ResumeTimer (void);
	ReturnStatusType  DllExport WINAPI RequestTimerValue (DWORD *dwTimerValue);
	ReturnStatusType  DllExport WINAPI ConfigureTransportProtocol (ConfigureTransportType  *ConfigureTransportTypeData);
	ReturnStatusType  DllExport WINAPI SetBaudRate ( SetBaudRateType *SetBaudRateData);
	ReturnStatusType  DllExport WINAPI ResetDPA( ResetType *ResetData);
	ReturnStatusType  DllExport WINAPI GetLockSeed (char *lockName, BYTE *bseed);
	ReturnStatusType  DllExport WINAPI WriteDisplay ( DisplayType *displayData);
	ReturnStatusType  DllExport WINAPI InitDisplay ( InitDisplayType *InitDisplayData );
	ReturnStatusType  DllExport WINAPI StoreDataLink ( BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI RestoreDataLink ( BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI DisableDataLink ( BYTE bProtocol);
	ReturnStatusType  DllExport WINAPI ReadDataLink ( BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
	//extended definitions
	ReturnStatusType  DllExport WINAPI InitDataLinkExt (InitDataLinkTypeExt *InitDataLinkData);
	ReturnStatusType  DllExport WINAPI LoadMailBoxExt (LoadMailBoxTypeExt *pLoadMailBoxData);
	ReturnStatusType  DllExport WINAPI UnloadMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI TransmitMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
	ReturnStatusType  DllExport WINAPI ReceiveMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
	ReturnStatusType  DllExport WINAPI ConfigureProtocol (BYTE bProtocol, void  *ConfigureData);
	ReturnStatusType  DllExport WINAPI FInit (FInitType *pFInitData);
	ReturnStatusType  DllExport WINAPI GetSerialNumber ( char  *pData);
	ReturnStatusType  DllExport WINAPI ReadVoltage (BYTE bVoltageType, BYTE *bTens, BYTE *bHundredths);
	ReturnStatusType  DllExport WINAPI GetConnectStatus (short *c);
	ReturnStatusType  DllExport WINAPI GetCANBusLoad(RequestCANBusLoadType *RequestCANBusLoad);
	ReturnStatusType  DllExport WINAPI GetCANErrorCounters(BYTE bProtocol, BYTE *bTEC, BYTE *bREC, BYTE bFlag);
    ReturnStatusType  DllExport WINAPI ReadArea ( BYTE bArea, char *data );
    ReturnStatusType  DllExport WINAPI WriteArea ( BYTE bArea, char *data );
    ReturnStatusType  DllExport WINAPI ClearArea ( BYTE bArea );
    ReturnStatusType  DllExport WINAPI ReadCRP ( char *data );
}
#else
ReturnStatusType  DllExport WINAPI InitDPA (short dpaNumber);
ReturnStatusType  DllExport WINAPI InitCommLink (CommLinkType *CommLinkData);
ReturnStatusType  DllExport WINAPI InitPCCard (PCCardType *PCCardData);
ReturnStatusType  DllExport WINAPI InitTCP_IP (TCP_IPType *TCP_IPData);
ReturnStatusType  DllExport WINAPI InitUSBLink (USBLinkType *USBData);
ReturnStatusType  DllExport WINAPI InitUSBDPA (short sUSBDPAType);
ReturnStatusType  DllExport WINAPI InitBluetoothLink (char *bName);
ReturnStatusType  DllExport WINAPI RestoreDPA (void);
ReturnStatusType  DllExport WINAPI RestoreCommLink (void);
ReturnStatusType  DllExport WINAPI RestorePCCard (void);
ReturnStatusType  DllExport WINAPI RestoreTCP_IP (void);
ReturnStatusType  DllExport WINAPI RestoreUSBLink (void);
ReturnStatusType  DllExport WINAPI RestoreBluetoothLink (void);
ReturnStatusType  DllExport WINAPI InitDataLink (InitDataLinkType *InitDataLinkData);
ReturnStatusType  DllExport WINAPI CheckDataLink (char *cVersion);
ReturnStatusType  DllExport WINAPI ReadDPAChecksum (unsigned int *uiDPAChecksum);
ReturnStatusType  DllExport WINAPI LoadDPABuffer (BYTE *bData, WORD wLength, WORD wOffset);
ReturnStatusType  DllExport WINAPI ReadDPABuffer (BYTE *bData, WORD wLength, WORD wOffset);
ReturnStatusType  DllExport WINAPI CheckLock (char *szSearchString, BYTE *bFound);
ReturnStatusType  DllExport WINAPI GoToLoader(void);
ReturnStatusType  DllExport WINAPI LoadMailBox (LoadMailBoxType *pLoadMailBoxData);
ReturnStatusType  DllExport WINAPI TransmitMailBox (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxAsync (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransMailBoxData (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransMailBoxDataAsync (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBox (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsync (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBox (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsync (MailBoxType *pMailBoxHandle, BYTE bUpdateFlag);
ReturnStatusType  DllExport WINAPI ReceiveMailBox (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UnloadMailBox (MailBoxType *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI LoadTimer (DWORD dwTime);
ReturnStatusType  DllExport WINAPI EnableTimerInterrupt (EnableTimerInterruptType *pEnableTimerInterruptData);
ReturnStatusType  DllExport WINAPI SuspendTimerInterrupt (void);
ReturnStatusType  DllExport WINAPI PauseTimer (void);
ReturnStatusType  DllExport WINAPI ResumeTimer (void);
ReturnStatusType  DllExport WINAPI RequestTimerValue (DWORD *dwTimerValue);
ReturnStatusType  DllExport WINAPI ConfigureTransportProtocol (ConfigureTransportType  *ConfigureTransportTypeData);
ReturnStatusType  DllExport WINAPI SetBaudRate ( SetBaudRateType *SetBaudRateData);
ReturnStatusType  DllExport WINAPI ResetDPA( ResetType *ResetData);
ReturnStatusType  DllExport WINAPI GetLockSeed (char *lockName, BYTE *bseed);
ReturnStatusType  DllExport WINAPI WriteDisplay ( DisplayType *displayData);
ReturnStatusType  DllExport WINAPI InitDisplay ( InitDisplayType *InitDisplayData );
ReturnStatusType  DllExport WINAPI StoreDataLink ( BYTE bProtocol);
ReturnStatusType  DllExport WINAPI RestoreDataLink ( BYTE bProtocol);
ReturnStatusType  DllExport WINAPI DisableDataLink ( BYTE bProtocol);
ReturnStatusType  DllExport WINAPI ReadDataLink ( BYTE bProtocol,ReadDataLinkType *ReadDataLinkData);
//extended definitions
ReturnStatusType  DllExport WINAPI InitDataLinkExt (InitDataLinkTypeExt *InitDataLinkData);
ReturnStatusType  DllExport WINAPI LoadMailBoxExt (LoadMailBoxTypeExt *pLoadMailBoxData);
ReturnStatusType  DllExport WINAPI UnloadMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI TransmitMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateTransmitMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI UpdateReceiveMailBoxAsyncExt (MailBoxTypeExt *pMailBoxHandle, WORD wUpdateFlag);
ReturnStatusType  DllExport WINAPI ReceiveMailBoxExt (MailBoxTypeExt *pMailBoxHandle);
ReturnStatusType  DllExport WINAPI ConfigureProtocol (BYTE bProtocol, void  *ConfigureData);
ReturnStatusType  DllExport WINAPI FInit (FInitType *pFInitData);
ReturnStatusType  DllExport WINAPI GetSerialNumber ( char  *pData);
ReturnStatusType  DllExport WINAPI ReadVoltage (BYTE bVoltageType, BYTE *bTens, BYTE *bHundredths);
ReturnStatusType  DllExport WINAPI GetConnectStatus (short *c);
ReturnStatusType  DllExport WINAPI GetCANBusLoad(RequestCANBusLoadType *RequestCANBusLoad);
ReturnStatusType  DllExport WINAPI GetCANErrorCounters(BYTE bProtocol, BYTE *bTEC, BYTE *bREC, BYTE bFlag);
ReturnStatusType  DllExport WINAPI ReadArea ( BYTE bArea, char *data );
ReturnStatusType  DllExport WINAPI WriteArea ( BYTE bArea, char *data );
ReturnStatusType  DllExport WINAPI ClearArea ( BYTE bArea );
ReturnStatusType  DllExport WINAPI ReadCRP ( char *data );

#endif   

#endif
