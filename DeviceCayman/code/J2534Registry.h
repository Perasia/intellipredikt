/*******************************************************************************
*
* Copyright (C) 2010 DG Technologies/Dearborn Group, Inc. All rights reserved
* The copyright here does not evidence any actual or
* intended publication.
* Unpublished, confidential proprietary source code.
*
* File Name: J2534Registry.h
* Author(s): Ming-Chee Chang, Sanjay Mehta, Pat Ruelle, Val Korcheva
* Target Project: J2534 Generic
* Target Processor: MachineX86
* Compiler: Visual Studio 9
* Description: This file contains the logging headers
* Note:
*
*******************************************************************************/

#ifndef _J2534REGISTRY_H_
#define _J2534REGISTRY_H_

// Constants
#ifdef J2534_0305
#define J2534REGISTRY_KEY_PATH			"Software\\PassThruSupport"
#else
#define J2534REGISTRY_KEY_PATH			"Software\\PassThruSupport.04.04"
#endif
#define J2534REGISTRY_DEVICE_ID			"DeviceId"
#define J2534REGISTRY_NAME				"Name"
#define J2534REGISTRY_DEARBORN_GROUP	"Dearborn Group, Inc."
#define J2534REGISTRY_DEVICES			"Devices"
#define J2534REGISTRY_DOUBLE_BACKSLASH	"\\"
#define J2534REGISTRY_HYPHEN_SPACE		" - "
#define J2534REGISTRY_DPA				"DPA"
#define J2534REGISTRY_CAYMAN			"CaymanDPA"
#define J2534REGISTRY_VENDOR			"Vendor"
#define J2534REGISTRY_NAME				"Name"
#define J2534REGISTRY_PROTOCOLSSUPPORT	"ProtocolsSupported"
#define J2534REGISTRY_CONFIGAPP			"ConfigApplication"
#define J2534REGISTRY_FUNCLIB			"FunctionLibrary"
#define J2534REGISTRY_APIVERSION		"APIVersion"
#define J2534REGISTRY_PRODUCTVERSION	"ProductVersion"
#define J2534REGISTRY_LOGGING			"Logging"
#define J2534REGISTRY_CAN				"CAN"
#define J2534REGISTRY_ISO15765			"ISO15765"
#define J2534REGISTRY_J1850PWM			"J1850PWM"
#define J2534REGISTRY_J1850VPW			"J1850VPW"
#define J2534REGISTRY_ISO9141			"ISO9141"
#define J2534REGISTRY_ISO14230			"ISO14230"

// DPA specific
#define J2534REGISTRY_DEVICE_DPA_COMMPORT	"CommPort"

#define J2534REGISTRY_MAX_STRING_LENGTH		1024
#define J2534REGISTRY_MINI_STRING_LENGTH	128

typedef enum
{
	J2534REGISTRY_DEVICE_DPA = 0x01,
	J2534REGISTRY_DEVICE_CAYMAN,
	J2534REGISTRY_INVALID_DEVICE
}
J2534REGISTRY_DEVICE_TYPE;

typedef enum
{
	J2534REGISTRY_ERR_SUCCESS = 0x00,
	J2534REGISTRY_ERR_DEVICE_NOT_FOUND,
	J2534REGISTRY_ERR_NULL_PARAMETER,
	J2534REGISTRY_ERR_INSUFFICIENT_WRITING_PRIVILEGE,
	J2534REGISTRY_ERR_NO_DATA_KEY_VALUE
}
J2534REGISTRY_ERROR_TYPE;

typedef struct 
{
	char chCommPort[J2534REGISTRY_MAX_STRING_LENGTH];
}
J2534REGISTRY_CONFIG_DPA;

typedef union
{
	J2534REGISTRY_CONFIG_DPA stJ2534RegConfigDPA;
}
J2534REGISTRY_DEVICE;

typedef struct {
	DWORD	dwDeviceId;
	char	chVendor[J2534REGISTRY_MAX_STRING_LENGTH];
	char	chName[J2534REGISTRY_MAX_STRING_LENGTH];
	char	chProtocolSupported[J2534REGISTRY_MAX_STRING_LENGTH];
	DWORD	dwCAN;
	DWORD	dwISO15765;
	DWORD	dwJ1850PWM;
	DWORD	dwJ1850VPW;
	DWORD	dwISO9141;
	DWORD	dwISO14230;
	DWORD	dwSCIAEng;
	DWORD	dwSCIATrans;
	DWORD	dwSCIBEng;
	DWORD	dwSCIBTrans;
	DWORD	dwSWISO15765PS;
	DWORD	dwSWCANPS;
	DWORD	dwGMUARTPS;
	char	chConfigApplication[J2534REGISTRY_MAX_STRING_LENGTH];
	char	chFunctionLibrary[J2534REGISTRY_MAX_STRING_LENGTH];
	char	chAPIVersion[J2534REGISTRY_MAX_STRING_LENGTH];	
	char	chProductVersion[J2534REGISTRY_MAX_STRING_LENGTH];	
	DWORD	dwLogging;
	char	chLoggingDirectory[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD   dwLoggingSizeMaxKB;     
    char    chDataLoggingFileName[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD   dwDataLoggingSizeMaxKB;
    DWORD   dwDataLoggingAppend;
    DWORD   dwDefaultCANDataRate;   // Used for Python Applications
						
	J2534REGISTRY_DEVICE_TYPE	enJ2534RegistryDeviceType; 
	J2534REGISTRY_DEVICE		unJ2534RegistryDevice;
	
//	DWORD	dwTimeStampResolution;
//	char	chIPAddress[J2534REGISTRY_MAX_STRING_LENGTH];
//	DWORD	dwPortNum;
} J2534REGISTRY_CONFIGURATION;

class CJ2534Registry
{
	public:
		CJ2534Registry();
		~CJ2534Registry();
		J2534REGISTRY_ERROR_TYPE GetRegistry(
			J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);
		J2534REGISTRY_ERROR_TYPE WriteToRegistry(
			J2534REGISTRY_CONFIGURATION* pstJ2534RegistryConfig);

	private:
		HKEY* FindJ2534Entry(
			char chJ2534DeviceName[J2534REGISTRY_MAX_STRING_LENGTH]);
		void CreateWriteKeyValues(HKEY *thiskey,
			J2534REGISTRY_CONFIGURATION stJ2534RegistryConfig);
};

#endif 
