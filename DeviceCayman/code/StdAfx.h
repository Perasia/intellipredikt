// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__E8E463A2_9185_11D9_94E3_0040F6B43A4C__INCLUDED_)
#define AFX_STDAFX_H__E8E463A2_9185_11D9_94E3_0040F6B43A4C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#define SMALL_TEXT_SIZE		            80

#include <afx.h>
#include <afxwin.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__E8E463A2_9185_11D9_94E3_0040F6B43A4C__INCLUDED_)
