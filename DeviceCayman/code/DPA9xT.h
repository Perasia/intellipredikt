/**************************************/
/*                                    */
/* Dearborn Group, Copyright (c) 2008 */
/* Dearborn Protocol Adapter          */
/* Version 9.13                       */
/*                                    */
/**************************************/

#ifndef _DPA9XTH
#define _DPA9XTH

#ifdef  _DLTSRC
  #define SCOPE
  #define DllExport   __declspec( dllexport )
#else
  #define SCOPE extern
  #define DllExport
#endif

/***********/
/* defines */
/***********/
#define DllImport       __declspec( dllimport )
//#define DllExport       __declspec( dllexport )

/* maximum number of DPA open at any point in time */
#define MAX_DPAS  8
/* max size of data buffer for mailbox */
#define MAILBOX_BUFFER_SIZE       2048
#define MAILBOX_BUFFER_SIZE_EXT   4128
#define MAILBOX_J1939_BUFFER_SIZE 1785
#define MAILBOX_ISO15765_BUFFER_SIZE 4097
#define MAX_TRANSFER_SIZE         4150
#define MAX_BUFFER_SIZE           4200
#define MaxBufferSize			  MAX_BUFFER_SIZE //old name

/* max size of check datalink string */
#define MAX_CHECKDATALINK_SIZE    120

/* update mailbox flag definitions */
//valid for BOTH transmit and receive
#define UPDATE_DATA_LOCATION    0x0080 // update data location
#define UPDATE_DATA_COUNT       0x0020 // update data count
#define UPDATE_TIME_STAMP       0x0004 // update time
#define UPDATE_ID               0x0002 // update ID / (MID-PID-Priority)
#define UPDATE_FCID				0x0100 // update Flow Control ID for ISO15765
#define UPDATE_CAN_CONFIG		0x0800 // update CAN padding and extended addressing parameters

//ONLY valid for transmit
#define TRANSMIT_IMMEDIATE      0x0040 // transmit immediatly
#define UPDATE_BROADCAST_TIME   0x0010 // update broadcast time
#define UPDATE_BROADCAST_COUNT  0x0008 // update broadcast count
#define UPDATE_DATA             0x0001 // update data
#define SEND_HIGH_VOLTAGE		0x0400 // send the message using high voltage wakeup

// ONLY valid for receive
#define UPDATE_ID_MASK          0x0010 // update ID MASK / (MID-PID-Priority)


/* inihibit flags */
#define TimeStamp_Inhibit       0x20
#define ID_Inhibit              0x40
#define Data_Inhibit            0x80

/************/
/* typedefs */
/************/

/* Enumerations for serial communication ports
        supported by Dearborn Protocol Adapter */
typedef enum CommPortType
{
  eComm1,
  eComm2,
  eComm3,
  eComm4,
  eComm5,
  eComm6,
  eComm7,
  eComm8,
  eComm9
} CommPortType;

/* Enumerations for serial communication baudrate
        supported by Dearborn Protocol Adapter (DPA II, II+, III, IV) */
typedef enum BaudRateType
{
  eB9600,
  eB19200,
  eB28800,
  eB38400,
  eB57600,
  eB115200,
  eB230400,
  eB460800,
  eB4800
} BaudRateType;

/* Enumerations for serial communication baudrate
        supported by Industrial Dearborn Protocol Adapter (DPA III/i)*/
typedef enum IndustrialBaudRateType
{
	eIB1200,
	eIB2400,
	eIB4800,
	eIB9600,
	eIB14400,
	eIB19200,
	eIB28800,
	eIB38400,
	eIB57600,
	eIB115200,
} IndustrialBaudRateType;

/* Enumerations for protocols
   support by Dearborn Protocol Adapter */
typedef enum ProtocolType
{
  eReserved0 = 0,  //Reserved
  eJ1708	= 1,   //J1708 and GMUART
  eJ1850	= 2,   //J1850 Class 2
  eJ1939	= 3,   //SAE J1939 channel 1
  eCAN		= 3,   //CAN channel 1
  eReserved4 = 4,  //Reserved
  eReserved5 = 5,  //Reserved
  eReserved6 = 6,  //Reserved
  eJ1939_2	= 7,   //SAE J1939 channel 2
  eCAN_2	= 7,   //CAN channel 2  
  eISO15765 = 8,   //ISO15765 channel 1
  eISO15765_2 = 9, //ISO15765 channel 2
  eReserved10 = 10,//Reserved
  eISO9141_ZF = 11,//ZF
  eISO14230	= 12,  //ISO14230 (aka Key Word Protocol 2000)
  eISO9141  = 13,   //ISO9141
  eJ1939_3	= 14,   //SAE J1939 channel 3
  eCAN_3	= 14,   //CAN channel 3
  eJ1939_4	= 15,   //SAE J1939 channel 4
  eCAN_4	= 15,   //CAN channel 4
  eISO15765_3 = 16, //ISO15765 channel 3
  eISO15765_4 = 17,  //ISO15765 channel 4
  eVoltageVPP = 0x50  
} ProtocolType;


typedef enum VoltageType
{
	eVBAT		= 0x51
} VoltageType;


typedef enum ReceiveFilterType
{
  ePass,
  eBlock
} ReceiveFilterType;


/* Enumerations for return status
   from Dearborn Protocol Adapter API calls */
typedef enum ReturnStatusType
{
  eNoError,					//Success
  eDeviceTimeout,			//Unable to communicate with the DPA before the timeout period expired.
  eProtocolNotSupported,	//Invalid protocol passed into function
  eBaudRateNotSupported,	//Invalid baud rate passed in
  eInvalidBitIdentSize,		//Invalid identifier size
  eInvalidDataCount,		//Invalid data count. 
  eInvalidMailBox,			//Attempting to transmit or receive with an unopened mailbox
  eNoDataAvailable,			//No data in mailbox
  eMailBoxInUse,			//Not used
  eMailBoxNotActive, 		//Mailbox was not open
  eMailBoxNotAvailable, 	//All mailboxes are in use.
  eTimerNotSupported,		//Timer not supported
  eTimeoutValueOutOfRange,	//A period greater than 1 minute (60,000ms) was specified
  eInvalidTimerValue,		//Timer value out of range
  eInvalidMailBoxDirection,	//Not used
  eSerialIncorrectPort,		//Incorrect com port
  eSerialIncorrectBaud,		//Incorrect baud rate
  eSerialPortNotFound,		//Com port not found
  eSerialPortTimeout,		//Not used
  eCommLinkNotInitialized, 	//Communication port not opened
  eAsyncCommBusy, 			//Still waiting for a previous asynchronous command to finish processing
  eSyncCommInCallBack,		//Not used
  eAsyncCommandNotAllowed,	//Not used
  eSyncCommandNotAllowed,	//Cannot call from within callback (ISR)
  eLinkedMailBox,			//Mailbox is linked
  eInvalidExtendedFlags,	//Not used
  eInvalidCommand,			//Not used
  eInvalidTransportType,	//Invalid transport type
  eSerialOuputError,		//Could not set PC baud
  eInvalidBufferOffset, 	//Invalid buffer offset
  eInvalidBufferLocation,	//Invalid buffer location
  eOutOfMemory,				//Driver needs more memory on computer
  eInvalidDpa,				//Invalid DPA type
  eInvalidDPAHandle,		//Invalid DPA handle
  eInvalidPointer,			//Invalid pointer
  eBaudRateConflict,		//Trying to initialize a com port that is already intitialized at a different baud rate
  eIrqConflict,				//Irq in use
  eIncorrectDriver,			//The driver is the wrong driver
  eInvalidDriverSetup,		//The driver is set up incorrectly
  eInvalidBaseAddress, 		//The base address is invalid
  eInvalidINI,				//The INI record is invalid
  eInvalidDll,				//There is a bad or missing DLL
  eCommVerificationFailed,	//Could not communicate after chane
  eInvalidLock,				//Lock not found
  eServerDisconnect,		//The TCP/IP server disconnected
  eInvalidSocket,			//Invalid socket
  eWinSockError,			//Win sock error occurred
  eInvalidDisplayType,		//Invalid display type
  eModemError,				//A modem error occurred
  eInvalidResetType, 		//Invalid reset type
  eProtocolNotInitialzed,	//Protocol not initialized
  eOperatingSystemNotSupported, //OS not supported
  eInvalidFMode,			//Invalid FMode Type  
  eInvalidSerialNumber, 	//Serial number couldn't be retreived from device
  eVoltageNotSupported,		//Voltage level out of range
} ReturnStatusType;

/* Enumerations for MailBox direction */
typedef enum MailBoxDirectionType
{
  eRemoteMailBox,       /* used for receiving data from link */

  eDataMailBox          /* used for sending data across link */

} MailBoxDirectionType;

/* Enumerations for Transport Layer type */
typedef enum TransportLayerType
{
  eTransportNone = 0x1,       /* No Transmort Layer */
  eTransportBAM  = 0x2,       /* Use Broadcast Anouncment Message */
  eTransportRTS  = 0x4,       /* Use Request to Send */
  eISO15765Transport = 0x8    /* ISO15765 Transport */
} TransportLayerType;

/* Enumerations for FINIT type */
typedef enum FiveBaudType
{
  eFBISO91412 	= 0x00,
  eFBInverseKey2 = 0x01,
  eFBInverseAddress  = 0x02,
  eFBISO9141 	 = 0x03,
} FiveBaudType;

/* Enumerations for Transmit MailBox type */
typedef enum TransmitMailBoxType
{
  eResident,            /* MailBox remain active and can be used again  */
                        /* TransmitMailBox function                     */

  eRelease              /* MailBox is automatically unloaded after used */

} TransmitMailBoxType;

/* Enumerations for Dearborn Protocol Adapter Errors */
typedef enum DataLinkErrorCodeType
{
  eBusOff = 1,
  eCanOverRun = 2,
  eErrorSendingAsync = 3,
  eInvalidJ1708Checksum = 4,
  eInvalidJ1708Message = 5,
} DataLinkCANErrorCodeType;

typedef enum DataLink15765ErrorCodeType{
 eISO15765Timeout_A =    0x01,
 eISO15765Timeout_BS =   0x02,
 eISO15765PadError =     0x03,
 eISO15765Invalid_FS =   0x05,
 eISO15765WFT_Overrun =  0x07,
 eISO15765Buf_OverFlow = 0x08,
 eISO15765Error =        0x09 
} DataLink15765ErrorCodeType;

typedef enum LinkType
{
  eNoLink,
  eLinkHead,
  eLinkBody,
  eLinkTail
} LinkType;

typedef enum 
{
 eIRQ5 = 5,
 eIRQ7 = 7,
 eIRQ10 = 10,
}PCIrqType;


typedef enum
{
	eFullReset = 0,
	eCommReset = 1,
	eDPARingBufferReset = 4
}eResetType;

typedef enum FModeType
{
	eFastInit 	= 0x01,
	eFiveBaudInit = 0x02,
	eZFInit = 0x03
} FModeType;

typedef enum
{
	eFullSpeed,
	eHighSpeed
}eUSBSpeed;

/**************/
/* structures */
/**************/
/****************************************************************************/
/* Structure for PC copy of MailBox                                         */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
                                            /* mailbox                      */

  BYTE                  bActive;            /* Active or inuse flag         */

  BYTE                  bBitIdentSize;      /* specifies the length of the  */
                                            /* MailBox identifier, max 32   */

  BYTE                  bMailBoxNumber;     /* Handle used for communicat-  */
                                            /* ion between PC and DPA       */

  short                 iVBBufferNumber;

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier Sixe      */

  BYTE                  bCTSSource;         /* Destination Address to be    */
											/* for RTS Transport layer      */

  long unsigned int     dwMailBoxIdentMask; /* MailBox Identifier mask      */ 
                                              /* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Pass or Bloxk filtering      */

  BYTE                  bMailBoxDirectionData; /* used to identify MailBox  */
                                            /* direction                    */

  BYTE                  bTransportType;     /* Type of transport to use     */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
                                            /* false = relative             */

                                            /* address of callback routine  */
                                            /* NULL disables                */
  void (CALLBACK *pfApplicationRoutine)(void *);

                                            /* address of callback routine  */
                                            /* NULL disables                */
  void (CALLBACK *pfMailBoxReleased)(void *);


  DWORD                 dwTimeStamp;        /* time of received message     */

  DWORD                 dwTransmitTimeStamp;/* time of transmited message   */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
                                            /* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
                                            /* message is to be sent for    */
                                            /* broadcast messages           */

  WORD                  wDataCount;         /* Number of bytes per message  */
                                            /* maximum of                   */
                                            /* MAILBOX_BUFFER_SIZE bytes    */

  BYTE                  bData[MAILBOX_BUFFER_SIZE];
                                            /* temp holding buffer for      */
                                            /* message data                 */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
                                                  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
																																						/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bLinkType;          /* J1708 link type for multiple */
                                            /* PID's                        */

  BYTE                  bLink;              /* J1708 link for multiple PID's*/

  BYTE                  bPriority;          /* J1708 priority               */

  BYTE                  bMID;               /* J1708 MID                    */

  BYTE                  bPID;               /* J1708 PID                    */

  BYTE                  bMIDMask;           /* J1708 MID mask               */

  BYTE                  bPIDMask;           /* J1708 PID mask               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  BYTE                  bDataRequested;     /* flag indicates that data has */
											/* been requested from the DPA  */

  BYTE                  bReceiveFlags;      /* flag indicating requested    */
                                            /* data                         */

  BYTE                  bDataUpdated;       /* flag indicates that data has */
                                            /* been updated                 */

  void                 *vpData;             /* address of users copy of    */
                                            /* message data                */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */
} MailBoxType;

/****************************************************************************/
/* Structure for InitCommLink                                               */
/****************************************************************************/
typedef struct
{
  BYTE                  bCommPort;          /* Comm: prot to use			*/

  BYTE                  bBaudRate;          /* Baud rate to communicate		*/

  BYTE					bPolledPort;        /* set to 1 if this is a polled port */
  
  char					cModem[80];			/* DPA/H Modem parameters	   */

} CommLinkType;

/****************************************************************************/
/* Structure for PCCardLink                                               */
/****************************************************************************/
typedef struct
{
  WORD 					bBaseAddress;  /*The base address of the card		*/

  BYTE                  bIrq;          /* The Irq of the isa card		    */
                                       /* with the DPA						*/
} PCCardType;

/****************************************************************************/
/* Structure for TCP_IPType                                                 */
/****************************************************************************/
typedef struct
{
	unsigned char IP1;
	unsigned char IP2;
	unsigned char IP3;
	unsigned char IP4;
	short portNumber;
} TCP_IPType;  


/****************************************************************************/
/* Structure for USBLinkType                                               */
/****************************************************************************/
typedef struct
{
  WORD 					wInterface;  /*The interface we should init from the DPA */

  BYTE                  bHS_FS;      /* High speed or  Full speed USB communications*/
									 /* Default = eFullSpeed*/
} USBLinkType;


/****************************************************************************/
/* Structure for Dearborn Protocol Adapter Error                            */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol that     */
                                            /* error occured               */

  BYTE                  bErrorCode;         /* specifies error code        */
} DataLinkErrorType;

/****************************************************************************/
/* structure used for timer interrupts                                      */
/****************************************************************************/
typedef struct
{
  DWORD                 dwTimeOut;			/* specifies the period             */
											/* in milliseconds of the interrupt */

  void (CALLBACK *pTimerFunction)(DWORD);
											/* address of callback routine for  */
											/* timer                            */
                                            /* NULL disables                    */
} EnableTimerInterruptType;


/****************************************************************************/
/* Structure for initializing Dearborn Protocol Adapter Datalink            */
/* J1939, CAN, J1708, J1850													*/
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  BYTE                  bParam0;            /* specifies parameter 0 for    */
                                            /* specified protocol           */

  BYTE                  bParam1;            /* specifies parameter 1 for    */
                                            /* specified protocol           */

  BYTE                  bParam2;            /* specifies parameter 2 for    */
                                            /* specified protocol           */

  BYTE                  bParam3;            /* specifies parameter 3 for    */
                                            /* specified protocol           */

  BYTE                  bParam4;            /* specifies parameter 4 for    */
                                            /* specified protocol           */

  void (CALLBACK *pfDataLinkError)(MailBoxType *, DataLinkErrorType *);
                                            /* address of routine to call   */
                                            /* in case of Dearborn Protocol */
                                            /* Adapter error                */

  void (CALLBACK *pfTransmitVector)(MailBoxType *);
                                            /* address of routine to call   */
                                            /* when message is transmitted  */

  void (CALLBACK *pfReceiveVector)(void);   /* address of routine to call   */
                                            /* when message is received     */
} InitDataLinkType;


/****************************************************************************/
/* Structure for reading in protocol power up parameters from the DPA		*/
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  BYTE                  bParamCount;        /* the number of params used for*/
											/* the given protocol			*/

  BYTE                  bParam0;            /* specifies parameter 0 for    */
                                            /* specified protocol           */

  BYTE                  bParam1;            /* specifies parameter 1 for    */
                                            /* specified protocol           */

  BYTE                  bParam2;            /* specifies parameter 2 for    */
                                            /* specified protocol           */

  BYTE                  bParam3;            /* specifies parameter 3 for    */
                                            /* specified protocol           */

  BYTE                  bParam4;            /* specifies parameter 4 for    */
                                            /* specified protocol           */
} ReadDataLinkType;

/****************************************************************************/
/* Structure for loading Dearborn Protocol Adapter Mailbox                  */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  MailBoxType          *pMailBoxHandle;     /* address of MailBox handle    */
											/* returned if load was success */

  BYTE                  bRemote_Data;       /* used to identify MailBox     */
											/* direction                    */

  unsigned char         bTransportType;     /* Transport Type to be used    */

  BYTE                  bResidentOrRelease; /* Auto Release Data mailbox    */

  BYTE                  bBitIdentSize;      /* Mailbox Idenivier size       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier 11/29     */

  BYTE                  bCTSSource;         /* Destination Address to be    */
                                            /* for RTS Transport session    */

  DWORD                 dwMailBoxIdentMask; /* MailBox Identifier mask      */
											/* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Match or Reject filter       */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
												  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
											/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
											/* false = relative             */

  DWORD                 dwTimeStamp;        /* specifies if time transmit   */
											/* message is to be sent        */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
											/* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
											/* message is to be sent for    */
											/* broadcast messages           */

  BYTE                  bLinkType;          /* J1708 link type for multiple */
                                            /* PID's                        */

  BYTE                  bLink;              /* J1708 link for multiple PID's*/

  BYTE                  bPriority;          /* J1708 priority               */

  BYTE                  bMID;               /* J1708 MID                    */

  BYTE                  bPID;               /* J1708 PID                    */

  BYTE                  bMIDMask;           /* J1708 MID mask               */

  BYTE                  bPIDMask;           /* J1708 PID mask               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  void (CALLBACK *pfApplicationRoutine)(MailBoxType *); /* address of       */
											/* callback routine             */

  BYTE                  bMailBoxReleased;   /* Mailbox Releases flag        */

  void (CALLBACK *pfMailBoxReleased)(MailBoxType *);  /* Address of Mailbox */
											/* Release callback routine     */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */

  WORD                  wDataCount;         /* Number of bytes per message */
                                            /* maximum of                  */
                                            /* MAILBOX_BUFFER_SIZE bytes   */

  void                 *vpData;             /* address of message data     */

} LoadMailBoxType;

/****************************************************************************/
/* structure used for J1939 Transpot protocol configuration                 */
/****************************************************************************/
typedef struct
{
  BYTE  bProtocol;
  WORD  iBamTimeout;            /* Inter message timeout for BAM receive    */
  WORD  iBAM_BAMTXTime;         /* Time between BAM and first data packet   */
  WORD  iBAM_DataTXTime;         /* Time between BAM Data packets           */
  WORD  iRTS_Retry;             /* Number of times to send RTS without CTS  */
  WORD  iRTS_RetryTransmitTime; /* Time between retries of RTS TX           */
  WORD  iRTS_TX_Timeout;        /* Time to wait for a CTS after RTS TX      */
  WORD  iRTS_TX_TransmitTime;   /* Time between data packets on RTS TX      */
  WORD  iRTS_RX_TimeoutData;    /* Timeout between data packets on RTS RX   */
  WORD  iRTS_RX_TimeoutCMD;     /* Timeout between CTS and 1st data packet  */
  BYTE  iRTS_RX_CTS_Count;      /* Number of packets to CTS for             */
  BYTE  iRTS_TX_CTS_Count;      /* Number of packets to CTS for             */
} ConfigureTransportType;

typedef struct
{
  BYTE  bPCBaud;
  BYTE  bDPABaud;
} SetBaudRateType;

typedef struct
{
  BYTE  bResetType;
} ResetType;

typedef enum 
{
  eDash,
  eServiceBay,
} eDisplayType;

/****************************************************************************/
/* Structure for Display                                                    */
/****************************************************************************/
typedef struct
{
	unsigned char eDisplayType;  //eDash or eServiceBay
	unsigned char *line1;
	unsigned char *line2;
	unsigned char *line3;
	unsigned char *line4;
} DisplayType;

typedef struct
{
    unsigned char eDisplayType;
	void (CALLBACK *displayCallback)( short buttonNumber, void *userPointer);
	void *userPointer;
} InitDisplayType;



/****************************************************************************/
/* Structure for Internal Use                                               */
/****************************************************************************/

typedef struct DG_Proto
{
  WORD            wDataCount;
  WORD            wReadIndex;
  BYTE            bEchoChar;
  BYTE            bRetryCount;
  void           *MailBoxHandle;
  BYTE            bData[MAX_BUFFER_SIZE];
} DG_Proto;


/****************************************************************************/
/* Structure for PC copy of Extended MailBox                                */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
                                            /* mailbox                      */

  BYTE                  bActive;			/* Active or inuse flag         */

  BYTE                  bIdentSize;			/* specifies the length of the  */
                                            /* MailBox identifier   */

  BYTE                  bMailBoxNumber;     /* Handle used for communicat-  */
                                            /* ion between PC and DPA       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier Sixe      */

  BYTE                  bCTSSource;         /* Destination Address to be    */
											/* for RTS Transport layer      */

  DWORD					dwFCIdent;			/* flow contol identifier for	*/
											/* ISO15765 msgs				*/											

  DWORD					dwMailBoxIdentMask; /* MailBox Identifier mask      */ 
                                              /* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Pass or Bloxk filtering      */

  BYTE                  bMailBoxDirectionData; /* used to identify MailBox  */
                                            /* direction                    */

  BYTE                  bTransportType;     /* Type of transport to use     */

  BYTE					bHighVoltage;		/* High Voltage messaging		*/

  BYTE					bSCIConfig;			/* SCI message config duplex	*/
                                            /* mode and Vpp flag			*/

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
                                            /* false = relative             */
                                            
  void (CALLBACK *pfApplicationRoutine)(void *); /* address of callback     */
                                            /* routine NULL disables        */

                                            
  void (CALLBACK *pfMailBoxReleased)(void *);/* address of callback			*/
                                            /* routine NULL disables        */

  DWORD                 dwTimeStamp;        /* time of received message     */

  DWORD                 dwTransmitTimeStamp;/* time of transmited message   */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
                                            /* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
                                            /* message is to be sent for    */
                                            /* broadcast messages           */

  WORD                  wDataCount;         /* Number of bytes per message  */
                                            /* maximum of                   */
                                            /* MAILBOX_BUFFER_SIZE bytes    */

  BYTE                  bData[MAILBOX_BUFFER_SIZE_EXT];
                                            /* temp holding buffer for      */
                                            /* message data                 */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
                                                  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
																																						/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

 BYTE					bDataPadInhibit;	/* ISO15765 msgs padding		*/
											/* 1 = do not pad message		*/
											/* 0 = padding enabled			*/
 
 BYTE					bEAEnabled;			/* ISO15765 Extended Addressing	*/
											/* (1 = enabled, 0 = disabled)  */

  BYTE                  bPriority;          /* UART message priority        */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  BYTE                  bDataRequested;     /* flag indicates that data has */
											/* been requested from the DPA  */

  BYTE                  bReceiveFlags;      /* flag indicating requested    */
                                            /* data                         */

  BYTE                  bDataUpdated;       /* flag indicates that data has */
                                            /* been updated                 */

  void                 *vpData;  /* address of users copy of    */
                                            /* message data                */

  void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */
} MailBoxTypeExt;

/****************************************************************************/
/* Structure for loading Dearborn Protocol Adapter Extended Mailbox         */
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          /* specifies protocol for       */
											/* Dearborn Protocol Adapter    */

  MailBoxTypeExt       *pMailBoxHandle;     /* address of MailBox handle    */
											/* returned if load was success */

  BYTE                  bRemote_Data;       /* used to identify MailBox     */
											/* direction                    */

  BYTE					bTransportType;     /* Transport Type to be used    */

  BYTE					bHighVoltage;		/* High Voltage messaging		*/

  BYTE					bSCIConfig;			/* SCI message config duplex	*/
                                            /* mode and Vpp flag			*/

  BYTE                  bResidentOrRelease; /* Auto Release Data mailbox    */

  BYTE                  bIdentSize;			/* Mailbox Idenivier size       */

  DWORD                 dwMailBoxIdent;     /* MailBox Identifier 11/29     */

  BYTE                  bCTSSource;         /* Destination Address to be    */
                                            /* for RTS Transport session    */

  DWORD					dwFCIdent;			/* flow contol identifier for	*/
											/* ISO15765 msgs				*/

  BYTE					bDataPadInhibit;	/* ISO15765 msgs padding		*/
											/* 1 = do not pad message		*/
											/* 0 = padding enabled			*/

  BYTE					bEAEnabled;			/* ISO15765 Extended Addressing	*/
											/* (1 = enabled, 0 = disabled)  */

  DWORD                 dwMailBoxIdentMask; /* MailBox Identifier mask      */
											/* (1 = match, 0 = don't care)  */

  BYTE                  bFilterType;        /* Match or Reject filter       */

  BYTE                  bTransparentUpdateEnable; /* flag for enabling      */
												  /* transparent update     */

  BYTE                  bTimeStampInhibit;  /* flag for removing timestamp  */
											/* in receive data message      */

  BYTE                  bIDInhibit;         /* flag for removing MailBox    */
                                            /* Identifier in receive data   */
                                            /* message                      */

  BYTE                  bDataCountInhibit;  /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bDataInhibit;       /* flag for removing data in    */
                                            /* receive data message         */

  BYTE                  bTimeAbsolute;      /* flag for setting absolute or */
                                            /* relative time for timestamp  */
                                            /* true = absolute              */
											/* false = relative             */

  DWORD                 dwTimeStamp;        /* specifies if time transmit   */
											/* message is to be sent        */

  DWORD                 dwBroadcastTime;    /* specifies time interval used */
											/* for broadcast messages       */

  short                 iBroadcastCount;    /* specifies number of times    */
											/* message is to be sent for    */
											/* broadcast messages           */

  BYTE                  bPriority;          /* UART priority               */

  BYTE                  bExtendedPtrMode;   /* Use Scratch pad for data     */

  WORD                  wExtendedOffset;    /* Location in scratch pad      */

  void (CALLBACK *pfApplicationRoutine)(MailBoxTypeExt *); /* address of       */
											/* callback routine             */

  BYTE                  bMailBoxReleased;   /* Mailbox Releases flag        */

  void (CALLBACK *pfMailBoxReleased)(MailBoxTypeExt *);  /* Address of Mailbox */
											/* Release callback routine     */

    void                 *vpUserPointer;      /* a user defined pointer      */
                                            /* typical use is for a "this" */
                                            /* pointer                     */

  WORD                  wDataCount;         /* Number of bytes per message */
                                            /* maximum of                  */
                                            /* MAILBOX_BUFFER_SIZE bytes   */

  void                 *vpData;  /* address of message data     */

} LoadMailBoxTypeExt;

/****************************************************************************/
/* Structure for initializing Dearborn Protocol Adapter						*/
/* ISO15765, ISO9141, ISO14230												*/
/****************************************************************************/
typedef struct
{
  BYTE                  bProtocol;          
  BYTE                  bParam0;            
  BYTE                  bParam1;            
  BYTE                  bParam2;            
  BYTE                  bParam3;            
  BYTE                  bParam4;            
  void (CALLBACK *pfDataLinkError)(MailBoxTypeExt *, DataLinkErrorType *);                                            
  void (CALLBACK *pfTransmitVector)(MailBoxTypeExt *);                                            
  void (CALLBACK *pfReceiveVector)(void);   
  void (CALLBACK *pfReceiveFirstByteVector)(MailBoxTypeExt *);
} InitDataLinkTypeExt;


/****************************************************************************/
/* structure used for J1939 Transpot protocol configuration                 */
/****************************************************************************/
typedef struct
{
  WORD  wBamTimeout; /* Inter message timeout for BAM receive    */
  WORD  wBAM_BAMTXTime;         /* Time between BAM and first data packet   */
  WORD  wBAM_DataTXTime;         /* Time between BAM Data packets           */
  WORD  wRTS_Retry;  /* Number of times to send RTS without CTS  */
  WORD  wRTS_RetryTransmitTime; /* Time between retries of RTS TX           */
  WORD  wRTS_TX_Timeout;        /* Time to wait for a CTS after RTS TX      */
  WORD  wRTS_TX_TransmitTime;   /* Time between data packets on RTS TX      */
  WORD  wRTS_RX_TimeoutData;    /* Timeout between data packets on RTS RX   */
  WORD  wRTS_RX_TimeoutCMD;     /* Timeout between CTS and 1st data packet  */
  BYTE  bRTS_RX_CTS_Count;      /* Number of packets to CTS for             */
  BYTE  bRTS_TX_CTS_Count;      /* Number of packets to CTS for             */
} ConfigureJ1939Type;

/****************************************************************************/
/* structure used for ISO15765 protocol configuration						*/
/****************************************************************************/
typedef struct
{
BYTE bISO15765_BS;		//Block size interface reports to vehicle for receiving segmented transfers
BYTE bISO15765_STMIN;   //Seperation time the interface reports to vehicle for receivng segmented transfers
WORD wBS_TX;			//Block size interface should use to transmit segmented messages to the vehicle.
						//If 0xffff, use the value reported by the vehicle.
WORD wSTMIN_TX;			//Separation time the interface should use to transmit segmented messages to vehicle.
						//If 0xffff, use the value reported by the vehicle.
BYTE bWFTMAX;			//Max number of WAIT flow control frames allowed durrring a multi-segmented transer.
WORD wBSTimer;			//Time VDA wait for flow control response from first frame request - default 1000ms.
} ConfigureISO15765Type; 

/****************************************************************************/
/* structure used for ISO9141 protocol configuration						*/
/****************************************************************************/
typedef struct
{
WORD wP1_MAX;	//Max inter-byte time for ECU responses
WORD wP3_MIN;	//Min tme between end of ECU req and start of new tester req
WORD wP4_MIN;	//Min inter-byte time for tester req
WORD wW0;	//Min bus idle time before tester starts to tx address byte
WORD wW1; 	//Max time from end of address byte to start of sync pattern
WORD wW2; 	//Max time from end of sync pattern to start of key byte 1
WORD wW3; 	//Max time between key byte 1 and key byte 2
WORD wW4; 	//Min time between key byte 2 and its inversion from tester
WORD wW5; 	//Min bus idle time before the tester starts to tx add byte
WORD wTIDLE; 	//Min bus idle ime that is needed before a fast init begins
WORD wTINIL; 	//Duration of low pulse in fast init
WORD wTWUP; 	//Duration of wake-up pulse for fast init
} ConfigureISO9141Type;

/****************************************************************************/
/* structure used for ISO9141_ZF protocol configuration						*/
/****************************************************************************/
typedef struct
{
WORD wT0;	//Idle wait time
WORD wT1;	//Init wait time
WORD wT2;	//Sync wait time
WORD wT3;	//KB1 wait time
WORD wT4; 	//Timeout KB2 echo
WORD wT5; 	//Config wait time
WORD wT6; 	//Wait time
WORD wT7; 	//Data timeout
WORD wT8; 	//Wait time
WORD wT9; 	//Echo timeout
WORD wT10; 	//Wait time
WORD wT11; 	//Block timeout
} ConfigureISO9141_ZFType;

/****************************************************************************/
/* Structure for issuing Fast or Five Baud Init								*/
/****************************************************************************/

typedef struct
{
  WORD                  wDataCount;         /* length of response */

  BYTE                  *pData; /* pointer to response data   */
                                            
} FInitResponse;


typedef struct
{
  BYTE                  bFInitType;         /* specifies fast or five baud Init */


  BYTE                  *pData; /* pointer to data   */
  
  void (CALLBACK *pfReceiveKeyBytes)(FInitResponse *FInitResp);   /* address of routine to call   */
                                            /* when response message is received */
} FInitType;

#undef SCOPE
#endif
