//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Unat2534SDK.rc
//
#define IDD_MAIN                        101
#define IDR_MAINMENU                    103
#define IDD_CONNECT                     104
#define IDD_READMSGS                    108
#define IDD_STARTFILTER                 109
#define IDD_WRITEMSGS                   110
#define IDD_STOPFILTER                  111
#define IDD_STARTPERIODIC               112
#define IDD_IOCTL                       113
#define IDD_STOPPERIODIC                114
#define IDD_SETPROGRAMMINGVOLTAGE       126
#define IDI_ICON1                       127
#define IDD_OPEN                        127
#define IDD_LOGICALDISCONNECT           128
#define IDD_SELECT                      129
#define IDC_COMBO1                      1000
#define IDC_OUTPUT                      1001
#define IDC_COPY                        1002
#define IDC_RADIO1                      1003
#define IDC_RADIO2                      1005
#define IDC_CANGROUP                    1006
#define IDC_CONNECT                     1007
#define IDC_NUM                         1009
#define IDC_TIMEOUT                     1010
#define IDC_TXFLAGS                     1010
#define IDC_READ                        1011
#define IDC_TXFLAGS2                    1011
#define IDC_PROTOCOLID                  1011
#define IDC_WRITEMSGS                   1012
#define IDC_PHY_FILTER_ID               1012
#define IDC_MSGLIST                     1013
#define IDC_PERIODIC_CHNID              1013
#define IDC_ADD                         1015
#define IDC_MSG                         1016
#define IDC_REMOVE                      1017
#define IDC_STOP                        1018
#define IDC_PROTOCOLID2                 1018
#define IDC_EDTCHANNEL                  1018
#define IDC_EDTCHANNEL2                 1019
#define IDC_EDTCHANNEL_MSGHDL           1020
#define IDC_DELAY                       1022
#define IDC_START                       1023
#define IDC_PATTERN                     1024
#define IDC_MASK                        1025
#define IDC_TYPE                        1026
#define IDC_FCMSG                       1027
#define IDC_COMBO2                      1028
#define IDC_CONFIGPARAM                 1028
#define IDC_PARAM                       1029
#define IDC_EDIT2                       1030
#define IDC_INT                         1030
#define IDC_CHOICE                      1031
#define IDC_ISSUEIOCTL                  1032
#define IDC_IOCTL                       1033
#define IDC_FLAGS                       1034
#define IDC_PINNUMBER                   1035
#define IDC_VOLTAGE                     1036
#define IDC_COMBO_DEVICE                1037
#define IDC_OPEN                        1038
#define IDC_EXTRADATAINDEX              1039
#define IDC_PROTOCOL                    1040
#define IDC_GET_PROTOCOL_ID             1041
#define IDC_CHANNELID                   1042
#define IDC_EDIT_LOCAL_ID               1043
#define IDC_CHANNELID2                  1043
#define IDC_EDIT_REMOTE_ID              1044
#define IDC_STATIC_lOCAL_ID             1045
#define IDC_EDT_IOCTL_CHID              1046
#define IDC_STAT_CHANNEL_ID             1047
#define IDC_EDIT_PHY_ID                 1048
#define IDC_STATIC_PHY_ID               1049
#define IDC_SEL_CHANNELS                1050
#define IDC_STATIC_CHNID                1051
#define PASSTHRUCONNECT                 40002
#define PASSTHRUDISCONNECT              40003
#define PASSTHRUREADMSGS                40004
#define PASSTHRUWRITEMSGS               40005
#define PASSTHRUSTARTPERIODICMSG        40006
#define PASSTHRUSTOPPERIODICMSG         40007
#define PASSTHRUSTARTMSGFILTER          40008
#define PASSTHRUSTOPMSGFILTER           40009
#define PASSTHRUREADVERSION             40010
#define PASSTHRUGETLASTERROR            40011
#define PASSTHRUIOCTL                   40012
#define IDM_SAVE                        40013
#define IDM_EXIT                        40014
#define IDM_RESET                       40016
#define PASSTHRUOPEN                    40017
#define PASSTHRUCLOSE                   40018
#define PASSTHRUSETPROGRAMMINGVOLTAGE   40019
#define PASSTHRUQUEUEMSGS               40020
#define PASSTHRUSCANFORDEVICES          40021
#define PASSTHRUGETNEXTDEVICE           40022
#define PASSTHRULOGICALCONNECT          40023
#define PASSTHRULOGICALDISCONNECT       40024
#define ID_ABOUT                        40025
#define PASSTHRUSELECT                  40026
#define ABOUT                           65535

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         40029
#define _APS_NEXT_CONTROL_VALUE         1052
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
