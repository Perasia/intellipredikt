Description:
============
The scope of this utility is to help user develop a J2534-based application compliant to 04.04 specification. The source 
code for the utility is located under "C:\Dearborn Group Products\d-briDGe J2534\Sample\Source Code" (default installation
directory) and can be used for reference while developing an application. This application is designed to communicate 
with any Dearborn Group Technology J2534-based products. The executable can be created by compiling the source code with 
Microsoft Visual C++ 6.0 or later.