// Unat2534SDK.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include "windowsx.h"
#include "commdlg.h"
#include "functions.h"
#include "stdio.h"
#include "j2534.h"
#include "defines.h"
#include "fcntl.h"
#include "sys/stat.h"
#include "io.h"

char  *UpgradedVersion;

char OriginalVSIHWVersion[] = "1.10";
char UpgradedVSIHWVersion[] = "1.20";


// Constants
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
#define J2534REGISTRY_KEY_PATH         "Software\\PassThruSupport.05.00"
#define J2534REGISTRY_KEY_PATH_6432        "Software\\WOW6432Node\\PassThruSupport.05.00"
#else 
#define J2534REGISTRY_KEY_PATH          "Software\\PassThruSupport.04.04"
#define J2534REGISTRY_KEY_PATH_6432        "Software\\WOW6432Node\\PassThruSupport.04.04"
#endif
#define J2534REGISTRY_DEVICE_ID         "DeviceId"
#define J2534REGISTRY_NAME              "Name"
#define J2534REGISTRY_DEARBORN_GROUP    "Dearborn Group, Inc."
#define J2534REGISTRY_DEVICES           "Devices"
#define J2534REGISTRY_DOUBLE_BACKSLASH  "\\"
#define J2534REGISTRY_HYPHEN_SPACE      " - "
#define J2534REGISTRY_CAYMAN            "VSI-2534"
#define J2534REGISTRY_VENDOR            "Vendor"
#define J2534REGISTRY_NAME              "Name"
#define J2534REGISTRY_PROTOCOLSSUPPORT  "ProtocolsSupported"
#define J2534REGISTRY_CONFIGAPP         "ConfigApplication"
#define J2534REGISTRY_FUNCLIB           "FunctionLibrary"
#define J2534REGISTRY_APIVERSION        "APIVersion"
#define J2534REGISTRY_PRODUCTVERSION    "ProductVersion"
#define J2534REGISTRY_LOGGING           "Logging"
#define J2534REGISTRY_LOGGINGDIRECTORY  "LoggingDirectory"
#define J2534REGISTRY_CAN               "CAN"
#define J2534REGISTRY_ISO15765          "ISO15765"
#define J2534REGISTRY_J1850PWM          "J1850PWM"
#define J2534REGISTRY_J1850VPW          "J1850VPW"
#define J2534REGISTRY_ISO9141           "ISO9141"
#define J2534REGISTRY_ISO14230          "ISO14230"
#define J2534REGISTRY_SCI_A_ENGINE      "SCI_A_ENGINE"
#define J2534REGISTRY_SCI_A_TRANS       "SCI_A_TRANS"
#define J2534REGISTRY_SCI_B_ENGINE      "SCI_B_ENGINE"
#define J2534REGISTRY_SCI_B_TRANS       "SCI_B_TRANS"
#define J2534REGISTRY_SWCAN_ISO15765_PS "SWCAN_ISO15765_PS"
#define J2534REGISTRY_SWCAN_PS          "SWCAN_PS"
#define J2534REGISTRY_GM_UART_PS        "GM_UART_PS"
#define J2534REGISTRY_MAX_STRING_LENGTH     MAX_STRING_LENGTH
#define J2534REGISTRY_MINI_STRING_LENGTH    128
#define MAX_STRING_LENGTH               16384
#define MAX_VERSION_TOKENS              20


char tempbuf[MAX_STRING_LENGTH], path[MAX_STRING_LENGTH];
unsigned char tempstr[16384], tempmsg[16384], tempmsg2[16384];  
char lastmsgs[10][16384];
int iCurProtocol;
unsigned int iListboxCharWidth, iListboxCurrentWidth;
BOOL bCanExtended;
HINSTANCE hInst, hDLL;
HWND hListbox, hMainDlg;
unsigned long ulChannelID, pLogicalChannelId;
unsigned long ulDeviceID;
char chFuncLib[256];
unsigned int uiShowAllDevices;
HMENU hMenu;
bool bSupportPinSwitchProtocols;

int CheckAndConvertMsg(unsigned char *, unsigned char *);
J2534_PROTOCOL fnGetProtocol(char *strProtocol);
int fnGetProtocolIndex(long enProtocolID);
J2534IOCTLPARAMID fnGetIoctlParamId(char *strIoctlParamId);
int fnGetIoctlParamIdIndex(long enIoctlParamIdID); 
J2534GETDEVICEINFOPARAMID fnGetIoctlGetDeviceInfoParamId(char *strIoctlGetDeviceInfoParamId);
int fnGetIoctlGetDeviceInfoParamIdIndex(long enIoctlGetDeviceInfoParamIdID);
J2534GETPROTOCOLINFOPARAMID fnGetIoctlGetProtocolInfoParamId(char *strIoctlGetProtocolInfoParamId);
int fnGetIoctlGetProtocolInfoParamIdIndex(long enIoctlGetProtocolInfoParamIdID);

J2534ERROR TestVSI4PinSwitchSupported(bool *bSupportPinSwitchCANProtocols);
#ifdef J2534_0500
unsigned long ulDeviceCount = 0;
static SDEVICE j2534_device_list[25] = {0};
static unsigned long ulgetNextDevice = 0;
#endif

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
    // TODO: Place code here.
    hInst=hInstance;
    iCurProtocol=0;
    ulDeviceID = 0;
    hDLL = 0;
    bSupportPinSwitchProtocols = false;
    uiShowAllDevices = GetPrivateProfileInt("DeviceList", "ShowAllDevices", false, "J2534SDK.ini");
    
    long            lSuccess;
    char            chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH]; 
    DWORD           dwDatasize;     
    DWORD           dwDword, dwString; 
    HKEY            hMainkey;
    HKEY            hTempkey;
    HKEY            *phTempkey;
    int             i, iHighdevice, iHighvendor;
    bool            bDeviceExist;

    dwDword = REG_DWORD, dwString = REG_SZ;
    i=0, iHighdevice=1, iHighvendor=1;
    hTempkey = NULL;
    phTempkey = NULL;
    bDeviceExist = false;
	
	/*Block is required to check Windows OS > 10 and < 10*/
	strcpy_s(chTempstr1, J2534REGISTRY_KEY_PATH);
	lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr1, 0, KEY_ALL_ACCESS, &hMainkey);
	if (lSuccess != ERROR_SUCCESS)
	{
		strcpy_s(chTempstr1, J2534REGISTRY_KEY_PATH_6432);
		lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr1, 0, KEY_ALL_ACCESS, &hMainkey);
	}

    if(lSuccess==ERROR_SUCCESS) 
    {
        i=0;
        while (lSuccess==ERROR_SUCCESS) 
        {
            dwDatasize=J2534REGISTRY_MAX_STRING_LENGTH;
            lSuccess = RegEnumKey(hMainkey, i, chTempstr1, dwDatasize);
            if(lSuccess==ERROR_SUCCESS)
            {
                bDeviceExist = true;
            }
            i++;
        }
        RegCloseKey(hMainkey);
    }

    if (bDeviceExist == false)
    {
        MessageBox(NULL, 
            "There is currently no J2534 device installed.\r\nAt least one J2534 device must be installed for this SDK to function.",
            "Note", MB_OK);
        return 0;
    }
    DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, (DLGPROC)MainDlg);

    FreeLibrary(hDLL);
    return 0;
}

LRESULT CALLBACK MainDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{ 
    RECT rt, rt2;
    int selected[500], num, i;
    unsigned long totaldata;
    HGLOBAL globalmem;  
    char *ptr, filter[256];
    OPENFILENAME openfilename;
    HDC hdc;
    TEXTMETRIC lptm;
    
    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            hListbox=GetDlgItem(hDlg, IDC_OUTPUT);
            hMainDlg=hDlg;

            hdc=GetDC(hListbox);
            GetTextMetrics(hdc, &lptm);

            iListboxCharWidth=lptm.tmAveCharWidth;
            iListboxCurrentWidth=ListBox_GetHorizontalExtent(hListbox)/lptm.tmAveCharWidth;
            hMenu = GetMenu(hMainDlg);

#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500

			EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUWRITEMSGS, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
			
#else
			EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSCANFORDEVICES, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUWRITEMSGS, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_GRAYED);
            EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_GRAYED);
			EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
#endif
            return TRUE;
        case WM_COMMAND:
            if(HIWORD(wParam)==LBN_SELCHANGE)
            {
                num=ListBox_GetSelCount(hListbox);
                if(num==0)
                {
                    EnableWindow(GetDlgItem(hDlg, IDC_COPY), FALSE);
                }
                else
                {
                    EnableWindow(GetDlgItem(hDlg, IDC_COPY), TRUE);
                }
                return TRUE;
            }
            switch (LOWORD(wParam))
            {
                case IDM_RESET:
                    ListBox_ResetContent(hListbox);
                    break;
                case IDM_SAVE:
                    num=ListBox_GetCount(hListbox);
                    if(num==0)
                        break;
                    else
                    {
                        memset(&openfilename, 0, sizeof(OPENFILENAME));
                        tempbuf[0] = '\0';
                        strcpy_s(filter, "All files (*.*)");
                        strcpy_s(filter+strlen(filter)+1,sizeof(filter) - (strlen(filter)+1), "*.*");
                        filter[strlen(filter)+4]=0;
                        filter[strlen(filter)+5]=0;
                        openfilename.hInstance=hInst;
                        openfilename.lStructSize=sizeof(OPENFILENAME);
                        openfilename.hwndOwner=hDlg;
                        openfilename.lpstrFilter=filter;
                        openfilename.lpstrFile=/*NULL*/tempbuf;
                        openfilename.nMaxFile=sizeof(tempbuf);
                        openfilename.lpstrTitle=NULL;
                        openfilename.lpstrFileTitle=tempbuf;
                        openfilename.nMaxFileTitle=16384;
                        openfilename.lpstrInitialDir="C:\\";
                        openfilename.Flags=OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
                        if(GetSaveFileName(&openfilename))
                        {
                            FILE *TmpFile;
                            int err = fopen_s(&TmpFile, tempbuf, "wt");
                            if (err)
                            {
                                ::MessageBox(NULL,"temp SAVE file not created","Error",MB_OK);
                                break;
                            }
                            for(i=0; i<num; i++)
                            {
                                if(ListBox_GetText(hListbox, i, tempbuf))
                                {
                                    strcat_s(tempbuf, "\n");
                                    fputs (tempbuf, TmpFile); // Write the line
                                }
                            }
                            fclose ( TmpFile );

                        }
                    }
                    break;
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
				case PASSTHRUQUEUEMSGS:
					Do_PASSTHRQUEUEMSGS();
					break;
				case PASSTHRUSCANFORDEVICES:
					Do_PASSTHRSCANFORDEVICES();
					break;
				case PASSTHRUGETNEXTDEVICE:
					Do_PASSTHRUGETNEXTDEVICE();
					break;
				case PASSTHRULOGICALCONNECT:
					Do_PASSTHRULOGICALCONNECT();
					break;
				case PASSTHRULOGICALDISCONNECT:
					Do_PASSTHRULOGICALDISCONNECT();
					break;
				case PASSTHRUSELECT:
					Do_PASSTHRUSELECT();
					break;				
#endif
                case PASSTHRUOPEN:
                    Do_PASSTHRUOPEN();
                    break;  
                case PASSTHRUCLOSE:
                    Do_PASSTHRUCLOSE();
                    break;
                case PASSTHRUCONNECT:
                    Do_PASSTHRUCONNECT();
                    break;
                case PASSTHRUDISCONNECT: 
                    Do_PASSTHRUDISCONNECT();
                    break;
                case PASSTHRUREADMSGS: 
                    Do_PASSTHRUREADMSGS();
                    break;
                case PASSTHRUWRITEMSGS: 
                    Do_PASSTHRUWRITEMSGS();
                    break;
                case PASSTHRUSTARTPERIODICMSG: 
                    Do_PASSTHRUSTARTPERIODICMSG();
                    break;
                case PASSTHRUSTOPPERIODICMSG: 
                    Do_PASSTHRUSTOPPERIODICMSG();
                    break;
                case PASSTHRUSTARTMSGFILTER: 
                    Do_PASSTHRUSTARTMSGFILTER();
                    break;
                case PASSTHRUSTOPMSGFILTER: 
                    Do_PASSTHRUSTOPMSGFILTER();
                    break;
                case PASSTHRUSETPROGRAMMINGVOLTAGE: 
                    Do_PASSTHRUSETPROGRAMMINGVOLTAGE();
                    break;
                case PASSTHRUREADVERSION: 
                    Do_PASSTHRUREADVERSION();
                    break;
                case PASSTHRUGETLASTERROR: 
                    Do_PASSTHRUGETLASTERROR();
                    break;
                case PASSTHRUIOCTL: 
                    Do_PASSTHRUIOCTL();
                    break;
                case IDC_COPY:
                    ListBox_GetSelItems(hListbox, 500, selected);
                    totaldata=0;
                    num=ListBox_GetSelCount(hListbox);
                    for(i=0; i<num; i++)
                    {
                        totaldata+=ListBox_GetTextLen(hListbox, selected[i]);
                        totaldata+=2;
                    }
                    if(OpenClipboard(hDlg))
                    {
                        if(!EmptyClipboard())
                            MessageBox(hDlg, "Cannot empty clipboard", "Error:", MB_OK);

                        globalmem=GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, totaldata);
                        ptr=(char*)GlobalLock(globalmem);
                        totaldata=0;
                        for(i=0; i<num; i++)
                        {
                            ListBox_GetText(hListbox,  selected[i], ptr+totaldata);
                            strcat_s(ptr, totaldata - strlen(ptr), "\r\n");    
                            totaldata=strlen(ptr);
                        }
                        GlobalUnlock(globalmem);
                        if(!SetClipboardData(CF_TEXT, globalmem))
                        {
                            MessageBox(hDlg, "Cannot set clipboard data", "Error:", MB_OK);
                            DWORD err=GetLastError();
                        }
                        CloseClipboard();
                    }
                    else
                        MessageBox(hDlg, "Cannot open clipboard", "Error:", MB_OK);

                    ListBox_SetSel(hListbox, FALSE, -1);
                    EnableWindow(GetDlgItem(hDlg, IDC_COPY), FALSE);
                    break;
                case ID_ABOUT:
#ifdef J2534_0404
                    sprintf_s(tempbuf, "J2534 SDK Version 5.00.15");
#endif

#ifdef J2534_0500
					sprintf_s(tempbuf, "J2534 SDK Version 6.00.00");
#endif                 
					AddToScreen(tempbuf);
                    AddToScreen("");
                    sprintf_s(tempbuf, "Dearborn Group, Inc.");
                    AddToScreen(tempbuf);
                    sprintf_s(tempbuf, "33604 West Eight Mile");
                    AddToScreen(tempbuf);
                    sprintf_s(tempbuf, "Farmington Hills, MI 48335");
                    AddToScreen(tempbuf);
                    sprintf_s(tempbuf, "(248) 888-2000");
                    AddToScreen(tempbuf);
                    sprintf_s(tempbuf, "(248) 888-9977 FAX");
                    AddToScreen(tempbuf);
                    sprintf_s(tempbuf, "Web: www.dgtech.com");
                    AddToScreen(tempbuf);
                    AddToScreen("");
                    break;
                case IDCANCEL:
                case IDM_EXIT:
                    /*if (ulDeviceID > 0)
                    {
                        PassThruClose(ulDeviceID);
                    }*/    
                    if (hDLL)
                    {
                        FreeLibrary(hDLL);
                    }
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
}

void AddToScreen(char *msg)
{
    int height, numitems, numonscreen;
    RECT rt;
    if(strlen(msg)>iListboxCurrentWidth)
    {
        iListboxCurrentWidth=strlen(msg);
        ListBox_SetHorizontalExtent(hListbox, iListboxCurrentWidth*iListboxCharWidth);
    }
    ListBox_AddString(hListbox, msg);
    height=ListBox_GetItemHeight(hListbox, 0);
    GetClientRect(hListbox, &rt);
    numonscreen=(rt.bottom-rt.top)/height;
    numitems=ListBox_GetCount(hListbox);
    ListBox_SetTopIndex(hListbox, numitems-numonscreen);
}
/*J2534 - 1 V0500 Spec changes for below new APIs */
#ifdef J2534_0500
void Do_PASSTHRQUEUEMSGS()
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_WRITEMSGS), hMainDlg, (DLGPROC)QueueMsgsProc);
	return;
}
void Do_PASSTHRSCANFORDEVICES()
{
	int error=0;
	long            lSuccess;
	char            chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH];
	DWORD           dwDatasize;
	DWORD           dwDword, dwString;
	HKEY            hMainkey;
	HKEY            hTempkey;//, hTempkey2;         // handle for key
	int             i;

	dwDword = REG_DWORD, dwString = REG_SZ;
	hTempkey = NULL;
	strcpy_s(chTempstr1, J2534REGISTRY_KEY_PATH);
	lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr1,
		0, KEY_READ, &hMainkey);

	if (lSuccess == ERROR_SUCCESS)
	{
		i = 0;
		while (lSuccess == ERROR_SUCCESS)
		{
			dwDatasize = J2534REGISTRY_MAX_STRING_LENGTH;
			lSuccess = RegEnumKey(hMainkey, i, chTempstr1, dwDatasize);
			if (lSuccess == ERROR_SUCCESS)
			{
				if (LoadFunctions(chTempstr1) == FALSE)
				{
					MessageBox(NULL, "Couldn't load J2534 functions.  You may need (re)install the J2534 libraries.", "Error:", MB_OK);
					FreeLibrary(hDLL);
				}				
			}
			i++;
		}
		RegCloseKey(hMainkey);
		
	}
	AddToScreen("PassThruScanForDevices();");
	error = PassThruScanForDevices(&ulDeviceCount);
	if (error == 0)
	{
		EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_ENABLED);
	}
	sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
	AddToScreen(tempbuf);
	sprintf_s(tempbuf, "returned device count: %d", ulDeviceCount);
	AddToScreen(tempbuf);
	AddToScreen("");
}
void Do_PASSTHRUGETNEXTDEVICE()
{
	int error =0;
	AddToScreen("PassThruGetNextDevice();");
	error = PassThruGetNextDevice(j2534_device_list);
	if (error != 0)
		ulgetNextDevice = 0;

	if (error == 0)
	{
		sprintf_s(tempbuf, "Device name : %s", j2534_device_list[ulgetNextDevice].DeviceName);
		AddToScreen(tempbuf);
		switch (j2534_device_list[ulgetNextDevice].DeviceAvailable)
		{
		case 0:
			AddToScreen("Device Available : DEVICE_STATE_UNKNOWN");
			break;
		case 1:
			AddToScreen("Device Available : DEVICE_AVAILABLE");
			break;
		case 2:
			AddToScreen("Device Available : DEVICE_IN_USE");
			break;
		default:
			break;
		}	
		switch (j2534_device_list[ulgetNextDevice].DeviceDLLFWStatus)
		{
		case 0:
			AddToScreen("Device DLL FW Status : DEVICE_DLL_FW_COMPATIBILTY_UNKNOWN");
			break;
		case 1:
			AddToScreen("Device DLL FW Status : DEVICE_DLL_FW_COMPATIBLE");
			break;
		case 2:
			AddToScreen("Device DLL FW Status : DEVICE_DLL_OR_FW_NOT_COMPATIBLE");
			break;
		case 3:
			AddToScreen("Device DLL FW Status : DEVICE_DLL_NOT_COMPATIBLE");
			break;
		case 4:
			AddToScreen("Device DLL FW Status : DEVICE_FW_NOT_COMPATIBLE");
			break;
		default:
			break;
		}
		switch (j2534_device_list[ulgetNextDevice].DeviceConnectMedia)
		{
		case 0:
			AddToScreen("Device Connect Media : DEVICE_CONN_UNKNOWN");
			break;
		case 1:
			AddToScreen("Device Connect Media : DEVICE_CONN_WIRELESS");
			break;
		case 2:
			AddToScreen("Device Connect Media : DEVICE_CONN_WIRED");
			break;
		default:
			break;
		}
		sprintf_s(tempbuf, "Device Connect Speed : %d", j2534_device_list[ulgetNextDevice].DeviceConnectSpeed);
		AddToScreen(tempbuf);
		sprintf_s(tempbuf, "Device Signal Quality : %d", j2534_device_list[ulgetNextDevice].DeviceSignalQuality);
		AddToScreen(tempbuf);
		sprintf_s(tempbuf, "Device Signal Strength : %d", j2534_device_list[ulgetNextDevice].DeviceSignalStrength);
		AddToScreen(tempbuf);
		ulgetNextDevice++;
	}

	sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
	AddToScreen(tempbuf);
	AddToScreen("");
}
void Do_PASSTHRULOGICALCONNECT()
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_CONNECT), hMainDlg, (DLGPROC)LogicalConnectProc);
	return;

}
void Do_PASSTHRULOGICALDISCONNECT()
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_LOGICALDISCONNECT), hMainDlg, (DLGPROC)LogicalDisconnectProc);
	return;
}
void Do_PASSTHRUSELECT()
{	
	DialogBox(hInst, MAKEINTRESOURCE(IDD_SELECT), hMainDlg, (DLGPROC)PassThruSelectProc);
	return;
}
#endif
void Do_PASSTHRUOPEN()
{
   DialogBox(hInst, MAKEINTRESOURCE(IDD_OPEN), hMainDlg, (DLGPROC)OpenProc);
   return;
}
void Do_PASSTHRUCLOSE()
{
    int error = -1; 
	AddToScreen("PassThruClose();"); 
	error = PassThruClose(ulDeviceID);	
	if (error == 0)
    {
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
		EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSCANFORDEVICES, MF_ENABLED);
		EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_ENABLED);
		EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUOPEN, MF_ENABLED);
		EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUWRITEMSGS, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
#else
		EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSCANFORDEVICES, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUOPEN, MF_ENABLED);
        EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUWRITEMSGS, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_GRAYED);
        EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_GRAYED);
		EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);
#endif		
    }
	sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
	AddToScreen(tempbuf);
	AddToScreen("");
    return;
}
void Do_PASSTHRUCONNECT()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_CONNECT), hMainDlg, (DLGPROC)ConnectProc);
    return;
}
void Do_PASSTHRUDISCONNECT()
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_LOGICALDISCONNECT), hMainDlg, (DLGPROC)DisconnectProc);
	return;
}
void Do_PASSTHRUREADMSGS()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_READMSGS), hMainDlg, (DLGPROC)ReadMsgsProc);
    return;
}

void Do_PASSTHRUWRITEMSGS()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_WRITEMSGS), hMainDlg, (DLGPROC)WriteMsgsProc);
    return;
}

void Do_PASSTHRUSTARTPERIODICMSG()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_STARTPERIODIC), hMainDlg, (DLGPROC)StartPeriodicProc);
    return;
}

void Do_PASSTHRUSTOPPERIODICMSG()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_STOPPERIODIC), hMainDlg, (DLGPROC)StopPeriodicProc);
    return;
}

void Do_PASSTHRUSTARTMSGFILTER()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_STARTFILTER), hMainDlg, (DLGPROC)StartFilterProc);
    return;
}

void Do_PASSTHRUSTOPMSGFILTER()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_STOPFILTER), hMainDlg, (DLGPROC)StopFilterProc);
    return;
}

void Do_PASSTHRUSETPROGRAMMINGVOLTAGE()
{
    DialogBox(hInst, MAKEINTRESOURCE(IDD_SETPROGRAMMINGVOLTAGE), hMainDlg, (DLGPROC)SetProgrammingVoltageProc);
    return;
}

void Do_PASSTHRUREADVERSION()
{
    char firmware[MAX_STRING_LENGTH], dll[MAX_STRING_LENGTH], api[MAX_STRING_LENGTH];
    int error;

    error=PassThruReadVersion(ulDeviceID, firmware, dll, api);
    AddToScreen("PassThruReadVersion(ucFirmware, ucDll, ucApi);");
    if(error==STATUS_NOERROR)
    {
        AddToScreen(tempbuf);
        sprintf_s(tempbuf, "Firmware Version: %s", firmware);
        AddToScreen(tempbuf);
        sprintf_s(tempbuf, "DLL Version: %s", dll);
        AddToScreen(tempbuf);
        sprintf_s(tempbuf, "API Version: %s", api);
        AddToScreen(tempbuf);
    }
    sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
    AddToScreen(tempbuf);
    AddToScreen("");
    return;
}

void Do_PASSTHRUGETLASTERROR()
{
    char errorstr[MAX_STRING_LENGTH];
    int error;

    error=PassThruGetLastError(errorstr);
    AddToScreen("PassThruGetLastError(errorstr);");
    if(error==STATUS_NOERROR)
    {
        sprintf_s(tempbuf, "Last Error: %s", errorstr);
        AddToScreen(tempbuf);
    }
    sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
    AddToScreen(tempbuf);
    AddToScreen("");
    return;
}

void Do_PASSTHRUIOCTL()
{
#ifndef J2534_0500
    DialogBox(hInst, MAKEINTRESOURCE(IDD_IOCTL), hMainDlg, (DLGPROC)IoctlProc);
#endif
#ifdef J2534_0500
	DialogBox(hInst, MAKEINTRESOURCE(IDD_IOCTL), hMainDlg, (DLGPROC)Ioctl0500Proc);
#endif
    return;
}

bool SupportedDevice(char *Device)
{
   bool rc = false;
   for (int i = 0; i < DEVICE_ENTRY_COUNT; i++)
   {
      if (strcmp(Device, Devices[i].RegistryDescription) == 0)
      {
         rc = TRUE;
         break;
      }
   }
   return rc;
}

bool SupportedPinSwitchDevice(char *Device)
{
    // Here we test for:
    //  - Is this a Pin Switched device?
    //      - Yes:
    //          - Is this a VSI?
    //              - Yes
    //                  - Is this OLD device?
    //                      - Yes, set return to FALSE
    //                  - Is this NEW device?
    //                      - Yes, set return to TRUE
    //              - No, set return to TRUE
    //          - No, set return to TRUE
    //      - No, set return to FALSE
    //  - return condition
    //
    bool rc = false;
    for (int i = 0; i < DEVICE_ENTRY_COUNT; i++)
    {
        if (strcmp(Device, Devices[i].RegistryDescription) == 0)
        {
            // Found our device
            if (Devices[i].bCAN_PinSwitchSupported)
            {
                // Yes! Pin-switch supported, as far as we know
                UpgradedVersion = Devices[i].UpgradedVersion;
                if (Devices[i].bTestVSI4Version)
                {   // This is a VSI, old version doesn't support pin-switch, test version of board
                    bool bSupportPinSwitchCANProtocols;
                    if (TestVSI4PinSwitchSupported(&bSupportPinSwitchCANProtocols) == STATUS_NOERROR)
                    {
                        rc = bSupportPinSwitchCANProtocols;
                    }
                    else
                    {
                        rc = false;
                    }
                }
                else
                {
                    rc = true;
                }
            }
            else
                rc = false;

            break;
        }
    }
   return rc;
}

bool SupportedCANProtocol(J2534_PROTOCOL J2534ProtocolID)
{
   bool rc = false;
   for (int i = 0; i < CAN_PROTOCOL_ENTRY_COUNT; i++)
   {
      if (J2534ProtocolID == CANProtocols[i].J2534ProtocolID)
      {
         rc = TRUE;
         break;
      }
   }
   return rc;
}
#ifdef J2534_0500
LRESULT CALLBACK LogicalConnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hCombo, hControl;
	RECT rt, rt2;
	int nProtocol, flags, error;
	char chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH];
	J2534_PROTOCOL Protocol;
	RESOURCE_STRUCT resourceStruct;
	ISO15765_CHANNEL_DESCRIPTOR channelDesck;
	memset(&channelDesck, 0, sizeof(ISO15765_CHANNEL_DESCRIPTOR));
	unsigned long size;
	int iChannelId = -1;
	BOOL retval;
	flags = 0;

	switch (message)
	{
	case WM_INITDIALOG: 
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		SetWindowText(hDlg, "Logical Connect");
		hCombo = GetDlgItem(hDlg, IDC_COMBO1);
		for (int iProtocolIndex = 0; iProtocolIndex < PROTOCOL_ENTRY_COUNT; iProtocolIndex++)
		{
			if (Protocols[iProtocolIndex].VerifyPinSwitchSupport == false || bSupportPinSwitchProtocols == true)
			{
				if(strcmp(Protocols[iProtocolIndex].Description, "ISO15765_LOGICAL") == 0)
					ComboBox_AddString(hCombo, "ISO15765_LOGICAL");
			}
		}
		hControl = GetDlgItem(hDlg, IDC_EDIT_LOCAL_ID);
		EnableWindow(hControl, TRUE);
		hControl = GetDlgItem(hDlg, IDC_EDIT_REMOTE_ID);
		EnableWindow(hControl, TRUE);
		hControl = GetDlgItem(hDlg, IDC_FLAGS);
		EnableWindow(hControl, FALSE);
		//SetDlgItemText(hDlg, IDC_FLAGS, "0");
		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == CBN_SELCHANGE)
		{
			// Get the selected item from the list
			if ((nProtocol = ComboBox_GetCurSel(hCombo)) == -1)
			{
				MessageBox(hDlg, "You must first select a device", "Error:", MB_OK);
				return FALSE;
			}
			ComboBox_GetLBText(hCombo, nProtocol, chTempstr1);
			Protocol = fnGetProtocol(chTempstr1);
		}
		else
		{
			switch (LOWORD(wParam))
			{
			case IDC_CONNECT:
				bCanExtended = FALSE;
				if ((nProtocol = ComboBox_GetCurSel(hCombo)) == -1)
				{
					MessageBox(hDlg, "You must first select a protocol", "Error:", MB_OK);
				}
				else
				{
					ComboBox_GetLBText(hCombo, nProtocol, chTempstr1);
					Protocol = fnGetProtocol(chTempstr1);
					sprintf_s(tempbuf, "PassThruConnect()");
					if (GetDlgItemText(hDlg, IDC_FLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
						sscanf_s(tempbuf, "%X", &flags);

					int iProtocolIndex = fnGetProtocolIndex(Protocol);

					if (Protocol == ISO15765_LOGICAL)
					{
						if (!GetDlgItemText(hDlg, IDC_EDIT_LOCAL_ID, (char*)tempmsg, 16384))
							break;

						if (!GetDlgItemText(hDlg, IDC_EDIT_REMOTE_ID, (char*)tempmsg2, 16384))
							break;

						size = CheckAndConvertMsg((unsigned char *)tempmsg, tempstr);
						memcpy(channelDesck.LocalAddress, tempstr, size);

						size = CheckAndConvertMsg((unsigned char *)tempmsg2, tempstr);
						memcpy(channelDesck.RemoteAddress, tempstr, size);

						if (IsDlgButtonChecked(hDlg, IDC_RADIO1))
						{
							flags &= ~(0x100);
							bCanExtended = FALSE;
							sprintf_s(tempbuf, "PassThruLogicalConnect(%s, 0, &ulChannelID);", Protocols[iProtocolIndex].Description);
							AddToScreen(tempbuf);
						}
						else if (IsDlgButtonChecked(hDlg, IDC_RADIO2))
						{
							flags |= 0x100;
							bCanExtended = TRUE;
							sprintf_s(tempbuf, "PassThruLogicalConnect(%s, 0x100, &ulChannelID);", Protocols[iProtocolIndex].Description);
							AddToScreen(tempbuf);
						}
						iChannelId = GetDlgItemInt(hDlg, IDC_EDIT_PHY_ID, &retval, FALSE);
						if (!retval)
							break;
						error = PassThruLogicalConnect(iChannelId, ISO15765_LOGICAL, flags, &channelDesck, &pLogicalChannelId);
						sprintf_s(tempbuf, "PassThruLogicalConnect(%s, %X, %d);", Protocols[iProtocolIndex].Description, flags, pLogicalChannelId);
						AddToScreen(tempbuf);
						sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
						AddToScreen(tempbuf);
						AddToScreen("");
					}
					iCurProtocol = Protocol;
					EndDialog(hDlg, LOWORD(wParam));
				}
				break;
			case IDCANCEL:
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
				break;
			}
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK LogicalDisconnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt2;
	BOOL retval;
	int iMsgNum, error;

	switch (message)
	{
	case WM_INITDIALOG:
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_STOP:
			iMsgNum = GetDlgItemInt(hDlg, IDC_NUM, &retval, FALSE);
			if (!retval)
				break;

			sprintf_s(tempbuf, "PassThruLogicalDisconnect(ChannelID, %d);", iMsgNum);
			AddToScreen(tempbuf);
			AddToScreen("");
			error = PassThruLogicalDisconnect(iMsgNum);
			sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
			AddToScreen(tempbuf);
			AddToScreen("");

		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
		return TRUE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK QueueMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	PASSTHRU_MSG *ptrMsg;
	RECT rt, rt2;
	int i, size, error;
	static HWND hMsgList;
	unsigned long ulNumMsgs, iMsgNum,iMsgHandle;
	unsigned long ulTxFlags;
	unsigned long ulExtraDataIndex;
	BOOL test, retval;

	switch (message)
	{
	case WM_INITDIALOG:
		iMsgHandle = 0;
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		SetWindowText(hDlg, "Queue Message");
		SetWindowText(GetDlgItem(hDlg, IDC_WRITEMSGS), "Queue Msg");
		for (i = 0; i<10; i++)
		{
			ComboBox_AddString(GetDlgItem(hDlg, IDC_MSG), (char*)lastmsgs[i]);
		}
		hMsgList = GetDlgItem(hDlg, IDC_MSGLIST);
		sprintf_s(tempbuf, "%d", iCurProtocol);
		SetDlgItemText(hDlg, IDC_PROTOCOLID, tempbuf);
		SetDlgItemText(hDlg, IDC_TXFLAGS, "0");
		if (bCanExtended)
			SetDlgItemText(hDlg, IDC_TXFLAGS, "100");
		SetDlgItemText(hDlg, IDC_EXTRADATAINDEX, "0");
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_ADD:
			if (GetDlgItemText(hDlg, IDC_MSG, (char*)tempstr, 16384))
			{
				SetDlgItemText(hDlg, IDC_MSG, NULL);
				if (CheckAndConvertMsg(tempstr, tempmsg) != -1)
				{
					if (tempstr[strlen((char*)tempstr) - 1] == ' ')
						tempstr[strlen((char*)tempstr) - 1] = 0;

					WritePrivateProfileString("Previous Msgs", NULL, NULL, path);
					for (i = 0; i<10; i++)
					{
						if (!strcmp((char*)tempstr, lastmsgs[i]))
							break;
					}
					if (i == 10)
					{
						for (i = 9; i>0; i--)
						{
							memset(lastmsgs[i], 0, 16384);
							strcpy_s((char*)lastmsgs[i], sizeof(lastmsgs[0]), (char*)lastmsgs[i - 1]);
						}

						memset(lastmsgs[0], 0, 16384);
						strcpy_s((char*)lastmsgs[0], sizeof(lastmsgs[0]), (char*)tempstr);
						ComboBox_InsertString(GetDlgItem(hDlg, IDC_MSG), 0, (char*)lastmsgs[0]);
						ComboBox_DeleteString(GetDlgItem(hDlg, IDC_MSG), 10);
					}
					for (i = 0; i<10; i++)
					{
						sprintf_s(tempbuf, "Msg%d", i);
						WritePrivateProfileString("Previous Msgs", (char*)tempbuf, (char*)lastmsgs[i], path);
					}
					ListBox_AddString(hMsgList, tempstr);
					EnableWindow(GetDlgItem(hDlg, IDC_WRITEMSGS), TRUE);
				}
			}
			break;
		case IDC_REMOVE:
			if ((i = ListBox_GetCurSel(hMsgList)) != LB_ERR)
			{
				ListBox_DeleteString(hMsgList, i);
				if (ListBox_GetCount(hMsgList) == 0)
					EnableWindow(GetDlgItem(hDlg, IDC_WRITEMSGS), FALSE);
			}
			break;
		case IDC_WRITEMSGS:
			ulTxFlags = GetDlgItemInt(hDlg, IDC_TXFLAGS, &test, FALSE);
			if (GetDlgItemText(hDlg, IDC_TXFLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
				sscanf_s(tempbuf, "%X", &ulTxFlags);
			
			iMsgNum = GetDlgItemInt(hDlg, IDC_EDTCHANNEL, &retval, FALSE);
			if (!retval)
				break;
			iMsgHandle = GetDlgItemInt(hDlg, IDC_EDTCHANNEL_MSGHDL, &retval, FALSE);
			ulExtraDataIndex = GetDlgItemInt(hDlg, IDC_EXTRADATAINDEX, &test, FALSE);
			
			if ((ulNumMsgs = ListBox_GetCount(hMsgList)) != 0)
			{
				ptrMsg = new PASSTHRU_MSG[ulNumMsgs];

				for (unsigned int j = 0; j<ulNumMsgs; j++)
				{
					ListBox_GetText(hMsgList, j, tempstr);
					memset(ptrMsg + j, 0, sizeof(PASSTHRU_MSG));
					size = CheckAndConvertMsg(tempstr, tempmsg);
					ptrMsg[j].Data = new unsigned char[size];
					memset(ptrMsg[j].Data, 0, size);
					//memcpy((unsigned char*)&ptrMsg[j].Data[0], tempmsg, size);
					memcpy(ptrMsg[j].Data, tempmsg, size);
					ptrMsg[j].DataSize = size;
					ptrMsg[j].ProtocolID = GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);
					if (bCanExtended)
						ptrMsg[j].TxFlags = 0x100;
					else
						ptrMsg[j].TxFlags = 0x00;

					ptrMsg[j].ExtraDataIndex = ulExtraDataIndex;
					ptrMsg[j].MsgHandle = iMsgHandle;
					ptrMsg[j].DataBufferSize = 0;
					ptrMsg[j].RxStatus = 0;	
				}

				AddToScreen("ptrMsg=new PASSTHRU_MSG[ulNumMsgs];");
				AddToScreen("for(unsigned int count=0; count<ulNumMsgs; count++)");
				AddToScreen("{");
				AddToScreen("   memset(ptrMsg+count, 0, sizeof(PASSTHRU_MSG));");
				AddToScreen("");
				AddToScreen("   memcpy(ptrMsg[count].Data, ptrTempMsg, size);");
				AddToScreen("   ptrMsg[count].DataSize=ulSize;");
				AddToScreen("   ptrMsg[count].ExtraDataIndex=0;");
				AddToScreen("   ptrMsg[count].ProtocolID=iCurProtocol;");
				if (bCanExtended)
					AddToScreen("   ptrMsg[count].TxFlags=0x100;");
				else
					AddToScreen("   ptrMsg[count].TxFlags=0x00;");
				AddToScreen("}");
				AddToScreen("PassThruQueueMsgs(ulChannelID, ptrMsg, &ulNumMsgs);");
				AddToScreen("delete [] ptrMsg;");
				AddToScreen("");
				error = PassThruQueueMsgs(iMsgNum, ptrMsg, &ulNumMsgs);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
				//delete[] ptrMsg;
				EndDialog(hDlg, LOWORD(wParam));
			}
			break;
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
		return TRUE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK PassThruSelectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt2;
	BOOL retval;
	unsigned long iMsgNum, error;
	SCHANNELSET ChannelSetPtr;

	switch (message)
	{
	case WM_INITDIALOG:
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_STOP:
			iMsgNum = GetDlgItemInt(hDlg, IDC_SEL_CHANNELS, &retval, FALSE);
			if (!retval)
				break;
			sprintf_s(tempbuf, "PassThruSelect(ChannelID, %d);", iMsgNum);
			AddToScreen(tempbuf);
			memset(&ChannelSetPtr, 0, sizeof(SCHANNELSET));
			unsigned long ulChanList[1];
			ulChanList[0] = iMsgNum;
			//ChannelSetPtr.ChannelList = new unsigned long[1];
			ChannelSetPtr.ChannelList = &ulChanList[0];
			ChannelSetPtr.ChannelCount = 1;
			ChannelSetPtr.ChannelThreshold = 0;
			error = PassThruSelect(&ChannelSetPtr,READABLE_TYPE, 1000);
			if (error == STATUS_NOERROR)
			{
				sprintf_s(tempbuf, "No of messages available on the channel Id: %d", ChannelSetPtr.ChannelThreshold);
				AddToScreen(tempbuf);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
			}
			else
			{
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
			}
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
		return TRUE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}
#endif
LRESULT CALLBACK OpenProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{ 
    static HWND hCombo, hControl;
    RECT rt, rt2;
    int nProtocol;//, flags, error;
    long            lSuccess;
    char            chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH]; 
    DWORD           dwDatasize;     
    DWORD           dwDword, dwString; 
    HKEY            hMainkey;
    HKEY            hTempkey;//, hTempkey2;         // handle for key
    HKEY            *phTempkey;
    int             i, iHighdevice, iHighvendor;

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

            hCombo=GetDlgItem(hDlg, IDC_COMBO_DEVICE);

            dwDword = REG_DWORD, dwString = REG_SZ;
            i=0, iHighdevice=1, iHighvendor=1;
            hTempkey = NULL;
            phTempkey = NULL;

            strcpy_s(chTempstr1, J2534REGISTRY_KEY_PATH);
            lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr1, 
                    0, KEY_READ, &hMainkey);

            if(lSuccess==ERROR_SUCCESS) 
            {
                i=0;
                while (lSuccess==ERROR_SUCCESS) 
                {
                    dwDatasize=J2534REGISTRY_MAX_STRING_LENGTH;
                    lSuccess = RegEnumKey(hMainkey, i, chTempstr1, dwDatasize);
                    if(lSuccess==ERROR_SUCCESS)
                    {
                        // Insert the device found in the registry to the list
                        if (uiShowAllDevices != 0 || SupportedDevice(chTempstr1))
                        {
                           ComboBox_AddString(hCombo, chTempstr1);
                        }
                    }
                    i++;
                }
                RegCloseKey(hMainkey);
            }
            return TRUE;
        case WM_COMMAND:
            {
                switch (LOWORD(wParam))
                {
                    case IDC_OPEN:
                        if((nProtocol=ComboBox_GetCurSel(hCombo))==-1)
                        {
                            MessageBox(hDlg, "You must first select a device", "Error:", MB_OK);
                        }
                        else
                        {
                            int error; 
                            // Get the selected item from the list
                            ComboBox_GetLBText(hCombo, nProtocol, chTempstr1);

                            if(LoadFunctions(chTempstr1)==FALSE)
                            {
                                MessageBox(NULL, "Couldn't load J2534 functions.  You may need (re)install the J2534 libraries.", "Error:", MB_OK);
                                FreeLibrary(hDLL);
                                return (FALSE);
                            }
							AddToScreen("PassThruOpen();"); 
							error=PassThruOpen(NULL, &ulDeviceID);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            EndDialog(hDlg, LOWORD(wParam));

                           // if (error == 0)
                            {
                                if (SupportedPinSwitchDevice(chTempstr1))
                                {
                                    // Set flag so that we add CAN_PS, ISO15765_PS, CAN_CH1, CAN_CH2, ISO15765_CH1, and ISO15765_CH2
                                    bSupportPinSwitchProtocols = true;
                                }
#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check6 specific functionality of V0500
								EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSCANFORDEVICES, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUOPEN, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSELECT, MF_ENABLED);
#else
								EnableMenuItem(hMenu, PASSTHRUQUEUEMSGS, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRUSCANFORDEVICES, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRUGETNEXTDEVICE, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRULOGICALCONNECT, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRULOGICALDISCONNECT, MF_GRAYED);
								EnableMenuItem(hMenu, PASSTHRUOPEN, MF_GRAYED);
                                EnableMenuItem(hMenu, PASSTHRUCLOSE, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUCONNECT, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUDISCONNECT, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUREADMSGS, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUWRITEMSGS, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUSTARTPERIODICMSG, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUSTOPPERIODICMSG, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUSTARTMSGFILTER, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUSTOPMSGFILTER, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUSETPROGRAMMINGVOLTAGE, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUREADVERSION, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUGETLASTERROR, MF_ENABLED);
                                EnableMenuItem(hMenu, PASSTHRUIOCTL, MF_ENABLED);
								EnableMenuItem(hMenu, PASSTHRUSELECT, MF_GRAYED);

#endif
                            }
                        }
                        break;
                    case IDCANCEL:
                        EndDialog(hDlg, LOWORD(wParam));
                        return TRUE;
                    break;
                }
            }
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK ConnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{ 
    static HWND hCombo, hControl;
    RECT rt, rt2;
    int nProtocol, flags, error;
    char chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH]; 
    J2534_PROTOCOL Protocol;
#ifdef J2534_0500
	RESOURCE_STRUCT resourceStruct;
	unsigned long ulResource[2];
#endif 
    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

            hCombo=GetDlgItem(hDlg, IDC_COMBO1);
            for (int iProtocolIndex = 0; iProtocolIndex < PROTOCOL_ENTRY_COUNT; iProtocolIndex++)
            {   
                if (Protocols[iProtocolIndex].VerifyPinSwitchSupport == false || bSupportPinSwitchProtocols == true)
                {
#ifndef J2534_0500
					ComboBox_AddString(hCombo, Protocols[iProtocolIndex].Description);
#endif
#ifdef J2534_0500
					if ((strcmp(Protocols[iProtocolIndex].Description, "CAN") == 0) || 
					    (strcmp(Protocols[iProtocolIndex].Description, "CAN_CH1") == 0) ||
						(strcmp(Protocols[iProtocolIndex].Description, "CAN_CH2") == 0) ||
						(strcmp(Protocols[iProtocolIndex].Description, "CAN_CH3") == 0) ||
						(strcmp(Protocols[iProtocolIndex].Description, "CAN_CH4") == 0) ||
						(strcmp(Protocols[iProtocolIndex].Description, "J1850VPW") == 0) || 
						(strcmp(Protocols[iProtocolIndex].Description, "J1850PWM") == 0) ||
						(strcmp(Protocols[iProtocolIndex].Description, "ISO9141") == 0) || 
						(strcmp(Protocols[iProtocolIndex].Description, "ISO14230") == 0))
						
					ComboBox_AddString(hCombo, Protocols[iProtocolIndex].Description);
#endif
                }
            } 
			hControl = GetDlgItem(hDlg, IDC_EDIT_LOCAL_ID);
			EnableWindow(hControl, FALSE);
			hControl = GetDlgItem(hDlg, IDC_EDIT_REMOTE_ID);
			EnableWindow(hControl, FALSE);
			hControl = GetDlgItem(hDlg, IDC_EDIT_PHY_ID);
			EnableWindow(hControl, FALSE);
            SetDlgItemText(hDlg, IDC_FLAGS, "0");
            return TRUE;
        case WM_COMMAND:
            if(HIWORD(wParam)==CBN_SELCHANGE)
            {
                // Get the selected item from the list
                if((nProtocol=ComboBox_GetCurSel(hCombo))==-1)
                {
                    MessageBox(hDlg, "You must first select a device", "Error:", MB_OK);
                    return FALSE;
                }
                ComboBox_GetLBText(hCombo, nProtocol, chTempstr1);
                Protocol = fnGetProtocol(chTempstr1);
                //if(ComboBox_GetCurSel(hCombo)==4 || ComboBox_GetCurSel(hCombo)==5)
                if (SupportedCANProtocol(Protocol))
                {
                    hControl=GetDlgItem(hDlg, IDC_CANGROUP);
                    EnableWindow(hControl, TRUE);
                    hControl=GetDlgItem(hDlg, IDC_RADIO1);
                    EnableWindow(hControl, TRUE);
                    hControl=GetDlgItem(hDlg, IDC_RADIO2);
                    EnableWindow(hControl, TRUE);
                    if(IsDlgButtonChecked(hDlg, IDC_RADIO1)==0 &&
                       IsDlgButtonChecked(hDlg, IDC_RADIO2)==0)
                       CheckRadioButton(hDlg, IDC_RADIO1, IDC_RADIO2, IDC_RADIO1);
                }
                else
                {
                    hControl=GetDlgItem(hDlg, IDC_CANGROUP);
                    EnableWindow(hControl, FALSE);
                    hControl=GetDlgItem(hDlg, IDC_RADIO1);
                    EnableWindow(hControl, FALSE);
                    hControl=GetDlgItem(hDlg, IDC_RADIO2);
                    EnableWindow(hControl, FALSE);
                }
            }
            else
            {
                switch (LOWORD(wParam))
                {
                    case IDC_CONNECT:
                        bCanExtended=FALSE;
                        if((nProtocol=ComboBox_GetCurSel(hCombo))==-1)
                        {
                            MessageBox(hDlg, "You must first select a protocol", "Error:", MB_OK);
                        }
                        else
                        {
                            ComboBox_GetLBText(hCombo, nProtocol, chTempstr1);
                            Protocol = fnGetProtocol(chTempstr1);
                            sprintf_s(tempbuf, "PassThruConnect()");
                            if(GetDlgItemText(hDlg, IDC_FLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
                                sscanf_s(tempbuf, "%X", &flags);

                            int iProtocolIndex = fnGetProtocolIndex(Protocol);

                            if(Protocol == CAN || Protocol == ISO15765 || Protocol == CAN_PS || Protocol == ISO15765_PS ||
                               Protocol == FT_CAN_CH1 || Protocol == FT_ISO15765_CH1 || Protocol == FT_CAN_PS || Protocol == FT_ISO15765_PS ||
                                Protocol == CAN_CH1 || Protocol == ISO15765_CH1 ||
                                Protocol == CAN_CH2 || Protocol == ISO15765_CH2 ||
                                Protocol == CAN_CH3 || Protocol == ISO15765_CH3 ||
                                Protocol == CAN_CH4 || Protocol == ISO15765_CH4 || Protocol == CAN_FD || 
                                Protocol == SW_CAN_PS || Protocol == SW_ISO15765_PS || 
                                Protocol == SW_CAN_CAN_CH1 || Protocol == SW_CAN_ISO15765_CH1)
                            {
                                if(IsDlgButtonChecked(hDlg, IDC_RADIO1))
                                {
                                    flags&=~(0x100);
                                    bCanExtended=FALSE;
                                    sprintf_s(tempbuf, "PassThruConnect(%s, 0, &ulChannelID);", Protocols[iProtocolIndex].Description);
                                    AddToScreen(tempbuf);
                                }
                                else if(IsDlgButtonChecked(hDlg, IDC_RADIO2))
                                {
                                    flags|=0x100;
                                    bCanExtended=TRUE;
                                    sprintf_s(tempbuf, "PassThruConnect(%s, 0x100, &ulChannelID);", Protocols[iProtocolIndex].Description);
                                    AddToScreen(tempbuf);
                                }
                                long BaudRate = 500000; // default
                                if(Protocol == SW_CAN_PS || Protocol == SW_ISO15765_PS || 
                                    Protocol == SW_CAN_CAN_CH1 || Protocol == SW_CAN_ISO15765_CH1)
                                    BaudRate = SWCAN_DATA_RATE_DEFAULT;
                                if(Protocol == FT_CAN_CH1 || Protocol == FT_ISO15765_CH1 ||
                                    Protocol == FT_CAN_PS || Protocol == FT_ISO15765_PS)
                                    BaudRate = 125000;

#if defined (J2534_0404) || defined (J2534_0305)
						        error = PassThruConnect(ulDeviceID, Protocol, flags, BaudRate, &ulChannelID);
#endif
#ifdef J2534_0500
							   memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
						       resourceStruct.Connector = J1962_CONNECTOR;
						       resourceStruct.NumOfResources = 2;
							   ulResource[0] = 6;
							   ulResource[1] = 14;
							   resourceStruct.ResourceListPtr = ulResource;
						       error = PassThruConnect(ulDeviceID, Protocol, flags, BaudRate, resourceStruct,&ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if(Protocol == J1939 || Protocol == J1939_CH1 ||
                                Protocol == J1939_CH2 || Protocol == J1939_CH3 || 
                                Protocol == J1939_CH4)
                            {
                                sprintf_s(tempbuf, "PassThruConnect(%s, %X, &ulChannelID);", Protocols[iProtocolIndex].Description, flags);
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 250000, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 250000, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if(Protocol==ISO9141 || Protocol==ISO14230 || Protocol==UART_ECHO_BYTE_PS)
                            {
                                sprintf_s(tempbuf, "PassThruConnect(%s, %X, &ulChannelID);", Protocols[iProtocolIndex].Description, flags);
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 10400, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								ulResource[0] = 6;
								ulResource[1] = 14;
								resourceStruct.ResourceListPtr = ulResource;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 10400, resourceStruct, &ulChannelID);
#endif
								sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if(Protocol==J1850VPW)
                            {
                                sprintf_s(tempbuf, "PassThruConnect(%s, %X, &ulChannelID);", Protocols[iProtocolIndex].Description, flags);
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 10416, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								ulResource[0] = 6;
								ulResource[1] = 14;
								resourceStruct.ResourceListPtr = ulResource;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 10416, resourceStruct, &ulChannelID);
#endif							
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if(Protocol==J1850PWM)
                            {
                                sprintf_s(tempbuf, "PassThruConnect(%s, %X, &ulChannelID);", Protocols[iProtocolIndex].Description, flags);
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 41666, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								ulResource[0] = 6;
								ulResource[1] = 14;
								resourceStruct.ResourceListPtr = ulResource;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 41666, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if(Protocol==SCI_A_ENGINE || Protocol==SCI_A_TRANS ||
                                Protocol==SCI_B_ENGINE || Protocol==SCI_B_TRANS)
                            {
                                sprintf_s(tempbuf, "PassThruConnect(%s, %X, &ulChannelID);", Protocols[iProtocolIndex].Description, flags);
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 7812, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								ulResource[0] = 6;
								ulResource[1] = 14;
								resourceStruct.ResourceListPtr = ulResource;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 7812, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if (Protocol == SCI_B_TRANS+1)
                            {
                                //nProtocol = 0x8007;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 33333, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 33333, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if (Protocol == SCI_B_TRANS+2)
                            {
                                //nProtocol = 0x8008;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 33333, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 33333, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if (Protocol == SCI_B_TRANS+3)
                            {
                                //nProtocol = 0x8009;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 8192, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 8192, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }     
                            else if (Protocol == J1708)
                            {
                                //nProtocol = 0x8009;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 9600, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 9600, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if (Protocol == GM_UART)
                            {
                                //nProtocol = 0x8009;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 8192, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 8192, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            else if (Protocol == LIN)
                            {
                                //nProtocol = 0x8009;
                                AddToScreen(tempbuf);
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruConnect(ulDeviceID, Protocol, flags, 19231, &ulChannelID);
#endif
#ifdef J2534_0500
								memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
								resourceStruct.Connector = J1962_CONNECTOR;
								resourceStruct.NumOfResources = 2;
								error = PassThruConnect(ulDeviceID, Protocol, flags, 19231, resourceStruct, &ulChannelID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                            }
                            iCurProtocol=Protocol;
                            EndDialog(hDlg, LOWORD(wParam));
                        }
                        break;
                    case IDCANCEL:
                        EndDialog(hDlg, LOWORD(wParam));
                        return TRUE;
                    break;
                }
            }
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK DisconnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt2;
	BOOL retval;
	int iMsgNum, error;

	switch (message)
	{
	case WM_INITDIALOG:
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		SetWindowText(hDlg, "PassThruDisconnect");
		SetWindowText(GetDlgItem(hDlg, IDC_STATIC_CHNID), "Channel number");
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_STOP:
			iMsgNum = GetDlgItemInt(hDlg, IDC_NUM, &retval, FALSE);
			if (!retval)
				break;
			sprintf_s(tempbuf, "PassThruDisconnect(ChannelID, %d);", iMsgNum);
			AddToScreen(tempbuf);
			AddToScreen("");
			error = PassThruDisconnect(iMsgNum);
			sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
			AddToScreen(tempbuf);
			AddToScreen("");

		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
		return TRUE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

LRESULT CALLBACK ReadMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    BOOL test;
    PASSTHRU_MSG *ptrMsg;
    unsigned long  ulNum, ulTimeout;
    int error;
    

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                    case IDC_READ:
                        ulNum=GetDlgItemInt(hDlg, IDC_NUM, &test, FALSE);
                        if(test==FALSE)
                        {
                            MessageBox(hDlg, "You must first specify how many messages", "Error:", MB_OK);
                        }
                        ulTimeout=GetDlgItemInt(hDlg, IDC_TIMEOUT, &test, FALSE);
                        if(test==FALSE)
                        {
                            MessageBox(hDlg, "You must first specify a timeout", "Error:", MB_OK);
                        }
                        else
                        {   
							ulChannelID = GetDlgItemInt(hDlg, IDC_CHANNELID, &test, FALSE);
							if (!test)
								break;
                            sprintf_s(tempbuf, "PassThruReadMsgs(ulChannelID, ptrMsg, &ulNum, %d);", ulTimeout);
                            AddToScreen(tempbuf);
							for (unsigned long index = 1; index <= ulNum; index++)
							{
								ptrMsg = new PASSTHRU_MSG;
								memset(ptrMsg, 0, sizeof(PASSTHRU_MSG));
#ifdef J2534_0500
								ptrMsg->Data = new unsigned char[4128]; 
#endif
								memset(ptrMsg->Data, 0, 4128);
								unsigned long uGetData = 1;
								error = PassThruReadMsgs(ulChannelID, ptrMsg, &uGetData, ulTimeout);
								sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
								AddToScreen(tempbuf);
								if (error == 0 || error == 9)
								{
									if (ulNum>0)								
									{
										strcpy_s(tempbuf, "Message Received:");
										for (unsigned long j = 0; j<ptrMsg->DataSize; j++)
										{
											sprintf_s(tempbuf + strlen(tempbuf), sizeof(tempbuf) - strlen(tempbuf), "% 02X", ptrMsg->Data[j]);
										}
										AddToScreen(tempbuf);

										sprintf_s(tempbuf, "ProtocolID: %d", ptrMsg->ProtocolID);
										AddToScreen(tempbuf);
										sprintf_s(tempbuf, "RxStatus: %08X", ptrMsg->RxStatus);
										AddToScreen(tempbuf);
										sprintf_s(tempbuf, "Timestamp: %d", ptrMsg->Timestamp);
										AddToScreen(tempbuf);
										if (ptrMsg->ExtraDataIndex != 0)
										{
											sprintf_s(tempbuf, "at ExtraIndex: %02X", ptrMsg->ExtraDataIndex);
											AddToScreen(tempbuf);
										}
										if (ptrMsg->RxStatus&ISO15765_TX_DONE)
										{
											AddToScreen("Tx Done.");
										}
										if (ptrMsg->RxStatus&ISO15765_FIRST_FRAME)
										{
											AddToScreen("First frame.");
										}
									}
								}
								AddToScreen("");
							}// End of Main for loop
                            EndDialog(hDlg, LOWORD(wParam));
                        }
                        break;
                    case IDCANCEL:
                        EndDialog(hDlg, LOWORD(wParam));
                        return TRUE;
                    break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK WriteMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    PASSTHRU_MSG *ptrMsg;
    RECT rt, rt2;
    int i, size, error;
    static HWND hMsgList;
    unsigned long ulNumMsgs;
    unsigned long ulTxFlags;
    unsigned long ulExtraDataIndex;
    BOOL test;

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            for(i=0; i<10; i++)
            {
                ComboBox_AddString(GetDlgItem(hDlg, IDC_MSG), (char*)lastmsgs[i]);
            }
            hMsgList=GetDlgItem(hDlg, IDC_MSGLIST);
            sprintf_s(tempbuf, "%d", iCurProtocol);
            SetDlgItemText(hDlg, IDC_PROTOCOLID, tempbuf);
            SetDlgItemText(hDlg, IDC_TXFLAGS, "0");
            if(bCanExtended)
                SetDlgItemText(hDlg, IDC_TXFLAGS, "100");
            SetDlgItemText(hDlg, IDC_EXTRADATAINDEX, "0");
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDC_ADD:
                    if(GetDlgItemText(hDlg, IDC_MSG, (char*)tempstr, 16384))
                    {
                        SetDlgItemText(hDlg, IDC_MSG, NULL);
                        if(CheckAndConvertMsg(tempstr, tempmsg)!=-1)
                        {
                            if(tempstr[strlen((char*)tempstr)-1]==' ')
                                tempstr[strlen((char*)tempstr)-1]=0;

                            WritePrivateProfileString("Previous Msgs", NULL, NULL, path);
                            for(i=0; i<10; i++)
                            {
                                if(!strcmp((char*)tempstr, lastmsgs[i]))
                                    break;
                            }
                            if(i==10)
                            {
                                for(i=9; i>0; i--)
                                {
                                    memset(lastmsgs[i], 0, 16384);
                                    strcpy_s((char*)lastmsgs[i], sizeof(lastmsgs[0]),  (char*)lastmsgs[i-1]);
                                }

                                memset(lastmsgs[0], 0, 16384);
                                strcpy_s((char*)lastmsgs[0], sizeof(lastmsgs[0]), (char*)tempstr);
                                ComboBox_InsertString(GetDlgItem(hDlg, IDC_MSG), 0, (char*)lastmsgs[0]);
                                ComboBox_DeleteString(GetDlgItem(hDlg, IDC_MSG), 10);
                            }
                            for(i=0; i<10; i++)
                            {
                                sprintf_s(tempbuf, "Msg%d", i); 
                                WritePrivateProfileString("Previous Msgs", (char*)tempbuf, (char*)lastmsgs[i], path);
                            }
                            ListBox_AddString(hMsgList, tempstr);
                            EnableWindow(GetDlgItem(hDlg, IDC_WRITEMSGS), TRUE);
                        }
                    }
                    break;
                case IDC_REMOVE:
                    if((i=ListBox_GetCurSel(hMsgList))!=LB_ERR)
                    {
                        ListBox_DeleteString(hMsgList, i);
                        if(ListBox_GetCount(hMsgList)==0)
                            EnableWindow(GetDlgItem(hDlg, IDC_WRITEMSGS), FALSE);
                    }
                    break;
                case IDC_WRITEMSGS:
                    ulTxFlags=GetDlgItemInt(hDlg, IDC_TXFLAGS, &test, FALSE);
                    if(GetDlgItemText(hDlg, IDC_TXFLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
                        sscanf_s(tempbuf, "%X", &ulTxFlags);
                    ulExtraDataIndex=GetDlgItemInt(hDlg, IDC_EXTRADATAINDEX, &test, FALSE);
                    if((ulNumMsgs=ListBox_GetCount(hMsgList))!=0)
                    {
                        ptrMsg=new PASSTHRU_MSG[ulNumMsgs];
                        for(unsigned int j=0; j<ulNumMsgs; j++)
                        {
                            ListBox_GetText(hMsgList, j, tempstr);
                            size=CheckAndConvertMsg(tempstr, tempmsg);
                            memset(ptrMsg+j, 0, sizeof(PASSTHRU_MSG));

                            memcpy(ptrMsg[j].Data, tempmsg, size);
                            ptrMsg[j].DataSize=size;
                            ptrMsg[j].ExtraDataIndex=0;
                            ptrMsg[j].ProtocolID=GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);
                            if(bCanExtended)
                                ptrMsg[j].TxFlags=0x100;
                            else
                                ptrMsg[j].TxFlags=0x00;
                            ptrMsg[j].TxFlags=ulTxFlags;
                            ptrMsg[j].ExtraDataIndex=ulExtraDataIndex;
                        }

                        AddToScreen("ptrMsg=new PASSTHRU_MSG[ulNumMsgs];");
                        AddToScreen("for(unsigned int count=0; count<ulNumMsgs; count++)");
                        AddToScreen("{");
                        AddToScreen("   memset(ptrMsg+count, 0, sizeof(PASSTHRU_MSG));");
                        AddToScreen("");
                        AddToScreen("   memcpy(ptrMsg[count].Data, ptrTempMsg, size);");
                        AddToScreen("   ptrMsg[count].DataSize=ulSize;");
                        AddToScreen("   ptrMsg[count].ExtraDataIndex=0;");
                        AddToScreen("   ptrMsg[count].ProtocolID=iCurProtocol;");
                        if(bCanExtended)
                            AddToScreen("   ptrMsg[count].TxFlags=0x100;");
                        else
                            AddToScreen("   ptrMsg[count].TxFlags=0x00;");
                        AddToScreen("}");
                        AddToScreen("PassThruWriteMsgs(ulChannelID, ptrMsg, &ulNumMsgs, 10000);");
                        AddToScreen("delete [] ptrMsg;");
                        AddToScreen("");
                        error=PassThruWriteMsgs(ulChannelID, ptrMsg, &ulNumMsgs, 10000);
                        sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                        AddToScreen(tempbuf);
                        AddToScreen("");
                        //delete [] ptrMsg;
                        EndDialog(hDlg, LOWORD(wParam));
                    }
                    break;
                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK StartPeriodicProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    unsigned long ulTimerDelay, ulPeriodicID;
    BOOL retval;
    PASSTHRU_MSG ptrMsg;
	memset(&ptrMsg, 0, sizeof(PASSTHRU_MSG));
#ifdef J2534_0500
	ptrMsg.Data = new unsigned char[4128];
#endif
    unsigned long ulTxFlags;
    unsigned long ulExtraDataIndex;
    int error, size;
    BOOL test;
	
    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            sprintf_s(tempbuf, "%d", iCurProtocol);
            SetDlgItemText(hDlg, IDC_PROTOCOLID, tempbuf);
            SetDlgItemText(hDlg, IDC_TXFLAGS, "0");
            if(bCanExtended)
                SetDlgItemText(hDlg, IDC_TXFLAGS, "100");
            SetDlgItemText(hDlg, IDC_EXTRADATAINDEX, "0");
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDC_START:
					ulChannelID = GetDlgItemInt(hDlg, IDC_PERIODIC_CHNID, &retval, FALSE);
					if (!retval)
						break;
                    ulTxFlags=GetDlgItemInt(hDlg, IDC_TXFLAGS, &test, FALSE);
                    if(GetDlgItemText(hDlg, IDC_TXFLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
                        sscanf_s(tempbuf, "%X", &ulTxFlags);
                    ulExtraDataIndex=GetDlgItemInt(hDlg, IDC_EXTRADATAINDEX, &test, FALSE);
                    if(GetDlgItemText(hDlg, IDC_MSG, tempbuf, 16384))
                    {
                        ulTimerDelay=GetDlgItemInt(hDlg, IDC_DELAY, &retval, FALSE);
                        if(retval)
                        {
#ifdef J2534_0500
							if(size=CheckAndConvertMsg((unsigned char *)tempbuf, ptrMsg.Data))
#endif
#ifndef J2534_0500
							if (size = CheckAndConvertMsg((unsigned char *)tempbuf, tempmsg))
#endif
							{
                                ptrMsg.DataSize=size;
                                ptrMsg.ExtraDataIndex=0;
                                ptrMsg.ProtocolID=GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);
                                if(bCanExtended)
                                    ptrMsg.TxFlags=0x100;
                                else
                                    ptrMsg.TxFlags=0x00;
                                
                                ptrMsg.TxFlags=ulTxFlags;
                                ptrMsg.ExtraDataIndex=ulExtraDataIndex;
#ifndef J2534_0500
								memcpy(ptrMsg.Data, tempmsg, size);
#endif

                                AddToScreen("memset(&ptrMsg, 0, sizeof(PASSTHRU_MSG));");
                                AddToScreen("memcpy(ptrMsg.Data, ptrTempMsg, size);");
                                AddToScreen("ptrMsg.DataSize=ulSize;");
                                AddToScreen("ptrMsg.ExtraDataIndex=0;");
                                AddToScreen("ptrMsg.ProtocolID=iCurProtocol;");
                                
								if(bCanExtended)
                                    AddToScreen("ptrMsg.TxFlags=0x100;");
                                else
                                    AddToScreen("ptrMsg.TxFlags=0x00;");

                                AddToScreen("");
                                AddToScreen("PassThruStartPeriodicMsg(ulChannelID, &ptrMsg, &ulPeriodicID, ulTimerDelay);");
                                AddToScreen("");
                                
								error=PassThruStartPeriodicMsg(ulChannelID, &ptrMsg, &ulPeriodicID, ulTimerDelay); 
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                
								if(error==STATUS_NOERROR)
                                {
                                    sprintf_s(tempbuf, "ulPeriodicID=%d", ulPeriodicID);
                                    AddToScreen(tempbuf);
                                }
                                AddToScreen("");
                            }
                            EndDialog(hDlg, LOWORD(wParam));
                        }
                    }

                    break;
                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK StopPeriodicProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    BOOL retval;
    int iMsgNum, error;

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDC_STOP:
                    iMsgNum=GetDlgItemInt(hDlg, IDC_NUM, &retval, FALSE);
                    if(!retval)
                        break;

                    sprintf_s(tempbuf,"PassThruStopPeriodicMsg(ulChannelID, %d);", iMsgNum);
                    AddToScreen(tempbuf);
                    AddToScreen("");
                    error=PassThruStopPeriodicMsg(ulChannelID, iMsgNum);
                    sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                    AddToScreen(tempbuf);
                    AddToScreen("");

                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK StartFilterProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    static HWND hCombo;
    PASSTHRU_MSG ptrMask, ptrPattern, ptrFC;
	memset(&ptrMask, 0, sizeof(PASSTHRU_MSG));
	memset(&ptrPattern, 0, sizeof(PASSTHRU_MSG));
#ifdef J2534_0500
	ptrMask.Data = new unsigned char[12];
	ptrPattern.Data = new unsigned char[12];
#endif

	unsigned long ulTxFlags;
    unsigned long ulMsgID, size;
    int error;     
    BOOL test;

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

            hCombo=GetDlgItem(hDlg, IDC_TYPE);
            ComboBox_AddString(hCombo, "PASS_FILTER");
            ComboBox_AddString(hCombo, "BLOCK_FILTER");

#ifndef J2534_0500
            ComboBox_AddString(hCombo, "FLOW_CONTROL_FILTER");
#endif
            sprintf_s(tempbuf, "%d", iCurProtocol);
            SetDlgItemText(hDlg, IDC_PROTOCOLID, tempbuf);
            SetDlgItemText(hDlg, IDC_TXFLAGS, "0");
            return TRUE;
        case WM_COMMAND:
            if(HIWORD(wParam)==CBN_SELCHANGE)
            {
                if(ComboBox_GetCurSel(hCombo)==2)
                {
                    EnableWindow(GetDlgItem(hDlg, IDC_FCMSG), TRUE);
                }
                else
                {
                    EnableWindow(GetDlgItem(hDlg, IDC_FCMSG), FALSE);
                }
            }
            else
            {
                switch (LOWORD(wParam))
                {
                    case IDC_START:
						ulChannelID = GetDlgItemInt(hDlg, IDC_PHY_FILTER_ID, &test, FALSE);
                        ulTxFlags=GetDlgItemInt(hDlg, IDC_TXFLAGS, &test, FALSE);
                        if(GetDlgItemText(hDlg, IDC_TXFLAGS, (char*)tempbuf, MAX_STRING_LENGTH))
                            sscanf_s(tempbuf, "%X", &ulTxFlags);
                        if(ComboBox_GetCurSel(hCombo)==-1)
                            break;
                        else
                        {
                            if(!GetDlgItemText(hDlg, IDC_PATTERN, (char*)tempmsg, 16384))
                                break;

                            if(!GetDlgItemText(hDlg, IDC_MASK, (char*)tempmsg2, 16384))
                                break;

#ifdef J2534_0500
							size = CheckAndConvertMsg((unsigned char *)tempmsg2, ptrMask.Data);
#endif
#ifndef J2534_0500
							size = CheckAndConvertMsg((unsigned char *)tempmsg2, tempstr);
							memcpy(ptrMask.Data, tempstr, size);
#endif
                            ptrMask.DataSize=size;
                            ptrMask.ExtraDataIndex=0;
                            ptrMask.ProtocolID=GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);

#ifdef J2534_0500
							size = CheckAndConvertMsg((unsigned char *)tempmsg, ptrPattern.Data);
#endif
#ifndef J2534_0500
							size = CheckAndConvertMsg((unsigned char *)tempmsg, tempstr);
							memcpy(ptrPattern.Data, tempstr, size);
#endif
                            ptrPattern.DataSize=size;
                            ptrPattern.ExtraDataIndex=0;
                            ptrPattern.ProtocolID=GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);

                            if(bCanExtended)
                            {
                                ptrMask.TxFlags=0x100;
                                ptrPattern.TxFlags=0x100;
                            }
                            ptrMask.TxFlags=ulTxFlags;
                            ptrPattern.TxFlags=ulTxFlags;

                            if(ComboBox_GetCurSel(hCombo)==2)
                            {
                                if(!GetDlgItemText(hDlg, IDC_FCMSG, (char*)tempmsg, 16384))
                                    break;

                                size=CheckAndConvertMsg(tempmsg, tempstr);
                                memset(&ptrFC, 0, sizeof(PASSTHRU_MSG));
                                memcpy(ptrFC.Data, tempstr, size);
                                ptrFC.DataSize=size;
                                ptrFC.ExtraDataIndex=0;
                                ptrFC.ProtocolID=GetDlgItemInt(hDlg, IDC_PROTOCOLID, &test, FALSE);
                                if(bCanExtended)
                                {
                                    ptrFC.TxFlags=0x100;
                                }
                                ptrFC.TxFlags=ulTxFlags;

                                AddToScreen("memset(&ptrFC, 0, sizeof(PASSTHRU_MSG));");
                                AddToScreen("memcpy(ptrFC.Data, ptrTempMsg, size);");
                                AddToScreen("ptrFC.DataSize=ulSize;");
                                AddToScreen("ptrFC.ExtraDataIndex=0;");
                                AddToScreen("ptrFC.ProtocolID=iCurProtocol;");
                                if(bCanExtended)
                                    AddToScreen("ptrFC.TxFlags=0x100;");
                                else
                                    AddToScreen("ptrFC.TxFlags=0x00;");
                                AddToScreen("");
                            }

                            AddToScreen("memset(&ptrMask, 0, sizeof(PASSTHRU_MSG));");
                            AddToScreen("memcpy(ptrMask.Data, ptrTempMsg, size);");
                            AddToScreen("ptrMask.DataSize=ulSize;");
                            AddToScreen("ptrMask.ExtraDataIndex=0;");
                            AddToScreen("ptrMask.ProtocolID=iCurProtocol;");
                            if(bCanExtended)
                                AddToScreen("ptrMask.TxFlags=0x100;");
                            else
                                AddToScreen("ptrMask.TxFlags=0x00;");
                            AddToScreen("");
                            AddToScreen("memset(&ptrPattern, 0, sizeof(PASSTHRU_MSG));");
                            AddToScreen("memcpy(ptrPattern.Data, ptrTempMsg, size);");
                            AddToScreen("ptrPattern.DataSize=ulSize;");
                            AddToScreen("ptrPattern.ExtraDataIndex=0;");
                            AddToScreen("ptrPattern.ProtocolID=iCurProtocol;");
                            if(bCanExtended)
                                AddToScreen("ptrPattern.TxFlags=0x100;");
                            else
                                AddToScreen("ptrPattern.TxFlags=0x00;");
                            AddToScreen("");

                            if(ComboBox_GetCurSel(hCombo)==0)
                            {
                                AddToScreen("PassThruStartMsgFilter(ulChannelID, PASS_FILTER, ptrMask, ptrPattern, &ulMsgID);");
                                AddToScreen("");
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruStartMsgFilter(ulChannelID, ComboBox_GetCurSel(hCombo)+1, &ptrMask, &ptrPattern, NULL, &ulMsgID);
#endif
#ifdef J2534_0500
								
							    ulChannelID = GetDlgItemInt(hDlg, IDC_PHY_FILTER_ID, &test, FALSE);
								if (!test)
									break;
								error=PassThruStartMsgFilter(ulChannelID, ComboBox_GetCurSel(hCombo)+1, &ptrMask, &ptrPattern, &ulMsgID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                                if(error==STATUS_NOERROR)
                                {
                                    sprintf_s(tempbuf, "ulMsgID=%d", ulMsgID);
                                    AddToScreen(tempbuf);
                                }
                            }
                            else if(ComboBox_GetCurSel(hCombo)==1)
                            {
                                AddToScreen("PassThruStartMsgFilter(ulChannelID, BLOCK_FILTER, ptrMask, ptrPattern, &ulMsgID);");
                                AddToScreen("");
#if defined (J2534_0404) || defined (J2534_0305)
                                error=PassThruStartMsgFilter(ulChannelID, ComboBox_GetCurSel(hCombo)+1, &ptrMask, &ptrPattern, NULL, &ulMsgID);
#endif
#ifdef J2534_0500
                                error=PassThruStartMsgFilter(ulChannelID, ComboBox_GetCurSel(hCombo)+1, &ptrMask, &ptrPattern, &ulMsgID);
#endif
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                                if(error==STATUS_NOERROR)
                                {
                                    sprintf_s(tempbuf, "ulMsgID=%d", ulMsgID);
                                    AddToScreen(tempbuf);
                                }
                            }
#if defined (J2534_0404) || defined (J2534_0305)
                            else if(ComboBox_GetCurSel(hCombo)==2)
                            {
                                AddToScreen("memset(&ptrFC, 0, sizeof(PASSTHRU_MSG));");
                                AddToScreen("memcpy(ptrFC.Data, ptrTempMsg, size);");
                                AddToScreen("ptrFC.DataSize=ulSize;");
                                AddToScreen("ptrFC.ExtraDataIndex=0;");
                                AddToScreen("ptrFC.ProtocolID=iCurProtocol;");
                                AddToScreen("");
                                AddToScreen("PassThruStartMsgFilter(ulChannelID, FLOW_CONTROL_FILTER, ptrMask, ptrPattern, ptrFlowControl, &ulMsgID);");
                                AddToScreen("");
                                error=PassThruStartMsgFilter(ulChannelID, ComboBox_GetCurSel(hCombo)+1, &ptrMask, &ptrPattern, &ptrFC, &ulMsgID);

                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                AddToScreen("");
                                if(error==STATUS_NOERROR)
                                {
                                    sprintf_s(tempbuf, "ulMsgID=%d", ulMsgID);
                                    AddToScreen(tempbuf);
                                }
                            }
#endif
                        }
                        //break;
                    case IDCANCEL:
                        EndDialog(hDlg, LOWORD(wParam));
                    break;
                }
                return TRUE;
            }
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK StopFilterProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    BOOL retval;
    int iMsgNum, error;

    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDC_STOP:
                    iMsgNum=GetDlgItemInt(hDlg, IDC_NUM, &retval, FALSE);
                    if(!retval)
                        break;

                    sprintf_s(tempbuf,"PassThruStopMsgFilter(ulChannelID, %d);", iMsgNum);
                    AddToScreen(tempbuf);
                    AddToScreen("");
                    error=PassThruStopMsgFilter(ulChannelID, iMsgNum);
                    sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                    AddToScreen(tempbuf);
                    AddToScreen("");

                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}

LRESULT CALLBACK SetProgrammingVoltageProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    BOOL retval;
    unsigned long iPinNum, voltage, error;
#ifdef J2534_0500
	RESOURCE_STRUCT resourceStruct;
#endif
    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
            return TRUE;
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDOK:
                    iPinNum=GetDlgItemInt(hDlg, IDC_PINNUMBER, &retval, FALSE);
                    if(!retval)
                        break;
                    voltage=GetDlgItemInt(hDlg, IDC_VOLTAGE, &retval, FALSE);
                    if(!retval)
                        break;
#ifdef J2534_0500
					memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));
					resourceStruct.Connector = J1962_CONNECTOR;
					resourceStruct.NumOfResources = 1;
					resourceStruct.ResourceListPtr = &iPinNum;
					AddToScreen("memset(&resourceStruct, 0, sizeof(RESOURCE_STRUCT));");
					sprintf_s(tempbuf, "PassThruSetProgrammingVoltage(ulDeviceID, %lu, %lu %lu, %lu);", resourceStruct.Connector,
						resourceStruct.NumOfResources,iPinNum, voltage);
					AddToScreen(tempbuf);
					error = PassThruSetProgrammingVoltage(ulDeviceID, resourceStruct, voltage);
					sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
					AddToScreen(tempbuf);
					AddToScreen("");
#endif

#if defined (J2534_0404) || defined (J2534_0305)
					sprintf_s(tempbuf,"PassThruSetProgrammingVoltage(ulDeviceID, %lu, %lu);", iPinNum, voltage);
                    AddToScreen(tempbuf);
                    AddToScreen("");
                    error=PassThruSetProgrammingVoltage(ulDeviceID, iPinNum, voltage);
                    sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                    AddToScreen(tempbuf);
                    AddToScreen("");
#endif
                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}
#ifdef J2534_0500
LRESULT CALLBACK Ioctl0500Proc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT rt, rt2;
	static HWND hCombo, hCombo2;
	int sel, sel2, error, size, param;
	unsigned long longparam, ulVBattVoltage;
	unsigned char address, Keybytes[2];
	SBYTE_ARRAY InputParam, OutputParam;
	PASSTHRU_MSG PassThruTempMsg, ResultMsg;
	SCONFIG_LIST ConfigList;
	SCONFIG Config;
	SPARAM_LIST ParamList;
	SPARAM Param;
	J2534IOCTLPARAMID IoctlParamID;
	J2534GETDEVICEINFOPARAMID IoctlGetDeviceInfoParamID;
	J2534GETPROTOCOLINFOPARAMID IoctlGetProtocolInfoParamID;
	char chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH];
	int iIoctlParamIdIndex;
	int iIoctlGetDeviceInfoParamIdIndex;
	int iIoctlGetProtocolInfoParamIdIndex;
	unsigned long ulDataSize = 10000;
	BOOL retval;
	switch (message)
	{
	case WM_INITDIALOG:
		GetWindowRect(GetDesktopWindow(), &rt);
		GetWindowRect(hDlg, &rt2);
		SetWindowPos(hDlg, hDlg, (rt.right - rt.left) / 2 - (rt2.right - rt2.left) / 2,
			(rt.bottom - rt.top) / 2 - (rt2.bottom - rt2.top) / 2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		ShowWindow(GetDlgItem(hDlg, IDC_EDT_IOCTL_CHID), SW_SHOW);
		ShowWindow(GetDlgItem(hDlg, IDC_STAT_CHANNEL_ID), SW_SHOW);

		hCombo = GetDlgItem(hDlg, IDC_IOCTL);
		ComboBox_AddString(hCombo, "GET_CONFIG");
		ComboBox_AddString(hCombo, "SET_CONFIG");
		ComboBox_AddString(hCombo, "READ_PIN_VOLTAGE");
		ComboBox_AddString(hCombo, "FIVE_BAUD_INIT");
		ComboBox_AddString(hCombo, "FAST_INIT");
		ComboBox_AddString(hCombo, "SET_PIN_USE");
		ComboBox_AddString(hCombo, "CLEAR_TX_QUEUE");
		ComboBox_AddString(hCombo, "CLEAR_RX_QUEUE");
		ComboBox_AddString(hCombo, "CLEAR_PERIODIC_MSGS");
		ComboBox_AddString(hCombo, "CLEAR_MSG_FILTERS");
		ComboBox_AddString(hCombo, "CLEAR_FUNCT_MSG_LOOKUP_TABLE");
		ComboBox_AddString(hCombo, "ADD_TO_FUNCT_MSG_LOOKUP_TABLE");
		ComboBox_AddString(hCombo, "DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE");
		ComboBox_AddString(hCombo, "READ_PROG_VOLTAGE");
		ComboBox_AddString(hCombo, "BUS_ON");

		hCombo2 = GetDlgItem(hDlg, IDC_CONFIGPARAM);
		for (param = 0; param < IOCTL_PARAM_ENTRY_COUNT; param++)
		{
             ComboBox_AddString(hCombo2, IoctlParamId[param].Description);
		}

		return TRUE;
	case WM_COMMAND:
		if (HIWORD(wParam) == CBN_SELCHANGE)
		{
			if (LOWORD(wParam) == IDC_IOCTL)
			{
				ShowWindow(GetDlgItem(hDlg, IDC_GET_PROTOCOL_ID), SW_HIDE);
				ShowWindow(GetDlgItem(hDlg, IDC_PROTOCOL), SW_HIDE);
				SetDlgItemText(hDlg, IDC_PROTOCOL, "");
				ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
				ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
				SetDlgItemText(hDlg, IDC_PARAM, "");
				int foo = ComboBox_GetCurSel(hCombo);
				if (ComboBox_GetCurSel(hCombo) == 0 ||             // GET_CONFIG 
					ComboBox_GetCurSel(hCombo) == 1)              // SET_CONFIG 
				{
					ComboBox_ResetContent(hCombo2);
					for (param = 0; param < IOCTL_PARAM_ENTRY_COUNT; param++)
					{
						ComboBox_AddString(hCombo2, IoctlParamId[param].Description);
					}
					EnableWindow(hCombo2, TRUE);
					ComboBox_SetCurSel(hCombo2, -1);
				}
				else
				{
					EnableWindow(hCombo2, FALSE);
					if (ComboBox_GetCurSel(hCombo) == 3 ||         // FIVE_BAUD_INIT
						ComboBox_GetCurSel(hCombo) == 4 ||        // FAST_INIT
						ComboBox_GetCurSel(hCombo) == 10 ||       // CLEAR_FUNCT_MSG_LOOKUP_TABLE
						ComboBox_GetCurSel(hCombo) == 11 )
					{
						SetDlgItemText(hDlg, IDC_INT, "");
						ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
						ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
						SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex except data rate)");
					}
					else
					{
						ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
						ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
						SetDlgItemText(hDlg, IDC_PARAM, "");
					}
				}
			}
			else if (LOWORD(wParam) == IDC_CONFIGPARAM)
			{
				if (ComboBox_GetCurSel(hCombo) == 0 ||             // GET_CONFIG 
					ComboBox_GetCurSel(hCombo) == 1)              // SET_CONFIG
				{
					ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
					ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
					SetDlgItemText(hDlg, IDC_PARAM, "");
					switch (ComboBox_GetCurSel(hCombo2) + 2)
					{

					case NETWORK_LINE:
						if (ComboBox_GetCurSel(hCombo) == 1)
						{
							ComboBox_ResetContent(GetDlgItem(hDlg, IDC_CHOICE));
							ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "0 (BUS_NORMAL)");
							ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "1 (BUS_PLUS)");
							ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "2 (BUS_MINUS)");
							ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_SHOW);
							ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
							SetDlgItemText(hDlg, IDC_PARAM, "Please select a parameter:");
						}
						else
						{
							ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
							ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
							SetDlgItemText(hDlg, IDC_PARAM, "");
						}
						break;
					default:
						if (ComboBox_GetCurSel(hCombo) == 1)
						{
							SetDlgItemText(hDlg, IDC_INT, "");
							ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
							ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
							SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex except data rate)");
						}
						else
						{
							ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
							ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
							SetDlgItemText(hDlg, IDC_PARAM, "");
						}
						break;
					}
				}
				else
				{
					ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
					SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex)");
				}
			}
			break;
		}
		switch (LOWORD(wParam))
		{
		case IDC_ISSUEIOCTL:
			sel = ComboBox_GetCurSel(hCombo);
			ulChannelID = GetDlgItemInt(hDlg, IDC_EDT_IOCTL_CHID, &retval, FALSE);
			if (!retval)
				break;
			switch (sel)
			{
			case 0: case 1:
				param = ComboBox_GetCurSel(hCombo2);
				ComboBox_GetLBText(hCombo2, param, chTempstr1);
				IoctlParamID = fnGetIoctlParamId(chTempstr1);
				iIoctlParamIdIndex = fnGetIoctlParamIdIndex(IoctlParamID);
				param++;
				if (param >= 2)
					param++;
				ConfigList.NumOfParams = 1;
				ConfigList.ConfigPtr = &Config;
				Config.Parameter = IoctlParamID;
				Config.Value = 0;
				AddToScreen("ConfigList.NumOfParams=1;");
				AddToScreen("ConfigList.ConfigPtr=&Config;");
				sprintf_s(tempbuf, "Config.Parameter=%s;", IoctlParamId[iIoctlParamIdIndex].Description);
				AddToScreen(tempbuf);
				if (IoctlParamID == NETWORK_LINE)
				{
					if (sel == 1)
					{
						sel2 = ComboBox_GetCurSel(GetDlgItem(hDlg, IDC_CHOICE));
						Config.Value = sel2;
						sprintf_s(tempbuf, "Config.Value=%d;", sel2);
						AddToScreen(tempbuf);
					}
				}
				else
				{
					if (sel == 1)
					{
						if (!GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH))
							break;

						if (IoctlParamID == DATA_RATE)
						{
							sscanf_s((char*)tempstr, "%d", &longparam);
							Config.Value = longparam;
							sprintf_s(tempbuf, "Config.Value=%d;", longparam);
							AddToScreen(tempbuf);
						}
						else
						{
							sscanf_s((char*)tempstr, "%X", &longparam);
							Config.Value = longparam;
							sprintf_s(tempbuf, "Config.Value=0x%X;", longparam);
							AddToScreen(tempbuf);
						}
					}
				}
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &ConfigList, NULL);", IoctlList_0500[sel + 1]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulChannelID, sel + 1, &ConfigList, NULL);
				if (error == STATUS_NOERROR && sel == 0)
				{
					iIoctlParamIdIndex = fnGetIoctlParamIdIndex(IoctlParamID);
					if (IoctlParamID == DATA_RATE)
						sprintf_s(tempbuf, "%s = %d", IoctlParamId[iIoctlParamIdIndex].Description, Config.Value);
					else
						sprintf_s(tempbuf, "%s = 0x%X", IoctlParamId[iIoctlParamIdIndex].Description, Config.Value);

					AddToScreen(tempbuf);
				}
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
				break;
			case 2:
				sel += 1;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				AddToScreen("");
				sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulDeviceID, sel, NULL, &ulVBattVoltage);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				if (error == STATUS_NOERROR)
				{
					sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
					AddToScreen(tempbuf);
				}
				AddToScreen("");
				break;
			case 3:
				sel += 1;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				sscanf_s((char*)tempstr, "%02X", &address);
				sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", address);
				AddToScreen(tempbuf);
				AddToScreen("InputParam.NumOfBytes=1;");
				AddToScreen("InputParam.BytePtr=szTempbuf;");
				AddToScreen("OutputParam.NumOfBytes=2;");
				AddToScreen("OutputParam.BytePtr=Keybytes;");
				AddToScreen("");
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, &OutputParam);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				InputParam.NumOfBytes = 1;
				InputParam.BytePtr = &address;
				OutputParam.BytePtr = Keybytes;
				OutputParam.NumOfBytes = 2;
				error = PassThruIoctl(ulChannelID, sel, &InputParam, &OutputParam);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				if (error == STATUS_NOERROR)
				{
					sprintf_s(tempbuf, "Keybyte 1: 0x%02X   Keybyte 2: 0x%02X", Keybytes[0], Keybytes[1]);
					AddToScreen(tempbuf);
				}
				AddToScreen("");
				break;
			case 4:
				sel += 1;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				memset(&PassThruTempMsg, 0, sizeof(PASSTHRU_MSG));
				PassThruTempMsg.Data = new unsigned char[10];
				size = CheckAndConvertMsg((unsigned char *)tempstr, PassThruTempMsg.Data);
				if (size > 0)
				{
					PassThruTempMsg.DataSize = size;
					PassThruTempMsg.ExtraDataIndex = 0;
					PassThruTempMsg.ProtocolID = iCurProtocol;

					sprintf_s(tempbuf, "szTempMsg=%s", tempstr);
					AddToScreen(tempbuf);
					AddToScreen("<Convert szTempMsg from Ascii Text to actual data bytes>");
					AddToScreen("");
					AddToScreen("memset(&PassThruTempMsg, 0, sizeof(PASSTHRU_MSG));");
					AddToScreen("memcpy(PassThruTempMsg.Data, szTempMsg, size);");
					AddToScreen("PassThruTempMsg.DataSize=ulSize;");
					AddToScreen("PassThruTempMsg.ExtraDataIndex=0;");
					AddToScreen("PassThruTempMsg.ProtocolID=iCurProtocol;");
					AddToScreen("");
					sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &PassThruTempMsg, &ResultMsg);", IoctlList_0500[sel]);
					AddToScreen(tempbuf);
					error = PassThruIoctl(ulChannelID, sel, &PassThruTempMsg, &ResultMsg);
					sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
					AddToScreen(tempbuf);
					if (error == STATUS_NOERROR)
					{
						strcpy_s((char *)tempstr, sizeof(tempstr), "Response: ");
						for (unsigned int i = 0; i < ResultMsg.DataSize; i++)
						{
							sprintf_s((char *)tempstr + strlen((char *)tempstr), sizeof(tempstr) - strlen((char *)tempstr), "%02X ", ResultMsg.Data[i]);
						}
						AddToScreen((char*)tempstr);
						sprintf_s((char*)tempstr, sizeof(tempstr), "Response Length: %d", ResultMsg.DataSize);
						AddToScreen((char*)tempstr);
					}
					AddToScreen("");
				}
				break;
			case 10:
			case 11:
				sel += 2;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				sscanf_s((char*)tempstr, "%02X", &address);
				sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", address);
				AddToScreen(tempbuf);
				AddToScreen("InputParam.NumOfBytes=1;");
				AddToScreen("InputParam.BytePtr=szTempbuf;");
				AddToScreen("");
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, NULL);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				InputParam.NumOfBytes = 1;
				InputParam.BytePtr = &address;
				error = PassThruIoctl(ulChannelID, sel, &InputParam, NULL);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
				break;
			case 12:
				sel += 2;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				AddToScreen("");
				sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulDeviceID, sel, NULL, &ulVBattVoltage);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				if (error == STATUS_NOERROR)
				{
					sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
					AddToScreen(tempbuf);
				}
				AddToScreen("");
				break;
			case 13:
				sel += 2;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, &OutputParam);", IoctlList_0500[sel-1]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulChannelID, sel, &InputParam, &OutputParam);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
				break;
			case 16:
				sel += 2;
				GetDlgItemText(hDlg, IDC_INT, (char *)tempstr, MAX_STRING_LENGTH);
				sscanf_s((char*)tempstr, "%02X %02X", Keybytes, Keybytes + 1);
				sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", Keybytes[0]);
				AddToScreen(tempbuf);
				sprintf_s(tempbuf, "szTempbuf[1]=0x%02X;", Keybytes[1]);
				AddToScreen(tempbuf);
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, szTempbuf, NULL);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulChannelID, 0x10100, Keybytes, NULL);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");

				break;
			case 17:
				sel += 1;
				GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
				AddToScreen("");
				sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulDeviceID, 0x10007, NULL, &ulVBattVoltage);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				if (error == STATUS_NOERROR)
				{
					sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
					AddToScreen(tempbuf);
				}
				AddToScreen("");
				break;
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
				break;
			default:
				sel += 1;
				sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, NULL, NULL);", IoctlList_0500[sel]);
				AddToScreen(tempbuf);
				error = PassThruIoctl(ulChannelID, sel, NULL, NULL);
				sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
				AddToScreen(tempbuf);
				AddToScreen("");
			}
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
		return TRUE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}
#endif
#ifndef J2534_0500
LRESULT CALLBACK IoctlProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rt, rt2;
    static HWND hCombo, hCombo2;
    int sel, sel2, error, size, param;
    unsigned long longparam, ulVBattVoltage;
    unsigned char address, Keybytes[2];
    SBYTE_ARRAY InputParam, OutputParam;
    PASSTHRU_MSG PassThruTempMsg, ResultMsg;
    SCONFIG_LIST ConfigList;
    SCONFIG Config;                          
    SPARAM_LIST ParamList;
    SPARAM Param;
    J2534IOCTLPARAMID IoctlParamID;
    J2534GETDEVICEINFOPARAMID IoctlGetDeviceInfoParamID;
    J2534GETPROTOCOLINFOPARAMID IoctlGetProtocolInfoParamID;
    char chTempstr1[J2534REGISTRY_MAX_STRING_LENGTH];
    int iIoctlParamIdIndex;
    int iIoctlGetDeviceInfoParamIdIndex;
    int iIoctlGetProtocolInfoParamIdIndex;
    unsigned long ulDataSize = 10000;
	BOOL retval;
    switch (message)
    {
        case WM_INITDIALOG:
            GetWindowRect(GetDesktopWindow(), &rt);
            GetWindowRect(hDlg, &rt2);
            SetWindowPos(hDlg, hDlg, (rt.right-rt.left)/2-(rt2.right-rt2.left)/2,
                        (rt.bottom-rt.top)/2-(rt2.bottom-rt2.top)/2, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
#ifndef J2534_0500
			ShowWindow(GetDlgItem(hDlg, IDC_EDT_IOCTL_CHID), SW_HIDE);
			ShowWindow(GetDlgItem(hDlg, IDC_STAT_CHANNEL_ID), SW_HIDE);
#endif
#ifdef J2534_0500
			ShowWindow(GetDlgItem(hDlg, IDC_EDT_IOCTL_CHID), SW_SHOW);
			ShowWindow(GetDlgItem(hDlg, IDC_STAT_CHANNEL_ID), SW_SHOW);			
#endif
            hCombo=GetDlgItem(hDlg, IDC_IOCTL);
            ComboBox_AddString(hCombo, "GET_CONFIG");
            ComboBox_AddString(hCombo, "SET_CONFIG");
            ComboBox_AddString(hCombo, "READ_VBATT");
            ComboBox_AddString(hCombo, "FIVE_BAUD_INIT");
            ComboBox_AddString(hCombo, "FAST_INIT");
            ComboBox_AddString(hCombo, "CLEAR_TX_BUFFER");
            ComboBox_AddString(hCombo, "CLEAR_RX_BUFFER");
            ComboBox_AddString(hCombo, "CLEAR_PERIODIC_MSGS");
            ComboBox_AddString(hCombo, "CLEAR_MSG_FILTERS");
            ComboBox_AddString(hCombo, "CLEAR_FUNCT_MSG_LOOKUP_TABLE");
            ComboBox_AddString(hCombo, "ADD_TO_FUNCT_MSG_LOOKUP_TABLE");
            ComboBox_AddString(hCombo, "DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE");
            ComboBox_AddString(hCombo, "READ_PROG_VOLTAGE");
            ComboBox_AddString(hCombo, "PROTECT_J1939_ADDR");
            ComboBox_AddString(hCombo, "GET_DEVICE_INFO");
            ComboBox_AddString(hCombo, "GET_PROTOCOL_INFO");
            ComboBox_AddString(hCombo, "CAN_SET_BTR");
            ComboBox_AddString(hCombo, "READ_VIGNITION");
            ComboBox_AddString(hCombo, "LIN_SENDWAKEUP");
            ComboBox_AddString(hCombo, "LIN_ADD_SCHED");
            ComboBox_AddString(hCombo, "LIN_GET_SCHED");
            ComboBox_AddString(hCombo, "LIN_GET_SCHED_SIZE");
            ComboBox_AddString(hCombo, "LIN_DEL_SCHED");
            ComboBox_AddString(hCombo, "LIN_ACT_SCHED");
            ComboBox_AddString(hCombo, "LIN_DEACT_SCHED");    
            ComboBox_AddString(hCombo, "LIN_GET_ACT_SCHED");
            ComboBox_AddString(hCombo, "LIN_GET_NUM_SCHEDS");
            ComboBox_AddString(hCombo, "LIN_GET_SCHED_NAMES");
            ComboBox_AddString(hCombo, "LIN_SET_FLAGS");
            ComboBox_AddString(hCombo, "RESETHC08");
            ComboBox_AddString(hCombo, "TESTHC08COP");
            ComboBox_AddString(hCombo, "SWCAN_HS");
            ComboBox_AddString(hCombo, "SWCAN_NS");

            hCombo2=GetDlgItem(hDlg, IDC_CONFIGPARAM);
            for (param = 0; param < IOCTL_PARAM_ENTRY_COUNT; param++)
            {
/*                if (!bSupportPinSwitchProtocols)
                {
                    if (strcmp(IoctlParamId[param].Description, "J1962_PINS") == 0)
                        continue;   // Don't even give them the choice
                }
*/              ComboBox_AddString(hCombo2, IoctlParamId[param].Description);
            }

            return TRUE;
        case WM_COMMAND:
            if(HIWORD(wParam)==CBN_SELCHANGE)
            {
                if(LOWORD(wParam)==IDC_IOCTL)
                {
                    ShowWindow(GetDlgItem(hDlg, IDC_GET_PROTOCOL_ID), SW_HIDE);
                    ShowWindow(GetDlgItem(hDlg, IDC_PROTOCOL), SW_HIDE);
                    SetDlgItemText(hDlg, IDC_PROTOCOL, "");
                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                    SetDlgItemText(hDlg, IDC_PARAM, "");
                    int foo = ComboBox_GetCurSel(hCombo);
                    if(ComboBox_GetCurSel(hCombo)==0 ||             // GET_CONFIG 
                        ComboBox_GetCurSel(hCombo)==1)              // SET_CONFIG 
                    {
                        ComboBox_ResetContent(hCombo2);
                        for (param = 0; param < IOCTL_PARAM_ENTRY_COUNT; param++)
                        {
                          ComboBox_AddString(hCombo2, IoctlParamId[param].Description);
                        }
                        EnableWindow(hCombo2, TRUE);
                        ComboBox_SetCurSel(hCombo2, -1);
                    } 
                    else if(ComboBox_GetCurSel(hCombo)==14)         // GET_DEVICE_INFO 
                    {
                        ComboBox_ResetContent(hCombo2);
                        for (param = 0; param < IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT; param++)
                        {
            /*                if (!bSupportPinSwitchProtocols)
                            {
                                if (strcmp(IoctlParamId[param].Description, "J1962_PINS") == 0)
                                    continue;   // Don't even give them the choice
                            }
            */              ComboBox_AddString(hCombo2, IoctlGetDeviceInfoParamId[param].Description);
                        }
                        EnableWindow(hCombo2, TRUE);
                        ComboBox_SetCurSel(hCombo2, -1);
                    }
                    else if(ComboBox_GetCurSel(hCombo)==15)         // GET_PROTOCOL_INFO 
                    {
                        ShowWindow(GetDlgItem(hDlg, IDC_GET_PROTOCOL_ID), SW_NORMAL);
                        ShowWindow(GetDlgItem(hDlg, IDC_PROTOCOL), SW_NORMAL);
                        SetDlgItemText(hDlg, IDC_PROTOCOL, "0");
                        ComboBox_ResetContent(hCombo2);
                        for (param = 0; param < IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT; param++)
                        {
            /*                if (!bSupportPinSwitchProtocols)
                            {
                                if (strcmp(IoctlParamId[param].Description, "J1962_PINS") == 0)
                                    continue;   // Don't even give them the choice
                            }
            */              ComboBox_AddString(hCombo2, IoctlGetProtocolInfoParamId[param].Description);
                        }
                        EnableWindow(hCombo2, TRUE);
                        ComboBox_SetCurSel(hCombo2, -1);
                    }
                    else
                    {
                        EnableWindow(hCombo2, FALSE);
                        if(ComboBox_GetCurSel(hCombo)==3 ||         // FIVE_BAUD_INIT
                            ComboBox_GetCurSel(hCombo)==4 ||        // FAST_INIT
                            ComboBox_GetCurSel(hCombo)==10 ||       // CLEAR_FUNCT_MSG_LOOKUP_TABLE
                            ComboBox_GetCurSel(hCombo)==11 ||       // ADD_TO_FUNCT_MSG_LOOKUP_TABLE
                            ComboBox_GetCurSel(hCombo)==13 ||       // READ_PROG_VOLTAGE
                            ComboBox_GetCurSel(hCombo)==14 ||       // PROTECT_J1939_ADDR
                            (ComboBox_GetCurSel(hCombo)>=16 &&
                            ComboBox_GetCurSel(hCombo)<=30))         
                        {
                            SetDlgItemText(hDlg, IDC_INT, "");
                            ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                            ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
                            SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex except data rate)");
                        }
                        else
                        {
                            ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                            ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                            SetDlgItemText(hDlg, IDC_PARAM, "");
                        }
                    }
                }
                else if(LOWORD(wParam)==IDC_CONFIGPARAM)
                {
                    if(ComboBox_GetCurSel(hCombo)==0 ||             // GET_CONFIG 
                        ComboBox_GetCurSel(hCombo)==1)              // SET_CONFIG
                    {
                        ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                        ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                        SetDlgItemText(hDlg, IDC_PARAM, "");
                        switch(ComboBox_GetCurSel(hCombo2)+2)
                        {
#ifndef J2534_0500   
						case LOOPBACK:
#endif
                                if(ComboBox_GetCurSel(hCombo)==1)
                                {
                                    ComboBox_ResetContent(GetDlgItem(hDlg, IDC_CHOICE));
                                    ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "0 (OFF)");
                                    ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "1 (ON)");
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_SHOW);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                                    SetDlgItemText(hDlg, IDC_PARAM, "Please select a parameter:");
                                }
                                else
                                {
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                                    SetDlgItemText(hDlg, IDC_PARAM, "");
                                }
                                break;
                            case NETWORK_LINE:
                                if(ComboBox_GetCurSel(hCombo)==1)
                                {
                                    ComboBox_ResetContent(GetDlgItem(hDlg, IDC_CHOICE));
                                    ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "0 (BUS_NORMAL)");
                                    ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "1 (BUS_PLUS)");
                                    ComboBox_AddString(GetDlgItem(hDlg, IDC_CHOICE), "2 (BUS_MINUS)");
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_SHOW);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                                    SetDlgItemText(hDlg, IDC_PARAM, "Please select a parameter:");
                                }
                                else
                                {
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                                    SetDlgItemText(hDlg, IDC_PARAM, "");
                                }
                                break;
                            default:
                                if(ComboBox_GetCurSel(hCombo)==1)
                                {
                                    SetDlgItemText(hDlg, IDC_INT, "");
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
                                    SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex except data rate)");
                                }
                                else
                                {
                                    ShowWindow(GetDlgItem(hDlg, IDC_CHOICE), SW_HIDE);
                                    ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_HIDE);
                                    SetDlgItemText(hDlg, IDC_PARAM, "");
                                }
                                break;
                        }
                    }
                    else
                    {
                        ShowWindow(GetDlgItem(hDlg, IDC_INT), SW_SHOW);
                        SetDlgItemText(hDlg, IDC_PARAM, "Enter parameter: (Be sure to enter all data in hex)");
                    }
                }
                break;
            }
            switch (LOWORD(wParam))
            {
                case IDC_ISSUEIOCTL:
                    sel=ComboBox_GetCurSel(hCombo);
#ifdef J2534_0500
					ulChannelID = GetDlgItemInt(hDlg,IDC_EDT_IOCTL_CHID, &retval, FALSE);
					if (!retval)
						break;
#endif
                    switch(sel)
                    {
                        case 0: case 1:
                            param=ComboBox_GetCurSel(hCombo2);
                            ComboBox_GetLBText(hCombo2, param, chTempstr1);
                            IoctlParamID = fnGetIoctlParamId(chTempstr1);
                            iIoctlParamIdIndex = fnGetIoctlParamIdIndex(IoctlParamID);
                            param++;
                            if(param>=2)
                                param++;
                            ConfigList.NumOfParams=1;
                            ConfigList.ConfigPtr=&Config;
                            Config.Parameter=IoctlParamID;
                            Config.Value=0;
                            AddToScreen("ConfigList.NumOfParams=1;");
                            AddToScreen("ConfigList.ConfigPtr=&Config;");
                            sprintf_s(tempbuf, "Config.Parameter=%s;", IoctlParamId[iIoctlParamIdIndex].Description);
                            AddToScreen(tempbuf);
                            if(IoctlParamID==LOOPBACK || IoctlParamID==NETWORK_LINE)
                            {
                                if(sel==1)
                                {
                                    sel2=ComboBox_GetCurSel(GetDlgItem(hDlg, IDC_CHOICE));
                                    Config.Value=sel2;
                                    sprintf_s(tempbuf, "Config.Value=%d;", sel2);
                                    AddToScreen(tempbuf);
                                }
                            }
                            else
                            {
                                if(sel==1)
                                {
                                    if(!GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH))
                                        break;

//                                  if(param==DATA_RATE)
                                    if(IoctlParamID==DATA_RATE || IoctlParamID==CAN_FD_DATA_RATE)
                                    {
                                        sscanf_s((char*)tempstr, "%d", &longparam);
                                        Config.Value=longparam;
                                        sprintf_s(tempbuf, "Config.Value=%d;", longparam);
                                        AddToScreen(tempbuf);
                                    }
                                    else
                                    {
                                        sscanf_s((char*)tempstr, "%X", &longparam);
                                        Config.Value=longparam;
                                        sprintf_s(tempbuf, "Config.Value=0x%X;", longparam);
                                        AddToScreen(tempbuf);
                                    }
                                }
                            }
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &ConfigList, NULL);", IoctlList[sel+1]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, sel+1, &ConfigList, NULL);
                            if(error==STATUS_NOERROR && sel==0)
                            {
                                iIoctlParamIdIndex = fnGetIoctlParamIdIndex(IoctlParamID);
                                if(IoctlParamID == DATA_RATE || IoctlParamID==CAN_FD_DATA_RATE)
                                    sprintf_s(tempbuf, "%s = %d", IoctlParamId[iIoctlParamIdIndex].Description, Config.Value);
                                else
                                    sprintf_s(tempbuf, "%s = 0x%X", IoctlParamId[iIoctlParamIdIndex].Description, Config.Value);

                                AddToScreen(tempbuf);
                            }
                            if(error !=STATUS_NOERROR && 
                                (IoctlParamID == J1962_PINS || IoctlParamID == J1939_PINS || 
                                IoctlParamID == J1708_PINS))
                            {
                                PassThruGetLastError(tempbuf);
                                AddToScreen(tempbuf);
                            }
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 2:
                            sel+=1;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            AddToScreen("");
                            sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulDeviceID, sel, NULL, &ulVBattVoltage);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
                                AddToScreen(tempbuf);
                            }
                            AddToScreen("");
                            break;
                        case 3:
                            sel+=1;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%02X", &address);       
                            sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", address);
                            AddToScreen(tempbuf);
                            AddToScreen("InputParam.NumOfBytes=1;");
                            AddToScreen("InputParam.BytePtr=szTempbuf;");
                            AddToScreen("OutputParam.NumOfBytes=2;");
                            AddToScreen("OutputParam.BytePtr=Keybytes;");
                            AddToScreen("");
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, &OutputParam);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            InputParam.NumOfBytes=1;
                            InputParam.BytePtr=&address;
                            OutputParam.BytePtr=Keybytes;
                            OutputParam.NumOfBytes=2;
                            error=PassThruIoctl(ulChannelID, sel, &InputParam, &OutputParam);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Keybyte 1: 0x%02X   Keybyte 2: 0x%02X", Keybytes[0], Keybytes[1]);
                                AddToScreen(tempbuf);
                            }
                            AddToScreen("");
                            break;
                        case 4:
                            sel+=1;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
							memset(&PassThruTempMsg, 0, sizeof(PASSTHRU_MSG));
							size = CheckAndConvertMsg((unsigned char *)tempstr, PassThruTempMsg.Data);
                            if(size>0)
                            {
                                PassThruTempMsg.DataSize=size;
                                PassThruTempMsg.ExtraDataIndex=0;
                                PassThruTempMsg.ProtocolID=iCurProtocol;

                                sprintf_s(tempbuf, "szTempMsg=%s", tempstr);
                                AddToScreen(tempbuf);
                                AddToScreen("<Convert szTempMsg from Ascii Text to actual data bytes>");
                                AddToScreen("");
                                AddToScreen("memset(&PassThruTempMsg, 0, sizeof(PASSTHRU_MSG));");
                                AddToScreen("memcpy(PassThruTempMsg.Data, szTempMsg, size);");
                                AddToScreen("PassThruTempMsg.DataSize=ulSize;");
                                AddToScreen("PassThruTempMsg.ExtraDataIndex=0;");
                                AddToScreen("PassThruTempMsg.ProtocolID=iCurProtocol;");
                                AddToScreen("");
                                sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &PassThruTempMsg, &ResultMsg);", IoctlList[sel]);
                                AddToScreen(tempbuf);
                                error=PassThruIoctl(ulChannelID, sel, &PassThruTempMsg, &ResultMsg);
                                sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                                AddToScreen(tempbuf);
                                if(error==STATUS_NOERROR)
                                {
                                    strcpy_s((char *) tempstr, sizeof(tempstr), "Response: ");
                                    for (unsigned int i=0; i<ResultMsg.DataSize; i++)
                                    {
                                        sprintf_s((char *)tempstr+strlen((char *) tempstr), sizeof(tempstr) - strlen((char *) tempstr), "%02X ", ResultMsg.Data[i]);
                                    }
                                    AddToScreen((char*)tempstr);
                                    sprintf_s((char*)tempstr, sizeof(tempstr), "Response Length: %d", ResultMsg.DataSize);
                                    AddToScreen((char*)tempstr);
                                }
                                AddToScreen("");
                            }
                            break;
                        case 10:
                        case 11:
                            sel+=2;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%02X", &address);           
                            sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", address);
                            AddToScreen(tempbuf);
                            AddToScreen("InputParam.NumOfBytes=1;");
                            AddToScreen("InputParam.BytePtr=szTempbuf;");
                            AddToScreen("");
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            InputParam.NumOfBytes=1;
                            InputParam.BytePtr=&address;
                            error=PassThruIoctl(ulChannelID, sel, &InputParam, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 12:
                            sel+=2;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            AddToScreen("");
                            sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulDeviceID, sel, NULL, &ulVBattVoltage);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
                                AddToScreen(tempbuf);
                            }
                            AddToScreen("");
                            break;
                        case 13:
                            sel+=2;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            size=CheckAndConvertMsg(tempstr, tempmsg);
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, &InputParam, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            InputParam.NumOfBytes=size;
                            InputParam.BytePtr=tempmsg;
                            error=PassThruIoctl(ulChannelID, PROTECT_J1939_ADDR, &InputParam, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 14:
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%08X", &Param.Value);
                            param=ComboBox_GetCurSel(hCombo2);
                            ComboBox_GetLBText(hCombo2, param, chTempstr1);
                            IoctlGetDeviceInfoParamID = fnGetIoctlGetDeviceInfoParamId(chTempstr1);
                            iIoctlGetDeviceInfoParamIdIndex = fnGetIoctlGetDeviceInfoParamIdIndex(IoctlGetDeviceInfoParamID);

                            ParamList.NumOfParams=1;
                            ParamList.ParamPtr=&Param;
                            Param.Parameter=IoctlGetDeviceInfoParamID;
                            Param.Value=0;
                            AddToScreen("ParamList.NumOfParams=1;");
                            AddToScreen("ParamList.pParamPtr=&Param;");
                            sprintf_s(tempbuf, "Param.Parameter=%s;", IoctlGetDeviceInfoParamId[iIoctlGetDeviceInfoParamIdIndex].Description);
                            AddToScreen(tempbuf);
                            sprintf_s(tempbuf, "Param.Value=%08X;", Param.Value);
                            AddToScreen(tempbuf);

                            sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, NULL, &ParamList);", IoctlList[sel+2]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulDeviceID, GET_DEVICE_INFO, NULL, &ParamList);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Param.Value=%08X;", Param.Value);
                                AddToScreen(tempbuf);
                                sprintf_s(tempbuf, "Param.Supported=%08X;", Param.Supported);
                                AddToScreen(tempbuf);
                            }
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                       case 15:
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%08X", &Param.Value);
                            param=ComboBox_GetCurSel(hCombo2);
                            ComboBox_GetLBText(hCombo2, param, chTempstr1);
                            IoctlGetProtocolInfoParamID = fnGetIoctlGetProtocolInfoParamId(chTempstr1);
                            iIoctlGetProtocolInfoParamIdIndex = fnGetIoctlGetProtocolInfoParamIdIndex(IoctlGetProtocolInfoParamID);

                            ParamList.NumOfParams=1;
                            ParamList.ParamPtr=&Param;
                            Param.Parameter=IoctlGetProtocolInfoParamID;
                            //Param.Value=0;
                            AddToScreen("ParamList.NumOfParams=1;");
                            AddToScreen("ParamList.pParamPtr=&Param;");
                            sprintf_s(tempbuf, "Param.Parameter=%s;", IoctlGetProtocolInfoParamId[iIoctlGetProtocolInfoParamIdIndex].Description);
                            AddToScreen(tempbuf);
                            sprintf_s(tempbuf, "Param.Value=%08X;", Param.Value);
                            AddToScreen(tempbuf);
                            
                            unsigned long ulGetProtocolInfoID;
                            GetDlgItemText(hDlg, IDC_PROTOCOL, (char*)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%08X", &ulGetProtocolInfoID);
                            sprintf_s(tempbuf, "Protocol=%08X;", ulGetProtocolInfoID);
                            AddToScreen(tempbuf);

                            sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &ulGetProtocolInfoID, &ParamList);", IoctlList[sel+2]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulDeviceID, GET_PROTOCOL_INFO, &ulGetProtocolInfoID, &ParamList);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Param.Value=%08X;", Param.Value);
                                AddToScreen(tempbuf);
                                sprintf_s(tempbuf, "Param.Supported=%08X;", Param.Supported);
                                AddToScreen(tempbuf);
                            }
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 16:
                            sel+=2;
                            GetDlgItemText(hDlg, IDC_INT, (char *)tempstr, MAX_STRING_LENGTH);
                            sscanf_s((char*)tempstr, "%02X %02X", Keybytes, Keybytes+1);          
                            sprintf_s(tempbuf, "szTempbuf[0]=0x%02X;", Keybytes[0]);
                            AddToScreen(tempbuf);
                            sprintf_s(tempbuf, "szTempbuf[1]=0x%02X;", Keybytes[1]);
                            AddToScreen(tempbuf);
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, szTempbuf, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, 0x10100, Keybytes, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            
                            break;
                        case 17:
                            sel+=1;
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            AddToScreen("");
                            sprintf_s(tempbuf, "PassThruIoctl(ulDeviceID, %s, &InputParam, &OutputParam);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulDeviceID, 0x10007, NULL, &ulVBattVoltage);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            if(error==STATUS_NOERROR)
                            {
                                sprintf_s(tempbuf, "Voltage Read: %lu mV", ulVBattVoltage);
                                AddToScreen(tempbuf);
                            }
                            AddToScreen("");
                            break;
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                        case 30:
                            GetDlgItemText(hDlg, IDC_INT, (char*)tempstr, MAX_STRING_LENGTH);
                            size=CheckAndConvertMsg(tempstr, tempmsg);
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, NULL, NULL);", IoctlList[23+sel-15]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, LIN_SENDWAKEUP+sel-15, tempmsg, &size);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 31:
                            sel+=8;
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, NULL, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, SWCAN_HS, NULL, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                            break;
                        case 32:
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, NULL, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, SWCAN_NS, NULL, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");       
                            break;
                        default:
                            sel+=2;
                            sprintf_s(tempbuf, "PassThruIoctl(ulChannelID, %s, NULL, NULL);", IoctlList[sel]);
                            AddToScreen(tempbuf);
                            error=PassThruIoctl(ulChannelID, sel, NULL, NULL);
                            sprintf_s(tempbuf, "Function returned: %s", ErrorList[error]);
                            AddToScreen(tempbuf);
                            AddToScreen("");
                    }
//                  break;
                case IDCANCEL:
                    EndDialog(hDlg, LOWORD(wParam));
                break;
            }
            return TRUE;
            break;
        default:
            return FALSE;
    }
    return TRUE;
}
#endif
//-----------------------------------------------------------------------------
//  Function Name   : FindJ2534Entry()
//  Input Params    : 
//  Output Params   : 
//  Return          : 
//  Description     : This is a function to find the J2534 device entry in
//                    the registry.
//-----------------------------------------------------------------------------
bool FindJ2534Entry(char* pchCurText)
{
    long            lSuccess;
    char            chTempstr[J2534REGISTRY_MAX_STRING_LENGTH]; 
    DWORD           dwDatasize;     
    unsigned char   ucValueread[J2534REGISTRY_MAX_STRING_LENGTH];
    DWORD           dwDword, dwString; 
    HKEY            hMainkey;
    HKEY            hTempkey;//, hTempkey2;         // handle for key
    HKEY            *phTempkey;
    int             i, iHighdevice, iHighvendor;

    dwDword = REG_DWORD, dwString = REG_SZ;
    i=0, iHighdevice=1, iHighvendor=1;
    hTempkey = NULL;
    phTempkey = NULL;

	/*Block is required to check Windows OS > 10 and < 10*/
	strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH);
	lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0, KEY_ALL_ACCESS, &hMainkey);
	if (lSuccess != ERROR_SUCCESS)
	{
		strcpy_s(chTempstr, J2534REGISTRY_KEY_PATH_6432);
		lSuccess = RegOpenKeyEx(HKEY_LOCAL_MACHINE, chTempstr, 0, KEY_ALL_ACCESS, &hMainkey);
	}

    if(lSuccess==ERROR_SUCCESS) 
    {
        i=0;
        dwDatasize=J2534REGISTRY_MAX_STRING_LENGTH;
        lSuccess = RegOpenKeyEx(hMainkey, pchCurText, 
                0, KEY_READ, &hTempkey);
        if (lSuccess==ERROR_SUCCESS)
        {
            dwDatasize = sizeof(ucValueread);
            lSuccess = RegQueryValueEx(hTempkey, "FunctionLibrary", NULL, &dwString, ucValueread, &dwDatasize);
        
            if(lSuccess==ERROR_SUCCESS)
            {
                // Found DLL to load
                sprintf_s(chFuncLib, "%s", ucValueread);
            }
            RegCloseKey(hTempkey);
            hTempkey = NULL;
        }
    }
    RegCloseKey(hMainkey);
    return true;
    if (phTempkey != NULL)
    {
        RegCloseKey(*phTempkey);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

BOOL LoadFunctions(char* chTempstr)
{
    char chFuncIni[256];
    memset(chFuncIni, 0, 256);
    memset(chFuncLib, 0, 256);

    if (!FindJ2534Entry(chTempstr))
    {
        return(FALSE);
    }
    if ((hDLL = LoadLibrary(chFuncLib)) == NULL)
    {
        DWORD temp=GetLastError();
        return(FALSE);
    }
    if ((PassThruOpen = (PTOPEN)GetProcAddress(hDLL, "PassThruOpen")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruClose = (PTCLOSE)GetProcAddress(hDLL, "PassThruClose")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruConnect = (PTCONNECT)GetProcAddress(hDLL, "PassThruConnect")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruDisconnect = (PTDISCONNECT)GetProcAddress(hDLL, "PassThruDisconnect")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruReadMsgs = (PTREADMSGS)GetProcAddress(hDLL, "PassThruReadMsgs")) == NULL)
    {
        return(FALSE);
    }
#ifndef J2534_0500
    if ((PassThruWriteMsgs = (PTWRITEMSGS)GetProcAddress(hDLL, "PassThruWriteMsgs")) == NULL)
    {
        return(FALSE);
    }
#endif
    if ((PassThruStartPeriodicMsg = (PTSTARTPERIODICMSG)GetProcAddress(hDLL, "PassThruStartPeriodicMsg")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruStopPeriodicMsg = (PTSTOPPERIODICMSG)GetProcAddress(hDLL, "PassThruStopPeriodicMsg")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruStartMsgFilter = (PTSTARTMSGFILTER)GetProcAddress(hDLL, "PassThruStartMsgFilter")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruStopMsgFilter = (PTSTOPMSGFILTER)GetProcAddress(hDLL, "PassThruStopMsgFilter")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruSetProgrammingVoltage = (PTSETPROGRAMMINGVOLTAGE)GetProcAddress(hDLL, "PassThruSetProgrammingVoltage")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruReadVersion = (PTREADVERSION)GetProcAddress(hDLL, "PassThruReadVersion")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruGetLastError = (PTGETLASTERROR)GetProcAddress(hDLL, "PassThruGetLastError")) == NULL)
    {
        return(FALSE);
    }
    if ((PassThruIoctl = (PTIOCTL)GetProcAddress(hDLL, "PassThruIoctl")) == NULL)
    {
        return(FALSE);
    }

#ifdef J2534_0500

	if ((PassThruQueueMsgs = (PTQUEUEMSG)GetProcAddress(hDLL, "PassThruQueueMsgs")) == NULL)
	{
		return(FALSE);
	}
	if ((PassThruScanForDevices = (PTSCANFORDV)GetProcAddress(hDLL, "PassThruScanForDevices")) == NULL)
	{
		return(FALSE);
	}
	if ((PassThruGetNextDevice = (PTGETNEXTDEV)GetProcAddress(hDLL, "PassThruGetNextDevice")) == NULL)
	{
		return(FALSE);
	}
	if ((PassThruLogicalConnect = (PTLOGCONNECT)GetProcAddress(hDLL, "PassThruLogicalConnect")) == NULL)
	{
		return(FALSE);
	}
	if ((PassThruLogicalDisconnect = (PTLOGDISCONN)GetProcAddress(hDLL, "PassThruLogicalDisconnect")) == NULL)
	{
		return(FALSE);
	}
	if ((PassThruSelect = (PTSELECT)GetProcAddress(hDLL, "PassThruSelect")) == NULL)
	{
		return(FALSE);
	}
#endif 

	GetWindowsDirectory((LPSTR)path, sizeof(path) - 13);

    if (path[strlen(path)-1] != '\\')
    {
        strcat_s(path, "\\");
    }
    strcat_s(path, "J2534SDK.ini");

    for(int i=0; i<10; i++)
    {
        sprintf_s((char *)tempbuf, sizeof(tempbuf), "Msg%d", i);
        GetPrivateProfileString("Previous Msgs", tempbuf, NULL, (char*)lastmsgs[i], 16384, path);
    }

    return TRUE;
}

int CheckAndConvertMsg(unsigned char *in, unsigned char *out)
{
    unsigned int i,j=0;

    if(strlen((char*)in)<=0)
        return -1;

    for(i=0; i<strlen((char*)in);)
    {
        while(in[i]==' ')
            i++;

        if(i>=strlen((char*)in))
            break;

        if(in[i]<='9' && in[i]>='0')
            out[j]=in[i]-'0';
        else if (toupper(in[i])>='A' && toupper(in[i])<='F')
            out[j]=toupper(in[i])-'A'+10;
        else
            out[j]=0;

        i++;
        if(in[i]<='9' && in[i]>='0')
        {
            out[j]=out[j]<<4;
            out[j]+=in[i]-'0';
        }
        else if (toupper(in[i])>='A' && toupper(in[i])<='F')
        {
            out[j]=out[j]<<4;
            out[j]+=toupper(in[i])-'A'+10;
        }
        j++;
        i++;
    }

    in[0]=0;
    for(i=0; i<j; i++)
    {
        sprintf_s((char*)in+strlen((char*)in), MAX_STRING_LENGTH - +strlen((char*)in), "%02X ", out[i]);
    }
    return j;
}

J2534_PROTOCOL fnGetProtocol(char *strProtocol)
{
    int iProtocolIndex;
    if (strProtocol[0] == 0)
        return (J2534_PROTOCOL)0;   // Back at-ya with the same - invalid

    for (iProtocolIndex = 0; iProtocolIndex < PROTOCOL_ENTRY_COUNT; iProtocolIndex++)
    {
        if (strcmp(strProtocol, Protocols[iProtocolIndex].Description) == 0)
        {
            // We found our Protocol, iProtocolIndex is set
            break;

        }
    }
    if (iProtocolIndex >= PROTOCOL_ENTRY_COUNT)
    {   // Failed to find our Protocol
        iProtocolIndex = (J2534_PROTOCOL)0; // ERROR - defaults to a valid protocol index - things just won't work right
    }
    return Protocols[iProtocolIndex].J2534ProtocolID;
}

int fnGetProtocolIndex(long enProtocolID)
{
    int iProtocolIndex;
    if (enProtocolID == 0)
        return 0;   // Back at-ya with the same - invalid

    for (iProtocolIndex = 0; iProtocolIndex < PROTOCOL_ENTRY_COUNT; iProtocolIndex++)
    {
        if (Protocols[iProtocolIndex].J2534ProtocolID == enProtocolID)
        {
            // We found our Protocol, iProtocolIndex is set
            break;

        }
    }
    if (iProtocolIndex >= PROTOCOL_ENTRY_COUNT)
    {   // Failed to find our Protocol
        iProtocolIndex = 0; // ERROR - defaults to a valid protocol index - things just won't work right
    }
    return iProtocolIndex;
}

J2534IOCTLPARAMID fnGetIoctlParamId(char *strIoctlParamId)
{
    int iIoctlParamIdIndex;
    if (strIoctlParamId[0] == 0)
        return (J2534IOCTLPARAMID)0;   // Back at-ya with the same - invalid

    for (iIoctlParamIdIndex = 0; iIoctlParamIdIndex < IOCTL_PARAM_ENTRY_COUNT; iIoctlParamIdIndex++)
    {
        if (strcmp(strIoctlParamId, IoctlParamId[iIoctlParamIdIndex].Description) == 0)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlParamIdIndex >= IOCTL_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlParamIdIndex = (J2534IOCTLPARAMID)0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return IoctlParamId[iIoctlParamIdIndex].IoctlParamID;
}

int fnGetIoctlParamIdIndex(long enIoctlParamIdID)
{
    int iIoctlParamIdIndex;
    if (enIoctlParamIdID == 0)
        return 0;   // Back at-ya with the same - invalid

    for (iIoctlParamIdIndex = 0; iIoctlParamIdIndex < IOCTL_PARAM_ENTRY_COUNT; iIoctlParamIdIndex++)
    {
        if (IoctlParamId[iIoctlParamIdIndex].IoctlParamID == enIoctlParamIdID)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlParamIdIndex >= IOCTL_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlParamIdIndex = 0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return iIoctlParamIdIndex;
}

J2534GETDEVICEINFOPARAMID fnGetIoctlGetDeviceInfoParamId(char *strIoctlGetDeviceInfoParamId)
{
    int iIoctlGetDeviceInfoParamIdIndex;
    if (strIoctlGetDeviceInfoParamId[0] == 0)
        return (J2534GETDEVICEINFOPARAMID)0;   // Back at-ya with the same - invalid

    for (iIoctlGetDeviceInfoParamIdIndex = 0; iIoctlGetDeviceInfoParamIdIndex < IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT; 
        iIoctlGetDeviceInfoParamIdIndex++)
    {
        if (strcmp(strIoctlGetDeviceInfoParamId, IoctlGetDeviceInfoParamId[iIoctlGetDeviceInfoParamIdIndex].Description) == 0)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlGetDeviceInfoParamIdIndex >= IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlGetDeviceInfoParamIdIndex = (J2534GETDEVICEINFOPARAMID)0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return IoctlGetDeviceInfoParamId[iIoctlGetDeviceInfoParamIdIndex].IoctlGetDeviceInfoParamID;
}

int fnGetIoctlGetDeviceInfoParamIdIndex(long enIoctlGetDeviceInfoParamIdID)
{
    int iIoctlGetDeviceInfoParamIdIndex;
    if (enIoctlGetDeviceInfoParamIdID == 0)
        return 0;   // Back at-ya with the same - invalid

    for (iIoctlGetDeviceInfoParamIdIndex = 0; iIoctlGetDeviceInfoParamIdIndex < IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT; 
        iIoctlGetDeviceInfoParamIdIndex++)
    {
        if (IoctlGetDeviceInfoParamId[iIoctlGetDeviceInfoParamIdIndex].IoctlGetDeviceInfoParamID == enIoctlGetDeviceInfoParamIdID)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlGetDeviceInfoParamIdIndex >= IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlGetDeviceInfoParamIdIndex = 0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return iIoctlGetDeviceInfoParamIdIndex;
}        

J2534GETPROTOCOLINFOPARAMID fnGetIoctlGetProtocolInfoParamId(char *strIoctlGetProtocolInfoParamId)
{
    int iIoctlGetProtocolInfoParamIdIndex;
    if (strIoctlGetProtocolInfoParamId[0] == 0)
        return (J2534GETPROTOCOLINFOPARAMID)0;   // Back at-ya with the same - invalid

    for (iIoctlGetProtocolInfoParamIdIndex = 0; iIoctlGetProtocolInfoParamIdIndex < IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT; 
        iIoctlGetProtocolInfoParamIdIndex++)
    {
        if (strcmp(strIoctlGetProtocolInfoParamId, IoctlGetProtocolInfoParamId[iIoctlGetProtocolInfoParamIdIndex].Description) == 0)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlGetProtocolInfoParamIdIndex >= IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlGetProtocolInfoParamIdIndex = (J2534GETPROTOCOLINFOPARAMID)0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return IoctlGetProtocolInfoParamId[iIoctlGetProtocolInfoParamIdIndex].IoctlGetProtocolInfoParamID;
}

int fnGetIoctlGetProtocolInfoParamIdIndex(long enIoctlGetProtocolInfoParamIdID)
{
    int iIoctlGetProtocolInfoParamIdIndex;
    if (enIoctlGetProtocolInfoParamIdID == 0)
        return 0;   // Back at-ya with the same - invalid

    for (iIoctlGetProtocolInfoParamIdIndex = 0; iIoctlGetProtocolInfoParamIdIndex < IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT; 
        iIoctlGetProtocolInfoParamIdIndex++)
    {
        if (IoctlGetProtocolInfoParamId[iIoctlGetProtocolInfoParamIdIndex].IoctlGetProtocolInfoParamID == enIoctlGetProtocolInfoParamIdID)
        {
            // We found our IoctlParamId, iIoctlParamIdIndex is set
            break;

        }
    }
    if (iIoctlGetProtocolInfoParamIdIndex >= IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT)
    {   // Failed to find our IoctlParamId
        iIoctlGetProtocolInfoParamIdIndex = 0; // ERROR - defaults to a valid Ioctl Parameter Id index - things just won't work right
    }
    return iIoctlGetProtocolInfoParamIdIndex;
}

J2534ERROR TestVSI4PinSwitchSupported(bool *bSupportPinSwitchCANProtocols)
{
    char firmware[MAX_STRING_LENGTH], dll[MAX_STRING_LENGTH], api[MAX_STRING_LENGTH];
    J2534ERROR Status = (J2534ERROR) PassThruReadVersion(ulDeviceID, firmware, dll, api);

    // parse string returned by checkDataLink() to determine h/w version
	if (Status == STATUS_NOERROR && memcmp(UpgradedVersion, firmware, strlen(UpgradedVersion)) == 0)
	{
		*bSupportPinSwitchCANProtocols = true;
	}
	else
	{
		*bSupportPinSwitchCANProtocols = false;
	}
	return Status;
}
