
#include "j2534.h"

void Do_PASSTHRUOPEN();
void Do_PASSTHRUCONNECT();
void Do_PASSTHRUDISCONNECT();
void Do_PASSTHRUREADMSGS();
void Do_PASSTHRUWRITEMSGS();
void Do_PASSTHRUSTARTPERIODICMSG();
void Do_PASSTHRUSTOPPERIODICMSG();
void Do_PASSTHRUSTARTMSGFILTER();
void Do_PASSTHRUSTOPMSGFILTER();
void Do_PASSTHRUSETPROGRAMMINGVOLTAGE();
void Do_PASSTHRUREADVERSION();
void Do_PASSTHRUGETLASTERROR();
void Do_PASSTHRUIOCTL();
void Do_PASSTHRUOPEN();
void Do_PASSTHRUCLOSE();

#ifdef J2534_0500
void Do_PASSTHRQUEUEMSGS();
void Do_PASSTHRSCANFORDEVICES();
void Do_PASSTHRUGETNEXTDEVICE();
void Do_PASSTHRULOGICALCONNECT();
void Do_PASSTHRULOGICALDISCONNECT();
void Do_PASSTHRUSELECT();
#endif

void AddToScreen(char *);
BOOL LoadFunctions(char* chTempstr);

LRESULT CALLBACK MainDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK OpenProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ConnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ReadMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WriteMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK StartPeriodicProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK StopPeriodicProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK StartFilterProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK StopFilterProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK DisconnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
#ifdef J2534_0404
LRESULT CALLBACK IoctlProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
#endif
LRESULT CALLBACK SetProgrammingVoltageProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);


#ifdef J2534_0500
LRESULT CALLBACK LogicalConnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK QueueMsgsProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK LogicalDisconnectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK Ioctl0500Proc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK PassThruSelectProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM);
#endif

#ifdef J2534_0500
char* ErrorList[] = 
{
		"STATUS_NOERROR",             // Function call successful.
		"ERR_NOT_SUPPORTED",            // Function not supported.
		"ERR_INVALID_CHANNEL_ID",       // Invalid ChannelID value.
		"ERR_INVALID_PROTOCOL_ID",      // Invalid ProtocolID value.
		"ERR_NULLPARAMETER",            // NULL pointer supplied where a valid 
		// pointer
		// is required.
		"ERR_INVALID_IOCTL_VALUE",      // Invalid value for Ioctl parameter 
		"ERR_INVALID_FLAGS",            // Invalid flag values.
		"ERR_FAILED",                   // Undefined error, use 
		// PassThruGetLastError for description
		// of error.
		"ERR_DEVICE_NOT_CONNECTED",     // Unable to communicate with device
		"ERR_TIMEOUT",                  // Timeout. No message available to 
		// read or 
		// could not read the specified no. of 
		// msgs.
		"ERR_INVALID_MSG",              // Invalid message structure pointed 
		// to by pMsg.
		"ERR_INVALID_TIME_INTERVAL",    // Invalid TimeInterval value.
		"ERR_EXCEEDED_LIMIT",           // ALL periodic message IDs have been 
		// used.
		"ERR_INVALID_MSG_ID",           // Invalid MsgID value.
		"ERR_DEVICE_IN_USE",            // Device already open and/or in use
		"ERR_INVALID_IOCTL_ID",         // Invalid IoctlID value.
		"ERR_BUFFER_EMPTY",             // Protocol message buffer empty.
		"ERR_BUFFER_FULL",              // Protocol message buffer full.
		"ERR_BUFFER_OVERFLOW",          // Protocol message buffer overflow.
		"ERR_PIN_INVALID",              // Invalid pin number.
		"ERR_CHANNEL_IN_USE",           // Channel already in use.
		"ERR_MSG_PROTOCOL_ID",          // Protocol type does not match the
		// protocol associated with the 
		// Channel ID
		"ERR_INVALID_FILTER_ID",        // Invalid MsgID value
		"ERR_NO_FLOW_CONTROL",          // An attempt was made to send a message
		// on an ISO15765 ChannelID before a flow
		// control filter was established.  
		"ERR_NOT_UNIQUE",               // A CAN ID in PatternMsg or FlowControlMsg
		// matches either ID in an existing Flow Control Filter
		"ERR_INVALID_BAUDRATE",         // Desired baud rate cannot be achieved within tolerances           
		"ERR_INVALID_DEVICE_ID",        // Device ID Invalid    
		"ERR_PIN_NOT_SUPPORTED",
		"ERR_FILTER_TYPE_NOT_SUPPORTED",
		"ERR_DEVICE_NOT_OPEN",
		"ERR_LOG_CHAN_NOT_ALLOWED",
		"ERR_MSG_NOT_ALLOWED",
		"ERR_ADDRESS_NOT_CLAIMED",
		"ERR_NO_CONNECTION_ESTABLISHED"
};
#endif

#ifndef J2534_0500
char* ErrorList[] =
{
		"STATUS_NOERROR",             // Function call successful.
		"ERR_NOT_SUPPORTED",            // Function not supported.
		"ERR_INVALID_CHANNEL_ID",       // Invalid ChannelID value.
		"ERR_INVALID_PROTOCOL_ID",      // Invalid ProtocolID value.
		"ERR_NULLPARAMETER",            // NULL pointer supplied where a valid 
		// pointer
		// is required.
		"ERR_INVALID_IOCTL_VALUE",      // Invalid value for Ioctl parameter 
		"ERR_INVALID_FLAGS",            // Invalid flag values.
		"ERR_FAILED",                   // Undefined error, use 
		// PassThruGetLastError for description
		// of error.
		"ERR_DEVICE_NOT_CONNECTED",     // Unable to communicate with device
		"ERR_TIMEOUT",                  // Timeout. No message available to 
		// read or 
		// could not read the specified no. of 
		// msgs.
		"ERR_INVALID_MSG",              // Invalid message structure pointed 
		// to by pMsg.
		"ERR_INVALID_TIME_INTERVAL",    // Invalid TimeInterval value.
		"ERR_EXCEEDED_LIMIT",           // ALL periodic message IDs have been 
		// used.
		"ERR_INVALID_MSG_ID",           // Invalid MsgID value.
		"ERR_DEVICE_IN_USE",            // Device already open and/or in use
		"ERR_INVALID_IOCTL_ID",         // Invalid IoctlID value.
		"ERR_BUFFER_EMPTY",             // Protocol message buffer empty.
		"ERR_BUFFER_FULL",              // Protocol message buffer full.
		"ERR_BUFFER_OVERFLOW",          // Protocol message buffer overflow.
		"ERR_PIN_INVALID",              // Invalid pin number.
		"ERR_CHANNEL_IN_USE",           // Channel already in use.
		"ERR_MSG_PROTOCOL_ID",          // Protocol type does not match the
		// protocol associated with the 
		// Channel ID
		"ERR_INVALID_FILTER_ID",        // Invalid MsgID value
		"ERR_NO_FLOW_CONTROL",          // An attempt was made to send a message
		// on an ISO15765 ChannelID before a flow
		// control filter was established.  
		"ERR_NOT_UNIQUE",               // A CAN ID in PatternMsg or FlowControlMsg
		// matches either ID in an existing Flow Control Filter
		"ERR_INVALID_BAUDRATE",         // Desired baud rate cannot be achieved within tolerances           
		"ERR_INVALID_DEVICE_ID",        // Device ID Invalid                                       
		"ERR_ADDRESS_NOT_CLAIMED",
		"ERR_NO_CONNECTION_ESTABLISHED"
}; 
#endif
#ifdef J2534_0500
char* IoctlList_0500[] = {	"",
						"GET_CONFIG",
						"SET_CONFIG",
						"READ_PIN_VOLTAGE",
						"FIVE_BAUD_INIT",
						"FAST_INIT",
	                    "SET_PIN_USE",
						"CLEAR_TX_QUEUE",
						"CLEAR_RX_QUEUE",
						"CLEAR_PERIODIC_MSGS",
						"CLEAR_MSG_FILTERS",
						"CLEAR_FUNCT_MSG_LOOKUP_TABLE",
						"ADD_TO_FUNCT_MSG_LOOKUP_TABLE",
						"DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE",
						"READ_PROG_VOLTAGE",
						"BUS_ON"};
#endif
#ifndef J2534_0500
char* IoctlList[] = {   "",
                        "GET_CONFIG",
                        "SET_CONFIG",
                        "READ_VBATT",
                        "FIVE_BAUD_INIT",
                        "FAST_INIT",
                        "SET_PIN_USE",
                        "CLEAR_TX_BUFFER",
                        "CLEAR_RX_BUFFER",
                        "CLEAR_PERIODIC_MSGS",
                        "CLEAR_MSG_FILTERS",
                        "CLEAR_FUNCT_MSG_LOOKUP_TABLE",
                        "ADD_TO_FUNCT_MSG_LOOKUP_TABLE",
                        "DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE",
                        "READ_PROG_VOLTAGE",
                        "PROTECT_J1939_ADDR",
                        "GET_DEVICE_INFO",
                        "GET_PROTOCOL_INFO",
                        "CAN_SET_BTR", 
                        "GET_TIMESTAMP",
                        "SET_TIMESTAMP",
                        "GET_SERIAL_NUMBER",
                        "CAN_SET_LISTEN_MODE",
                        "CAN_SET_ERROR_REPORTING",
                        "CAN_SET_TRANSMIT_MODE",
                        "CAN_SET_INTERNAL_TERMINATION",
                        "LIN_SENDWAKEUP",
                        "LIN_ADD_SCHED",
                        "LIN_GET_SCHED",
                        "LIN_GET_SCHED_SIZE",
                        "LIN_DEL_SCHED",
                        "LIN_ACT_SCHED",
                        "LIN_DEACT_SCHED",
                        "LIN_GET_ACT_SCHED",
                        "LIN_GET_NUM_SCHEDS",
                        "LIN_GET_SCHED_NAMES",
                        "LIN_SET_FLAGS",
                        "RESETHC08",
                        "TESTHC08COP",
                        "SWCAN_HS",
	                    "SWCAN_NS"};
#endif
#ifndef J2534_0500
char* IoctlParamList[] = {  "",
                            "DATA_RATE",
                            "LOOPBACK",
                            "NODE_ADDRESS",
                            "NETWORK_LINE",
                            "P1_MIN",
                            "P1_MAX",
                            "P2_MIN",
                            "P2_MAX",
                            "P3_MIN",
                            "P3_MAX",
                            "P4_MIN",
                            "P4_MAX",
                            "W1",
                            "W2",
                            "W3",
                            "W4",
                            "W5",
                            "TIDLE",
                            "TINIL",
                            "TWUP",
                            "PARITY",
                            "BIT_SAMPLE_POINT",
                            "SYNC_JUMP_WIDTH",
                            "W0",
                            "T1_MAX",
                            "T2_MAX",
                            "T4_MAX",
                            "T5_MAX",
                            "ISO15765_BS",
                            "ISO15765_STMIN",
                            "DATA_BITS",
                            "FIVE_BAUD_MOD",
                            "BS_TX",
                            "STMIN_TX",
                            "T3_MAX",
                            "ISO15765_WFT_MAX",
                            "LIN_BRKSPACE",
	                        "LIN_BRKMARK",
	                        "LIN_IDDELAY",
	                        "LIN_RESPDELAY",
	                        "LIN_INTERBYTE",
	                        "LIN_WAKEUPDELAY",
	                        "LIN_WAKEUPTIMEOUT",
	                        "LIN_WAKEUPTIMEOUT3BRK",
	                        "LIN_MODE",
	                        "LIN_SLEW"};
#endif
#ifdef J2534_0500
char* IoctlParamList[] = {  "",
							"DATA_RATE",
							"NODE_ADDRESS",
							"NETWORK_LINE",
							"P1_MIN",
							"P1_MAX",
							"P2_MIN",
							"P2_MAX",
							"P3_MIN",
							"P3_MAX",
							"P4_MIN",
							"P4_MAX",
							"W1_MAX",
							"W2_MAX",
							"W3_MAX",
							"W4_MIN",
							"W5_MIN",
							"TIDLE",
							"TINIL",
							"TWUP",
							"PARITY",
							"W0_MIN",
							"T1_MAX",
							"T2_MAX",
							"T4_MAX",
							"T5_MAX",
							"ISO15765_BS",
							"ISO15765_STMIN",
							"DATA_BITS",
							"FIVE_BAUD_MOD",
							"BS_TX",
							"STMIN_TX",
							"T3_MAX",
							"ISO15765_WAIT_LIMIT",
							"W1_MIN",
							"W2_MIN",
							"W3_MIN",
							"W4_MAX",
							"N_BR_MIN",
							"ISO15765_PAD_VALUE",
							"N_AS_MAX",
							"N_AR_MAX",
							"N_BS_MAX",
							"N_CR_MAX",
							"N_CS_MIN"};
#endif

