typedef long (CALLBACK* PTOPEN)(void *, unsigned long *);
typedef long (CALLBACK* PTCLOSE)(unsigned long);
typedef long (CALLBACK* PTDISCONNECT)(unsigned long);
typedef long (CALLBACK* PTREADMSGS)(unsigned long, void *, unsigned long *, unsigned long);
typedef long (CALLBACK* PTWRITEMSGS)(unsigned long, void *, unsigned long *, unsigned long);
typedef long (CALLBACK* PTSTARTPERIODICMSG)(unsigned long, void *, unsigned long *, unsigned long);
typedef long (CALLBACK* PTSTOPPERIODICMSG)(unsigned long, unsigned long);
typedef long (CALLBACK* PTSTOPMSGFILTER)(unsigned long, unsigned long);
typedef long (CALLBACK* PTREADVERSION)(unsigned long, char *, char *, char *);
typedef long (CALLBACK* PTGETLASTERROR)(char *);
typedef long (CALLBACK* PTGETLASTERRORTEXT)(char *);
typedef long (CALLBACK* PTIOCTL)(unsigned long, unsigned long, void *, void *);
typedef long (CALLBACK* PTLOADFIRMWARE)(void);
typedef long (CALLBACK* PTRECOVERFIRMWARE)(void);
typedef long (CALLBACK* PTREADIPSETUP)(char *, char *, char *, char *, char *);
typedef long (CALLBACK* PTWRITEIPSETUP)(char *, char *, char *, char *, char *);
typedef long (CALLBACK* PTREADPCSETUP)(char *, char *);

#if defined (J2534_0404) || defined (J2534_0305)
typedef long (CALLBACK* PTCONNECT)(unsigned long, unsigned long, unsigned long, unsigned long, unsigned long *);
typedef long (CALLBACK* PTSTARTMSGFILTER)(unsigned long, unsigned long, void *, void *, void *,unsigned long *);
typedef long (CALLBACK* PTSETPROGRAMMINGVOLTAGE)(unsigned long, unsigned long, unsigned long);
#endif

#ifdef J2534_0500
typedef long (CALLBACK* PTCONNECT)(unsigned long, unsigned long, unsigned long,unsigned long, RESOURCE_STRUCT, unsigned long *);
typedef long (CALLBACK* PTSTARTMSGFILTER)(unsigned long, unsigned long, void *, void *, unsigned long *);
typedef long (CALLBACK* PTSETPROGRAMMINGVOLTAGE)(unsigned long, RESOURCE_STRUCT, unsigned long);
typedef long (CALLBACK* PTQUEUEMSG)(unsigned long, void *, unsigned long *);
typedef long (CALLBACK* PTSCANFORDV)(unsigned long *);
typedef long (CALLBACK* PTGETNEXTDEV)(void *);
typedef long (CALLBACK* PTLOGCONNECT)(unsigned long, unsigned long, unsigned long, void *, unsigned long *);
typedef long (CALLBACK* PTLOGDISCONN)(unsigned long);
typedef long (CALLBACK* PTSELECT)(void *, unsigned long, unsigned long);
#endif 

PTOPEN  PassThruOpen;
PTCLOSE PassThruClose;
PTCONNECT PassThruConnect;
PTDISCONNECT PassThruDisconnect;
PTREADMSGS PassThruReadMsgs;
PTWRITEMSGS PassThruWriteMsgs;
PTSTARTPERIODICMSG PassThruStartPeriodicMsg;
PTSTOPPERIODICMSG PassThruStopPeriodicMsg;
PTSTARTMSGFILTER PassThruStartMsgFilter;
PTSTOPMSGFILTER PassThruStopMsgFilter;
PTSETPROGRAMMINGVOLTAGE PassThruSetProgrammingVoltage;
PTREADVERSION PassThruReadVersion;
PTGETLASTERROR PassThruGetLastError;
PTGETLASTERRORTEXT PassThruGetLastErrorText;
PTIOCTL PassThruIoctl;
PTLOADFIRMWARE PassThruLoadFirmware;
PTRECOVERFIRMWARE PassThruRecoverFirmware;
PTREADIPSETUP PassThruReadIPSetup;
PTWRITEIPSETUP PassThruWriteIPSetup;
PTREADPCSETUP PassThruReadPCSetup;

#ifdef J2534_0500
PTQUEUEMSG	  PassThruQueueMsgs;
PTSCANFORDV	  PassThruScanForDevices;
PTGETNEXTDEV  PassThruGetNextDevice;
PTLOGCONNECT  PassThruLogicalConnect;
PTLOGDISCONN  PassThruLogicalDisconnect;
PTSELECT	  PassThruSelect;
#endif 


/* Maximum number of OBD ECUs and header size */
#define OBD_MAX_ECUS            8
#define OBD_MAX_PROTOCOLS       5//7

/* Function return value definitions */
#define PASS                    0
#define FAIL                    1

/* Tester node address */
#define TESTER_NODE_ADDRESS     0xF1

/* User prompt type definitions */
#define ENTER_PROMPT            1
#define YES_NO_PROMPT           2

/* Protocol list structure */
typedef struct
{
    unsigned long Protocol;
    unsigned long ChannelID;
    unsigned long InitFlags;
    unsigned long FilterID;
    unsigned long FlowFilterID;
    char Name[20];
} PROTOCOL_LIST;

/* NOTE: Keep some statistics (average response time, ...)? */

/* OBD data structures */
typedef struct
{
    unsigned char PID;
    unsigned char Data[4];
} SID1;

typedef struct
{
    unsigned char PID;
    unsigned char FrameNumber;
    unsigned char Data[4];
} SID2;

typedef struct
{
    unsigned char OBDMID;
    unsigned char SDTID;
    unsigned char UASID;
    unsigned char TVHI;
    unsigned char TVLO;
    unsigned char MINTLHI;
    unsigned char MINTLLO;
    unsigned char MAXTLHI;
    unsigned char MAXTLLO;
} SID6;

typedef struct
{
    unsigned char FirstID;
    unsigned char IDBits[4];
} ID_SUPPORT;

typedef struct
{
    unsigned char FirstID;
    unsigned char FrameNumber;
    unsigned char IDBits[4];
} FF_SUPPORT;

/* NOTE: Change data sizes to definitions for maximums */
typedef struct
{
    unsigned char Header[4];
    unsigned char Sid1PidSupportSize;
    ID_SUPPORT    Sid1PidSupport[8];
    unsigned char Sid1PidSize;
    unsigned char Sid1Pid[8];
    unsigned char Sid2PidSupportSize;
    FF_SUPPORT    Sid2PidSupport[8];
    unsigned char Sid2PidSize;
    unsigned char Sid2Pid[256];
    unsigned char Sid3Size;
    unsigned char Sid3[8];
    unsigned char Sid4Size;
    unsigned char Sid4[8];
    unsigned char Sid5TidSupportSize;
    ID_SUPPORT    Sid5TidSupport[8];
    unsigned char Sid5TidSize;
    unsigned char Sid5Tid[256];
    unsigned char Sid6MidSupportSize;
    ID_SUPPORT    Sid6MidSupport[8];
    unsigned char Sid6MidSize;
    unsigned char Sid6Mid[256];
    unsigned char Sid7Size;
    unsigned char Sid7[8];
    unsigned char Sid8TidSupportSize;
    ID_SUPPORT    Sid8TidSupport[8];
    unsigned char Sid8TidSize;
    unsigned char Sid8Tid[256];
    unsigned char Sid9InfSupportSize;
    ID_SUPPORT    Sid9InfSupport[8];
    unsigned char Sid9InfSize;
    unsigned char Sid9Inf[256];
} OBD_DATA;

/* Service ID (Mode) request structure */
typedef struct
{
    unsigned char SID;
    unsigned char NumIds;
    unsigned char Ids[8];
} SID_REQ;

/* Local function prototypes */
int FindJ2534Interface(void);
int DetermineOBDProtocol(void);
int CheckMILBulb(void);
int SidRequest(SID_REQ *);
int SidSaveResponseData(PASSTHRU_MSG *);
int ConnectProtocol(void);
int DisconnectProtocol(void);
void InitProtocolList(void);
void Log2534Error(char *, unsigned long, char *);
void LogPrint(const char *, ...);
void LogMsg(PASSTHRU_MSG *);
char LogUserPrompt(char *, unsigned long);

/* Global variables */
unsigned char OBDDetermined = FALSE;
unsigned long OBDRequestDelay = 100;
unsigned long OBDListIndex = 0;
PROTOCOL_LIST OBDList[OBD_MAX_PROTOCOLS];
unsigned long OBDMaxResponseTimeMsecs = (1000 + 1);
unsigned long OBDNumEcus = 0;
OBD_DATA OBDResponse[OBD_MAX_ECUS];

FILE *hLogFile = NULL;

// Devices Supported
struct DeviceEntry
{
    char *RegistryDescription;
    bool bCAN_PinSwitchSupported;
    bool bTestVSI4Version;
    char *UpgradedVersion;
};

const struct DeviceEntry Devices[] = {
   {"Cardone Industries, Inc. - FLASH2 All-Makes Reprogrammer", true, true, "1.2"},
   {"Dearborn Group Technology, Inc. - Beacon", false, false, ""},
   {"Dearborn Group Technology, Inc. - Gryphon/SCAT/SCAT2/SCAT3", false, false, ""},
   {"Dearborn Group Technology, Inc. - Python1B", false, false},
   {"Dearborn Group, Inc. - VSI-2534", true, true, "1.2"},
   {"Dearborn Group, Inc. - DPA 5", true, false, ""},
   {"Dearborn Group, Inc. - Px-3", false, false, ""},
   {"Dearborn Group Technology, Inc. - Px2", false, false, ""},
   {"Dearborn Group Technology, Inc. - Px-2", false, false, ""},
   {"Autocom Diagnostic Partner AB - Euro5", false, false, ""},
   {"Autocom Diagnostic Partner AB - SoftBridge", true, false, ""},
   {"W�rth Online World! - WoWFlashBox", true, false, ""},
   {"Delphi - Delphi Euro 5 Diagnostics", true, false, ""},
   {"KeylessRide - KeylessRide HotWire", true, true, "2."},
   {"Siemens - BlueVCI", false, false, ""},
   {"Dearborn Group, Inc. - d-briDGe", true, false, ""},
   {"Dearborn Group, Inc. - Netbridge", true, false, ""},
   {"Dearborn Group, Inc. - SVCI", true, false, ""}
};

// Devices that support J2534-2 features
#define DEVICE_ENTRY_COUNT sizeof(Devices)/sizeof(DeviceEntry)

// ProtocolEntry defined in j2534.h
const struct ProtocolEntry CANProtocols[] = {
    {CAN,           "CAN"},
    {ISO15765,      "ISO15765"},
    {SW_ISO15765_PS,"SW_ISO15765_PS"},
    {SW_CAN_PS,     "SW_CAN_PS"},
    {J1939,         "J1939"},    
    {J1939_CH1,     "J1939_CH1"},
    {J1939_CH2,     "J1939_CH2"},
    {J1939_CH3,     "J1939_CH3"},
    {J1939_CH4,     "J1939_CH4"},
    {CAN_FD,        "CAN_FD"},
    {CAN_PS,        "CAN_PS"},
    {ISO15765_PS,   "ISO15765_PS"},
    {FT_ISO15765_PS, "FT_ISO15765_PS"},
    {FT_CAN_PS,      "FT_CAN_PS"},
    {FT_ISO15765_CH1, "FT_ISO15765_CH1"},
    {FT_CAN_CH1,     "FT_CAN_CH1"}, 
    {SW_CAN_ISO15765_CH1, "SW_CAN_ISO15765_CH1"},
    {SW_CAN_CAN_CH1,    "SW_CAN_CAN_CH1"},
    {CAN_CH1,       "CAN_CH1"},
    {CAN_CH2,       "CAN_CH2"},   
    {CAN_CH3,       "CAN_CH3"},
    {CAN_CH4,       "CAN_CH4"},
    {ISO15765_CH1,  "ISO15765_CH1"},
    {ISO15765_CH2,  "ISO15765_CH2"},
    {ISO15765_CH3,  "ISO15765_CH3"},
    {ISO15765_CH4,  "ISO15765_CH4"}
};

#define CAN_PROTOCOL_ENTRY_COUNT sizeof(CANProtocols)/sizeof(ProtocolEntry)
