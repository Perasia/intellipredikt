#ifndef __J2534_H
#define __J2534_H

// Protocols supported
typedef enum
{
    J1850VPW = 0x01,            // J1850VPW Protocol
    J1850PWM,                   // J1850PWM Protocol
    ISO9141,                    // ISO9141 Protocol
    ISO14230,                   // ISO14230 Protocol
    CAN,                        // CAN Protocol
    ISO15765,
    SCI_A_ENGINE,
    SCI_A_TRANS,
    SCI_B_ENGINE,
    SCI_B_TRANS,
    ISO14230_PS = 0x8003,
    CAN_PS,
    ISO15765_PS,
    SW_ISO15765_PS = 0x8007,
    SW_CAN_PS,
    GM_UART,
	UART_ECHO_BYTE_PS = 0x800A,
    J1939 = 0x800c,
    J1708,
    FT_CAN_PS = 0x800f,
    FT_ISO15765_PS,
    CAN_CH1 = 0x9000,
    CAN_CH2,
    CAN_CH3,
    CAN_CH4,
    ISO15765_CH1 = 0x9280,
    ISO15765_CH2,
    ISO15765_CH3,
    ISO15765_CH4,        
    SW_CAN_CAN_CH1 = 0x9300,
    SW_CAN_ISO15765_CH1 = 0x9380,
    FT_CAN_CH1 = 0x9480,
	FT_ISO15765_CH1 = 0x9500,
    J1939_CH1 = 0x9700,                 // Use 'Reserved' Protocol ID for now
    J1939_CH2,                          // Use 'Reserved' Protocol ID for now
    J1939_CH3,                          // Use 'Reserved' Protocol ID for now
    J1939_CH4,                          // Use 'Reserved' Protocol ID for now    
    CCD = 0x10000,
    CAN_FD = 0x10100,
    LIN,
	ISO15765_LOGICAL = 0x00000200
} 
J2534_PROTOCOL;

struct ProtocolEntry
{
    J2534_PROTOCOL J2534ProtocolID;
    char *Description;
    bool VerifyPinSwitchSupport;
};

const struct ProtocolEntry Protocols[] = {
    {J1850VPW,      "J1850VPW", false},
    {J1850PWM,      "J1850PWM", false},
    {ISO9141,       "ISO9141", false},
    {ISO14230,      "ISO14230", false},
    {CAN,           "CAN", false},
    {ISO15765,      "ISO15765", false},
    {SCI_A_ENGINE,  "SCI_A_ENGINE", false},
    {SCI_A_TRANS,   "SCI_A_TRANS", false},
    {SCI_B_ENGINE,  "SCI_B_ENGINE", false},
    {SCI_B_TRANS,   "SCI_B_TRANS", false},
    {SW_ISO15765_PS,"SW_ISO15765_PS", false},
    {SW_CAN_PS,     "SW_CAN_PS", false},
    {GM_UART,       "GM_UART_PS", false},
    {UART_ECHO_BYTE_PS,"UART_ECHO_BYTE_PS", false},
    {J1939,         "J1939", false},
    {J1939_CH1,     "J1939_CH1", false},
    {J1939_CH2,     "J1939_CH2", false},
    {J1939_CH3,     "J1939_CH3", false},
    {J1939_CH4,     "J1939_CH4", false},
    {J1708,         "J1708", false},
    {CCD,           "CCD", false},
    {CAN_FD,        "CAN_FD", false},
    {LIN,           "LIN", false},
    {CAN_PS,        "CAN_PS", true},
    {ISO15765_PS,   "ISO15765_PS", true},
    {FT_ISO15765_PS, "FT_ISO15765_PS", true},
    {FT_CAN_PS,      "FT_CAN_PS", true},
    {FT_ISO15765_CH1, "FT_ISO15765_CH1", false},
    {FT_CAN_CH1,     "FT_CAN_CH1", false},     
    {SW_CAN_ISO15765_CH1, "SW_CAN_ISO15765_CH1", false},
    {SW_CAN_CAN_CH1,    "SW_CAN_CAN_CH1", false},
    {CAN_CH1,        "CAN_CH1", false},
    {CAN_CH2,        "CAN_CH2", false},
    {CAN_CH3,        "CAN_CH3", false},
    {CAN_CH4,        "CAN_CH4", false},
    {ISO15765_CH1,   "ISO15765_CH1", false},
    {ISO15765_CH2,   "ISO15765_CH2", false},
    {ISO15765_CH3,   "ISO15765_CH3", false},
    {ISO15765_CH4,   "ISO15765_CH4", false},
	{ISO15765_LOGICAL,   "ISO15765_LOGICAL", false }
};

#define PROTOCOL_ENTRY_COUNT sizeof(Protocols)/sizeof(ProtocolEntry)
#ifdef J2534_0500
typedef enum
{
	GET_CONFIG = 0x01,
	SET_CONFIG,
	READ_PIN_VOLTAGE,
	FIVE_BAUD_INIT,
	FAST_INIT,
	CLEAR_TX_BUFFER = 0x07,
	CLEAR_RX_BUFFER,
	CLEAR_PERIODIC_MSGS,
	CLEAR_MSG_FILTERS,
	CLEAR_FUNCT_MSG_LOOKUP_TABLE,
	ADD_TO_FUNCT_MSG_LOOKUP_TABLE,
	DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE,
	READ_PROG_VOLTAGE,
	BUS_ON
}
J2534IOCTLID;
#endif
#ifndef J2534_0500
/* IOCTL IDs */
typedef enum
{
	GET_CONFIG = 0x01,
	SET_CONFIG,
	READ_VBATT,
	FIVE_BAUD_INIT,
	FAST_INIT,
	SET_PIN_USE,
	CLEAR_TX_BUFFER,
	CLEAR_RX_BUFFER,
	CLEAR_PERIODIC_MSGS,
	CLEAR_MSG_FILTERS,
	CLEAR_FUNCT_MSG_LOOKUP_TABLE,
	ADD_TO_FUNCT_MSG_LOOKUP_TABLE,
	DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE,
	READ_PROG_VOLTAGE,
	SWCAN_HS = 0x8000,
	SWCAN_NS,
	SET_POLL_RESPONSE,
	BECOME_MASTER,
	PROTECT_J1939_ADDR = 0x8009, 
    GET_DEVICE_INFO = 0x800C,
    GET_PROTOCOL_INFO = 0x800D,
	READ_MEMORY = 0x10000,
	WRITE_MEMORY =0x10001,
	READ_VWALLJACK,
	READ_VPWRVEH,
	TURN_AUX_OFF,
	TURN_AUX_ON,
	START_PULSE,
	READ_VIGNITION,  
	CAN_SET_BTR	= 0x10100, 
	GET_TIMESTAMP,
	SET_TIMESTAMP,
	GET_SERIAL_NUMBER,
	CAN_SET_LISTEN_MODE,
	CAN_SET_ERROR_REPORTING,
	CAN_SET_TRANSMIT_MODE,
	CAN_SET_INTERNAL_TERMINATION,
	LIN_SENDWAKEUP = 0x11000,
	LIN_ADD_SCHED,
	LIN_GET_SCHED,
	LIN_GET_SCHED_SIZE,
	LIN_DEL_SCHED,
	LIN_ACT_SCHED,
	LIN_DEACT_SCHED,
	LIN_GET_ACT_SCHED,
	LIN_GET_NUM_SCHEDS,
	LIN_GET_SCHED_NAMES,
	LIN_SET_FLAGS,
	RESETHC08,
	TESTHC08COP
}
J2534IOCTLID;
#endif
#ifndef J2534_0500
/* Configuration Parameter IDs */
typedef enum
{
	DATA_RATE = 0x01,
	LOOPBACK = 0x03,
	NODE_ADDRESS,
	NETWORK_LINE,
	P1_MIN,
	P1_MAX,
	P2_MIN,
	P2_MAX,
	P3_MIN,
	P3_MAX,
	P4_MIN,
	P4_MAX,
	W1,
	W2,
	W3,
	W4,
	W5,
	TIDLE,
	TINIL,
	TWUP,
	PARITY,
	BIT_SAMPLE_POINT,
	SYNC_JUMP_WIDTH,
	W0,
	T1_MAX,
	T2_MAX,
	T4_MAX,
	T5_MAX,
	ISO15765_BS,
	ISO15765_STMIN,
	DATA_BITS,
	FIVE_BAUD_MOD,
	BS_TX,
	STMIN_TX,
	T3_MAX,
	ISO15765_WFT_MAX,
    CAN_MIXED_FORMAT = 0x8000,
    J1962_PINS = 0x8001,
	SWCAN_HS_DATA_RATE = 0x8010,
	SWCAN_SPEEDCHANGE_ENABLE,
    SWCAN_RES_SWITCH,
	UEB_T0_MIN = 0x8028,
	UEB_T1_MAX,
	UEB_T2_MAX,
	UEB_T3_MAX,
	UEB_T4_MIN,
	UEB_T5_MAX,
	UEB_T6_MAX,
	UEB_T7_MIN,
	UEB_T7_MAX,
	UEB_T9_MIN,
    J1939_PINS = 0x803d,
    J1708_PINS,
	J1939_T1,
	J1939_T2,
	J1939_T3,
	J1939_T4,
	J1939_BRDCST_MIN_DELAY,
	CAN_FD_DATA_RATE = 0x10000,
	LIN_BRKSPACE = 0x11000,
	LIN_BRKMARK,
	LIN_IDDELAY,
	LIN_RESPDELAY,
	LIN_INTERBYTE,
	LIN_WAKEUPDELAY,
	LIN_WAKEUPTIMEOUT,
	LIN_WAKEUPTIMEOUT3BRK,
	LIN_MODE,
	LIN_SLEW,
}
J2534IOCTLPARAMID;
#endif

#ifdef J2534_0500
/* Configuration Parameter IDs */
typedef enum
{
	DATA_RATE = 0x01,
	NODE_ADDRESS = 0x04,
	NETWORK_LINE = 0x05,
	P1_MIN,
	P1_MAX,
	P2_MIN,
	P2_MAX,
	P3_MIN,
	P3_MAX,
	P4_MIN,
	P4_MAX,
	W1_MAX,
	W2_MAX,
	W3_MAX,
	W4_MIN,
	W5_MIN,
	TIDLE,
	TINIL,
	TWUP,
	PARITY,
	W0_MIN = 0x19,
	T1_MAX,
	T2_MAX,
	T4_MAX,
	T5_MAX,
	ISO15765_BS,
	ISO15765_STMIN,
	DATA_BITS,
	FIVE_BAUD_MOD,
	BS_TX,
	STMIN_TX,
	T3_MAX,
	ISO15765_WAIT_LIMIT,
	W1_MIN,
	W2_MIN,
	W3_MIN,
	W4_MAX,
	N_BR_MIN,
	ISO15765_PAD_VALUE,
	N_AS_MAX,
	N_AR_MAX,
	N_BS_MAX,
	N_CR_MAX,
	N_CS_MIN,
}
J2534IOCTLPARAMID;
#endif

// GET_DEVICE_INFO ID Values
typedef enum
{
    SERIAL_NUMBER = 0x01,
    J1850PWM_SUPPORTED,
    J1850VPW_SUPPORTED,
    ISO9141_SUPPORTED,
    ISO14230_SUPPORTED,
    CAN_SUPPORTED,
    ISO15765_SUPPORTED,
    SCI_A_ENGINE_SUPPORTED,
    SCI_A_TRANS_SUPPORTED,
    SCI_B_ENGINE_SUPPORTED,
    SCI_B_TRANS_SUPPORTED,
    SW_ISO15765_SUPPORTED,
    SW_CAN_SUPPORTED,
    GM_UART_SUPPORTED,
    UART_ECHO_BYTE_SUPPORTED,
    HONDA_DIAGH_SUPPORTED,
    J1939_SUPPORTED,
    J1708_SUPPORTED,
    TP2_0_SUPPORTED,
    J2610_SUPPORTED,
    ANALOG_IN_SUPPORTED,
    MAX_NON_VOLATILE_STORAGE,
    SHORT_TO_GND_J1962,
    PGM_VOLTAGE_J1962,
    J1850PWM_PS_J1962,
    J1850VPW_PS_J1962,
    ISO9141_PS_K_LINE_J1962,
    ISO9141_PS_L_LINE_J1962,
    ISO14230_PS_K_LINE_J1962,
    ISO14230_PS_L_LINE_J1962,
    CAN_PS_J1962,
    ISO15765_PS_J1962,
    SW_CAN_PS_J1962,
    SW_ISO15765_PS_J1962,
    GM_UART_PS_J1962,
    UART_ECHO_BYTE_PS_J1962,
    HONDA_DIAGH_PS_J1962,
    J1939_PS_J1962,
    J1708_PS_J1962,
    TP2_0_PS_J1962,
    J2610_PS_J1962,
    J1939_PS_J1939,
    J1708_PS_J1939,
    ISO9141_PS_K_LINE_J1939,
    ISO9141_PS_L_LINE_J1939,
    ISO14230_PS_K_LINE_J1939,
    ISO14230_PS_L_LINE_J1939,
    J1708_PS_J1708,
    FT_CAN_SUPPORTED,
    FT_ISO15765_SUPPORTED,
    FT_CAN_PS_J1962,
    FT_ISO15765_PS_J1962,
    J1850PWM_SIMULTANEOUS,
    J1850VPW_SIMULTANEOUS,
    ISO9141_SIMULTANEOUS,
    ISO14230_SIMULTANEOUS,
    CAN_SIMULTANEOUS,
    ISO15765_SIMULTANEOUS,
    SCI_A_ENGINE_SIMULTANEOUS,
    SCI_A_TRANS_SIMULTANEOUS,
    SCI_B_ENGINE_SIMULTANEOUS,
    SCI_B_TRANS_SIMULTANEOUS,
    SW_ISO15765_SIMULTANEOUS,
    SW_CAN_SIMULTANEOUS,
    GM_UART_SIMULTANEOUS,
    UART_ECHO_BYTE_SIMULTANEOUS,
    HONDA_DIAGH_SIMULTANEOUS,
    J1939_SIMULTANEOUS,
    J1708_SIMULTANEOUS,
    TP2_0_SIMULTANEOUS,
    J2610_SIMULTANEOUS,
    ANALOG_IN_SIMULTANEOUS,
    PART_NUMBER,
    FT_CAN_SIMULTANEOUS,
    FT_ISO15765_SIMULTANEOUS
}
J2534GETDEVICEINFOPARAMID;

// GET_PROTOCOL_INFO ID Values
typedef enum
{
    MAX_RX_BUFFER_SIZE = 0x01,
    MAX_PASS_FILTER,
    MAX_BLOCK_FILTER,
    MAX_FILTER_MSG_LENGTH,
    MAX_PERIODIC_MSGS,
    MAX_PERIODIC_MSG_LENGTH,
    DESIRED_DATA_RATE,
    MAX_REPEAT_MESSAGING,
    MAX_REPEAT_MESSAGING_LENGTH,
    NETWORK_LINE_SUPPORTED,
    MAX_FUNCT_MSG_LOOKUP,
    PARITY_SUPPORTED,
    DATA_BITS_SUPPORTED,
    FIVE_BAUD_MOD_SUPPORTED,
    L_LINE_SUPPORTED,
    CAN_11_29_IDS_SUPPORTED,
    CAN_MIXED_FORMAT_SUPPORTED,
    MAX_FLOW_CONTROL_FILTER,
    MAX_ISO15765_WFT_MAX,
    MAX_AD_ACTIVE_CHANNELS,
    MAX_AD_SAMPLE_RATE,
    MAX_AD_SAMPLES_PER_READING,
    AD_SAMPLE_RESOLUTION,
    AD_INPUT_RANGE_LOW,
    AD_INPUT_RANGE_HIGH,
    RESOURCE_GROUP,
    TIMESTAMP_RESOLUTION
}
J2534GETPROTOCOLINFOPARAMID;

struct IoctlParamIdEntry
{
    J2534IOCTLPARAMID IoctlParamID;
    char *Description;
};
#ifndef J2534_0500
const struct IoctlParamIdEntry IoctlParamId[] = {
    {DATA_RATE,         "DATA_RATE"},
    {LOOPBACK,          "LOOPBACK"},
    {NODE_ADDRESS,      "NODE_ADDRESS"},
    {NETWORK_LINE,      "NETWORK_LINE"},
    {P1_MIN,            "P1_MIN"},
    {P1_MAX,            "P1_MAX"},
    {P2_MIN,            "P2_MIN"},
    {P2_MAX,            "P2_MAX"},
    {P3_MIN,            "P3_MIN"},
    {P3_MAX,            "P3_MAX"},
    {P4_MIN,            "P4_MIN"},
    {P4_MAX,            "P4_MAX"},
    {W1,                "W1"},
    {W2,                "W2"},
    {W3,                "W3"},
    {W4,                "W4"},
    {W5,                "W5"},
    {TIDLE,             "TIDLE"},
    {TINIL,             "TINIL"},
    {TWUP,              "TWUP"},
    {PARITY,            "PARITY"},
    {BIT_SAMPLE_POINT,  "BIT_SAMPLE_POINT"},
    {SYNC_JUMP_WIDTH,   "SYNC_JUMP_WIDTH"},
    {W0,                "W0"},
    {T1_MAX,            "T1_MAX"},
    {T2_MAX,            "T2_MAX"},
    {T4_MAX,            "T4_MAX"},
    {T5_MAX,            "T5_MAX"},
    {ISO15765_BS,       "ISO15765_BS"},
    {ISO15765_STMIN,    "ISO15765_STMIN"},
    {DATA_BITS,         "DATA_BITS"},
    {FIVE_BAUD_MOD,     "FIVE_BAUD_MOD"},
    {BS_TX,             "BS_TX"},
    {STMIN_TX,          "STMIN_TX"},
    {T3_MAX,            "T3_MAX"},
    {ISO15765_WFT_MAX,  "ISO15765_WFT_MAX"},
    {CAN_MIXED_FORMAT,  "CAN_MIXED_FORMAT"},
    {J1962_PINS,        "J1962_PINS"},
    {J1939_PINS,        "J1939_PINS"},
    {J1708_PINS,        "J1708_PINS"},
    {SWCAN_HS_DATA_RATE,        "SWCAN_HS_DATA_RATE"},
    {SWCAN_SPEEDCHANGE_ENABLE,  "SWCAN_SPEEDCHANGE_ENABLE"},
    {SWCAN_RES_SWITCH,          "SWCAN_RES_SWITCH"},
    {UEB_T0_MIN,        "UEB_T0_MIN"},
    {UEB_T1_MAX,        "UEB_T1_MAX"},
    {UEB_T2_MAX,        "UEB_T2_MAX"},
    {UEB_T3_MAX,        "UEB_T3_MAX"},
    {UEB_T4_MIN,        "UEB_T4_MIN"},
    {UEB_T5_MAX,        "UEB_T5_MAX"},
    {UEB_T6_MAX,        "UEB_T6_MAX"},
    {UEB_T7_MIN,        "UEB_T7_MIN"},
    {UEB_T7_MAX,        "UEB_T7_MAX"},
    {UEB_T9_MIN,        "UEB_T9_MIN"},
    {CAN_FD_DATA_RATE,  "CAN_FD_DATA_RATE"},
    {LIN_BRKSPACE,  "LIN_BRKSPACE"},
    {LIN_BRKMARK,  "LIN_BRKMARK"},
    {LIN_IDDELAY,  "LIN_IDDELAY"},
    {LIN_RESPDELAY,  "LIN_RESPDELAY"},
    {LIN_INTERBYTE,  "LIN_INTERBYTE"},
    {LIN_WAKEUPDELAY,  "LIN_WAKEUPDELAY"},
    {LIN_WAKEUPTIMEOUT,  "LIN_WAKEUPTIMEOUT"},
    {LIN_WAKEUPTIMEOUT3BRK,  "LIN_WAKEUPTIMEOUT3BRK"},
    {LIN_MODE,  "LIN_MODE"},
    {LIN_SLEW,  "LIN_SLEW"},
};
#endif  
#ifdef J2534_0500
const struct IoctlParamIdEntry IoctlParamId[] = {
	{ DATA_RATE,         "DATA_RATE" },
	{ NODE_ADDRESS,      "NODE_ADDRESS" },
	{ NETWORK_LINE,      "NETWORK_LINE" },
	{ P1_MIN,            "P1_MIN" },
	{ P1_MAX,            "P1_MAX" },
	{ P2_MIN,            "P2_MIN" },
	{ P2_MAX,            "P2_MAX" },
	{ P3_MIN,            "P3_MIN" },
	{ P3_MAX,            "P3_MAX" },
	{ P4_MIN,            "P4_MIN" },
	{ P4_MAX,            "P4_MAX" },
	{ W1_MAX,                "W1_MAX" },
	{ W2_MAX,                "W2_MAX" },
	{ W3_MAX,                "W3_MAX" },
	{ W4_MIN,                "W4_MIN" },
	{ W5_MIN,                "W5_MIN" },
	{ TIDLE,             "TIDLE" },
	{ TINIL,             "TINIL" },
	{ TWUP,              "TWUP" },
	{ PARITY,            "PARITY" },
	{ W0_MIN,            "W0_MIN"},
	{ T1_MAX,            "T1_MAX" },
	{ T2_MAX,            "T2_MAX" },
	{ T4_MAX,            "T4_MAX" },
	{ T5_MAX,            "T5_MAX" },
	{ ISO15765_BS,       "ISO15765_BS" },
	{ ISO15765_STMIN,    "ISO15765_STMIN" },
	{ DATA_BITS,         "DATA_BITS" },
	{ FIVE_BAUD_MOD,     "FIVE_BAUD_MOD" },
	{ BS_TX,             "BS_TX" },
	{ STMIN_TX,          "STMIN_TX" },
	{ T3_MAX,            "T3_MAX" },
	{ ISO15765_WAIT_LIMIT,  "ISO15765_WAIT_LIMIT" },
	{ W1_MIN,  "W1_MIN" },
	{ W2_MIN,  "W2_MIN" },
	{ W3_MIN,  "W3_MIN" },
	{ W4_MAX,  "W4_MAX" },
	{ N_BR_MIN,  "N_BR_MIN"},
	{ ISO15765_PAD_VALUE,  "ISO15765_PAD_VALUE" },
	{ N_AS_MAX,  "N_AS_MAX" },
	{ N_AR_MAX,  "N_AR_MAX" },
	{ N_BS_MAX,  "N_BS_MAX" },
	{ N_CR_MAX,  "N_CR_MAX" },
	{ N_CS_MIN,  "N_CS_MIN" },
};
#endif
#define IOCTL_PARAM_ENTRY_COUNT sizeof(IoctlParamId)/sizeof(IoctlParamIdEntry)

struct IoctlGetDeviceInfoParamIdEntry
{
    J2534GETDEVICEINFOPARAMID IoctlGetDeviceInfoParamID;
    char *Description;
};

const struct IoctlGetDeviceInfoParamIdEntry IoctlGetDeviceInfoParamId[] = {
    {SERIAL_NUMBER, "SERIAL_NUMBER"},
    {J1850PWM_SUPPORTED, "J1850PWM_SUPPORTED"},
    {J1850VPW_SUPPORTED, "J1850VPW_SUPPORTED"},
    {ISO9141_SUPPORTED, "ISO9141_SUPPORTED"},
    {ISO14230_SUPPORTED, "ISO14230_SUPPORTED"},
    {CAN_SUPPORTED, "CAN_SUPPORTED"},
    {ISO15765_SUPPORTED, "ISO15765_SUPPORTED"},
    {SCI_A_ENGINE_SUPPORTED, "SCI_A_ENGINE_SUPPORTED"},
    {SCI_A_TRANS_SUPPORTED, "SCI_A_TRANS_SUPPORTED"},
    {SCI_B_ENGINE_SUPPORTED, "SCI_B_ENGINE_SUPPORTED"},
    {SCI_B_TRANS_SUPPORTED, "SCI_B_TRANS_SUPPORTED"},
    {SW_ISO15765_SUPPORTED, "SW_ISO15765_SUPPORTED"},
    {SW_CAN_SUPPORTED, "SW_CAN_SUPPORTED"},
    {GM_UART_SUPPORTED, "GM_UART_SUPPORTED"},
    {UART_ECHO_BYTE_SUPPORTED, "UART_ECHO_BYTE_SUPPORTED"},
    {HONDA_DIAGH_SUPPORTED, "HONDA_DIAGH_SUPPORTED"},
    {J1939_SUPPORTED, "J1939_SUPPORTED"},
    {J1708_SUPPORTED, "J1708_SUPPORTED"},
    {TP2_0_SUPPORTED, "TP2_0_SUPPORTED"},
    {J2610_SUPPORTED, "J2610_SUPPORTED"},
    {ANALOG_IN_SUPPORTED, "ANALOG_IN_SUPPORTED"},
    {MAX_NON_VOLATILE_STORAGE, "MAX_NON_VOLATILE_STORAGE"},
    {SHORT_TO_GND_J1962, "SHORT_TO_GND_J1962"},
    {PGM_VOLTAGE_J1962, "PGM_VOLTAGE_J1962"},
    {J1850PWM_PS_J1962, "J1850PWM_PS_J1962"},
    {J1850VPW_PS_J1962, "J1850VPW_PS_J1962"},
    {ISO9141_PS_K_LINE_J1962, "ISO9141_PS_K_LINE_J1962"},
    {ISO9141_PS_L_LINE_J1962, "ISO9141_PS_L_LINE_J1962"},
    {ISO14230_PS_K_LINE_J1962, "ISO14230_PS_K_LINE_J1962"},
    {ISO14230_PS_L_LINE_J1962, "ISO14230_PS_L_LINE_J1962"},
    {CAN_PS_J1962, "CAN_PS_J1962"},
    {ISO15765_PS_J1962, "ISO15765_PS_J1962"},
    {SW_CAN_PS_J1962, "SW_CAN_PS_J1962"},
    {SW_ISO15765_PS_J1962, "SW_ISO15765_PS_J1962"},
    {GM_UART_PS_J1962, "GM_UART_PS_J1962"},
    {UART_ECHO_BYTE_PS_J1962, "UART_ECHO_BYTE_PS_J1962"},
    {HONDA_DIAGH_PS_J1962, "HONDA_DIAGH_PS_J1962"},
    {J1939_PS_J1962, "J1939_PS_J1962"},
    {J1708_PS_J1962, "J1708_PS_J1962"},
    {TP2_0_PS_J1962, "TP2_0_PS_J1962"},
    {J2610_PS_J1962, "J2610_PS_J1962"},
    {J1939_PS_J1939, "J1939_PS_J1939"},
    {J1708_PS_J1939, "J1708_PS_J1939"},
    {ISO9141_PS_K_LINE_J1939, "ISO9141_PS_K_LINE_J1939"},
    {ISO9141_PS_L_LINE_J1939, "ISO9141_PS_L_LINE_J1939"},
    {ISO14230_PS_K_LINE_J1939, "ISO14230_PS_K_LINE_J1939"},
    {ISO14230_PS_L_LINE_J1939, "ISO14230_PS_L_LINE_J1939"},
    {J1708_PS_J1708, "J1708_PS_J1708"},
    {FT_CAN_SUPPORTED, "FT_CAN_SUPPORTED"},
    {FT_ISO15765_SUPPORTED, "FT_ISO15765_SUPPORTED"},
    {FT_CAN_PS_J1962, "FT_CAN_PS_J1962"},
    {FT_ISO15765_PS_J1962, "FT_ISO15765_PS_J1962"},
    {J1850PWM_SIMULTANEOUS, "J1850PWM_SIMULTANEOUS"},
    {J1850VPW_SIMULTANEOUS, "J1850VPW_SIMULTANEOUS"},
    {ISO9141_SIMULTANEOUS, "ISO9141_SIMULTANEOUS"},
    {ISO14230_SIMULTANEOUS, "ISO14230_SIMULTANEOUS"},
    {CAN_SIMULTANEOUS, "CAN_SIMULTANEOUS"},
    {ISO15765_SIMULTANEOUS, "ISO15765_SIMULTANEOUS"},
    {SCI_A_ENGINE_SIMULTANEOUS, "SCI_A_ENGINE_SIMULTANEOUS"},
    {SCI_A_TRANS_SIMULTANEOUS, "SCI_A_TRANS_SIMULTANEOUS"},
    {SCI_B_ENGINE_SIMULTANEOUS, "SCI_B_ENGINE_SIMULTANEOUS"},
    {SCI_B_TRANS_SIMULTANEOUS, "SCI_B_TRANS_SIMULTANEOUS"},
    {SW_ISO15765_SIMULTANEOUS, "SW_ISO15765_SIMULTANEOUS"},
    {SW_CAN_SIMULTANEOUS, "SW_CAN_SIMULTANEOUS"},
    {GM_UART_SIMULTANEOUS, "GM_UART_SIMULTANEOUS"},
    {UART_ECHO_BYTE_SIMULTANEOUS, "UART_ECHO_BYTE_SIMULTANEOUS"},
    {HONDA_DIAGH_SIMULTANEOUS, "HONDA_DIAGH_SIMULTANEOUS"},
    {J1939_SIMULTANEOUS, "J1939_SIMULTANEOUS"},
    {J1708_SIMULTANEOUS, "J1708_SIMULTANEOUS"},
    {TP2_0_SIMULTANEOUS, "TP2_0_SIMULTANEOUS"},
    {J2610_SIMULTANEOUS, "J2610_SIMULTANEOUS"},
    {ANALOG_IN_SIMULTANEOUS, "ANALOG_IN_SIMULTANEOUS"},
    {PART_NUMBER, "PART_NUMBER"},
    {FT_CAN_SIMULTANEOUS, "FT_CAN_SIMULTANEOUS"},
    {FT_ISO15765_SIMULTANEOUS, "FT_ISO15765_SIMULTANEOUS"},
};
     
#define IOCTL_GETDEVICEINFO_PARAM_ENTRY_COUNT sizeof(IoctlGetDeviceInfoParamId)/sizeof(IoctlGetDeviceInfoParamIdEntry)

struct IoctlGetProtocolInfoParamIdEntry
{
    J2534GETPROTOCOLINFOPARAMID IoctlGetProtocolInfoParamID;
    char *Description;
};

const struct IoctlGetProtocolInfoParamIdEntry IoctlGetProtocolInfoParamId[] = {
    {MAX_RX_BUFFER_SIZE, "MAX_RX_BUFFER_SIZE"},
    {MAX_PASS_FILTER, "MAX_PASS_FILTER"},
    {MAX_BLOCK_FILTER, "MAX_BLOCK_FILTER"},
    {MAX_FILTER_MSG_LENGTH, "MAX_FILTER_MSG_LENGTH"},
    {MAX_PERIODIC_MSGS, "MAX_PERIODIC_MSGS"},
    {MAX_PERIODIC_MSG_LENGTH, "MAX_PERIODIC_MSG_LENGTH"},
    {DESIRED_DATA_RATE, "DESIRED_DATA_RATE"},
    {MAX_REPEAT_MESSAGING, "MAX_REPEAT_MESSAGING"},
    {MAX_REPEAT_MESSAGING_LENGTH, "MAX_REPEAT_MESSAGING_LENGTH"},
    {NETWORK_LINE_SUPPORTED, "NETWORK_LINE_SUPPORTED"},
    {MAX_FUNCT_MSG_LOOKUP, "MAX_FUNCT_MSG_LOOKUP"},
    {PARITY_SUPPORTED, "PARITY_SUPPORTED"},
    {DATA_BITS_SUPPORTED, "DATA_BITS_SUPPORTED"},
    {FIVE_BAUD_MOD_SUPPORTED, "FIVE_BAUD_MOD_SUPPORTED"},
    {L_LINE_SUPPORTED, "L_LINE_SUPPORTED"},
    {CAN_11_29_IDS_SUPPORTED, "CAN_11_29_IDS_SUPPORTED"},
    {CAN_MIXED_FORMAT_SUPPORTED, "CAN_MIXED_FORMAT_SUPPORTED"},
    {MAX_FLOW_CONTROL_FILTER, "MAX_FLOW_CONTROL_FILTER"},
    {MAX_ISO15765_WFT_MAX, "MAX_ISO15765_WFT_MAX"},
    {MAX_AD_ACTIVE_CHANNELS, "MAX_AD_ACTIVE_CHANNELS"},
    {MAX_AD_SAMPLE_RATE, "MAX_AD_SAMPLE_RATE"},
    {MAX_AD_SAMPLES_PER_READING, "MAX_AD_SAMPLES_PER_READING"},
    {AD_SAMPLE_RESOLUTION, "AD_SAMPLE_RESOLUTION"},
    {AD_INPUT_RANGE_LOW, "AD_INPUT_RANGE_LOW"},
    {AD_INPUT_RANGE_HIGH, "AD_INPUT_RANGE_HIGH"},
    {RESOURCE_GROUP, "RESOURCE_GROUP"},
    {TIMESTAMP_RESOLUTION, "TIMESTAMP_RESOLUTION"},
};
     
#define IOCTL_GETPROTOCOLINFO_PARAM_ENTRY_COUNT sizeof(IoctlGetProtocolInfoParamId)/sizeof(IoctlGetProtocolInfoParamIdEntry)
// J2534 Error codes
#ifdef J2534_0500
typedef enum
{
	STATUS_NOERROR = 0,             // Function call successful.
	ERR_NOT_SUPPORTED,            // Function not supported.
	ERR_INVALID_CHANNEL_ID,       // Invalid ChannelID value.
	ERR_INVALID_PROTOCOL_ID,      // Invalid ProtocolID value.
	ERR_NULLPARAMETER,            // NULL pointer supplied where a valid 
	// pointer
	// is required.
	ERR_INVALID_IOCTL_VALUE,      // Invalid value for Ioctl parameter 
	ERR_INVALID_FLAGS,            // Invalid flag values.
	ERR_FAILED,                   // Undefined error, use 
	// PassThruGetLastError for description
	// of error.
	ERR_DEVICE_NOT_CONNECTED,     // Unable to communicate with device
	ERR_TIMEOUT,                  // Timeout. No message available to 
	// read or 
	// could not read the specified no. of 
	// msgs.
	ERR_INVALID_MSG,              // Invalid message structure pointed 
	// to by pMsg.
	ERR_INVALID_TIME_INTERVAL,    // Invalid TimeInterval value.
	ERR_EXCEEDED_LIMIT,           // ALL periodic message IDs have been 
	// used.
	ERR_INVALID_MSG_ID,           // Invalid MsgID value.
	ERR_DEVICE_IN_USE,            // Device already open and/or in use
	ERR_INVALID_IOCTL_ID,         // Invalid IoctlID value.
	ERR_BUFFER_EMPTY,             // Protocol message buffer empty.
	ERR_BUFFER_FULL,              // Protocol message buffer full.
	ERR_BUFFER_OVERFLOW,          // Protocol message buffer overflow.
	ERR_PIN_INVALID,              // Invalid pin number.
	ERR_CHANNEL_IN_USE,           // Channel already in use.
	ERR_MSG_PROTOCOL_ID,          // Protocol type does not match the
	// protocol associated with the 
	// Channel ID
	ERR_INVALID_FILTER_ID,        // Invalid MsgID value
	ERR_NO_FLOW_CONTROL,          // An attempt was made to send a message
	// on an ISO15765 ChannelID before a flow
	// control filter was established.  
	ERR_NOT_UNIQUE,               // A CAN ID in PatternMsg or FlowControlMsg
	// matches either ID in an existing Flow Control Filter
	ERR_INVALID_BAUDRATE,         // Desired baud rate cannot be achieved within tolerances           
	ERR_INVALID_DEVICE_ID,        // Device ID Invalid      
	ERR_PIN_NOT_SUPPORTED,
	ERR_FILTER_TYPE_NOT_SUPPORTED,
	ERR_DEVICE_NOT_OPEN,
	ERR_LOG_CHAN_NOT_ALLOWED,
	ERR_MSG_NOT_ALLOWED,
	ERR_ADDRESS_NOT_CLAIMED = 0x10000,
	ERR_NO_CONNECTION_ESTABLISHED
}
J2534ERROR;

#endif
#ifdef J2534_0404
typedef enum
{
	STATUS_NOERROR = 0,             // Function call successful.
	ERR_NOT_SUPPORTED,            // Function not supported.
	ERR_INVALID_CHANNEL_ID,       // Invalid ChannelID value.
	ERR_INVALID_PROTOCOL_ID,      // Invalid ProtocolID value.
	ERR_NULLPARAMETER,            // NULL pointer supplied where a valid 
	// pointer
	// is required.
	ERR_INVALID_IOCTL_VALUE,      // Invalid value for Ioctl parameter 
	ERR_INVALID_FLAGS,            // Invalid flag values.
	ERR_FAILED,                   // Undefined error, use 
	// PassThruGetLastError for description
	// of error.
	ERR_DEVICE_NOT_CONNECTED,     // Unable to communicate with device
	ERR_TIMEOUT,                  // Timeout. No message available to 
	// read or 
	// could not read the specified no. of 
	// msgs.
	ERR_INVALID_MSG,              // Invalid message structure pointed 
	// to by pMsg.
	ERR_INVALID_TIME_INTERVAL,    // Invalid TimeInterval value.
	ERR_EXCEEDED_LIMIT,           // ALL periodic message IDs have been 
	// used.
	ERR_INVALID_MSG_ID,           // Invalid MsgID value.
	ERR_DEVICE_IN_USE,            // Device already open and/or in use
	ERR_INVALID_IOCTL_ID,         // Invalid IoctlID value.
	ERR_BUFFER_EMPTY,             // Protocol message buffer empty.
	ERR_BUFFER_FULL,              // Protocol message buffer full.
	ERR_BUFFER_OVERFLOW,          // Protocol message buffer overflow.
	ERR_PIN_INVALID,              // Invalid pin number.
	ERR_CHANNEL_IN_USE,           // Channel already in use.
	ERR_MSG_PROTOCOL_ID,          // Protocol type does not match the
	// protocol associated with the 
	// Channel ID
	ERR_INVALID_FILTER_ID,        // Invalid MsgID value
	ERR_NO_FLOW_CONTROL,          // An attempt was made to send a message
	// on an ISO15765 ChannelID before a flow
	// control filter was established.  
	ERR_NOT_UNIQUE,               // A CAN ID in PatternMsg or FlowControlMsg
	// matches either ID in an existing Flow Control Filter
	ERR_INVALID_BAUDRATE,         // Desired baud rate cannot be achieved within tolerances           
	ERR_INVALID_DEVICE_ID,        // Device ID Invalid                                       
	ERR_ADDRESS_NOT_CLAIMED = 0x10000,
	ERR_NO_CONNECTION_ESTABLISHED
}
J2534ERROR;
#endif

/* Error IDs */

/* Miscellaneous definitions */
#define SHORT_TO_GROUND						0xFFFFFFFE
#define VOLTAGE_OFF							0xFFFFFFFF

/* RxStatus definitions */
#define TX_MSG_TYPE							0x00000001
#define ISO15765_FIRST_FRAME				0x00000002
#ifndef RX_BREAK
#define RX_BREAK							0x00000004
#endif
#define	ISO15765_TX_DONE					0x00000008

/* TxFlags definitions */
#define TX_NORMAL_TRANSMIT					0x00000000
#define ISO15765_FRAME_PAD					0x00000040
#define ISO15765_EXT_ADDR					0x00000080
#ifndef CAN_29BIT_ID
#define CAN_29BIT_ID						0x00000100
#endif
#define CAN_EXTENDED_ID						0x00000100
#define TX_BLOCKING							0x00010000
#define SCI_TX_VOLTAGE						0x00800000

/* Filter definitions */
#define PASS_FILTER							0x00000001
#define BLOCK_FILTER						0x00000002
#define FLOW_CONTROL_FILTER					0x00000003

#ifdef J2534_0500 // For J2534 -1 V0500 Spec Marcro. To check specific functionality of V0500
// Values for <SelectType>
#define READABLE_TYPE 0x00000001

typedef struct
{
	unsigned long ProtocolID;
	unsigned long MsgHandle;
	unsigned long RxStatus;
	unsigned long TxFlags;
	unsigned long Timestamp;
	unsigned long DataSize;
	unsigned long ExtraDataIndex;
	unsigned char *Data;
	unsigned long DataBufferSize;
} PASSTHRU_MSG;

typedef struct
{
	char DeviceName[80];
	unsigned long DeviceAvailable;
	unsigned long DeviceDLLFWStatus;
	unsigned long DeviceConnectMedia;
	unsigned long DeviceConnectSpeed;
	unsigned long DeviceSignalQuality;
	unsigned long DeviceSignalStrength;
} SDEVICE;

typedef struct
{
	unsigned long ChannelCount;     /* number of ChannelList elements */
	unsigned long ChannelThreshold; /* minimum number of channels that must have messages */
	unsigned long *ChannelList;     /* pointer to an array of Channel IDs to be monitored */
} SCHANNELSET;
// Values for <Connector>
#define J1962_CONNECTOR 0x00000001

//Reserved for physical communication channels 0x00000008 - 0x00000001FF
#define ISO15765_LOGICAL   0x00000200
//Reserved for logical communication channels 0x00000201 - 0x000003FF
//Reserved for SAE J2534-1 0x00000400 - 0x00007FFF
//Reserved for SAE - 0x00008000 - 0xFFFFFFFF

// Values for <DeviceAvailable>
#define DEVICE_STATE_UNKNOWN  0x00
#define DEVICE_AVAILABLE      0x01
#define DEVICE_IN_USE         0x02
// Values for <DeviceDLLFWStatus>
#define DEVICE_DLL_FW_COMPATIBILTY_UNKNOWN  0x00000000
#define DEVICE_DLL_FW_COMPATIBLE            0x00000001
#define DEVICE_DLL_OR_FW_NOT_COMPATIBLE     0x00000002
#define DEVICE_DLL_NOT_COMPATIBLE           0x00000003
#define DEVICE_FW_NOT_COMPATIBLE            0x00000004

// Values for <DeviceConnectedMedia>
#define DEVICE_CONN_UNKNOWN  0x00000000
#define DEVICE_CONN_WIRELESS 0x00000001
#define DEVICE_CONN_WIRED    0x00000002
#else 
/* Message Structure */
typedef struct
{
	unsigned long ProtocolID;
	unsigned long RxStatus;
	unsigned long TxFlags;
	unsigned long Timestamp;
	unsigned long DataSize;
	unsigned long ExtraDataIndex;
	unsigned char Data[4128];
} PASSTHRU_MSG;
#endif

// SPARAM Structure
typedef struct
{
    unsigned long Parameter;            // name of parameter
    unsigned long Value;                // value of the parameter
    unsigned long Supported;            // support for parameter
}
SPARAM;

// SPARAM_LIST Structure
typedef struct
{
    unsigned long NumOfParams;          // number of SPARAM elements
    SPARAM *ParamPtr;                   // array of SPARAM
}
SPARAM_LIST;

/* IOCTL Structures */
typedef struct
{
	unsigned long Parameter;
	unsigned long Value;
} SCONFIG;

typedef struct
{
	unsigned long NumOfParams;
	SCONFIG *ConfigPtr;
} SCONFIG_LIST;

typedef struct
{
	unsigned long NumOfBytes;
	unsigned char *BytePtr;
} SBYTE_ARRAY;

typedef struct
{
	unsigned long NumOfPins;
	unsigned char *PinUsePtr;
} SPIN_CONTROL;

typedef struct
{
	unsigned long PinNumber;
	unsigned long PinUse;
	unsigned long Parameter;
} SPIN_USE;

typedef struct 
{
	unsigned long Connector;        /* connector identifier */
	unsigned long NumOfResources;   /* number of resources pointed to by ResourceListPtr */
	unsigned long *ResourceListPtr; /* pointer to list of resources */
} RESOURCE_STRUCT;

typedef struct
{
	unsigned long LocalTxFlags;      /* TxFlags for the Local Address */
	unsigned long RemoteTxFlags;     /* TxFlags for the Remote Address */
	char LocalAddress[5];            /* Address for J2534 Device ISO 15765 end point */
	char RemoteAddress[5];           /* Address for remote/vehicle ISO 15765 end point */
} ISO15765_CHANNEL_DESCRIPTOR;

#define SWCAN_DATA_RATE_DEFAULT 33333
#define SWCAN_DATA_RATE_MEDIUM	83333

#endif /* __J2534_H */

